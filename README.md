[![Build Status](https://www.bitrise.io/app/f68117f140b0e451.svg?token=wnFHJzivBppxoNRE2NkBfg)](https://www.bitrise.io/app/f68117f140b0e451)
[![codecov](https://codecov.io/bb/abaenglish/aba-android/branch/master/graph/badge.svg?token=p3yRFELwdG)](https://codecov.io/bb/abaenglish/aba-android)

ABA Android Application
==========

# Build



# Build Variants



# Test

Launch unit tests for internal debug variant:
    `./gradlew clean testInternalDebugUnitTest`

Launch acceptance tests for internal debug variant :
    `./gradlew clean connectedInternalDebugAndroidTest`

Launch unit and acceptance tests and merge code coverage reports:
    `./gradlew clean createInternalDebugCoverageReport jacocoTestReport `


# Links
[**Bitrise**](https://www.bitrise.io/app/f68117f140b0e451) 
[**Codecov**](https://codecov.io/bb/abaenglish/aba-android) 
[**Playstore**](https://play.google.com/store/apps/details?id=com.abaenglish.videoclass) 


# Powered by
![logo](https://github.com/images/error/angry_unicorn.png)