package com.abaenglish.shepherd;

import android.content.Context;
import android.content.SharedPreferences;

import com.abaenglish.shepherd.configuration.configurators.GenericShepherdConfigurator.ShepherdConfiguratorType;
import com.abaenglish.shepherd.configuration.configurators.abacore.ABACoreConfigurator;
import com.abaenglish.shepherd.configuration.configurators.zendesk.ZendeskConfigurator;
import com.abaenglish.shepherd.configuration.engine.exceptions.ShepherdConfigurationException;
import com.abaenglish.shepherd.configuration.engine.model.ShepherdConfiguration;
import com.abaenglish.shepherd.configuration.engine.model.ShepherdGenericEnvironment;
import com.abaenglish.shepherd.plugin.ShepherdPluginInterface;
import com.abaenglish.shepherd.plugin.plugins.ShepherdABTestUnitDetailPlugin;
import com.abaenglish.shepherd.plugin.plugins.ShepherdAppVersioningPlugin;
import com.abaenglish.shepherd.plugin.plugins.ShepherdAutomatorPlugin;
import com.abaenglish.shepherd.plugin.plugins.ShepherdCleanFilesPlugin;
import com.abaenglish.shepherd.plugin.plugins.ShepherdCrashPlugin;
import com.abaenglish.shepherd.plugin.plugins.ShepherdEvaluationPlugin;
import com.abaenglish.shepherd.plugin.plugins.ShepherdExternalLogin;
import com.abaenglish.shepherd.plugin.plugins.ShepherdForceUserTypePlugin;
import com.abaenglish.shepherd.plugin.plugins.ShepherdLanguagePlugin;
import com.abaenglish.shepherd.plugin.plugins.ShepherdLoginPlugin;
import com.abaenglish.shepherd.plugin.plugins.ShepherdNotificationPlugin;
import com.abaenglish.shepherd.plugin.plugins.ShepherdProfileSelectorPlugin;
import com.abaenglish.videoclass.BuildConfig;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Jesus Espejo using mbp13jesus on 30/06/15.
 */
public class ABAShepherdEditor {

    private static ABAShepherdEditor sharedInstance;

    public static boolean didConfigurationChange = false;

    private List<ShepherdPluginInterface> listOfPlugings;
    private List<ShepherdConfiguration> listOfConfigurations;
    private Context applicationContext;

    public static synchronized ABAShepherdEditor shared(Context context) {
        if (sharedInstance == null) {
            sharedInstance = new ABAShepherdEditor(context);
        }
        return sharedInstance;
    }

    public ABAShepherdEditor(Context context) {
        super();
        applicationContext = context.getApplicationContext();

        // Plugins only available in development
        if (isInternal()) {
            registerPlugins();
        }

        registerConfigurations();
    }

    // ================================================================================
    // Public methods - Configuration
    // ================================================================================

    public ShepherdGenericEnvironment environmentForShepherdConfigurationType(Context context, ShepherdConfiguratorType type) {
        for (ShepherdConfiguration configuration : getListOfConfigurations()) {
            if (configuration.getType().equals(type)) {
                return currentEnvironmentForConfiguration(context, configuration);
            }
        }
        throw new ShepherdConfigurationException("Configuration with type " + type + " not found");
    }

    public void setCurrentEnvironment(Context context, ShepherdGenericEnvironment environment, ShepherdConfiguration configuration) {
        Map<String, String> mapWithCurrentEnvironments = mapWithCurrentEnvironments(context);
        mapWithCurrentEnvironments.put(configuration.getConfigurationName(), environment.getEnvironmentName());
        saveMapWithEnvironments(context, mapWithCurrentEnvironments);
    }

    public void resetCurrentEnvironments(Context context) {
        Map<String, String> mapWithEnvs = mapWithCurrentEnvironments(context);
        for (String key : mapWithEnvs.keySet()) {
            mapWithEnvs.put(key, null);
        }
        saveMapWithEnvironments(context, mapWithEnvs);
    }

    public static boolean isInternal() {
        return BuildConfig.FLAVOR.equals("internal");
    }

    // ================================================================================
    // Private methods - configuration
    // ================================================================================

    private void registerPlugins() {
        registerPlugin(new ShepherdCrashPlugin());
        registerPlugin(new ShepherdAppVersioningPlugin());
        registerPlugin(new ShepherdCleanFilesPlugin());
        registerPlugin(new ShepherdLoginPlugin(applicationContext));
        registerPlugin(new ShepherdExternalLogin());
        registerPlugin(new ShepherdLanguagePlugin(applicationContext));
        registerPlugin(new ShepherdEvaluationPlugin(applicationContext));
        registerPlugin(new ShepherdAutomatorPlugin(applicationContext));
        registerPlugin(new ShepherdNotificationPlugin());
        registerPlugin(ShepherdForceUserTypePlugin.createInstance(applicationContext));
        registerPlugin(ShepherdProfileSelectorPlugin.createInstance(applicationContext));
        registerPlugin(ShepherdABTestUnitDetailPlugin.createInstance(applicationContext));
    }

    private void registerConfigurations() {
        registerConfiguration(new ABACoreConfigurator().createConfiguration(applicationContext));
        registerConfiguration(new ZendeskConfigurator().createConfiguration(applicationContext));
    }

    // ================================================================================
    // Private methods - Registration
    // ================================================================================

    private void registerPlugin(ShepherdPluginInterface sheepPlugin) {
        getListOfPlugings().add(sheepPlugin);
    }

    private void registerConfiguration(ShepherdConfiguration configuration) {
        getListOfConfigurations().add(configuration);
    }

    // ================================================================================
    // Private methods
    // ================================================================================

    private ShepherdGenericEnvironment currentEnvironmentForConfiguration(Context context, ShepherdConfiguration configuration) {

        if (isInternal()) {
            // In development the utility returns the selected one, otherwise the default

            // Looking for selected
            Map<String, String> mapWithEnvs = mapWithCurrentEnvironments(context);
            String environmentName = mapWithEnvs.get(configuration.getConfigurationName());
            if (environmentName != null) {
                for (Object environmentObject : configuration.getListOfEnvironments()) {
                    ShepherdGenericEnvironment environment = (ShepherdGenericEnvironment) environmentObject;
                    if (environment.getEnvironmentName().equalsIgnoreCase(environmentName)) {
                        return environment;
                    }
                }
            }

            // Looking for default
            for (Object environmentObject : configuration.getListOfEnvironments()) {
                ShepherdGenericEnvironment environment = (ShepherdGenericEnvironment) environmentObject;
                if (environment.isDefaultEnvironment()) {
                    return environment;
                }
            }
        } else {

            // In production the utility just picks the default one
            for (Object environmentObject : configuration.getListOfEnvironments()) {
                ShepherdGenericEnvironment environment = (ShepherdGenericEnvironment) environmentObject;
                if (environment.isDefaultEnvironment()) {
                    return environment;
                }
            }
        }

        throw new ShepherdConfigurationException("Environment not found");
    }

    // ================================================================================
    // Private methods - User defaults
    // ================================================================================

    private static String SHEPHERD_PREFERENCES = "SHEPHERD_PREFERENCES";

    private Map<String, String> mapWithCurrentEnvironments(Context context) {
        Map<String, String> mapToReturn = new HashMap<>();
        SharedPreferences keyValues = context.getSharedPreferences(SHEPHERD_PREFERENCES, Context.MODE_PRIVATE);

        // Filling the map from the SharedPreferences
        for (ShepherdConfiguration configuration : getListOfConfigurations()) {
            String envName = keyValues.getString(configuration.getConfigurationName(), null);
            if (envName != null) {
                mapToReturn.put(configuration.getConfigurationName(), envName);
            }
        }
        return mapToReturn;
    }

    private void saveMapWithEnvironments(Context context, Map<String, String> mapToSave) {
        SharedPreferences keyValues = context.getSharedPreferences(SHEPHERD_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = keyValues.edit();

        for (String key : mapToSave.keySet()) {
            editor.putString(key, mapToSave.get(key));
        }
        editor.commit();
    }

    // ================================================================================
    // Accessors
    // ================================================================================

    public List<ShepherdPluginInterface> getListOfPlugings() {
        if (listOfPlugings == null) {
            listOfPlugings = new ArrayList<>();
        }
        return listOfPlugings;
    }

    public List<ShepherdConfiguration> getListOfConfigurations() {
        if (listOfConfigurations == null) {
            listOfConfigurations = new ArrayList<>();
        }
        return listOfConfigurations;
    }

}
