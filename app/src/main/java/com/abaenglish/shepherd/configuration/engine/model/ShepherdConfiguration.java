package com.abaenglish.shepherd.configuration.engine.model;


import com.abaenglish.shepherd.configuration.configurators.GenericShepherdConfigurator.ShepherdConfiguratorType;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Jesus Espejo using mbp13jesus on 15/01/16.
 */
public class ShepherdConfiguration implements Serializable {

    private ShepherdConfiguratorType type;
    private String configurationName;
    private List<ShepherdGenericEnvironment> listOfEnvironments;

    // ================================================================================
    // Accessors
    // ================================================================================

    public String getConfigurationName() {
        return configurationName;
    }

    public void setConfigurationName(String configurationName) {
        this.configurationName = configurationName;
    }

    public List<ShepherdGenericEnvironment> getListOfEnvironments() {
        return listOfEnvironments;
    }

    public void setListOfEnvironments(List<ShepherdGenericEnvironment> listOfEnvironments) {
        this.listOfEnvironments = listOfEnvironments;
    }

    public ShepherdConfiguratorType getType() {
        return type;
    }

    public void setType(ShepherdConfiguratorType type) {
        this.type = type;
    }
}
