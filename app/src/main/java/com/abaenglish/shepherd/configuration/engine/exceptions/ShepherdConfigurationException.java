package com.abaenglish.shepherd.configuration.engine.exceptions;

/**
 * Created by Jesus Espejo using mbp13jesus on 18/01/16.
 */
public class ShepherdConfigurationException extends RuntimeException {

    public ShepherdConfigurationException(String s) {
        super(s);
    }
}
