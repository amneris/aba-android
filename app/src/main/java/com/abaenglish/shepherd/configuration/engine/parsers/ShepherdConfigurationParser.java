package com.abaenglish.shepherd.configuration.engine.parsers;

import android.content.Context;
import android.support.annotation.NonNull;

import com.abaenglish.shepherd.configuration.configurators.GenericShepherdConfigurator;
import com.abaenglish.shepherd.configuration.engine.exceptions.ShepherdConfigurationException;
import com.abaenglish.shepherd.configuration.engine.model.ShepherdConfiguration;
import com.abaenglish.shepherd.configuration.engine.model.ShepherdGenericEnvironment;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jesus Espejo using mbp13jesus on 15/01/16.
 */
public class ShepherdConfigurationParser {

    private static final String SHEPHERD_FOLDER_IN_ASSETS = "shepherd";

    public static ShepherdConfiguration parseConfiguration(Context context, GenericShepherdConfigurator configurator) {

        try {
            // read config to jsonString
            JSONObject configJson = configToJsonObject(context, configurator.getConfigurationFileName());

            // Parsing time
            if (configJson.has("configuration_name") && configJson.has("environments")) {

                // Parsing configuration name and environment lists
                String configurationName = configJson.getString("configuration_name");
                List<ShepherdGenericEnvironment> listOfEnvironments = parseEnvironments(configurator, configJson);

                // Setting parsed fields into model
                ShepherdConfiguration configuration = new ShepherdConfiguration();
                configuration.setConfigurationName(configurationName);
                configuration.setListOfEnvironments(listOfEnvironments);
                configuration.setType(configurator.getType());

                return configuration;

            } else {
                throw new ShepherdConfigurationException("Json file has not properly defined a Shepherd configuration");
            }

        } catch (IOException | JSONException | ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
        }

        return null;
    }

    // ================================================================================
    // Environments parsing
    // ================================================================================

    /**
     * It converts each config section to a model and adds it to list
     */
    private static List<ShepherdGenericEnvironment> parseEnvironments(GenericShepherdConfigurator configurator, JSONObject configJson) throws JSONException, InstantiationException, IllegalAccessException, ClassNotFoundException {

        List<ShepherdGenericEnvironment> parsedEnvironments = new ArrayList<>();
        Boolean defaultConfigurationExist = false;

        JSONArray jsonArrayOfEnvironments = configJson.getJSONArray("environments");
        for (int envIndex = 0; envIndex < jsonArrayOfEnvironments.length(); envIndex++) {
            // Reading raw environment
            JSONObject envAtIndex = jsonArrayOfEnvironments.getJSONObject(envIndex);
            ShepherdGenericEnvironment environment = configurator.getEnvironmentParser().parseEnvironment(envAtIndex);


            // Adding environment to the list
            parsedEnvironments.add(environment);

            // check we only have one default environment
            if (environment.isDefaultEnvironment() && defaultConfigurationExist) {
                throw new ShepherdConfigurationException("SHEPHERD: There are two default configurations");
            }

            defaultConfigurationExist = defaultConfigurationExist || environment.isDefaultEnvironment();
        }

        // check that we have a default environment
        if (!defaultConfigurationExist) {
            throw new ShepherdConfigurationException("SHEPHERD: There is not a default configuration");
        }
        return parsedEnvironments;
    }

    // ================================================================================
    // Json methods
    // ================================================================================

    @NonNull
    private static JSONObject configToJsonObject(Context context, String configFileName) throws IOException, JSONException {
        String jsonConfig = getConfigAsJson(context, configFileName);
        return new JSONObject(jsonConfig);
    }

    private static String getConfigAsJson(Context context, String configFileName) throws IOException {
        InputStream stream = context.getAssets().open(SHEPHERD_FOLDER_IN_ASSETS + "/" + configFileName);
        StringWriter writer = new StringWriter();
        IOUtils.copy(stream, writer, "UTF-8");
        return writer.toString();
    }
}
