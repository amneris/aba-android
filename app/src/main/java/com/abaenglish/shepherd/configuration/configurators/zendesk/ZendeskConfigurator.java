package com.abaenglish.shepherd.configuration.configurators.zendesk;

import com.abaenglish.shepherd.configuration.configurators.GenericShepherdConfigurator;
import com.abaenglish.shepherd.configuration.engine.parsers.ShepherdEnvironmentParser;

import static com.abaenglish.shepherd.configuration.configurators.GenericShepherdConfigurator.ShepherdConfiguratorType.kShepherdConfiguratorTypeZendesk;

/**
 * Created by Jesus Espejo using mbp13jesus on 18/01/16.
 */
public class ZendeskConfigurator extends GenericShepherdConfigurator {

    private static final String ZENDESK_CONFIGURATION_ASSETS = "zendesk_configuration.json";

    @Override
    public String getConfigurationFileName() {
        return ZENDESK_CONFIGURATION_ASSETS;
    }

    @Override
    public ShepherdEnvironmentParser getEnvironmentParser() {
        return new ZendeskEnvironmentParser();
    }

    @Override
    public ShepherdConfiguratorType getType() {
        return kShepherdConfiguratorTypeZendesk;
    }
}
