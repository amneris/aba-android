package com.abaenglish.shepherd.configuration.configurators;

import android.content.Context;

import com.abaenglish.shepherd.configuration.engine.model.ShepherdConfiguration;
import com.abaenglish.shepherd.configuration.engine.parsers.ShepherdConfigurationParser;
import com.abaenglish.shepherd.configuration.engine.parsers.ShepherdEnvironmentParser;

/**
 * Created by Jesus Espejo using mbp13jesus on 18/01/16.
 */
public abstract class GenericShepherdConfigurator {

    public enum ShepherdConfiguratorType {

        kShepherdConfiguratorTypeABACore(1),
        kShepherdConfiguratorTypeZendesk(2),
        kShepherdConfiguratorTypeCooladata(3);

        private final int value;

        ShepherdConfiguratorType(final int newValue) {
            value = newValue;
        }

        public int getValue() {
            return value;
        }
    }

    public ShepherdConfiguration createConfiguration(Context aContext) {
        return ShepherdConfigurationParser.parseConfiguration(aContext.getApplicationContext(), this);
    }

    /**
     * Method to retrieve the configuration file name
     *
     * @return
     */
    public abstract String getConfigurationFileName();

    /**
     * @return
     */
    public abstract ShepherdEnvironmentParser getEnvironmentParser();

    /**
     * @return
     */
    public abstract ShepherdConfiguratorType getType();

}
