package com.abaenglish.shepherd.configuration.configurators.abacore;

import com.abaenglish.shepherd.configuration.configurators.GenericShepherdConfigurator;
import com.abaenglish.shepherd.configuration.engine.parsers.ShepherdEnvironmentParser;

import static com.abaenglish.shepherd.configuration.configurators.GenericShepherdConfigurator.ShepherdConfiguratorType.kShepherdConfiguratorTypeABACore;

/**
 * Created by Jesus Espejo using mbp13jesus on 18/01/16.
 */
public class ABACoreConfigurator extends GenericShepherdConfigurator {

    private static final String ABA_CORE_CONFIGURATION_ASSETS = "abacore_configuration.json";

    @Override
    public String getConfigurationFileName() {
        return ABA_CORE_CONFIGURATION_ASSETS;
    }

    @Override
    public ShepherdEnvironmentParser getEnvironmentParser() {
        return new ABACoreEnvironmentParser();
    }

    @Override
    public ShepherdConfiguratorType getType() {
        return kShepherdConfiguratorTypeABACore;
    }
}
