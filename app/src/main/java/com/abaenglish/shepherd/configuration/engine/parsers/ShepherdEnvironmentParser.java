package com.abaenglish.shepherd.configuration.engine.parsers;

import com.abaenglish.shepherd.configuration.engine.exceptions.ShepherdConfigurationException;
import com.abaenglish.shepherd.configuration.engine.model.ShepherdGenericEnvironment;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Jesus Espejo using mbp13jesus on 18/01/16.
 */
public abstract class ShepherdEnvironmentParser {

    // ================================================================================
    // Public methods
    // ================================================================================

    public ShepherdGenericEnvironment parseEnvironment(JSONObject environmentJsonObject) throws JSONException {

        ShepherdGenericEnvironment environment = createEnvironmentObject();

        // Parsing common attributes
        if (environmentJsonObject.has("environment_name") || environmentJsonObject.has("is_default")) {
            environment.setEnvironmentName(environmentJsonObject.getString("environment_name"));
            environment.setIsDefaultEnvironment(environmentJsonObject.getBoolean("is_default"));
        }
        else {
            throw new ShepherdConfigurationException("Unable to parse common attributes for environment");
        }

        // Parsing specific properties
        if (environmentJsonObject.has("properties")) {
            parseProperties(environmentJsonObject.getJSONObject("properties"), environment);
        }

        return environment;
    }

    // ================================================================================
    // Abstract methods (to implement)
    // ================================================================================

    protected abstract void parseProperties(JSONObject envPropertiesJsonObject, ShepherdGenericEnvironment environment) throws JSONException;

    protected abstract ShepherdGenericEnvironment createEnvironmentObject();
}
