package com.abaenglish.shepherd.configuration.configurators.abacore;

import com.abaenglish.shepherd.configuration.engine.exceptions.ShepherdConfigurationException;
import com.abaenglish.shepherd.configuration.engine.model.ShepherdGenericEnvironment;
import com.abaenglish.shepherd.configuration.engine.parsers.ShepherdEnvironmentParser;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Jesus Espejo using mbp13jesus on 18/01/16.
 */
public class ABACoreEnvironmentParser extends ShepherdEnvironmentParser {

    // ================================================================================
    // Refined methods
    // ================================================================================

    @Override
    protected void parseProperties(JSONObject envPropertiesJsonObject, ShepherdGenericEnvironment environment) throws JSONException {
        if (environment.getClass().equals(ABACoreShepherdEnvironment.class)) {
            ABACoreShepherdEnvironment abaCoreEnvironment = (ABACoreShepherdEnvironment) environment;
            abaCoreEnvironment.setAbaSecureApiUrl(envPropertiesJsonObject.getString("ABA_SECURE_API_URL"));
            abaCoreEnvironment.setAbaAudioResourcesUrl(envPropertiesJsonObject.getString("ABA_AUDIO_RESOURCES_URL"));
            abaCoreEnvironment.setAbaImageResourcesUrl(envPropertiesJsonObject.getString("ABA_IMAGE_RESOURCES_URL"));
            abaCoreEnvironment.setAbaGatewayUrl(envPropertiesJsonObject.getString("ABA_GATEWAY"));
            abaCoreEnvironment.setClientId(envPropertiesJsonObject.getString("OAUTH_CLIENT_ID"));
            abaCoreEnvironment.setClientSecret(envPropertiesJsonObject.getString("OAUTH_CLIENT_SECRET"));
        } else {
            throw new ShepherdConfigurationException("This class can only parse ABACoreShepherdEnvironment.class a definitely not " + environment.getClass().getSimpleName());
        }
    }

    @Override
    protected ShepherdGenericEnvironment createEnvironmentObject() {
        ABACoreShepherdEnvironment environment = new ABACoreShepherdEnvironment();
        return environment;
    }
}
