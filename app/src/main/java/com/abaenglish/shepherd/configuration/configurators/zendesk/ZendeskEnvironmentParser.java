package com.abaenglish.shepherd.configuration.configurators.zendesk;

import com.abaenglish.shepherd.configuration.engine.exceptions.ShepherdConfigurationException;
import com.abaenglish.shepherd.configuration.engine.model.ShepherdGenericEnvironment;
import com.abaenglish.shepherd.configuration.engine.parsers.ShepherdEnvironmentParser;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Jesus Espejo using mbp13jesus on 18/01/16.
 * Modified by xilosada on 8/4/2016
 */
public class ZendeskEnvironmentParser extends ShepherdEnvironmentParser {

    @Override
    protected void parseProperties(JSONObject envPropertiesJsonObject, ShepherdGenericEnvironment environment) throws JSONException {

        if (environment.getClass().equals(ZendeskShepherdEnvironment.class)) {
            ZendeskShepherdEnvironment zendeskEnvironment = (ZendeskShepherdEnvironment) environment;
            zendeskEnvironment.setBaseUrl(envPropertiesJsonObject.getString("HELPDESK_BASE_URL"));
            zendeskEnvironment.setApplicationId(envPropertiesJsonObject.getString("HELPDESK_API_KEY"));
            zendeskEnvironment.setOauthClientId(envPropertiesJsonObject.getString("HELPDESK_SDK_KEY"));
        } else {
            throw new ShepherdConfigurationException("This class can only parse ZendeskShepherdEnvironment.class a definitely not " + environment.getClass().getSimpleName());
        }

    }

    @Override
    protected ShepherdGenericEnvironment createEnvironmentObject() {
        ZendeskShepherdEnvironment environment = new ZendeskShepherdEnvironment();
        return environment;
    }
}