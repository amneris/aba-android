package com.abaenglish.shepherd.configuration.engine.model;

import java.io.Serializable;

/**
 * Created by Jesus Espejo using mbp13jesus on 30/06/15.
 */
public class ShepherdGenericEnvironment implements Serializable {

    private String environmentName;
    private boolean isDefaultEnvironment;

    // ================================================================================
    // Overriding methods
    // ================================================================================

    @Override
    public boolean equals(Object o) {
        if (o instanceof ShepherdGenericEnvironment) {
            ShepherdGenericEnvironment anotherObject = (ShepherdGenericEnvironment) o;
            return anotherObject.getEnvironmentName().equalsIgnoreCase(getEnvironmentName());
        }
        return false;
    }

    // ================================================================================
    // Accessors
    // ================================================================================

    public boolean isDefaultEnvironment() {
        return isDefaultEnvironment;
    }

    public void setIsDefaultEnvironment(boolean isDefaultEnvironment) {
        this.isDefaultEnvironment = isDefaultEnvironment;
    }

    public String getEnvironmentName() {
        return environmentName;
    }

    public void setEnvironmentName(String environmentName) {
        this.environmentName = environmentName;
    }
}
