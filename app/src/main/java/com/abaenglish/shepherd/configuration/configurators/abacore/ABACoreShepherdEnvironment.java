package com.abaenglish.shepherd.configuration.configurators.abacore;

import com.abaenglish.shepherd.configuration.engine.model.ShepherdGenericEnvironment;

import java.io.Serializable;

/**
 * Created by Jesus Espejo using mbp13jesus on 30/06/15.
 */
public class ABACoreShepherdEnvironment extends ShepherdGenericEnvironment implements Serializable {

    private String abaSecureApiUrl;
    private String abaAudioResourcesUrl;
    private String abaImageResourcesUrl;
    private String abaGatewayUrl;
    private String clientSecret;
    private String clientId;

    // ================================================================================
    // Overriding
    // ================================================================================

    @Override
    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("\n abaSecureApiUrl: ").append(getAbaSecureApiUrl());
        stringBuffer.append("\n gateway url url: ").append(getAbaGatewayUrl());
        stringBuffer.append("\n name: ").append(getEnvironmentName());
        stringBuffer.append("\n isDefault: ").append(isDefaultEnvironment() ? "YES" : "NO");
        stringBuffer.append("\n image url: ").append(getAbaImageResourcesUrl());
        stringBuffer.append("\n audio url: ").append(getAbaAudioResourcesUrl());
        return stringBuffer.toString();
    }

    // ================================================================================
    // Accessors
    // ================================================================================

    public String getAbaSecureApiUrl() {
        return abaSecureApiUrl;
    }

    void setAbaSecureApiUrl(String abaSecureApiUrl) {
        this.abaSecureApiUrl = abaSecureApiUrl;
    }

    public String getAbaAudioResourcesUrl() {
        return abaAudioResourcesUrl;
    }

    void setAbaAudioResourcesUrl(String abaAudioResourcesUrl) {
        this.abaAudioResourcesUrl = abaAudioResourcesUrl;
    }

    public String getAbaImageResourcesUrl() {
        return abaImageResourcesUrl;
    }

    void setAbaImageResourcesUrl(String abaImageResourcesUrl) {
        this.abaImageResourcesUrl = abaImageResourcesUrl;
    }

    public String getAbaGatewayUrl() {
        return abaGatewayUrl;
    }

    void setAbaGatewayUrl(String abaGatewayUrl) {
        this.abaGatewayUrl = abaGatewayUrl;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }
}
