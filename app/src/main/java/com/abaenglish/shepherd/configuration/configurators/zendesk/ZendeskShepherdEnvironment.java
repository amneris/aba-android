package com.abaenglish.shepherd.configuration.configurators.zendesk;

import com.abaenglish.shepherd.configuration.engine.model.ShepherdGenericEnvironment;

import java.io.Serializable;

/**
 * Created by Jesus Espejo using mbp13jesus on 11/01/16.
 * Refactorized by xilosada on 8/4/2016
 */
public class ZendeskShepherdEnvironment extends ShepherdGenericEnvironment implements Serializable {

    private String baseUrl;
    private String applicationId;
    private String oauthClientId;

    // ================================================================================
    // Overriding
    // ================================================================================

    /**
     * serialises class to string
     *
     * @return
     */
    @Override
    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("baseUrl: ").append(getBaseUrl());
        stringBuffer.append("\n applicationId: ").append(getApplicationId());
        stringBuffer.append("\n oauthClientId: ").append(getOauthClientId());
        stringBuffer.append("\n name: ").append(getEnvironmentName());
        stringBuffer.append("\n isDefault: ").append(isDefaultEnvironment() ? "YES" : "NO");
        return stringBuffer.toString();
    }

    // ================================================================================
    // Accessors
    // ================================================================================

    public String getBaseUrl() {
        return baseUrl;
    }

    public String getApplicationId() {
        return applicationId;
    }

    public String getOauthClientId() {
        return oauthClientId;
    }

    public void setApplicationId(String applicationId) {
        this.applicationId = applicationId;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public void setOauthClientId(String oauthClientId) {
        this.oauthClientId = oauthClientId;
    }
}