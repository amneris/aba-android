package com.abaenglish.shepherd.plugin.plugins;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.DataSetObserver;
import android.os.Handler;
import android.os.Looper;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.abaenglish.shepherd.plugin.ShepherdPluginInterface;

/**
 * Created by gnatok on 09/11/2016.
 */

public class ShepherdProfileSelectorPlugin implements ShepherdPluginInterface {

    public static final String PROFILE_TYPE_SHARED_PREFERENCES = "PROFILE_TYPE_SHARED_PREFERENCES";
    public static final String PROFILE_TYPE_KEY = "PROFILE_TYPE_KEY";

    private static final String PROFILE_TYPE_OLD = "Old Profile";
    private static final String PROFILE_TYPE_NEW = "New Profile";
    private static final String PROFILE_TYPE_NONE = "None";

    private Context mContext;
    private static ShepherdProfileSelectorPlugin _plugin = null;
    boolean warningShown = false;

    public static ShepherdProfileSelectorPlugin getInstance() {
        return _plugin;
    }

    public static ShepherdProfileSelectorPlugin createInstance(Context context) {
        if (_plugin == null) {
            _plugin = new ShepherdProfileSelectorPlugin(context);
        }
        return _plugin;
    }

    public ShepherdProfileSelectorPlugin(Context context) {
        super();
        this.mContext = context;
    }


    @Override
    public View viewForPlugin(Context context) {
        TextView textViewForPlugin = new TextView(context);
        textViewForPlugin.setText("Profile Type: " + stringForProfileType(currentSelectedProfileType(mContext)) + ". Click to change");
        textViewForPlugin.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
        return textViewForPlugin;
    }

    @Override
    public void execute(Context context) {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(context, AlertDialog.THEME_DEVICE_DEFAULT_DARK);
        builderSingle.setTitle("Select Profile type");
        builderSingle.setNegativeButton("Cancel", new ShepherdProfileSelectorPlugin.NegativeOnClickListener());
        builderSingle.setAdapter(new ShepherdProfileSelectorPlugin.ProfileTypeListAdapter(), new ShepherdProfileSelectorPlugin.ProfileTypeSelectionOnClickListener());
        builderSingle.show();
    }

    // ================================================================================
    // Public methods
    // ================================================================================

    public static boolean shouldForceOld(Context context) {
        int selection = currentSelectedProfileType(context);
        return selection == 0;
    }

    public static boolean shouldForceNew(Context context) {
        int selection = currentSelectedProfileType(context);
        return selection == 1;
    }

    public void showWarningMessage() {
        String textToShow = null;

        // Determining text to show
        if (shouldForceOld(mContext)) {
            textToShow = "Forcing old profile";
        }
        if (shouldForceNew(mContext)) {
            textToShow = "Forcing new profile";
        }

        if (textToShow != null && !warningShown) {
            Toast.makeText(mContext, textToShow, Toast.LENGTH_LONG).show();
            warningShown = true;

            // Showing warning once every 5 seconds
            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    warningShown = false;
                }
            }, 5000);
        }
    }

    // ================================================================================
    // Private methods
    // ================================================================================

    private String stringForProfileType(int type) {
        switch (type) {
            case 0:
                return PROFILE_TYPE_OLD;
            case 1:
                return PROFILE_TYPE_NEW;
            case 2:
                return PROFILE_TYPE_NONE;
        }

        return "Unknown";
    }

    private static int currentSelectedProfileType(Context context) {
        SharedPreferences settings = context.getSharedPreferences(PROFILE_TYPE_SHARED_PREFERENCES, Context.MODE_PRIVATE);
        return settings.getInt(PROFILE_TYPE_KEY, 2); // Default is None
    }


    // ================================================================================
    // Private classes
    // ================================================================================

    /**
     * Class to handle cancel button action
     */
    private class NegativeOnClickListener implements DialogInterface.OnClickListener {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
        }
    }

    /**
     * Class to handle user type selection
     */
    private class ProfileTypeListAdapter implements ListAdapter {


        @Override
        public boolean areAllItemsEnabled() {
            return true;
        }

        @Override
        public boolean isEnabled(int position) {
            return true;
        }

        @Override
        public void registerDataSetObserver(DataSetObserver observer) {

        }

        @Override
        public void unregisterDataSetObserver(DataSetObserver observer) {

        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            TextView userTypeTextView = new TextView(mContext);
            userTypeTextView.setPadding(50, 50, 50, 50);
            userTypeTextView.setText(stringForProfileType(position));
            return userTypeTextView;
        }

        @Override
        public int getItemViewType(int position) {
            return 0;
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }

        @Override
        public boolean isEmpty() {
            return false;
        }
    }

    /**
     * Class to handle user type selection
     */
    private class ProfileTypeSelectionOnClickListener implements DialogInterface.OnClickListener {

        @Override
        public void onClick(DialogInterface dialog, int which) {
            SharedPreferences settings = mContext.getSharedPreferences(PROFILE_TYPE_SHARED_PREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = settings.edit();
            editor.putInt(PROFILE_TYPE_KEY, which);
            editor.apply();

            Toast.makeText(mContext, "Forcing Profile type to: " + stringForProfileType(which), Toast.LENGTH_SHORT).show();
        }
    }
}

