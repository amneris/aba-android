package com.abaenglish.shepherd.plugin;

import android.content.Context;
import android.view.View;

/**
 * Created by Jesus Espejo using mbp13jesus on 30/06/15.
 */
public interface ShepherdPluginInterface {

    View viewForPlugin(Context context);
    void execute(Context context);
}
