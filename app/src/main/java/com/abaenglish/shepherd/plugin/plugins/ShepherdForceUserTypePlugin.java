package com.abaenglish.shepherd.plugin.plugins;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.DataSetObserver;
import android.os.Handler;
import android.os.Looper;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.abaenglish.shepherd.plugin.ShepherdPluginInterface;

/**
 * Created by Jesus Espejo using mbp13jesus on 01/10/15.
 * Enhanced by Jesus Espejo on 04/08/2016. Dark theme.
 */
public class ShepherdForceUserTypePlugin implements ShepherdPluginInterface {

    public static final String USER_TYPE_SHARED_PREFERENCES = "USER_TYPE_SHARED_PREFERENCES";
    public static final String USER_TYPE_KEY = "USER_TYPE_KEY";

    private static final String USER_FREE = "FREE";
    private static final String USER_PREMIUM = "PREMIUM";
    private static final String USER_NONE = "NONE";

    private Context mContext;
    private static ShepherdForceUserTypePlugin _plugin = null;
    boolean warningShown = false;

    public static ShepherdForceUserTypePlugin getInstance() {
        return _plugin;
    }

    public static ShepherdForceUserTypePlugin createInstance(Context context) {
        if (_plugin == null) {
            _plugin = new ShepherdForceUserTypePlugin(context);
        }
        return _plugin;
    }

    public ShepherdForceUserTypePlugin(Context context) {
        super();
        this.mContext = context;
    }

    @Override
    public View viewForPlugin(Context context) {
        TextView textViewForPlugin = new TextView(context);
        textViewForPlugin.setText("Forcing user type: " + stringForUserType(currentSelectedUserType(mContext)) + ". Click to change");
        textViewForPlugin.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
        return textViewForPlugin;
    }

    @Override
    public void execute(Context context) {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(context, AlertDialog.THEME_DEVICE_DEFAULT_DARK);
        builderSingle.setTitle("Select user type to force");
        builderSingle.setNegativeButton("Cancel", new NegativeOnClickListener());
        builderSingle.setAdapter(new UserTypeListAdapter(), new UserTypeSelectionOnClickListener());
        builderSingle.show();
    }


    // ================================================================================
    // Public methods
    // ================================================================================

    public boolean shouldForceFree() {
        int selection = currentSelectedUserType(mContext);
        return selection == 0;
    }

    public boolean shouldForcePremium() {
        int selection = currentSelectedUserType(mContext);
        return selection == 1;
    }

    public void showWarningMessage() {

        String textToShow = null;

        // Determining text to show
        if (shouldForceFree()) {
            textToShow = "Forcing free user";
        }
        if (shouldForcePremium()) {
            textToShow = "Forcing premium user";
        }

        // Showing text (once every 5 secs)x
        if (textToShow != null && !warningShown) {
            Toast.makeText(mContext, textToShow, Toast.LENGTH_LONG).show();
            warningShown = true;

            // Showing warning once every 5 seconds
            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    warningShown = false;
                }
            }, 5000);
        }
    }

    // ================================================================================
    // Private methods
    // ================================================================================

    private String stringForUserType(int type) {
        switch (type) {
            case 0:
                return USER_FREE;
            case 1:
                return USER_PREMIUM;
            case 2:
                return USER_NONE;
        }

        return "Unknown";
    }

    private static int currentSelectedUserType(Context context) {
        SharedPreferences settings = context.getSharedPreferences(USER_TYPE_SHARED_PREFERENCES, Context.MODE_PRIVATE);
        return settings.getInt(USER_TYPE_KEY, 2); // Default is NONE
    }

    // ================================================================================
    // Private classes
    // ================================================================================

    /**
     * Class to handle cancel button action
     */
    private class NegativeOnClickListener implements DialogInterface.OnClickListener {

        @Override
        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
        }
    }

    /**
     * Class to handle user type selection
     */
    private class UserTypeListAdapter implements ListAdapter {


        @Override
        public boolean areAllItemsEnabled() {
            return true;
        }

        @Override
        public boolean isEnabled(int position) {
            return true;
        }

        @Override
        public void registerDataSetObserver(DataSetObserver observer) {

        }

        @Override
        public void unregisterDataSetObserver(DataSetObserver observer) {

        }

        @Override
        public int getCount() {
            return 3;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            TextView userTypeTextView = new TextView(mContext);
            userTypeTextView.setPadding(50, 50, 50, 50);
            userTypeTextView.setText(stringForUserType(position));
            return userTypeTextView;
        }

        @Override
        public int getItemViewType(int position) {
            return 0;
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }

        @Override
        public boolean isEmpty() {
            return false;
        }
    }

    /**
     * Class to handle user type selection
     */
    private class UserTypeSelectionOnClickListener implements DialogInterface.OnClickListener {

        @Override
        public void onClick(DialogInterface dialog, int which) {
            SharedPreferences settings = mContext.getSharedPreferences(USER_TYPE_SHARED_PREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = settings.edit();
            editor.putInt(USER_TYPE_KEY, which);
            editor.apply();

            Toast.makeText(mContext, "Forcing user type to: " + stringForUserType(which), Toast.LENGTH_SHORT).show();
        }
    }
}
