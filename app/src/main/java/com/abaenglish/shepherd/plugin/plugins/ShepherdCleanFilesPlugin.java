package com.abaenglish.shepherd.plugin.plugins;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;

import com.abaenglish.shepherd.plugin.ShepherdPluginInterface;

/**
 * Created by Jesus Espejo using mbp13jesus on 01/07/15.
 */
public class ShepherdCleanFilesPlugin implements ShepherdPluginInterface {

    @Override
    public View viewForPlugin(Context context) {
        TextView textViewForPlugin = new TextView(context);
        textViewForPlugin.setText("Clean local files");
        textViewForPlugin.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
        return textViewForPlugin;
    }

    @Override
    public void execute(final Context context) {
        cleanCache(context);
    }

    // ================================================================================
    // Private methods
    // ================================================================================

    private void cleanCache(Context context) {
        Intent intent = new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setData(Uri.parse("package:" + "com.abaenglish.videoclass"));

        context.startActivity(intent);
    }
}
