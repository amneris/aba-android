package com.abaenglish.shepherd.plugin.plugins;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;

import com.abaenglish.shepherd.plugin.ShepherdPluginInterface;

/**
 * Created by Jesus Espejo using mbp13jesus on 02/09/15.
 * Enhanced by Jesus Espejo on 04/08/2016. Dark theme.
 */
public class ShepherdEvaluationPlugin implements ShepherdPluginInterface {

    private static final String ABA_UNLOCK_EVALUATION_SHARED_PREFERENCES = "ABA_UNLOCK_EVALUATION_SHARED_PREFERENCES";
    private static final String ABA_UNLOCK_EVALUATION_KEY = "ABA_UNLOCK_EVALUATION_KEY";
    private Context mContext;

    public ShepherdEvaluationPlugin(Context aContext) {
        super();
        this.mContext = aContext;
    }

    @Override
    public View viewForPlugin(Context context) {
        TextView textViewForPlugin = new TextView(context);
        if (isUnlockActive(mContext)) {
            textViewForPlugin.setText("Unlock evaluation? - YES");
        } else {
            textViewForPlugin.setText("Unlock evaluation? - NO");
        }
        textViewForPlugin.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
        return textViewForPlugin;
    }

    @Override
    public void execute(Context context) {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(context, AlertDialog.THEME_DEVICE_DEFAULT_DARK);
        builderSingle.setTitle("Unlock evaluation?");
        builderSingle.setNegativeButton("Reset", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                setUnlockActive(false);
                dialog.dismiss();
            }
        });
        builderSingle.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                setUnlockActive(true);
                dialog.dismiss();
            }
        });
        builderSingle.show();
    }

    // ================================================================================
    // Private methods
    // ================================================================================

    public static boolean isUnlockActive(Context aContext) {
        SharedPreferences settings = aContext.getSharedPreferences(ABA_UNLOCK_EVALUATION_SHARED_PREFERENCES, Context.MODE_PRIVATE);
        return settings.getBoolean(ABA_UNLOCK_EVALUATION_KEY, false);
    }

    private void setUnlockActive(boolean active) {
        SharedPreferences settings = mContext.getSharedPreferences(ABA_UNLOCK_EVALUATION_SHARED_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(ABA_UNLOCK_EVALUATION_KEY, active);
        editor.commit();
    }
}
