package com.abaenglish.shepherd.plugin.plugins;

import android.app.NotificationManager;
import android.content.Context;
import android.support.v7.app.NotificationCompat;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;

import com.abaenglish.shepherd.plugin.ShepherdPluginInterface;
import com.abaenglish.videoclass.R;

/**
 * Created by Jesus Espejo using mbp13jesus on 04/08/16.
 */

public class ShepherdNotificationPlugin implements ShepherdPluginInterface {
    @Override
    public View viewForPlugin(Context context) {
        TextView textViewForPlugin = new TextView(context);
        textViewForPlugin.setText("Show notification");
        textViewForPlugin.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
        return textViewForPlugin;
    }

    @Override
    public void execute(Context context) {
        NotificationCompat.Builder builder =
                (NotificationCompat.Builder) new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.ic_logo_white_24dp)
                        .setContentTitle("Notifications Example")
                        .setContentText("This is a test notification");

        // Add as notification
        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(234234234, builder.build());
    }
}
