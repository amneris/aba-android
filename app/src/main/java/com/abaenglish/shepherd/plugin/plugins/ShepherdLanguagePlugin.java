package com.abaenglish.shepherd.plugin.plugins;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.DataSetObserver;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.abaenglish.shepherd.plugin.ShepherdPluginInterface;
import com.abaenglish.videoclass.domain.LanguageController;

/**
 * Created by Jesus Espejo using mbp13jesus on 17/08/15.
 * Enhanced by Jesus Espejo on 04/08/2016. Dark theme.
 */
public class ShepherdLanguagePlugin implements ShepherdPluginInterface {

    private Context mContext;

    public ShepherdLanguagePlugin(Context context) {
        super();
        this.mContext = context;
    }

    @Override
    public View viewForPlugin(Context context) {
        TextView textViewForPlugin = new TextView(context);
        textViewForPlugin.setText("Language selection");
        textViewForPlugin.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
        return textViewForPlugin;
    }

    @Override
    public void execute(Context context) {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(context, AlertDialog.THEME_DEVICE_DEFAULT_DARK);
        builderSingle.setTitle("Language selection");
        builderSingle.setNegativeButton("Cancel", new NegativeOnClickListener());
        builderSingle.setAdapter(new LanguageListAdapter(), new LanguageSelectionOnClickListener());
        builderSingle.show();
    }

    // ================================================================================
    // Private classes
    // ================================================================================

    /**
     * Class to handle cancel button action
     */
    private class NegativeOnClickListener implements DialogInterface.OnClickListener {

        @Override
        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
        }
    }

    /**
     * Class to handle visual aspect of the selector dialog
     */
    private class LanguageListAdapter implements ListAdapter {

        @Override
        public boolean areAllItemsEnabled() {
            return true;
        }

        @Override
        public boolean isEnabled(int position) {
            return true;
        }

        @Override
        public void registerDataSetObserver(DataSetObserver observer) {

        }

        @Override
        public void unregisterDataSetObserver(DataSetObserver observer) {

        }

        @Override
        public int getCount() {
            return LanguageController.SUPPORTED_LANGUAGE_ARRAY.length;
        }

        @Override
        public Object getItem(int position) {
            return LanguageController.SUPPORTED_LANGUAGE_ARRAY[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            String language = LanguageController.SUPPORTED_LANGUAGE_ARRAY[position];

            // Creating language text view
            TextView languageTextView = new TextView(mContext);
            languageTextView.setText(language);

            // Creating main layout
            LinearLayout verticalLayout = new LinearLayout(mContext);
            verticalLayout.setOrientation(LinearLayout.VERTICAL);
            verticalLayout.addView(languageTextView);
            verticalLayout.setPadding(25, 25, 25, 25);

            return verticalLayout;
        }

        @Override
        public int getItemViewType(int position) {
            return 1;
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }

        @Override
        public boolean isEmpty() {
            return LanguageController.SUPPORTED_LANGUAGE_ARRAY.length == 0;
        }
    }

    /**
     * Class to handle language selection action
     */
    private class LanguageSelectionOnClickListener implements DialogInterface.OnClickListener {

        @Override
        public void onClick(DialogInterface dialog, int which) {

            String selectedLanguage = LanguageController.SUPPORTED_LANGUAGE_ARRAY[which];

            LanguageController.setCurrentLanguage(mContext, selectedLanguage);
            Toast.makeText(mContext, "The language '" + selectedLanguage + "' has been selected. Restart the app.", Toast.LENGTH_SHORT).show();
            ;
        }
    }
}
