package com.abaenglish.shepherd.plugin.plugins;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.DataSetObserver;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.abaenglish.shepherd.plugin.ShepherdPluginInterface;
import com.abaenglish.videoclass.R;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jesus Espejo using mbp13jesus on 04/08/15.
 * Enhanced by Jesus Espejo on 04/08/2016. Dark theme.
 */
public class ShepherdLoginPlugin implements ShepherdPluginInterface {

    // User default keys
    public static final String LAST_LOGIN_EMAIL_KEY = "LAST_EMAIL_KEY";
    public static final String LAST_LOGIN_PASS_KEY = "LAST_PASS_KEY";
    public static final String LAST_LOGIN_SHARED_PREFERENCES = "LAST_LOGIN_SHARED_PREFERENCES";

    // Assets file and subfolder
    private static final String SHEPHERD_FOLDER_IN_ASSETS = "shepherd";
    private static final String HARDCODED_LOGIN_FILE = "login_plugin.json";

    // External file name
    private static final String CREDENTIALS_FILE_NAME = "credentials.data";


    private Context mContext;
    private List<LoginModel> listOfHardcodedLoginCredentials;
    private List<LoginModel> listOfPreviouslyEnteredLoginCredentials;

    public ShepherdLoginPlugin(Context context) {
        super();
        this.mContext = context;
        listOfHardcodedLoginCredentials = readLoginInformationFromAssets(mContext);
    }

    @Override
    public View viewForPlugin(Context context) {
        TextView textViewForPlugin = new TextView(context);
        textViewForPlugin.setText("Login selection");
        textViewForPlugin.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
        return textViewForPlugin;
    }

    @Override
    public void execute(final Context context) {

        // Reloading entered credentials
        listOfPreviouslyEnteredLoginCredentials = readEnteredLoginOptions();

        AlertDialog.Builder builderSingle = new AlertDialog.Builder(context, AlertDialog.THEME_DEVICE_DEFAULT_DARK);
        builderSingle.setTitle("Select Login credentials");
        builderSingle.setNegativeButton("Cancel", new NegativeOnClickListener());
        builderSingle.setAdapter(new LoginListAdapter(), new LoginOnClickListener());
        builderSingle.show();
    }

    // ================================================================================
    // Public methods
    // ================================================================================

    public static void addLoginOption(Context aContext, String user, String password, String additionalInfo) {
        LoginModel newLoginOption = new LoginModel();
        newLoginOption.setLogin(user);
        newLoginOption.setPassword(password);
        newLoginOption.setMoreInfo(additionalInfo);

        addLoginCredentials(aContext, newLoginOption);
    }

    // ================================================================================
    // Private classes
    // ================================================================================

    /**
     * Class to handle cancel button action
     */
    private class NegativeOnClickListener implements DialogInterface.OnClickListener {

        @Override
        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
        }
    }

    /**
     * Class to handle login selection action
     */
    private class LoginOnClickListener implements DialogInterface.OnClickListener {

        @Override
        public void onClick(DialogInterface dialog, int which) {
            LoginModel selectedLogin;
            if (which < listOfHardcodedLoginCredentials.size()) {
                selectedLogin = listOfHardcodedLoginCredentials.get(which);
            } else {
                selectedLogin = listOfPreviouslyEnteredLoginCredentials.get(which - listOfHardcodedLoginCredentials.size());
            }

            SharedPreferences settings = mContext.getSharedPreferences(LAST_LOGIN_SHARED_PREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = settings.edit();
            editor.putString(LAST_LOGIN_EMAIL_KEY, selectedLogin.getLogin());
            editor.putString(LAST_LOGIN_PASS_KEY, selectedLogin.getPassword());
            editor.apply();

            Toast.makeText(mContext, "The credentials have been set", Toast.LENGTH_SHORT).show();;
        }
    }

    /**
     * Class to handle visual aspect of the selector dialog
     */
    private class LoginListAdapter implements ListAdapter {

        List<LoginModel> listOfLoginCredentials;

        public LoginListAdapter() {
            super();

            listOfLoginCredentials = new ArrayList<>();
            listOfLoginCredentials.addAll(listOfHardcodedLoginCredentials);
            listOfLoginCredentials.addAll(listOfPreviouslyEnteredLoginCredentials);
        }

        @Override
        public boolean areAllItemsEnabled() {
            return true;
        }

        @Override
        public boolean isEnabled(int position) {
            return true;
        }

        @Override
        public void registerDataSetObserver(DataSetObserver observer) {

        }

        @Override
        public void unregisterDataSetObserver(DataSetObserver observer) {

        }

        @Override
        public int getCount() {
            return listOfLoginCredentials.size();
        }

        @Override
        public Object getItem(int position) {
            return listOfLoginCredentials.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            LoginModel loginModel = listOfLoginCredentials.get(position);

            LinearLayout verticalLayout = new LinearLayout(mContext);
            verticalLayout.setOrientation(LinearLayout.VERTICAL);
            if (position < listOfHardcodedLoginCredentials.size()) {
                verticalLayout.setBackgroundColor(ContextCompat.getColor(mContext, R.color.abaLightGrey));
            } else {
                verticalLayout.setBackgroundColor(ContextCompat.getColor(mContext, R.color.abaDescriptionLightGrey));
            }

            String loginString = "<b>User:</b> " + loginModel.getLogin();
            TextView loginTextView = new TextView(mContext);
            loginTextView.setText(Html.fromHtml(loginString));

            String passwordString = "<b>Password:</b> " + loginModel.getPassword();
            TextView passwordTextView = new TextView(mContext);
            passwordTextView.setText(Html.fromHtml(passwordString));

            String moreInfoString = "<b>Info:</b> " + loginModel.getMoreInfo();
            TextView moreInfoTextView = new TextView(mContext);
            moreInfoTextView.setText(Html.fromHtml(moreInfoString));

            verticalLayout.addView(loginTextView);
            verticalLayout.addView(passwordTextView);
            verticalLayout.addView(moreInfoTextView);

            return verticalLayout;
        }

        @Override
        public int getItemViewType(int position) {
            return 1;
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }

        @Override
        public boolean isEmpty() {
            return listOfLoginCredentials.isEmpty();
        }
    }

    /**
     * Login model to handle login metadata
     */
    private static class LoginModel implements Serializable {
        private String login;
        private String password;
        private String moreInfo;

        public String getMoreInfo() {
            return moreInfo;
        }

        public void setMoreInfo(String moreInfo) {
            this.moreInfo = moreInfo;
        }

        public String getLogin() {
            return login;
        }

        public void setLogin(String login) {
            this.login = login;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        @Override
        public boolean equals(Object o) {
            if (o instanceof LoginModel) {
                LoginModel anotherLoginModel = (LoginModel) o;
                if (!moreInfo.equals(anotherLoginModel.getMoreInfo())) {
                    return false;
                }
                if (!login.equals(anotherLoginModel.getLogin())) {
                    return false;
                }
                if (!password.equals(anotherLoginModel.getPassword())) {
                    return false;
                }
                return true;

            } else {
                return false;
            }
        }
    }

    // ================================================================================
    // Private methods - Hardcoded login
    // ================================================================================

    /**
     * Method to read the login information from the hardcoded file in Assets
     *
     * @param context
     * @return
     */
    private static List<LoginModel> readLoginInformationFromAssets(Context context) {

        List<LoginModel> listOfHardcodedLoginOptions = new ArrayList<>();
        try {

            InputStream stream = context.getAssets().open(SHEPHERD_FOLDER_IN_ASSETS + "/" + HARDCODED_LOGIN_FILE);
            StringWriter writer = new StringWriter();
            IOUtils.copy(stream, writer, "UTF-8");
            String jsonString = writer.toString();
            JSONObject jsonObject = new JSONObject(jsonString);
            if (jsonObject.has("login_options")) {
                JSONArray arrayOfLoginOptions = jsonObject.getJSONArray("login_options");
                for (int loginOptionIndex = 0; loginOptionIndex < arrayOfLoginOptions.length(); loginOptionIndex++) {
                    JSONObject loginOptionAtIndex = arrayOfLoginOptions.getJSONObject(loginOptionIndex);

                    LoginModel loginModel = new LoginModel();
                    loginModel.setLogin(loginOptionAtIndex.getString("login"));
                    loginModel.setPassword(loginOptionAtIndex.getString("password"));
                    loginModel.setMoreInfo(loginOptionAtIndex.getString("info"));

                    listOfHardcodedLoginOptions.add(loginModel);
                }
            }

        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }

        return listOfHardcodedLoginOptions;
    }

    private static void addLoginCredentials(Context aContext, LoginModel loginModel) {

        // If login is already present in hardcoded list
        List<LoginModel> hardcodedListOfLoginCredentials = readLoginInformationFromAssets(aContext);
        if (hardcodedListOfLoginCredentials.contains(loginModel)) {
            // Do not add
            return;
        }

        // If list has been previously entered
        List<LoginModel> currentListOfLoginCredentials = readEnteredLoginOptions();
        if (currentListOfLoginCredentials.contains(loginModel)) {
            // Do not add
            return;
        }

        // Adding new entry at the beginning of the list
        currentListOfLoginCredentials.add(0, loginModel);
        saveEnteredLoginCredentials(currentListOfLoginCredentials);
    }

    // ================================================================================
    // Private methods (Storage)
    // ================================================================================

    private static List<LoginModel> readEnteredLoginOptions() {
        List<LoginModel> listOfModels = new ArrayList<>();
        try {
            FileInputStream fis = new FileInputStream(getCredentialsFileUrl());
            ObjectInputStream inputStream = new ObjectInputStream(fis);
            List<LoginModel> readList = (List<LoginModel>) inputStream.readObject();
            listOfModels.addAll(readList);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return listOfModels;
    }

    private static boolean saveEnteredLoginCredentials(List<LoginModel> listToSave) {
        try {
            FileOutputStream fos = new FileOutputStream(getCredentialsFileUrl());
            ObjectOutputStream os = new ObjectOutputStream(fos);
            os.writeObject(listToSave);
            os.close();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    private static File getCredentialsFileUrl() {

        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/shepherd_credentials");
        myDir.mkdirs();
        File file = new File(myDir, CREDENTIALS_FILE_NAME);
        return file;
    }

}
