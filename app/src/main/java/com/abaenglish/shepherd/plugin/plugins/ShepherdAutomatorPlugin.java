package com.abaenglish.shepherd.plugin.plugins;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Handler;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;

import com.abaenglish.shepherd.ABAShepherdEditor;
import com.abaenglish.shepherd.plugin.ShepherdPluginInterface;

/**
 * Created by Jesus Espejo using mbp13jesus on 23/10/15.
 * Enhanced by Jesus Espejo on 04/08/2016. Dark theme.
 */
public class ShepherdAutomatorPlugin implements ShepherdPluginInterface {

    public interface ABAShepherdAutomatorPluginAction {
        void automate();
    }

    private static final String ABA_AUTOMATE_SHARED_PREFERENCES = "ABA_AUTOMATE_SHARED_PREFERENCES";
    private static final String ABA_AUTOMATE_EVALUATION_KEY = "ABA_AUTOMATE_EVALUATION_KEY";
    private Context mContext;

    public ShepherdAutomatorPlugin(Context aContext) {
        super();
        this.mContext = aContext;
    }

    private static int DELAY_1_S = 1000;
    private static int DELAY_0_6_S = 600;

    @Override
    public View viewForPlugin(Context context) {
        TextView textViewForPlugin = new TextView(context);
        if (isAutomationEnabled(mContext)) {
            textViewForPlugin.setText("Automate sections? - YES");
        } else {
            textViewForPlugin.setText("Automate sections? - NO");
        }
        textViewForPlugin.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
        return textViewForPlugin;
    }

    @Override
    public void execute(Context context) {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(context, AlertDialog.THEME_DEVICE_DEFAULT_DARK);
        builderSingle.setTitle("Automate sections?");
        builderSingle.setNegativeButton("Reset", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                setAutomation(false);
                dialog.dismiss();
            }
        });
        builderSingle.setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                setAutomation(true);
                dialog.dismiss();
            }
        });
        builderSingle.show();
    }

    public static void automateMethod(Context aContext, final ABAShepherdAutomatorPluginAction automator) {
        if (ABAShepherdEditor.isInternal() && ShepherdAutomatorPlugin.isAutomationEnabled(aContext)) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    automator.automate();
                }
            }, ShepherdAutomatorPlugin.DELAY_0_6_S);
        }
    }

    // ================================================================================
    // Private methods
    // ================================================================================

    public static boolean isAutomationEnabled(Context aContext) {
        SharedPreferences settings = aContext.getSharedPreferences(ABA_AUTOMATE_SHARED_PREFERENCES, Context.MODE_PRIVATE);
        return settings.getBoolean(ABA_AUTOMATE_EVALUATION_KEY, false);
    }

    private void setAutomation(boolean active) {
        SharedPreferences settings = mContext.getSharedPreferences(ABA_AUTOMATE_SHARED_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(ABA_AUTOMATE_EVALUATION_KEY, active);
        editor.commit();
    }
}
