package com.abaenglish.shepherd.plugin.plugins;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.util.TypedValue;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.abaenglish.shepherd.plugin.ShepherdPluginInterface;

/**
 * Created by Jesus Espejo using mbp13jesus on 05/08/15.
 */
public class ShepherdExternalLogin implements ShepherdPluginInterface {

    private static final String ABA_EXTERNAL_TOKEN_SHARED_PREFERENCES = "ABA_EXTERNAL_TOKEN_SHARED_PREFERENCES";
    private static final String ABA_EXTERNAL_TOKEN_KEY = "ABA_EXTERNAL_TOKEN_KEY";

    @Override
    public View viewForPlugin(Context context) {
        TextView textViewForPlugin = new TextView(context);
        textViewForPlugin.setText("User external Token");
        textViewForPlugin.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
        return textViewForPlugin;
    }

    @Override
    public void execute(final Context context) {

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Enter the token");

        // Use an EditText view to get user input.
        final EditText input = new EditText(context);
        builder.setView(input);

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int whichButton) {
                String value = input.getText().toString();

                SharedPreferences settings = context.getSharedPreferences(ABA_EXTERNAL_TOKEN_SHARED_PREFERENCES, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = settings.edit();
                editor.putString(ABA_EXTERNAL_TOKEN_KEY, value);
                editor.apply();

                Toast.makeText(context, "Copied!. Make sure you are logged out in order to user the token. Remember: Only one usage!", Toast.LENGTH_LONG).show();
            }
        });

        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do nothing
            }
        });

        builder.create();
        builder.show();
    }

    // ================================================================================
    // Public methods
    // ================================================================================

    public static String retrieveTokenAndErase(Context aContext) {

        // Reading token
        SharedPreferences settings = aContext.getSharedPreferences(ABA_EXTERNAL_TOKEN_SHARED_PREFERENCES, Context.MODE_PRIVATE);
        String token = settings.getString(ABA_EXTERNAL_TOKEN_KEY, null);

        // Erasing token
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(ABA_EXTERNAL_TOKEN_KEY, null);
        editor.apply();

        // Token return
        return token;
    }


}
