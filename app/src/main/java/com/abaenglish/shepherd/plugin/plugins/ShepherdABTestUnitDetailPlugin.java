package com.abaenglish.shepherd.plugin.plugins;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.DataSetObserver;
import android.os.Handler;
import android.os.Looper;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.abaenglish.shepherd.plugin.ShepherdPluginInterface;

/**
 * Created by Jesus Espejo using mbp13jesus on 14/10/16.
 */

public class ShepherdABTestUnitDetailPlugin implements ShepherdPluginInterface {

    private static final String UNIT_DETAIL_TYPE_SHARED_PREFERENCES = "UNIT_DETAIL_TYPE_SHARED_PREFERENCES";
    private static final String UNIT_DETAIL_TYPE_KEY = "UNIT_DETAIL_TYPE_KEY";

    private static final String UNIT_DETAIL_TYPE_ROSCO = "ROSCO";
    private static final String UNIT_DETAIL_TYPE_LIST = "LIST";
    private static final String UNIT_DETAIL_TYPE_LIST_2 = "LIST_2";
    private static final String UNIT_DETAIL_TYPE_NONE = "NONE";

    private Context mContext;
    private static ShepherdABTestUnitDetailPlugin _plugin = null;
    boolean warningShown = false;

    public static ShepherdABTestUnitDetailPlugin getInstance() {
        return _plugin;
    }

    public static ShepherdABTestUnitDetailPlugin createInstance(Context context) {
        if (_plugin == null) {
            _plugin = new ShepherdABTestUnitDetailPlugin(context);
        }
        return _plugin;
    }

    public ShepherdABTestUnitDetailPlugin(Context context) {
        super();
        this.mContext = context;
    }

    @Override
    public View viewForPlugin(Context context) {
        TextView textViewForPlugin = new TextView(context);
        textViewForPlugin.setText("Unit detail: " + currentSelectedUnitDetailType(mContext) + ". Click to change");
        textViewForPlugin.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
        return textViewForPlugin;
    }

    @Override
    public void execute(Context context) {
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(context, AlertDialog.THEME_DEVICE_DEFAULT_DARK);
        builderSingle.setTitle("Select user type to force");
        builderSingle.setNegativeButton("Cancel", new NegativeOnClickListener());
        builderSingle.setAdapter(new UnitDetailTypeListAdapter(), new UnitDetailTypeSelectionOnClickListener());
        builderSingle.show();
    }

    // ================================================================================
    // Public methods
    // ================================================================================

    public boolean shouldForceRosco() {
        return currentSelectedUnitDetailType(mContext).equalsIgnoreCase(UNIT_DETAIL_TYPE_ROSCO);
    }

    public boolean shouldForceList() {
        return currentSelectedUnitDetailType(mContext).equalsIgnoreCase(UNIT_DETAIL_TYPE_LIST);
    }

    public boolean shouldForceList2() {
        return currentSelectedUnitDetailType(mContext).equalsIgnoreCase(UNIT_DETAIL_TYPE_LIST_2);
    }

    public void showWarningMessage() {
        String textToShow = null;

        // Determining text to show
        if (shouldForceRosco()) {
            textToShow = "Forcing ROSCO";
        }
        if (shouldForceList()) {
            textToShow = "Forcing LIST 1";
        }
        if (shouldForceList2()) {
            textToShow = "Forcing LIST 2";
        }
        // Showing text (once every 5 secs)
        if (textToShow != null && !warningShown) {
            Toast.makeText(mContext, textToShow, Toast.LENGTH_LONG).show();
            warningShown = true;

            // Showing warning once every 5 seconds
            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    warningShown = false;
                }
            }, 5000);
        }
    }

    // ================================================================================
    // Private methods
    // ================================================================================

    private static void saveUnitDetailType(Context mContext, String type) {
        SharedPreferences settings = mContext.getSharedPreferences(UNIT_DETAIL_TYPE_SHARED_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(UNIT_DETAIL_TYPE_KEY, type);
        editor.apply();
    }

    private static String currentSelectedUnitDetailType(Context context) {
        SharedPreferences settings = context.getSharedPreferences(UNIT_DETAIL_TYPE_SHARED_PREFERENCES, Context.MODE_PRIVATE);
        return settings.getString(UNIT_DETAIL_TYPE_KEY, UNIT_DETAIL_TYPE_NONE); // Default is NONE
    }

    // ================================================================================
    // Private classes
    // ================================================================================

    /**
     * Class to handle cancel button action
     */
    private class NegativeOnClickListener implements DialogInterface.OnClickListener {

        @Override
        public void onClick(DialogInterface dialog, int which) {
            dialog.dismiss();
        }
    }

    /**
     * Class to handle user type selection
     */
    private class UnitDetailTypeListAdapter implements ListAdapter {


        @Override
        public boolean areAllItemsEnabled() {
            return true;
        }

        @Override
        public boolean isEnabled(int position) {
            return true;
        }

        @Override
        public void registerDataSetObserver(DataSetObserver observer) {

        }

        @Override
        public void unregisterDataSetObserver(DataSetObserver observer) {

        }

        @Override
        public int getCount() {
            return 4;
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public boolean hasStableIds() {
            return true;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            TextView userTypeTextView = new TextView(mContext);
            userTypeTextView.setPadding(50, 50, 50, 50);
            userTypeTextView.setText(detailTypeForIndex(position));
            return userTypeTextView;
        }

        @Override
        public int getItemViewType(int position) {
            return 0;
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }

        @Override
        public boolean isEmpty() {
            return false;
        }
    }

    /**
     * Class to handle user type selection
     */
    private class UnitDetailTypeSelectionOnClickListener implements DialogInterface.OnClickListener {

        @Override
        public void onClick(DialogInterface dialog, int which) {
            String type = detailTypeForIndex(which);
            saveUnitDetailType(mContext, type);

            Toast.makeText(mContext, "Forcing unit detail type to: " + detailTypeForIndex(which), Toast.LENGTH_SHORT).show();
        }
    }

    private String detailTypeForIndex(int index) {
        String type;
        switch (index) {
            case 0:
                type = UNIT_DETAIL_TYPE_ROSCO;
                break;
            case 1:
                type = UNIT_DETAIL_TYPE_LIST;
                break;
            case 2:
                type = UNIT_DETAIL_TYPE_LIST_2;
                break;
            default:
                type = UNIT_DETAIL_TYPE_NONE;
                break;
        }

        return type;
    }
}
