package com.abaenglish.shepherd.plugin.plugins;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.DataSetObserver;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.abaenglish.shepherd.plugin.ShepherdPluginInterface;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jesus Espejo using mbp13jesus on 01/07/15.
 */
public class ShepherdAppVersioningPlugin implements ShepherdPluginInterface {

    @Override
    public View viewForPlugin(Context context) {
        TextView textViewForPlugin = new TextView(context);
        textViewForPlugin.setText("App and device info");
        textViewForPlugin.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
        return textViewForPlugin;
    }

    @Override
    public void execute(Context context) {
        Intent shepherdIntent = new Intent(context, AppVersioningActivity.class);
        context.startActivity(shepherdIntent);
    }

    // ================================================================================
    // Private class - Activity to show the info
    // ================================================================================

    public static class AppVersioningActivity extends Activity {

        public AppVersioningActivity() {
            super();
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(initContentView());
        }

        // ================================================================================
        // Private methods
        // ================================================================================

        protected View initContentView() {
            LinearLayout verticalLayout = new LinearLayout(this);
            verticalLayout.setOrientation(LinearLayout.VERTICAL);

            TextView textviewHeader = new TextView(this);
            textviewHeader.setText("App and device info");
            textviewHeader.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            textviewHeader.setTypeface(null, Typeface.BOLD);
            textviewHeader.setGravity(Gravity.CENTER);
            verticalLayout.addView(textviewHeader, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));

            ListView propertyList = new ListView(this);
            propertyList.setAdapter(new InfoListAdapter());
            verticalLayout.addView(propertyList, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));

            return verticalLayout;
        }

        // ================================================================================
        // Private classes
        // ================================================================================

        public class InfoListAdapter implements ListAdapter {

            private List<String> listOfKeys;
            private List<String> listOfValues;

            public InfoListAdapter() {
                super();

                String appVersion = "";
                String buildNumber = "";
                String androidVersion = String.valueOf(android.os.Build.VERSION.SDK_INT);
                String deviceManufacturer = Build.MANUFACTURER;
                String deviceModel = Build.MODEL;
                String display = Build.DISPLAY;
                DisplayMetrics metrics = getResources().getDisplayMetrics();

                try {
                    PackageInfo pInfo = AppVersioningActivity.this.getPackageManager().getPackageInfo(getPackageName(), 0);
                    appVersion = pInfo.versionName;
                    buildNumber = String.valueOf(pInfo.versionCode);
                } catch (PackageManager.NameNotFoundException e) {
                    e.printStackTrace();
                }

                // Creating arrays
                listOfKeys = new ArrayList<>();
                listOfValues = new ArrayList<>();

                // Filling arrays
                listOfKeys.add("App version");
                listOfValues.add(appVersion);

                listOfKeys.add("Build number");
                listOfValues.add(buildNumber);

                listOfKeys.add("Android version");
                listOfValues.add(androidVersion);

                listOfKeys.add("Device manufacturer");
                listOfValues.add(deviceManufacturer);

                listOfKeys.add("Device model");
                listOfValues.add(deviceModel);

                listOfKeys.add("Display");
                listOfValues.add(display);

                listOfKeys.add("Screen density");
                listOfValues.add(metrics.density + " ");

                listOfKeys.add("Screen density dpi");
                listOfValues.add(metrics.densityDpi + " dpi");
            }

            @Override
            public boolean areAllItemsEnabled() {
                return true;
            }

            @Override
            public boolean isEnabled(int position) {
                return true;
            }

            @Override
            public void registerDataSetObserver(DataSetObserver observer) {

            }

            @Override
            public void unregisterDataSetObserver(DataSetObserver observer) {

            }

            @Override
            public int getCount() {
                return listOfKeys.size();
            }

            @Override
            public Object getItem(int position) {
                return listOfKeys.get(position);
            }

            @Override
            public long getItemId(int position) {
                return position;
            }

            @Override
            public boolean hasStableIds() {
                return false;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                LinearLayout verticalLayout = new LinearLayout(AppVersioningActivity.this);
                verticalLayout.setOrientation(LinearLayout.VERTICAL);
                verticalLayout.setPadding(10, 10, 10, 10);

                TextView textviewEnvTitle = new TextView(AppVersioningActivity.this);
                textviewEnvTitle.setText(listOfKeys.get(position));
                textviewEnvTitle.setTypeface(null, Typeface.BOLD);
                textviewEnvTitle.setPadding(0, 5, 0, 10);
                verticalLayout.addView(textviewEnvTitle, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));

                TextView textviewEnvDescription = new TextView(AppVersioningActivity.this);
                textviewEnvDescription.setText(listOfValues.get(position));
                verticalLayout.addView(textviewEnvDescription, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));

                return verticalLayout;
            }

            @Override
            public int getItemViewType(int position) {
                return 0;
            }

            @Override
            public int getViewTypeCount() {
                return 1;
            }

            @Override
            public boolean isEmpty() {
                return listOfKeys.isEmpty();
            }
        }

    }
}
