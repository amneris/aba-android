package com.abaenglish.shepherd.activities;

import android.app.Activity;
import android.database.DataSetObserver;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.abaenglish.shepherd.ABAShepherdEditor;
import com.abaenglish.shepherd.configuration.configurators.GenericShepherdConfigurator.ShepherdConfiguratorType;
import com.abaenglish.shepherd.configuration.engine.model.ShepherdConfiguration;
import com.abaenglish.shepherd.configuration.engine.model.ShepherdGenericEnvironment;

/**
 * Created by Jesus Espejo using mbp13jesus on 01/07/15.
 */
public class ShepherdConfigurationController extends Activity {

    public static String CONFIGURATION_EXTRA = "";
    private ShepherdConfiguration configuration;
    private ShepherdGenericEnvironment currentEnvironment;
    private ListView environmentList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        configuration = (ShepherdConfiguration) getIntent().getSerializableExtra(CONFIGURATION_EXTRA);
        currentEnvironment = ABAShepherdEditor.shared(this).environmentForShepherdConfigurationType(this, ShepherdConfiguratorType.kShepherdConfiguratorTypeABACore);

        setContentView(initContentView());
    }

    // ================================================================================
    // Private methods
    // ================================================================================

    protected View initContentView() {

        LinearLayout verticalLayout = new LinearLayout(this);
        verticalLayout.setOrientation(LinearLayout.VERTICAL);

        TextView textviewHeader = new TextView(this);
        textviewHeader.setText(configuration.getConfigurationName());
        textviewHeader.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        textviewHeader.setTypeface(null, Typeface.BOLD);
        textviewHeader.setGravity(Gravity.CENTER);
        verticalLayout.addView(textviewHeader, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));

        environmentList = new ListView(this);
        environmentList.setAdapter(new EnvironmentListAdapter());
        environmentList.setOnItemClickListener(new EnvironmentOnItemClickListener());
        verticalLayout.addView(environmentList, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));

        return verticalLayout;
    }

    // ================================================================================
    // Private classes
    // ================================================================================

    private class EnvironmentListAdapter implements ListAdapter {

        @Override
        public boolean areAllItemsEnabled() {
            return true;
        }

        @Override
        public boolean isEnabled(int position) {
            return true;
        }

        @Override
        public void registerDataSetObserver(DataSetObserver observer) {

        }

        @Override
        public void unregisterDataSetObserver(DataSetObserver observer) {

        }

        @Override
        public int getCount() {
            return configuration.getListOfEnvironments().size();
        }

        @Override
        public Object getItem(int position) {
            return configuration.getListOfEnvironments().get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ShepherdGenericEnvironment environment = (ShepherdGenericEnvironment) getItem(position);

            LinearLayout verticalLayout = new LinearLayout(ShepherdConfigurationController.this);
            verticalLayout.setOrientation(LinearLayout.VERTICAL);
            verticalLayout.setPadding(10, 10, 10, 10);

            ImageView selectedImageView = new ImageView(ShepherdConfigurationController.this);

            TextView textviewEnvTitle = new TextView(ShepherdConfigurationController.this);
            textviewEnvTitle.setText(environment.getEnvironmentName());
            textviewEnvTitle.setTypeface(null, Typeface.BOLD);
            textviewEnvTitle.setPadding(0, 5, 0, 10);

            LinearLayout horizontalTitleLayout = new LinearLayout(ShepherdConfigurationController.this);
            horizontalTitleLayout.addView(textviewEnvTitle, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            horizontalTitleLayout.addView(selectedImageView, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));

            verticalLayout.addView(horizontalTitleLayout, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));

            TextView textviewEnvDescription = new TextView(ShepherdConfigurationController.this);
            textviewEnvDescription.setText(environment.toString());
            verticalLayout.addView(textviewEnvDescription, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));

            boolean isCurrentEnvironment = environment.equals(currentEnvironment);
            if (isCurrentEnvironment) {
                verticalLayout.setBackgroundColor(Color.LTGRAY);
                selectedImageView.setImageResource(android.R.drawable.checkbox_on_background);
            }

            return verticalLayout;
        }

        @Override
        public int getItemViewType(int position) {
            return 0;
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }

        @Override
        public boolean isEmpty() {
            return configuration.getListOfEnvironments().size() == 0;
        }
    }

    private class EnvironmentOnItemClickListener implements AdapterView.OnItemClickListener {

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            // Setting new selected environment
            ShepherdGenericEnvironment selectedEnvironment = configuration.getListOfEnvironments().get(position);
            ABAShepherdEditor.shared(ShepherdConfigurationController.this).setCurrentEnvironment(ShepherdConfigurationController.this, selectedEnvironment, configuration);

            // Updating view by recreating the adapter again
            currentEnvironment = selectedEnvironment;
            environmentList.setAdapter(new EnvironmentListAdapter());

            // Notifying the Editor it has to close the app
            ABAShepherdEditor.didConfigurationChange = true;
        }
    }
}
