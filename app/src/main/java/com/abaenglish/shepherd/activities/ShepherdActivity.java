package com.abaenglish.shepherd.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.TextView;

import com.abaenglish.shepherd.ABAShepherdEditor;
import com.abaenglish.shepherd.configuration.engine.model.ShepherdConfiguration;
import com.abaenglish.shepherd.plugin.ShepherdPluginInterface;

import java.io.Serializable;

import static android.graphics.Color.LTGRAY;

/**
 * Created by Jesus Espejo using mbp13jesus on 30/06/15.
 */
public class ShepherdActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(initContentView());
    }

    // ================================================================================
    // Overriding
    // ================================================================================

    @Override
    public void onBackPressed() {
        if (ABAShepherdEditor.didConfigurationChange) {
            new AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_DARK)
                    .setTitle("The changes you applied need an application restart. The application will now quit!")
                    .setPositiveButton("Restart", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            quitApp();
                        }
                    })
                    .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // do nothing
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .show();
        } else {
            super.onBackPressed();
        }
    }


    // ================================================================================
    // Private methods
    // ================================================================================

    protected void quitApp() {
        int pid = android.os.Process.myPid();
        android.os.Process.killProcess(pid);
        System.exit(0);
    }

    protected View initContentView() {

        LinearLayout verticalLayout = new LinearLayout(this);
        verticalLayout.setOrientation(LinearLayout.VERTICAL);

        // Adding plugins section header
        TextView pluginsHeader = new TextView(this);
        pluginsHeader.setGravity(Gravity.CENTER);
        pluginsHeader.setPadding(0, 5, 0, 5);
        pluginsHeader.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        pluginsHeader.setTypeface(null, Typeface.BOLD);
        pluginsHeader.setText("Plugins");
        verticalLayout.addView(pluginsHeader, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
        createSeparator(verticalLayout);

        // Adding plugins
        for (ShepherdPluginInterface plugin : ABAShepherdEditor.shared(this).getListOfPlugings()) {
            View viewRepresentingPlugin = plugin.viewForPlugin(this);
            viewRepresentingPlugin.setPadding(20, 15, 0, 15);
            viewRepresentingPlugin.setBackgroundColor(LTGRAY);
            viewRepresentingPlugin.setOnClickListener(new PluginOnClickListener(plugin));
            verticalLayout.addView(viewRepresentingPlugin, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
            createSeparator(verticalLayout);
        }

        // Adding configuration section header
        TextView configurationHeader = new TextView(this);
        configurationHeader.setText("Configurations");
        configurationHeader.setTypeface(null, Typeface.BOLD);
        configurationHeader.setGravity(Gravity.CENTER);
        configurationHeader.setPadding(0, 5, 0, 5);
        configurationHeader.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        verticalLayout.addView(configurationHeader, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
        createSeparator(verticalLayout);

        // Adding configurations
        for (ShepherdConfiguration configuration : ABAShepherdEditor.shared(this).getListOfConfigurations()) {
            TextView textViewConfiguration = new TextView(this);
            textViewConfiguration.setBackgroundColor(LTGRAY);
            textViewConfiguration.setPadding(20, 15, 0, 15);
            textViewConfiguration.setTextSize(TypedValue.COMPLEX_UNIT_SP, 15);
            textViewConfiguration.setText(configuration.getConfigurationName());
            textViewConfiguration.setOnClickListener(new ConfigurationOnClickListener(configuration));
            verticalLayout.addView(textViewConfiguration, new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
            createSeparator(verticalLayout);
        }

        // Creating main scrollview with the vertical layout and returning it
        ScrollView scrollView = new ScrollView(this);
        scrollView.addView(verticalLayout);
        return scrollView;
    }

    protected void createSeparator(LinearLayout verticalLayoutToAdd) {
        View v = new View(this);
        LinearLayout.LayoutParams viewLp = new LayoutParams(LayoutParams.MATCH_PARENT, 1);
        v.setLayoutParams(viewLp);
        v.setBackgroundColor(Color.BLACK);
        verticalLayoutToAdd.addView(v);
    }

    // ================================================================================
    // Private classes - OnClickListener
    // ================================================================================

    private class PluginOnClickListener implements View.OnClickListener {

        private ShepherdPluginInterface plugin;

        public PluginOnClickListener(ShepherdPluginInterface plugin) {
            super();
            this.plugin = plugin;
        }

        @Override
        public void onClick(View v) {
            plugin.execute(ShepherdActivity.this);
        }
    }

    private class ConfigurationOnClickListener implements View.OnClickListener {

        private ShepherdConfiguration configuration;

        public ConfigurationOnClickListener(ShepherdConfiguration configuration) {
            this.configuration = configuration;
        }

        @Override
        public void onClick(View v) {
            // open configuration activity
            Intent shepherdIntent = new Intent(ShepherdActivity.this, ShepherdConfigurationController.class);
            shepherdIntent.putExtra(ShepherdConfigurationController.CONFIGURATION_EXTRA, (Serializable) configuration);
            startActivity(shepherdIntent);
        }
    }
}
