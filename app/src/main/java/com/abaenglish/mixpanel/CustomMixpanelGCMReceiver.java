package com.abaenglish.mixpanel;

import android.content.Context;
import android.content.Intent;

import com.abaenglish.videoclass.domain.content.PreferencesProvider;
import com.abaenglish.videoclass.domain.content.PreferencesProviderImpl;
import com.mixpanel.android.mpmetrics.GCMReceiver;

/**
 * Created by Jesus Espejo using mbp13jesus on 03/08/16.
 */

public class CustomMixpanelGCMReceiver extends GCMReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        PreferencesProvider pref = new PreferencesProviderImpl(context);

        boolean notificationShouldUserMaterialIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
        if (notificationShouldUserMaterialIcon) {
            intent.putExtra("mp_icnm", "ic_logo_white_24dp");
        }

        if (pref.isPushNotificationAllowed()) {
            super.onReceive(context, intent);
        }
    }

}
