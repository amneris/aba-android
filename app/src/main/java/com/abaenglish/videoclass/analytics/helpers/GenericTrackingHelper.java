package com.abaenglish.videoclass.analytics.helpers;

import android.content.Context;

import com.abaenglish.videoclass.analytics.helpers.GenericTrackingInterface.DownloadDialogResultType;
import com.abaenglish.videoclass.analytics.helpers.GenericTrackingInterface.DownloadDialogType;
import com.abaenglish.videoclass.data.network.APIManager;
import com.abaenglish.videoclass.data.persistence.ABALevel;
import com.abaenglish.videoclass.data.persistence.ABAPhrase;
import com.abaenglish.videoclass.data.persistence.ABAPlan;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.data.persistence.ABAUser;
import com.abaenglish.videoclass.presentation.section.SectionType;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import static com.abaenglish.videoclass.analytics.helpers.GenericTrackingInterface.DOWNLOAD_DIALOG_RESULT_CANCEL;
import static com.abaenglish.videoclass.analytics.helpers.GenericTrackingInterface.DOWNLOAD_DIALOG_RESULT_PROCEED;
import static com.abaenglish.videoclass.analytics.helpers.GenericTrackingInterface.DOWNLOAD_DIALOG_STORAGE;
import static com.abaenglish.videoclass.analytics.helpers.GenericTrackingInterface.DOWNLOAD_DIALOG_UNKNOWN;
import static com.abaenglish.videoclass.analytics.helpers.GenericTrackingInterface.DOWNLOAD_DIALOG_WIFI;
import static com.abaenglish.videoclass.analytics.helpers.GenericTrackingInterface.DOWNLOAD_DIALOG_RESULT_UNKNOWN;

/**
 * Created by Jesus Espejo using mbp13jesus on 15/06/15.
 * Helper with static methods to do common calculations regarding the trackers
 */
public class GenericTrackingHelper {

    public static Integer SECTION_TIME_THRESHOLD = 30; // Seconds

    // ================================================================================
    // Public methods - ABAUser
    // ================================================================================

    public static Map<String, Object> ABAUserTrackingProperties(Context aContext, ABAUser user) {
        Map<String, Object> trackingProperties = new HashMap<>();
        if (user == null) {
            return trackingProperties;
        }
        if (user.getUserId() != null) {
            safePut(trackingProperties, "Userid", user.getUserId());
        }
        if (user.getName() != null) {
            safePut(trackingProperties, "$name", user.getName());
        }
        if (user.getEmail() != null) {
            safePut(trackingProperties, "$email", user.getEmail());
        }
        if (user.getCountry() != null) {
            safePut(trackingProperties, "User country", user.getCountry());
        }
        if (user.getUserLang() != null) {
            safePut(trackingProperties, "Language", user.getUserLang());
        }
        if (user.getCurrentLevel() != null && user.getCurrentLevel().getIdLevel() != null) {
            safePut(trackingProperties, "Level", user.getCurrentLevel().getIdLevel());
        }
        if (user.getTeacherId() != null) {
            safePut(trackingProperties, "Teacherid", user.getTeacherId());
        }
        if (user.getTeacherName() != null) {
            safePut(trackingProperties, "Teacher name", user.getTeacherName());
        }
        if (user.getUserType() != null) {
            safePut(trackingProperties, "User Type", getUserTypeDescription(user.getUserType()));
        }
        if (user.getPartnerID() != null) {
            safePut(trackingProperties, "PartnerID", user.getPartnerID());
        }
        if (user.getSourceID() != null) {
            safePut(trackingProperties, "SourceID", user.getSourceID());
        }
        if (user.getGender() != null) {
            safePut(trackingProperties, "Gender", user.getGender());
        }
        if (user.getPhone() != null) {
            safePut(trackingProperties, "Phone", user.getPhone());
        }
        if (user.getBirthdate() != null) {
            safePut(trackingProperties, "Birthdate", user.getBirthdate());
        }

        // Adding device information
        if (APIManager.isTablet(aContext)) {
            trackingProperties.put("Has Android tablet", "yes");
        } else {
            trackingProperties.put("Has Android smartphone", "yes");
        }

        return trackingProperties;
    }

    public static Map ABAUserTrackingSuperProperties(ABAUser user) {
        Map trackingSuperProperties = new HashMap();

        if (user.getUserId() != null) {
            safePut(trackingSuperProperties, "Userid", user.getUserId());
        }
        if (user.getEmail() != null) {
            safePut(trackingSuperProperties, "Email", user.getEmail());
        }
        if (user.getPartnerID() != null) {
            safePut(trackingSuperProperties, "PartnerID", user.getPartnerID());
        }

        safePut(trackingSuperProperties, "User Type", getUserTypeDescription(user.getUserType()));

        return trackingSuperProperties;
    }

    // ================================================================================
    // Public method - Time
    // ================================================================================

    public static Map<String, Object> timestampTrackingPropertiesObject() {
        Map<String, Object> mapWithRecordType = new HashMap<>();
        safePut(mapWithRecordType, "ABA Timestamp", currentMixpanelFormatedDate());
        return mapWithRecordType;
    }

    public static Map<String, Object> registrationDateUserProperties() {
        Map<String, Object> mapWithRecordType = new HashMap<>();
        safePut(mapWithRecordType, "Registration date", currentMixpanelFormatedDate());
        return mapWithRecordType;
    }

    public static Map<String, Object> lastLoginDateUserProperties() {
        Map<String, Object> mapWithRecordType = new HashMap<>();
        safePut(mapWithRecordType, "Last login date", currentMixpanelFormatedDate());
        return mapWithRecordType;
    }

    // ================================================================================
    // Public method - Record type (Register value)
    // ================================================================================

    public static Map<String, Object> recordTypeTrackingPropertiesObject(GenericTrackingInterface.RecordType recordType) {
        Map<String, Object> mapWithRecordType = new HashMap<>();
        safePut(mapWithRecordType, "Type", attributeForRecordType(recordType));
        return mapWithRecordType;
    }

    public static String attributeForRecordType(GenericTrackingInterface.RecordType recordType) {
        switch (recordType) {
            case kRecordTypeFacebook: {
                return GenericTrackingInterface.REGISTRATION_TYPE_FACEBOOK;
            }
            case kRecordTypeMail: {
                return GenericTrackingInterface.REGISTRATION_TYPE_EMAIL;
            }
            default: {
                return GenericTrackingInterface.REGISTRATION_UNKNOWN;
            }
        }
    }

    // ================================================================================
    // Public method - Download dismissed
    // ================================================================================

    public static Map<String, Object> downloadDismissedTrackingPropertiesObject(DownloadDialogType dialogType, DownloadDialogResultType result) {
        Map<String, Object> mapWithRecordType = new HashMap<>();
        safePut(mapWithRecordType, "Dialog type", attributeForDownloadDialogType(dialogType));
        safePut(mapWithRecordType, "Dialog result", attributeForDownloadDialogResultType(result));
        return mapWithRecordType;
    }

    private static String attributeForDownloadDialogType(DownloadDialogType dialogType) {
        switch (dialogType) {
            case kDownloadDialogWifi: {
                return DOWNLOAD_DIALOG_WIFI;
            }
            case kDownloadDialogStorage: {
                return DOWNLOAD_DIALOG_STORAGE;
            }
            default: {
                return DOWNLOAD_DIALOG_UNKNOWN;
            }
        }
    }

    private static String attributeForDownloadDialogResultType(DownloadDialogResultType resultType) {
        switch (resultType) {
            case kDownloadDialogCancel: {
                return DOWNLOAD_DIALOG_RESULT_CANCEL;
            }
            case kDownloadDialogProceed: {
                return DOWNLOAD_DIALOG_RESULT_PROCEED;
            }
            default: {
                return DOWNLOAD_DIALOG_RESULT_UNKNOWN;
            }
        }
    }

    // ================================================================================
    // Public method - Level change
    // ================================================================================

    public static Map levelChangeJsonObject(ABALevel fromLevel, ABALevel toLevel) {
        Map mapWithLevelChange = new HashMap();
        if (fromLevel.getIdLevel() != null) {
            safePut(mapWithLevelChange, "Old level", fromLevel.getIdLevel());
        }
        if (toLevel.getIdLevel() != null) {
            safePut(mapWithLevelChange, "New level", toLevel.getIdLevel());
        }

        return mapWithLevelChange;
    }

    // ================================================================================
    // Public method - Linear Experience popup action
    // ================================================================================

    public static Map popupActionTypeJsonObject(GenericTrackingInterface.PopupActionType popupActionType) {

        Map mapWithPopupAction = new HashMap();
        if (popupActionType == GenericTrackingInterface.PopupActionType.kActionTypeDismiss) {
            safePut(mapWithPopupAction, "Action", GenericTrackingInterface.POPUP_ACTION_DISMISS);
        } else if (popupActionType == GenericTrackingInterface.PopupActionType.kActionTypeNext) {
            safePut(mapWithPopupAction, "Action", GenericTrackingInterface.POPUP_ACTION_NEXT);
        } else if (popupActionType == GenericTrackingInterface.PopupActionType.kActionTypeTimeOut) {
            safePut(mapWithPopupAction, "Action", GenericTrackingInterface.POPUP_ACTION_TIMEOUT);
        } else {
            safePut(mapWithPopupAction, "Action", GenericTrackingInterface.POPUP_ACTION_UNKNOWN);
        }

        return mapWithPopupAction;
    }

    // ================================================================================
    // Public method - Plans screen origin
    // ================================================================================

    public static Map plansOriginTypeJsonObject(GenericTrackingInterface.PlansViewControllerOriginType originType) {

        Map mapWithOriginType = new HashMap();
        if (originType == GenericTrackingInterface.PlansViewControllerOriginType.kPlansViewControllerOriginBanner) {
            safePut(mapWithOriginType, "Source", GenericTrackingInterface.ARRIVAL_TYPE_BANNER);
        } else if (originType == GenericTrackingInterface.PlansViewControllerOriginType.kPlansViewControllerOriginUnlockContent) {
            safePut(mapWithOriginType, "Source", GenericTrackingInterface.ARRIVAL_TYPE_LOCKED);
        } else if (originType == GenericTrackingInterface.PlansViewControllerOriginType.kPlansViewControllerOriginMenu) {
            safePut(mapWithOriginType, "Source", GenericTrackingInterface.ARRIVAL_TYPE_MENU);
        } else {
            safePut(mapWithOriginType, "Source", GenericTrackingInterface.ARRIVAL_TYPE_UNKNOWN);
        }

        return mapWithOriginType;
    }

    // ================================================================================
    // Public method - Notifications
    // ================================================================================

    public static Map notificationConfigurationJsonObject(boolean notificationsAreEnabled) {

        Map mapWithNotificationConfig = new HashMap();
        if (notificationsAreEnabled) {
            safePut(mapWithNotificationConfig, "Notification enabled", "yes");
        } else {
            safePut(mapWithNotificationConfig, "Notification enabled", "no");
        }

        return mapWithNotificationConfig;
    }

    // ================================================================================
    // Public method - ABASection
    // ================================================================================

    public static Map<String, Object> ABASectionTrackingProperties(Object section) {

        ABAUnit unitForSection = null;
        Map<String, Object> mapWithOriginType = new HashMap<>();
        try {
            Method getUnitMethod = section.getClass().getMethod("getUnit");
            unitForSection = (ABAUnit) getUnitMethod.invoke(section);
        } catch (NoSuchMethodException | InvocationTargetException | ClassCastException | IllegalAccessException e) {
            e.printStackTrace();
        }

        if (unitForSection != null) {
            if (unitForSection.getIdUnit() != null) {
                safePut(mapWithOriginType, "Unitid", unitForSection.getIdUnit());
            }

            safePut(mapWithOriginType, "Sectionid", SectionType.fromClass(section.getClass()).toString());
            safePut(mapWithOriginType, "Section name", section.getClass().getSimpleName());
        }

        return mapWithOriginType;
    }

    public static Map<String, Object> studyTimeJsonObject(double secondsOfStudy, double initialProgress, double finalProgress) {

        Map<String, Object> mapWithStudyTime = new HashMap<>();
        Double minutes = secondsOfStudy / 60.0;
        safePut(mapWithStudyTime, "Initial progress", Double.valueOf(initialProgress).toString());
        safePut(mapWithStudyTime, "Final progress", Double.valueOf(finalProgress).toString());
        safePut(mapWithStudyTime, "Minutes of study", minutes.toString());

        return mapWithStudyTime;
    }

    // ================================================================================
    // Public method - ABASection
    // ================================================================================

    public static Map<String, Object> ABAUnitTrackingProperties(String unitID) {

        Map<String, Object> mapWithUnit = new HashMap<>();
        if (unitID != null) {
            safePut(mapWithUnit, "Unitid", unitID);
        }

        return mapWithUnit;
    }

    // ================================================================================
    // Public method - Purchase
    // ================================================================================

    public static Map purchaseFromJsonObject(String productId, String price, String currency, String transactionID) {
        Map jsonObjectPurchase = new HashMap();
        safePut(jsonObjectPurchase, "Currency", currency);
        safePut(jsonObjectPurchase, "Price", price);
        safePut(jsonObjectPurchase, "Product", getProductPeriod(productId));
//        safePut(jsonObjectPurchase, "Transaction ID", transactionID);

        return jsonObjectPurchase;
    }

    public static Map ABAPlansTrackingPropierties(ABAPlan abaPlan) {
        return purchaseFromJsonObject(abaPlan.getDiscountIdentifier(), String.valueOf(abaPlan.getDiscountPrice()), abaPlan.getCurrency(), null);
    }


    // ================================================================================
    // Public methods - Error
    // ================================================================================

    public static Map errorJsonObject(String errorCategory, String tag, ABAUnit unit) {
        Map jsonObjectPurchase = new HashMap();
        safePut(jsonObjectPurchase, "Error category", errorCategory);
        safePut(jsonObjectPurchase, "Error tag", tag);
        if (unit != null) {
            safePut(jsonObjectPurchase, "Error unit id", unit.getIdUnit());
        }
        return jsonObjectPurchase;
    }

    public static Map phraseErrorURLJsonObject(String phraseUrl, ABAPhrase phrase) {
        Map mapWithPhrase = new HashMap();
        safePut(mapWithPhrase, "phraseID", phrase != null ? phrase.getIdPhrase() : "null");
        safePut(mapWithPhrase, "audioURL", phraseUrl);
        return mapWithPhrase;
    }

    // ================================================================================
    // Public methods - Device detection
    // ================================================================================

    public static Map deviceSuperproperties(Context context) {
        Map<String, String> mapDeviceProperties = new HashMap<>();
        if (APIManager.isTablet(context)) {
            mapDeviceProperties.put("Device type", "Android tablet");
        } else {
            mapDeviceProperties.put("Device type", "Android smartphone");
        }

        return mapDeviceProperties;
    }

    // ================================================================================
    // Public method - JSONObject
    // ================================================================================

    public static Map<String, Object> mergeMaps(Map<String, Object> obj1, Map<String, Object> obj2) {
        Map<String, Object> mergedMap = new HashMap<>();
        mergedMap.putAll(obj1);
        mergedMap.putAll(obj2);

        return mergedMap;
    }

    // ================================================================================
    // Private methods
    // ================================================================================

    private static String getProductPeriod(String productId) {
        if (productId.startsWith("1m")) {
            return "30";
        } else if (productId.startsWith("6m")) {
            return "180";
        } else if (productId.startsWith("12m")) {
            return "360";
        } else {
            return "0";
        }
    }

    public static String getProductName(String productId) {
        if (productId != null) {
            if (productId.startsWith("1m")) {
                return "1 month";
            } else if (productId.startsWith("6m")) {
                return "6 months";
            } else if (productId.startsWith("12m")) {
                return "12 months";
            }
        }
        return "Unknown";
    }

    private static String getUserTypeDescription(String userType) {
        // Free / Premium / Teacher (0-lead, Free-1, Premium-2, Plus-3, Teacher-4)
        if (userType.equalsIgnoreCase("2")) {
            return GenericTrackingInterface.USER_TYPE_PREMIUM;
        } else {
            return GenericTrackingInterface.USER_TYPE_FREE;
        }
    }

    private static final String MIXPANEL_DATE_PATTERN = "yyyy-MM-dd'T'HH:mm:ss";

    public static String currentMixpanelFormatedDate() {
        DateFormat dateFormatter = new SimpleDateFormat(MIXPANEL_DATE_PATTERN, Locale.US);
        dateFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date current = new Date();

        String currentDateString = dateFormatter.format(current);

        return currentDateString;
    }

    // ================================================================================
    // Private methods - JSONObject
    // ================================================================================

    /**
     * Method to automatically handle the JSONException so that it does not block the rest of the properties to add to the JSONObject
     *
     * @param mapObject map object to put given key and value
     * @param key       key for the given value
     * @param value     value for the given key
     */
    private static void safePut(Map mapObject, String key, Object value) {
        try {
            mapObject.put(key, value);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
