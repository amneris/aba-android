package com.abaenglish.videoclass.analytics.cooladata;

/**
 * Created by Jesus Espejo using mbp13jesus on 25/04/16.
 * Class with definitions on the Cooladata scope.
 */
public class CooladataTrackingDefinitions {

    protected static final String ANDROID_IDSITE = "3";
    protected static final String COOLADATA_PREFERENCES = "COOLADATA_PREFERENCES";
    protected static final String[] LOGIN_PROPERTY_KEYS = new String[]{"user_expiration_date", "user_user_type", "user_lang_id", "user_birthday", "user_level", "user_country_id", "user_partner_first_pay_id", "user_partner_source_id", "user_partner_current_id", "user_source_id", "user_device_type", "user_entry_date", "user_product", "user_conversion_date"};
    protected static final String USER_SCOPE = "{u}";

    /**
     * Enum to handle the section conversion and encoding
     */
    public enum CooladataSectionType {
        ABAFilm("1"),
        ABASpeak("2"),
        ABAWrite("3"),
        ABAInterpret("4"),
        ABAVideoClass("5"),
        ABAExercises("6"),
        ABAVocabulary("7"),
        ABAEvaluation("8");

        private final String name;

        CooladataSectionType(String s) {
            name = s;
        }

        public boolean equalsName(String otherName) {
            return otherName != null && name.equals(otherName);
        }

        public String toString() {
            return this.name;
        }
    }
}
