package com.abaenglish.videoclass.analytics.cooladata.progress;

import com.abaenglish.videoclass.analytics.cooladata.CooladataTrackingCommons;
import com.abaenglish.videoclass.analytics.cooladata.progress.model.ABACooladataProgressActionEvent;
import com.abaenglish.videoclass.analytics.cooladata.progress.parser.ABACooladataEventParser;
import com.abaenglish.videoclass.data.persistence.ABAUser;
import com.cooladata.android.CoolaDataTracker;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Jesus Espejo using mbp13jesus on 21/04/16.
 * Class to handle all the progress tracking related to Cooladata
 */
public class CooladataProgressTrackingManager {

    // ================================================================================
    // Progress actions and progress events
    // ================================================================================

    public static void sendUpdatedProgress(JSONObject progressUpdate, ABAUser currentUser) {

        try {
            // create event model with details of event fired
            ABACooladataProgressActionEvent abaEvent = ABACooladataEventParser.cooladataEventFromJson(progressUpdate, currentUser);

            // Tracking event
            trackProgress(abaEvent);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // ================================================================================
    // Private methods
    // ================================================================================

    private static void trackProgress(ABACooladataProgressActionEvent abaEvent) {

        Map<String, Object> eventProperties = abaEvent.getProperties().toMap();
        Map<String, Object> basicProperties = CooladataTrackingCommons.createBasicStudyMap();

        Map<String, Object> properties = new HashMap<>();
        properties.putAll(eventProperties);
        properties.putAll(basicProperties);

        // Tracking event
        CoolaDataTracker.trackEvent(abaEvent.getEventName(), properties);
    }
}
