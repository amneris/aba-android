package com.abaenglish.videoclass.analytics.cooladata.progress.model;

import com.abaenglish.videoclass.data.persistence.ABAUser;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Class for holding details regarding an aba event
 */
public class ABACooladataEventProperties {

    @SerializedName("event_timestamp_epoch")
    private long eventTimestampEpoch;

    @SerializedName("platform")
    private String platform = "mobile";

    @SerializedName("user_id")
    private String userId;

    @SerializedName("unit_id")
    private String unit;

    @SerializedName("section")
    private String section;

    @SerializedName("id_elem")
    private String idElem;

    @SerializedName("exercises")
    private String exercises;

    @SerializedName("time")
    private String time;

    @SerializedName("page")
    private String page;

    @SerializedName("idSite")
    private String idSite;

    @SerializedName("progress_ABA_Film")
    private Integer progressABAFilm = null;

    @SerializedName("progress_Speak")
    private Integer progressSpeak = null;

    @SerializedName("progress_Write")
    private Integer progressWrite = null;

    @SerializedName("progress_Interpret")
    private Integer progressInterpret = null;

    @SerializedName("progress_Video_Class")
    private Integer progressVideo_Class = null;

    @SerializedName("progress_Excercises")
    private Integer progressExcercises = null;

    @SerializedName("progress_Vocabulary")
    private Integer progressVocabulary = null;

    @SerializedName("progress_Assessment")
    private Integer progressAssessment = null;

    @SerializedName("progress_TotalPercentExcludingEvaluation")
    private Integer progressTotalPercentExcludingEvaluation = null;

    @SerializedName("progress_TotalPercent")
    private Integer progressTotalPercent = null;

    @SerializedName("lastchange")
    private String lastChange;

    @SerializedName("dateInit")
    private String dateInit;

    @SerializedName("device")
    private String device;

    // ================================================================================
    // Constructors
    // ================================================================================

    public ABACooladataEventProperties() {
    }

    public ABACooladataEventProperties(ABAUser abaUser) {
        this.userId = abaUser.getUserId();
    }

    // ================================================================================
    // Public methods - Conversion
    // ================================================================================

    /**
     * marshals current object to a json object
     *
     * @return
     * @throws JSONException
     */
    public JSONObject toJsonObject() throws JSONException {

        GsonBuilder gb = new GsonBuilder();
        //   gb.serializeNulls();
        Gson gson = gb.create();
        String json = gson.toJson(this);
        return new JSONObject(json);
    }

    public Map<String, Object> toMap() {
        try {
            return toMap(toJsonObject());
        } catch (Exception e) {
            return new HashMap<>();
        }
    }

    // ================================================================================
    // Private methods - Json to Map
    // ================================================================================

    private Map<String, Object> toMap(JSONObject object) throws JSONException {
        Map<String, Object> map = new HashMap<String, Object>();

        Iterator<String> keysItr = object.keys();
        while (keysItr.hasNext()) {
            String key = keysItr.next();
            Object value = object.get(key);

            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            map.put(key, value);
        }
        return map;
    }

    private List<Object> toList(JSONArray array) throws JSONException {
        List<Object> list = new ArrayList<Object>();
        for (int i = 0; i < array.length(); i++) {
            Object value = array.get(i);
            if (value instanceof JSONArray) {
                value = toList((JSONArray) value);
            } else if (value instanceof JSONObject) {
                value = toMap((JSONObject) value);
            }
            list.add(value);
        }
        return list;
    }


    // ================================================================================
    // Accessors
    // ================================================================================

    public void setEventTimestampEpoch(long eventTimestampEpoch) {
        this.eventTimestampEpoch = eventTimestampEpoch;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public void setSection(String section) {
        this.section = section;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public void setProgressABAFilm(int progressABAFilm) {
        this.progressABAFilm = progressABAFilm;
    }

    public void setProgressSpeak(int progressSpeak) {
        this.progressSpeak = progressSpeak;
    }

    public void setProgressWrite(int progressWrite) {
        this.progressWrite = progressWrite;
    }

    public void setProgressInterpret(int progressInterpret) {
        this.progressInterpret = progressInterpret;
    }

    public void setProgressVideoClass(int progressVideoClass) {
        this.progressVideo_Class = progressVideoClass;
    }

    public void setProgressExcercises(int progressExcercises) {
        this.progressExcercises = progressExcercises;
    }

    public void setProgressVocabulary(int progressVocabulary) {
        this.progressVocabulary = progressVocabulary;
    }

    public void setProgressAssessment(int progressAssessment) {
        this.progressAssessment = progressAssessment;
    }

    public void setProgressTotalPercentExcludingEvaluation(int progressTotalPercentExcludingEvaluation) {
        this.progressTotalPercentExcludingEvaluation = progressTotalPercentExcludingEvaluation;
    }

    public void setProgressTotalPercent(int progressTotalPercent) {
        this.progressTotalPercent = progressTotalPercent;
    }

    public void setIdSite(String idSite) {
        this.idSite = idSite;
    }

    public void setDateInit(String dateInit) {
        this.dateInit = dateInit;
    }

    public void setLastChange(String lastChange) {
        this.lastChange = lastChange;
    }

    public void setDevice(String device) {
        this.device = device;
    }
}
