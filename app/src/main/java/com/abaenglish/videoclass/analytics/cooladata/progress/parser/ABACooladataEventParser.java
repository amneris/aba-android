package com.abaenglish.videoclass.analytics.cooladata.progress.parser;

import android.annotation.SuppressLint;
import android.util.Log;

import com.abaenglish.videoclass.analytics.cooladata.progress.model.ABACooladataEventProperties;
import com.abaenglish.videoclass.analytics.cooladata.progress.model.ABACooladataProgressActionEvent;
import com.abaenglish.videoclass.data.persistence.ABAUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * Created by Jesus Espejo using mbp13jesus on 16/02/16.
 * Class to handle all the cooladata parsing needs.
 */
public class ABACooladataEventParser {

    private static final String PROGRESS_ACTION_EVENT_NAME = "PROGRESS_UPDATE_TOTAL";

    public static ABACooladataProgressActionEvent cooladataEventFromAction(Map<String, Object> actionDetails) throws JSONException {
        // get action name
        String actionName = actionName(actionDetails);
        ABACooladataEventProperties properties = jsonFromCooladataEventProperties(actionDetails);

        return new ABACooladataProgressActionEvent(actionName, properties);
    }
    
    public static ABACooladataProgressActionEvent cooladataEventFromJson(JSONObject progressUpdate, ABAUser currentUser) throws JSONException {
        Log.d("JSON",progressUpdate.toString(2));
        return new ABACooladataProgressActionEvent(PROGRESS_ACTION_EVENT_NAME, parseServerResponseForCooladataProperties(progressUpdate, currentUser));
    }

    // ================================================================================
    // Extracting info from Map<String, Object> actionDetails
    // ================================================================================

    private static String actionName(Map<String, Object> actionDetails) {
        return (String) actionDetails.get("action");
    }

    private static ABACooladataEventProperties jsonFromCooladataEventProperties(Map<String, Object> actionDetails) throws JSONException {
        ABACooladataEventProperties abaEventProperties = new ABACooladataEventProperties();
        abaEventProperties.setIdSite(actionDetails.get("idSite").toString());
        abaEventProperties.setUserId(actionDetails.get("idUser").toString());
        abaEventProperties.setDevice(actionDetails.get("device").toString());
        abaEventProperties.setDateInit(actionDetails.get("dateInit").toString());

        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date parsed = format.parse(actionDetails.get("dateInit").toString());
            abaEventProperties.setEventTimestampEpoch(parsed.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }


        return abaEventProperties;
    }

    private static ABACooladataEventProperties parseServerResponseForCooladataProperties(JSONObject progressUpdate, ABAUser currentUser) throws JSONException {

        Integer filmProgress = safeExtraction(progressUpdate, "AbaFilmPct");
        Integer speakProgress = safeExtraction(progressUpdate, "SpeakPct");
        Integer writeProgress = safeExtraction(progressUpdate, "WritePct");
        Integer interpretProgress = safeExtraction(progressUpdate, "InterpretPct");
        Integer videoclassProgress = safeExtraction(progressUpdate, "VideoClassPct");
        Integer exercisesProgress = safeExtraction(progressUpdate, "ExercisesPct");
        Integer vocabularyProgress = safeExtraction(progressUpdate, "VocabularyPct");
        Integer evaluationProgress = safeExtraction(progressUpdate, "AssessmentPct");
        Integer total = safeExtraction(progressUpdate, "Total");
        Integer totalWithEval = safeExtraction(progressUpdate, "totalWithEval");
        String unit = progressUpdate.optString("IdUnit");
        String lastchange = progressUpdate.optString("lastchange");

        ABACooladataEventProperties abaEventProperties = new ABACooladataEventProperties(currentUser);
        abaEventProperties.setProgressABAFilm(filmProgress);
        abaEventProperties.setProgressSpeak(speakProgress);
        abaEventProperties.setProgressWrite(writeProgress);
        abaEventProperties.setProgressInterpret(interpretProgress);
        abaEventProperties.setProgressVideoClass(videoclassProgress);
        abaEventProperties.setProgressExcercises(exercisesProgress);
        abaEventProperties.setProgressVocabulary(vocabularyProgress);
        abaEventProperties.setProgressAssessment(evaluationProgress);
        abaEventProperties.setProgressTotalPercentExcludingEvaluation(total);
        abaEventProperties.setProgressTotalPercent(totalWithEval);
        abaEventProperties.setLastChange(lastchange);
        abaEventProperties.setUnit(unit);

        return abaEventProperties;
    }

    // ================================================================================
    // Safe extracting integers
    // ================================================================================

    private static Integer safeExtraction(JSONObject progressUpdate, String key) throws JSONException {
        if (progressUpdate.has(key)) {
            String progress = progressUpdate.getString(key);
            try {
                return Integer.parseInt(progress);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }

        return 0;
    }
}
