package com.abaenglish.videoclass.analytics.cooladata;

import android.content.Context;
import android.content.SharedPreferences;

import com.abaenglish.videoclass.ABAApplication;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.abaenglish.videoclass.analytics.cooladata.CooladataTrackingDefinitions.ANDROID_IDSITE;
import static com.abaenglish.videoclass.analytics.cooladata.CooladataTrackingDefinitions.COOLADATA_PREFERENCES;
import static com.abaenglish.videoclass.analytics.cooladata.CooladataTrackingDefinitions.LOGIN_PROPERTY_KEYS;

/**
 * Created by Jesus Espejo using mbp13jesus on 25/04/16.
 * Class to handle all the shared methods among the other cooladata classes.
 */
public class CooladataTrackingCommons {

    // ================================================================================
    // Public methods - Properties
    // ================================================================================

    public static Map<String, Object> createBasicStudyMap() {
        Map<String, Object> mapWithBasicProperties = createBasicCooladataMap();
        // Add more properties to map type
        return mapWithBasicProperties;
    }

    public static Map<String, Object> createBasicProfileMap() {
        Map<String, Object> mapWithBasicProperties = createBasicCooladataMap();
        // Add more properties to map type
        return mapWithBasicProperties;
    }

    public static Map<String, Object> createBasicNavigationMap() {
        Map<String, Object> mapWithBasicProperties = createBasicCooladataMap();
        // Add more properties to map type
        return mapWithBasicProperties;
    }

    public static Map<String, Object> createBasicOtherMap() {
        Map<String, Object> mapWithBasicProperties = createBasicCooladataMap();
        // Add more properties to map type
        return mapWithBasicProperties;
    }

    // ================================================================================
    // Public methods - Login and register properties
    // ================================================================================

    public static void recordUserProperties(JSONObject jsonObject) {

        // First it removes previous properties
        wipeUserProperties();

        try {
            if (jsonObject.has("cooladata")) {
                JSONArray cooladataArray = jsonObject.getJSONArray("cooladata");
                for (int index = 0; index < cooladataArray.length(); index++) {
                    JSONObject cooladataJsonObject = (JSONObject) cooladataArray.get(index);
                    if (cooladataJsonObject.has("userScope")) {
                        JSONObject userScopeJSONObject = cooladataJsonObject.getJSONObject("userScope");

                        SharedPreferences prefs = ABAApplication.get().getSharedPreferences(COOLADATA_PREFERENCES, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = prefs.edit();

                        for (String propertyKey : LOGIN_PROPERTY_KEYS) {
                            try {
                                if (userScopeJSONObject.has(propertyKey)) {
                                    String propertyValue = userScopeJSONObject.getString(propertyKey);
                                    if (!propertyValue.equalsIgnoreCase("null")) {
                                        editor.putString(propertyKey, propertyValue);
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        editor.commit();
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ClassCastException e) {
            e.printStackTrace();
        }
    }

    public static Map<String, Object> readUserProperties() {
        SharedPreferences prefs = ABAApplication.get().getSharedPreferences(COOLADATA_PREFERENCES, Context.MODE_PRIVATE);
        Map<String, Object> loginProperties = new HashMap<>();
        for (String propertyKey : LOGIN_PROPERTY_KEYS) {
            String propertyWithScope = CooladataTrackingDefinitions.USER_SCOPE + propertyKey;
            loginProperties.put(propertyWithScope, prefs.getString(propertyKey, ""));
        }
        return loginProperties;
    }

    public static void wipeUserProperties() {
        SharedPreferences prefs = ABAApplication.get().getSharedPreferences(COOLADATA_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        for (String propertyKey : LOGIN_PROPERTY_KEYS) {
            editor.putString(propertyKey, null);
        }
        editor.commit();
    }

    // ================================================================================
    // Private methods
    // ================================================================================

    private static Map<String, Object> createBasicCooladataMap() {
        Map<String, Object> mapWithBasicProperties = new HashMap<>();
        mapWithBasicProperties.put("event_timestamp_epoch", new Date().getTime());
        mapWithBasicProperties.put("idSite", ANDROID_IDSITE);
        return mapWithBasicProperties;
    }
}
