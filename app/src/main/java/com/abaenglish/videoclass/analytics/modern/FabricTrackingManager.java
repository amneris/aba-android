package com.abaenglish.videoclass.analytics.modern;

import android.content.Context;

import com.abaenglish.videoclass.analytics.helpers.GenericTrackingHelper;
import com.abaenglish.videoclass.analytics.helpers.GenericTrackingInterface;
import com.abaenglish.videoclass.analytics.helpers.GenericTrackingInterface.RecordType;
import com.abaenglish.videoclass.data.persistence.ABALevel;
import com.abaenglish.videoclass.data.persistence.ABAPlan;
import com.abaenglish.videoclass.data.persistence.ABAUser;
import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.AddToCartEvent;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.AnswersEvent;
import com.crashlytics.android.answers.CustomEvent;
import com.crashlytics.android.answers.LoginEvent;
import com.crashlytics.android.answers.PurchaseEvent;
import com.crashlytics.android.answers.SignUpEvent;
import com.crashlytics.android.answers.StartCheckoutEvent;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Map;

import io.realm.Realm;

/**
 * Created by Jesus Espejo using mbp13jesus on 28/07/15.
 */
public class FabricTrackingManager {

    public static void trackLoginEvent(ABAUser currentUser) {
        Map userProperties = GenericTrackingHelper.ABAUserTrackingSuperProperties(currentUser);
        LoginEvent loginEvent = (LoginEvent) putFabricEventAttributes(new LoginEvent(), userProperties);
        Answers.getInstance().logLogin(loginEvent.putMethod("Login").putSuccess(true));
    }

    public static void trackFacebookLoginEvent(ABAUser currentUser) {
        Map userProperties = GenericTrackingHelper.ABAUserTrackingSuperProperties(currentUser);
        LoginEvent loginEvent = (LoginEvent) putFabricEventAttributes(new LoginEvent(), userProperties);
        Answers.getInstance().logLogin(loginEvent.putMethod("Login with Facebook").putSuccess(true));
    }

    public static void trackLoginOfflineEvent() {
        Answers.getInstance().logCustom(new CustomEvent("Login offline"));
    }

    public static void trackRegister(Context aContext, ABAUser currentUser, RecordType recordType) {
        // Translating record type
        String stringForRecordType = GenericTrackingHelper.attributeForRecordType(recordType);

        // Getting user properties
        Map userProperties = GenericTrackingHelper.ABAUserTrackingProperties(aContext, currentUser);

        // Creating event and using it
        SignUpEvent signUpEvent = (SignUpEvent) putFabricEventAttributes(new SignUpEvent(), userProperties);
        Answers.getInstance().logSignUp(signUpEvent.putMethod(stringForRecordType).putSuccess(true));
    }

    public static void trackLevelChange(ABALevel fromLevel, ABALevel toLevel) {
        Map levelChangeProperties = GenericTrackingHelper.levelChangeJsonObject(fromLevel, toLevel);
        CustomEvent levelChangeEvent = (CustomEvent) putFabricEventAttributes(new CustomEvent("Level change"), levelChangeProperties);
        Answers.getInstance().logCustom(levelChangeEvent);
    }

    public static void trackPricingShownWithOriginController(GenericTrackingInterface.PlansViewControllerOriginType originType) {
        Map originProperties = GenericTrackingHelper.plansOriginTypeJsonObject(originType);
        CustomEvent subscriptionShownEvent = (CustomEvent) putFabricEventAttributes(new CustomEvent("Subscription shown"), originProperties);
        Answers.getInstance().logCustom(subscriptionShownEvent);
    }

    public static void trackPricingSelected(ABAPlan selectedPlan) {
        Map trackingPricingProperties = GenericTrackingHelper.ABAPlansTrackingPropierties(selectedPlan);
        CustomEvent trackPricingSelected = (CustomEvent) putFabricEventAttributes(new CustomEvent("Subscription selected"), trackingPricingProperties);
        Answers.getInstance().logCustom(trackPricingSelected);

        Answers.getInstance().logAddToCart(new AddToCartEvent()
                .putItemPrice(BigDecimal.valueOf(selectedPlan.getDiscountPrice()))
                .putCurrency(Currency.getInstance(selectedPlan.getCurrency()))
                .putItemId(selectedPlan.getDiscountIdentifier())
                .putItemName(GenericTrackingHelper.getProductName(selectedPlan.getDiscountIdentifier())));

        Answers.getInstance().logStartCheckout(new StartCheckoutEvent()
                .putTotalPrice(BigDecimal.valueOf(selectedPlan.getDiscountPrice()))
                .putCurrency(Currency.getInstance(selectedPlan.getCurrency())));
    }

    public static void trackPurchase(String productIdentifier, String price, String currency, String transactionID) {

        double parsedPrice;
        try {
            parsedPrice = Double.parseDouble(price);
            Answers.getInstance().logPurchase(new PurchaseEvent()
                    .putItemPrice(BigDecimal.valueOf(parsedPrice))
                    .putCurrency(Currency.getInstance(currency))
                    .putItemId(productIdentifier)
                    .putItemName(GenericTrackingHelper.getProductName(productIdentifier))
                    .putSuccess(true));
        } catch (NumberFormatException e) {
            e.printStackTrace();
        } catch (Exception e) {
            Crashlytics.logException(e);
            e.printStackTrace();
        }
    }

    public static void trackSection(Realm realm, Object section, double secondsOfStudy, double initialProgress, double finalProgress) {
        // We cannot consider the user has studied under 30 seconds
        if (secondsOfStudy < GenericTrackingHelper.SECTION_TIME_THRESHOLD) {
            return;
        }

        Map<String, Object> jsonObjectStudyTime = GenericTrackingHelper.studyTimeJsonObject(secondsOfStudy, initialProgress, finalProgress);
        Map<String, Object> jsonObjectSectionProperties = GenericTrackingHelper.ABASectionTrackingProperties(section);
        Map mergedProperties = GenericTrackingHelper.mergeMaps(jsonObjectStudyTime, jsonObjectSectionProperties);

        CustomEvent sectionEvent = (CustomEvent) putFabricEventAttributes(new CustomEvent("Section"), mergedProperties);
        Answers.getInstance().logCustom(sectionEvent);
    }

    // ================================================================================
    // Private methods - (Attributes)
    // ================================================================================

    private static AnswersEvent putFabricEventAttributes(AnswersEvent event, Map attributeList) {
        for (Object key : attributeList.keySet()) {
            Object value = attributeList.get(key);
            if (value instanceof String) {
                event = event.putCustomAttribute((String) key, (String) value);
            } else if (value instanceof Number) {
                event = event.putCustomAttribute((String) key, (Double) value);
            }
        }

        return event;
    }
}
