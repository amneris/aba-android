package com.abaenglish.videoclass.analytics.cooladata.navigation;

import com.abaenglish.videoclass.analytics.cooladata.CooladataTrackingCommons;
import com.abaenglish.videoclass.analytics.cooladata.CooladataTrackingDefinitions.CooladataSectionType;
import com.abaenglish.videoclass.domain.LanguageController;
import com.cooladata.android.CoolaDataTracker;

import java.util.Map;

/**
 * Created by Jesus Espejo using mbp13jesus on 21/04/16.
 * Class to handle all the navigation tracking related to Cooladata
 */
public class CooladataNavigationTrackingManager {

    // ================================================================================
    // Login
    // ================================================================================

    public static void trackLoginEventWithOfflineFlag(boolean loginOfflineFlag, String userId) {
        // Reading user properties
        Map<String, Object> userProperties = CooladataTrackingCommons.readUserProperties();

        // Adding user properties to navigation properties
        Map<String, Object> loginProperties = CooladataTrackingCommons.createBasicNavigationMap();
        loginProperties.putAll(userProperties);
        loginProperties.put("user_id", userId);

        // Offline flag
        if (loginOfflineFlag) {
            loginProperties.put("type", "offline");
        } else {
            loginProperties.put("type", "regular");
        }

        // Adding original device language
        loginProperties.put("device_language", LanguageController.getOriginalLanguage());

        // Sending event
        CoolaDataTracker.trackEvent("LOGGED_IN", loginProperties);
    }

    // ================================================================================
    // Navigation
    // ================================================================================

    public static void trackEnteredCourseIndex(String userId, String levelId) {
        Map<String, Object> eventProperties = CooladataTrackingCommons.createBasicNavigationMap();
        eventProperties.put("user_id", userId);
        eventProperties.put("level_id", levelId);
        CoolaDataTracker.trackEvent("ENTERED_COURSE_INDEX", eventProperties);
    }

    public static void trackEnteredUnit(String userId, String levelId, String unitId) {
        Map<String, Object> eventProperties = CooladataTrackingCommons.createBasicNavigationMap();
        eventProperties.put("user_id", userId);
        eventProperties.put("level_id", levelId);
        eventProperties.put("unit_id", unitId);
        CoolaDataTracker.trackEvent("ENTERED_UNIT", eventProperties);
    }

    public static void trackEnteredSection(String userId, String levelId, String unitId, CooladataSectionType sectionId) {
        Map<String, Object> eventProperties = CooladataTrackingCommons.createBasicNavigationMap();
        eventProperties.put("user_id", userId);
        eventProperties.put("level_id", levelId);
        eventProperties.put("unit_id", unitId);
        eventProperties.put("section_id", sectionId.toString());
        CoolaDataTracker.trackEvent("ENTERED_SECTION", eventProperties);
    }

    public static void trackOpenedPrices(String userId) {
        Map<String, Object> eventProperties = CooladataTrackingCommons.createBasicNavigationMap();
        eventProperties.put("user_id", userId);
        CoolaDataTracker.trackEvent("OPENED_PRICES", eventProperties);
    }

    public static void trackOpenedHelp(String userId) {
        Map<String, Object> eventProperties = CooladataTrackingCommons.createBasicNavigationMap();
        eventProperties.put("user_id", userId);
        CoolaDataTracker.trackEvent("OPENED_HELP", eventProperties);
    }

    public static void trackSubscriptionSelected(String userId, int daysInPlan) {
        Map<String, Object> eventProperties = CooladataTrackingCommons.createBasicNavigationMap();
        eventProperties.put("user_id", userId);
        eventProperties.put("selected_plan", Integer.toString(daysInPlan));
        CoolaDataTracker.trackEvent("SELECTED_PRICE_PLAN", eventProperties);
    }

    // ================================================================================
    // Private methods
    // ================================================================================
}