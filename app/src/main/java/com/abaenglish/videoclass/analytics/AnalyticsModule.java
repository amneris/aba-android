package com.abaenglish.videoclass.analytics;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.realm.RealmConfiguration;

@Module
public class AnalyticsModule {

    private final Context context;

    public AnalyticsModule(Context context) {
        this.context = context;
    }

    @Singleton @Provides
    public UserInfoProvider providesUserInfoProvider(RealmConfiguration realmConfiguration) {
        return new UserInfoProviderImpl(realmConfiguration);
    }

    @Singleton @Provides
    public TrackerContract.Tracker providesTracker(GpsIdProvider gpsIdProvider,
                                                   UserInfoProvider userInfoProvider,
                                                   RealmConfiguration realmConfiguration) {
        return new FirebaseTracker(context, gpsIdProvider, userInfoProvider, realmConfiguration);
    }

}
