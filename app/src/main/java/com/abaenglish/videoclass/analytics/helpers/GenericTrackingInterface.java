package com.abaenglish.videoclass.analytics.helpers;

/**
 * Created by Jesus Espejo using mbp13jesus on 15/06/15.
 */
public interface GenericTrackingInterface {

    // ================================================================================
    // Registration
    // ================================================================================

    enum RecordType {
        kRecordTypeFacebook(1),
        kRecordTypeMail(2);

        RecordType(Integer b) {
            this.value = b;
        }

        private Integer value;
    }

    // User type definitions
    String USER_TYPE_PREMIUM = "Premium";
    String USER_TYPE_FREE = "Free";

    // Registration type definitions
    String REGISTRATION_TYPE_FACEBOOK = "Facebook";
    String REGISTRATION_TYPE_EMAIL = "Email";
    String REGISTRATION_UNKNOWN = "Registration unknown";

    // ================================================================================
    // Plan view controller
    // ================================================================================

    enum PlansViewControllerOriginType {
        kPlansViewControllerOriginBanner(1),
        kPlansViewControllerOriginUnlockContent(2),
        kPlansViewControllerOriginMenu(3);

        PlansViewControllerOriginType(Integer b) {
            this.value = b;
        }

        private Integer value;
    }

    // Plans screen origin type definitions
    String ARRIVAL_TYPE_BANNER = "Banner";
    String ARRIVAL_TYPE_LOCKED = "Locked section";
    String ARRIVAL_TYPE_MENU = "Menu";
    String ARRIVAL_TYPE_UNKNOWN = "Origin unknown";

    // ================================================================================
    // Download dialogs
    // ================================================================================

    enum DownloadDialogType {
        kDownloadDialogWifi(1),
        kDownloadDialogStorage(2);

        DownloadDialogType(Integer b) {
            this.value = b;
        }

        private Integer value;
    }

    // Download dialog type
    String DOWNLOAD_DIALOG_STORAGE = "Storage";
    String DOWNLOAD_DIALOG_WIFI = "WiFi";
    String DOWNLOAD_DIALOG_UNKNOWN = "Download dialog type unknown";

    enum DownloadDialogResultType {
        kDownloadDialogProceed(1),
        kDownloadDialogCancel(2);

        DownloadDialogResultType(Integer b) {
            this.value = b;
        }

        private Integer value;
    }

    String DOWNLOAD_DIALOG_RESULT_CANCEL = "Cancel";
    String DOWNLOAD_DIALOG_RESULT_PROCEED = "Proceed";
    String DOWNLOAD_DIALOG_RESULT_UNKNOWN = "Result unknown";


    // ================================================================================
    // Linear Experience
    // ================================================================================

    enum PopupActionType {
        kActionTypeDismiss(1),
        kActionTypeNext(2),
        kActionTypeTimeOut(3);

        PopupActionType(Integer b) {
            this.value = b;
        }

        private Integer value;
    }

    // Linear Experience popup action type definitions
    String POPUP_ACTION_DISMISS = "Dismiss";
    String POPUP_ACTION_NEXT = "Continue button";
    String POPUP_ACTION_TIMEOUT = "Time complete";
    String POPUP_ACTION_UNKNOWN = "Action unknown";

}
