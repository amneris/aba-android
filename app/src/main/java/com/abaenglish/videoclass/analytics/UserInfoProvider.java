package com.abaenglish.videoclass.analytics;

public interface UserInfoProvider {

    boolean refresh();

    String getUserId();

    String getUserLevel();

    String getUserLangId();

    String getUserBirthday();

    String getUserCountryId();

    String getUserType();

    String getUserEntryDate();

    String getUserExpirationDate();

    String getUserPartnerFirstPayId();

}
