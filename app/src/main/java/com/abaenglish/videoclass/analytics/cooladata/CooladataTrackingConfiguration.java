package com.abaenglish.videoclass.analytics.cooladata;

import android.content.Context;

import com.abaenglish.videoclass.R;
import com.cooladata.android.CoolaDataTracker;
import com.cooladata.android.CoolaDataTrackerOptions;

/**
 * Created by Jesus Espejo using mbp13jesus on 16/02/16.
 * Class to handle the configuration regarding Cooladata
 */
public class CooladataTrackingConfiguration {

    // ================================================================================
    // Initializer
    // ================================================================================

    public static void Initialize(Context aContext) {
        String cooladataSdkKey = aContext.getString(R.string.COOLADATA_TOKEN);
        CoolaDataTracker.setup(aContext.getApplicationContext(), new CoolaDataTrackerOptions(cooladataSdkKey));
    }

    // ================================================================================
    // Flush
    // ================================================================================

    public static void flush() {
        CoolaDataTracker.flush();
    }
}
