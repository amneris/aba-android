package com.abaenglish.videoclass.analytics.cooladata.profile;

import com.abaenglish.videoclass.analytics.cooladata.CooladataTrackingCommons;
import com.cooladata.android.CoolaDataTracker;

import java.util.Map;

/**
 * Created by Jesus Espejo using mbp13jesus on 25/04/16.
 * Class to handle all the Profile tracking related to Cooladata
 */
public class CooladataProfileTrackingManager {

    // ================================================================================
    // Register
    // ================================================================================

    public static void trackUserRegistered(String userId) {
        // Reading user properties
        Map<String, Object> userProperties = CooladataTrackingCommons.readUserProperties();

        // Adding user properties to navigation properties
        Map<String, Object> registerProperties = CooladataTrackingCommons.createBasicProfileMap();
        registerProperties.putAll(userProperties);
        registerProperties.put("user_id", userId);

        // Sending event
        CoolaDataTracker.trackEvent("USER_REGISTERED", registerProperties);
    }

    public static void trackLevelChange(String userId, String newLevelId) {
        Map<String, Object> eventProperties = CooladataTrackingCommons.createBasicProfileMap();
        eventProperties.put("user_id", userId);
        eventProperties.put("user_level", newLevelId);
        CoolaDataTracker.trackEvent("CHANGED_LEVEL", eventProperties);
    }

    public static void trackNotificationSwitchChange(String userId, boolean condition) {
        Map<String, Object> eventProperties = CooladataTrackingCommons.createBasicProfileMap();
        eventProperties.put("user_id", userId);
        eventProperties.put("conditon", condition);
        CoolaDataTracker.trackEvent("RECEIVE_NOTIFICATIONS", eventProperties);
    }

    public static void trackDeleteDownloads(String userId) {
        Map<String, Object> eventProperties = CooladataTrackingCommons.createBasicProfileMap();
        eventProperties.put("user_id", userId);
        CoolaDataTracker.trackEvent("DELETE_DOWNLOADS", eventProperties);
    }

    public static void trackOpenPricesFromBanner(String userId) {
        Map<String, Object> eventProperties = CooladataTrackingCommons.createBasicProfileMap();
        eventProperties.put("user_id", userId);
        CoolaDataTracker.trackEvent("PROFILE_OPEN_PRICES", eventProperties);
    }

}
