package com.abaenglish.videoclass.analytics.cooladata.abaMoments;

import com.abaenglish.videoclass.analytics.cooladata.CooladataTrackingCommons;
import com.cooladata.android.CoolaDataTracker;

import java.util.Map;

/**
 * Created by Jesus Espejo using mbp13jesus on 16/03/17.
 */

public class CooladataMomentsTrackingManager {

    private static final String OPENED_MOMENTS_LIST = "OPENED_MOMENTS_LIST";
    private static final String OPENED_MOMENTS_TYPE = "OPENED_MOMENT_TYPE";
    private static final String OPENED_MOMENT = "OPENED_MOMENT";
    private static final String STARTED_MOMENT = "STARTED_MOMENT";
    private static final String FINISHED_MOMENT = "FINISHED_MOMENT";
    private static final String STARTED_MOMENT_EXERCISE = "STARTED_MOMENT_EXERCISE";
    private static final String SELECTED_MOMENT_ANSWER = "SELECTED_MOMENT_ANSWER";
    private static final String CLOSED_MOMENT = "CLOSED_MOMENT";

    /**
     * Method to be used in the future, when we have several list of moment types
     *
     * @param userId
     */
    public static void trackMomentListOpened(String userId) {
        Map<String, Object> eventProperties = CooladataTrackingCommons.createBasicOtherMap();
        eventProperties.put("user_id", userId);
        CoolaDataTracker.trackEvent(OPENED_MOMENTS_LIST, eventProperties);
    }

    public static void trackMomentTypeOpened(String userId, String momentType) {
        Map<String, Object> eventProperties = CooladataTrackingCommons.createBasicOtherMap();
        eventProperties.put("user_id", userId);
        eventProperties.put("moment_type", momentType);
        CoolaDataTracker.trackEvent(OPENED_MOMENTS_TYPE, eventProperties);
    }

    public static void trackMomentOpened(String userId, String momentType, String momentId) {
        Map<String, Object> eventProperties = CooladataTrackingCommons.createBasicOtherMap();
        eventProperties.put("user_id", userId);
        eventProperties.put("moment_type", momentType);
        eventProperties.put("moment_id", momentId);
        CoolaDataTracker.trackEvent(OPENED_MOMENT, eventProperties);
    }

    public static void trackMomentStarted(String userId, String momentType, String momentId) {
        Map<String, Object> eventProperties = CooladataTrackingCommons.createBasicOtherMap();
        eventProperties.put("user_id", userId);
        eventProperties.put("moment_type", momentType);
        eventProperties.put("moment_id", momentId);
        CoolaDataTracker.trackEvent(STARTED_MOMENT, eventProperties);
    }

    public static void trackMomentFinished(String userId, String momentType, String momentId) {
        Map<String, Object> eventProperties = CooladataTrackingCommons.createBasicOtherMap();
        eventProperties.put("user_id", userId);
        eventProperties.put("moment_type", momentType);
        eventProperties.put("moment_id", momentId);
        CoolaDataTracker.trackEvent(FINISHED_MOMENT, eventProperties);
    }

    public static void trackMomentExercise(String userId, String momentType, String momentId, String exercise_id, String exercise_type) {
        Map<String, Object> eventProperties = CooladataTrackingCommons.createBasicOtherMap();
        eventProperties.put("user_id", userId);
        eventProperties.put("moment_type", momentType);
        eventProperties.put("moment_id", momentId);
        eventProperties.put("exercise_id", exercise_id);
        eventProperties.put("exercise_type", exercise_type);
        CoolaDataTracker.trackEvent(STARTED_MOMENT_EXERCISE, eventProperties);
    }

    public static void trackMomentAnswerSelected(String userId, String momentType, String momentId, String exerciseId, String exercise_type, String answerId, boolean isCorrect) {
        Map<String, Object> eventProperties = CooladataTrackingCommons.createBasicOtherMap();
        eventProperties.put("user_id", userId);
        eventProperties.put("moment_type", momentType);
        eventProperties.put("moment_id", momentId);
        eventProperties.put("exercise_id", exerciseId);
        eventProperties.put("exercise_type", exercise_type);
        eventProperties.put("answer_id", answerId);
        eventProperties.put("result", isCorrect ? "CORRECT" : "INCORRECT");
        CoolaDataTracker.trackEvent(SELECTED_MOMENT_ANSWER, eventProperties);
    }

    public static void trackMomentFinishedClosed(String userId, String momentType, String momentId) {
        Map<String, Object> eventProperties = CooladataTrackingCommons.createBasicOtherMap();
        eventProperties.put("user_id", userId);
        eventProperties.put("moment_type", momentType);
        eventProperties.put("moment_id", momentId);
        CoolaDataTracker.trackEvent(CLOSED_MOMENT, eventProperties);
    }
}
