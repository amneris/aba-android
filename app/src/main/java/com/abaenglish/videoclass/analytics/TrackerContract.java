package com.abaenglish.videoclass.analytics;


import com.abaenglish.videoclass.domain.content.PlanController;

import org.json.JSONObject;

import static com.abaenglish.videoclass.analytics.helpers.GenericTrackingInterface.DownloadDialogResultType;
import static com.abaenglish.videoclass.analytics.helpers.GenericTrackingInterface.DownloadDialogType;
import static com.abaenglish.videoclass.analytics.helpers.GenericTrackingInterface.PlansViewControllerOriginType;

public interface TrackerContract {

    String LOGGED_IN = "LOGGED_IN";
    String ENTERED_UNIT = "ENTERED_UNIT";
    String ENTERED_SECTION = "ENTERED_SECTION";
    String OPENED_HELP = "OPENED_HELP";
    String OPENED_PRICES = "OPENED_PRICES";
    String SELECTED_PRICE_PLAN = "SELECTED_PRICE_PLAN";
    String PAID = "PAID";
    String WATCHED_FILM = "WATCHED_FILM";
    String LISTENED_AUDIO = "LISTENED_AUDIO";
    String RECORDED_AUDIO = "RECORDED_AUDIO";
    String COMPARED_AUDIO = "COMPARED_AUDIO";
    String VERIFIED_TEXT = "VERIFIED_TEXT";
    String GOT_TEXT_SUGGESTION = "GOT_TEXT_SUGGESTION";
    String STARTED_INTERPRETATION = "STARTED_INTERPRETATION";
    String PROGRESS_UPDATE_TOTAL = "PROGRESS_UPDATE_TOTAL";
    String REVIEWED_TEST_RESULTS = "REVIEWED_TEST_RESULTS";
    String USER_REGISTERED = "USER_REGISTERED";
    String CHANGED_LEVEL = "CHANGED_LEVEL";
    String ENTERED_COURSE_INDEX = "ENTERED_COURSE_INDEX";

    interface Tracker extends UserPropertiesTracker, NavigationTracker, ProfileManagementTracker, StudyTracker,
            UnitContentDownloadTracker, SubscriptionsEventTracker, ScreensTracker, AppEventsTracker, CourseTracker {

        // Utility method to perform the PROGRESS_UPDATE_TOTAL event
        void trackProgressMap(JSONObject jsonObject);
    }

    interface UserPropertiesTracker {

        void addProperty(String name, String value);
    }

    interface NavigationTracker {

        void trackUserLogin(String userId, String signInMethod);

        void trackEnteredUnit(String levelId, String unitId);

        void trackEnteredSection(String levelId, String unitId, String sectionId);

        void trackEnteredCourseIndex(String levelId);

        void trackOpenedHelp();

        void trackOpenedPrices();

        void trackSelectedPlan(double amount, String currency, String discountIdentifier, String productDuration);

        void trackPaid(double amount, String currency, String discountIdentifier, String productDuration);
    }

    interface ProfileManagementTracker {

        void trackUserRegistered(String userId, String signInMethod);

        void trackChangeLevel(String userLevel);
    }

    interface StudyTracker {

        void trackWatchedFilm(String levelId, String unitId, String sectionId);

        void trackListenedAudio(String levelId, String unitId, String sectionId, String exerciseId);

        void trackRecordedAudio(String levelId, String unitId, String sectionId, String exerciseId);

        void trackComparedAudio(String levelId, String unitId, String sectionId, String exerciseId);

        void trackVerifiedText(String levelId, String unitId, String sectionId, String exerciseId, boolean result, String textWritten);

        void trackGotTextSuggestion(String levelId, String unitId, String sectionId, String exerciseId);

        void trackStartedInterpretation(String levelId, String unitId, String sectionId);

        void trackProgressUpdateTotal(String levelId, String unitId, String sectionId, int progress);

        void trackReviewedTestResults(String levelId, String unitId, String testResults, int result);
    }

    String DOWNLOAD_STARTED = "DOWNLOAD_STARTED";
    String DOWNLOAD_CANCELLED = "DOWNLOAD_CANCELLED";
    String DOWNLOAD_DIALOG = "DOWNLOAD_DIALOG";
    String DOWNLOAD_ERROR = "DOWNLOAD_ERROR";
    String DOWNLOAD_FINISHED = "DOWNLOAD_FINISHED";
    String DOWNLOADED_FILES_REMOVED = "DOWNLOADED_FILES_REMOVED";

    interface UnitContentDownloadTracker {

        void trackDownloadStarted(String levelId, String unitId);

        void trackDownloadCancelled(String levelId, String unitId);

        void trackDownloadDialog(String levelId, String unitId, DownloadDialogType dialogType, DownloadDialogResultType resultType);

        void trackDownloadError(String levelId, String unitId);

        void trackDownloadFinished(String levelId, String unitId);

        void trackDownloadedFilesRemoved(String levelId, String unitId);
    }

    String SUBSCRIPTION_ACTIVATED = "SUBSCRIPTION_ACTIVATED";
    String SUBSCRIPTION_STATUS = "SUBSCRIPTION_STATUS";
    String SUBSCRIPTION_STATUS_ACTIVATION_FAILED = "Subscription Status Activation Process Failed";
    String SUBSCRIPTION_STATUS_PURCHASE_ALREADY_ASSIGNED = "Subscription Status Purchase Already Assigned";
    String SUBSCRIPTION_STATUS_PROCESS_FAILED = "Subscription Status Purchase Process Failed";
    String SUBSCRIPTION_STATUS_ACTIVATION_PROCESS_FAILED = "Subscription Status Activation Process Failed";
    String SUBSCRIPTION_STATUS_VERIFICATION_FAILED = "Subscription Status Transaction Verification Failed";
    String SUBSCRIPTION_STATUS_NO_TRANSACTIONS = "Subscription Status Restore No Transactions";

    interface SubscriptionsEventTracker {

        void trackPurchaseActivated();

        void trackSubscriptionErrorWithSubscriptionStatus(PlanController.SubscriptionResult result);
    }

    String REGISTRATION_FORM_SHOWN = "REGISTRATION_FORM_SHOWN";
    String SUBSCRIPTION_SHOWN = "SUBSCRIPTION_SHOWN";

    interface ScreensTracker {

        void trackRegistrationShown();

        void trackSubscriptionShown(PlansViewControllerOriginType originType);
    }

    String FIRST_TIME_APP_OPEN = "FIRST_OPEN";
    String GOOGLE_PLAY_SERVICES_OUTDATED = "OUTDATED_GOOGLE_PLAY_SERVICES";
    String LOGOUT = "LOGOUT";
    String CONFIGURATION_CHANGED = "CONFIGURATION_CHANGED";

    interface AppEventsTracker {

        void trackFirstAppOpen();
        void trackGooglePlayServicesOutdated();
        void trackLogout();
        void trackConfigurationChanged();
    }

    String COURSE_FINISHED = "COURSE_FINISHED";

    interface CourseTracker {

        void trackCourseFinished();
    }
}
