package com.abaenglish.videoclass.analytics;

import android.content.Context;
import android.os.Bundle;

import com.abaenglish.videoclass.analytics.cooladata.navigation.CooladataNavigationTrackingManager;
import com.abaenglish.videoclass.analytics.helpers.GenericTrackingInterface;
import com.abaenglish.videoclass.analytics.helpers.GenericTrackingInterface.PlansViewControllerOriginType;
import com.abaenglish.videoclass.analytics.modern.MixpanelTrackingManager;
import com.abaenglish.videoclass.domain.content.PlanController;
import com.abaenglish.videoclass.domain.content.UserController;
import com.google.firebase.analytics.FirebaseAnalytics;

import org.json.JSONObject;

import io.realm.Realm;
import io.realm.RealmConfiguration;

public class FirebaseTracker implements TrackerContract.Tracker {

    public static final String SELECTED_PLAN = "selected_plan";
    public static final String LEVEL_ID = "level_id";
    public static final String UNIT_ID = "unit_id";
    public static final String SECTION_ID = "section_id";
    public static final String EXERCISE_ID = "exercise_id";
    public static final String TEXT_WRITTEN = "text_written";
    public static final String EXERCISE_RESULT = "result";

    public static final String GPS_ADID = "gps_adid";
    //    public static final String USER_PARTNER_FIRST_PAY_ID = "user_partner_first_pay_id";
    public static final String USER_PARTNER_FIRST_PAY_ID = "partner_first_pay_id";
    public static final String USER_EXPIRATION_DATE = "user_expiration_date";
    public static final String USER_ENTRY_DATE = "user_entry_date";
    public static final String USER_TYPE = "user_user_type";
    public static final String USER_COUNTRY_ID = "user_country_id";
    public static final String USER_BIRTHDAY = "user_birthday";
    public static final String USER_LANG_ID = "user_lang_id";
    public static final String USER_LEVEL = "user_level";
    public static final String RESPONSES = "responses";
    public static final String RESULT_SCORE = "result_score";
    public static final String PRICE_PAID = "price_paid";
    public static final String CURRENCY = "transaction_currency";
    public static final String DISCOUNT_APPLIED = "discount_applied";
    //    public static final String CURRENT_SECTION_PROGRESS = "progress_CurrentSectionProgress";
    public static final String CURRENT_SECTION_PROGRESS = "progress_CurrentSection";
    public static final String PRODUCT_BOUGHT = "product_bought";

    public static final String SOURCE = "SOURCE";

    private final GpsIdProvider gpsIdProvider;
    private final Context context;

    private FirebaseAnalytics firebaseAnalytics;
    private UserInfoProvider userInfoProvider;
    private static FirebaseTracker INSTANCE;

    private RealmConfiguration realmConfiguration;

    public FirebaseTracker(Context context, GpsIdProvider gpsIdProvider, UserInfoProvider userInfoProvider, RealmConfiguration realmConfiguration) {
        this.context = context;
        this.userInfoProvider = userInfoProvider;
        this.gpsIdProvider = gpsIdProvider;
        this.firebaseAnalytics = FirebaseAnalytics.getInstance(context);
        this.firebaseAnalytics.setAnalyticsCollectionEnabled(true);
        this.realmConfiguration = realmConfiguration;
        INSTANCE = this;
    }

    //Ugly singleton for SessionService
    public static FirebaseTracker getInstance() {
        return INSTANCE;
    }

    @Override
    public void trackUserLogin(String userId, String signInMethod) {
        firebaseAnalytics.setUserId(userId);
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.VALUE, signInMethod);
        firebaseAnalytics.logEvent(TrackerContract.LOGGED_IN, bundle);
    }

    @Override
    public void trackEnteredSection(String levelId, String unitId, String sectionId) {
        Bundle bundle = generateSectionBundle(levelId, unitId, sectionId);
        firebaseAnalytics.logEvent(TrackerContract.ENTERED_SECTION, bundle);
    }

    @Override
    public void trackEnteredCourseIndex(String levelId) {
        Bundle bundle = generateLevelBundle(levelId);
        firebaseAnalytics.logEvent(TrackerContract.ENTERED_COURSE_INDEX, bundle);
    }

    @Override
    public void trackEnteredUnit(String levelId, String unitId) {
        updateUserProperties();
        Bundle bundle = generateLevelUnitBundle(levelId, unitId);
        firebaseAnalytics.logEvent(TrackerContract.ENTERED_UNIT, bundle);
        trackEnteredUnitInCooladata(levelId, unitId);
    }

    private void trackEnteredUnitInCooladata(String levelId, String unitId) {
        Realm realm = Realm.getInstance(realmConfiguration);
        String userId = UserController.getInstance().getCurrentUser(realm).getUserId();
        realm.close();
        CooladataNavigationTrackingManager.trackEnteredUnit(userId, levelId, unitId);
    }

    @Override
    public void trackOpenedHelp() {
        firebaseAnalytics.logEvent(TrackerContract.OPENED_HELP, null);
    }

    @Override
    public void trackOpenedPrices() {
        firebaseAnalytics.logEvent(TrackerContract.OPENED_PRICES, null);
    }

    @Override
    public void trackSelectedPlan(double amount, String currency, String discountIdentifier, String productDuration) {
        Bundle bundle = new Bundle();
        bundle.putInt(SELECTED_PLAN, Integer.valueOf(productDuration));
        firebaseAnalytics.logEvent(TrackerContract.SELECTED_PRICE_PLAN, bundle);
        trackFirebaseBeginCheckout(productDuration, discountIdentifier, amount, currency);
    }

    @Override
    public void trackPaid(double amount, String currency, String discountIdentifier, String productDuration) {
        Bundle bundle = new Bundle();
        bundle.putDouble(PRICE_PAID, amount);
        bundle.putString(CURRENCY, currency);
        bundle.putString(DISCOUNT_APPLIED, discountIdentifier);
        bundle.putString(PRODUCT_BOUGHT, productDuration);
        bundle.putDouble(FirebaseAnalytics.Param.PRICE, amount);
        bundle.putString(FirebaseAnalytics.Param.CURRENCY, currency);
        firebaseAnalytics.logEvent(TrackerContract.PAID, bundle);
        trackFirebasePurchaseEvent(productDuration, discountIdentifier, amount, currency);
    }

    @Override
    public void trackUserRegistered(String userId, String signInMethod) {
        firebaseAnalytics.setUserId(userId);
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.VALUE, signInMethod);
        firebaseAnalytics.logEvent(TrackerContract.USER_REGISTERED, bundle);
    }

    @Override
    public void trackChangeLevel(String levelId) {
        Bundle bundle = generateLevelBundle(levelId);
        bundle.putString(FirebaseAnalytics.Param.VALUE, levelId);
        firebaseAnalytics.logEvent(TrackerContract.CHANGED_LEVEL, bundle);
    }

    @Override
    public void trackWatchedFilm(String levelId, String unitId, String sectionId) {
        Bundle bundle = generateSectionBundle(levelId, unitId, sectionId);
        bundle.putString(FirebaseAnalytics.Param.VALUE, unitId + "-" + sectionId);
        firebaseAnalytics.logEvent(TrackerContract.WATCHED_FILM, bundle);
    }

    @Override
    public void trackListenedAudio(String levelId, String unitId, String sectionId, String exerciseId) {
        Bundle bundle = generateExerciseBundle(levelId, unitId, sectionId, exerciseId);
        bundle.putString(FirebaseAnalytics.Param.VALUE, unitId + "-" + sectionId + "-" + exerciseId);
        firebaseAnalytics.logEvent(TrackerContract.LISTENED_AUDIO, bundle);
    }

    @Override
    public void trackRecordedAudio(String levelId, String unitId, String sectionId, String exerciseId) {
        Bundle bundle = generateExerciseBundle(levelId, unitId, sectionId, exerciseId);
        bundle.putString(FirebaseAnalytics.Param.VALUE, unitId + "-" + sectionId + "-" + exerciseId);
        firebaseAnalytics.logEvent(TrackerContract.RECORDED_AUDIO, bundle);
    }

    @Override
    public void trackComparedAudio(String levelId, String unitId, String sectionId, String exerciseId) {
        Bundle bundle = generateExerciseBundle(levelId, unitId, sectionId, exerciseId);
        bundle.putString(FirebaseAnalytics.Param.VALUE, unitId + "-" + sectionId + "-" + exerciseId);
        firebaseAnalytics.logEvent(TrackerContract.COMPARED_AUDIO, bundle);
    }

    @Override
    public void trackVerifiedText(String levelId, String unitId, String sectionId, String exerciseId, boolean result, String textWritten) {
        Bundle bundle = generateExerciseBundle(levelId, unitId, sectionId, exerciseId);
        bundle.putBoolean(EXERCISE_RESULT, result);
        bundle.putString(TEXT_WRITTEN, textWritten);
        bundle.putString(FirebaseAnalytics.Param.VALUE, unitId + "-" + sectionId + "-" + exerciseId);
        firebaseAnalytics.logEvent(TrackerContract.VERIFIED_TEXT, bundle);
    }

    @Override
    public void trackGotTextSuggestion(String levelId, String unitId, String sectionId, String exerciseId) {
        Bundle bundle = generateExerciseBundle(levelId, unitId, sectionId, exerciseId);
        bundle.putString(FirebaseAnalytics.Param.VALUE, unitId + "-" + sectionId + "-" + exerciseId);
        firebaseAnalytics.logEvent(TrackerContract.GOT_TEXT_SUGGESTION, bundle);
    }

    @Override
    public void trackStartedInterpretation(String levelId, String unitId, String sectionId) {
        Bundle bundle = generateSectionBundle(levelId, unitId, sectionId);
        bundle.putString(FirebaseAnalytics.Param.VALUE, unitId + "-" + sectionId);
        firebaseAnalytics.logEvent(TrackerContract.STARTED_INTERPRETATION, bundle);
    }

    @Override
    public void trackProgressUpdateTotal(String levelId, String unitId, String sectionId, int progress) {
        Bundle bundle = generateSectionBundle(levelId, unitId, sectionId);
        bundle.putInt(CURRENT_SECTION_PROGRESS, progress);
        bundle.putInt(FirebaseAnalytics.Param.VALUE, progress);
        firebaseAnalytics.logEvent(TrackerContract.PROGRESS_UPDATE_TOTAL, bundle);
    }

    @Override
    public void trackReviewedTestResults(String levelId, String unitId, String testResults, int result) {
        Bundle bundle = generateLevelUnitBundle(levelId, unitId);
        bundle.putString(RESPONSES, testResults);
        bundle.putInt(RESULT_SCORE, result);
        firebaseAnalytics.logEvent(TrackerContract.REVIEWED_TEST_RESULTS, bundle);
    }

    @Override
    public void addProperty(String name, String value) {
        firebaseAnalytics.setUserProperty(name, value);
    }

    @Override
    public void trackProgressMap(JSONObject jsonObject) {
        Integer filmProgress = jsonObject.optInt("AbaFilmPct", 0);
        Integer speakProgress = jsonObject.optInt("SpeakPct", 0);
        Integer writeProgress = jsonObject.optInt("WritePct", 0);
        Integer interpretProgress = jsonObject.optInt("InterpretPct", 0);
        Integer videoclassProgress = jsonObject.optInt("VideoClassPct", 0);
        Integer exercisesProgress = jsonObject.optInt("ExercisesPct", 0);
        Integer vocabularyProgress = jsonObject.optInt("VocabularyPct", 0);
        Integer evaluationProgress = jsonObject.optInt("AssessmentPct", 0);
        Integer total = jsonObject.optInt("Total", 0);
        Integer totalWithEval = jsonObject.optInt("totalWithEval", 0);
        String unit = jsonObject.optString("IdUnit");

        Bundle bundle = new Bundle();

        bundle.putInt("progress_ABA_Film", filmProgress);
        bundle.putInt("progress_Speak", speakProgress);
        bundle.putInt("progress_Write", writeProgress);
        bundle.putInt("progress_Interpret", interpretProgress);
        bundle.putInt("progress_Video_Class", videoclassProgress);
        bundle.putInt("progress_Excercises", exercisesProgress);
        bundle.putInt("progress_Vocabulary", vocabularyProgress);
        bundle.putInt("progress_Assessment", evaluationProgress);
        bundle.putInt("progress_TotalNoEvaluation", total);
        bundle.putInt("progress_TotalPercent", totalWithEval);
        bundle.putString(UNIT_ID, unit);

        this.firebaseAnalytics.logEvent(TrackerContract.PROGRESS_UPDATE_TOTAL, bundle);
    }

    private void trackFirebasePurchaseEvent(String productName, String couponId, double price, String currency) {
        Bundle bundle = generateFirebasePurchaseBundle(productName, couponId, price, currency);
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.ECOMMERCE_PURCHASE, bundle);
    }


    private void trackFirebaseBeginCheckout(String productName, String couponId, double price, String currency) {
        Bundle bundle = generateFirebasePurchaseBundle(productName, couponId, price, currency);
        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.BEGIN_CHECKOUT, bundle);
    }

    // ================================================================================
    // UnitContentDownloadTracker
    // ================================================================================

    public void trackDownloadStarted(String levelId, String unitId) {
        Bundle bundle = generateLevelUnitBundle(levelId, unitId);
        firebaseAnalytics.logEvent(TrackerContract.DOWNLOAD_STARTED, bundle);
    }

    public void trackDownloadCancelled(String levelId, String unitId) {
        Bundle bundle = generateLevelUnitBundle(levelId, unitId);
        firebaseAnalytics.logEvent(TrackerContract.DOWNLOAD_CANCELLED, bundle);
    }

    public void trackDownloadDialog(String levelId, String unitId, GenericTrackingInterface.DownloadDialogType dialogType, GenericTrackingInterface.DownloadDialogResultType resultType) {
        Bundle bundle = generateLevelUnitBundle(levelId, unitId);
        firebaseAnalytics.logEvent(TrackerContract.DOWNLOAD_DIALOG, bundle);
    }

    public void trackDownloadError(String levelId, String unitId) {

        Bundle bundle = generateLevelUnitBundle(levelId, unitId);
        firebaseAnalytics.logEvent(TrackerContract.DOWNLOAD_ERROR, bundle);
    }

    public void trackDownloadFinished(String levelId, String unitId) {

        Bundle bundle = generateLevelUnitBundle(levelId, unitId);
        firebaseAnalytics.logEvent(TrackerContract.DOWNLOAD_FINISHED, bundle);
    }

    public void trackDownloadedFilesRemoved(String levelId, String unitId) {

        Bundle bundle = generateLevelUnitBundle(levelId, unitId);
        firebaseAnalytics.logEvent(TrackerContract.DOWNLOADED_FILES_REMOVED, bundle);
    }

    // ================================================================================
    // SubscriptionTracker
    // ================================================================================

    public void trackPurchaseActivated() {
        firebaseAnalytics.logEvent(TrackerContract.SUBSCRIPTION_ACTIVATED, null);
    }

    public void trackSubscriptionErrorWithSubscriptionStatus(PlanController.SubscriptionResult result) {
        Bundle bundle = generateSubscriptionErrorBundle(result);
        firebaseAnalytics.logEvent(TrackerContract.SUBSCRIPTION_STATUS, bundle);
    }

    // ================================================================================
    // ScreensTracker
    // ================================================================================

    public void trackRegistrationShown() {
        firebaseAnalytics.logEvent(TrackerContract.REGISTRATION_FORM_SHOWN, null);
    }

    public void trackSubscriptionShown(PlansViewControllerOriginType originType) {
        Bundle nundle = generateOriginTypeBundle(originType);
        firebaseAnalytics.logEvent(TrackerContract.SUBSCRIPTION_SHOWN, nundle);
    }

    // ================================================================================
    // AppEventsTracker
    // ================================================================================

    public void trackFirstAppOpen() {
        firebaseAnalytics.logEvent(TrackerContract.FIRST_TIME_APP_OPEN, null);
    }

    public void trackGooglePlayServicesOutdated() {
        firebaseAnalytics.logEvent(TrackerContract.GOOGLE_PLAY_SERVICES_OUTDATED, null);
    }

    public void trackLogout() {
        firebaseAnalytics.logEvent(TrackerContract.LOGOUT, null);
    }

    public void trackConfigurationChanged() {
        firebaseAnalytics.logEvent(TrackerContract.CONFIGURATION_CHANGED, null);
    }

    // ================================================================================
    // CourseTracker
    // ================================================================================

    public void trackCourseFinished() {
        MixpanelTrackingManager.getSharedInstance(context).trackCourseFinished();
        firebaseAnalytics.logEvent(TrackerContract.COURSE_FINISHED, null);
    }

    // ================================================================================
    // Private methods
    // ================================================================================

    private void updateUserProperties() {
        if (userInfoProvider.refresh()) {
            firebaseAnalytics.setUserId(userInfoProvider.getUserId());
            addProperty(USER_LEVEL, userInfoProvider.getUserLevel());
            addProperty(USER_LANG_ID, userInfoProvider.getUserLangId());
            addProperty(USER_BIRTHDAY, userInfoProvider.getUserBirthday());
            addProperty(USER_COUNTRY_ID, userInfoProvider.getUserCountryId());
            addProperty(USER_TYPE, userInfoProvider.getUserType());
            addProperty(USER_ENTRY_DATE, userInfoProvider.getUserEntryDate());
            addProperty(USER_EXPIRATION_DATE, userInfoProvider.getUserExpirationDate());
            addProperty(USER_PARTNER_FIRST_PAY_ID, userInfoProvider.getUserPartnerFirstPayId());
        }
    }

    private Bundle generateLevelBundle(String levelId) {
        Bundle bundle = new Bundle();
        bundle.putString(LEVEL_ID, levelId);
        return bundle;
    }

    private Bundle generateLevelUnitBundle(String levelId, String unitId) {
        Bundle bundle = generateLevelBundle(levelId);
        bundle.putString(UNIT_ID, unitId);
        return bundle;
    }

    private Bundle generateSectionBundle(String levelId, String unitId, String sectionId) {
        Bundle bundle = generateLevelUnitBundle(levelId, unitId);
        bundle.putString(SECTION_ID, sectionId);
        return bundle;
    }

    private Bundle generateExerciseBundle(String levelId, String unitId, String sectionId, String exerciseId) {
        Bundle bundle = generateSectionBundle(levelId, unitId, sectionId);
        bundle.putString(EXERCISE_ID, exerciseId);
        return bundle;
    }

    private Bundle generateFirebasePurchaseBundle(String productName, String couponId, double price, String currency) {
        Bundle bundle = generateFirebaseProductBundle(productName, price, currency);
        bundle.putString(FirebaseAnalytics.Param.COUPON, couponId);
        return bundle;
    }

    private Bundle generateFirebaseProductBundle(String productName, double price, String currency) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.TRANSACTION_ID, productName);
        bundle.putDouble(FirebaseAnalytics.Param.PRICE, price);
        bundle.putString(FirebaseAnalytics.Param.CURRENCY, currency);
        return bundle;
    }

    private Bundle generateSubscriptionErrorBundle(PlanController.SubscriptionResult result) {
        Bundle bundle = new Bundle();
        switch (result) {
            case SUBSCRIPTION_RESTORE_GENERIC_KO: {
                bundle.putString(TrackerContract.SUBSCRIPTION_STATUS, TrackerContract.SUBSCRIPTION_STATUS_ACTIVATION_FAILED);
                break;
            }
            case SUBSCRIPTION_RESULT_ALREADY_ASSIGNED: {
                bundle.putString(TrackerContract.SUBSCRIPTION_STATUS, TrackerContract.SUBSCRIPTION_STATUS_PURCHASE_ALREADY_ASSIGNED);
                break;
            }
            case SUBSCRIPTION_RESULT_ERROR_FETCHING_SUBSCRIPTIONS: {
                bundle.putString(TrackerContract.SUBSCRIPTION_STATUS, TrackerContract.SUBSCRIPTION_STATUS_PROCESS_FAILED);
                break;
            }
            case SUBSCRIPTION_RESULT_KO: {
                bundle.putString(TrackerContract.SUBSCRIPTION_STATUS, TrackerContract.SUBSCRIPTION_STATUS_ACTIVATION_PROCESS_FAILED);
                break;
            }
            case SUBSCRIPTION_RESULT_KO_AT_ABA_API: {
                bundle.putString(TrackerContract.SUBSCRIPTION_STATUS, TrackerContract.SUBSCRIPTION_STATUS_VERIFICATION_FAILED);
                break;
            }
            case SUBSCRIPTION_RESULT_NO_SUBSCRIPTIONS: {
                bundle.putString(TrackerContract.SUBSCRIPTION_STATUS, TrackerContract.SUBSCRIPTION_STATUS_NO_TRANSACTIONS);
                break;
            }
        }
        return bundle;
    }

    private Bundle generateOriginTypeBundle(PlansViewControllerOriginType originType) {
        Bundle bundle = new Bundle();
        switch (originType) {
            case kPlansViewControllerOriginBanner: {
                bundle.putString(SOURCE, GenericTrackingInterface.ARRIVAL_TYPE_BANNER);
                break;
            }
            case kPlansViewControllerOriginMenu: {
                bundle.putString(SOURCE, GenericTrackingInterface.ARRIVAL_TYPE_MENU);
                break;
            }
            case kPlansViewControllerOriginUnlockContent: {
                bundle.putString(SOURCE, GenericTrackingInterface.ARRIVAL_TYPE_LOCKED);
                break;
            }
        }
        return bundle;
    }

}
