package com.abaenglish.videoclass.analytics.legacy;

import android.content.Context;

import com.abaenglish.videoclass.ABAApplication;
import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.data.persistence.ABAUser;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.adjust.sdk.Adjust;
import com.adjust.sdk.AdjustEvent;
import com.facebook.appevents.AppEventsLogger;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

import java.util.HashMap;

import io.realm.Realm;

/**
 * Created by Marc Güell Segarra on 18/6/15.
 * Modified by xilosada 8/4/2016
 */
public class LegacyTrackingManager {

    private static LegacyTrackingManager ourInstance;

    public static LegacyTrackingManager getInstance() {
        if (ourInstance == null)
            ourInstance = new LegacyTrackingManager();
        return ourInstance;
    }

    private HashMap<String, String> adjustEvents;

    private AppEventsLogger logger;

    private Context context;

    public static GoogleAnalytics analytics;
    public static Tracker tracker;

    private LegacyTrackingManager() {

        context = ABAApplication.get();

        adjustEvents = new HashMap<>();

        adjustEvents.put("registration", "tq4t6x");
        adjustEvents.put("interaction", "nkq7cy");
        adjustEvents.put("purchase", "ka94uz");
        adjustEvents.put("login", "mllsuk");

        logger = AppEventsLogger.newLogger(context);

        analytics = GoogleAnalytics.getInstance(context);
        analytics.setLocalDispatchPeriod(1800);

        tracker = analytics.newTracker(R.xml.global_tracker);
        tracker.enableExceptionReporting(true);
        tracker.enableAdvertisingIdCollection(true);
        tracker.enableAutoActivityTracking(true);
    }

    public void trackEvent(Realm realm, String event, String category, String label) {
        trackEvent(DataStore.getInstance().getUserController().getCurrentUser(realm), event, category, label);
    }

    public void trackEvent(ABAUser currentUser, String event, String category, String label) {

        if (adjustEvents.get(event) != null) {
            AdjustEvent adjustEvent = new AdjustEvent(adjustEvents.get(event));

            if (event.equals("registration")) {
                adjustEvent.addCallbackParameter("user_id", currentUser.getUserId());
            }

            Adjust.trackEvent(adjustEvent);
        }

        tracker.send(new HitBuilders.EventBuilder()
                .setCategory(category)
                .setAction(event)
                .setLabel(label)
                .build());

        logger.logEvent(event);
    }

    public void trackInteraction(ABAUser currentUser) {
        //GA
        tracker.send(new HitBuilders.EventBuilder().setAction("interaction").setCategory("interaction").setLabel("interacción de actividad en app - section").build());
        // Adjust
        trackEvent(currentUser, "interaction", "interaction", "interacción de actividad en app - section");
    }

    public void trackRegister(ABAUser currentUser) {
        //GA
        tracker.send(new HitBuilders.EventBuilder().setAction("registration").setCategory("register").setLabel("registro realizado correctamente").build());
        // Adjust
        trackEvent(currentUser, "registration", "register", "registro realizado correctamente");
    }

    public void trackLogin(ABAUser currentUser) {
        //GA
        tracker.send(new HitBuilders.EventBuilder().setAction("login").setCategory("login").setLabel("inicio de sesion satisfactorio").build());
        // Adjust
        trackEvent(currentUser, "login", "login", "inicio de sesion satisfactorio");
    }

    public void trackTransaction(String productIdentifier, String revenue, String currency, String transactionID) {
        // Parsing double. If error do nothing
        try {
            double parsedRevenue;
            parsedRevenue = Double.parseDouble(revenue);

            // Adjust
            AdjustEvent transactionEvent = new AdjustEvent(adjustEvents.get("purchase"));
            transactionEvent.setRevenue(parsedRevenue, currency);
            Adjust.trackEvent(transactionEvent);

            //GA
            tracker.send(new HitBuilders.TransactionBuilder().setTransactionId(transactionID).setRevenue(parsedRevenue).setTax(0).setShipping(0).setCurrencyCode(currency).setAffiliation("Android-web").build());
            tracker.send(new HitBuilders.ItemBuilder().setTransactionId(transactionID).setCurrencyCode(currency).setPrice(parsedRevenue).setSku(productIdentifier).setCategory("").setQuantity(1).build());
            tracker.send(new HitBuilders.EventBuilder().setCategory("payments").setAction(productIdentifier).build());

        } catch (NumberFormatException a) {
            a.printStackTrace();
            return;
        }
    }
}