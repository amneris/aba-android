package com.abaenglish.videoclass.analytics.helpers;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.content.WakefulBroadcastReceiver;

/**
 * Created by vmadalin on 21/12/15.
 */
public class GcmBroadcastReceiver extends WakefulBroadcastReceiver {

    public static String DEEPLINK_KEY = "deeplink_data";
    public static SharedPreferences receiveData;

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getExtras().get(DEEPLINK_KEY) != null) {
            receiveData = context.getSharedPreferences(DEEPLINK_KEY, Context.MODE_PRIVATE);
            receiveData.edit().putString(DEEPLINK_KEY, intent.getExtras().get(DEEPLINK_KEY).toString()).commit();
        }
    }
}
