package com.abaenglish.videoclass.analytics.cooladata.progress.model;

import org.json.JSONException;

public class ABACooladataProgressActionEvent {

    private String eventName;
    private ABACooladataEventProperties properties;

    public ABACooladataProgressActionEvent(String eventName, ABACooladataEventProperties eventProperties) throws JSONException {
        this.eventName = eventName;
        this.properties = eventProperties;
    }

    public String getEventName() {
        return eventName;
    }

    public ABACooladataEventProperties getProperties() {
        return properties;
    }
}
