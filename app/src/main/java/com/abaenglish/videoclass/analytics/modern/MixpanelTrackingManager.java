package com.abaenglish.videoclass.analytics.modern;

import android.app.Activity;
import android.content.Context;
import android.preference.PreferenceManager;
import android.util.Log;

import com.abaenglish.videoclass.ABAApplication;
import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.analytics.helpers.GenericTrackingHelper;
import com.abaenglish.videoclass.analytics.helpers.GenericTrackingInterface.DownloadDialogResultType;
import com.abaenglish.videoclass.analytics.helpers.GenericTrackingInterface.DownloadDialogType;
import com.abaenglish.videoclass.analytics.legacy.LegacyTrackingManager;
import com.abaenglish.videoclass.data.currency.CurrencyManager;
import com.abaenglish.videoclass.data.persistence.ABALevel;
import com.abaenglish.videoclass.data.persistence.ABAPlan;
import com.abaenglish.videoclass.data.persistence.ABAUser;
import com.abaenglish.videoclass.domain.content.PlanController.SubscriptionResult;
import com.crashlytics.android.Crashlytics;
import com.mixpanel.android.mpmetrics.MixpanelAPI;
import com.mixpanel.android.mpmetrics.OnMixpanelUpdatesReceivedListener;

import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

import static com.abaenglish.videoclass.analytics.helpers.GenericTrackingInterface.PlansViewControllerOriginType;
import static com.abaenglish.videoclass.analytics.helpers.GenericTrackingInterface.PopupActionType;
import static com.abaenglish.videoclass.analytics.helpers.GenericTrackingInterface.RecordType;

/**
 * Created by Jesus Espejo using mbp13jesus on 15/06/15.
 * Modified by xilosada on 28/04/2016
 */
public class MixpanelTrackingManager {

    private static final String PREFS_PLAY_SERVICES_OUTDATED = "com.abaenglish.videoclass.PREF_GPS_OOD";

    private final Context mAppContext;
    private MixpanelAPI mMixpanelAPI = null;
    private static MixpanelTrackingManager sharedInstance;

    private static final String GCM_SENDER_ID = "24106137087";
    private static final String UNKNOWN_ORIGINAL_PRICE = "Unknown original price";

    private MixpanelTrackingManager(Context context) {
        super();
        mAppContext = context.getApplicationContext();
        mMixpanelAPI = MixpanelAPI.getInstance(mAppContext, mAppContext.getString(R.string.MIXPANEL_TOKEN));
        mMixpanelAPI.registerSuperProperties(new JSONObject(GenericTrackingHelper.deviceSuperproperties(mAppContext)));

        Log.d("ABLISTROSCO", " ------------------------------------ UnitDetailABTestingConfigurator Constructor");
        mMixpanelAPI = MixpanelAPI.getInstance(ABAApplication.get(), ABAApplication.get().getString(R.string.MIXPANEL_TOKEN));
        mMixpanelAPI.getPeople().addOnMixpanelUpdatesReceivedListener(new OnMixpanelUpdatesReceivedListener() {
            @Override
            public void onMixpanelUpdatesReceived() {
                Log.d("ABLISTROSCO", " ------------------------------------ onMixpanelUpdatesReceived");
                mMixpanelAPI.getPeople().joinExperimentIfAvailable();
            }
        });
    }

    public synchronized static MixpanelTrackingManager getSharedInstance(Context context) {
        if (sharedInstance == null) {
            sharedInstance = new MixpanelTrackingManager(context);
        }
        return sharedInstance;
    }

    // ================================================================================
    // Public methods - Notifications
    // ================================================================================

    private void registerForGCM(ABAUser currentUser) {
        if (currentUser != null) {
            MixpanelAPI.People people = mMixpanelAPI.getPeople();
            people.identify(currentUser.getUserId());
            people.initPushHandling(GCM_SENDER_ID);
        }
    }

    // ================================================================================
    // Public methods - Library errors
    // ================================================================================

    /**
     * Send an event to Mixpanel when Google Play Services is outdated
     */
    public void trackGooglePlayServicesOutdated() {
        if (!isPlayServicesOutdatedVersionTracked()) {
            mMixpanelAPI.track("Google Play Services outdated");
            setPlayServicesOutdatedVersionTracked();
        }
    }

    public void resetGooglePlayServicesOutdated() {
        PreferenceManager.getDefaultSharedPreferences(mAppContext)
                .edit()
                .putBoolean(PREFS_PLAY_SERVICES_OUTDATED, false)
                .apply();
    }

    // ================================================================================
    // Private methods - Google Play Services Outdated track
    // ================================================================================

    private boolean isPlayServicesOutdatedVersionTracked() {
        return PreferenceManager
                .getDefaultSharedPreferences(mAppContext)
                .getBoolean(PREFS_PLAY_SERVICES_OUTDATED, false);
    }

    private void setPlayServicesOutdatedVersionTracked() {
        PreferenceManager.getDefaultSharedPreferences(mAppContext)
                .edit()
                .putBoolean(PREFS_PLAY_SERVICES_OUTDATED, true)
                .apply();
    }

    // ================================================================================
    // Public methods - Configuration
    // ================================================================================

    public void reset() {
        mMixpanelAPI.flush();
        mMixpanelAPI.reset();
        createABAUniqueIdentifier();
        mMixpanelAPI.flush();
    }

    public void createABAUniqueIdentifier() {
        String abaUniqueId = "ABA-" + mMixpanelAPI.getDistinctId() + "-" + new Date().getTime();
        mMixpanelAPI.identify(abaUniqueId);
    }

    // ================================================================================
    // Public methods - Download
    // ================================================================================

    public void trackDownloadStarted(String unitID) {
        mMixpanelAPI.track("Download started", new JSONObject(GenericTrackingHelper.ABAUnitTrackingProperties(unitID)));
    }

    public void trackDownloadCancelled(String unitID) {
        mMixpanelAPI.track("Download cancelled", new JSONObject(GenericTrackingHelper.ABAUnitTrackingProperties(unitID)));
    }

    public void trackDownloadDialog(String unitID, DownloadDialogType dialogType, DownloadDialogResultType resultType) {

        Map<String, Object> unitTrackingProperty = GenericTrackingHelper.ABAUnitTrackingProperties(unitID);
        Map<String, Object> cancelationTypeTrackingProperty = GenericTrackingHelper.downloadDismissedTrackingPropertiesObject(dialogType, resultType);

        Map<String, Object> trackingProperties = GenericTrackingHelper.mergeMaps(unitTrackingProperty, cancelationTypeTrackingProperty);
        mMixpanelAPI.trackMap("Download dialog", trackingProperties);
    }

    public void trackDownloadError(String unitID) {
        mMixpanelAPI.track("Download error", new JSONObject(GenericTrackingHelper.ABAUnitTrackingProperties(unitID)));
    }

    public void trackDownloadFinished(String unitID) {
        mMixpanelAPI.track("Download finished", new JSONObject(GenericTrackingHelper.ABAUnitTrackingProperties(unitID)));
    }

    public void trackDownloadedFilesRemoved(String unitID) {
        mMixpanelAPI.track("Downloaded files removed", new JSONObject(GenericTrackingHelper.ABAUnitTrackingProperties(unitID)));
    }

    // ================================================================================
    // Public methods - AB testing Linear Experience
    // ================================================================================

    public void trackLinearExpPopupAction(PopupActionType popupActionType) {
        mMixpanelAPI.track("Linear Experience popup", new JSONObject(GenericTrackingHelper.popupActionTypeJsonObject(popupActionType)));
    }

    // ================================================================================
    // Public methods - Login / register
    // ================================================================================

    public void trackFirstAppOpen() {
        mMixpanelAPI.track("First open", new JSONObject(GenericTrackingHelper.timestampTrackingPropertiesObject()));
        mMixpanelAPI.flush();
    }

    public void trackLoginEvent(RecordType recordType) {
        // Updating user properties
        mMixpanelAPI.getPeople().set(new JSONObject(GenericTrackingHelper.lastLoginDateUserProperties()));

        Map<String, Object> recordTypeProperties = GenericTrackingHelper.recordTypeTrackingPropertiesObject(recordType);
        Map<String, Object> timestampProperties = GenericTrackingHelper.timestampTrackingPropertiesObject();
        mMixpanelAPI.track("Login", new JSONObject(GenericTrackingHelper.mergeMaps(recordTypeProperties, timestampProperties)));

        mMixpanelAPI.getPeople().joinExperimentIfAvailable();
        Log.d("ABLISTROSCO", " ------------------------------------ joinExperimentIfAvailable");
    }

    public void trackLoginWithToken() {
        mMixpanelAPI.getPeople().joinExperimentIfAvailable();
    }

    public void trackLogout() {
        mMixpanelAPI.track("Logout");
        Log.d("ABLISTROSCO", " ------------------------------------ Logout");
    }

    public void trackLoginSuperproperties(Context aContext, ABAUser currentUser) {
        if (currentUser != null) {
            // People
            mMixpanelAPI.alias(currentUser.getUserId(), mMixpanelAPI.getDistinctId());
            mMixpanelAPI.identify(currentUser.getUserId());
            mMixpanelAPI.getPeople().identify(currentUser.getUserId());
            mMixpanelAPI.getPeople().set(new JSONObject(GenericTrackingHelper.ABAUserTrackingProperties(aContext, currentUser)));

            // Superproperties
            mMixpanelAPI.registerSuperProperties(new JSONObject(GenericTrackingHelper.ABAUserTrackingSuperProperties(currentUser)));

            // Registering user for GCM after login
            registerForGCM(currentUser);
        }
    }

    public void trackLoginOfflineEvent() {
        mMixpanelAPI.track("Login offline");
    }

    public void trackRegister(Context aContext, ABAUser currentUser, RecordType recordType) {
        if (currentUser != null) {
            // Profile
            mMixpanelAPI.alias(currentUser.getUserId(), mMixpanelAPI.getDistinctId());
            mMixpanelAPI.identify(mMixpanelAPI.getDistinctId());
            mMixpanelAPI.getPeople().identify(mMixpanelAPI.getDistinctId());

            mMixpanelAPI.getPeople().joinExperimentIfAvailable();
            Log.d("ABLISTROSCO", " ------------------------------------ joinExperimentIfAvailable");

            mMixpanelAPI.getPeople().set(new JSONObject(GenericTrackingHelper.ABAUserTrackingProperties(aContext, currentUser)));
            mMixpanelAPI.getPeople().set(new JSONObject(GenericTrackingHelper.registrationDateUserProperties()));

            // Event
            Map<String, Object> recordTypeProperties = GenericTrackingHelper.recordTypeTrackingPropertiesObject(recordType);
            Map<String, Object> timestampProperties = GenericTrackingHelper.timestampTrackingPropertiesObject();
            mMixpanelAPI.track("Register", new JSONObject(GenericTrackingHelper.mergeMaps(recordTypeProperties, timestampProperties)));

            // Super-properties from user
            mMixpanelAPI.registerSuperProperties(new JSONObject(GenericTrackingHelper.ABAUserTrackingSuperProperties(currentUser)));

            // Registering user for GCM after login
            registerForGCM(currentUser);
        }
    }

    // ================================================================================
    // Public methods - Analytics / Events
    // ================================================================================

    public void trackLevelChange(ABALevel fromLevel, ABALevel toLevel) {
        mMixpanelAPI.track("Level change", new JSONObject(GenericTrackingHelper.levelChangeJsonObject(fromLevel, toLevel)));
    }

    public void trackAppVariation(String key, String version) {
        Map<String, Object> experimentMap = new HashMap<>();
        experimentMap.put(key, version);

        // Tracking event property
        mMixpanelAPI.registerSuperPropertiesMap(experimentMap);

        // Tracking user property
        mMixpanelAPI.getPeople().setMap(experimentMap);
    }

    public void trackConfigurationChaged() {
        mMixpanelAPI.track("Configuration Changed");
    }

    public void trackPricingScreenArrivalWithOriginController(PlansViewControllerOriginType originType) {
        mMixpanelAPI.track("Subscription shown", new JSONObject(GenericTrackingHelper.plansOriginTypeJsonObject(originType)));
    }

    public void trackPricingSelected(ABAPlan abaPlan) {
        mMixpanelAPI.track("Subscription selected", new JSONObject(GenericTrackingHelper.ABAPlansTrackingPropierties(abaPlan)));
    }

    private void trackNotificationConfigurationChange() {
        mMixpanelAPI.track("Notification configuration change");
    }

    private void trackNotificationSuperproperties(boolean notificationsAreEnabled) {

        // Superproperties for event
        mMixpanelAPI.registerSuperProperties(new JSONObject(GenericTrackingHelper.notificationConfigurationJsonObject(notificationsAreEnabled)));

        // Superproperties for people
        mMixpanelAPI.getPeople().set(new JSONObject(GenericTrackingHelper.notificationConfigurationJsonObject(notificationsAreEnabled)));
    }

    public void trackSection(ABAUser currentUser, Object section, double secondsOfStudy, double initialProgress, double finalProgress) {

        // Threshold:
        // - 30 secs study time, or
        // - progress difference
        if (initialProgress == finalProgress && secondsOfStudy < GenericTrackingHelper.SECTION_TIME_THRESHOLD) {
            return;
        }

        Map<String, Object> mapObjectStudyTime = GenericTrackingHelper.studyTimeJsonObject(secondsOfStudy, initialProgress, finalProgress);
        Map<String, Object> mapObjectSectionProperties = GenericTrackingHelper.ABASectionTrackingProperties(section);
        JSONObject mergedJson = new JSONObject(GenericTrackingHelper.mergeMaps(mapObjectStudyTime, mapObjectSectionProperties));

        // Event
        mMixpanelAPI.track("Section", mergedJson);

        // Profile - sending hours of study and incrementing frequency of study
        double hoursOfStudy = secondsOfStudy / 3600.0; // From seconds to hours
        mMixpanelAPI.getPeople().increment("Hours of study", hoursOfStudy);
        mMixpanelAPI.getPeople().increment("Study session count", 1);

        // Also, we want to track Adjust event every time that section MixPanel event fires
        LegacyTrackingManager.getInstance().trackInteraction(currentUser);
    }

    // ================================================================================
    // Public methods - Purchases
    // ================================================================================

    public void trackPurchase(Context aContext, String productId, String price, String originalPrice, String currency, String transactionID) {
        try {
            double pricedInCurrency = Double.parseDouble(price);
            double priceInDollars = CurrencyManager.convertAmountToDollars(aContext, pricedInCurrency, currency);
            mMixpanelAPI.getPeople().trackCharge(priceInDollars, null);

            Map mapWithPurchaseInfo = new HashMap(GenericTrackingHelper.purchaseFromJsonObject(productId, price, currency, transactionID));
            mapWithPurchaseInfo.put("charged_price_eur", String.valueOf(CurrencyManager.convertAmountToEuros(aContext, pricedInCurrency, currency)));
            if (originalPrice != null && !originalPrice.equalsIgnoreCase("0.0")) {
                double originalPriceInCurrency = Double.parseDouble(originalPrice);
                mapWithPurchaseInfo.put("original_price_eur", String.valueOf(CurrencyManager.convertAmountToEuros(aContext, originalPriceInCurrency, currency)));
            } else {
                mapWithPurchaseInfo.put("original_price_eur", UNKNOWN_ORIGINAL_PRICE);
            }
            mMixpanelAPI.track("Subscription purchased", new JSONObject(mapWithPurchaseInfo));

        } catch (NumberFormatException | NoSuchElementException e) {
            Crashlytics.logException(e);
            e.printStackTrace();
        }
    }

    public void trackActivatedPurchase() {
        mMixpanelAPI.track("Subscription activated");
    }

    public void trackSubscriptionErrorWithSubscriptionStatus(SubscriptionResult result) {
        Map<String, Object> subscriptionMap = new HashMap<>();
        switch (result) {
            case SUBSCRIPTION_RESTORE_GENERIC_KO: {
                subscriptionMap.put("Subscription status", "Subscription Status Activation Process Failed");
                break;
            }
            case SUBSCRIPTION_RESULT_ALREADY_ASSIGNED: {
                subscriptionMap.put("Subscription status", "Subscription Status Purchase Already Assigned");
                break;
            }
            case SUBSCRIPTION_RESULT_ERROR_FETCHING_SUBSCRIPTIONS: {
                subscriptionMap.put("Subscription status", "Subscription Status Purchase Process Failed");
                break;
            }
            case SUBSCRIPTION_RESULT_KO: {
                subscriptionMap.put("Subscription status", "Subscription Status Activation Process Failed");
                break;
            }
            case SUBSCRIPTION_RESULT_KO_AT_ABA_API: {
                subscriptionMap.put("Subscription status", "Subscription Status Transaction Verification Failed");
                break;
            }
            case SUBSCRIPTION_RESULT_NO_SUBSCRIPTIONS: {
                subscriptionMap.put("Subscription status", "Subscription Status Restore No Transactions");
                break;
            }
            default: {
                break;
            }
        }

        mMixpanelAPI.track("Subscription attempt error", new JSONObject(subscriptionMap));
    }

    // ================================================================================
    // Public methods - IN-APP Notification / Surveys
    // ================================================================================

    public void showNotificationIfAvailable(Activity currentActivity) {
        mMixpanelAPI.getPeople().showNotificationIfAvailable(currentActivity);
    }

    public void showSurveyIfAvailable(Activity currentActivity) {
        mMixpanelAPI.getPeople().showSurveyIfAvailable(currentActivity);
    }

    // ================================================================================
    // Public methods - Registration
    // ================================================================================

    /**
     * Replacement of the old registrationFunnelShown event
     */
    public void trackRegistrationShown() {
        // Storing funnel super-properties
        Map<String, Object> timestampProperties = GenericTrackingHelper.timestampTrackingPropertiesObject();
        mMixpanelAPI.track("Registration form shown", new JSONObject(timestampProperties));
    }

    // ================================================================================
    // Public methods - Registration
    // ================================================================================

    public void trackCourseFinished() {
        // Storing funnel super-properties
        Map<String, Object> timestampProperties = GenericTrackingHelper.timestampTrackingPropertiesObject();
        mMixpanelAPI.track("Course finished", new JSONObject(timestampProperties));
    }


    // ================================================================================
    // Public methods - Flush
    // ================================================================================

    public void flush() {
        mMixpanelAPI.flush();
        Log.d("ABLISTROSCO", " ------------------------------------ Flush");
    }
}
