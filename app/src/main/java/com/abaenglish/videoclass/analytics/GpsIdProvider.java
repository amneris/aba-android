package com.abaenglish.videoclass.analytics;

import android.content.Context;
import android.os.AsyncTask;

import com.google.android.gms.ads.identifier.AdvertisingIdClient;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;

import java.io.IOException;

import javax.inject.Inject;

public class GpsIdProvider {

    private final Context context;

    @Inject
    public GpsIdProvider(Context context) {
        this.context = context;
    }

    public String getAdvertisingIdInfo() {
//        try {
//            return AdvertisingIdClient.getAdvertisingIdInfo(context).getId();
//        } catch (GooglePlayServicesNotAvailableException | GooglePlayServicesRepairableException | IOException e) {
//            e.printStackTrace();
//        }
        return "unknown";
    }

    private class GetGpsAdidTask extends AsyncTask<Void, Void, String> {

        private final Context context;

        public GetGpsAdidTask(Context context) {
            super();
            this.context = context;
        }

        @Override
        protected String doInBackground(Void... params) {
            try {
                return AdvertisingIdClient.getAdvertisingIdInfo(context).getId();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (GooglePlayServicesNotAvailableException e) {
                e.printStackTrace();
            } catch (GooglePlayServicesRepairableException e) {
                e.printStackTrace();
            }
            return "Unknown";
        }
    }
}
