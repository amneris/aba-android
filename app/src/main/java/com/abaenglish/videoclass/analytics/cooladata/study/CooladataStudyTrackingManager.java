package com.abaenglish.videoclass.analytics.cooladata.study;

import com.abaenglish.videoclass.analytics.cooladata.CooladataTrackingCommons;
import com.abaenglish.videoclass.analytics.cooladata.CooladataTrackingDefinitions.CooladataSectionType;
import com.cooladata.android.CoolaDataTracker;

import java.util.Map;

/**
 * Created by Jesus Espejo using mbp13jesus on 22/04/16.
 * Class to handle all the Study tracking related to Cooladata
 */
public class CooladataStudyTrackingManager {

    // ================================================================================
    // Public methods: Media
    // ================================================================================

    public static void trackWatchedFilm(String userId, String levelId, String unitId, CooladataSectionType sectionId) {
        Map<String, Object> eventProperties = CooladataTrackingCommons.createBasicStudyMap();
        eventProperties.put("user_id", userId);
        eventProperties.put("level_id", levelId);
        eventProperties.put("unit_id", unitId);
        eventProperties.put("section_id", sectionId.toString());
        CoolaDataTracker.trackEvent("WATCHED_FILM", eventProperties);
    }

    public static void trackListenedToAudio(String userId, String levelId, String unitId, CooladataSectionType sectionId, String exerciseId) {
        Map<String, Object> eventProperties = CooladataTrackingCommons.createBasicStudyMap();
        eventProperties.put("user_id", userId);
        eventProperties.put("level_id", levelId);
        eventProperties.put("unit_id", unitId);
        eventProperties.put("section_id", sectionId.toString());
        eventProperties.put("exercise_id", exerciseId);
        CoolaDataTracker.trackEvent("LISTENED_AUDIO", eventProperties);
    }

    public static void trackRecordedAudio(String userId, String levelId, String unitId, CooladataSectionType sectionId, String exerciseId) {
        Map<String, Object> eventProperties = CooladataTrackingCommons.createBasicStudyMap();
        eventProperties.put("user_id", userId);
        eventProperties.put("level_id", levelId);
        eventProperties.put("unit_id", unitId);
        eventProperties.put("section_id", sectionId.toString());
        eventProperties.put("exercise_id", exerciseId);
        CoolaDataTracker.trackEvent("RECORDED_AUDIO", eventProperties);
    }

    public static void trackComparedAudio(String userId, String levelId, String unitId, CooladataSectionType sectionId, String exerciseId) {
        Map<String, Object> eventProperties = CooladataTrackingCommons.createBasicStudyMap();
        eventProperties.put("user_id", userId);
        eventProperties.put("level_id", levelId);
        eventProperties.put("unit_id", unitId);
        eventProperties.put("section_id", sectionId.toString());
        eventProperties.put("exercise_id", exerciseId);
        CoolaDataTracker.trackEvent("COMPARED_AUDIO", eventProperties);
    }

    // ================================================================================
    // Public methods: Texts
    // ================================================================================

    public static void trackVerifiedText(String userId, String levelId, String unitId, CooladataSectionType sectionId, String exerciseId, boolean isResultCorrect, String writtenText) {
        Map<String, Object> eventProperties = CooladataTrackingCommons.createBasicStudyMap();
        eventProperties.put("user_id", userId);
        eventProperties.put("level_id", levelId);
        eventProperties.put("unit_id", unitId);
        eventProperties.put("section_id", sectionId.toString());
        eventProperties.put("exercise_id", exerciseId);
        eventProperties.put("result", isResultCorrect ? "CORRECT" : "INCORRECT");
        eventProperties.put("text_written", writtenText);
        CoolaDataTracker.trackEvent("VERIFIED_TEXT", eventProperties);
    }

    public static void trackTextSuggestion(String userId, String levelId, String unitId, CooladataSectionType sectionId, String exerciseId) {
        Map<String, Object> eventProperties = CooladataTrackingCommons.createBasicStudyMap();
        eventProperties.put("user_id", userId);
        eventProperties.put("level_id", levelId);
        eventProperties.put("unit_id", unitId);
        eventProperties.put("section_id", sectionId.toString());
        eventProperties.put("exercise_id", exerciseId);
        CoolaDataTracker.trackEvent("GOT_TEXT_SUGGESTION", eventProperties);
    }

    // ================================================================================
    // Public methods: Evaluation
    // ================================================================================

    public static void trackEvaluation(String userId, String levelId, String unitId, String responses, String score) {
        Map<String, Object> eventProperties = CooladataTrackingCommons.createBasicStudyMap();
        eventProperties.put("user_id", userId);
        eventProperties.put("level_id", levelId);
        eventProperties.put("unit_id", unitId);
        eventProperties.put("responses", responses);
        eventProperties.put("result_score", score);
        CoolaDataTracker.trackEvent("REVIEWED_TEST_RESULTS", eventProperties);
    }

    // ================================================================================
    // Public methods: Interpret
    // ================================================================================

    public static void trackInterpretationStarted(String userId, String levelId, String unitId, CooladataSectionType sectionId) {
        Map<String, Object> eventProperties = CooladataTrackingCommons.createBasicStudyMap();
        eventProperties.put("user_id", userId);
        eventProperties.put("level_id", levelId);
        eventProperties.put("unit_id", unitId);
        eventProperties.put("section_id", sectionId.toString());
        CoolaDataTracker.trackEvent("STARTED_INTERPRETATION", eventProperties);
    }

    // ================================================================================
    // Private methods
    // ================================================================================
}
