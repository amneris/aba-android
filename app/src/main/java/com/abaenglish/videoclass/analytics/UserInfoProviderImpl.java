package com.abaenglish.videoclass.analytics;

import com.abaenglish.videoclass.data.persistence.ABAUser;
import com.abaenglish.videoclass.domain.content.UserController;

import io.realm.Realm;
import io.realm.RealmConfiguration;

import static java.lang.String.valueOf;

public class UserInfoProviderImpl implements UserInfoProvider {

    private UserProfile userProfile;
    private RealmConfiguration realmConfiguration;

    public UserInfoProviderImpl(RealmConfiguration realmConfiguration) {
        this.realmConfiguration = realmConfiguration;
        loadProfile();
    }

    @Override
    public boolean refresh() {
        return loadProfile();
    }

    @Override
    public String getUserId() {
        return userProfile.userId;
    }

    @Override
    public String getUserLevel() {
        return userProfile.currentLevel;
    }

    @Override
    public String getUserLangId() {
        return userProfile.userLanguage;
    }

    @Override
    public String getUserBirthday() {
        return userProfile.birthDate;
    }

    @Override
    public String getUserCountryId() {
        return userProfile.country;
    }

    @Override
    public String getUserType() {
        return userProfile.userType;
    }

    @Override
    public String getUserEntryDate() {
        return userProfile.entryDate;
    }

    @Override
    public String getUserExpirationDate() {
        return userProfile.expirationDate;
    }

    @Override
    public String getUserPartnerFirstPayId() {
        return userProfile.partnerId;
    }

    private boolean loadProfile() {
        boolean refreshed;
        Realm realm = Realm.getInstance(realmConfiguration);
        ABAUser user = UserController.getInstance().getCurrentUser(realm);
        if (user != null) {
            userProfile = new UserProfile(user);
            refreshed = true;
        }
        else {
            refreshed = false;
        }
        realm.close();
        return refreshed;
    }

    private class UserProfile {

        private final String userId;
        private final String currentLevel;
        private final String userLanguage;
        private final String birthDate;
        private final String country;
        private final String userType;
        private final String entryDate;
        private final String expirationDate;
        private final String partnerId;

        UserProfile(ABAUser user) {
            userId = user.getUserId();
            currentLevel = valueOf(user.getCurrentLevel());
            userLanguage = user.getUserLang();
            birthDate = user.getBirthdate();
            country = user.getCountry();
            userType = user.getUserType();
            entryDate = "TBD";
            expirationDate = String.valueOf(user.getExpirationDate());
            partnerId = user.getPartnerID();
        }

    }
}
