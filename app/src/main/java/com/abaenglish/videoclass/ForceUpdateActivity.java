package com.abaenglish.videoclass;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.widget.Button;

import com.abaenglish.videoclass.presentation.base.ABAMasterActivity;
import com.abaenglish.videoclass.presentation.base.custom.ABAButton;
import com.abaenglish.videoclass.presentation.base.custom.ABATextView;
import com.abaenglish.videoclass.presentation.base.custom.FontCache;
import com.bzutils.BZScreenHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Jesus Espejo using mbp13jesus on 29/01/16.
 */
public class ForceUpdateActivity extends ABAMasterActivity {

    @BindView(R.id.force_update_text_view_1)
    ABATextView centralTextView1;
    @BindView(R.id.force_update_text_view_2)
    ABATextView centralTextView2;
    @BindView(R.id.goToStoreButton)
    Button goToStoreButton;

    public static Intent getCallingIntent(Context context) {
        return new Intent(context, ForceUpdateActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_force_update);
        ButterKnife.bind(this);
        configureUI();
    }

    @Override
    public void onBackPressed() {
        // Blocking back pressed
    }

    // ================================================================================
    // Private methods
    // ================================================================================
    protected void configureUI() {
        centralTextView1.setABATag(getResources().getInteger(R.integer.typefaceSlab500));
        centralTextView1.setTextSize(BZScreenHelper.dpFromPx(getResources().getDimension(R.dimen.tutorialTitleTextBigSize), getApplicationContext()));
        centralTextView2.setABATag(getResources().getInteger(R.integer.typefaceSlab500));
        centralTextView2.setTextSize(BZScreenHelper.dpFromPx(getResources().getDimension(R.dimen.tutorialTitleTextBigSize), getApplicationContext()));

        goToStoreButton.setTypeface(fontCache.getTypeface(FontCache.ABATypeface.rBold));
        if (Build.VERSION.SDK_INT >= 21) {
            goToStoreButton.setLetterSpacing(0.3f);
        }
    }

    // ================================================================================
    // Actions
    // ================================================================================

    @OnClick(R.id.goToStoreButton)
    protected void goToStore() {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + getPackageName())));
    }
}
