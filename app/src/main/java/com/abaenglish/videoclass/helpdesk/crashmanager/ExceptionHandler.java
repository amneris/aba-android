package com.abaenglish.videoclass.helpdesk.crashmanager;

/**
 * Created by adrian on 25/8/15.
 */

import com.bzutils.LogBZ;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.Thread.UncaughtExceptionHandler;

public class  ExceptionHandler implements UncaughtExceptionHandler {
    private boolean ignoreDefaultHandler = false;
    private CrashManagerListener listener;
    private UncaughtExceptionHandler defaultExceptionHandler;

    public ExceptionHandler(UncaughtExceptionHandler defaultExceptionHandler, CrashManagerListener listener, boolean ignoreDefaultHandler) {
        this.defaultExceptionHandler = defaultExceptionHandler;
        this.ignoreDefaultHandler = ignoreDefaultHandler;
        this.listener = listener;
    }

    public void setListener(CrashManagerListener listener) {
        this.listener = listener;
    }

    public static void saveException(Throwable exception, CrashManagerListener listener) {
        final Writer result = new StringWriter();
        final PrintWriter printWriter = new PrintWriter(result);
        exception.printStackTrace(printWriter);

        try {
            listener.onNewCrashesFound();

        } catch (Exception another) {
            LogBZ.e("Error saving exception stacktrace!" + another.getMessage());
        }
    }

    public void uncaughtException(Thread thread, Throwable exception) {

        exception.printStackTrace();

        saveException(exception, listener);
        if (!ignoreDefaultHandler) {
            defaultExceptionHandler.uncaughtException(thread, exception);
        } else {
            android.os.Process.killProcess(android.os.Process.myPid());
            System.exit(10);
        }
    }
}