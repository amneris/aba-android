package com.abaenglish.videoclass.helpdesk;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;

import com.abaenglish.videoclass.ABAApplication;
import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.zendesk.sdk.feedback.ZendeskFeedbackConfiguration;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

/**
 * Created by Jesus Espejo using mbp13jesus on 22/07/15.
 * Updated by Jesus Espejo on 10/08/16. Zendesk updated from 1.5.1.2 to 1.6.1.1
 */
public class ZendeskFeedbackConfigurationFactory {

    private static final String UNKNOWN_DEVICE_LANG = "UNKNOWN LANGUAGE";

    public static ZendeskFeedbackConfiguration getContactUsFeedbackConfiguration(final Context context) {
        return new ZendeskFeedbackConfiguration() {

            @Override
            public List<String> getTags() {
                List<String> tags = new ArrayList<>();
                tags.add("CONTACT FEEDBACK ANDROID");
                tags.add(userLocale());
                return tags;
            }

            @Override
            public String getAdditionalInfo() {
                return readDeviceAndAppInformation(context);
            }

            @Override
            public String getRequestSubject() {
                return context.getString(R.string.emailTitleHelpKey);
            }
        };
    }

    public static ZendeskFeedbackConfiguration getCrashFeedbackConfiguration(final Context context) {
        return new ZendeskFeedbackConfiguration() {
            @Override
            public List<String> getTags() {
                List<String> tags = new ArrayList<>();
                tags.add("CRASH FEEDBACK ANDROID");
                return tags;
            }

            @Override
            public String getAdditionalInfo() {
                return readDeviceAndAppInformation(context);
            }

            @Override
            public String getRequestSubject() {
                return context.getString(R.string.emailTitleHelpKey);
            }
        };
    }

    // ================================================================================
    // Private methods
    // ================================================================================

    private static String readDeviceAndAppInformation(Context context) {
        StringBuffer buffer = new StringBuffer();

        // Device info
        buffer.append("Version SDK: ").append(Build.VERSION.SDK_INT).append("\n");
        buffer.append("Device: ").append(Build.DEVICE).append("\n");
        buffer.append("Model: ").append(Build.MODEL).append("\n");
        buffer.append("Product: ").append(Build.PRODUCT).append("\n");
        buffer.append("Manufacturer: ").append(Build.MANUFACTURER).append("\n");

        // App info
        try {
            PackageManager manager = context.getPackageManager();
            PackageInfo info = manager.getPackageInfo(context.getPackageName(), 0);
            buffer.append("App version name: ").append(info.versionName).append("\n");
            buffer.append("App version code: ").append(info.versionCode).append("\n");
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        return buffer.toString();
    }

    private static String userLocale() {
        String uLocale = UNKNOWN_DEVICE_LANG;
        try {
            Realm realm = Realm.getInstance(ABAApplication.get().getRealmConfiguration());
            uLocale = DataStore.getInstance().getUserController().getCurrentUser(realm).getUserLang();
            realm.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return uLocale;
    }
}
