package com.abaenglish.videoclass.helpdesk.crashmanager;

import android.content.Context;

import java.lang.Thread.UncaughtExceptionHandler;
import java.lang.ref.WeakReference;

public class CrashManager {

    public static void register(Context context, CrashManagerListener listener) {
        initialize(context, listener);
        execute(context, listener);
    }

    public static void initialize(Context context, CrashManagerListener listener) {
        initialize(context, listener, true);
    }

    public static void execute(Context context, CrashManagerListener listener) {
        Boolean ignoreDefaultHandler = (listener != null) && (listener.ignoreDefaultHandler());
        WeakReference<Context> weakContext = new WeakReference<Context>(context);
        registerHandler(weakContext, listener, ignoreDefaultHandler);
    }

    private static void initialize(Context context, CrashManagerListener listener, boolean registerHandler) {
        if (context != null) {
            if (registerHandler) {
                Boolean ignoreDefaultHandler = (listener != null) && (listener.ignoreDefaultHandler());
                WeakReference<Context> weakContext = new WeakReference<Context>(context);
                registerHandler(weakContext, listener, ignoreDefaultHandler);
            }
        }
    }

    private static void registerHandler(WeakReference<Context> weakContext, CrashManagerListener listener, boolean ignoreDefaultHandler) {
        // Get current handler
        UncaughtExceptionHandler currentHandler = Thread.getDefaultUncaughtExceptionHandler();

        // Update listener if already registered, otherwise set new handler
        if (currentHandler instanceof ExceptionHandler) {
            ((ExceptionHandler) currentHandler).setListener(listener);
        } else {
            Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler(currentHandler, listener, ignoreDefaultHandler));
        }
    }

}