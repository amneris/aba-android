package com.abaenglish.videoclass.helpdesk;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import com.abaenglish.videoclass.ABAApplication;
import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.data.persistence.ABAUser;
import com.abaenglish.videoclass.domain.LanguageController;
import com.abaenglish.videoclass.domain.content.UserController;
import com.abaenglish.videoclass.presentation.base.ABAMasterFragment;
import com.abaenglish.videoclass.presentation.base.custom.ABATextView;
import com.crashlytics.android.Crashlytics;
import com.zendesk.sdk.feedback.ui.ContactZendeskActivity;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import com.zendesk.sdk.support.SupportActivity;

import java.util.Locale;

import io.realm.Realm;


public class ZendeskHelpCenterFragment extends ABAMasterFragment implements View.OnClickListener {

    private Button zendeskKnowlage;
    private Button zendeskRate;

    private static final String ZENDESK_MOBILE_LABEL = "mobile app";

    @Override
    protected int onResLayout() {
        return R.layout.fragment_zendesk_help_center;
    }

    @Override
    protected void onConfigView() {
        zendeskKnowlage = $(R.id.zendesk_knowledge);
        zendeskRate = $(R.id.zendesk_rate);

        zendeskRate.setOnClickListener(this);
        zendeskKnowlage.setOnClickListener(this);

        Crashlytics.log(Log.INFO, "Contact and Help", "User opens Contact and Help view");
    }

    @Override
    protected void onConfigToolbar(ABATextView toolbarTitle, ABATextView toolbarSubTitle, ImageButton toolbarButton) {
        toolbarTitle.setText(getString(R.string.menuKey));
        toolbarSubTitle.setText(getString(R.string.helpAndContactKey));
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.zendesk_knowledge: {

                Realm realm = Realm.getInstance(ABAApplication.get().getRealmConfiguration());
                ABAUser currentUser = UserController.getInstance().getCurrentUser(realm);
                String currentUserEmail = currentUser.getEmail();
                String currentUserLanguage = currentUser.getUserLang();
                realm.close();

                Intent intent = new Intent(getActivity(), InbentaActivity.class);
                intent.putExtra(InbentaActivity.EMAIL_PARAM_KEY, currentUserEmail);
                intent.putExtra(InbentaActivity.LANGUAGE_PARAM_KEY, currentUserLanguage);
                startActivity(intent);
                break;
            }
            case R.id.zendesk_rate: {
                // TODO: DIRECTLY TO THE PLAY STORE
                Uri uri = Uri.parse("market://details?id=" + getActivity().getPackageName());
                Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                try {
                    startActivity(goToMarket);
                } catch (ActivityNotFoundException e) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + getActivity().getPackageName())));
                }
                break;
            }
        }
    }

}
