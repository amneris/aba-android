package com.abaenglish.videoclass.helpdesk;

import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.abaenglish.videoclass.ABAApplication;
import com.abaenglish.videoclass.BuildConfig;
import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.domain.LanguageController;
import com.abaenglish.videoclass.presentation.base.ABAMasterActivity;
import com.abaenglish.videoclass.presentation.base.custom.ABATextView;

import java.util.ArrayList;
import java.util.List;

public class InbentaActivity extends ABAMasterActivity {

    private static final String INBENTA_URL = "http://abaenglish-app-faqs.inbenta.com/";
    private static final String DEFAULT_LANGUAGE = "en";
    private static final List<String> LIST_OF_LANGUAGES = new ArrayList<String>() {{
        add("es");
        add("de");
        add("en");
        add("ru");
        add("it");
        add("pt");
        add("fr");
    }};

    public static final String EMAIL_PARAM_KEY = "EMAIL_PARAM_KEY";
    public static final String LANGUAGE_PARAM_KEY = "LANGUAGE_PARAM_KEY";

    private WebView myWebView;
    private RelativeLayout mainLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inbenta_webview);
        configActionBar();

        myWebView = (WebView) findViewById(R.id.webView);
        mainLayout = (RelativeLayout) findViewById(R.id.inbenta_main_layout);

        String email = getIntent().getStringExtra(EMAIL_PARAM_KEY);
        String language = getIntent().getStringExtra(LANGUAGE_PARAM_KEY);
        configureWebViewWithEmail(email, language);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        LanguageController.currentLanguageOverride();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // Check if the key event was the Back button and if there's history
        if ((keyCode == KeyEvent.KEYCODE_BACK) && myWebView.canGoBack()) {
            myWebView.goBack();
            return true;
        }

        // If it wasn't the Back key or there's no web page history, bubble up to the default
        // system behavior (probably exit the activity)
        return super.onKeyDown(keyCode, event);
    }


    // ================================================================================
    // Configuration
    // ================================================================================

    protected void configureWebViewWithEmail(String emailAddress, String language) {

        if (!LIST_OF_LANGUAGES.contains(language)) {
            language = DEFAULT_LANGUAGE;
        }

        //enable Javascript
        myWebView.getSettings().setJavaScriptEnabled(true);

        //loads the WebView completely zoomed out
        myWebView.getSettings().setLoadWithOverviewMode(true);

        //true makes the Webview have a normal viewport such as a normal desktop browser
        //when false the webview will have a viewport constrained to it's own dimensions
        myWebView.getSettings().setUseWideViewPort(true);

        //override the web client to open all links in the same webview
        myWebView.setWebViewClient(new WebViewClient());
        myWebView.getSettings().setJavaScriptEnabled(true);
        myWebView.getSettings().setAllowFileAccessFromFileURLs(true); //Maybe you don't need this rule
        myWebView.getSettings().setAllowUniversalAccessFromFileURLs(true);
        myWebView.setWebChromeClient(new WebChromeClient());

        // Web client
        myWebView.setWebViewClient(new OnReceivedErrorWebClient());

        String urlToLoad = constructURLString(emailAddress, language);
        myWebView.loadUrl(urlToLoad);
    }

    protected String constructURLString(String emailAddress, String language) {
        int androidVersion = android.os.Build.VERSION.SDK_INT;
        String appVersion = BuildConfig.VERSION_NAME + "(" + BuildConfig.VERSION_CODE + ")";
        String reqString = Build.MANUFACTURER + " " + Build.MODEL;

        StringBuilder builder = new StringBuilder(INBENTA_URL + "/" + language + "/?");

        // Adding email
        if (emailAddress != null) {
            builder.append("&user=" + emailAddress);
        }

        // Adding device version
        builder.append("&deviceVersion=" + androidVersion);

        // Adding device name
        if (reqString != null) {
            builder.append("&deviceInfo=" + reqString.replaceAll("\\s+", ""));
        }

        // Adding app version
        if (appVersion != null) {
            builder.append("&appVersion=" + appVersion.replaceAll("\\s+", ""));
        }

        return builder.toString();
    }

    protected void configActionBar() {
        findViewById(R.id.toolbarLeftButton).setOnTouchListener(((ABAApplication) getApplicationContext()).getImagePressedState(null));
        findViewById(R.id.toolbarLeftButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ABATextView titleTextView = (ABATextView) findViewById(R.id.toolbarTitle);
        ABATextView subtitleTextView = (ABATextView) findViewById(R.id.toolbarSubTitle);

        titleTextView.setText(getResources().getText(R.string.helpAndContactKey));
        subtitleTextView.setText(getResources().getText(R.string.help_center_main_btn_knowledge_base));
    }

    // ================================================================================
    // Private classes
    // ================================================================================

    private class OnReceivedErrorWebClient extends WebViewClient {

        private boolean isErrorShown = false;

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
            showErrorOnce();
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);
            showErrorOnce();
        }

        // ================================================================================
        // Private methods
        // ================================================================================

        private void showErrorOnce() {

            if (!isErrorShown) {
                isErrorShown = true;

                myWebView.setVisibility(View.GONE);

                // Show message
                Snackbar snackbar = Snackbar.make(mainLayout, getResources().getString(R.string.getAllSectionsForUnitErrorKey), Snackbar.LENGTH_INDEFINITE)
                        .setAction("OK", new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                finish();
                            }
                        });
                TextView textView = (TextView) snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);
                textView.setMaxLines(5);  // show multiple line
                snackbar.show();
            }
        }
    }
}
