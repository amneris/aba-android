package com.abaenglish.videoclass.helpdesk.crashmanager;


public abstract class CrashManagerListener {

    public boolean ignoreDefaultHandler() {
        return false;
    }

    public String getContact() {
        return null;
    }

    public void onNewCrashesFound() {
    }

}
