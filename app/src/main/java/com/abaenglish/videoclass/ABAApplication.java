package com.abaenglish.videoclass;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PorterDuff;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.support.v4.content.ContextCompat;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.abaenglish.shepherd.ABAShepherdEditor;
import com.abaenglish.shepherd.configuration.configurators.GenericShepherdConfigurator;
import com.abaenglish.shepherd.configuration.configurators.abacore.ABACoreShepherdEnvironment;
import com.abaenglish.videoclass.analytics.AnalyticsModule;
import com.abaenglish.videoclass.analytics.TrackerContract;
import com.abaenglish.videoclass.analytics.cooladata.CooladataTrackingConfiguration;
import com.abaenglish.videoclass.data.file.ImageDecoder;
import com.abaenglish.videoclass.data.network.APIManager;
import com.abaenglish.videoclass.data.persistence.migration.ABARealmMigration;
import com.abaenglish.videoclass.domain.ApplicationConfiguration;
import com.abaenglish.videoclass.domain.ProgressActionThread;
import com.abaenglish.videoclass.domain.abTesting.ServerConfigImpl;
import com.abaenglish.videoclass.helpdesk.crashmanager.CrashManager;
import com.abaenglish.videoclass.helpdesk.crashmanager.CrashManagerListener;
import com.abaenglish.videoclass.presentation.base.custom.ABATextView;
import com.abaenglish.videoclass.presentation.di.ApplicationComponent;
import com.abaenglish.videoclass.presentation.di.ApplicationModule;
import com.abaenglish.videoclass.presentation.di.DaggerApplicationComponent;
import com.abaenglish.videoclass.presentation.section.assessment.DaggerEvaluationComponent;
import com.abaenglish.videoclass.presentation.section.assessment.EvaluationComponent;
import com.abaenglish.videoclass.presentation.section.assessment.EvaluationModule;
import com.abaenglish.videoclass.presentation.section.assessment.result.AssessmentResultComponent;
import com.abaenglish.videoclass.presentation.section.assessment.result.AssessmentResultModule;
import com.abaenglish.videoclass.presentation.section.assessment.result.DaggerProductionAssessmentResultComponent;
import com.adjust.sdk.Adjust;
import com.adjust.sdk.AdjustConfig;
import com.bzutils.BZUtils;
import com.bzutils.LogBZ;
import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.squareup.leakcanary.LeakCanary;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.fabric.sdk.android.Fabric;
import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by iaguila on 23/3/15.
 */
@Singleton
public class ABAApplication extends MultiDexApplication {

    public static final String CRASH_SHARED_PREFERENCES = "crashSharedPreferences";
    public static final String CURRENT_APP_STATUS = "currentAppStatus";

    private View.OnTouchListener imagePressedState;
    private View.OnTouchListener textPressedState;
    private ImageView iconObject;
    private ABATextView textObject;
    private boolean cacheInMemory = true;
    private boolean cacheOnDisc = true;
    private boolean resetViewOnLoad = false;

    private EvaluationComponent component;
    private AssessmentResultComponent assessmentResultComponent;
    private ServerConfigImpl experiment;

    public enum ABATypeface {
        sans100, sans300, sans500, sans700, sans900,
        slab300, slab500, slab500i, slab700, slab900,
        rBold, rRegular
    }

    private static ABAApplication instance;
    private final String TAG = "ABA-English";

    // Realm configuration
    private RealmConfiguration configuration;
    private ApplicationComponent applicationComponent;

    @Inject
    protected ApplicationConfiguration applicationConfiguration;

    public static ABAApplication get() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        MultiDex.install(this);
        LeakCanary.install(this);
        instance = this;

        applicationComponent = createApplicationComponent();
        applicationComponent.inject(this);

        CrashManager.initialize(this, new CrashManagerListener() {
            @Override
            public boolean ignoreDefaultHandler() {
                return super.ignoreDefaultHandler();
            }

            @Override
            public void onNewCrashesFound() {
                setCrashFlagInSharedPreferences(true);
            }
        });

        // Fabric
        if (BuildConfig.DEBUG) {
            // Fabric without crash reporting  development
            Fabric.with(this, new Crashlytics.Builder().disabled(true).build());
        } else {
            // Fabric goes with fully equipped in production
            Fabric.with(this, new Crashlytics());
        }

        // Configuring Cooladata
        CooladataTrackingConfiguration.Initialize(getBaseContext());

        // Realm
        setConfigurationForMigrationRealm();

        LogBZ.setDEBUG(BuildConfig.DEBUG);
        LogBZ.setTAG(TAG);
        BZUtils.printSignature(this);

        // Facebook
        FacebookSdk.sdkInitialize(this);
        AppEventsLogger.activateApp(this);

        BitmapFactory.Options resizeOptions = new BitmapFactory.Options();
        resizeOptions.inScaled = true;

        DisplayImageOptions.Builder optionsBuilder = new DisplayImageOptions.Builder();

        ImageDecoder.init(this);

        optionsBuilder.resetViewBeforeLoading(resetViewOnLoad)
                .cacheInMemory(cacheInMemory)
                .cacheOnDisk(cacheOnDisc)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .decodingOptions(resizeOptions)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT);

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this.getApplicationContext())
                .defaultDisplayImageOptions(optionsBuilder.build())
                .memoryCache(new WeakMemoryCache())
                .build();

        ImageLoader.getInstance().init(config);

        configImagePressedState();
        configTextPressedState();

        initAppContext();

        // GA and Adjust
        FirebaseAnalytics.getInstance(this).setAnalyticsCollectionEnabled(true);
        setupGoogleAnalytics();
        setupAdjust();
    }

    // ================================================================================
    // Public methods - Base methods for other classes
    // ================================================================================

    public RealmConfiguration getRealmConfiguration() {
        if (configuration == null) {
            setConfigurationForMigrationRealm();
        }
        return configuration;
    }

    public void addCrashlyticsTracking(String key, String track) {
        Crashlytics.setString(key, track);
    }

    // ================================================================================
    // Private methods - Initial configuration
    // ================================================================================

    private void setConfigurationForMigrationRealm() {
        try {
            for (ABARealmMigration.ABARealm oldRealm : ABARealmMigration.getOldRealms()) {
                File file = new File(this.getFilesDir(), oldRealm.getName());
                if (file.exists()) {
                    //Realm old file copy data & delete if exist.
                    InputStream realmStream = new FileInputStream(file);
                    copyBundledRealmFile(realmStream, ABARealmMigration.REALM_MIGRATION3);

                    try {
                        Realm.deleteRealm(new RealmConfiguration.Builder(ABAApplication.get())
                                .name(oldRealm.getName())
                                .schemaVersion(oldRealm.getVersion()).build());
                    } catch (Exception ex) {
                        ex.printStackTrace();
                        LogBZ.e("Error on deleted old realm file");
                    }
                }
            }

            configuration = new RealmConfiguration.Builder(ABAApplication.get())
                    .name(ABARealmMigration.REALM_MIGRATION3)
                    .schemaVersion(ABARealmMigration.REALM_MIGRATION3_FILE_SCHEMA)
                    .migration(new ABARealmMigration())
                    .build();
            Realm.setDefaultConfiguration(configuration);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Starts all the elements to be et prior on app execution
     */
    private void initAppContext() {

        ABACoreShepherdEnvironment abaCoreEnvironment = (ABACoreShepherdEnvironment) ABAShepherdEditor.shared(this).environmentForShepherdConfigurationType(this, GenericShepherdConfigurator.ShepherdConfiguratorType.kShepherdConfiguratorTypeABACore);
        applicationConfiguration.setApiUrl(abaCoreEnvironment.getAbaSecureApiUrl());
        applicationConfiguration.setGatewayUrl(abaCoreEnvironment.getAbaGatewayUrl());
        applicationConfiguration.setClientId(abaCoreEnvironment.getClientId());
        applicationConfiguration.setClientSecret(abaCoreEnvironment.getClientSecret());

        // Set the context to APIManager
        APIManager.getInstance().setApplicationContext(getApplicationContext(), applicationConfiguration);
        ProgressActionThread progressThread = new ProgressActionThread(getTracker());
        progressThread.start();
    }

    private void configTextPressedState() {
        textPressedState = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        if (v instanceof ABATextView) {
                            ((ABATextView) v).setTextColor(ContextCompat.getColor(ABAApplication.this, R.color.abaTeacherBannerBackground));
                        } else {
                            textObject.setTextColor(ContextCompat.getColor(ABAApplication.this, R.color.abaTeacherBannerBackground));
                        }
                        break;

                    case MotionEvent.ACTION_UP:
                        TextPressedCondition(v);
                        break;

                    case MotionEvent.ACTION_MOVE:
                        if (event.getX() <= 0 || event.getY() <= 0 || event.getX() >= v.getWidth() || event.getY() >= v.getHeight()) {
                            TextPressedCondition(v);
                        }
                        break;
                }
                return false;
            }
        };
    }

    private void configImagePressedState() {
        imagePressedState = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        if (v instanceof ImageButton) {
                            ((ImageButton) v).getDrawable().setColorFilter(ContextCompat.getColor(ABAApplication.this, R.color.abaDescriptionLightGrey), PorterDuff.Mode.MULTIPLY);
                        } else if (v instanceof ImageView && iconObject == null) {
                            ((ImageView) v).getDrawable().setColorFilter(ContextCompat.getColor(ABAApplication.this, R.color.abaDescriptionLightGrey), PorterDuff.Mode.MULTIPLY);
                        } else {
                            iconObject.getBackground().setColorFilter(ContextCompat.getColor(ABAApplication.this, R.color.abaDescriptionLightGrey), PorterDuff.Mode.MULTIPLY);
                        }
                        break;

                    case MotionEvent.ACTION_UP:
                        ImagePressedCondition(v);
                        break;

                    case MotionEvent.ACTION_MOVE:
                        if (event.getX() <= 0 || event.getY() <= 0 || event.getX() >= v.getWidth() || event.getY() >= v.getHeight()) {
                            ImagePressedCondition(v);
                        }
                        break;
                }
                return false;
            }
        };
    }

    private void ImagePressedCondition(View v) {
        if (v instanceof ImageButton) {
            ((ImageButton) v).getDrawable().setColorFilter(ContextCompat.getColor(ABAApplication.this, R.color.abaWhite), PorterDuff.Mode.MULTIPLY);
        } else if (v instanceof ImageView && iconObject == null) {
            ((ImageView) v).getDrawable().setColorFilter(ContextCompat.getColor(ABAApplication.this, R.color.abaWhite), PorterDuff.Mode.MULTIPLY);
        } else {
            iconObject.getBackground().setColorFilter(ContextCompat.getColor(ABAApplication.this, R.color.abaWhite), PorterDuff.Mode.MULTIPLY);
        }
    }

    private void TextPressedCondition(View v) {
        if (v instanceof ABATextView) {
            ((ABATextView) v).setTextColor(ContextCompat.getColor(ABAApplication.this, R.color.abaGrey));
        } else {
            textObject.setTextColor(ContextCompat.getColor(ABAApplication.this, R.color.abaWhite));
        }
    }

    public void setCrashFlagInSharedPreferences(boolean isCrashed) {
        SharedPreferences preferences = getSharedPreferences(CRASH_SHARED_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(CURRENT_APP_STATUS, isCrashed);
        editor.commit();
    }

    public View.OnTouchListener getImagePressedState(ImageView Object) {
        if (Object != null) {
            iconObject = Object;
        }
        return imagePressedState;
    }

    public View.OnTouchListener getTextPressedState(ABATextView Object) {
        if (Object != null) {
            textObject = Object;
        }
        return textPressedState;
    }

    public void setImagePressedState(View.OnTouchListener TouchListener) {
        this.imagePressedState = TouchListener;
    }

    public void setTextPressedState(View.OnTouchListener TouchListener) {
        this.textPressedState = TouchListener;
    }

    private void setupGoogleAnalytics() {

    }

    private void setupAdjust() {
        String appToken = "vujq2qru2p93";
        String environment;

        if (BuildConfig.BUILD_TYPE.equalsIgnoreCase("debug") || BuildConfig.BUILD_TYPE.equalsIgnoreCase("releaseTesting")) {
            environment = AdjustConfig.ENVIRONMENT_SANDBOX;
        } else {
            environment = AdjustConfig.ENVIRONMENT_PRODUCTION;
        }

        AdjustConfig config = new AdjustConfig(this, appToken, environment);
        Adjust.onCreate(config);
    }

    private String copyBundledRealmFile(InputStream inputStream, String outFileName) {
        try {
            File file = new File(this.getFilesDir(), outFileName);
            FileOutputStream outputStream = new FileOutputStream(file);
            byte[] buf = new byte[1024];
            int bytesRead;
            while ((bytesRead = inputStream.read(buf)) > 0) {
                outputStream.write(buf, 0, bytesRead);
            }
            outputStream.close();
            return file.getAbsolutePath();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Injects this application to the Dagger graph
     */
    private ApplicationComponent createApplicationComponent() {
        return applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .analyticsModule(new AnalyticsModule(this))
                .build();
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }

    public EvaluationComponent getEvaluationComponent() {
        return DaggerEvaluationComponent.builder()
                .applicationComponent(getApplicationComponent())
                .evaluationModule(new EvaluationModule(this))
                .build();
    }

    public AssessmentResultComponent getAssessmentResultComponent() {
        return assessmentResultComponent = DaggerProductionAssessmentResultComponent.builder()
                .evaluationComponent(getEvaluationComponent())
                .assessmentResultModule(new AssessmentResultModule())
                .build();
    }

    public TrackerContract.Tracker getTracker() {
        return applicationComponent.tracker();
    }

    // ================================================================================
    // Private classes
    // ================================================================================

}
