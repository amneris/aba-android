package com.abaenglish.videoclass.clean.interactor;

import rx.Observable;

/**
 * Created by xabierlosada on 13/07/16.
 */
public interface UseCase<T1,T2> {
    Observable<T2> build(T1 inputBoundary);
}
