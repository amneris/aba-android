package com.abaenglish.videoclass.clean.interactor;

import com.crashlytics.android.Crashlytics;

import rx.Subscriber;

/**
 * Created by xabierlosada on 13/07/16.
 */

public abstract class DefaultSubscriber extends Subscriber {

    @Override
    public void onCompleted() {

    }

    @Override
    public void onError(Throwable e) {
        Crashlytics.logException(e);
    }
}
