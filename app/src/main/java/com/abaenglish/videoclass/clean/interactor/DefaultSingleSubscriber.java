package com.abaenglish.videoclass.clean.interactor;

import com.crashlytics.android.Crashlytics;

import rx.SingleSubscriber;

/**
 * Created by xabierlosada on 01/08/16.
 */

public abstract class DefaultSingleSubscriber<T> extends SingleSubscriber<T> {

    @Override
    public void onError(Throwable e) {
        Crashlytics.logException(e);
    }
}
