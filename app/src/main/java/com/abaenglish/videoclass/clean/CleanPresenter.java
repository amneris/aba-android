package com.abaenglish.videoclass.clean;

import rx.Observable;
import rx.Single;
import rx.SingleSubscriber;
import rx.Subscriber;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by xabierlosada on 31/07/16.
 */

public abstract class CleanPresenter<T  extends CleanView, Z extends Router> {

    private T view;
    private Z router;

    private CompositeSubscription subscriptionPool;

    public void initialize(T view, Z router) {
        setView(view);
        setRouter(router);
        subscriptionPool = new CompositeSubscription();
    }

    private void setView(T view) {
        this.view = view;
    }
    private void setRouter(Z router) {
        this.router = router;
    }

    public T getView() {
        return view;
    }

    public Z getRouter() {
        return router;
    }

    @SuppressWarnings("unchecked")
    public void execute(Observable observable, Subscriber subscriber) {
        subscriptionPool.add(observable.subscribe(subscriber));
    }

    @SuppressWarnings("unchecked")
    public void executeSingle(Single<?> observable, SingleSubscriber subscriber) {
        subscriptionPool.add(observable.subscribe(subscriber));
    }

    public void destroy() {
        view = null;
        router = null;
        if (subscriptionPool != null && !subscriptionPool.isUnsubscribed()) {
            subscriptionPool.unsubscribe();
        }
    }
}
