package com.abaenglish.videoclass.clean.interactor;

import rx.Single;

/**
 * Created by xabierlosada on 13/07/16.
 */
public interface SingleShotUseCase<T1,T2> {
    Single<T2> build(T1 inputBoundary);
}
