package com.abaenglish.videoclass;

import android.content.Context;

import com.abaenglish.shepherd.ABAShepherdEditor;
import com.abaenglish.shepherd.configuration.configurators.GenericShepherdConfigurator;
import com.abaenglish.shepherd.configuration.configurators.abacore.ABACoreShepherdEnvironment;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Created by xabierlosada on 27/07/16.
 */

public class UrlUtils {

    public static String urlEncode(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("URLEncoder.encode() failed for " + s);
        }
    }

    /**
     * STATIC METHODS FOR URLS IN ABA MOMENTS
     */

    private static String imageBaseUrlAbaMoments;
    private static String audioBaseUrlAbaMoments;

    public static String getIconUrl(Context context, String id, String name) {

        checkUrl(context);
        String density = getDensityName(context);
        if (id != null && name != null) {
            return imageBaseUrlAbaMoments + id + "/android/" + density + "/" + name + ".png";
        } else {
            return "";
        }
    }

    public static String getImageUrl(Context context, String id, String name) {
        checkUrl(context);
        if (id != null && name != null) {
            return imageBaseUrlAbaMoments + id + "/" + name.toLowerCase() + "www.png";
        } else {
            return "";
        }
    }

    public static String getAudioUrl(Context context, String id, String name) {
        checkUrl(context);
        if (id != null && name != null) {
            return audioBaseUrlAbaMoments + id + "/" + name.toLowerCase() + ".mp3";
        } else {
            return "";
        }
    }

    private static void checkUrl(Context context) {
        if (imageBaseUrlAbaMoments == null || audioBaseUrlAbaMoments == null) {
            ABACoreShepherdEnvironment abaCoreEnvironment = (ABACoreShepherdEnvironment) ABAShepherdEditor.shared(context).environmentForShepherdConfigurationType(context, GenericShepherdConfigurator.ShepherdConfiguratorType.kShepherdConfiguratorTypeABACore);
            imageBaseUrlAbaMoments = abaCoreEnvironment.getAbaImageResourcesUrl();
            audioBaseUrlAbaMoments = abaCoreEnvironment.getAbaAudioResourcesUrl();
        }
    }

    private static String getDensityName(Context context) {
        float density = context.getResources().getDisplayMetrics().density;
        if (density >= 4.0) {
            return "xxxhdpi";
        }
        if (density >= 3.0) {
            return "xxhdpi";
        }
        if (density >= 2.0) {
            return "xhdpi";
        }
        if (density >= 1.5) {
            return "hdpi";
        }
        if (density >= 1.0) {
            return "mdpi";
        }
        return "ldpi";
    }

}
