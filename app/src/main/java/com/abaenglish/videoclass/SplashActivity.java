package com.abaenglish.videoclass;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.abaenglish.shepherd.ABAShepherdEditor;
import com.abaenglish.shepherd.plugin.plugins.ShepherdExternalLogin;
import com.abaenglish.videoclass.analytics.helpers.GcmBroadcastReceiver;
import com.abaenglish.videoclass.analytics.modern.MixpanelTrackingManager;
import com.abaenglish.videoclass.data.ConnectionService;
import com.abaenglish.videoclass.data.ConnectionService.ConnectionServiceCallback;
import com.abaenglish.videoclass.data.currency.CurrencyManager;
import com.abaenglish.videoclass.data.network.retrofit.clients.ABAMomentBadgeClient;
import com.abaenglish.videoclass.data.persistence.ABAAPIError;
import com.abaenglish.videoclass.data.persistence.ABAUser;
import com.abaenglish.videoclass.domain.SupportedVersionManager;
import com.abaenglish.videoclass.domain.content.DataController;
import com.abaenglish.videoclass.domain.content.DataController.ABASimpleCallback;
import com.abaenglish.videoclass.domain.content.UserController;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.abaenglish.videoclass.presentation.base.ABAMasterActivity;
import com.abaenglish.videoclass.presentation.register.RegisterActivity;
import com.abaenglish.videoclass.presentation.shell.MenuActivity;
import com.abaenglish.videoclass.session.SessionService;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

public class SplashActivity extends ABAMasterActivity {

    private String openUnit = null;
    private String openSection = null;
    private boolean openPricesInWeb = false;
    private boolean openMoments = false;
    private String deepLinkToken = null;

    private static final String MOVE_FOLDER_KEY = "movefolderskey";

    private static final String IS_FIRST_SESSION = "IS_FIRST_APP_SESSION";
    private static final String ABA_FIRST_SESSION_SHARED_PREFERENCES = "ABA_FIRST_APP_SESSION_SHARED_PREFERENCES";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);
        onNewIntent(getIntent());

        // TODO: Check whether this is still necessary
        SharedPreferences firstRunPreferences = getSharedPreferences(ABAMasterActivity.RUN_SHARED_PREFERENCES, Context.MODE_PRIVATE);
        if (firstRunPreferences.getBoolean(MOVE_FOLDER_KEY, true)) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    DataStore.getInstance().getAudioController().moveAllNotOfflineVersionFolders();
                }
            }).start();
            SharedPreferences.Editor editor = firstRunPreferences.edit();
            editor.putBoolean(MOVE_FOLDER_KEY, false);
            editor.commit();
        }

        // Detecting first session
        SharedPreferences firstAppSessionPreferences = getSharedPreferences(ABA_FIRST_SESSION_SHARED_PREFERENCES, Context.MODE_PRIVATE);
        if (firstAppSessionPreferences.getBoolean(IS_FIRST_SESSION, true) && DataStore.getInstance().getLevelUnitController().allLevels(getRealm()).size() == 0) {
            MixpanelTrackingManager.getSharedInstance(this).trackFirstAppOpen();
            tracker.trackFirstAppOpen();

            SharedPreferences.Editor editor = firstAppSessionPreferences.edit();
            editor.putBoolean(IS_FIRST_SESSION, false);
            editor.commit();
        }

        checkAppSupport();
    }

    protected void onNewIntent(Intent intent) {
        String action = intent.getAction();
        Uri uri = intent.getData();

        GcmBroadcastReceiver.receiveData = getSharedPreferences(GcmBroadcastReceiver.DEEPLINK_KEY, MODE_PRIVATE);
        if (GcmBroadcastReceiver.receiveData.getString(GcmBroadcastReceiver.DEEPLINK_KEY, null) != null) {
            uri = Uri.parse(GcmBroadcastReceiver.receiveData.getString(GcmBroadcastReceiver.DEEPLINK_KEY, null));
            // Reset receiveData
            GcmBroadcastReceiver.receiveData.edit().putString(GcmBroadcastReceiver.DEEPLINK_KEY, null).commit();
        }

        if (uri != null) {
            String scheme = uri.getScheme();
            String authority = uri.getAuthority();
            String parameters = uri.getQuery() != null ? uri.getQuery().replace("@", "&") : "";

            if (authority != null) {

                Crashlytics.log(Log.INFO, "Logout", "Notification received with authority: '" + authority + "'");

                Uri fixedUri = Uri.parse(scheme + "://" + authority + "?" + parameters);

                // Deeplink will be like `campusapp://...`
                if (authority.equals("course") || authority.equals("autologin")) {
                    openUnit = fixedUri.getQueryParameter("unit");
                    openSection = fixedUri.getQueryParameter("section");
                    deepLinkToken = fixedUri.getQueryParameter("externalToken");

                    String unit = openUnit != null ? openUnit : "";
                    String section = openSection != null ? openSection : "";
                    Crashlytics.log(Log.INFO, "Logout", "Notification received - Going to unit: '" + unit + "' and section: '" + section + "'");
                }

                if (authority.equals("purchase")) {

                    Crashlytics.log(Log.INFO, "Logout", "Notification received - Going to plans view");
                    openPricesInWeb = true;
                }

                if (authority.equalsIgnoreCase("moments")) {
                    openMoments = true;
                }
            }
        }
    }

    private void checkAppSupport() {
        SupportedVersionManager.getSharedInstance(this).isCurrentVersionSupported(new SupportedVersionManager.SupportedVersionResponse() {
            @Override
            public void isSupportedVersion(boolean supported, String lastSupportedVersion) {
                if (!supported) {
                    // Version not supported. Blocking app usage.
                    startForceUpdateActivity();
                } else {
                    // Initiating app normally
                    checkPlayServices();
                    initApp();
                }
            }
        });
    }

    /**
     * Checks if Google Play Services are up to date, if it is out of date a log to
     * Crashlytics is sent
     */
    private void checkPlayServices() {
        GoogleApiAvailability playServiceAvailability = GoogleApiAvailability.getInstance();
        int status = playServiceAvailability.isGooglePlayServicesAvailable(this);

        // If Play Services are up to date
        if (ConnectionResult.SUCCESS == status) {
            MixpanelTrackingManager.getSharedInstance(this).resetGooglePlayServicesOutdated();
            return;
        }

        //If the error is recoverable by the user. For now we only report it to Crashlytics
        /*else if (playServiceAvailability.isUserResolvableError(status)) {
            Dialog gpsDialog = playServiceAvailability.getErrorDialog(this, status, REQUEST_CODE_RECOVER_GPS);
            gpsDialog.setCancelable(false);
            gpsDialog.show();
        }*/
        tracker.trackGooglePlayServicesOutdated();
        MixpanelTrackingManager.getSharedInstance(this).trackGooglePlayServicesOutdated();
    }

    private void initApp() {

        // Updating currencies
        CurrencyManager.updateCurrencies(this);

        // The first login control
        if (DataStore.getInstance().getUserController().isUserLogged() || deepLinkToken != null) {
            if (deepLinkToken == null) {
                final ABAUser currentUser = DataStore.getInstance().getUserController().getCurrentUser(getRealm());

                Crashlytics.log(Log.INFO, "Logout", "Splash screen initApp method. No deeplink token. Performing login with stored user token.");

                appConfig.setUserToken(currentUser.getToken());
                loginUserWithToken(currentUser.getToken());

            } else {
                // Logout CurrentUser and Login newDeeplinkUser
                Crashlytics.log(Log.INFO, "Logout", "Logging out user after Deeplink with token");
                mSessionService.logout(this, new DataController.ABALogoutCallback() {
                    @Override
                    public void onLogoutFinished() {
                        // Resetting token so that oauth can be init again
                        tokenAccessor.reset();
                        ABAMomentBadgeClient.reset(SplashActivity.this);

                        Crashlytics.log(Log.INFO, "Logout", "Logging user in after previous logout due to new deeplink token");
                        appConfig.setUserToken(deepLinkToken);
                        loginUserWithToken(deepLinkToken);
                    }
                });
            }

        } else {

            // Login with Token - Only during development
            if (ABAShepherdEditor.isInternal()) {
                String enteredToken = ShepherdExternalLogin.retrieveTokenAndErase(SplashActivity.this);
                if (enteredToken != null) {
                    Toast.makeText(SplashActivity.this, "Shepherd: Login with token", Toast.LENGTH_SHORT).show();
                    Crashlytics.log(Log.INFO, "Logout", "Logging user in with Shepherd token.");
                    appConfig.setUserToken(enteredToken);
                    loginUserWithToken(enteredToken);
                }
            }
            openTutorial();
        }
    }

    private void loginUserWithToken(String userToken) {
        deepLinkToken = null;

        Crashlytics.log(Log.INFO, "Logout", "Proceeding with login with token.");

        mSessionService.loginWithToken(SplashActivity.this, userToken, tracker, new ABASimpleCallback() {
            @Override
            public void onSuccess() {
                // Creating login action
                DataStore.getInstance().getEvaluationController().saveProgressActionForSection(getRealm(), null, null, null, null, false, false);

                // Traking
                ABAUser currentUser = UserController.getInstance().getCurrentUser(getRealm());
                MixpanelTrackingManager.getSharedInstance(SplashActivity.this).trackLoginSuperproperties(SplashActivity.this, currentUser);

                // Getting ip
                ConnectionService.getInstance().getPublicIP(new ConnectionServiceCallback() {
                    @Override
                    public void finished(String ipAddress) {
                        openApp();
                    }
                });
            }

            @Override
            public void onError(ABAAPIError error) {
                Crashlytics.log(Log.INFO, "Logout", "Unable to log in with given token.");
                logout(false);
            }
        });
    }

    private void openApp() {
        Intent intent = new Intent(this, MenuActivity.class);

        if (DataStore.getInstance().getCurrentActivity() != null) {
            DataStore.getInstance().getCurrentActivity().finish();
        }

        intent.putExtra(MenuActivity.EXTRA_FRAGMENT, MenuActivity.FragmentType.kCurso);
        intent.putExtra(MenuActivity.OPEN_UNIT, openUnit);
        intent.putExtra(MenuActivity.OPEN_SECTION, openSection);
        intent.putExtra(MenuActivity.OPEN_PRICES_IN_WEB, openPricesInWeb);
        intent.putExtra(MenuActivity.OPEN_MOMENTS, openMoments);
        intent.putExtra(MenuActivity.USER_TYPE, MenuActivity.UserType.kUserJoinApp);

        ABAUser user = DataStore.getInstance().getUserController().getCurrentUser(getRealm());
        this.appConfig.setUserEmail(user.getEmail());
        this.appConfig.setUserToken(user.getToken());

        startActivity(intent);
        ActivityCompat.finishAffinity(this);
    }

    @Override
    public void onBackPressed() {
        // Nothing
    }
}