package com.abaenglish.videoclass.domain.content;

import com.abaenglish.videoclass.analytics.legacy.LegacyTrackingManager;
import com.abaenglish.videoclass.data.persistence.ABAPhrase;
import com.abaenglish.videoclass.data.persistence.ABASpeak;
import com.abaenglish.videoclass.data.persistence.ABASpeakDialog;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.domain.ProgressService;
import com.bzutils.LogBZ;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

/**
 * Created by Marc Güell Segarra on 16/4/15.
 */
public class SpeakController extends SectionController<ABASpeak> {

    @Override
    public ABASpeak getSectionForUnit(ABAUnit unit) {
        return unit.getSectionSpeak();
    }

    @Override
    public boolean isSectionCompleted(ABASpeak section) {
        return getElementsCompletedForSection(section) >= getTotalElementsForSection(section);
    }

    @Override
    public ABAPhrase getCurrentPhraseForSection(ABASpeak section, int position) {
        for (ABASpeakDialog dialog : section.getContent()) {
            for (ABAPhrase phrase : dialog.getDialog()) {
                if (!phrase.isDone())
                    return phrase;
            }

            for (ABAPhrase phrase : dialog.getSample()) {
                if (!phrase.isDone() && phrase.getSubPhrases().size() == 0)
                    return phrase;

                for (ABAPhrase subPhrase : phrase.getSubPhrases()) {
                    if (!subPhrase.isDone() == !subPhrase.getText().equals("") && subPhrase.getAudioFile() != null && !subPhrase.getAudioFile().equals(""))
                        return subPhrase;
                }
            }
        }
        return null;
    }

    @Override
    public Integer getTotalElementsForSection(ABASpeak section) {
        return getElementsFromSection(section).size() * 2;
    }

    @Override
    public Integer getElementsCompletedForSection(ABASpeak section) {
        int total = 0;
        for (ABASpeakDialog dialog : section.getContent()) {
            for (ABAPhrase phrase : dialog.getDialog()) {
                if (phrase.isListened())
                    total++;
                if (phrase.isDone())
                    total++;
            }

            for (ABAPhrase phraseSample : dialog.getSample()) {
                if (phraseSample.getSubPhrases().size() > 0) {
                    for (ABAPhrase subPhrase : phraseSample.getSubPhrases()) {
                        if (subPhrase.isListened() && subPhrase.getText().length() > 0) total++;

                        if (subPhrase.isDone() && subPhrase.getText().length() > 0) total++;
                    }
                } else {
                    if (phraseSample.isListened()) total++;

                    if (phraseSample.isDone()) total++;
                }
            }
        }
        return total;
    }

    @Override
    public void setCompletedSection(Realm realm, ABASpeak section) {
        realm.beginTransaction();
        section.setCompleted(true);
        section.setProgress(100);
        ProgressService.getInstance().updateProgressUnit(section.getUnit());
        realm.commitTransaction();

        LegacyTrackingManager.getInstance().trackEvent(realm, "unit_s2_finish", "course", "acaba habla");
    }

    @Override
    public String getPercentageForSection(ABASpeak section) {
        return (int) getProgressForSection(section) + "%";
    }

    @Override
    public float getProgressForSection(ABASpeak section) {
        if (getTotalElementsForSection(section) == 0) {
            return 0;
        }
        float percentage = getElementsCompletedForSection(section) * 100 / getTotalElementsForSection(section);
        return percentage > 100 ? 100 : percentage;
    }

    @Override
    public void syncCompletedActions(Realm realm, ABASpeak section, JSONArray actions) throws IllegalStateException {
        for (int i = 0; i < actions.length(); i++) {
            try {
                JSONObject action = actions.getJSONObject(i);
                ABAPhrase phrase = phraseWithID(action.getString("Audio"), action.getString("Page"), section);

                if (phrase == null) continue;

                if (action.getString("Action").equals("LISTENED")) {
                    if (phrase.isListened()) continue;

                } else {
                    if (phrase.isDone()) continue;

                    setCurrentPhraseDone(phrase);
                    checkTextPhrases(phrase, section);

                    if (isSectionCompleted(section) && !section.isCompleted()) {
                        section.setCompleted(true);
                        section.setProgress(100);
                    }
                }

            } catch (JSONException e) {
                LogBZ.printStackTrace(e);
            }
        }
    }

    @Override
    public ABAPhrase phraseWithID(String audioId, String page, ABASpeak section) {
        for (ABASpeakDialog dialog : section.getContent()) {
            for (ABAPhrase phrase : dialog.getDialog()) {
                if(phrase.getAudioFile() != null && !phrase.getAudioFile().isEmpty() && matchPhrase(audioId, phrase, page)) {
                    return phrase;
                }
            }

            for (ABAPhrase phrase : dialog.getSample()) {
                if(phrase.getAudioFile() != null && !phrase.getAudioFile().isEmpty() && matchPhrase(audioId, phrase, page)) {
                    return phrase;
                }

                for (ABAPhrase subPhrase : phrase.getSubPhrases()) {
                    if(subPhrase.getAudioFile() != null && !subPhrase.getAudioFile().isEmpty() && matchPhrase(audioId, subPhrase, page)) {
                        return subPhrase;
                    }
                }
            }
        }
        return null;
    }

    public int getCurrentDialogFromSection(ABASpeak section){
        int index = 0;
        for(ABASpeakDialog speakDialog : section.getContent()){
            index += speakDialog.getSample().size() + 1;
            for (ABAPhrase phrase : speakDialog.getDialog()) {
                if (!phrase.isDone())
                    return index;
            }

            for (ABAPhrase phrase : speakDialog.getSample()) {
                if (!phrase.isDone() && phrase.getSubPhrases().size() == 0)
                    return index;

                for (ABAPhrase subPhrase : phrase.getSubPhrases()) {
                    if (!subPhrase.isDone() == !subPhrase.getText().equals("") && subPhrase.getAudioFile() != null && !subPhrase.getAudioFile().equals(""))
                        return index;
                }
            }
        }
        //Section completed show all
        return index;
    }

    public List<ABAPhrase> getElementsFromSection(ABASpeak section) {
        ArrayList<ABAPhrase> phrases = new ArrayList<>();
        for (ABASpeakDialog dialog : section.getContent()) {
            phrases.addAll(dialog.getDialog());

            for (ABAPhrase phrase : dialog.getSample()) {
                if (phrase.getSubPhrases().size() > 0) {
                    for (ABAPhrase subPhrase : phrase.getSubPhrases()) {
                        if (subPhrase.getText().length() > 0) {
                            phrases.add(subPhrase);
                        }
                    }
                } else {
                    phrases.add(phrase);
                }
            }
        }
        return phrases;
    }

    private void checkTextPhrases(ABAPhrase donePhrase, ABASpeak section) {
        for (ABASpeakDialog dialog : section.getContent()) {
            for (ABAPhrase phrase : dialog.getSample()) {
                for (ABAPhrase subPhrase : phrase.getSubPhrases()) {
                    if (subPhrase.getAudioFile().equals(donePhrase.getAudioFile())) {
                        setCurrentPhraseDone(subPhrase);
                    }
                }
            }
        }
    }

    public ABAPhrase getCurrentSampleToPlay(ABAPhrase selectedPhrase) {
        if (selectedPhrase == null) //end of section
            return null;

        if (selectedPhrase.isSpeakDialogPhrase())
            return selectedPhrase;

        LogBZ.d("SubPhrases size: " + selectedPhrase.getSubPhrases().size());
        if (selectedPhrase.getSubPhrases().size() > 0) {
            for (ABAPhrase subPhrase : selectedPhrase.getSubPhrases()) {
                if (!subPhrase.isDone() && !subPhrase.getText().equals("")) {
                    return subPhrase;
                }
            }
            return selectedPhrase.getSubPhrases().first();
        }
        return selectedPhrase;
    }
}
