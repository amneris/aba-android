package com.abaenglish.videoclass.domain.content;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by gnatok on 08/11/2016.
 */

public class PreferencesProviderImpl implements PreferencesProvider {

    private Context context;
    private static final String PROFILE_PREFERENCE_NAME = "PROFILE_PREFERENCE_NAME";
    private static final String MOBILE_DATA_PREFERENCE_KEY = "MOBILE_DATA_PREFERENCE";
    private static final String NOTIFICATION_PREFERENCE_KEY = "NOTIFICATION_PREFERENCE";

    public PreferencesProviderImpl(Context context) {
        this.context = context;
    }

    // ================================================================================
    // PreferencesProvider interface
    // ================================================================================

    @Override
    public void setPushNotificationAllowance(boolean condition) {
        config(NOTIFICATION_PREFERENCE_KEY, condition);
    }

    @Override
    public void setCarrierDownloadAllowance(boolean condition) {
        config(MOBILE_DATA_PREFERENCE_KEY, condition);
    }

    @Override
    public boolean isPushNotificationAllowed() {
        SharedPreferences sharedPref = getSharedPreferences();
        return sharedPref.getBoolean(NOTIFICATION_PREFERENCE_KEY, true);
    }

    @Override
    public boolean isCarrierDataDownloadAllowed() {
        SharedPreferences sharedPref = getSharedPreferences();
        return sharedPref.getBoolean(MOBILE_DATA_PREFERENCE_KEY, true);
    }

    @Override
    public void resetProfilePreferences() {
        SharedPreferences sharedPref = getSharedPreferences();
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.remove(MOBILE_DATA_PREFERENCE_KEY);
        editor.remove(NOTIFICATION_PREFERENCE_KEY);
        editor.apply();
    }

    // ================================================================================
    // Private methods
    // ================================================================================

    private void config(String key, boolean condition) {
        SharedPreferences sharedPref = getSharedPreferences();
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(key, condition);
        editor.apply();
    }

    private SharedPreferences getSharedPreferences() {
        return context.getSharedPreferences(PROFILE_PREFERENCE_NAME, Context.MODE_PRIVATE);
    }
}
