package com.abaenglish.videoclass.domain.content;

import android.os.Environment;

import com.abaenglish.videoclass.data.persistence.ABAUnit;

import java.io.File;
import java.io.IOException;

/**
 * Created by Jesus Espejo using mbp13jesus on 15/10/15.
 */
public class GenericController {

    public static final String abaEnglishFolderPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/ABA_English";

    public static boolean checkIfFileExist(ABAUnit currentUnit, String url) {
        return new File(getLocalFilePath(currentUnit, url)).exists();
    }

    public static String getLocalFilePath(ABAUnit currentUnit, String url) {
        return getABAEnglishFolderPath(currentUnit) + "/" + getFileNameFromUrl(currentUnit, url) + "." + getFileExtensionFromURL(url);
    }

    public static boolean deleteDirectory(File path) {
        if (path.exists()) {
            File[] files = path.listFiles();
            if (files == null) {
                return true;
            }
            for (int i = 0; i < files.length; i++) {
                if (files[i].isDirectory()) {
                    deleteDirectory(files[i]);
                } else {
                    files[i].delete();
                }
            }
        }
        return (path.delete());
    }

    public static String getABAEnglishFolderPath(ABAUnit unit) {
        File abaEnglishFolder = new File(abaEnglishFolderPath);
        File noMediaFile = new File(abaEnglishFolderPath + "/.nomedia");
        File targetFolder;

        if (!abaEnglishFolder.exists()) {
            abaEnglishFolder.mkdir();
        }

        if (!noMediaFile.exists()) {
            try {
                noMediaFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (unit != null) {
            targetFolder = new File(abaEnglishFolderPath + "/UNIT" + unit.getIdUnit());
        } else {
            targetFolder = new File(abaEnglishFolderPath + "/TEACHER");
        }

        if (!targetFolder.exists()) {
            targetFolder.mkdir();
        }

        return targetFolder.getAbsolutePath();
    }

    public static String getFileNameFromUrl(ABAUnit currentUnit, String url) {
        if (url != null && url.length() != 0) {
            if (currentUnit != null) {
                if (currentUnit.getSectionFilm().getHdVideoURL().equals(url) || currentUnit.getSectionFilm().getSdVideoURL().equals(url)) {
                    return "abaFilm";
                } else if (currentUnit.getSectionVideoClass().getHdVideoURL().equals(url) || currentUnit.getSectionVideoClass().getSdVideoURL().equals(url)) {
                    return "abaVideoclass";
                }
            }
            return url.substring(url.lastIndexOf("/") + 1, url.lastIndexOf("."));
        }
        return "";
    }

    // ================================================================================
    // Protected methods
    // ================================================================================

    protected static String getFileExtensionFromURL(String url) {
        if (url != null && url.length() != 0)
            return url.substring(url.lastIndexOf(".") + 1, url.lastIndexOf(".") + 4);
        return null;
    }
}
