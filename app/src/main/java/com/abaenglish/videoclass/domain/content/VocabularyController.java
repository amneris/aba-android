package com.abaenglish.videoclass.domain.content;

import com.abaenglish.videoclass.analytics.legacy.LegacyTrackingManager;
import com.abaenglish.videoclass.data.persistence.ABAPhrase;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.data.persistence.ABAVocabulary;
import com.abaenglish.videoclass.domain.ProgressService;
import com.bzutils.LogBZ;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

/**
 * Created by Marc Güell Segarra on 16/4/15.
 */
public class VocabularyController extends SectionController<ABAVocabulary> {

    @Override
    public ABAVocabulary getSectionForUnit(ABAUnit unit) {
        return unit.getSectionVocabulary();
    }

    @Override
    public boolean isSectionCompleted(ABAVocabulary section) {
        return (getElementsCompletedForSection(section) >= getTotalElementsForSection(section));
    }

    @Override
    public Integer getTotalElementsForSection(ABAVocabulary section) {
        return section.getContent().size() * 2;
    }

    @Override
    public Integer getElementsCompletedForSection(ABAVocabulary section) {
        return getTotalPhrasesCompletedForVocabulary(section);
    }

    @Override
    public void setCompletedSection(Realm realm, ABAVocabulary section) {
        realm.beginTransaction();
        section.setCompleted(true);
        section.setProgress(100);
        ProgressService.getInstance().updateProgressUnit(section.getUnit());
        realm.commitTransaction();
        LegacyTrackingManager.getInstance().trackEvent(realm, "unit_s7_finish", "course", "acaba vocabulario");
    }

    @Override
    public String getPercentageForSection(ABAVocabulary section) {
        return (int) getProgressForSection(section) + "%";
    }

    @Override
    public float getProgressForSection(ABAVocabulary section) {
        if (getTotalElementsForSection(section) == 0) {
            return 0;
        }
        float percentage = getElementsCompletedForSection(section) * 100 / getTotalElementsForSection(section);
        return percentage > 100 ? 100 : percentage;
    }


    @Override
    public void syncCompletedActions(Realm realm, ABAVocabulary section, JSONArray actions) throws IllegalStateException {
        for (int i = 0; i < actions.length(); i++) {
            try {
                JSONObject action = actions.getJSONObject(i);
                ABAPhrase phrase = phraseWithID(action.getString("Audio"), Integer.toString(action.getInt("Page")), section);

                if (phrase == null) continue;

                if (action.getString("Action").equals("LISTENED")) {
                    if (phrase.isListened()) continue;

                } else {
                    if (phrase.isDone()) continue;

                    setCurrentPhraseDone(phrase);

                    if (isSectionCompleted(section) && !section.isCompleted()) {
                        section.setCompleted(true);
                        section.setProgress(100);
                    }
                }
            } catch (JSONException e) {
                LogBZ.printStackTrace(e);
            }
        }
    }

    @Override
    public ABAPhrase phraseWithID(String audioId, String page, ABAVocabulary vocabularySection) {
        for (ABAPhrase phrase : vocabularySection.getContent()) {
            if(phrase.getAudioFile() != null && !phrase.getAudioFile().isEmpty() && phrase.getAudioFile().equals(audioId) && phrase.getPage().equals(page)) {
                return phrase;
            }
        }
        return null;
    }

    @Override
    public List<String> getAllAudioIDsForSection(ABAVocabulary vocabularySection) {
        List<String> audioIds = new ArrayList<>();
        for (ABAPhrase phrase : vocabularySection.getContent()) {
            audioIds.add(phrase.getAudioFile());
        }
        return audioIds;
    }

    @Override
    public ABAPhrase getCurrentPhraseForSection(ABAVocabulary section, int position) {
        if (position == ContinueWithFirstUndoneWord) {
            for (ABAPhrase phrase : section.getContent()) {
                if (!phrase.isDone())
                    return phrase;
            }
        } else {
            for (ABAPhrase phrase : section.getContent()) {
                if (section.getContent().indexOf(phrase) == position) {
                    return phrase;
                }
            }
        }
        return null;
    }

    public int getTotalPhrasesDoneForVocabulary(ABAVocabulary vocabularySection) {
        int total = 0;
        for (ABAPhrase phrase : vocabularySection.getContent()) {
            if (phrase.isDone()) {
                total++;
            }
        }
        return total;
    }

    public int getTotalPhrasesCompletedForVocabulary(ABAVocabulary vocabularySection) {
        int total = 0;
        for (ABAPhrase phrase : vocabularySection.getContent()) {
            if (phrase.isDone())
                total++;

            if (phrase.isListened())
                total++;
        }
        return total;
    }

    public ArrayList<ABAPhrase> getElementsFromSection(ABAVocabulary section) {
        ArrayList<ABAPhrase> phrases = new ArrayList<>();
        for (ABAPhrase phrase : section.getContent()) {
            phrases.add(phrase);
        }
        return phrases;
    }
}
