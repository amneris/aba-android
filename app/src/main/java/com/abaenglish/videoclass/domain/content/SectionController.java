package com.abaenglish.videoclass.domain.content;


import com.abaenglish.videoclass.data.persistence.ABAEvaluation;
import com.abaenglish.videoclass.data.persistence.ABAExercises;
import com.abaenglish.videoclass.data.persistence.ABAFilm;
import com.abaenglish.videoclass.data.persistence.ABAInterpret;
import com.abaenglish.videoclass.data.persistence.ABAPhrase;
import com.abaenglish.videoclass.data.persistence.ABAProgressAction;
import com.abaenglish.videoclass.data.persistence.ABASpeak;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.data.persistence.ABAVideoClass;
import com.abaenglish.videoclass.data.persistence.ABAVocabulary;
import com.abaenglish.videoclass.data.persistence.ABAWrite;
import com.abaenglish.videoclass.domain.ProgressService;
import com.abaenglish.videoclass.presentation.section.SectionType;

import org.json.JSONArray;

import java.util.List;

import io.realm.Realm;

/**
 * Created by marc on 25/5/15.
 */
public abstract class SectionController<K> {

    public static int ContinueWithFirstUndoneWord = -1;

    /**
     * Structure to handle the section status
     */
    public enum SectionStatus {
        kABAStatusLocked,
        kABAStatusPending,
        kABAStatusActual,
        kABAStatusCompleted,
    }

    public abstract K getSectionForUnit(ABAUnit unit);

    public abstract Integer getTotalElementsForSection(K section);

    public abstract Integer getElementsCompletedForSection(K section);

    public abstract void setCompletedSection(Realm realm, K section);

    public abstract String getPercentageForSection(K section);

    public abstract float getProgressForSection(K section);

    public void saveProgressActionForSection(final Realm realm, K section, ABAUnit unit, ABAPhrase phrase, String errorText, Boolean isListen, Boolean isHelp) {
        ProgressActionController<K> progressActionController = new ProgressActionController<>();

        ABAProgressAction progressAction = progressActionController.createProgressAction(
                section,
                unit,
                UserController.getInstance().getCurrentUser(realm),
                phrase,
                isListen,
                isHelp,
                errorText
        );
        ProgressActionController.saveProgressAction(progressAction);
    }

    public abstract void syncCompletedActions(Realm realm, K section, JSONArray actions);

    public abstract boolean isSectionCompleted(K section);

    public List<String> getAllAudioIDsForSection(K section) {
        throw new UnsupportedOperationException();
    }

    public abstract ABAPhrase getCurrentPhraseForSection(K section, int position);

    public ABAPhrase phraseWithID(String audioId, String page, K section) {
        throw new UnsupportedOperationException();
    }

    public boolean matchPhrase(String audioID, ABAPhrase phrase, String page) {
        if (phrase.getAudioFile().equals(audioID) && phrase.getPage().equals(page))
            return true;
        return false;
    }

    public void setPhraseDone(Realm realm, ABAPhrase phrase, K section, boolean sendProgressAction) {

        // We set ABAPhrase object to done and listened in Realm
        realm.beginTransaction();
        phrase.setDone(true);
        phrase.setListened(true);
        realm.commitTransaction();

        if (sendProgressAction) {
            // We don't create progress action with Empty Phrase
            if (phrase.getAudioFile() != null && !phrase.getAudioFile().isEmpty()) {
                saveProgressActionForSection(realm, section, getUnitFromSection(section), phrase, null, false, false);
            }

            // And update the global unit progress
            realm.beginTransaction();
            ProgressService.getInstance().updateProgressUnit(getUnitFromSection(section));
            realm.commitTransaction();
        }
    }

    public void setCurrentPhraseDone(ABAPhrase phrase) {
        //Set DonePhrase without newTransaction
        phrase.setDone(true);
        phrase.setListened(true);
    }

    public ABAUnit getUnitFromSection(K section) {

        switch (SectionType.fromClass(section.getClass())) {
            case kABAFilm:
                return ((ABAFilm) section).getUnit();
            case kEjercicios:
                return ((ABAExercises) section).getUnit();
            case kEscribe:
                return ((ABAWrite) section).getUnit();
            case kEvaluacion:
                return ((ABAEvaluation) section).getUnit();
            case kHabla:
                return ((ABASpeak) section).getUnit();
            case kInterpreta:
                return ((ABAInterpret) section).getUnit();
            case kVideoClase:
                return ((ABAVideoClass) section).getUnit();
            case kVocabulario:
                return ((ABAVocabulary) section).getUnit();
            default:
                return null;
        }
    }

}
