package com.abaenglish.videoclass.domain.content;

/**
 * Created by gnatok on 08/11/2016.
 */

public interface PreferencesProvider {
    void setPushNotificationAllowance(boolean condition);
    void setCarrierDownloadAllowance(boolean condition);
    boolean isPushNotificationAllowed();
    boolean isCarrierDataDownloadAllowed();
    void resetProfilePreferences();
}
