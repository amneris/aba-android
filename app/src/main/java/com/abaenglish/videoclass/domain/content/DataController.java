package com.abaenglish.videoclass.domain.content;

import com.abaenglish.videoclass.data.persistence.ABAAPIError;

/**
 * Created by Marc Güell Segarra on 26/3/15.
 * Refactored by Jesus Espejo on 02/03/2016
 */
public class DataController {

    // ================================================================================
    // Interfaces
    // ================================================================================

    public interface ABASimpleCallback {
        void onSuccess();

        void onError(ABAAPIError error);
    }

    public interface ABAAPICallback<T> {
        void onSuccess(T response);

        void onError(ABAAPIError error);
    }

    public interface ABAAPIFacebookCallback {
        void onSuccess(Boolean isNewUser);

        void onError(ABAAPIError error);
    }

    public interface ABADatabaseCallback<T> {
        void onSuccess(T object);
    }

    public interface ABALogoutCallback {
        void onLogoutFinished();
    }
}