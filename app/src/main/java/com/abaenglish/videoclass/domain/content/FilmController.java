package com.abaenglish.videoclass.domain.content;

import com.abaenglish.videoclass.analytics.cooladata.study.CooladataStudyTrackingManager;
import com.abaenglish.videoclass.analytics.legacy.LegacyTrackingManager;
import com.abaenglish.videoclass.data.network.APIManager;
import com.abaenglish.videoclass.data.persistence.ABAAPIError;
import com.abaenglish.videoclass.data.persistence.ABAFilm;
import com.abaenglish.videoclass.data.persistence.ABAPhrase;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.data.persistence.ABAUser;
import com.abaenglish.videoclass.domain.ProgressService;
import com.abaenglish.videoclass.presentation.section.videoclass.videoplayer.FormatSRT;
import com.bzutils.LogBZ;

import org.json.JSONArray;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import io.realm.Realm;

import static com.abaenglish.videoclass.analytics.cooladata.CooladataTrackingDefinitions.CooladataSectionType.ABAFilm;

/**
 * Created by Marc Güell Segarra on 16/4/15.
 */
public class FilmController extends SectionController<ABAFilm> {

    // ================================================================================
    // SectionController methods
    // ================================================================================

    @Override
    public ABAFilm getSectionForUnit(ABAUnit unit) {
        return unit.getSectionFilm();
    }

    @Override
    public boolean isSectionCompleted(ABAFilm section) {
        return section.getProgress() >= 100 || section.isCompleted();
    }

    @Override
    public void setCompletedSection(Realm realm, ABAFilm section) {
        realm.beginTransaction();
        section.setCompleted(true);
        section.setProgress(100);
        ProgressService.getInstance().updateProgressUnit(section.getUnit());
        realm.commitTransaction();

        // Tracking ABA Analytics
        saveProgressActionForSection(realm, section, section.getUnit(), null, null, false, false);

        // Tracking
        ABAUser user = UserController.getInstance().getCurrentUser(realm);
        CooladataStudyTrackingManager.trackWatchedFilm(user.getUserId(), section.getUnit().getLevel().getIdLevel(), section.getUnit().getIdUnit(), ABAFilm);
        LegacyTrackingManager.getInstance().trackEvent(realm, "unit_s1_finish", "course", "acaba abafilm");
    }

    @Override
    public String getPercentageForSection(ABAFilm section) {
        return (int) getProgressForSection(section) + "%";
    }

    @Override
    public float getProgressForSection(ABAFilm section) {
        return section.getProgress();
    }

    @Override
    public ABAPhrase getCurrentPhraseForSection(ABAFilm section, int position) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Integer getTotalElementsForSection(ABAFilm section) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Integer getElementsCompletedForSection(ABAFilm section) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void syncCompletedActions(Realm realm, ABAFilm section, JSONArray actions) {
        throw new UnsupportedOperationException();
    }

    // ================================================================================
    // Public methods
    // ================================================================================

    public void downloadSubtitles(String url, final DataController.ABAAPICallback callback) {
        if (url != null && url.length() != 0) {
            APIManager.getInstance().downloadSubtitles(url, new APIManager.ABAAPIResponseCallback() {
                @Override
                public void onResponse(String response) {
                    try {
                        callback.onSuccess(new FormatSRT().parseFile("sample.srt", new ByteArrayInputStream(response.getBytes("UTF-8"))));
                    } catch (IOException e) {
                        LogBZ.printStackTrace(e);
                        callback.onError(new ABAAPIError("error in download inf subs"));
                    }
                }

                @Override
                public void onError(Exception e) {
                    LogBZ.printStackTrace(e);
                    callback.onError(new ABAAPIError("error in download inf subs"));
                }
            });
        }
    }
}
