package com.abaenglish.videoclass.domain;

/**
 * Created by Jesus Espejo using mbp13jesus on 12/04/2017.
 */
public class ApplicationConfiguration {

    private String apiUrl;
    private String gatewayUrl;
    private String clientSecret;
    private String clientId;
    private String userToken;
    private String userEmail;

    // ================================================================================
    // Public methods
    // ================================================================================

    public void reset() {
        this.apiUrl = null;
        this.gatewayUrl = null;
        clientSecret = null;
        clientId = null;
        this.userToken = null;
        this.userEmail = null;
    }

    // ================================================================================
    // Accessors
    // ================================================================================

    public String getApiUrl() {
        return apiUrl;
    }

    public void setApiUrl(String apiUrl) {
        this.apiUrl = apiUrl;
    }

    public String getGatewayUrl() {
        return gatewayUrl;
    }

    public void setGatewayUrl(String gatewayUrl) {
        this.gatewayUrl = gatewayUrl;
    }

    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    @Override
    public String toString() {
        return "ApplicationConfiguration{" +
                "\napiUrl='" + apiUrl + '\'' +
                "\ngatewayUrl='" + gatewayUrl + '\'' +
                "\nuserToken='" + userToken + '\'' +
                "\nuserEmail='" + userEmail + '\'' +
                "\n}";
    }
}
