package com.abaenglish.videoclass.domain.content;

import com.abaenglish.videoclass.data.persistence.ABAAPIError;

/**
 * Created by madalin on 29/07/15.
 */
public class FacebookCallbackController {

    private DataController.ABAAPIFacebookCallback facebookLoginCallback;

    public FacebookCallbackController() {
    }

    public void setFacebookLoginCallback(DataController.ABAAPIFacebookCallback callback) {
        this.facebookLoginCallback = callback;
    }

    public DataController.ABAAPIFacebookCallback getFacebookLoginCallback() {
        return this.facebookLoginCallback;
    }

    public void showFacebookError(String messageError) {
        if (facebookLoginCallback != null)
            facebookLoginCallback.onError(new ABAAPIError(messageError));
    }
}
