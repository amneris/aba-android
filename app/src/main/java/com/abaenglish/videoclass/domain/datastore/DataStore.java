package com.abaenglish.videoclass.domain.datastore;

import android.app.Activity;

import com.abaenglish.videoclass.analytics.TrackerContract;
import com.abaenglish.videoclass.data.DownloadController;
import com.abaenglish.videoclass.domain.content.EvaluationController;
import com.abaenglish.videoclass.domain.content.ExercisesController;
import com.abaenglish.videoclass.domain.content.FacebookCallbackController;
import com.abaenglish.videoclass.domain.content.FilmController;
import com.abaenglish.videoclass.domain.content.InterpretController;
import com.abaenglish.videoclass.domain.content.LevelUnitController;
import com.abaenglish.videoclass.domain.content.PlanController;
import com.abaenglish.videoclass.domain.content.SpeakController;
import com.abaenglish.videoclass.domain.content.UserController;
import com.abaenglish.videoclass.domain.content.VideoClassController;
import com.abaenglish.videoclass.domain.content.VocabularyController;
import com.abaenglish.videoclass.domain.content.WriteController;
import com.abaenglish.videoclass.presentation.base.AudioController;
import com.abaenglish.videoclass.presentation.unit.RoscoController;

/**
 * Created by iaguila on 23/3/15.
 */
public class DataStore {

    private static DataStore instance;
    private static boolean isStartActivity = false;
    private static Activity currentActivity;

    public static DataStore getInstance() {
        if (instance == null) {
            instance = new DataStore();
        }
        return instance;
     }

    private UserController userController;
    private LevelUnitController levelUnitController;
    private DownloadController downloadController;
    private FilmController filmController;
    private VideoClassController videoClassController;
    private EvaluationController evaluationController;
    private ExercisesController exercisesController;
    private InterpretController interpretController;
    private SpeakController speakController;
    private VocabularyController vocabularyController;
    private WriteController writeController;
    private AudioController audioController;
    private PlanController planController;
    private RoscoController roscoController;

    private FacebookCallbackController facebookCallbackController;

    public UserController getUserController() {
        if (userController == null)
            userController = new UserController();
        return userController;
    }

    public LevelUnitController getLevelUnitController() {
        if (levelUnitController == null)
            levelUnitController = new LevelUnitController();
        return levelUnitController;
    }

    public VideoClassController getVideoClassController() {
        if (videoClassController == null)
            videoClassController = new VideoClassController();
        return videoClassController;
    }

    public DownloadController getDownloadController() {
        if (downloadController == null) {
            downloadController = new DownloadController();
        }
        return downloadController;
    }

    public EvaluationController getEvaluationController() {
        if (evaluationController == null)
            evaluationController = new EvaluationController();
        return evaluationController;
    }

    public ExercisesController getExercisesController() {
        if (exercisesController == null)
            exercisesController = new ExercisesController();
        return exercisesController;
    }

    public InterpretController getInterpretController() {
        if (interpretController == null)
            interpretController = new InterpretController();
        return interpretController;
    }

    public SpeakController getSpeakController() {
        if (speakController == null)
            speakController = new SpeakController();
        return speakController;
    }

    public VocabularyController getVocabularyController() {
        if (vocabularyController == null)
            vocabularyController = new VocabularyController();
        return vocabularyController;
    }

    public WriteController getWriteController() {
        if (writeController == null)
            writeController = new WriteController();
        return writeController;
    }

    public FilmController getFilmController() {
        if (filmController == null)
            filmController = new FilmController();
        return filmController;
    }

    public AudioController getAudioController() {
        if (audioController == null)
            audioController = new AudioController();
        return audioController;
    }

    public PlanController getPlanController(TrackerContract.Tracker tracker) {
        if (planController == null)
            planController = new PlanController(tracker);
        return planController;
    }

    public RoscoController getRoscoController() {
        if (roscoController == null) {
            roscoController = new RoscoController();
        }
        return roscoController;
    }

    public FacebookCallbackController getFacebookCallbackController() {
        if (facebookCallbackController == null)
            facebookCallbackController = new FacebookCallbackController();
        return facebookCallbackController;
    }

    public boolean getStartActivity() {
        return isStartActivity;
    }

    public void setCurrentActivity(Activity activity) {
        currentActivity = activity;
    }

    public Activity getCurrentActivity() {
        return currentActivity;
    }
}
