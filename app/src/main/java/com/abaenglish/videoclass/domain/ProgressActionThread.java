package com.abaenglish.videoclass.domain;

import android.os.Handler;
import android.os.Looper;

import com.abaenglish.videoclass.ABAApplication;
import com.abaenglish.videoclass.analytics.TrackerContract;
import com.abaenglish.videoclass.data.persistence.ABAAPIError;
import com.abaenglish.videoclass.domain.content.DataController;
import com.abaenglish.videoclass.domain.content.ProgressActionController;

import io.realm.Realm;

/**
 * Created by jaume on 22/5/15.
 * Refactored by Jesus on 14/03/2016
 */
public class ProgressActionThread extends Thread {

    private TrackerContract.Tracker tracker;

    public ProgressActionThread(TrackerContract.Tracker tracker) {
        this.tracker = tracker;
    }

    private enum ProgressActionThreadStatus {
        REGULAR_MODE,
        FAST_MODE
    }

    private static final int DEFAULT_DIVIDER = 1;
    private static int mDivider = DEFAULT_DIVIDER;

    private static final Integer PROGRESS_ACTION_LIMIT = 1000;
    private static final long PROCESS_QUEUE_TIME = 30000;

    private Handler mHandler = new Handler();
    private Runnable mProgressActions = new Runnable() {
        @Override
        public void run() {
            // Sending next progress batch to server
            dispatchProgressActionsToServer(tracker);
        }
    };

    public void run() {
        Looper.prepare();
        updateStatus();
        Looper.loop();
    }

    // ================================================================================
    // Private methods
    // ================================================================================

    /**
     * Method to calculate whether the fast mode is enabled or not. If there are more progress actions than the batch then
     * fast mode should by on
     *
     * @return true if fast mode is on
     */
    private Boolean shouldFastModeBeOn() {
        Realm realmToCalculateFastMode = Realm.getInstance(ABAApplication.get().getRealmConfiguration());
        Boolean fastMode = ProgressActionController.getPendingProgressActionCount(realmToCalculateFastMode) > PROGRESS_ACTION_LIMIT;
        realmToCalculateFastMode.close();

        return fastMode;
    }

    private void dispatchProgressActionsToServer(TrackerContract.Tracker tracker) {
        ProgressActionController.sendProgressActionsToServer(PROGRESS_ACTION_LIMIT / mDivider, new DataController.ABASimpleCallback() {
            @Override
            public void onSuccess() {
                updateStatus();
            }

            @Override
            public void onError(ABAAPIError error) {
                forceStatus(ProgressActionThreadStatus.REGULAR_MODE);
            }
        }, tracker);
    }

    private ProgressActionThreadStatus calculateStatus() {
        if (shouldFastModeBeOn()) {
            return ProgressActionThreadStatus.FAST_MODE;
        }
        return ProgressActionThreadStatus.REGULAR_MODE;
    }

    private void updateStatus() {
        ProgressActionThreadStatus newStatus = calculateStatus();
        forceStatus(newStatus);
    }

    private void forceStatus(ProgressActionThreadStatus status) {
        switch (status) {
            case REGULAR_MODE: {
                mHandler.postDelayed(mProgressActions, PROCESS_QUEUE_TIME);
                break;
            }
            case FAST_MODE: {
                mProgressActions.run();
                break;
            }
        }
    }

    // ================================================================================
    // Static methods
    // ================================================================================

    public static void resetDivider() {
        mDivider = DEFAULT_DIVIDER;
    }

    public static void increaseDivider() {
        mDivider++;
    }
}
