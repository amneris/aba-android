package com.abaenglish.videoclass.domain.content;

import com.abaenglish.videoclass.analytics.legacy.LegacyTrackingManager;
import com.abaenglish.videoclass.data.persistence.ABAPhrase;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.data.persistence.ABAWrite;
import com.abaenglish.videoclass.data.persistence.ABAWriteDialog;
import com.abaenglish.videoclass.domain.ProgressService;
import com.bzutils.LogBZ;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

/**
 * Created by Marc Güell Segarra on 16/4/15.
 */
public class WriteController extends SectionController<ABAWrite> {

    @Override
    public ABAWrite getSectionForUnit(ABAUnit unit) {
        return unit.getSectionWrite();
    }

    @Override
    public boolean isSectionCompleted(ABAWrite section) {
        return getTotalElementsForSection(section) == getTotalPhraseCompletedForWriteSection(section);
    }

    @Override
    public Integer getTotalElementsForSection(ABAWrite section) {
        return getPhrasesDatasourceForWriteSection(section).size() * 2;
    }

    @Override
    public Integer getElementsCompletedForSection(ABAWrite section) {
        return getTotalPhraseCompletedForWriteSection(section);
    }

    @Override
    public void setCompletedSection(Realm realm, ABAWrite section) {
        realm.beginTransaction();
        section.setCompleted(true);
        section.setProgress(100);
        ProgressService.getInstance().updateProgressUnit(section.getUnit());
        realm.commitTransaction();

        LegacyTrackingManager.getInstance().trackEvent(realm, "unit_s3_finish", "course", "acaba escribe");
    }

    @Override
    public String getPercentageForSection(ABAWrite section) {
        return (int) getProgressForSection(section) + "%";
    }

    @Override
    public float getProgressForSection(ABAWrite section) {
        if (getTotalElementsForSection(section) == 0) {
            return 0;
        }
        float percentage = getElementsCompletedForSection(section) * 100 / getTotalElementsForSection(section);
        return percentage > 100 ? 100 : percentage;
    }

    @Override
    public ABAPhrase phraseWithID(String idPhrase, String page, ABAWrite writeSection) {
        List<ABAPhrase> phrasesList = getPhrasesDatasourceForWriteSection(writeSection);
        for (ABAPhrase phrase : phrasesList) {
            if(phrase.getAudioFile() != null && !phrase.getAudioFile().isEmpty() && phrase.getIdPhrase().equals(idPhrase)) {
                return phrase;
            }
        }
        return null;
    }

    @Override
    public void syncCompletedActions(Realm realm, ABAWrite section, JSONArray actions) throws IllegalStateException {
        for (int i = 0; i < actions.length(); i++) {
            try {
                JSONObject action = actions.getJSONObject(i);

                if (action.getString("IncorrectText").equals("null")) {
                    ABAPhrase phrase = phraseWithID(action.getString("Audio"), "", section);

                    if (phrase == null || phrase.isDone()) continue;

                    setCurrentPhraseDone(phrase);

                    if (isSectionCompleted(section) && !section.isCompleted()) {
                        section.setCompleted(true);
                        section.setProgress(100);
                    }
                }
            } catch (JSONException e) {
                LogBZ.printStackTrace(e);
            }
        }
    }

    @Override
    public ABAPhrase getCurrentPhraseForSection(ABAWrite section, int position) {
        throw new UnsupportedOperationException();
    }

    public int getTotalPhraseCompletedForWriteSection(ABAWrite writeSection) {
        int total = 0;
        for (ABAPhrase phrase : getPhrasesDatasourceForWriteSection(writeSection)) {
            if (phrase.isListened())
                total++;

            if (phrase.isDone())
                total++;
        }
        return total;
    }

    public List<ABAPhrase> getPhrasesDatasourceForWriteSection(ABAWrite writeSection) {
        List<ABAPhrase> datasource = new ArrayList<>();
        for (ABAWriteDialog dialog : writeSection.getContent()) {
            for (ABAPhrase phrase : dialog.getDialog()) {
                datasource.add(phrase);
            }
        }
        return datasource;
    }

    public List<String> getAllAudioIDsForSection(ABAWrite writeSection) {
        List<ABAPhrase> phrasesList = getPhrasesDatasourceForWriteSection(writeSection);
        List<String> audioIds = new ArrayList<>();
        for (ABAPhrase phrase : phrasesList) {
            audioIds.add(phrase.getAudioFile());
        }
        return audioIds;
    }

    public ABAPhrase getCurrentPhrase(ABAWrite writeSection) {
        List<ABAPhrase> phrasesList = getPhrasesDatasourceForWriteSection(writeSection);
        for (ABAPhrase phrase : phrasesList) {
            if (!phrase.isDone()) {
                return phrase;
            }
        }
        return null;
    }

    public void saveProgressForWriteSection(Realm realm, ABAWrite section, ABAUnit unit, ABAPhrase phrase, String errorText, Boolean isListen, Boolean isHelp) {
        saveProgressActionForSection(realm, section, unit, phrase, errorText, isListen, isHelp);
    }
}
