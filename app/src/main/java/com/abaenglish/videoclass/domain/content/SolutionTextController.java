package com.abaenglish.videoclass.domain.content;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;

import com.abaenglish.videoclass.R;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Marc Güell Segarra on 28/5/15.
 */

public class SolutionTextController {

    String[] dayNamesArray = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
    String[] monthNamesArray = {"January", "February", "March", "April", "June", "July", "August", "September", "October", "November", "December"};
    String[] nationsAndLanguagesNamesArray = {"Italian"};
    String[] properNamesArray = {"Tom", "London"};
    String[] contractedForms = {"don't", "doesn't", "didn't", "'m not", "'m", "isn't", "'s", "aren't", "'re", "wasn't", "weren't", "haven't", "hasn't", "hadn't", "'ll", "won't", "'d", "wouldn't", "can't", "couldn't", "mustn't", "shouldn't", "oughtn't to", "'ve got", "'s got", "'d got"};
    String[] uncontractedForms = {"do not", "does not", "did not", "am not", "am", "is not", "is", "are not", "are", "was not", "were not", "have not", "has not", "had not", "will", "will not", "would", "would not", "cannot", "could not", "must not", "should not", "ought not to", "have got", "has got", "had got"};

    String[] specialWords = {"Côte", "café", "cafés","façades"};
    String[] specialWordsMatch = {"Cote", "cafe", "cafes","facades"};

    public boolean mustChangeOriginalText = false;
    public boolean strictContractForms = false;

    public String formatSolutionText(String text) {

        // We remove the special characters
        text = text.replace(".", " ");
        text = text.replace("?", " ");
        text = text.replace("!", " ");
        text = text.replace(",", " ");
        text = text.replace(":", " ");
        text = text.replace("\"", " ");

        // Removing whitespaces and newlines
        text = text.trim();

        // Avoid two or more whitespaces
        text = text.replaceAll("\\s+", " ");

        return text;
    }

    public boolean canLowerCase(String word) {
        if (word.length() > 1) {
            if (word.substring(0, 2).equals("I'")) {
                return false;
            }
        }
        // Check for I
        if (word.equals("I")) {
            return false;
        }
        // check for dayNamesArray
        if (checkIfWordExists(word, dayNamesArray)) {
            return false;
        }
        // check for monthNamesArray
        if (checkIfWordExists(word, monthNamesArray)) {
            return false;
        }
        // check for nationsAndLanguagesNamesArray
        if (checkIfWordExists(word, nationsAndLanguagesNamesArray)) {
            return false;
        }
        // check for properNamesArray
        if (checkIfWordExists(word, properNamesArray)) {
            return false;
        }
        return true;
    }

    private boolean checkIfWordExists(String word, String[] array) {
        for (String wordInArray : array) {
            if (word.equals(wordInArray.toLowerCase()) || word.equals(wordInArray)) {
                return true;
            }
        }
        return false;
    }

    private boolean compareWords(String word, String correctWord) {
        if (word.equals(correctWord)) {
            return true;
        } else if (canLowerCase(correctWord) && correctWord.toLowerCase().equals(word.toLowerCase())) {
            mustChangeOriginalText = true;
            return true;
        }

        return false;
    }

    public HashMap<Integer, String> checkSolutionText(String userText, String correctText) {
        mustChangeOriginalText = false;

        HashMap<Integer, String> errorDict = new HashMap<>();

        userText = convertSpecialWords(userText);
        correctText = convertSpecialWords(correctText);

        String userTextFormatted = formatSolutionText(userText);
        String correctTextFormatted = formatSolutionText(correctText);

        String userTextUncontracted = convertToUncontractedForm(userText);
        String correctTextUncontracted = convertToUncontractedForm(correctText);


        if (strictContractForms && !userTextFormatted.equals(correctTextFormatted) || !strictContractForms && !userTextUncontracted.equals(correctTextUncontracted)) {

            String[] userTextWordArray = userTextFormatted.split(" ");
            String[] correctTextWordArray = correctTextFormatted.split(" ");

            if (correctTextWordArray.length != userTextWordArray.length) {

                if (correctTextWordArray.length > userTextWordArray.length) {    // User has left words
                    for (int i = 0; i < correctTextWordArray.length; i++) {
                        if (i <= userTextWordArray.length - 1) {
                            if (!compareWords(userTextWordArray[i], correctTextWordArray[i])) {  // Incorrect, so the incorrect string for value

                                errorDict.put(i, userTextWordArray[i]);
                            } else {  // Correct, so blank string for value
                                errorDict.put(i, "");
                            }
                        } else {
                            errorDict.put(i, correctTextWordArray[i]);
                        }
                    }
                } else {  // User has written more words than necessary
                    for (int i = 0; i < userTextWordArray.length; i++) {
                        if (i > correctTextWordArray.length - 1) {
                            errorDict.put(i, userTextWordArray[i]);
                        } else {
                            if (!compareWords(userTextWordArray[i], correctTextWordArray[i])) {  // Incorrect, so the incorrect string for value

                                errorDict.put(i, userTextWordArray[i]);
                            } else {  // Correct, so blank string for value
                                errorDict.put(i, "");
                            }
                        }
                    }
                }
            } else {     // Same number of words, so check if all are correct

                for (int i = 0; i < userTextWordArray.length; i++) {
                    if (!compareWords(userTextWordArray[i], correctTextWordArray[i])) {  // Incorrect, so the incorrect string for value

                        errorDict.put(i, userTextWordArray[i]);
                    } else {  // Correct, so blank string for value
                        errorDict.put(i, "");
                    }
                }
            }
        } else {  // All words are correct
            String[] userTextWordArray = userTextFormatted.split(" ");

            for (int i = 0; i < userTextWordArray.length; i++) {
                errorDict.put(i, "");
            }
        }

        return errorDict;
    }

    public String convertToContractedForm(String textToTransform) {

        List<String> arrayUncontractedForms = Arrays.asList(uncontractedForms);

        for (String uncontractedItem : arrayUncontractedForms) {
            int index = arrayUncontractedForms.indexOf(uncontractedItem);

            textToTransform.replace(uncontractedItem, contractedForms[index]);
        }

        return textToTransform;
    }

    public String convertToUncontractedForm(String textToTransform) {

        List<String> arrayContractedForms = Arrays.asList(contractedForms);

        // Some users will type ´ instead of ' for contracted forms.
        textToTransform = textToTransform.replaceAll("´", "'");

        for (String contractedItem : arrayContractedForms) {
            int index = arrayContractedForms.indexOf(contractedItem);
            String uncontractedItem = " " + uncontractedForms[index];
            textToTransform = textToTransform.replace(contractedItem, uncontractedItem);
        }

        return formatSolutionText(textToTransform);
    }

    public String convertSpecialWords(String textToTransform) {

        List<String> arraySpecialWords = Arrays.asList(specialWords);

        for (String wordItem : arraySpecialWords) {
             if(textToTransform.contains(wordItem)){
                int index = arraySpecialWords.indexOf(wordItem);
                String wordMatchItem = specialWordsMatch[index];
                textToTransform = textToTransform.replace(wordItem, wordMatchItem);
            }
        }

        return textToTransform;
    }

    public boolean checkPhrase(String answerFormatted, HashMap<Integer, String> errorDict) {
        boolean almostOneWordIsIncorrect = false;

        String[] textArray = answerFormatted.split(" ");

        // Detect if we missed some word
        almostOneWordIsIncorrect = areThereMissingWords(answerFormatted, errorDict);

        // Detect if we entered blanks
        for (int i = 0; i < textArray.length; i++) {

            String incorrectWord = errorDict.get(i);

            if (incorrectWord != null && incorrectWord.length() > 0) {
                almostOneWordIsIncorrect = true;
            }
        }

        // We check every word
        return almostOneWordIsIncorrect;
    }

    public boolean areThereMissingWords(String answerFormatted, HashMap<Integer, String> errorDict) {
        boolean missingWords = false;

        String[] textArray = answerFormatted.split(" ");

        // Detect if we missed some word
        if (errorDict.size() != textArray.length &&
                textArray.length > 0) {

            String text = textArray[0];

            if (!text.equals("") && text.length() > 0) {
                missingWords = true;
            }
        }

        return missingWords;
    }

    public Spannable getSpannableString(String answerFormatted, HashMap<Integer, String> errorDict, Context context) {

        Spannable formattedString = new SpannableString(answerFormatted);
        String[] textArray = answerFormatted.split(" ");
        int position = 0;

        // We check every word
        for (int i = 0; i < textArray.length; i++) {
            String incorrectWord = errorDict.get(i);

            if(i!=0) {
                position++;
            }

            Object colorSpan;

            if(incorrectWord!=null && incorrectWord.length() > 0) {
                colorSpan = new ForegroundColorSpan(ContextCompat.getColor(context, R.color.recordRedColor));        // Red

            }
            else {
                colorSpan = new ForegroundColorSpan(ContextCompat.getColor(context, R.color.abaEmailGreen));        // Green
            }

            formattedString.setSpan(colorSpan, position, position+textArray[i].length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            position += textArray[i].length();
        }

        return formattedString;
    }

    public int getFirstIncorrectWordPosition(String answerFormatted, HashMap<Integer, String> errorDict) {
        boolean firstWordIncorrect = false;
        int firstWordPosition = answerFormatted.length();
        int position = 0;

        String[] textArray = answerFormatted.split(" ");

        // We check every word
        for (int i = 0; i < textArray.length; i++) {
            String incorrectWord = errorDict.get(i);

            if(i!=0) {
                position++;
            }

            if(incorrectWord!=null && incorrectWord.length() > 0) {
                if(!firstWordIncorrect) {
                    firstWordPosition = position + textArray[i].length();
                    firstWordIncorrect = true;
                }
            }

            position += textArray[i].length();
        }

        return firstWordPosition;
    }
}
