package com.abaenglish.videoclass.domain;

import com.abaenglish.videoclass.ABAApplication;
import com.abaenglish.videoclass.analytics.TrackerContract;
import com.abaenglish.videoclass.data.network.APIManager;
import com.abaenglish.videoclass.data.network.parser.ABACompletedActionsParser;
import com.abaenglish.videoclass.data.network.parser.ABAProgressParser;
import com.abaenglish.videoclass.data.persistence.ABAAPIError;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.data.persistence.ABAUser;
import com.abaenglish.videoclass.data.persistence.dao.ABAUnitDAO;
import com.abaenglish.videoclass.domain.content.DataController;
import com.abaenglish.videoclass.domain.content.DataController.ABASimpleCallback;
import com.abaenglish.videoclass.domain.content.ProgressActionController;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.android.volley.NoConnectionError;
import com.bzutils.LogBZ;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.Date;
import java.util.List;
import java.util.Map;

import io.realm.Realm;

/**
 * Created by marc on 20/4/15.
 * Refactored by Jesus Espejo on 02/03/16
 */
public class ProgressService {

    private static volatile ProgressService instance;

    public static ProgressService getInstance() {
        if (instance == null) {
            synchronized (ProgressService.class) {
                if (instance == null) {
                    instance = new ProgressService();
                }
            }
        }

        return instance;
    }

    public void getCourseProgress(ABAUser user, final ABASimpleCallback callback, final TrackerContract.Tracker tracker) {
        APIManager.getInstance().getCourseProgress(user.getUserId(), new APIManager.ABAAPIResponseCallback() {
            @Override
            public void onResponse(String response) {
                Realm realm = Realm.getInstance(ABAApplication.get().getRealmConfiguration());

                try {
                    realm.beginTransaction();
                    ABAProgressParser.parseUnitProgress(realm, response, tracker);
                    ABAProgressParser.syncLevelsProgress(realm);
                    realm.commitTransaction();

                    callback.onSuccess();

                } catch (Exception e) {
                    e.printStackTrace();
                    if (realm.isInTransaction()) {
                        realm.cancelTransaction();
                    }
                    callback.onError(new ABAAPIError("getCourseProgress error"));
                }

                realm.close();
            }

            @Override
            public void onError(Exception e) {
                callback.onError(new ABAAPIError("getCourseProgress error"));
            }
        });
    }

    public void sendProgressActionToServer(final List<Map<String, Object>> actionMaps, final List<String> actionIDS, final ABASimpleCallback callback, final TrackerContract.Tracker tracker) {
        APIManager.getInstance().sendProgressActionsToServer(actionMaps, new DataController.ABAAPICallback<JSONArray>() {
            @Override
            public void onSuccess(JSONArray jsonArray) {

                // Actions' flag to SENT so that they are not sent anymore
                ProgressActionController.markActionsAsSent(actionIDS);

                Realm realmToSaveUnitProgress = Realm.getInstance(ABAApplication.get().getRealmConfiguration());

                try {
                    realmToSaveUnitProgress.beginTransaction();
                    ABAProgressParser.parseUnitProgress(realmToSaveUnitProgress, jsonArray, tracker);
                    realmToSaveUnitProgress.commitTransaction();

                    callback.onSuccess();

                } catch (Exception e) {
                    e.printStackTrace();
                    if (realmToSaveUnitProgress.isInTransaction()) {
                        realmToSaveUnitProgress.cancelTransaction();
                    }
                    callback.onError(new ABAAPIError("getCourseProgress error"));
                }

                realmToSaveUnitProgress.close();
            }

            @Override
            public void onError(ABAAPIError error) {
                LogBZ.d(error.getError());
                callback.onError(error);
            }
        });
    }

    public void updateCompletedActions(ABAUser user, final ABAUnit unit, final ABASimpleCallback callback) {
        final String unitID = unit.getIdUnit();
        APIManager.getInstance().getCompletedActions(user.getUserId(), unit.getIdUnit(), unit.getLastChanged(), new APIManager.ABAAPIResponseCallback() {
            @Override
            public void onResponse(final String response) {
                Realm realm = Realm.getInstance(ABAApplication.get().getRealmConfiguration());
                try {

                    ABAUnit unitToUpdate = ABAUnitDAO.getUnitWithId(realm, unitID);
                    realm.beginTransaction();

                    ABACompletedActionsParser.parseCompletedActions(realm, response, unitID);
                    updateProgressUnit(unitToUpdate);
                    unitToUpdate.setLastChanged(new Date());

                    realm.commitTransaction();

                    callback.onSuccess();

                } catch (JSONException e) {
                    if (realm.isInTransaction()) {
                        realm.cancelTransaction();
                    }
                    callback.onError(new ABAAPIError("getCompletedActions JSON error"));
                } catch (IllegalStateException e) {
                    if (realm.isInTransaction()) {
                        realm.cancelTransaction();
                    }
                    callback.onError(new ABAAPIError(e.getLocalizedMessage()));
                }

                realm.close();
            }

            @Override
            public void onError(Exception e) {
                if (e instanceof NoConnectionError) {
                    callback.onSuccess();
                } else {
                    LogBZ.e("Error for getCompletedActions");
                    callback.onSuccess();
                }
            }
        });

    }

    public void updateProgressUnit(ABAUnit unit) {

        float filmProgress = unit.getSectionFilm().getProgress();
        float speakProgress = DataStore.getInstance().getSpeakController().getProgressForSection(unit.getSectionSpeak());
        float writeProgress = DataStore.getInstance().getWriteController().getProgressForSection(unit.getSectionWrite());
        float interpretProgress = DataStore.getInstance().getInterpretController().getProgressForSection(unit.getSectionInterpret());
        float videoClassProgress = unit.getSectionVideoClass().getProgress();
        float exercisesProgress = DataStore.getInstance().getExercisesController().getProgressForSection(unit.getSectionExercises());
        float vocabularyProgress = DataStore.getInstance().getVocabularyController().getProgressForSection(unit.getSectionVocabulary());
        float evaluationProgress = unit.getSectionEvaluation().getProgress();

        float unitSectionProgress = (filmProgress + speakProgress + writeProgress + interpretProgress + videoClassProgress + exercisesProgress + vocabularyProgress + evaluationProgress) / 8;

        if (unitSectionProgress > 100) unitSectionProgress = 100;

        if (unitSectionProgress > unit.getProgress()) {
            unit.setProgress(unitSectionProgress);
        }

    }
}
