package com.abaenglish.videoclass.domain.content;

import android.content.Context;

import com.abaenglish.shepherd.plugin.plugins.ShepherdForceUserTypePlugin;
import com.abaenglish.videoclass.ABAApplication;
import com.abaenglish.videoclass.BuildConfig;
import com.abaenglish.videoclass.data.network.parser.ABAUserParser;
import com.abaenglish.videoclass.data.persistence.ABAAPIError;
import com.abaenglish.videoclass.data.persistence.ABAUser;
import com.abaenglish.videoclass.data.persistence.dao.ABAUserDAO;
import com.abaenglish.videoclass.data.persistence.dao.UserDAO;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.bzutils.LogBZ;
import com.crashlytics.android.Crashlytics;

import java.io.File;
import java.util.Date;
import java.util.regex.Pattern;

import io.realm.Realm;


/**
 * Created by iaguila on 25/3/15.
 * Refactored by Jesus Espejo on 02/03/2016
 */
public class UserController {

    private static final double RECENT_LOGIN_THRESHOLD = 1000 * 60 * 60 * 24 * 10; // 10 days in millis

    private static volatile UserController instance;

    public static UserController getInstance() {
        if (instance == null) {
            synchronized (UserController.class) {
                if (instance == null) {
                    instance = new UserController();
                }
            }
        }

        return instance;
    }

    // ================================================================================
    // Public static methods
    // ================================================================================

    public void saveUserInfo(String rawJsonUser, String name, String email, String facebookId, DataController.ABAAPICallback<ABAUser> callback) {
        Realm realm = Realm.getInstance(ABAApplication.get().getRealmConfiguration());
        try {
            realm.beginTransaction();
            ABAUser parsedUser = ABAUserParser.parseABAUser(realm, rawJsonUser, name, email, facebookId);
            realm.commitTransaction();
            callback.onSuccess(parsedUser);
        } catch (Exception e) {
            Crashlytics.setString("parseUser jsonUser", rawJsonUser);
            Crashlytics.logException(e);
            if (realm.isInTransaction()) {
                realm.cancelTransaction();
            }
            callback.onError(new ABAAPIError(e.getLocalizedMessage()));
        }

        realm.close();
    }

    public static boolean isEmailValid(String email) {
        return Pattern.compile("^[\\w\\.+-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$", Pattern.CASE_INSENSITIVE).matcher(email).matches();
    }

    public static boolean isPasswordValid(String password) {
        return password != null && password.length() >= 6;
    }

    public static boolean isPasswordNotEmpty(String password) {
        return password != null && !password.isEmpty();
    }

    public static boolean isNameValid(String name) {
        return name != null && !name.trim().isEmpty();
    }

    /**
     * Method to determine whether the user accessed recently or not. There is a constant 'RECENT_LOGIN_THRESHOLD'
     * that defines the window of time within which the last login is considered recent
     *
     * @return yes if the user did login within the last 'RECENT_LOGIN_THRESHOLD' days
     */
    public boolean hasUserLoggedInRecently() {

        Realm realm = Realm.getInstance(ABAApplication.get().getRealmConfiguration());
        ABAUser currentUser = DataStore.getInstance().getUserController().getCurrentUser(realm);
        boolean recentLogin = hasUserLoggedInRecently(currentUser);
        realm.close();

        return recentLogin;
    }

    public void deleteCurrentUser() {
        ABAUserDAO.deleteCurrentUser();
    }


    // ================================================================================
    // Public methods
    // ================================================================================

    public ABAUser getCurrentUser(Realm realm) {
        return UserDAO.getCurrentUser(realm);
    }

    public boolean isUserLogged() {
        Realm realm = Realm.getInstance(ABAApplication.get().getRealmConfiguration());
        ABAUser currentUser = getCurrentUser(realm);
        Boolean isLogged = (currentUser != null);
        realm.close();

        return isLogged;
    }

    public boolean isUserPremium() {
        if (BuildConfig.FLAVOR.equals("internal")) {
            if (ShepherdForceUserTypePlugin.getInstance().shouldForcePremium()) {
                return true;
            } else if (ShepherdForceUserTypePlugin.getInstance().shouldForceFree()) {
                return false;
            }
        }
        Boolean isPremium = false;
        Realm realm = Realm.getInstance(ABAApplication.get().getRealmConfiguration());
        ABAUser currentUser = getCurrentUser(realm);
        if (currentUser != null) {
            try {
                // Reading user type. Sometimes it's an empty string. That will rise an exception that will be properly catched.
                int userType = Integer.parseInt(currentUser.getUserType());
                if (userType == 2) {
                    isPremium = true;
                }
                // Checking if user belongs to usertype4(teacher). Like Premium.
                else if (userType == 4) {
                    isPremium = true;
                }
            } catch (Exception e) {
                // e.printStackTrace();
                LogBZ.d("User type is empty: " + currentUser.getUserType());
                LogBZ.d("User is NOT premium");
            }
        }

        realm.close();
        return isPremium;
    }

    public String updatePasswordWithJson(String json) {
        try {
            String newToken = ABAUserParser.parseToken(json);

            Realm realm = Realm.getInstance(ABAApplication.get().getRealmConfiguration());
            ABAUser currentUser = getCurrentUser(realm);
            realm.beginTransaction();
            currentUser.setToken(newToken);
            realm.commitTransaction();
            realm.close();

            return newToken;

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    //================================================================================
    // Private methods
    //================================================================================

    private static boolean hasUserLoggedInRecently(ABAUser user) {
        // Checking data integrity
        if (user == null || user.getLastLoginDate() == null) {
            return false;
        }

        long nowTimeMillis = new Date().getTime();
        long lastLoginTimeMillis = user.getLastLoginDate().getTime();
        long ellapsedTimeMillis = nowTimeMillis - lastLoginTimeMillis;
        if (Math.abs(ellapsedTimeMillis) < RECENT_LOGIN_THRESHOLD) {
            return true;
        }

        return false;
    }

    private void deleteAppCache(Context context) {
        try {
            File dir = context.getCacheDir();
            if (dir != null && dir.isDirectory()) {
                deleteDir(dir);
            }
        } catch (Exception e) {
        }
    }

    private boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }
        return dir.delete();
    }
}
