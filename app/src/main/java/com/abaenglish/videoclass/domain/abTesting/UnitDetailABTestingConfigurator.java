package com.abaenglish.videoclass.domain.abTesting;

import android.app.Activity;
import android.content.Intent;

import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.presentation.section.SectionActivityInterface;
import com.abaenglish.videoclass.presentation.section.SectionType;
import com.abaenglish.videoclass.presentation.shell.MenuActivity;
import com.abaenglish.videoclass.presentation.unit.DetailUnitActivity;
import com.abaenglish.videoclass.presentation.unit.variation.DetailUnitActivityVariation;

import javax.inject.Inject;

/**
 * Created by Jesus Espejo using mbp13jesus on 10/10/16.
 */
public class UnitDetailABTestingConfigurator {

    private final ServerConfig serverConfig;

    @Inject
    public UnitDetailABTestingConfigurator(ServerConfig serverConfig) {
        this.serverConfig = serverConfig;
    }

    public void startUnitDetailIntent(Activity context, String unitID) {
        openIntentWithABPath(context, unitID, -1, null);
    }

    public void startUnitDetailIntent(Activity context, String unitID, SectionType finishedSection) {
        openIntentWithABPath(context, unitID, -1, finishedSection);
    }

    public void startUnitDetailIntent(Activity content, String unitID, long sectionID) {
        openIntentWithABPath(content, unitID, sectionID, null);
    }

    // ================================================================================
    // Private methods - AB path resolution
    // ================================================================================

    private void openIntentWithABPath(Activity context, String unitID, long sectionID, SectionType finishedSection) {

        String ctaText = context.getString(R.string.gain_access_unit_list_button);
        startList(context, unitID, sectionID, finishedSection, ctaText);

        /*
        // Production code
        switch (detailType()) {
            case LIST_1: {
                String ctaText = context.getString(R.string.gain_access_unit_list_button);
                startList(context, unitID, sectionID, finishedSection, ctaText);
                break;
            }

            case LIST_2: {
                String ctaText = context.getString(R.string.gain_access_unit_list_button_alternative);
                startList(context, unitID, sectionID, finishedSection, ctaText);
                break;
            }
            default: {
                // case CONTROL:
                startRosco(context, unitID, sectionID, finishedSection);
                Log.d("ABLISTROSCO", " ------------------------------------ Unknown type");
                break;
            }
        }
        */
    }

    // ================================================================================
    // Private methods - Detail type
    // ================================================================================

//    private
//    @UnitDetailMode
//    String detailType() {
//        // Debug mode
//        if (ABAShepherdEditor.isInternal()) {
//            if (ShepherdABTestUnitDetailPlugin.getInstance().shouldForceRosco()) {
//                return CONTROL;
//            } else if (ShepherdABTestUnitDetailPlugin.getInstance().shouldForceList()) {
//                return LIST_1;
//            } else if (ShepherdABTestUnitDetailPlugin.getInstance().shouldForceList2()) {
//                return LIST_2;
//            }
//        }
//
//        return serverConfig.getUnitDetailScreen();
//    }

    // ================================================================================
    // Private methods - Intent creation
    // ================================================================================

    /**
     * Configura el intent del detalle de la unidad tipo Rosco
     *
     * @param context         el contexto desde el que se abre
     * @param unitID          la unidad que tiene que abrir, como unitid
     * @param finishedSection Si volvemos de una sección finalizada, le pasamos la sección
     */
    private void startRosco(Activity context, String unitID, long sectionID, SectionType finishedSection) {
        Intent intent = new Intent(context, DetailUnitActivity.class);
        intent.putExtra(SectionActivityInterface.EXTRA_UNIT_ID, unitID);

        if (finishedSection != null) {
            intent.putExtra(DetailUnitActivity.EXTRA_COMPLETED_SECTION, finishedSection.getValue());
        }
        if (sectionID != -1) {
            intent.putExtra(DetailUnitActivity.OPEN_SECTION, sectionID);
        }
        context.startActivityForResult(intent, MenuActivity.DETAIL_UNIT_REQUEST_CODE);
    }

    /**
     * Configura el intent del detalle de la unidad tipo lista
     *
     * @param context         el contexto desde el que se abre
     * @param unitID          la unidad que tiene que abrir, como unitid
     * @param finishedSection Si volvemos de una sección finalizada, le pasamos la sección
     */
    private void startList(Activity context, String unitID, long sectionID, SectionType finishedSection, String ctaText) {
        Intent intent = new Intent(context, DetailUnitActivityVariation.class);
        intent.putExtra(SectionActivityInterface.EXTRA_UNIT_ID, unitID);

        if (finishedSection != null) {
            intent.putExtra(DetailUnitActivityVariation.EXTRA_COMPLETED_SECTION, finishedSection.getValue());
        }
        if (sectionID != -1) {
            intent.putExtra(DetailUnitActivityVariation.OPEN_SECTION, sectionID);
        }
        intent.putExtra(DetailUnitActivityVariation.EXTRA_CTA_TEXT, ctaText);

        context.startActivityForResult(intent, MenuActivity.DETAIL_UNIT_REQUEST_CODE);
    }
}
