package com.abaenglish.videoclass.domain.content;

import com.abaenglish.videoclass.analytics.legacy.LegacyTrackingManager;
import com.abaenglish.videoclass.data.persistence.ABAAPIError;
import com.abaenglish.videoclass.data.persistence.ABAEvaluation;
import com.abaenglish.videoclass.data.persistence.ABAEvaluationOption;
import com.abaenglish.videoclass.data.persistence.ABAEvaluationQuestion;
import com.abaenglish.videoclass.data.persistence.ABAPhrase;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.domain.ProgressService;
import com.abaenglish.videoclass.domain.content.DataController.ABASimpleCallback;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.abaenglish.videoclass.presentation.base.ABAMasterActivity;
import com.abaenglish.videoclass.presentation.base.custom.ABAProgressDialog;
import com.bzutils.LogBZ;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;

/**
 * Created by Marc Güell Segarra on 16/4/15.
 *
 * TODO
 * LISKOV!!!!!!!!!!!!!!!!!!
 */

public class EvaluationController extends SectionController<ABAEvaluation> {

    @Override
    public boolean isSectionCompleted(ABAEvaluation evaluation) {
        return evaluation.isCompleted();
    }

    @Override
    public ABAEvaluation getSectionForUnit(ABAUnit unit) {
        return unit.getSectionEvaluation();
    }

    @Override
    public Integer getTotalElementsForSection(ABAEvaluation section) {
        return section.getContent().size();
    }

    @Override
    public void setCompletedSection(Realm realm, ABAEvaluation section) throws IllegalStateException {
        section.setCompleted(true);
        section.setProgress(100);
        ProgressService.getInstance().updateProgressUnit(section.getUnit());

        LegacyTrackingManager.getInstance().trackEvent(realm, "unit_s8_finish", "course", "acaba evaluación");
    }

    @Override
    public Integer getElementsCompletedForSection(ABAEvaluation section) {
        int correctAnswers = 0;
        for (ABAEvaluationQuestion question : section.getContent()) {
            if (question.isAnswered()) {
                for (ABAEvaluationOption option : question.getOptions()) {
                    if (option.isSelected() && option.isGood()) {
                        correctAnswers++;
                    }
                }
            }
        }
        return correctAnswers;
    }

    @Override
    public String getPercentageForSection(ABAEvaluation section) {
        throw new UnsupportedOperationException();
    }

    @Override
    public float getProgressForSection(ABAEvaluation section) {
        throw new UnsupportedOperationException();
    }

    @Override
    public ABAPhrase getCurrentPhraseForSection(ABAEvaluation section, int position) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void syncCompletedActions(Realm realm, ABAEvaluation section, JSONArray actions) {
        throw new UnsupportedOperationException();
    }

    public static List<ABAEvaluationOption> getEvaluationAnswers(ABAEvaluation evaluation) {
        List<ABAEvaluationOption> answers = new ArrayList<>();

        for (ABAEvaluationQuestion question : evaluation.getContent()) {
            if (question.isAnswered()) {
                for (ABAEvaluationOption option : question.getOptions()) {
                    if (option.isSelected()) {
                        answers.add(option);
                    }
                }
            }
        }
        return answers;
    }

    public ArrayList<Integer> getExercisesWithNoCorrectAnswers(ABAEvaluation evaluation) {
        ArrayList<Integer> listNoCorrectAnswers = new ArrayList<>();

        for (ABAEvaluationQuestion question : evaluation.getContent()) {
            if (question.isAnswered()) {
                if (isOneOptionCorrect(question.getOptions())) {
                    listNoCorrectAnswers.add(question.getOrder());
                }
            }
        }
        return listNoCorrectAnswers;
    }

    private boolean isOneOptionCorrect(RealmList<ABAEvaluationOption> options) {
        for (ABAEvaluationOption option : options) {
            if (option.isSelected() && option.isGood()) {
                return false;
            }
        }
        return true;
    }

    public ABAEvaluationOption getUserAnswersAtExercise(ABAEvaluation evaluation, int currentExercise) {
        for (ABAEvaluationQuestion question : evaluation.getContent()) {
            if (question.isAnswered() && evaluation.getContent().indexOf(question) == currentExercise) {
                for (ABAEvaluationOption option : question.getOptions()) {
                    if (option.isSelected()) {
                        return option;
                    }
                }
            }
        }
        return null;
    }

    public void resetEvaluationAtQuestion(Realm realm, ABAEvaluation evaluation, int currentExercise) {
        realm.beginTransaction();
        for (ABAEvaluationQuestion question : evaluation.getContent()) {
            if (question.isAnswered() && evaluation.getContent().indexOf(question) == currentExercise) {
                for (ABAEvaluationOption option : question.getOptions()) {
                    option.setSelected(false);
                }
            }
        }
        realm.commitTransaction();
    }


    public void resetEvaluationAnswers(Realm realm, ABAEvaluation evaluation) {
        realm.beginTransaction();
        for (ABAEvaluationQuestion question : evaluation.getContent()) {
            question.setAnswered(false);
            for (ABAEvaluationOption option : question.getOptions()) {
                option.setSelected(false);
            }
        }
        realm.commitTransaction();
    }

    public void evaluationQuestionAnsweredWithOption(Realm realm, ABAEvaluationOption option) {
        realm.beginTransaction();
        option.setSelected(true);
        option.getEvaluationQuestion().setAnswered(true);
        realm.commitTransaction();
    }

    public void endEvaluationWithOK(Realm realm, ABAEvaluation evaluation) {
        realm.beginTransaction();
        setCompletedSection(realm, evaluation);
        DataStore.getInstance().getLevelUnitController().setCompletedUnit(realm, evaluation.getUnit());
        realm.commitTransaction();
        saveProgressActionForSection(realm, evaluation, evaluation.getUnit(), null, null, false, false);
    }

    public void endEvaluationWithKO(Realm realm, ABAEvaluation evaluation) {
        saveProgressActionForSection(realm, evaluation, evaluation.getUnit(), null, null, false, false);
    }

//    public void goNextLevelifCurrentIsCompleted(Realm realm, final ABAMasterActivity masterActivity, long unitId) {
//        ABAUnit unitRefreshed = DataStore.getInstance().getLevelUnitController().getUnitWithId(realm, String.valueOf(unitId));
//        if (unitRefreshed.getLevel().isCompleted()) {
//            masterActivity.showProgressDialog(ABAProgressDialog.SimpleDialog);
//            DataStore.getInstance().getLevelUnitController().goNextLevel(realm, masterActivity, unitRefreshed.getLevel(), new ABASimpleCallback() {
//                @Override
//                public void onSuccess() {
//                    if (masterActivity != null)
//                        masterActivity.dismissProgressDialog();
//                }
//
//                @Override
//                public void onError(ABAAPIError error) {
//                    LogBZ.e("ERROR for change nextUser Level");
//                    if (masterActivity != null)
//                        masterActivity.dismissProgressDialog();
//                }
//            });
//        }
//    }
}
