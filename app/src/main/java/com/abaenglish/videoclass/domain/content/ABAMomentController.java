package com.abaenglish.videoclass.domain.content;

import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.UrlUtils;
import com.abaenglish.videoclass.analytics.cooladata.abaMoments.CooladataMomentsTrackingManager;
import com.abaenglish.videoclass.data.network.oauth.OAuthClient;
import com.abaenglish.videoclass.data.network.retrofit.abaMomentsUtils.ABAMomentClientDefinitions;
import com.abaenglish.videoclass.data.network.retrofit.abaMomentsUtils.ABAMomentDetails;
import com.abaenglish.videoclass.data.network.retrofit.abaMomentsUtils.ABAMomentDetails.Exercise;
import com.abaenglish.videoclass.data.network.retrofit.abaMomentsUtils.ABAMomentDetails.Item;
import com.abaenglish.videoclass.data.network.retrofit.clients.ABAMomentClient;
import com.abaenglish.videoclass.presentation.abaMoment.ABAMomentActivity;
import com.google.gson.Gson;

import java.util.ArrayList;


/**
 * Created by Julien on 15/02/2017.
 * Controller class for {@link ABAMomentActivity}
 */
public class ABAMomentController {

    private ABAMomentActivity abaMomentActivity;
    private String abaMomentId;
    private String abaMomentType;
    private String userId;

    private ABAMomentClient mMomentClient;

    //private List<ABAMomentQuestion> abaMomentQuestions;
    private ABAMomentDetails abaMomentDetails;
    private int currentQuestion = 0;

    public enum QuestionType {textto3image, imageto3text}

    public ABAMomentController(final ABAMomentActivity abaMomentActivity, String abaMomentId, String abaMomentType, int state, String userId, ABAMomentClient client) {

        this.abaMomentId = abaMomentId;
        this.abaMomentType = abaMomentType;
        this.abaMomentActivity = abaMomentActivity;
        this.currentQuestion = state;
        this.userId = userId;
        this.mMomentClient = client;

        if (abaMomentActivity.getAbaMomentsAsJsonString() == null) {
            this.mMomentClient.fetchABAMoment(abaMomentId, new OAuthClient.OnResponseListener<ABAMomentDetails>() {
                @Override
                public void onResponseReceived(ABAMomentDetails response, String error) {

                    if (error.compareTo(ABAMomentClientDefinitions.ABAMomentErrorType.NONE.toString()) != 0) {
                        abaMomentActivity.showError((String) abaMomentActivity.getResources().getText(R.string.errorConnection));
                    } else {
                        ABAMomentController.this.abaMomentDetails = response;

                        //randomize questions and answers
                        ABAMomentController.this.abaMomentDetails.shake();

                        abaMomentActivity.setAbaMomentsAsJsonString(new Gson().toJson(ABAMomentController.this.abaMomentDetails));

                        cacheImages();
                        loadNextQuestion();
                    }
                }
            });
        } else {
            abaMomentDetails = new Gson().fromJson(abaMomentActivity.getAbaMomentsAsJsonString(), ABAMomentDetails.class);
            loadNextQuestion();
        }
    }

    public void checkAnswer(int index) {

        if (getCurrentQuestion() != null) {
            String clickedAnswerId = getCurrentQuestion().getItems().get(index).getId();

            String correctAnswerId = getCurrentQuestion().getCorrect();
            String exerciseId = getCurrentQuestion().getId();
            String exerciseType = getCurrentQuestion().getType();

            if (clickedAnswerId.compareTo(correctAnswerId) == 0) {
                //change the alpha of all bad answers
                for (int i = 0; i < 3; i++) {
                    if (i != (index - 1))
                        abaMomentActivity.startWrongAnswerAnimation(i, false);
                }

                abaMomentActivity.startCorrectAnswerAnimation(index);
                currentQuestion++;
                CooladataMomentsTrackingManager.trackMomentAnswerSelected(userId, abaMomentType, abaMomentId, exerciseId, exerciseType, clickedAnswerId, true);
            } else {
                abaMomentActivity.startWrongAnswerAnimation(index - 1, true);
                CooladataMomentsTrackingManager.trackMomentAnswerSelected(userId, abaMomentType, abaMomentId, exerciseId, exerciseType, clickedAnswerId, false);
            }
        }
    }


    public void loadNextQuestion() {
        int progress = ((currentQuestion * 100) / abaMomentDetails.getExercises().size() + 5);

        //check if data seems ok
        if (abaMomentDetails.getExercises().size() == 10) {

            //check if it was the last question
            if (currentQuestion < abaMomentDetails.getExercises().size()) {

                //prepare the questions and answers with images
                ArrayList<Element> elements = new ArrayList<>();
                for (Item item : getCurrentQuestion().getItems()) {
                    String value = item.getValue();
                    if (item.getType().compareTo("image") == 0) {
                        value = UrlUtils.getImageUrl(abaMomentActivity, abaMomentId, item.getValue());
                    }
                    elements.add(new Element(item.getId(), value));
                }

                String audio = UrlUtils.getAudioUrl(abaMomentActivity, abaMomentId, getCurrentQuestion().getItems().get(0).getAudio());

                //type of the current displayed question
                final String currentType = getCurrentIndex() == 0 ? abaMomentDetails.getExercises().get(0).getType() : abaMomentDetails.getExercises().get(getCurrentIndex() - 1).getType();

                abaMomentActivity.startNextQuestionAnimation(getQuestionType(currentType), getQuestionType(getCurrentQuestion().getType()), elements, audio, progress);
                CooladataMomentsTrackingManager.trackMomentExercise(userId, abaMomentType, abaMomentId, getCurrentQuestion().getId(), getCurrentQuestion().getType());

            } else {

                abaMomentActivity.finishWithResult();
            }

        } else {

            abaMomentActivity.showError((String) abaMomentActivity.getResources().getText(R.string.errorConnection));
        }
    }

    private QuestionType getQuestionType(String questionType) {
        if (questionType.compareTo("imageto3text") == 0) {
            return QuestionType.imageto3text;
        } else return QuestionType.textto3image;
    }

    public int getCurrentIndex() {
        return currentQuestion;
    }

    private Exercise getCurrentQuestion() {
        return abaMomentDetails.getExercises().get(currentQuestion);
    }

    private void cacheImages() {
        //caching images
        ArrayList<String> images = new ArrayList<>();
        if (abaMomentDetails.getExercises().size() == 10) {
            for (ABAMomentDetails.Exercise abaMomentQuestion : abaMomentDetails.getExercises()) {
                for (Item item : abaMomentQuestion.getItems()) {
                    if (item.getType().compareTo("image") == 0) {
                        String imageUrl = UrlUtils.getImageUrl(abaMomentActivity, abaMomentId, item.getValue());//imageBaseUrl + item.getValue() + "www@2x.png";
                        images.add(imageUrl);
                    }
                }
            }
            abaMomentActivity.loadImages(images);
        }
    }

    /**
     * Simple Pair object to pass the id and the value to display of the question to the Activity (id for testing purposes)
     */
    public class Element {

        private String id;
        private String value;

        Element(String id, String value) {
            this.id = id;
            this.value = value;
        }

        public String getId() {
            return id;
        }

        public String getValue() {
            return value;
        }
    }

}
