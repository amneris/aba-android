package com.abaenglish.videoclass.domain.content;

import com.abaenglish.videoclass.analytics.legacy.LegacyTrackingManager;
import com.abaenglish.videoclass.data.persistence.ABAInterpret;
import com.abaenglish.videoclass.data.persistence.ABAInterpretRole;
import com.abaenglish.videoclass.data.persistence.ABAPhrase;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.domain.ProgressService;
import com.bzutils.LogBZ;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

/**
 * Created by Marc Güell Segarra on 16/4/15.
 */
public class InterpretController extends SectionController<ABAInterpret> {

    @Override
    public ABAInterpret getSectionForUnit(ABAUnit unit) {
        return unit.getSectionInterpret();
    }

    @Override
    public boolean isSectionCompleted(ABAInterpret section) {
        return getNumberOfRolesCompleted(section) == section.getRoles().size();
    }

    @Override
    public Integer getTotalElementsForSection(ABAInterpret section) {
        int number = 0;
        for(ABAInterpretRole role : section.getRoles()) {
            number += getPhrasesForRole(role, section).size();
        }
        return number;
    }

    @Override
    public Integer getElementsCompletedForSection(ABAInterpret section) {
        int number = 0;
        for(ABAInterpretRole role : section.getRoles()) {
            number += getDonePhrasesForRole(role, section).size();
        }
        return number;
    }

    @Override
    public void setCompletedSection(Realm realm, ABAInterpret section) throws IllegalStateException {
        section.setCompleted(true);
        section.setProgress(100);
        ProgressService.getInstance().updateProgressUnit(section.getUnit());

        LegacyTrackingManager.getInstance().trackEvent(realm, "unit_s4_finish", "course", "acaba interpreta");
    }

    @Override
    public String getPercentageForSection(ABAInterpret section) {
        return (int) getProgressForSection(section) + "%";
    }

    @Override
    public float getProgressForSection(ABAInterpret section) {
        if (getTotalElementsForSection(section) == 0) {
            return 0;
        }
        float percentage = getElementsCompletedForSection(section) * 100 / getTotalElementsForSection(section);
        return percentage > 100 ? 100 : percentage;
    }

    @Override
    public List<String> getAllAudioIDsForSection(ABAInterpret interpretSection)  {
        List<String> audioIds = new ArrayList<>();
        for(ABAPhrase phrase : interpretSection.getDialog()) {
            audioIds.add(phrase.getAudioFile());
        }
        return audioIds;
    }

    @Override
    public ABAPhrase getCurrentPhraseForSection(ABAInterpret section,int position) {
        throw new UnsupportedOperationException();
    }

    @Override
    public ABAPhrase phraseWithID(String audioId, String page, ABAInterpret interpretSection)  {
        for(ABAPhrase phrase : interpretSection.getDialog()) {
            if(phrase.getAudioFile() != null && !phrase.getAudioFile().isEmpty() && phrase.getAudioFile().equals(audioId) && phrase.getPage().equals(page)) {
                return phrase;
            }
        }
        return null;
    }

    @Override
    public void syncCompletedActions(Realm realm, ABAInterpret section, JSONArray actions) throws IllegalStateException {
        for(int i = 0; i < actions.length(); i++) {
            try {
                JSONObject action = actions.getJSONObject(i);
                ABAPhrase phrase = phraseWithID(action.getString("Audio"), Integer.toString(action.getInt("Page")), section);

                if(phrase == null) continue;

                if(action.getString("Action").equals("LISTENED")) {
                    if(phrase.isListened()) continue;

                    checkIfRolesAreDone(realm, section);
                } else {
                    if(phrase.isDone()) continue;

                    setCurrentPhraseDone(phrase);
                    checkIfRolesAreDone(realm, section);

                   if(isSectionCompleted(section) && !section.isCompleted()) {
                       section.setCompleted(true);
                       section.setProgress(100);
                   }
                }
            } catch (JSONException e) {
                LogBZ.printStackTrace(e);
            }
        }
    }

    public ABAPhrase getCurrentPhraseForRole(ABAInterpretRole role, ABAInterpret interpretSection) {
        ABAPhrase lastDonePhrase = getLastDonePhraseForRole(role, interpretSection);
        if(lastDonePhrase == null) {    // There isn´t any phrase done
            return interpretSection.getDialog().get(0);
        } else {
            int indexForPreviousPhrase = interpretSection.getDialog().indexOf(lastDonePhrase) + 1;

            if(indexForPreviousPhrase <= interpretSection.getDialog().size() - 1)
                return interpretSection.getDialog().get(indexForPreviousPhrase);
            return interpretSection.getDialog().get(0);
        }
    }

    public ABAPhrase getLastDonePhraseForRole(ABAInterpretRole role, ABAInterpret interpretSection) {
        for(ABAPhrase phrase : interpretSection.getDialog()) {
            if(phrase.isDone() && phrase.getInterpretRole().equals(role)) {
                return phrase;
            }
        }
        return null;
    }

    public List<ABAPhrase> getDonePhrasesForRole(ABAInterpretRole role, ABAInterpret interpretSection) {
        List<ABAPhrase> donePhrases = new ArrayList<ABAPhrase>();
        for (ABAPhrase phrase : interpretSection.getDialog()) {
            if (phrase.isDone() && phrase.getInterpretRole().equals(role)) {
                donePhrases.add(phrase);
            }
        }
        return donePhrases;
    }

    public List<ABAPhrase> getPhrasesForRole(ABAInterpretRole role, ABAInterpret interpretSection) {
        List<ABAPhrase> phrases = new ArrayList<ABAPhrase>();
        for (ABAPhrase phrase : interpretSection.getDialog()) {
            if (phrase.getInterpretRole().equals(role)) {
                phrases.add(phrase);
            }
        }
        return phrases;
    }

    public List<ABAPhrase> getElementsFromSection(ABAInterpret interpretSection){
        ArrayList<ABAPhrase> phrases = new ArrayList<>();
        for(ABAPhrase phrase : interpretSection.getDialog()){
            phrases.add(phrase);
        }
        return  phrases;
    }

    public void setRoleDone(Realm realm, ABAInterpretRole role, ABAInterpret interpretSection) {
        realm.beginTransaction();

        role.setCompleted(true);

        // If all roles are completed, we set the section completed.
        if(allRolesAreCompleted(interpretSection)) {
            setCompletedSection(realm, interpretSection);
        }

        realm.commitTransaction();
    }

    public boolean allRolesAreCompleted(ABAInterpret interpretSection) {
        for(ABAInterpretRole role : interpretSection.getRoles()) {
            if(!role.isCompleted()) {
                return false;
            }
        }
        return true;
    }

    public int getNumberOfRolesCompleted(ABAInterpret interpretSection) {
        int number = 0;
        for (ABAInterpretRole role : interpretSection.getRoles()) {
            if (role.isCompleted()) {
                number++;
            }
        }
        return number;
    }

    public void eraseProgressForRole(Realm realm, ABAInterpretRole role, ABAInterpret interpretSection) {
        realm.beginTransaction();
        role.setCompleted(false);
        if(interpretSection.isCompleted())
            interpretSection.setCompleted(false);

        for(ABAPhrase phrase : getPhrasesForRole(role, interpretSection)) {
            phrase.setDone(false);
        }
        realm.commitTransaction();
    }

    public void checkIfRolesAreDone(Realm realm, ABAInterpret interpretSection) throws IllegalStateException {
        for(ABAInterpretRole role : interpretSection.getRoles()) {
            if(role.isCompleted())
                continue;

            if(getDonePhrasesForRole(role, interpretSection).size() == getPhrasesForRole(role, interpretSection).size()) {
                role.setCompleted(true);
                // If all roles are completed, we set the section completed.
                if(allRolesAreCompleted(interpretSection)) {
                    setCompletedSection(realm, interpretSection);
                }
            }
        }
    }

    public void setPhraseDoneForRole(Realm realm, ABAPhrase phrase, ABAInterpretRole role, boolean sendProgressAction) {
        setPhraseDone(realm, phrase, role.getInterpret(), sendProgressAction);

        List<ABAPhrase> phrasesForRole = getPhrasesForRole(role, role.getInterpret());
        if(phrasesForRole.indexOf(phrase) == phrasesForRole.size() -1 )
            setRoleDone(realm, role, role.getInterpret());
    }
}
