package com.abaenglish.videoclass.domain.abTesting;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;

import com.abaenglish.videoclass.BuildConfig;
import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.analytics.TrackerContract;
import com.abaenglish.videoclass.analytics.modern.MixpanelTrackingManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;

/**
 * Created by gnatok on 28/11/2016.
 * Updated by xilosada on 19/12/2016
 */
public class ServerConfigImpl implements ServerConfig {

    private static final long DEFAULT_CACHE_VALID = BuildConfig.DEBUG ? 0 : 5 * 60; // 5 minutos (cuantificados en segundos) para production

    private final FirebaseRemoteConfig firebaseRemoteConfig;
    private Context context;
    private TrackerContract.Tracker tracker;

    public ServerConfigImpl(Context context, TrackerContract.Tracker tracker) {
        this.context = context;
        this.tracker = tracker;

        FirebaseApp.initializeApp(context);
        firebaseRemoteConfig = FirebaseRemoteConfig.getInstance();

        FirebaseRemoteConfigSettings settings = new FirebaseRemoteConfigSettings.Builder()
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build();

        firebaseRemoteConfig.setConfigSettings(settings);
        firebaseRemoteConfig.setDefaults(R.xml.server_config);
    }

    // ================================================================================
    // Public methods
    // ================================================================================

    public void updateConfigValuesIfNecessary() {

        firebaseRemoteConfig.fetch(DEFAULT_CACHE_VALID).addOnCompleteListener(new OnCompleteListener<Void>() {

            @Override
            public void onComplete(@NonNull Task<Void> task) {

                if (task.isSuccessful()) {

                    firebaseRemoteConfig.activateFetched();

                    // SUPER-PROPERTIES: Performing these call to update super properties
                    trackLinearCourseVariation(getLinearCourseFromRemoteConfig());
                    trackSixMonthsTierVariation(getSixMonthsTierVariation());
                    trackImprovedPlansPageVariation(getImprovedPlansPageVariation());
                    trackImprovedLinearCourseVariation(getImprovedLinearCourseFromRemoteConfig());

                    if (didConfigurationChanged()) {
                        // EVENTS: Tracking configuration changed event
                        trackConfigurationChanged();
                        updateLastConfigurationHash();
                    }
                }
            }
        });
    }

    @Override
    public boolean isLinearCourseActive() {
        return getLinearCourseFromRemoteConfig();
    }

    @Override
    public boolean isSixMonthsTierActive() {
        return getSixMonthsTierVariation();
    }

    @Override
    public boolean shouldShowImprovedPlansPage() {
        String plansPageRemoteConfig = getImprovedPlansPageVariation();

        switch (plansPageRemoteConfig) {
            case PLANS_PAGE_IMPROVED:
                return true;
            case PLANS_PAGE_CLASSIC:
                return false;
            case PLANS_PAGE_NONE:
                return false;
            default:
                return false;
        }
    }

    @Override
    public boolean isImprovedLinearCourseActive() {

        String config = getImprovedLinearCourseFromRemoteConfig();

        switch (config) {
            case ServerConfig.LINEAR_SECTIONS_IMPROVED_NONE:
                return false;
            case ServerConfig.LINEAR_SECTIONS_IMPROVED_ACTIVATED:
                return true;
            case ServerConfig.LINEAR_SECTIONS_IMPROVED_DEACTIVATED:
                return false;
            default:
                return false;
        }
    }

    // ================================================================================
    // Private methods
    // ================================================================================

    private boolean getLinearCourseFromRemoteConfig() {
        return firebaseRemoteConfig.getBoolean(LINEAR_SECTIONS);
    }

    private boolean getSixMonthsTierVariation() {
        return firebaseRemoteConfig.getBoolean(SIX_MONTHS_TIER);
    }

    private String getImprovedPlansPageVariation() {
        return firebaseRemoteConfig.getString(PLANS_PAGE);
    }

    private String getImprovedLinearCourseFromRemoteConfig() {
        return firebaseRemoteConfig.getString(LINEAR_SECTIONS_IMPROVED);
    }


    // ================================================================================
    // Private methods - Tracking
    // ================================================================================

    //Add linear course variation property to the analytics tools
    private void trackLinearCourseVariation(boolean result) {
        String variation = result ? "active" : "deactivated";
        tracker.addProperty(LINEAR_SECTIONS, variation);
        MixpanelTrackingManager.getSharedInstance(context).trackAppVariation(LINEAR_SECTIONS, variation);
    }

    //Add six months tier variation property to the analytics tools
    private void trackSixMonthsTierVariation(boolean result) {
        String variation = result ? "active" : "deactivated";
        tracker.addProperty(SIX_MONTHS_TIER, variation);
        MixpanelTrackingManager.getSharedInstance(context).trackAppVariation(SIX_MONTHS_TIER, variation);
    }

    private void trackImprovedPlansPageVariation(String variation) {
        tracker.addProperty(PLANS_PAGE, variation);
        MixpanelTrackingManager.getSharedInstance(context).trackAppVariation(PLANS_PAGE, variation);
    }

    private void trackImprovedLinearCourseVariation(String variation) {
        tracker.addProperty(LINEAR_SECTIONS_IMPROVED, variation);
        MixpanelTrackingManager.getSharedInstance(context).trackAppVariation(LINEAR_SECTIONS_IMPROVED, variation);
    }

    private void trackConfigurationChanged() {
        tracker.trackConfigurationChanged();
        MixpanelTrackingManager.getSharedInstance(ServerConfigImpl.this.context).trackConfigurationChaged();
    }

    // ================================================================================
    // Private methods - Configuration changed
    // ================================================================================

    private static final String SERVER_CONFIG_SHARED_PREFERENCES = "FirebaseSharedPreferences";
    private static final String LAST_HASH_KEY = "LastHashKey";

    private boolean didConfigurationChanged() {
        String configurationHash = calculateConfigurationHash();
        String lastConfigurationHash = getLastConfigurationHash();

        return !lastConfigurationHash.equals(configurationHash);
    }

    private String getLastConfigurationHash() {
        SharedPreferences preferences = context.getSharedPreferences(SERVER_CONFIG_SHARED_PREFERENCES, Context.MODE_PRIVATE);
        return preferences.getString(LAST_HASH_KEY, "");
    }

    private void updateLastConfigurationHash() {
        SharedPreferences preferences = context.getSharedPreferences(SERVER_CONFIG_SHARED_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(LAST_HASH_KEY, calculateConfigurationHash());
        editor.commit();
    }

    private String calculateConfigurationHash() {
        String linearCourseConfig = LINEAR_SECTIONS + getLinearCourseFromRemoteConfig();
        String improvedLinearCourseConfig = LINEAR_SECTIONS_IMPROVED + getImprovedLinearCourseFromRemoteConfig();
        String sixMonthsTierConfig = SIX_MONTHS_TIER + getSixMonthsTierVariation();
        String plansPageConfig = PLANS_PAGE + getImprovedPlansPageVariation();
        
        String configuration = linearCourseConfig + sixMonthsTierConfig + plansPageConfig + improvedLinearCourseConfig;

        return MD5(configuration);
    }

    private String MD5(String md5) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(md5.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

}
