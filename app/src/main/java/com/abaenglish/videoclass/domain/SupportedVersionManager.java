package com.abaenglish.videoclass.domain;

import android.content.Context;
import android.content.SharedPreferences;

import com.abaenglish.videoclass.BuildConfig;
import com.abaenglish.videoclass.data.network.APIManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Jesus Espejo using mbp13jesus on 26/01/16.
 */
public class SupportedVersionManager {

    private static final String ANDROID_PARSE_KEY = "android";
    private static SupportedVersionManager mSharedInstance;
    private Context mApplicationContext;

    // ================================================================================
    // Interfaces
    // ================================================================================

    public enum VersionSourceType {
        CACHE,
        CACHE_AFTER_ERROR,
        WEBSERVICE
    }

    public interface SupportedVersionResponse {
        void isSupportedVersion(boolean supported, String lastSupportedVersion);
    }

    public interface SupportedVersionCallback {
        void finishedWithVersion(String version, VersionSourceType type);
    }

    // ================================================================================
    // Shared instance
    // ================================================================================

    public SupportedVersionManager(Context aContext) {
        super();
        mApplicationContext = aContext.getApplicationContext();
    }

    public static synchronized SupportedVersionManager getSharedInstance(Context aContext) {
        if (mSharedInstance == null) {
            mSharedInstance = new SupportedVersionManager(aContext);
        }
        return mSharedInstance;
    }

    // ================================================================================
    //
    // ================================================================================

    public void isCurrentVersionSupported(final SupportedVersionResponse response) {
        performUpdateIfNeeded(new SupportedVersionCallback() {
            @Override
            public void finishedWithVersion(String lastSupportedVersion, VersionSourceType type) {
                String appVersion = BuildConfig.VERSION_NAME;
                Boolean supported = checkCurrentVersionSupport(appVersion, lastSupportedVersion);
                response.isSupportedVersion(supported, lastSupportedVersion);
            }
        });
    }

    public boolean checkCurrentVersionSupport(String currentVersion, String lastSupportedVersion) {

        // Empty lastSupportedVersion means all versions are supported
        if (lastSupportedVersion.equalsIgnoreCase("")) {
            return true;
        }

        try {
            String[] currentVersionSplit = currentVersion.split("\\.");
            String[] lastSupportedVersionSplit = lastSupportedVersion.split("\\.");

            for (int index = 0; index < Math.min(currentVersionSplit.length, lastSupportedVersionSplit.length); index++) {

                Integer currentVersionPiece = Integer.parseInt(currentVersionSplit[index]);
                Integer lastSupportedVersionPiece = Integer.parseInt(lastSupportedVersionSplit[index]);

                if (currentVersionPiece < lastSupportedVersionPiece) {
                    return false;
                } else if (currentVersionPiece > lastSupportedVersionPiece) {
                    return true;
                }
                // else goes to next index
            }

            // once here versions split were the same
            if (currentVersionSplit.length >= lastSupportedVersionSplit.length) {
                // case "1.0.0.0" vs "1.0.0" or "1.0", "1.0"
                return true;
            }

            // case "1.0.0" vs "1.0.0.0"
            return false;

        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        // Case: exception
        return false;
    }

    // ================================================================================
    // Conditional web call
    // ================================================================================

    public void performUpdateIfNeeded(final SupportedVersionCallback callback) {
        if (shouldPerformUpdate()) {
            updateLastSupportedVersion(callback);
        } else {
            callback.finishedWithVersion(readSupportedVersionFromCache(), VersionSourceType.CACHE);
        }
    }

    // ================================================================================
    // Web call
    // ================================================================================

    public void updateLastSupportedVersion(final SupportedVersionCallback callback) {
        APIManager.getInstance().lastSupportedVersion(new APIManager.ABAAPIResponseCallback() {
            @Override
            public void onResponse(String response) {

                String lastSupportedVersion = readSupportedVersionFromCache();
                VersionSourceType sourceType = VersionSourceType.CACHE_AFTER_ERROR;

                try {
                    String parsedVersion = parseSupportedVersion(response);
                    if (checkVersionIntegrity(parsedVersion)) {
                        saveSupportedVersionToCache(parsedVersion);
                        lastSupportedVersion = parsedVersion;
                        sourceType = VersionSourceType.WEBSERVICE;
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                callback.finishedWithVersion(lastSupportedVersion, sourceType);
            }

            @Override
            public void onError(Exception e) {
                callback.finishedWithVersion(readSupportedVersionFromCache(), VersionSourceType.CACHE_AFTER_ERROR);
            }
        });
    }

    // ================================================================================
    // Refresh calculation
    // ================================================================================

    public static final Long UPDATE_WINDOW_MILLIS = (long) 60 * 60 * 1000; // One hour in millis

    public boolean shouldPerformUpdate() {
        return shouldPerformUpdateWithMillis(new Date().getTime());
    }

    public boolean shouldPerformUpdateWithMillis(long currentTimeMillis) {
        Long lastUpdateDateMillis = readUpdateDateFromCache();
        Long elapsedTimeMillis = currentTimeMillis - lastUpdateDateMillis;
        if (elapsedTimeMillis > UPDATE_WINDOW_MILLIS) {
            return true;
        }
        return false;
    }

    // ================================================================================
    // Parser
    // ================================================================================

    public String parseSupportedVersion(String jsonToParse) throws JSONException {
        String lastSupportedVersion = null;
        JSONObject jsonObject = new JSONObject(jsonToParse);
        if (jsonObject.has(ANDROID_PARSE_KEY)) {
            lastSupportedVersion = jsonObject.getString(ANDROID_PARSE_KEY);
        }

        return lastSupportedVersion;
    }

    public boolean checkVersionIntegrity(String versionToCheck) {
        if (versionToCheck != null) {
            Pattern versionPattern = Pattern.compile("([0-9]+\\.)*[0-9]+");
            Matcher m = versionPattern.matcher(versionToCheck);
            return m.matches();
        }

        return false;
    }

    // ================================================================================
    // Cache
    // ================================================================================

    private static final String PREFERENCE_NAME = "VERSION_MANAGER_PREFERENCES";
    private static final String LAST_SUPPORTED_VERSION_KEY = "LAST_SUPPORTED_VERSION_KEY";
    private static final String LAST_UPDATE_TIME_KEY = "LAST_UPDATE_TIME_KEY";

    public void saveSupportedVersionToCache(String versionToCache) {
        SharedPreferences preferences = mApplicationContext.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor preferencesEditor = preferences.edit();

        preferencesEditor.putString(LAST_SUPPORTED_VERSION_KEY, versionToCache);
        preferencesEditor.putLong(LAST_UPDATE_TIME_KEY, new Date().getTime());
        preferencesEditor.commit();
    }

    public String readSupportedVersionFromCache() {
        SharedPreferences preferences = mApplicationContext.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        return preferences.getString(LAST_SUPPORTED_VERSION_KEY, "");
    }

    public Long readUpdateDateFromCache() {
        SharedPreferences preferences = mApplicationContext.getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        return preferences.getLong(LAST_UPDATE_TIME_KEY, (long) 0);
    }

    public static void resetPreferences(Context aContext) {
        SharedPreferences preferences = aContext.getApplicationContext().getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor preferencesEditor = preferences.edit();

        preferencesEditor.putString(LAST_SUPPORTED_VERSION_KEY, null);
        preferencesEditor.putString(LAST_UPDATE_TIME_KEY, null);
        preferencesEditor.commit();
    }

}
