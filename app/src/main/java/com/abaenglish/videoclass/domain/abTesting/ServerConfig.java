package com.abaenglish.videoclass.domain.abTesting;

/**
 * Created by gnatok on 28/11/2016.
 */
public interface ServerConfig {

    String LINEAR_SECTIONS = "linear_sections";
    boolean isLinearCourseActive();

    String LINEAR_SECTIONS_IMPROVED_NONE = "none";
    String LINEAR_SECTIONS_IMPROVED_ACTIVATED = "active";
    String LINEAR_SECTIONS_IMPROVED_DEACTIVATED = "inactive";
    String LINEAR_SECTIONS_IMPROVED = "linear_sections_improved";
    boolean isImprovedLinearCourseActive();

    String SIX_MONTHS_TIER = "six_months_tier";
    boolean isSixMonthsTierActive();

    String PLANS_PAGE = "plans_page";
    String PLANS_PAGE_IMPROVED = "improved";
    String PLANS_PAGE_CLASSIC = "classic";
    String PLANS_PAGE_NONE = "none";
    boolean shouldShowImprovedPlansPage();

    void updateConfigValuesIfNecessary();
}
