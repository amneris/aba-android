package com.abaenglish.videoclass.domain.content;

import com.abaenglish.videoclass.ABAApplication;
import com.abaenglish.videoclass.analytics.TrackerContract;
import com.abaenglish.videoclass.data.persistence.ABAEvaluation;
import com.abaenglish.videoclass.data.persistence.ABAEvaluationOption;
import com.abaenglish.videoclass.data.persistence.ABAPhrase;
import com.abaenglish.videoclass.data.persistence.ABAProgressAction;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.data.persistence.ABAUser;
import com.abaenglish.videoclass.data.persistence.dao.ABAProgressActionDAO;
import com.abaenglish.videoclass.domain.ProgressService;
import com.abaenglish.videoclass.domain.content.DataController.ABASimpleCallback;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.abaenglish.videoclass.presentation.section.SectionType;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import io.realm.Realm;

/**
 * Created by jaume on 21/5/15.
 */
public class ProgressActionController<T> {
    private static String userIP = "";

    public static void setUserIP(String Ip) {
        userIP = Ip;
    }

    public enum ProgressActions {
        LISTENED("LISTENED"),
        DICTATION("DICTATION"),
        RECORDED("RECORDED"),
        WRITTEN("WRITTEN"),
        ENTER("ENTER"),
        NEWSESSION("NEWSESSION"),
        BLANK("BLANK"),
        HELP("HELP");

        private final String text;

        ProgressActions(final String text) {
            this.text = text;
        }

        public String toString() {
            return text;
        }
    }

    //================================================================================
    // Public methods - Static
    //================================================================================

    public static List<ABAProgressAction> getPendingProgressActions(Realm realm) {

        List<ABAProgressAction> currentPendingActions = ABAProgressActionDAO.getPendingProgressActions(realm);

        // Adding ABAProgressAction to a different array in order to unlink the RealmResults from the array
        List<ABAProgressAction> actions = new ArrayList<>();
        for (ABAProgressAction action : currentPendingActions) {
            actions.add(action);
        }

        return actions;
    }

    public static List<ABAProgressAction> getPendingProgressActions(Realm realm, Integer limit) {

        List<ABAProgressAction> currentPendingActions = ABAProgressActionDAO.getPendingProgressActions(realm, limit);

        // Adding ABAProgressAction to a different array in order to unlink the RealmResults from the array
        List<ABAProgressAction> actions = new ArrayList<>();
        for (ABAProgressAction action : currentPendingActions) {
            actions.add(action);
        }

        return actions;
    }

    public static Long getPendingProgressActionCount(Realm realm) {

        return ABAProgressActionDAO.getPendingActionsCount(realm);
    }

    public static void saveProgressAction(ABAProgressAction progressAction) {

        Realm realm = Realm.getInstance(ABAApplication.get().getRealmConfiguration());
        realm.beginTransaction();
        realm.copyToRealm(progressAction);
        realm.commitTransaction();
        realm.close();
    }

    public static void markActionsAsSent(List<String> actionIDS) {

        Realm realm = Realm.getInstance(ABAApplication.get().getRealmConfiguration());
        realm.beginTransaction();

        for (String actionID : actionIDS) {
            ABAProgressAction action = realm.where(ABAProgressAction.class).equalTo("actionID", actionID).findFirst();
            action.setSentToServer(true);
        }

        realm.commitTransaction();
        realm.close();
    }

    public static void sendProgressActionsToServer(Integer limit, ABASimpleCallback callback, TrackerContract.Tracker tracker) {
        Realm realmToGetProgressActions = Realm.getInstance(ABAApplication.get().getRealmConfiguration());
        List<ABAProgressAction> actions = ProgressActionController.getPendingProgressActions(realmToGetProgressActions, limit);

        if (actions.size() != 0) {

            final List<Map<String, Object>> actionMaps = new ArrayList<>();
            final List<String> actionIDS = new ArrayList<>();

            for (ABAProgressAction action : actions) {
                actionMaps.add(ProgressActionController.toMap(action));
                actionIDS.add(action.getActionID());
            }

            ProgressService.getInstance().sendProgressActionToServer(actionMaps, actionIDS, callback, tracker);
        }
        else {
            callback.onSuccess();
        }

        realmToGetProgressActions.close();
    }

    //================================================================================
    // Public methods - Instance
    //================================================================================

    public ABAProgressAction generateProgressAction(T section, ABAUnit unit, ABAUser user) {
        ABAProgressAction progressAction = new ABAProgressAction();
        progressAction.setActionID(UUID.randomUUID().toString());

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        progressAction.setTimestamp(sdf.format(new Date()));
        progressAction.setIp(userIP);
        if (unit != null)
            progressAction.setUnitId(unit.getIdUnit());
        if (user != null) {
            progressAction.setUserId(user.getUserId());
            progressAction.setIdSession(user.getIdSession());
            progressAction.setLanguage(user.getUserLang());
        }
        if (section != null)
            progressAction.setSectionType(SectionType.fromClass(section.getClass()).getValue());

        return progressAction;
    }

    public ABAProgressAction createEnterAction(T section, ABAUnit unit, ABAUser user) {
        ABAProgressAction progressAction = generateProgressAction(section, unit, user);
        progressAction.setAction(ProgressActions.ENTER.toString());
        return progressAction;
    }

    public ABAProgressAction createNewSessionAction(ABAUser user) {
        ABAProgressAction progressAction = generateProgressAction(null, null, user);
        progressAction.setAction(ProgressActions.NEWSESSION.toString());
        return progressAction;
    }

    public ABAProgressAction createProgressAction(T section, ABAUnit unit, ABAUser user, ABAPhrase phrase, Boolean isListen, Boolean isHelp, String errorText) {

        ABAProgressAction progressAction = generateProgressAction(section, unit, user);

        if (isListen) {
            progressAction.setAction(ProgressActions.LISTENED.toString());
        }
        else {
            progressAction.setAction(getActionStringFromSection(section));
        }

        if (section != null) {
            switch (SectionType.fromClass(section.getClass())) {
                case kABAFilm:
                case kVideoClase:
                    progressAction.setPage(0);
                    progressAction.setCorrect(true);
                    progressAction.setAudioID("");
                    break;
                case kEvaluacion:
                    progressAction.setPage(0);
                    progressAction.setCorrect(true);
                    progressAction.setAudioID("");

                    progressAction.setPunctuation(DataStore.getInstance().getEvaluationController().getElementsCompletedForSection((ABAEvaluation) section));
                    String options = "";

                    for (ABAEvaluationOption option : EvaluationController.getEvaluationAnswers((ABAEvaluation) section)) {
                        options = options + option.getOptionLetter() + ",";
                    }

                    if (!options.equals("")) {
                        options = options.substring(0, options.length() - 1);
                        progressAction.setOptionLettersEvaluation(options);
                    }

                    break;
                case kEjercicios:
                    if (phrase.getIdPhrase() != null) {
                        progressAction.setAudioID(phrase.getIdPhrase());
                    } else {
                        progressAction.setAudioID(phrase.getAudioFile());
                    }

                    progressAction.setPage(Integer.parseInt(phrase.getPage()));

                    if (errorText != null) {
                        progressAction.setText(errorText);
                        progressAction.setCorrect(false);
                    } else {
                        progressAction.setCorrect(true);
                    }
                    break;
                default:
                    progressAction.setAudioID(phrase.getAudioFile());
                    progressAction.setPage(Integer.parseInt(phrase.getPage()));

                    if (errorText != null) {
                        progressAction.setText(errorText);
                        progressAction.setCorrect(false);
                    } else {
                        progressAction.setCorrect(true);
                    }

                    break;
            }
        } else if (unit != null && section != null) {
            progressAction.setAction(ProgressActions.ENTER.toString());
        } else {
            progressAction.setStartSession(true);
            progressAction.setAction(ProgressActions.NEWSESSION.toString());
        }

        progressAction.setIsHelp(isHelp);

        return progressAction;
    }

    private String getActionStringFromSection(T section) {
        if (section != null) {
            switch (SectionType.fromClass(section.getClass())) {
                case kEscribe:
                    return ProgressActions.DICTATION.toString();
                case kInterpreta:
                case kVocabulario:
                case kHabla:
                    return ProgressActions.RECORDED.toString();
                case kEjercicios:
                    return ProgressActions.WRITTEN.toString();
                default:
                    return ProgressActions.BLANK.toString();
            }
        }
        return "";
    }


    public static Map<String, Object> toMap(ABAProgressAction action) {
        Map<String, Object> actionMap = new HashMap<>();

        actionMap.put("idSession", action.getIdSession());
        actionMap.put("dateInit", action.getTimestamp());
        if (action.getIp() != null)
            actionMap.put("deviceIp", action.getIp());
        actionMap.put("idUser", action.getUserId());
        actionMap.put("idSite", "3");
        //TODO ver que cambia para android o si cambia
        actionMap.put("device", "m");

        actionMap.put("action", action.getAction());

        if (action.getStartSession()) { //we're done
            return actionMap;
        }

        actionMap.put("idSectionType", Integer.toString(action.getSectionType()));
        actionMap.put("idUnit", action.getUnitId());

        if (ProgressActions.ENTER.toString().equals(action.getAction())) {
            return actionMap;
        }

        if (action.getIsHelp()) {
            actionMap.put("action", ProgressActions.HELP.toString());
            actionMap.put("audio", "");
        } else {
            actionMap.put("audio", action.getAudioID());
        }

        actionMap.put("page", Integer.toString(action.getPage()));
        String correct = action.getCorrect() ? "1" : "0";
        actionMap.put("correct", correct);

        if (action.getText() != null)
            actionMap.put("text", action.getText());
        else
            actionMap.put("text", "");

        if (action.getSectionType() == 8) {  // Evaluación
            actionMap.put("exercises", action.getOptionLettersEvaluation());
            actionMap.put("puntuation", action.getPunctuation());
        }

        return actionMap;
    }
}
