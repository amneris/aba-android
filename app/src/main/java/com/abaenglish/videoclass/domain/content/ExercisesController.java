package com.abaenglish.videoclass.domain.content;


import com.abaenglish.videoclass.analytics.legacy.LegacyTrackingManager;
import com.abaenglish.videoclass.data.persistence.ABAExercisePhraseItem;
import com.abaenglish.videoclass.data.persistence.ABAExercises;
import com.abaenglish.videoclass.data.persistence.ABAExercisesGroup;
import com.abaenglish.videoclass.data.persistence.ABAExercisesQuestion;
import com.abaenglish.videoclass.data.persistence.ABAPhrase;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.domain.ProgressService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import io.realm.Realm;

/**
 * Created by Marc Güell Segarra on 16/4/15.
 */
public class ExercisesController extends SectionController<ABAExercises> {

    @Override
    public ABAExercises getSectionForUnit(ABAUnit unit) {
        return unit.getSectionExercises();
    }

    @Override
    public boolean isSectionCompleted(ABAExercises section) {
        return getNumberOfGroupsCompleted(section) == section.getExercisesGroups().size();
    }

    @Override
    public Integer getTotalElementsForSection(ABAExercises section) {
        Integer total = 0;
        for (ABAExercisesGroup group : section.getExercisesGroups()) {
            total += group.getQuestions().size();
        }
        return total;
    }

    @Override
    public Integer getElementsCompletedForSection(ABAExercises section) {
        Integer total = 0;
        for (ABAExercisesGroup group : section.getExercisesGroups()) {
            for (ABAExercisesQuestion question : group.getQuestions()) {
                if (question.isCompleted())
                    total++;
            }
        }
        return total;
    }

    @Override
    public void setCompletedSection(Realm realm, ABAExercises section) throws IllegalStateException {
        section.setCompleted(true);
        section.setProgress(100);
        ProgressService.getInstance().updateProgressUnit(section.getUnit());

        LegacyTrackingManager.getInstance().trackEvent(realm, "unit_s6_finish", "course", "acaba ejercicios");
    }

    @Override
    public String getPercentageForSection(ABAExercises section) {
        return (int) getProgressForSection(section) + "%";
    }

    @Override
    public float getProgressForSection(ABAExercises section) {
        if (getTotalElementsForSection(section) == 0) {
            return 0;
        }
        float percentage = getElementsCompletedForSection(section) * 100 / getTotalElementsForSection(section);
        return percentage > 100 ? 100 : percentage;
    }

    @Override
    public void syncCompletedActions(Realm realm, ABAExercises section, JSONArray actions) throws IllegalStateException {
        for (int i = 0; i < actions.length(); i++) {
            try {
                JSONObject action = actions.getJSONObject(i);

                if (action.getString("Action").equals("WRITTEN") && action.getString("IncorrectText").equals("null")) {
                    ABAPhrase phrase = phraseWithID(action.getString("Audio"), action.getString("Page"), section);

                    if (phrase == null) continue;

                    setCurrentPhraseDone(phrase);
                    checkTextPhrases(phrase, section);

                    if (isSectionCompleted(section) && !section.isCompleted()) {
                        section.setCompleted(true);
                        section.setProgress(100);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public ABAPhrase phraseWithID(String audioId, String page, ABAExercises abaExercises) {
        ArrayList<ABAPhrase> phraseList = getPhrasesFromExercises(abaExercises);
        for (ABAPhrase phrase : phraseList) {
            if (phrase.getAudioFile() != null && !phrase.getAudioFile().isEmpty()) {
                if (phrase.getIdPhrase().equals(audioId) && phrase.getPage().equals(page)) {
                    return phrase;
                }
            }
        }
        return null;
    }

    @Override
    public ABAPhrase getCurrentPhraseForSection(ABAExercises section, int position) {
        throw new UnsupportedOperationException();
    }

    public ABAExercisesQuestion getCurrentABAExercisesQuestion(ABAExercises abaExercises) {
        for (ABAExercisesGroup group : abaExercises.getExercisesGroups()) {
            for (ABAExercisesQuestion question : group.getQuestions()) {
                for (ABAPhrase phrase : getBlankPhrasesForExerciseQuestion(question)) {
                    if (!phrase.isDone()) {
                        return phrase.getExercisesQuestion();
                    }
                }
            }
        }
        return null;
    }

    public void setBlankPhrasesDoneForExercisesQuestion(Realm realm, ABAExercisesQuestion exerciseQuestion, ABAExercises abaExercises) {
        ArrayList<ABAPhrase> blankPhrasesList = getBlankPhrasesForExerciseQuestion(exerciseQuestion);
        for (ABAPhrase phrase : blankPhrasesList) {
            setPhraseDone(realm, phrase, abaExercises, false);
        }
        setExercisesQuestionDone(realm, exerciseQuestion);
    }

    public void setExercisesQuestionDone(Realm realm, ABAExercisesQuestion exerciseQuestion) {
        realm.beginTransaction();
        exerciseQuestion.setCompleted(true);
        if (allQuestionsAreCompletedForExercisesGroup(exerciseQuestion.getExercisesGroup())) {
            exerciseQuestion.getExercisesGroup().setCompleted(true);

            if (allGroupsAreCompleted(exerciseQuestion.getExercisesGroup().getExercises()))
                setCompletedSection(realm, exerciseQuestion.getExercisesGroup().getExercises());
        }
        realm.commitTransaction();
    }

    public ArrayList<ABAPhrase> getBlankPhrasesForExerciseQuestion(ABAExercisesQuestion exerciseQuestion) {
        ArrayList<ABAPhrase> returnedArray = new ArrayList<>();
        for (ABAPhrase phrase : exerciseQuestion.getPhrases()) {
            if (phrase.getBlank().length() > 0) {
                returnedArray.add(phrase);
            }
        }
        return returnedArray;
    }

    public boolean allQuestionsAreCompletedForExercisesGroup(ABAExercisesGroup group) {
        for (ABAExercisesQuestion question : group.getQuestions()) {
            if (!question.isCompleted()) {
                return false;
            }
        }
        return true;
    }

    public boolean allGroupsAreCompleted(ABAExercises abaExercises) {
        for (ABAExercisesGroup group : abaExercises.getExercisesGroups()) {
            if (!group.isCompleted()) {
                return false;
            }
        }
        return true;
    }

    public ArrayList<ABAPhrase> getPhrasesFromExercises(ABAExercises abaExercises) {
        ArrayList<ABAPhrase> returnedArray = new ArrayList<>();
        for (ABAExercisesGroup group : abaExercises.getExercisesGroups()) {
            for (ABAExercisesQuestion question : group.getQuestions()) {
                returnedArray.addAll(getBlankPhrasesForExerciseQuestion(question));
            }
        }
        return returnedArray;
    }

    private void checkTextPhrases(ABAPhrase phrase, ABAExercises exercises) throws IllegalStateException {
        for (ABAExercisesGroup group : exercises.getExercisesGroups()) {
            for (ABAExercisesQuestion question : group.getQuestions()) {
                ArrayList<ABAPhrase> blankPhrases = getBlankPhrasesForExerciseQuestion(question);

                for (ABAPhrase blankPhrase : blankPhrases) {
                    if (phrase.getIdPhrase().equals(blankPhrase.getIdPhrase()))
                        setCurrentPhraseDone(blankPhrase);
                }

                if (isAllBlanksPhrasesDoneForExerciseQuestion(blankPhrases)) {
                    question.setCompleted(true);
                    if (allQuestionsAreCompletedForExercisesGroup(question.getExercisesGroup())) {
                        group.setCompleted(true);
                        if (allGroupsAreCompleted(group.getExercises())) {
                            exercises.setCompleted(true);
                            exercises.setProgress(100);
                        }
                    }
                }
            }
        }
    }

    private boolean isAllBlanksPhrasesDoneForExerciseQuestion(ArrayList<ABAPhrase> blankPhrases) {
        for (ABAPhrase phrase : blankPhrases) {
            if (!phrase.isDone()) {
                return false;
            }
        }
        return true;
    }

    private int getNumberOfGroupsCompleted(ABAExercises exercises) {
        int number = 0;
        for (ABAExercisesGroup group : exercises.getExercisesGroups()) {
            if (group.isCompleted()) {
                number++;
            }
        }
        return number;
    }

    public ArrayList<ABAExercisePhraseItem> getExercisesItemsForExerciseQuestion(ABAExercisesQuestion question) {
        ArrayList<ABAExercisePhraseItem> items = new ArrayList<>();
        for (ABAPhrase phrase : question.getPhrases()) {
            if (phrase.getBlank().length() > 0) {
                ABAExercisePhraseItem item = new ABAExercisePhraseItem();
                item.setType(ABAExercisePhraseItem.ABAExercisePhraseItemType.kABAExercisePhraseItemTypeBlank);
                item.setText(phrase.getBlank());
                item.setAudioFile(phrase.getAudioFile());
                item.setIdPhrase(phrase.getIdPhrase());
                item.setPage(phrase.getPage());

                items.add(item);
            } else {
                String[] wordsArray = phrase.getText().split(" ");

                for (int i = 0; i < wordsArray.length; i++) {
                    ABAExercisePhraseItem item = new ABAExercisePhraseItem();

                    item.setType(ABAExercisePhraseItem.ABAExercisePhraseItemType.kABAExercisePhraseItemTypeNormal);
                    item.setText(wordsArray[i] + " ");
                    item.setAudioFile(phrase.getAudioFile());
                    item.setIdPhrase(phrase.getIdPhrase());
                    item.setPage(phrase.getPage());

                    items.add(item);
                }
            }
        }
        return items;
    }

    public void setPhraseKO(Realm realm, ABAPhrase phrase, ABAExercises section, String errorText) {
        saveProgressActionForSection(realm, section, getUnitFromSection(section), phrase, errorText, false, false);
    }

    public void sendHelpAction(Realm realm, ABAPhrase phrase, ABAExercises section) {
        saveProgressActionForSection(realm, section, getUnitFromSection(section), phrase, null, false, true);
    }
}
