package com.abaenglish.videoclass.domain.content;

import com.abaenglish.videoclass.analytics.cooladata.study.CooladataStudyTrackingManager;
import com.abaenglish.videoclass.analytics.legacy.LegacyTrackingManager;
import com.abaenglish.videoclass.data.persistence.ABAPhrase;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.data.persistence.ABAUser;
import com.abaenglish.videoclass.data.persistence.ABAVideoClass;
import com.abaenglish.videoclass.domain.ProgressService;

import org.json.JSONArray;

import io.realm.Realm;

import static com.abaenglish.videoclass.analytics.cooladata.CooladataTrackingDefinitions.CooladataSectionType.ABAVideoClass;

/**
 * Created by Marc Güell Segarra on 16/4/15.
 * Edited by Jesus with regard of Cooladata on 25/04/16
 */
public class VideoClassController extends SectionController<ABAVideoClass> {

    @Override
    public ABAVideoClass getSectionForUnit(ABAUnit unit) {
        return unit.getSectionVideoClass();
    }

    @Override
    public boolean isSectionCompleted(ABAVideoClass section) {
        return section.getProgress() >= 100;
    }

    @Override
    public void setCompletedSection(Realm realm, ABAVideoClass section) {
        realm.beginTransaction();

        section.setCompleted(true);
        section.setProgress(100);
        ProgressService.getInstance().updateProgressUnit(section.getUnit());

        realm.commitTransaction();

        // Tracking ABA Analytics
        saveProgressActionForSection(realm, section, section.getUnit(), null, null, false, false);

        // Tracking
        ABAUser user = UserController.getInstance().getCurrentUser(realm);
        CooladataStudyTrackingManager.trackWatchedFilm(user.getUserId(), section.getUnit().getLevel().getIdLevel(), section.getUnit().getIdUnit(), ABAVideoClass);
        LegacyTrackingManager.getInstance().trackEvent(realm, "unit_s5_finish", "course", "acaba videoclase");
    }

    @Override
    public String getPercentageForSection(ABAVideoClass section) {
        return (int) getProgressForSection(section) + "%";
    }

    @Override
    public float getProgressForSection(ABAVideoClass section) {
        return section.getProgress();
    }

    @Override
    public ABAPhrase getCurrentPhraseForSection(ABAVideoClass section, int position) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Integer getTotalElementsForSection(ABAVideoClass section) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Integer getElementsCompletedForSection(ABAVideoClass section) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void syncCompletedActions(Realm realm, ABAVideoClass section, JSONArray actions) {
        throw new UnsupportedOperationException();
    }
}
