package com.abaenglish.videoclass.domain;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;

import com.abaenglish.videoclass.ABAApplication;

import java.util.Locale;

/**
 * Created by Jesus Espejo using mbp13jesus on 22/06/15.
 * Modified by Jesus Espejo on 09/08/16: Added original language
 */
public class LanguageController {

    private static final String CURRENT_LANGUAGE_KEY = "currentLanguageKey";
    private static final String ORIGINAL_LANGUAGE_KEY = "originalLanguageKey";
    private static final String LANGUAGE_SHARED_PREFERENCES = "languageSharedPreferences";
    private static final String DEFAULT_LANGUAGE = "en";

    private static String currentLanguage = null;
    public static String[] SUPPORTED_LANGUAGE_ARRAY = new String[]{"de", "es", "fr", "en", "it", "pt", "ru"};

    // ================================================================================
    // Public methods
    // ================================================================================

    public synchronized static String getCurrentLanguage() {
        if (currentLanguage == null) {
            currentLanguage = readCurrentLanguageFromDefaults();
        }
        return currentLanguage;
    }

    public static String getOriginalLanguage() {
        SharedPreferences preferences = ABAApplication.get().getSharedPreferences(LANGUAGE_SHARED_PREFERENCES, Context.MODE_PRIVATE);
        return preferences.getString(ORIGINAL_LANGUAGE_KEY, null);
    }

    public static void setCurrentLanguage(Context context, String language) {
        SharedPreferences preferences = context.getSharedPreferences(LANGUAGE_SHARED_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(CURRENT_LANGUAGE_KEY, language);
        currentLanguage = language;
        editor.commit();

        // Changing locale. Forcing the app to be in the specified language
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
    }

    // ================================================================================
    // Private methods
    // ================================================================================

    private static String readCurrentLanguageFromDefaults() {
        SharedPreferences preferences = ABAApplication.get().getSharedPreferences(LANGUAGE_SHARED_PREFERENCES, Context.MODE_PRIVATE);
        String language = preferences.getString(CURRENT_LANGUAGE_KEY, null);

        // There should be a language
        if (language == null) {

            // Reading device language
            String deviceLanguage = Locale.getDefault().getLanguage();

            // If there is a device language (strange there would not be one)
            if (deviceLanguage != null) {

                // normalising catalan into spanish
                if (deviceLanguage.equalsIgnoreCase("ca")) {
                    deviceLanguage = "es";
                }

                // Matching device language with other supported language
                for (String supportedLanguage : SUPPORTED_LANGUAGE_ARRAY) {
                    if (supportedLanguage.equalsIgnoreCase(deviceLanguage)) {
                        language = supportedLanguage;
                        break;
                    }
                }
            }
        }

        if (language == null) {
            language = DEFAULT_LANGUAGE;
        }

        return language;
    }

    public static void currentLanguageOverride() {
        // First storing the original device language
        saveOriginalDeviceLanguage();

        // if necessary forcing the app to be in the specified language
        if (!Locale.getDefault().getLanguage().equals(getCurrentLanguage())) {
            setCurrentLanguage(ABAApplication.get(), LanguageController.getCurrentLanguage());
        }
    }

    public static void saveOriginalDeviceLanguage() {
        SharedPreferences preferences = ABAApplication.get().getSharedPreferences(LANGUAGE_SHARED_PREFERENCES, Context.MODE_PRIVATE);
        String originalLanguage = preferences.getString(ORIGINAL_LANGUAGE_KEY, null);
        if (originalLanguage == null) {
            // Reading device language
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(ORIGINAL_LANGUAGE_KEY, Locale.getDefault().getLanguage());
            editor.commit();
        }
    }
}
