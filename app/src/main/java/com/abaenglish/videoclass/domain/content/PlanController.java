package com.abaenglish.videoclass.domain.content;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import com.abaenglish.videoclass.ABAApplication;
import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.analytics.TrackerContract;
import com.abaenglish.videoclass.analytics.legacy.LegacyTrackingManager;
import com.abaenglish.videoclass.analytics.modern.FabricTrackingManager;
import com.abaenglish.videoclass.analytics.modern.MixpanelTrackingManager;
import com.abaenglish.videoclass.data.network.APIManager;
import com.abaenglish.videoclass.data.network.parser.ABAPlanParser;
import com.abaenglish.videoclass.data.persistence.ABAAPIError;
import com.abaenglish.videoclass.data.persistence.ABAPlan;
import com.abaenglish.videoclass.data.persistence.ABAUser;
import com.abaenglish.videoclass.data.persistence.dao.PlanDAO;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.abaenglish.videoclass.presentation.base.ABAMasterActivity;
import com.android.vending.billing.IInAppBillingService;
import com.android.volley.NoConnectionError;
import com.android.volley.VolleyError;
import com.bzutils.LogBZ;
import com.crashlytics.android.Crashlytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.realm.Realm;

/**
 * Created by madalin on 18/09/15.
 * Refactored by jesus
 */
public class PlanController {

    public static final String PROMOCODE_SHARED_PREFERENCES = "PROMOCODE_SHARED_PREFERENCES";
    public static final String ORIGINAL_IDENTIFIER_SHARED_PREFERENCES = "ORIGINAL_IDENTIFIER_SHARED_PREFERENCES";
    private static final int RESULT_OK = 0;
    public static final int PURCHASE_REQUEST_CODE = 1001;
    private static final int DAYS_IN_MONTH = 30;
    private final TrackerContract.Tracker tracker;

    @Inject
    public PlanController(TrackerContract.Tracker tracker) {
        this.tracker = tracker;
    }

    public enum SubscriptionResult {
        SUBSCRIPTION_RESULT_OK,
        SUBSCRIPTION_RESTORE_OK,
        SUBSCRIPTION_RESULT_KO, // Generic error
        SUBSCRIPTION_RESTORE_GENERIC_KO, // Generic error
        SUBSCRIPTION_RESULT_KO_AT_ABA_API, // Generic error
        SUBSCRIPTION_RESULT_ALREADY_ASSIGNED,
        SUBSCRIPTION_RESULT_ERROR_FETCHING_SUBSCRIPTIONS,
        SUBSCRIPTION_RESULT_NO_SUBSCRIPTIONS
    }

    public interface ABAPlanCallback {
        void onResult(SubscriptionResult type);
    }

    // ================================================================================
    // Public methods - Plan
    // ================================================================================

    public static List<ABAPlan> getPlanContent(Realm realm) {
        List<ABAPlan> plans = PlanDAO.getPlans(realm);
        return plans;
    }

    public void fetchPlanContent(final Context context, final ABAUser user, final ABAPlanCallback callback) {
        APIManager.getInstance().fetchPlanContent(user.getUserId(), new APIManager.ABAAPIResponseCallback() {
            @Override
            public void onResponse(final String response) {
                getBillingServices(context, new DataController.ABAAPICallback<IInAppBillingService>() {
                    @Override
                    public void onSuccess(IInAppBillingService responseService) {
                        Realm realm = Realm.getInstance(ABAApplication.get().getRealmConfiguration());
                        try {
                            ABAUser user = DataStore.getInstance().getUserController().getCurrentUser(realm);

                            realm.beginTransaction();

                            // delete all previous instances From ABAPlan
                            PlanDAO.deleteAll(realm);

                            // Adding plan to user
                            List<ABAPlan> listOfParsedPlans = ABAPlanParser.parsePlans(realm, context, responseService, response);
                            for (ABAPlan parsedPlan : listOfParsedPlans) {
                                user.getPlans().add(parsedPlan);
                            }

                            // Committing transactions
                            realm.commitTransaction();

                            callback.onResult(SubscriptionResult.SUBSCRIPTION_RESULT_OK);

                        } catch (Exception e) {

                            // Logging exception if it's not runtime (avoiding non-fatals from Google API failures)
                            if (!(e instanceof RuntimeException)) {
                                Crashlytics.logException(e);
                            }

                            // Cancelling transaction
                            if (realm.isInTransaction()) {
                                realm.cancelTransaction();
                            }
                            callback.onResult(SubscriptionResult.SUBSCRIPTION_RESULT_ERROR_FETCHING_SUBSCRIPTIONS);
                        }

                        realm.close();
                    }

                    @Override
                    public void onError(ABAAPIError error) {
                        callback.onResult(SubscriptionResult.SUBSCRIPTION_RESULT_ERROR_FETCHING_SUBSCRIPTIONS);
                    }
                });
            }

            @Override
            public void onError(Exception e) {
                if (e instanceof NoConnectionError) {
                    callback.onResult(SubscriptionResult.SUBSCRIPTION_RESULT_KO);
                } else {
                    LogBZ.d("Get Plan API error: " + e.getLocalizedMessage());
                    callback.onResult(SubscriptionResult.SUBSCRIPTION_RESULT_ERROR_FETCHING_SUBSCRIPTIONS);
                }
            }
        });
    }

    // ================================================================================
    // Public methods - Subscription
    // ================================================================================

    public boolean subscribeToPlan(Context context, IInAppBillingService service, String identifier) {
        boolean isSuccessful = false;
        if (context instanceof Activity) {
            Activity activity = (Activity) context;
            try {
                Bundle buyIntentBundle = service.getBuyIntent(3, activity.getPackageName(), identifier, "subs", null);
                PendingIntent pendingIntent = buyIntentBundle.getParcelable("BUY_INTENT");
                if (pendingIntent != null) {
                    activity.startIntentSenderForResult(pendingIntent.getIntentSender(), PURCHASE_REQUEST_CODE, new Intent(), 0, 0, 0);
                    isSuccessful = true;
                    Crashlytics.log(Log.INFO, "Plans", "Successful purchase");
                }
            } catch (RemoteException | IntentSender.SendIntentException | NullPointerException e) {
                Crashlytics.logException(e);
                e.printStackTrace();
            }
        }

        return isSuccessful;
    }

    public void updateUserToPremiumWithReceipt(ABAMasterActivity activity, ABAUser user, String purchaseData, String purchaseSignature, final ABAPlanCallback callback) {
        try {
            String productID = null;
            String price = null;
            String originalPrice = null;
            String currency = null;
            String purchasePeriod = null;
            String promoCode = "";

            JSONObject jsonObject = new JSONObject(purchaseData);
            if (jsonObject.has("productId")) {
                productID = jsonObject.getString("productId");
            }

            // Reading price and currency from ABAPlan and determining purchase period
            for (ABAPlan availablePlan : getPlanContent(activity.getRealm())) {
                if (availablePlan.getDiscountIdentifier().equalsIgnoreCase(productID)) {
                    price = String.valueOf(availablePlan.getDiscountPrice());
                    originalPrice = String.valueOf(availablePlan.getOriginalPrice());
                    currency = availablePlan.getCurrency();
                    purchasePeriod = String.valueOf(getMonthFromDays(availablePlan.getDays()));
                    promoCode = availablePlan.getPlanPromocode();

                    // Saving Promocode for Product purchase
                    saveBoughtProductWithPromocode(activity, promoCode);
                    saveOriginalProductIdentifier(activity, availablePlan.getOriginalIdentifier());
                    break;
                }
            }

            prepareUserToUpdatePremiumWithPlan(user, currency, purchasePeriod, price, originalPrice, promoCode, purchaseData, purchaseSignature, callback);

        } catch (JSONException | NullPointerException e) {
            e.printStackTrace();
            notifyListener(callback, SubscriptionResult.SUBSCRIPTION_RESULT_KO);
        }
    }

    public void prepareToRestorePurchases(final ABAUser user, Realm realm, final ABAMasterActivity activity, final IInAppBillingService mService, final ABAPlanCallback callback) {
        if (getPlanContent(realm).isEmpty()) {
            DataStore.getInstance().getPlanController(tracker).fetchPlanContent(activity, user, new ABAPlanCallback() {
                @Override
                public void onResult(SubscriptionResult type) {
                    if (type == SubscriptionResult.SUBSCRIPTION_RESULT_OK) {
                        restorePurchases(user, activity, mService, callback);
                    } else {
                        notifyListener(callback, type);
                    }
                }
            });
        } else {
            restorePurchases(user, activity, mService, callback);
        }
    }

    public void restorePurchases(ABAUser user, ABAMasterActivity anActivity, IInAppBillingService mService, final ABAPlanCallback callback) {
        try {
            // Reading items from Billing API
            Bundle ownedItems = mService.getPurchases(3, anActivity.getPackageName(), "subs", null);
            int response = ownedItems.getInt("RESPONSE_CODE");
            if (response == RESULT_OK) {
                ArrayList<String> purchaseDataList = ownedItems.getStringArrayList("INAPP_PURCHASE_DATA_LIST");
                ArrayList<String> purchaseSignatureList = ownedItems.getStringArrayList("INAPP_DATA_SIGNATURE_LIST");

                if (purchaseDataList != null && purchaseSignatureList != null) {
                    restorePurchaseWithListener(user, anActivity, mService, purchaseDataList, purchaseSignatureList, callback, SubscriptionResult.SUBSCRIPTION_RESULT_NO_SUBSCRIPTIONS);
                } else {
                    notifyListener(callback, SubscriptionResult.SUBSCRIPTION_RESULT_NO_SUBSCRIPTIONS);
                }
            }
            // Connection error : (response != RESULT_OK)
            else {
                notifyListener(callback, SubscriptionResult.SUBSCRIPTION_RESULT_ERROR_FETCHING_SUBSCRIPTIONS);
            }

            // Generic error
        } catch (RemoteException | NullPointerException e) {
            e.printStackTrace();
            notifyListener(callback, SubscriptionResult.SUBSCRIPTION_RESTORE_GENERIC_KO);
        }
    }

    private void restorePurchaseWithListener(final ABAUser user, final ABAMasterActivity anActivity, final IInAppBillingService mService, final List<String> purchaseDataList, final List<String> purchaseSignatureList, final ABAPlanCallback callback, SubscriptionResult accumulatedResult) {
        if (purchaseDataList.isEmpty() || purchaseSignatureList.isEmpty()) {
            notifyListener(callback, accumulatedResult);
        } else {
            try {
                String subscriptionData = purchaseDataList.get(0);
                String purchaseSignature = purchaseSignatureList.get(0);
                JSONObject jsonObject = new JSONObject(subscriptionData);
                if (jsonObject.getInt("purchaseState") == 0) {

                    ABAPlan restorePlan = new ABAPlan();
                    restorePlan.setDiscountIdentifier(jsonObject.getString("productId"));
                    restorePlan.setOriginalIdentifier(getOriginalProductIdentifier(anActivity));
                    try {
                        ABAPlanParser.hydrateSkuFields(anActivity, mService, restorePlan);

                        String price = String.valueOf(restorePlan.getDiscountPrice());
                        String originalPrice = String.valueOf(restorePlan.getOriginalPrice());
                        String currency = restorePlan.getCurrency();
                        String purchasePeriod = getSubscriptionMonthPeriodFromProductID(restorePlan.getDiscountIdentifier());
                        String promoCode = getBoughtProductPromocode(anActivity);

                        prepareUserToUpdatePremiumWithPlan(user, currency, purchasePeriod, price, originalPrice, promoCode, subscriptionData, purchaseSignature, new ABAPlanCallback() {
                            @Override
                            public void onResult(SubscriptionResult type) {
                                switch (type) {
                                    case SUBSCRIPTION_RESULT_OK: {
                                        notifyListener(callback, SubscriptionResult.SUBSCRIPTION_RESTORE_OK);
                                        break;
                                    }
                                    case SUBSCRIPTION_RESULT_KO_AT_ABA_API: {
                                        notifyListener(callback, SubscriptionResult.SUBSCRIPTION_RESULT_KO_AT_ABA_API);
                                        break;
                                    }
                                    default: { // SUBSCRIPTION_RESULT_ALREADY_ASSIGNED
                                        purchaseDataList.remove(0);
                                        purchaseSignatureList.remove(0);
                                        restorePurchaseWithListener(user, anActivity, mService, purchaseDataList, purchaseSignatureList, callback, SubscriptionResult.SUBSCRIPTION_RESULT_ALREADY_ASSIGNED);
                                        break;
                                    }
                                }
                            }
                        });

                    } catch (Exception e) {
                        notifyListener(callback, SubscriptionResult.SUBSCRIPTION_RESULT_ERROR_FETCHING_SUBSCRIPTIONS);
                    }
                } else {
                    restorePurchaseWithListener(user, anActivity, mService, purchaseDataList.subList(1, purchaseDataList.size() - 1), purchaseSignatureList.subList(1, purchaseSignatureList.size() - 1), callback, accumulatedResult);
                }

            } catch (JSONException e) {
                e.printStackTrace();
                notifyListener(callback, SubscriptionResult.SUBSCRIPTION_RESULT_ERROR_FETCHING_SUBSCRIPTIONS);
            }
        }
    }

    private void notifyListener(ABAPlanCallback callback, SubscriptionResult accumulatedResult) {
        tracker.trackSubscriptionErrorWithSubscriptionStatus(accumulatedResult);
        MixpanelTrackingManager.getSharedInstance(ABAApplication.get()).trackSubscriptionErrorWithSubscriptionStatus(accumulatedResult);
        callback.onResult(accumulatedResult);
    }

    // ================================================================================
    // Private methods - Subscription
    // ================================================================================

    private void prepareUserToUpdatePremiumWithPlan(ABAUser user, final String currency, final String purchasePeriod, final String price, final String originalPrice, String promocode, final String completePurchaseReceipt, String purchaseSignature, final ABAPlanCallback callback) {

        APIManager.getInstance().updateUserToPremiumWithPlan(user.getUserId(), user.getCountry(), currency, purchasePeriod, price, promocode, completePurchaseReceipt, purchaseSignature, new APIManager.ABAAPIResponseCallback() {
            @Override
            public void onResponse(String response) {

                // Purchase validated server-side and therefore activated.
                callback.onResult(SubscriptionResult.SUBSCRIPTION_RESULT_OK);

                // Tracking purchase
                Context appContext = ABAApplication.get();
                try {
                    String orderId = null;
                    String productID = null;

                    JSONObject jsonObject = new JSONObject(completePurchaseReceipt);
                    if (jsonObject.has("orderId")) {
                        orderId = jsonObject.getString("orderId");
                    }
                    if (jsonObject.has("productId")) {
                        productID = jsonObject.getString("productId");
                    }

                    // Purchase is done. Sending info to analytics
                    LegacyTrackingManager.getInstance().trackTransaction(productID, price, currency, orderId);
                    MixpanelTrackingManager.getSharedInstance(appContext).trackPurchase(appContext, productID, price, originalPrice, currency, orderId);
                    FabricTrackingManager.trackPurchase(productID, price, currency, orderId);
                    tracker.trackPaid(Double.valueOf(price), currency, String.valueOf(Double.valueOf(originalPrice) - Double.valueOf(price)), purchasePeriod);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                // Tracking the purchase has been activated and finished
                tracker.trackPurchaseActivated();
                MixpanelTrackingManager.getSharedInstance(appContext).trackActivatedPurchase();

                // Resetting product identifier
                resetOriginalProductIdentifier(appContext);
                resetBoughtProductPromocode(appContext);
            }

            @Override
            public void onError(Exception e) {
                LogBZ.e("API UpdateUserToPremium ERROR :" + e.getMessage());

                int networkResponseStatusCode = 0;
                if (((VolleyError) e).networkResponse != null) {
                    networkResponseStatusCode = ((VolleyError) e).networkResponse.statusCode;
                }

                if (networkResponseStatusCode == 410) {
                    // The plan was already used to activate another user into premium
                    callback.onResult(SubscriptionResult.SUBSCRIPTION_RESULT_ALREADY_ASSIGNED);
                } else {
                    callback.onResult(SubscriptionResult.SUBSCRIPTION_RESULT_KO_AT_ABA_API);
                }

                // Resetting product identifier
                resetOriginalProductIdentifier(ABAApplication.get());
                resetBoughtProductPromocode(ABAApplication.get());
            }
        });
    }

    // ================================================================================
    // Public static methods - UI resources
    // ================================================================================

    public static String getMonthText(ABAPlan plan, Resources resources) {
        int day = plan.getDays();
        if (getMonthFromDays(day) == 1) {
            return getMonthFromDays(day) + " " + resources.getString(R.string.mes);
        } else {
            return getMonthFromDays(day) + " " + resources.getString(R.string.meses);
        }
    }

    public static String getTitleTextPlans(Realm realm) {
        List<ABAPlan> plans = getPlanContent(realm);
        return plans.get(0).getPlanTitle();
    }

    public static String getPriceWithCommaFormat(String price) {
        if (price != null && price.contains("."))
            return price.replace(".", ",");
        return price;
    }

    public static String getPriceForOneMonth(float originalPrice, int days) {
        return getPriceWithCommaFormat(String.format("%.2f", originalPrice / getMonthFromDays(days)));
    }

    public static float getMonthlyPrice(float originalPrice, int days) {
        return originalPrice / getMonthFromDays(days);
    }

    // ================================================================================
    // Private static methods
    // ================================================================================

    private static int getMonthFromDays(int days) {
        return days / DAYS_IN_MONTH;
    }

    private static String getSubscriptionMonthPeriodFromProductID(String discountIdentifier) {
        if (discountIdentifier != null && discountIdentifier.contains("m"))
            return discountIdentifier.substring(0, discountIdentifier.indexOf("m"));
        return null;
    }

    private static void saveBoughtProductWithPromocode(Context aContext, String promoCode) {
        SharedPreferences sharedPreferences = aContext.getSharedPreferences(PROMOCODE_SHARED_PREFERENCES, Context.MODE_PRIVATE);
        sharedPreferences.edit().putString(PROMOCODE_SHARED_PREFERENCES, promoCode).apply();
    }

    private static String getBoughtProductPromocode(Context aContext) {
        SharedPreferences sharedPreferences = aContext.getSharedPreferences(PROMOCODE_SHARED_PREFERENCES, Context.MODE_PRIVATE);
        return sharedPreferences.getString(PROMOCODE_SHARED_PREFERENCES, "");
    }

    public static void resetBoughtProductPromocode(Context aContext) {
        saveBoughtProductWithPromocode(aContext, null);
    }

    private static void saveOriginalProductIdentifier(Context aContext, String originalProductIdentifier) {
        SharedPreferences sharedPreferences = aContext.getSharedPreferences(ORIGINAL_IDENTIFIER_SHARED_PREFERENCES, Context.MODE_PRIVATE);
        sharedPreferences.edit().putString(ORIGINAL_IDENTIFIER_SHARED_PREFERENCES, originalProductIdentifier).apply();
    }

    private static String getOriginalProductIdentifier(Context aContext) {
        SharedPreferences sharedPreferences = aContext.getSharedPreferences(ORIGINAL_IDENTIFIER_SHARED_PREFERENCES, Context.MODE_PRIVATE);
        return sharedPreferences.getString(ORIGINAL_IDENTIFIER_SHARED_PREFERENCES, null);
    }

    public static void resetOriginalProductIdentifier(Context aContext) {
        saveOriginalProductIdentifier(aContext, null);
    }


    // ================================================================================
    // Public methods BillingService
    // ================================================================================

    public void getBillingServices(final Context context, final DataController.ABAAPICallback<IInAppBillingService> callback) {
        final boolean[] isBound = new boolean[1];
        isBound[0] = false;

        final ServiceConnection serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceDisconnected(ComponentName name) {
                callback.onError(null);
                if (isBound[0]) {
                    isBound[0] = false;
                    context.unbindService(this);
                }
            }

            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                IInAppBillingService mService = IInAppBillingService.Stub.asInterface(service);
                callback.onSuccess(mService);
                if (isBound[0]) {
                    isBound[0] = false;
                    context.unbindService(this);
                }
            }
        };

        Intent serviceIntent = new Intent("com.android.vending.billing.InAppBillingService.BIND");
        serviceIntent.setPackage("com.android.vending");
        isBound[0] = context.bindService(serviceIntent, serviceConnection, Context.BIND_AUTO_CREATE);
    }
}
