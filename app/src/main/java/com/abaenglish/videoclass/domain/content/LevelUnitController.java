package com.abaenglish.videoclass.domain.content;

import android.content.Context;
import android.os.Environment;
import android.widget.ImageView;

import com.abaenglish.videoclass.data.CourseContentService;
import com.abaenglish.videoclass.data.file.ImageDecoder;
import com.abaenglish.videoclass.data.persistence.ABALevel;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.data.persistence.dao.ABALevelDAO;
import com.abaenglish.videoclass.data.persistence.dao.ABAUnitDAO;
import com.abaenglish.videoclass.presentation.section.SectionsService;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;


/**
 * Created by marc on 8/4/15.
 * Refactored by Jesus on 16/08/2016: Removed unused methods.
 */

public class LevelUnitController {

    public static String abaFolderNoMediaPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/.nomedia";

    public List<ABALevel> allLevels(Realm realm) {
        return ABALevelDAO.getABALevels(realm);
    }

    public List<ABALevel> getAllLevelsDescending(Realm realm) {
        return ABALevelDAO.getABALevelsDescending(realm);
    }

    public ABALevel getABALevelWithId(Realm realm, String idLevel) {
        return ABALevelDAO.getABALevelWithId(realm, idLevel);
    }

    public static String getIdLevelForUnitId(String unitId) {
        int unitIdNumber = Integer.parseInt(unitId);

        if (unitIdNumber >= 1 && unitIdNumber <= 24) {
            return "1";
        } else if (unitIdNumber >= 25 && unitIdNumber <= 48) {
            return "2";
        } else if (unitIdNumber >= 49 && unitIdNumber <= 72) {
            return "3";
        } else if (unitIdNumber >= 73 && unitIdNumber <= 96) {
            return "4";
        } else if (unitIdNumber >= 97 && unitIdNumber <= 120) {
            return "5";
        } else if (unitIdNumber >= 121 && unitIdNumber <= 144) {
            return "6";
        } else return null;
    }

    public int getUnitIndexWithinLevel(String unitIdString, String idLevelString) {
        return Integer.parseInt(unitIdString) - (Integer.parseInt(idLevelString) - 1) * 24;
    }

    public ABAUnit getUnitWithId(Realm realm, String idUnit) {
        return ABAUnitDAO.getUnitWithId(realm, idUnit);
    }

    public boolean isFirstCompleteUnit(Realm realm) {
        return getCompleteUnits(realm) == 0;
    }


    public ABAUnit getUnitWithId(String idUnit, ABALevel level) {
        for (ABAUnit unit : level.getUnits()) {
            if (unit.getIdUnit().matches(idUnit)) {
                return unit;
            }
        }
        return null;
    }

    public ArrayList<ABAUnit> getIncompletedUnitsForLevel(ABALevel level) {
        ArrayList<ABAUnit> arrayUnits = new ArrayList<>();
        for (ABAUnit unit : level.getUnits()) {
            if (!unit.isCompleted()) {
                arrayUnits.add(unit);
            }
        }
        return arrayUnits;
    }

    public ArrayList<ABAUnit> getIncompletedUnitsForUnitsArray(ArrayList<ABAUnit> unitsArray) {
        ArrayList<ABAUnit> arrayUnits = new ArrayList<>();
        for (ABAUnit unit : unitsArray) {
            if (!unit.isCompleted()) {
                arrayUnits.add(unit);
            }
        }
        return arrayUnits;
    }

    public ABAUnit getCurrentUnitForLevel(ABALevel level) {

        //return firstUnit of level 6 when the course is finished
        if (level.getIdLevel().equals("6") && level.isCompleted()) {
            return level.getUnits().get(0);
        }

        ArrayList<ABAUnit> unitsSorted = getUnitsDescendingForLevel(level);
        ArrayList<ABAUnit> incompletedUnits = getIncompletedUnitsForUnitsArray(unitsSorted);

        if (incompletedUnits.size() == 0)
            return unitsSorted.get(unitsSorted.size() - 1);
        return incompletedUnits.get(incompletedUnits.size() - 1);
    }

    public void getSectionsforUnit(final Context context, final ABAUnit unit, final DataController.ABASimpleCallback callback) {
        SectionsService.getInstance().fetchSections(context, unit, callback);
    }

    public ArrayList<ABAUnit> getUnitsDescendingForLevel(ABALevel level) {
        ArrayList<ABAUnit> units = new ArrayList<>();
        for (int i = 0; i < level.getUnits().size(); i++) {
            units.add(level.getUnits().get(i));
        }

        Collections.reverse(units);
        return units;
    }

    public ABALevel getNextLevel(Realm realm, ABALevel level) {
        int numberOfNextLevel = Integer.parseInt(level.getIdLevel()) + 1;
        return numberOfNextLevel > 6 ? level : getABALevelWithId(realm, Integer.toString(numberOfNextLevel));
    }


    public double getGlobalProgressForUnit(ABAUnit unit) {
        return unit.getProgress();
    }

    public boolean checkIfFileExist(ABAUnit currentUnit, String url) {
        return new File(GenericController.getLocalFilePath(currentUnit, url)).exists();
    }

    public void setCompletedUnit(Realm realm, ABAUnit unit) throws IllegalStateException {
        unit.setCompleted(true);
        unit.setProgress(100);

        if (getCompletedUnitsForLevel(unit.getLevel()).size() == getABALevelWithId(realm, unit.getLevel().getIdLevel()).getUnits().size()) {
            unit.getLevel().setCompleted(true);
            unit.getLevel().setProgress(100);
        }
    }

//    public void goNextLevel(Realm realm, Context aContext, ABALevel level, DataController.ABASimpleCallback callback) {
//        mContentService.setCurrentLevel(aContext, getNextLevel(realm, level), callback);
//    }

    // ================================================================================
    // Private methods
    // ================================================================================

    private int getCompleteUnits(Realm realm) {
        int totalComplete = 0;
        List<ABALevel> abaLevels = ABALevelDAO.getABALevels(realm);
        for (ABALevel level : abaLevels) {
            RealmList<ABAUnit> abaUnits = level.getUnits();
            for (ABAUnit unit : abaUnits) {
                if (unit.isCompleted()) {
                    totalComplete++;
                }
            }
        }
        return totalComplete;
    }

    private ArrayList<ABAUnit> getCompletedUnitsForLevel(ABALevel level) {
        ArrayList<ABAUnit> arrayUnits = new ArrayList<>();
        for (ABAUnit unit : level.getUnits()) {
            if (unit.isCompleted()) {
                arrayUnits.add(unit);
            }
        }
        return arrayUnits;
    }

    public void displayImage(ABAUnit currentUnit, String imagePath, ImageView img) {
        String path = GenericController.getLocalFilePath(currentUnit, imagePath);
        try {
            ImageDecoder.getInstance().displayImage(path, img);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean isFreeUnit(String unitId) {
        return unitId.equals("1") ||
                unitId.equals("25") ||
                unitId.equals("49") ||
                unitId.equals("73") ||
                unitId.equals("97") ||
                unitId.equals("121");
    }
}

