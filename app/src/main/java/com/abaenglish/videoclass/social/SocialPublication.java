package com.abaenglish.videoclass.social;

/**
 * Created by xabierlosada on 05/08/16.
 */

public interface SocialPublication {
    void share();
}
