package com.abaenglish.videoclass.social;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;

import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.UrlUtils;

import java.util.List;

/**
 * Created by xabierlosada on 05/08/16.
 */

public class TwitterPublication implements SocialPublication{

    private static final String URL_SHARE_TWITTER = "https://app.adjust.com/ksjy8h";

    private final Activity activity;
    private String unitName;
    private String unitTitle;

    public TwitterPublication(Activity activity) {
        this.activity = activity;
    }

    public String generateTwitterMessage(String unitName, String unitTitle) {
//        String message;
//        if (assessmentResult.isLastUnitOfLevel()) {
//            message = activity.getString(R.string.evaluationEndLevelSharingTwitterMessage);
//            message = message.replace("%@", assessmentResult.levelName());
//        } else {
//            message = activity.getString(R.string.evaluationSharingTwitterMessage);
//            message = message.replace("%@", assessmentResult.unitName() + ". " + assessmentResult.unitTitle());
//        }
        String message;
        message = activity.getString(R.string.evaluationSharingTwitterMessage);
        message = message.replace("%@", unitName + ". " + unitTitle);
        return message;
    }

    public void configure(String unitName, String unitTitle) {
        this.unitName = unitName;
        this.unitTitle = unitTitle;
    }

    @Override
    public void share() {
        String tweetUrl =
                String.format("https://twitter.com/intent/tweet?text=%s&url=%s",
                        UrlUtils.urlEncode(generateTwitterMessage(unitName, unitTitle)), UrlUtils.urlEncode(URL_SHARE_TWITTER));
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(tweetUrl));

        List<ResolveInfo> matches = activity.getPackageManager().queryIntentActivities(intent, 0);
        for (ResolveInfo info : matches) {
            if (info.activityInfo.packageName.toLowerCase().startsWith("com.twitter")) {
                intent.setPackage(info.activityInfo.packageName);
            }
        }
        activity.startActivity(intent);
    }
}
