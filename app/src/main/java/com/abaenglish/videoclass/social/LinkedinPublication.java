package com.abaenglish.videoclass.social;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;

/**
 * Created by xabierlosada on 05/08/16.
 */

public class LinkedinPublication implements SocialPublication {

    private static final String URL_BEGINNER_LINKEDIN = "https://lnkd.in/dz2jezJ";
    private static final String URL_LOWER_INTERMEDIATE_LINKEDIN = "https://lnkd.in/eEr_zQx";
    private static final String URL_INTERMEDIATE_LINKEDIN = "https://lnkd.in/eZvtbdd";
    private static final String URL_UPPER_INTERMEDIATE_LINKEDIN = "https://lnkd.in/eJwtcjH";
    private static final String URL_ADVANCED_LINKEDIN = "https://lnkd.in/eF9rNjb";
    private static final String URL_BUSINESS_LINKEDIN = "https://lnkd.in/eUJyeP8";
    private long levelId = 1;
    private Activity activity;


    public LinkedinPublication(Activity activity) {
        this.activity = activity;
    }

    public void setLevelId(long levelId) {
        this.levelId = levelId;
    }

    public String getLinkedinUrlFromLevel(long levelId) {
        switch ( Long.valueOf(levelId).intValue()) {
            case 1:
                return URL_BEGINNER_LINKEDIN;
            case 2:
                return URL_LOWER_INTERMEDIATE_LINKEDIN;
            case 3:
                return URL_INTERMEDIATE_LINKEDIN;
            case 4:
                return URL_UPPER_INTERMEDIATE_LINKEDIN;
            case 5:
                return URL_ADVANCED_LINKEDIN;
            case 6:
                return URL_BUSINESS_LINKEDIN;
            default:
                return "";
        }
    }

    @Override
    public void share() {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(getLinkedinUrlFromLevel(levelId)));
        activity.startActivity(intent);
    }
}
