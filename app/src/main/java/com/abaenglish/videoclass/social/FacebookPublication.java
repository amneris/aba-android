package com.abaenglish.videoclass.social;

import android.app.Activity;
import android.net.Uri;

import com.abaenglish.videoclass.R;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

/**
 * Created by xabierlosada on 05/08/16.
 */

public class FacebookPublication implements SocialPublication{

    private static final String URL_SHARE_FACEBOOK = "https://app.adjust.com/wwfzjx";

    private Activity activity;

    public FacebookPublication(Activity activity) {
        this.activity = activity;
    }

    public String generateFacebookMessage() {
//        String message;
//        if (assessmentResult.isLastUnitOfLevel()) {
//            message = context.getString(R.string.evaluationEndLevelSharingFacebookMessage);
//            message = message.replace("%@", assessmentResult.levelName());
//        } else {
//            message = context.getString(R.string.evaluationSharingFacebookMessage);
//        }
        return activity.getString(R.string.evaluationSharingFacebookMessage);
    }

    @Override
    public void share() {
        ShareDialog shareDialog = new ShareDialog(activity);
        ShareLinkContent linkContent = new ShareLinkContent.Builder()
                .setContentDescription(generateFacebookMessage())
                .setContentUrl(Uri.parse(URL_SHARE_FACEBOOK))
                .build();
        shareDialog.show(linkContent);
    }
}
