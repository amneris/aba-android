package com.abaenglish.videoclass.session;

import android.content.Context;
import android.util.Log;

import com.abaenglish.videoclass.ABAApplication;
import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.analytics.FirebaseTracker;
import com.abaenglish.videoclass.analytics.TrackerContract;
import com.abaenglish.videoclass.analytics.cooladata.navigation.CooladataNavigationTrackingManager;
import com.abaenglish.videoclass.analytics.cooladata.profile.CooladataProfileTrackingManager;
import com.abaenglish.videoclass.analytics.helpers.GenericTrackingInterface;
import com.abaenglish.videoclass.analytics.legacy.LegacyTrackingManager;
import com.abaenglish.videoclass.analytics.modern.FabricTrackingManager;
import com.abaenglish.videoclass.analytics.modern.MixpanelTrackingManager;
import com.abaenglish.videoclass.data.CourseContentService;
import com.abaenglish.videoclass.data.DownloadController;
import com.abaenglish.videoclass.data.network.APIManager;
import com.abaenglish.videoclass.data.persistence.ABAAPIError;
import com.abaenglish.videoclass.data.persistence.ABAUser;
import com.abaenglish.videoclass.domain.ApplicationConfiguration;
import com.abaenglish.videoclass.domain.content.DataController;
import com.abaenglish.videoclass.domain.content.DataController.ABAAPICallback;
import com.abaenglish.videoclass.domain.content.DataController.ABASimpleCallback;
import com.abaenglish.videoclass.domain.content.PreferencesProvider;
import com.abaenglish.videoclass.domain.content.PreferencesProviderImpl;
import com.abaenglish.videoclass.domain.content.UserController;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.VolleyError;
import com.crashlytics.android.Crashlytics;

import org.json.JSONException;
import org.json.JSONObject;

import io.realm.Realm;

import static com.abaenglish.videoclass.analytics.helpers.GenericTrackingInterface.RecordType.kRecordTypeMail;

/**
 * Created by Jesus Espejo using mbp13jesus on 02/03/16.
 * Class that handles the login procedure
 */
public class SessionService {

    private static final int ERROR_CODE_EMAIL = 444;
    public static final String FACEBOOK = "facebook";
    private CourseContentService contentService;
    private ApplicationConfiguration appConfiguration;

    public SessionService(CourseContentService contentService, ApplicationConfiguration applicationConfiguration) {
        this.contentService = contentService;
        this.appConfiguration = applicationConfiguration;
    }

    // ================================================================================
    // Public methods - Login
    // ================================================================================

    public void loginWithEmail(final Context context, final String email, String password, final ABASimpleCallback callback, final TrackerContract.Tracker tracker) {
        Crashlytics.log(Log.INFO, "Login with email", "Login perform with email " + email);
        APIManager.getInstance().loginWithEmail(email, password, new APIManager.ABAAPIResponseCallback() {
            @Override
            public void onResponse(String jsonResponse) {
                // Life begins here. This will create ABAUser entity
                contentService.initializeContent(context, jsonResponse, null, email, null, new ABASimpleCallback() {
                    @Override
                    public void onSuccess() {
                        trackLogin(context);
                        callback.onSuccess();
                    }

                    @Override
                    public void onError(ABAAPIError error) {
                        callback.onError(error);
                    }
                }, tracker);
            }

            @Override
            public void onError(Exception e) {
                if (e instanceof AuthFailureError) {
                    callback.onError(new ABAAPIError(context.getString(R.string.loginError)));
                } else if (e instanceof NoConnectionError) {
                    callback.onError(new ABAAPIError(context.getString(R.string.errorConnection)));
                } else {
                    callback.onError(new ABAAPIError(context.getString(R.string.errorLogin)));
                }
            }
        });
    }

    public void loginWithToken(final Context context, final String token, final TrackerContract.Tracker tracker, final ABASimpleCallback callback) {
        Crashlytics.log(Log.INFO, "Login with token", "Login perform with token " + token);

        APIManager.getInstance().loginWithToken(token, new APIManager.ABAAPIResponseCallback() {
            @Override
            public void onResponse(String jsonResponse) {
                contentService.initializeContent(context, jsonResponse, callback, tracker);

                // Traking login event
                try {
                    JSONObject jsonObject = new JSONObject(jsonResponse);
                    String userID = jsonObject.getString("userId");
                    CooladataNavigationTrackingManager.trackLoginEventWithOfflineFlag(false, userID);
                    MixpanelTrackingManager.getSharedInstance(context).trackLoginWithToken();
                    tracker.trackUserLogin(userID, "token");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onError(Exception e) {
                if (e instanceof NoConnectionError) {
                    // In case of connection error it checks whether the user did login whithin the last days
                    if (UserController.getInstance().hasUserLoggedInRecently()) {
                        // Login offline
                        appConfiguration.setUserToken(token);
                        callback.onSuccess();

                        Realm realm = Realm.getInstance(ABAApplication.get().getRealmConfiguration());
                        ABAUser currentUser = UserController.getInstance().getCurrentUser(realm);

                        // Event to Mixpanel
                        MixpanelTrackingManager.getSharedInstance(context).trackLoginOfflineEvent();
                        FabricTrackingManager.trackLoginOfflineEvent();
                        CooladataNavigationTrackingManager.trackLoginEventWithOfflineFlag(true, currentUser != null ? currentUser.getUserId() : "");
                        return;
                    }
                }

                callback.onError(new ABAAPIError("loginWithToken - Error"));
            }
        });
    }

    public void loginWithFacebook(final Context context, String email, String facebookId, String name, String surnames, String gender, TrackerContract.Tracker tracker) {
        Crashlytics.log(Log.INFO, "Login with Facebook", "Login perform with email " + email);
        registerUserWithFacebook(context, email, facebookId, name, surnames, gender, new DataController.ABAAPIFacebookCallback() {
            @Override
            public void onSuccess(Boolean isNewUser) {
                trackFacebookAccess(context, isNewUser);
                DataStore.getInstance().getFacebookCallbackController().getFacebookLoginCallback().onSuccess(isNewUser);
            }

            @Override
            public void onError(ABAAPIError error) {
                DataStore.getInstance().getFacebookCallbackController().showFacebookError(error.getError());
            }
        }, tracker);
    }

    // ================================================================================
    // Public methods - Logout
    // ================================================================================

    public void logout(Context aContext, DataController.ABALogoutCallback callback) {

        PreferencesProvider pref = new PreferencesProviderImpl(aContext);

        Crashlytics.log(Log.INFO, "Logout", "User performs Logout");

        // Tracking logout before actually removing the user
        MixpanelTrackingManager.getSharedInstance(aContext).trackLogout();
        // TODO Kill the Singleton
        FirebaseTracker.getInstance().trackLogout();

        // removing user from DDBB
        /** now we have a crash after migration when delete from realm ABAPhrase Object
         this case is explain in follow url but for the moment is not fixed
         https://www.bountysource.com/issues/27263311-realm-is-null-sigsegv-after-updating-to-0-83-0 **/
        UserController.getInstance().deleteCurrentUser();

        // Setting token to null in order to cancel all current requests
        appConfiguration.setUserToken(null);

        MixpanelTrackingManager.getSharedInstance(aContext).reset();

        // Removing downloaded files
        DownloadController.removeDownloadedFiles();

        // Removing User Profile preferences
        pref.resetProfilePreferences();
        callback.onLogoutFinished();
    }

    // ================================================================================
    // Public methods - Register
    // ================================================================================

    public void register(final Context context, final String name, final String email, String password, final ABASimpleCallback callback, final TrackerContract.Tracker tracker) {
        Crashlytics.log(Log.INFO, "Register", "User performs registration with email " + email);
        APIManager.getInstance().registerUser(name, email, password, new APIManager.ABAAPIResponseCallback() {
            @Override
            public void onResponse(String response) {
                contentService.initializeContent(context, response, name, email, null, new ABASimpleCallback() {
                    @Override
                    public void onSuccess() {
                        callback.onSuccess();
                        trackRegister(context);
                    }

                    @Override
                    public void onError(ABAAPIError error) {
                        callback.onError(error);
                    }
                }, tracker);
            }

            @Override
            public void onError(Exception e) {
                NetworkResponse networkResponse = ((VolleyError) e).networkResponse;
                int networkResponseStatusCode = 0;
                if (networkResponse != null)
                    networkResponseStatusCode = networkResponse.statusCode;

                if (e instanceof NoConnectionError) {
                    callback.onError(new ABAAPIError(context.getString(R.string.errorConnection)));
                } else if (networkResponseStatusCode == ERROR_CODE_EMAIL) {
                    //Api return 444 code when user email has already register
                    callback.onError(new ABAAPIError(context.getString(R.string.regErrorEmailExist)));
                } else {
                    callback.onError(new ABAAPIError(context.getString(R.string.errorRegister)));
                }
            }
        });
    }

    // ================================================================================
    // Public methods - password
    // ================================================================================

    public void recoverPassword(final Context context, String email, final ABAAPICallback<Boolean> callback) {
        APIManager.getInstance().recoverPassword(email, new APIManager.ABAAPIResponseCallback() {
            @Override
            public void onResponse(String response) {
                // TODO: parseEnvironment success response
                // ...
                callback.onSuccess(true);
            }

            @Override
            public void onError(Exception e) {
                callback.onError(new ABAAPIError(context.getString(R.string.errorRecoverPass)));
            }
        });
    }

    public void checkEmailAndPassword(String email, String password, final DataController.ABAAPICallback<Boolean> callback) {
        APIManager.getInstance().loginWithEmail(email, password, new APIManager.ABAAPIResponseCallback() {
            @Override
            public void onResponse(String response) {
                callback.onSuccess(true);
            }

            @Override
            public void onError(Exception e) {
                callback.onSuccess(false);
            }
        });
    }

    public void changePassword(String nenPassworkd, final DataController.ABAAPICallback<Boolean> callback) {
        APIManager.getInstance().changePassword(nenPassworkd, new APIManager.ABAAPIResponseCallback() {
            @Override
            public void onResponse(String response) {
                String token = UserController.getInstance().updatePasswordWithJson(response);
                if (token != null) {
                    appConfiguration.setUserToken(token);
                    callback.onSuccess(true);
                } else {
                    callback.onError(new ABAAPIError("Unable to parse new credentials"));
                }
            }

            @Override
            public void onError(Exception e) {
                e.printStackTrace();
                callback.onError(new ABAAPIError(e.toString()));
            }
        });
    }

    // ================================================================================
    // Private methods - Facebook
    // ================================================================================

    public void registerUserWithFacebook(final Context context, final String email, final String facebookId, final String name, String surnames, String gender, final DataController.ABAAPIFacebookCallback callback, final TrackerContract.Tracker tracker) {
        Crashlytics.log(Log.INFO, "Register with Facebook", "User email " + email);
        APIManager.getInstance().registerUserWithFacebook(facebookId, email, name, surnames, gender, new APIManager.ABAAPIResponseCallback() {
            @Override
            public void onResponse(final String jsonResponse) {
                contentService.initializeContent(context, jsonResponse, name, email, facebookId, new ABASimpleCallback() {
                    @Override
                    public void onSuccess() {
                        Boolean isNewUser = false;
                        try {
                            JSONObject jsonObject = new JSONObject(jsonResponse);
                            if (jsonObject.has("isnewuser")) {
                                if (jsonObject.getInt("isnewuser") == 0) {
                                    isNewUser = false;
                                } else {
                                    isNewUser = true;
                                }
                            }

                            callback.onSuccess(isNewUser);
                        } catch (Exception e) {
                            e.printStackTrace();
                            callback.onError(new ABAAPIError(context.getString(R.string.errorRegister)));
                        }
                    }

                    @Override
                    public void onError(ABAAPIError error) {
                        callback.onError(error);
                    }
                }, tracker);
            }

            @Override
            public void onError(Exception e) {
                callback.onError(new ABAAPIError(context.getString(R.string.errorRegister)));
            }
        });
    }

    // ================================================================================
    // Private methods - Tracking
    // ================================================================================

    private void trackLogin(Context context) {
        Realm realm = Realm.getInstance(ABAApplication.get().getRealmConfiguration());
        ABAUser currentUser = UserController.getInstance().getCurrentUser(realm);

        // Other events tracking
        MixpanelTrackingManager.getSharedInstance(context).trackLoginSuperproperties(context, currentUser);
        MixpanelTrackingManager.getSharedInstance(context).trackLoginEvent(kRecordTypeMail);
        CooladataNavigationTrackingManager.trackLoginEventWithOfflineFlag(false, currentUser.getUserId());

        FabricTrackingManager.trackLoginEvent(currentUser);
        LegacyTrackingManager.getInstance().trackLogin(currentUser);


        realm.close();
    }

    private void trackRegister(Context context) {
        Realm realm = Realm.getInstance(ABAApplication.get().getRealmConfiguration());
        ABAUser currentUser = UserController.getInstance().getCurrentUser(realm);

        // Other events tracking
        MixpanelTrackingManager.getSharedInstance(context).trackRegister(context, currentUser, kRecordTypeMail);
        FabricTrackingManager.trackRegister(context, currentUser, kRecordTypeMail);
        LegacyTrackingManager.getInstance().trackRegister(currentUser);
        CooladataProfileTrackingManager.trackUserRegistered(currentUser.getUserId());

        realm.close();
    }

    private void trackFacebookAccess(Context context, boolean isNewUser) {
        Realm realm = Realm.getInstance(ABAApplication.get().getRealmConfiguration());
        ABAUser currentUser = UserController.getInstance().getCurrentUser(realm);

        if (isNewUser) {
            // Other events tracking
            MixpanelTrackingManager.getSharedInstance(context).trackRegister(context, currentUser, GenericTrackingInterface.RecordType.kRecordTypeFacebook);
            FabricTrackingManager.trackRegister(context, currentUser, GenericTrackingInterface.RecordType.kRecordTypeFacebook);
            LegacyTrackingManager.getInstance().trackRegister(currentUser);
            CooladataProfileTrackingManager.trackUserRegistered(currentUser.getUserId());
            //TODO Kill the Singleton
            FirebaseTracker.getInstance().trackUserRegistered(currentUser.getUserId(), FACEBOOK);

        } else {
            // Other events tracking
            MixpanelTrackingManager.getSharedInstance(context).trackLoginSuperproperties(context, currentUser);
            MixpanelTrackingManager.getSharedInstance(context).trackLoginEvent(GenericTrackingInterface.RecordType.kRecordTypeFacebook);
            FabricTrackingManager.trackFacebookLoginEvent(currentUser);
            LegacyTrackingManager.getInstance().trackLogin(currentUser);
            CooladataNavigationTrackingManager.trackLoginEventWithOfflineFlag(false, currentUser.getUserId());
            //TODO Kill the Singleton
            FirebaseTracker.getInstance().trackUserLogin(currentUser.getUserId(), FACEBOOK);

        }
        realm.close();
    }
}
