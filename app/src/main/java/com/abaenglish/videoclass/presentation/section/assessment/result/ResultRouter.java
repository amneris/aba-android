package com.abaenglish.videoclass.presentation.section.assessment.result;

import android.app.Activity;
import android.content.Intent;
import android.text.TextUtils;

import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.clean.Router;
import com.abaenglish.videoclass.domain.abTesting.UnitDetailABTestingConfigurator;
import com.abaenglish.videoclass.presentation.section.assessment.CourseFinishedActivity;
import com.abaenglish.videoclass.presentation.section.assessment.EvaluationExerciseActivity;
import com.abaenglish.videoclass.social.FacebookPublication;
import com.abaenglish.videoclass.social.LinkedinPublication;
import com.abaenglish.videoclass.social.TwitterPublication;

import static com.abaenglish.videoclass.presentation.section.SectionActivityInterface.EXTRA_UNIT_ID;

/**
 * Created by xabierlosada on 27/07/16.
 */

public class ResultRouter implements Router {

    private final Activity activity;
    private FacebookPublication facebookPublication;
    private TwitterPublication twitterPublication;
    private LinkedinPublication linkedinPublication;
    private UnitDetailABTestingConfigurator unitDetailABTestingConfigurator;

    ResultRouter(Activity activity,
                 FacebookPublication facebookPublication,
                 TwitterPublication twitterPublication,
                 LinkedinPublication linkedinPublication,
                 UnitDetailABTestingConfigurator unitDetailABTestingConfigurator) {
        this.activity = activity;
        this.facebookPublication = facebookPublication;
        this.twitterPublication = twitterPublication;
        this.linkedinPublication = linkedinPublication;
        this.unitDetailABTestingConfigurator = unitDetailABTestingConfigurator;
    }

    void repeatEvaluation(String currentUnit, EvaluationExerciseActivity.UserStatusType currentStatus) {
        Intent intent = new Intent(activity, EvaluationExerciseActivity.class);
        intent.putExtra(EXTRA_UNIT_ID, currentUnit);
        intent.putExtra(EvaluationExerciseActivity.CURRENT_STATUS, currentStatus.getValue());
        activity.startActivity(intent);
        activity.finish();
        activity.overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_exit_unitanimation2);
    }

    void continueWithUnit(String unitId) {
        if (!TextUtils.isEmpty(unitId)) {
            activity.finish();
        }

        unitDetailABTestingConfigurator.startUnitDetailIntent(activity, unitId);
        activity.finish();
        activity.overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_exit_unitanimation2);
    }

    void goToCourseFinished(String userName, String teacherImageUrl) {
        Intent intent = new Intent(activity, CourseFinishedActivity.class);
        intent.putExtra(CourseFinishedActivity.EXTRA_USERNAME, userName);
        intent.putExtra(CourseFinishedActivity.EXTRA_TEACHER_IMAGE, teacherImageUrl);

        activity.startActivity(intent);
        activity.finish();
    }

    void goToLinkedin(int levelId) {
        linkedinPublication.setLevelId(levelId);
        linkedinPublication.share();
    }

    void shareWithFacebook() {
        facebookPublication.share();
    }

    void shareWithTwitter(String unitName, String unitId) {
        twitterPublication.configure(unitName, unitId);
        twitterPublication.share();
    }
}
