package com.abaenglish.videoclass.presentation.base.custom;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.text.Html;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.abaenglish.videoclass.R;
import com.bzutils.images.RoundedImageView;
import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created by iaguila on 29/4/15.
 */
public class TeacherBannerView extends LinearLayout {

    private boolean isTeacherDismissed = false;
    private RoundedImageView img;
    private ABATextView textView;

    public TeacherBannerView(Context context) {
        super(context);
        init();
    }

    public TeacherBannerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TeacherBannerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public TeacherBannerView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        inflate(getContext(), R.layout.view_teacher_banner, this);
        img = (RoundedImageView) findViewById(R.id.teacherImg);
        textView = (ABATextView) findViewById(R.id.teacherText);
    }

    public void setImageUrl(String url) {
        ImageLoader.getInstance().displayImage(url, img);
    }

    public void setImageBitmap(Bitmap bitmap){
        img.setImageBitmap(bitmap);
    }

    public ImageView getImageView(){
        return img;
    }


    public void setText(String text) {
        textView.setText(Html.fromHtml(text));
    }

    public void dismiss(Context aContext) {
        if (!isTeacherDismissed) {

            isTeacherDismissed = true;

            Animation fadeOut = AnimationUtils.loadAnimation(aContext, R.anim.fade_out_evaluation);
            fadeOut.setAnimationListener(animationListener);
            this.startAnimation(fadeOut);
        }
    }

    // ================================================================================
    // Unnamed classes
    // ================================================================================

    private Animation.AnimationListener animationListener = new Animation.AnimationListener() {
        @Override
        public void onAnimationStart(Animation animation) {

        }

        @Override
        public void onAnimationEnd(Animation animation) {
            isTeacherDismissed = true;
            TeacherBannerView.this.setVisibility(View.GONE);
        }

        @Override
        public void onAnimationRepeat(Animation animation) {

        }
    };
}
