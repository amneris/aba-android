package com.abaenglish.videoclass.presentation.unit.variation;

import android.app.ActivityOptions;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.widget.ListViewCompat;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.abaenglish.videoclass.ABAApplication;
import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.analytics.legacy.LegacyTrackingManager;
import com.abaenglish.videoclass.data.network.APIManager;
import com.abaenglish.videoclass.data.persistence.ABAAPIError;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.data.persistence.ABAUser;
import com.abaenglish.videoclass.domain.LanguageController;
import com.abaenglish.videoclass.domain.ProgressService;
import com.abaenglish.videoclass.domain.content.LevelUnitController;
import com.abaenglish.videoclass.domain.content.UserController;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.abaenglish.videoclass.presentation.base.custom.ABAProgressDialog;
import com.abaenglish.videoclass.presentation.base.custom.TeacherBannerView;
import com.abaenglish.videoclass.presentation.plan.InitialPlansActivity;
import com.abaenglish.videoclass.presentation.section.SectionActivityInterface;
import com.abaenglish.videoclass.presentation.section.SectionsService;
import com.abaenglish.videoclass.presentation.section.assessment.ABAEvaluationActivity;
import com.abaenglish.videoclass.presentation.section.exercise.ABAExercisesActivity;
import com.abaenglish.videoclass.presentation.section.interpret.ABAInterpretationActivity;
import com.abaenglish.videoclass.presentation.section.speak.ABASpeakActivity;
import com.abaenglish.videoclass.presentation.section.videoclass.ABAFilmActivity;
import com.abaenglish.videoclass.presentation.section.vocabulary.ABAVocabularyActivity;
import com.abaenglish.videoclass.presentation.section.write.ABAWriteActivity;
import com.abaenglish.videoclass.presentation.unit.DetailUnitBase;
import com.abaenglish.videoclass.presentation.unit.variation.Section.SectionType;
import com.bzutils.LogBZ;
import com.crashlytics.android.Crashlytics;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.zendesk.sdk.rating.ui.RateMyAppDialog;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.realm.Realm;

import static android.support.v7.widget.LinearLayoutCompat.LayoutParams;
import static com.abaenglish.videoclass.domain.content.DataController.ABASimpleCallback;
import static com.abaenglish.videoclass.presentation.section.SectionActivityInterface.EXTRA_UNIT_ID;
import static com.abaenglish.videoclass.presentation.section.SectionType.kABAFilm;
import static com.abaenglish.videoclass.presentation.section.SectionType.kInterpreta;


/**
 * Created by Xilosada on September 16.
 */
public class DetailUnitActivityVariation extends DetailUnitBase {

    public static final String EXTRA_COMPLETED_SECTION = "EXTRA_COMPLETED_SECTION";
    public static String OPEN_SECTION = "OPEN_SECTION";
    public static String EXTRA_CTA_TEXT = "EXTRA_CTA_TEXT";

    // Domain
    private boolean unitRefreshed; // It will be true as soon as all the needed data to draw the rosco is properly set

    // UI
    private ArrayList<String> totalElementsForDownload;

    @BindView(R.id.finishSectionNotification)
    LinearLayout finishSectionNotificationLayout;
    @BindView(R.id.notification_message)
    TextView notificationMessage;
    @BindView(R.id.sections_list)
    ListViewCompat listView;
    @BindView(R.id.unlock_layout)
    LinearLayout unlockLayout;
    @BindView(R.id.button_unlock)
    Button unlockButton;

    protected Animation slideInDownAnimation;
    protected Animation slideOutUp;

    private TeacherBannerView teacherBannerView;
    private SectionsAdapter adapter;
    private String ctaText;

    // ================================================================================
    // Lifecycle
    // ================================================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_unit_variation);
        unitId = getIntent().getExtras().getString(EXTRA_UNIT_ID);
        ctaText = getIntent().getExtras().getString(EXTRA_CTA_TEXT);

        ButterKnife.bind(this);
        loadAnimations();
        initUnitDetail();
    }

    // ================================================================================
    // Lifecycle
    // ================================================================================

    @Override
    protected void onResume() {
        super.onResume();
        Crashlytics.log(Log.INFO, "Unit", "User opens Unit number " + unitId);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        LanguageController.currentLanguageOverride();

        // If the unit was already refreshed then refreshing rosco.
        if (unitRefreshed) {
            showSections();
        }
    }

    @OnClick(R.id.button_unlock)
    public void unlock() {
        startInitialPlansActivity();
    }

    @Override
    public void onDestroy() {
        DataStore.getInstance().getDownloadController().getDownloadThread().stopDownloadProcess(this);
        DataStore.getInstance().getDownloadController().getDownloadThread().hideDownloadDialog();
        adapter = null;
        super.onDestroy();
    }

    public void onBackPressed() {
        //super.onBackPressed();
        finishUnit();
    }

    // ================================================================================
    // Overriding methods from DetailUnitBase
    // ================================================================================

    protected void refreshScreen() {
        showSections();
    }

    // ================================================================================
    // Overriding methods from ABAMasterActivity
    // ================================================================================

    @Override
    protected void updateAfterLogin() {
        super.updateAfterLogin();
        showSections();
    }

    // ================================================================================
    // Private methods
    // ================================================================================
    private void loadAnimations() {
        slideInDownAnimation = AnimationUtils.loadAnimation(this, R.anim.fade_in);
        slideOutUp = AnimationUtils.loadAnimation(this, R.anim.fade_out);
    }

    private void initUnitDetail() {
        if (!APIManager.isTablet(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        LanguageController.currentLanguageOverride();

        unitId = getIntent().getExtras().getString(EXTRA_UNIT_ID);
        initApp();
        configActionBar();
        configBackground();
        configListHeader();
        configListFooter(unitId);
    }

    private void configListFooter(String unitId) {
        unlockButton.setText(ctaText);
        if (!DataStore.getInstance().getUserController().isUserPremium() && !LevelUnitController.isFreeUnit(unitId)) {
            int sizeInDp = 60;
            float scale = getResources().getDisplayMetrics().density;
            int dpAsPixels = (int) (sizeInDp*scale + 0.5f);
            unlockLayout.setVisibility(View.VISIBLE);
            listView.setPadding( 0, 0, 0, dpAsPixels);
        }
    }

    private void checkForCompletedSection() {
        if (getIntent().getExtras().containsKey(EXTRA_COMPLETED_SECTION)) {
            final com.abaenglish.videoclass.presentation.section.SectionType finishedSection = com.abaenglish.videoclass.presentation.section.SectionType.fromInt(getIntent().getExtras().getInt(EXTRA_COMPLETED_SECTION));
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showFinishedSection(finishedSection);
                }
            });
        }
    }

    protected void showFinishedSection(final com.abaenglish.videoclass.presentation.section.SectionType sectionType) {
        notificationMessage.setText(getString(R.string.youHaveFinishSectionKey) + " " + sectionType.getSectionName(this));
        finishSectionNotificationLayout.setVisibility(View.VISIBLE);
        finishSectionNotificationLayout.startAnimation(slideInDownAnimation);


        Handler handlerNotification = new android.os.Handler();
        handlerNotification.postDelayed(new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dismissFinishedSection();
                    }
                });

                if (sectionType.equals(kInterpreta)) {
                    try {
                        RateMyAppDialog mRateMyAppDialog = new RateMyAppDialog.Builder(DetailUnitActivityVariation.this)
                                .withAndroidStoreRatingButton()
                                .withDontRemindMeAgainButton()
                                .build();
                        mRateMyAppDialog.showAlways(DetailUnitActivityVariation.this);
                    } catch (RuntimeException e) {
                        Crashlytics.logException(e);
                        LogBZ.printStackTrace(e);
                    }
                }
            }
        }, 3000);
    }

    protected void dismissFinishedSection() {
        finishSectionNotificationLayout.startAnimation(slideOutUp);
        finishSectionNotificationLayout.setVisibility(View.GONE);
    }

    private void getCompletedActions(final SectionViewModel sectionToLoad) {
        ABAUser currentUser = DataStore.getInstance().getUserController().getCurrentUser(getRealm());
        ABAUnit currentUnit = DataStore.getInstance().getLevelUnitController().getUnitWithId(getRealm(), unitId);
        ProgressService.getInstance().updateCompletedActions(currentUser, currentUnit, new ABASimpleCallback() {
                    @Override
                    public void onSuccess() {
                        dismissProgressDialog();
                        unitRefreshed = true;
                        showSections();

                        checkForCompletedSection();
                        if (sectionToLoad != null) {
                            checkIfSectionIsAccesibleAndStartIntent(sectionToLoad);
                        }
                    }

                    @Override
                    public void onError(ABAAPIError error) {
                        dismissProgressDialog();
                        showABAErrorNotification(error.getError());
                    }
                }
        );
    }

    private void showSections() {
        adapter = new SectionsAdapter(this, new ArrayList<>(new SectionRepositoryImpl().getSections(unitId)), new SectionsAdapter.SectionListener() {
            @Override
            public void onSectionClick(SectionViewModel section) {
                if (!section.isUnlocked()) {
                    showLockMessage(section);
                } else {
                    launchSection(section.getType());
                }
            }
        });

        // Assign adapter to ListView
        listView.setAdapter(adapter);
    }

    private void launchSection(SectionType sectionType) {
        Intent intent;
        switch (sectionType) {
            case FILM:
                intent = new Intent(this, ABAFilmActivity.class);
                intent.putExtra(ABAFilmActivity.IS_DOWNLOADED, isUnitDownloaded);
                intent.putExtra(ABAFilmActivity.SECTION_ID, ABAFilmActivity.FILM_SECTION);
                LegacyTrackingManager.getInstance().trackEvent(getRealm(), "unit_s1", "course", "click en abafilm");
                break;
            case SPEAK:
                intent = new Intent(this, ABASpeakActivity.class);
                LegacyTrackingManager.getInstance().trackEvent(getRealm(), "unit_s2", "course", "click en habla");
                break;
            case WRITE:
                intent = new Intent(this, ABAWriteActivity.class);
                LegacyTrackingManager.getInstance().trackEvent(getRealm(), "unit_s3", "course", "click en escribe");
                break;
            case INTERPRET:
                intent = new Intent(this, ABAInterpretationActivity.class);
                LegacyTrackingManager.getInstance().trackEvent(getRealm(), "unit_s4", "course", "click en interpreta");
                break;
            case VIDEOCLASS:
                intent = new Intent(this, ABAFilmActivity.class);
                intent.putExtra(ABAFilmActivity.IS_DOWNLOADED, isUnitDownloaded);
                intent.putExtra(ABAFilmActivity.SECTION_ID, ABAFilmActivity.VIDEOCLASS_SECTION);
                LegacyTrackingManager.getInstance().trackEvent(getRealm(), "unit_s5", "course", "click en videoclase");
                break;
            case EXERCISE:
                intent = new Intent(this, ABAExercisesActivity.class);
                LegacyTrackingManager.getInstance().trackEvent(getRealm(), "unit_s6", "course", "click en ejercicios");
                break;
            case VOCABULARY:
                intent = new Intent(this, ABAVocabularyActivity.class);
                LegacyTrackingManager.getInstance().trackEvent(getRealm(), "unit_s7", "course", "click en vocabulario");
                break;
            case ASSESSMENT:
                intent = new Intent(this, ABAEvaluationActivity.class);
                LegacyTrackingManager.getInstance().trackEvent(getRealm(), "unit_s8", "course", "click en evaluación");
                break;
            default:
                throw new UnsupportedOperationException();
        }
        intent.putExtra(SectionActivityInterface.EXTRA_UNIT_ID, unitId);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.slide_enter_unitanimation1, R.anim.slide_enter_unitanimation2).toBundle();
            startActivity(intent, bndlanimation);
        } else {
            startActivity(intent);
        }
        finish();
    }

    private void configBackground() {
        ImageView background = (ImageView) findViewById(R.id.detailUnitBackground);
        ABAUnit currentUnit = DataStore.getInstance().getLevelUnitController().getUnitWithId(getRealm(), unitId);
        if (DataStore.getInstance().getLevelUnitController().checkIfFileExist(currentUnit, currentUnit.getFilmImageInactiveUrl())) {
            DataStore.getInstance().getLevelUnitController().displayImage(currentUnit, currentUnit.getFilmImageInactiveUrl(), background);
        } else {
            ImageLoader.getInstance().displayImage(currentUnit.getFilmImageInactiveUrl(), background);
        }
    }

    private void configListHeader() {
        teacherBannerView = new TeacherBannerView(this);
        ABAUnit currentUnit = DataStore.getInstance().getLevelUnitController().getUnitWithId(getRealm(), unitId);
        if (DataStore.getInstance().getLevelUnitController().checkIfFileExist(currentUnit, DataStore.getInstance().getUserController().getCurrentUser(getRealm()).getTeacherImage())) {
            DataStore.getInstance().getLevelUnitController().displayImage(null, DataStore.getInstance().getUserController().getCurrentUser(getRealm()).getTeacherImage(), teacherBannerView.getImageView());
        } else {
            teacherBannerView.setImageUrl(DataStore.getInstance().getUserController().getCurrentUser(getRealm()).getTeacherImage());
        }
        teacherBannerView.setText(getTeacherTip());


        LinearLayout header = new LinearLayout(this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        lp.setMargins(0, 0, 0, 30);
        header.addView(teacherBannerView, lp);
        listView.addHeaderView(header);
    }

    private String getTeacherTip() {
        ABAUnit currentUnit = DataStore.getInstance().getLevelUnitController().getUnitWithId(getRealm(), unitId);
        double unitProgress = DataStore.getInstance().getLevelUnitController().getGlobalProgressForUnit(currentUnit);
        com.abaenglish.videoclass.presentation.section.SectionType sectiontoLoad = DataStore.getInstance().getRoscoController().getCurrentSectionForUnit(currentUnit);

        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = settings.edit();
        boolean isNewUser = settings.getBoolean("newUser", false);
        if (isNewUser) {
            String teacherNewRegister = getResources().getString(R.string.unitTeacherNewRegister);
            teacherNewRegister = teacherNewRegister.replace("%@", DataStore.getInstance().getUserController().getCurrentUser(getRealm()).getTeacherName());
            editor.putBoolean("newUser", false);
            editor.apply();
            return teacherNewRegister;
        } else if (unitProgress > 0 && !currentUnit.isCompleted()) {
            if (sectiontoLoad.equals(kABAFilm)) {
                if (currentUnit.getSectionFilm().isCompleted()) {
                    return getString(R.string.unitTeacherFilmContinueKey);
                }
            } else {
                return getString(R.string.unitContinueSectionTeacherText) + " " + sectiontoLoad.getSectionName(this);
            }
        } else if (currentUnit.isCompleted()) {
            return getString(getResources().getIdentifier("unitCompletedTeacherText" + currentUnit.getIdUnit(), "string",
                    getPackageName()));
        }

        //TODO new registers

        return getString(getResources().getIdentifier("unitTeacherText" + currentUnit.getIdUnit(), "string",
                getPackageName()));
    }

    private void initApp() {

        int section = (int) getIntent().getExtras().getLong(OPEN_SECTION, -1);
        if (section != -1) {
            launchSection(new SectionViewModel(0, 0, 0, true, SectionType.values()[section]).getType());
        }

        if (getIntent().getExtras().get(OPEN_SECTION) != null) {
            getIntent().getExtras().putString(OPEN_SECTION, null);
        } else {
            getSectionsContentForCurrentUnit(null);
        }

        ABAUnit currentUnit = DataStore.getInstance().getLevelUnitController().getUnitWithId(getRealm(), unitId);
        if (currentUnit.getIdUnit() != null) {
            Crashlytics.setString("Selected Unit", currentUnit.getIdUnit());
        }
        if (currentUnit.getLastChanged() != null) {
            Crashlytics.setString("Selected Unit update date", currentUnit.getLastChanged().toString());
        }
    }

    private void checkIfSectionIsAccesibleAndStartIntent(SectionViewModel section) {
        if (section.isUnlocked()) {
            launchSection(section.getType());
        } else {
            showLockMessage(section);
        }
    }

    private void startInitialPlansActivity() {
        overridePendingTransition(R.anim.slide_enter_unitanimation1, R.anim.slide_enter_unitanimation2);
        startActivity(InitialPlansActivity.getCallingIntent(this));
    }

    private void showLockMessage(SectionViewModel section) {
        if (UserController.getInstance().isUserPremium() || LevelUnitController.isFreeUnit(unitId)) {
            if (section.getType().equals(Section.SectionType.ASSESSMENT)) {
                showAssessmentLockMessage();
            }
        } else {
            startInitialPlansActivity();
        }
    }

    private void showAssessmentLockMessage() {
        showABAErrorNotification(getString(R.string.cannotEnterAssesmentKey));
    }

    // ================================================================================

    public void getSectionsContentForCurrentUnit(final SectionViewModel sectionType) {
        showProgressDialog(ABAProgressDialog.ComplexDialog);
        ABAUnit currentUnit = DataStore.getInstance().getLevelUnitController().getUnitWithId(getRealm(), unitId);
        SectionsService.getInstance().fetchSections(DetailUnitActivityVariation.this, currentUnit, new ABASimpleCallback() {
            @Override
            public void onSuccess() {
                getCompletedActions(sectionType);

                Realm instance = Realm.getInstance(ABAApplication.get().getRealmConfiguration());
                ABAUnit callbackUnit = DataStore.getInstance().getLevelUnitController().getUnitWithId(instance, unitId);

                totalElementsForDownload = DataStore.getInstance().getDownloadController().getTotalElementsForDownloadAtUnit(getRealm(), callbackUnit);
                isUnitDownloaded = DataStore.getInstance().getDownloadController().isAllFileDownloaded(callbackUnit, totalElementsForDownload);

                instance.close();
            }

            @Override
            public void onError(ABAAPIError error) {
                dismissProgressDialog();
                if (error != null) {
                    error.showABANotificationError(DetailUnitActivityVariation.this);
                    LogBZ.e("Unit Sections API error: " + error.getError());
                }
            }
        });
    }
}