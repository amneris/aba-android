package com.abaenglish.videoclass.presentation.section.interpret;

import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.abaenglish.videoclass.ABAApplication;
import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.analytics.cooladata.study.CooladataStudyTrackingManager;
import com.abaenglish.videoclass.data.persistence.ABAInterpret;
import com.abaenglish.videoclass.data.persistence.ABAInterpretRole;
import com.abaenglish.videoclass.data.persistence.ABAPhrase;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.data.persistence.ABAUser;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.abaenglish.videoclass.presentation.base.ABAMasterSectionActivity;
import com.abaenglish.videoclass.presentation.base.custom.ABAButton;
import com.abaenglish.videoclass.presentation.base.custom.ABATextView;
import com.abaenglish.videoclass.presentation.base.custom.FontCache;
import com.abaenglish.videoclass.presentation.base.custom.ListenAndRecordControllerView;
import com.abaenglish.videoclass.presentation.section.SectionType;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

import static com.abaenglish.videoclass.analytics.cooladata.CooladataTrackingDefinitions.CooladataSectionType.ABAInterpret;
import static com.abaenglish.videoclass.presentation.section.SectionActivityInterface.EXTRA_UNIT_ID;

/**
 * Created by madalin on 27/05/15.
 * Edited by Jesus with regard of Cooladata on 25/04/16
 */
public class InterpretaDialogActivity extends ABAMasterSectionActivity implements ListenAndRecordControllerView.PlayerControlsListener, View.OnClickListener, ListenAndRecordControllerView.SectionControlsListener {

    public static final String ROLE_ID = "ROLE_ID";
    public static final String LISTEN_MODE = "LISTEN_MODE";

    private ABAUnit currentUnit;
    private ABAUser currentUser;
    private ABAInterpret interpretSection;
    private ABAInterpretRole currentRole;
    private ABAPhrase currentPhrase;
    private ArrayList<ABAPhrase> items;

    private ListView interpretDialogListView;
    private InterpretDialogAdapter dialogAdapter;

    private LinearLayout pauseView;
    private ABAButton continueInterpretation;
    private ABAButton restartInterpretation;

    private int totalCompletedRoles;
    private boolean listenMode;
    private String unitId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inter_dialog);

        unitId = getIntent().getExtras().getString(EXTRA_UNIT_ID);
        currentUnit = DataStore.getInstance().getLevelUnitController().getUnitWithId(getRealm(), unitId);
        currentUser = DataStore.getInstance().getUserController().getCurrentUser(getRealm());
        interpretSection = DataStore.getInstance().getInterpretController().getSectionForUnit(currentUnit);
        currentRole = interpretSection.getRoles().get(getIntent().getExtras().getInt(ROLE_ID));
        listenMode = getIntent().getExtras().getBoolean(LISTEN_MODE);

        if (!listenMode) {
            currentPhrase = DataStore.getInstance().getInterpretController().getCurrentPhraseForRole(currentRole, interpretSection);
        } else {
            currentPhrase = interpretSection.getDialog().get(0);
        }

        totalCompletedRoles = DataStore.getInstance().getInterpretController().getNumberOfRolesCompleted(interpretSection);

        configActionBar();
        configBottomController();
        configBackground();
        config();
//        checkPermissionMicrophone();

        setupControls(false);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.continueInterpretation: {
                //continue Interpretation
                bottomControllerView.restoreListenersForCurrentSection();
                pauseView.setVisibility(View.GONE);
                setupControls(false);
                break;
            }
            case R.id.restartInterpretation: {

                DataStore.getInstance().getInterpretController().eraseProgressForRole(getRealm(), currentRole, interpretSection);

                currentPhrase = DataStore.getInstance().getInterpretController().getCurrentPhraseForRole(currentRole, interpretSection);
                updateItems();

                dialogAdapter = new InterpretDialogAdapter(this, interpretSection.getUnit(), currentRole, items, fontCache);
                interpretDialogListView.setAdapter(dialogAdapter);
                updateProgressPercentage();

                dialogAdapter.notifyDataSetChanged();

                //restart Interpretation
                pauseView.setVisibility(View.GONE);
                bottomControllerView.restoreListenersForCurrentSection();
                setupControls(false);

                break;
            }


        }
    }

    private void pauseClicked() {
        pauseView.setVisibility(View.VISIBLE);
        bottomControllerView.unregisterListeners();
    }

    private void setupControls(boolean autostart) {

        if (bottomControllerView != null) {
            bottomControllerView.setCurrentPhrase(currentPhrase);

            if (listenMode) {
                bottomControllerView.disableCenterButtonClickListener();
                bottomControllerView.prepareForListen();

                if (currentPhrase.getInterpretRole().equals(currentRole)) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (bottomControllerView != null)
                                bottomControllerView.startListenRecordedPhrase();
                        }
                    }, 500);
                } else {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if (bottomControllerView != null)
                                bottomControllerView.startListenCurrentPhrase();
                        }
                    }, 500);
                }
            } else {
                if (currentPhrase.getInterpretRole().equals(currentRole)) {
                    bottomControllerView.prepareForRecord();

                    if (autostart) {
                        bottomControllerView.disableCenterButtonClickListener();

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (bottomControllerView != null)
                                    bottomControllerView.startRecording();
                            }
                        }, 500);
                    } else {
                        bottomControllerView.enableCenterButtonClickListener();
                    }
                } else {
                    bottomControllerView.prepareForListen();

                    if (autostart) {
                        bottomControllerView.disableCenterButtonClickListener();

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                if (bottomControllerView != null)
                                    bottomControllerView.startListenCurrentPhrase();
                            }
                        }, 500);
                    } else {
                        bottomControllerView.enableCenterButtonClickListener();
                    }
                }
            }
        }
    }

    private void next() {
        int nextIndex = interpretSection.getDialog().indexOf(currentPhrase) + 1;
        currentPhrase = interpretSection.getDialog().get(nextIndex);

        items.add(currentPhrase);
        dialogAdapter.notifyDataSetChanged();
        interpretDialogListView.setSelection(dialogAdapter.getCount() - 1);
    }

    // ================================================================================
    // PlayerControlsListener
    // ================================================================================

    @Override
    public void onListened() {
        // Tracking
        CooladataStudyTrackingManager.trackListenedToAudio(currentUser.getUserId(), currentUnit.getLevel().getIdLevel(), currentUnit.getIdUnit(), ABAInterpret, currentPhrase.getIdPhrase());
        tracker.trackListenedAudio(currentUnit.getLevel().getIdLevel(), currentUnit.getIdUnit(), ABAInterpret.toString(), currentPhrase.getIdPhrase());

        // Action
        if (interpretSection.getDialog().indexOf(currentPhrase) == interpretSection.getDialog().size() - 1) {
            if (DataStore.getInstance().getInterpretController().allRolesAreCompleted(interpretSection) &&
                    totalCompletedRoles < DataStore.getInstance().getInterpretController().getNumberOfRolesCompleted(interpretSection)) {
                finishActivity(true, bottomControllerView);
            } else {
                finishActivity(false, bottomControllerView);
            }
        } else {
            next();
            setupControls(true);
        }
    }

    @Override
    public void onRecorded() {

        // Tracking
        CooladataStudyTrackingManager.trackRecordedAudio(currentUser.getUserId(), currentUnit.getLevel().getIdLevel(), currentUnit.getIdUnit(), ABAInterpret, currentPhrase.getIdPhrase());
        tracker.trackRecordedAudio(currentUnit.getLevel().getIdLevel(), currentUnit.getIdUnit(), ABAInterpret.toString(), currentPhrase.getIdPhrase());

        // Action
        DataStore.getInstance().getInterpretController().setPhraseDoneForRole(getRealm(), currentPhrase, currentRole, true);
        currentRole = interpretSection.getRoles().get(getIntent().getExtras().getInt(ROLE_ID));
        if (currentRole.isCompleted() && interpretSection.getDialog().indexOf(currentPhrase) == interpretSection.getDialog().size() - 1) {
            if (DataStore.getInstance().getInterpretController().allRolesAreCompleted(interpretSection) &&
                    totalCompletedRoles < DataStore.getInstance().getInterpretController().getNumberOfRolesCompleted(interpretSection)) {
                finishActivity(true, bottomControllerView);
            } else {
                finishActivity(false, bottomControllerView);
            }
        } else {
            next();
            setupControls(true);
            updateProgressPercentage();
        }
    }

    @Override
    public void onCompared() {
        // do nothing
    }

    @Override
    public void finishCompared() {

    }

    @Override
    public void onRetryPhrase() {

    }

    private void updateProgressPercentage() {
        ABATextView unitSectionProgress = (ABATextView) findViewById(R.id.toolbarSubTitle);
        unitSectionProgress.setText(DataStore.getInstance().getInterpretController().getPercentageForSection(interpretSection));
    }

    protected void finishActivity(boolean completed, ListenAndRecordControllerView bottomControllerView) {

        bottomControllerView.unregisterListeners();
        bottomControllerView = null;

        if (completed) {
            setResult(RESULT_OK);
        }
        finish();
        overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_exit_unitanimation2);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishActivity(false, bottomControllerView);
    }

    // ================================================================================
    // ABAMasterSectionActivity
    // ================================================================================

    protected Object getSection() {
        return interpretSection;
    }

    @Override
    public void OnPausedSection() {
        pauseClicked();
    }

    // ================================================================================
    // Private methods - Configuration
    // ================================================================================

    private void configBottomController() {
        bottomControllerView = (ListenAndRecordControllerView) findViewById(R.id.interpretListenController);
        bottomControllerView.unregisterListeners();
        bottomControllerView.restoreListenersForCurrentSection();
        bottomControllerView.setSectionType(SectionType.kInterpreta);
        bottomControllerView.setCurrentUnit(interpretSection.getUnit());
        bottomControllerView.setPlayerControlsListener(this);
        bottomControllerView.setSectionControlsListener(this);
        bottomControllerView.setBehaviour(ListenAndRecordControllerView.PlayerControllerBehaviour.kListenRecord);
    }

    private void config() {
        pauseView = (LinearLayout) findViewById(R.id.pauseView);
        interpretDialogListView = (ListView) findViewById(R.id.interpretDialogView);
        continueInterpretation = (ABAButton) findViewById(R.id.continueInterpretation);
        restartInterpretation = (ABAButton) findViewById(R.id.restartInterpretation);
        continueInterpretation.setTypeface(fontCache.getTypeface(FontCache.ABATypeface.sans500));
        restartInterpretation.setTypeface(fontCache.getTypeface(FontCache.ABATypeface.sans500));
        ((ABATextView) findViewById(R.id.pauseTitle)).setTypeface(fontCache.getTypeface(FontCache.ABATypeface.sans500));

        if (listenMode) {
            bottomControllerView.setVisibility(View.GONE);
        }

        interpretDialogListView.setDividerHeight(0);

        updateItems();
        dialogAdapter = new InterpretDialogAdapter(this, interpretSection.getUnit(), currentRole, items, fontCache);

        interpretDialogListView.setAdapter(dialogAdapter);

        if (!listenMode) {
            interpretDialogListView.setPadding(0, 0, 0, (int) getResources().getDimension(R.dimen.listenAndrecordControllerHeight));
            interpretDialogListView.setClipToPadding(false);
        }

        // Disable scrolling
        interpretDialogListView.setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                return (event.getAction() == MotionEvent.ACTION_MOVE);
            }
        });

        restartInterpretation.setOnClickListener(this);
        continueInterpretation.setOnClickListener(this);
    }

    private void updateItems() {
        items = new ArrayList<>();
        int indexCurrentPhrase = interpretSection.getDialog().indexOf(currentPhrase);

        for (int i = 0; i <= indexCurrentPhrase; i++) {
            items.add(interpretSection.getDialog().get(i));
        }
    }

    private void configActionBar() {
        if (!listenMode) {
            findViewById(R.id.toolbarRightButton).setVisibility(View.VISIBLE);
            findViewById(R.id.toolbarRightButton).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    pauseClicked();
                }

            });
        }

        findViewById(R.id.toolbarLeftButton).setOnTouchListener(((ABAApplication) getApplicationContext()).getImagePressedState(null));
        findViewById(R.id.toolbarLeftButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishActivity(false, bottomControllerView);
            }
        });

        ABATextView unitSectionTitle = (ABATextView) findViewById(R.id.toolbarTitle);
        ABATextView unitSectionProgress = (ABATextView) findViewById(R.id.toolbarSubTitle);
        unitSectionTitle.setText(R.string.unitMenuTitle4Key);
        unitSectionProgress.setText((int) interpretSection.getUnit().getSectionInterpret().getProgress() + "%");
    }

    private void configBackground() {
        ImageView background = (ImageView) findViewById(R.id.InterpretDialogBackground);
        if (DataStore.getInstance().getLevelUnitController().checkIfFileExist(interpretSection.getUnit(), interpretSection.getUnit().getFilmImageInactiveUrl())) {
            DataStore.getInstance().getLevelUnitController().displayImage(interpretSection.getUnit(), interpretSection.getUnit().getFilmImageInactiveUrl(), background);
        } else {
            ImageLoader.getInstance().displayImage(interpretSection.getUnit().getFilmImageInactiveUrl(), background);
        }
    }
}
