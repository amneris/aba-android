package com.abaenglish.videoclass.presentation.section.interpret;

import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.abaenglish.videoclass.ABAApplication;
import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.analytics.cooladata.navigation.CooladataNavigationTrackingManager;
import com.abaenglish.videoclass.data.persistence.ABAInterpret;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.data.persistence.ABAUser;
import com.abaenglish.videoclass.domain.content.UserController;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.abaenglish.videoclass.presentation.base.ABAMasterActivity;
import com.abaenglish.videoclass.presentation.base.custom.ABATextView;
import com.abaenglish.videoclass.presentation.base.custom.FontCache;
import com.abaenglish.videoclass.presentation.base.custom.TeacherBannerView;
import com.abaenglish.videoclass.presentation.section.NextSectionDialog_;
import com.abaenglish.videoclass.presentation.section.SectionHelper;
import com.abaenglish.videoclass.presentation.section.SectionType;
import com.crashlytics.android.Crashlytics;
import com.nostra13.universalimageloader.core.ImageLoader;

import static com.abaenglish.videoclass.analytics.cooladata.CooladataTrackingDefinitions.CooladataSectionType.ABAInterpret;
import static com.abaenglish.videoclass.presentation.section.SectionActivityInterface.EXTRA_UNIT_ID;

/**
 * Created by madalin on 7/05/15.
 */
public class ABAInterpretationActivity extends ABAMasterActivity implements NextSectionDialog_.ResultListener, CharacterAdapter.CharacterSelectionListener {

    private static final int REQUEST_CODE_PROGRESS = 52342;
    private ABAUnit currentUnit;
    private ABAInterpret interpretSection;

    private ListView characterListView;
    private CharacterAdapter characterAdapter;
    private String unitId;

    // ================================================================================
    // Lifecycle
    // ================================================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interpretation);
        unitId = getIntent().getExtras().getString(EXTRA_UNIT_ID);
        initApp();
        configActionBar();
        configBackground();
        configTeacher();
        config();

        trackSection();
    }

    @Override
    protected void onResume() {
        super.onResume();
        characterAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PROGRESS && resultCode == RESULT_OK) {
            if (currentUnit.getSectionInterpret().getProgress() == 100) {
                if (serverConfig.isImprovedLinearCourseActive() && !NextSectionDialog_.checkNextSectionIsDone(currentUnit.getIdUnit(), 4)) {
                    showNextSectionDialog();
                } else {
                    finishSection(true);
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishSection(false);
    }

    // ================================================================================
    // Private methods - Activity configuration
    // ================================================================================

    private void initApp() {
        currentUnit = DataStore.getInstance().getLevelUnitController().getUnitWithId(getRealm(), unitId);
        interpretSection = DataStore.getInstance().getInterpretController().getSectionForUnit(currentUnit);
    }

    private void configActionBar() {
        findViewById(R.id.toolbarLeftButton).setOnTouchListener(((ABAApplication) getApplicationContext()).getImagePressedState(null));
        findViewById(R.id.toolbarLeftButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishSection(false);
            }
        });

        updateProgressPercentage();
    }

    // ================================================================================
    // Private methods
    // ================================================================================

    private void configTeacher() {
        ABAUser currentUser = DataStore.getInstance().getUserController().getCurrentUser(getRealm());
        TeacherBannerView teacherBannerView = (TeacherBannerView) findViewById(R.id.detailUnitTeacherView);
        if (currentUnit.getSectionInterpret().getProgress() == 0) {

            if (DataStore.getInstance().getLevelUnitController().checkIfFileExist(currentUnit, currentUser.getTeacherImage())) {
                DataStore.getInstance().getLevelUnitController().displayImage(null, currentUser.getTeacherImage(), teacherBannerView.getImageView());
            } else {
                teacherBannerView.setImageUrl(currentUser.getTeacherImage());
            }

            teacherBannerView.setText(getString(R.string.interpretTipKey));
        } else {
            teacherBannerView.setVisibility(View.GONE);
        }
    }

    private void configBackground() {
        ImageView background = (ImageView) findViewById(R.id.sectionInterpretBackground);
        if (DataStore.getInstance().getLevelUnitController().checkIfFileExist(currentUnit, currentUnit.getFilmImageInactiveUrl())) {
            DataStore.getInstance().getLevelUnitController().displayImage(currentUnit, currentUnit.getFilmImageInactiveUrl(), background);
        } else {
            ImageLoader.getInstance().displayImage(currentUnit.getFilmImageInactiveUrl(), background);
        }
    }

    private void config() {
        characterListView = (ListView) findViewById(R.id.characterList);

        characterAdapter = new CharacterAdapter(this, this, currentUnit, interpretSection, fontCache);
        characterListView.setAdapter(characterAdapter);

        TextView choseCharacter = (TextView) findViewById(R.id.choseCharacter);
        choseCharacter.setTypeface(fontCache.getTypeface(FontCache.ABATypeface.rBold));
    }

    protected void finishSection(boolean completed) {
        if (completed) {
            unitDetailABTestingConfigurator.startUnitDetailIntent(this, currentUnit.getIdUnit(), SectionType.kInterpreta);
        } else {
            unitDetailABTestingConfigurator.startUnitDetailIntent(this, currentUnit.getIdUnit());
        }

        finish();
        overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_exit_unitanimation2);
    }


    private void updateProgressPercentage() {
        ABATextView unitSectionTitle = (ABATextView) findViewById(R.id.toolbarTitle);
        ABATextView unitSectionProgress = (ABATextView) findViewById(R.id.toolbarSubTitle);

        unitSectionTitle.setText(R.string.unitMenuTitle4Key);
        unitSectionProgress.setText(DataStore.getInstance().getInterpretController().getPercentageForSection(interpretSection));
    }

    private void showNextSectionDialog() {
        FragmentManager fragmentManager = getFragmentManager();
        NextSectionDialog_ ndf = NextSectionDialog_.newInstance(SectionHelper.SECTION_INTERPRET, currentUnit.getIdUnit());
        ndf.addListener(this);
        ndf.show(fragmentManager, "Write");
    }

    // ================================================================================
    // NextSectionDialog
    // ================================================================================

    @Override
    public void goToNext(boolean result) {
        if (result) {
            startActivity(SectionHelper.getNextSectionIntent(this, Integer.parseInt(currentUnit.getIdUnit()), SectionHelper.SECTION_INTERPRET));
            finish();
        } else {
            finishSection(true);
        }
    }

    // ================================================================================
    // ABAMasterSectionActivity
    // ================================================================================

    protected Object getSection() {
        return interpretSection;
    }

    // ================================================================================
    // Private methods - Tracking
    // ================================================================================

    private void trackSection() {
        ABAUser user = UserController.getInstance().getCurrentUser(getRealm());
        CooladataNavigationTrackingManager.trackEnteredSection(user.getUserId(), currentUnit.getLevel().getIdLevel(), currentUnit.getIdUnit(), ABAInterpret);
        Crashlytics.log(Log.INFO, "Section", "User opens Section Interpret");
        tracker.trackEnteredSection(currentUnit.getLevel().getIdLevel(), currentUnit.getIdUnit(), ABAInterpret.toString());
        tracker.trackStartedInterpretation(currentUnit.getLevel().getIdLevel(), currentUnit.getIdUnit(), ABAInterpret.toString());
    }

    @Override
    public void onOptionSelected(Intent intent) {
        overridePendingTransition(R.anim.slide_enter_unitanimation1, R.anim.slide_enter_unitanimation2);
        startActivityForResult(intent, REQUEST_CODE_PROGRESS);
    }
}
