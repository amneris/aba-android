package com.abaenglish.videoclass.presentation.abaMoment;

import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.abaenglish.videoclass.ABAApplication;
import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.data.persistence.ABAUser;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.CircleBitmapDisplayer;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import io.realm.Realm;


/**
 * Created by Julien on 07/03/2017.
 * Dialog to encourage the user to go to the lectures when a ABA Moment has been completed.
 */

@EFragment
public class GoToUnitDialog extends DialogFragment {

    public static final int delayTimeInMilliSeconds = 5000;

    @ViewById
    ImageView teacherImageView;
    @ViewById
    TextView finishedABAMomentTextView;
    @ViewById
    Button nextUnitButton;
    @ViewById
    ProgressBar nextUnitProgressBar;

    private Realm realm;
    private String unitId;
    private String username;
    private String teacherImage;

    private DisplayImageOptions displayImageOptions;
    private ImageLoader imageLoader;

    private GoToUnitDialog.ResultListener resultListener;
    private boolean mDismissed = false;

    @SuppressWarnings("WeakerAccess")
    protected interface ResultListener {
        void goToCurrentUnit(boolean result, String unitID, String name);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        imageLoader = ImageLoader.getInstance();
        displayImageOptions = new DisplayImageOptions.Builder().displayer(new CircleBitmapDisplayer()).cacheOnDisk(true).resetViewBeforeLoading(true).build();

        realm = Realm.getInstance(ABAApplication.get().getRealmConfiguration());
        ABAUser user = DataStore.getInstance().getUserController().getCurrentUser(realm);
        username = user.getName();
        teacherImage = user.getTeacherImage();

        //for the text of the button
        if (user.getCurrentLevel() != null) {
            unitId = DataStore.getInstance().getLevelUnitController().getCurrentUnitForLevel(user.getCurrentLevel()).getIdUnit();
        }

        //realm.close();
    }

    @AfterViews
    public void afterViews() {

        imageLoader.displayImage(teacherImage, teacherImageView, displayImageOptions);

        //#ABAMoment
        SpannableStringBuilder builder = new SpannableStringBuilder();
        StyleSpan boldSpan = new StyleSpan(Typeface.BOLD);

        builder.append(getResources().getString(R.string.goToUnitDialogFinish1));
        builder.append(" ");
        int start = builder.length();
        builder.append(getResources().getString(R.string.goToUnitDialogFinish2));
        builder.setSpan(boldSpan, start, builder.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        builder.append(getResources().getString(R.string.goToUnitDialogFinish3));

        finishedABAMomentTextView.setText(builder);

        nextUnitButton.setText(String.format("%s %s", getResources().getString(R.string.goToUnitDialogNext), unitId));

        startTimeout();
    }

    // ================================================================================
    // DialogFragment methods
    // ================================================================================

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        if (dialog.getWindow() != null) {
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.dialog_go_to_unit, container);
    }

    @Override
    public void onDestroyView() {
        realm.close();
        super.onDestroyView();
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        goToCurrentUnit(false);
        super.onCancel(dialog);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        mDismissed = true;
    }

    // ================================================================================
    // Public methods
    // ================================================================================

    public void addListener(GoToUnitDialog.ResultListener resultListener) {
        this.resultListener = resultListener;
    }

    @Click(R.id.quitButton)
    public void onQuitClick() {
        // dismiss if the dialog is not dismissed yet
        if (!mDismissed) {
            dismiss();
        }
    }

    @UiThread
    @Click(R.id.nextUnitButton)
    public void onNextUnitClick() {
        // going to the next unitId only if the dialog is not dismissed yet
        if (!mDismissed) {
            goToCurrentUnit(true);
            dismiss();
        }
    }

    @Background
    public void startTimeout() {

        startAnimationProgress();
        try {
            Thread.sleep(delayTimeInMilliSeconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (!mDismissed) {
            onNextUnitClick();
        }
    }

    @UiThread
    public void startAnimationProgress() {
        if (nextUnitProgressBar != null) {
            //animate the progress bar
            ObjectAnimator progressAnimator;
            progressAnimator = ObjectAnimator.ofInt(nextUnitProgressBar, "progress", nextUnitProgressBar.getProgress(), 100);
            progressAnimator.setDuration(delayTimeInMilliSeconds);
            progressAnimator.start();
        }
    }

    // ================================================================================
    // Private methods
    // ================================================================================

    private void goToCurrentUnit(boolean result) {
        if (resultListener != null) {
            resultListener.goToCurrentUnit(result, unitId, username);
        }
    }

}