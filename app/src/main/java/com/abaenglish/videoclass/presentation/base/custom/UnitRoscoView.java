package com.abaenglish.videoclass.presentation.base.custom;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.RectF;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.abaenglish.videoclass.ABAApplication;
import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.abaenglish.videoclass.presentation.base.ABAMasterActivity;
import com.abaenglish.videoclass.presentation.section.SectionType;
import com.bzutils.BZUtils;
import com.bzutils.images.RoundedImageView;

import java.util.List;

import io.realm.Realm;

import static com.abaenglish.videoclass.R.id.center;
import static com.abaenglish.videoclass.R.id.textView;

/**
 * Created by iaguila on 4/5/15.
 * Refactored by Jesus Espejo on 12/08/2016: Resolving crash.
 */
public class UnitRoscoView extends RelativeLayout {

    private static final int CENTRAL_ROSCO_ID = 1111;

    // Animation
    private static final int FADE_IN_DURATION = 200;
    private static final int FADE_IN_DELAY = 100;
    private int mainCircleRadius;
    private int sectionCircleRadius;
    private int sectionAreaWidth;

    // Rosco
    private ABATextView textViewTitleCenter;
    private ABATextView textViewSectionCenter;
    private SemiCircleProgressBarView progressView;
    private String currentUnitId;
    private String currentLevelId;

    // Rosco unitProgress
    private float unitProgress;
    private UnitRoscoHandler unitRoscoHandler;
    private UnitRoscoView unitRoscoView;

    // ================================================================================
    // Constructors
    // ================================================================================

    public UnitRoscoView(Context context, String unitId, String levelId) {
        super(context);
        initiateView();
        this.unitRoscoView = this;
        this.currentUnitId = unitId;
        this.currentLevelId = levelId;
    }

    // ================================================================================
    // Private methods
    // ================================================================================

    private void updateRosco(ABAUnit unit, SectionType sectionToLoad) {

        int unitIndexWithinLevel = DataStore.getInstance().getLevelUnitController().getUnitIndexWithinLevel(currentUnitId, currentLevelId);
        boolean isRoscoBlocked = !(DataStore.getInstance().getUserController().isUserPremium() || unitIndexWithinLevel == 1);

        // If user is premium or the unit is the first one within the lesson
        if (!isRoscoBlocked) {
            configTextAtRoscoCenter(unit, sectionToLoad);
        } else {
            // Blocked center
            textViewTitleCenter.setText(getContext().getString(R.string.unitMenuCenterNoPremKey).toUpperCase());
            textViewSectionCenter.setVisibility(GONE);

            if (unit.getSectionVideoClass().isCompleted()) {
                unitProgress = 0.125f;
            } else {
                unitProgress = 0;
            }
        }

        // Manually adjusting the title center in case the text is very large
        if (textViewTitleCenter.getText().length() > 10) {
            int titlePx = (int) (mainCircleRadius * 0.22);
            textViewTitleCenter.setTextSize(TypedValue.COMPLEX_UNIT_PX, titlePx);
        }

        // Adjusting subtitle in case it's very large
        if (textViewSectionCenter.getText().length() > 10 && textViewSectionCenter.getText().length() <= 14) {
            int sectionPx = (int) (mainCircleRadius * 0.18);
            textViewSectionCenter.setTextSize(TypedValue.COMPLEX_UNIT_PX, sectionPx);
        } else if (textViewSectionCenter.getText().length() > 14) {
            int sectionPx = (int) (mainCircleRadius * 0.17);
            textViewSectionCenter.setTextSize(TypedValue.COMPLEX_UNIT_PX, sectionPx);
        }

        progressView.setClipping(unitProgress);

        // animating rosco
        animateRoscoAppearance();
    }

    private void configTextAtRoscoCenter(ABAUnit unit, SectionType sectionToLoad) {
        if (unit.isCompleted()) {
            textViewTitleCenter.setText(getContext().getString(R.string.unitMenuCenterPremComKey));
            textViewSectionCenter.setText("");
            unitProgress = 100;
        } else {
            unitProgress = unit.getProgress();
            if (sectionToLoad == SectionType.kABAFilm && unitProgress == 0) {
                textViewTitleCenter.setText(getContext().getString(R.string.unitMenuCenterPremEmKey).toUpperCase());
            } else {
                textViewTitleCenter.setText(getContext().getString(R.string.unitMenuCenterPremConKey).toUpperCase());
            }
            textViewSectionCenter.setText(sectionVerbalAction(sectionToLoad));
        }
    }

    private void animateRoscoAppearance() {
        // first animating the central part
        animate().alpha(1.0f).setDuration(FADE_IN_DURATION).setStartDelay(0);
        // animating sections
        animateSections();
    }

    private void animateSections() {

        Realm realmInstance = Realm.getInstance(ABAApplication.get().getRealmConfiguration());
        ABAUnit unit = DataStore.getInstance().getLevelUnitController().getUnitWithId(realmInstance, currentUnitId);

        // Iterating section list to animate the alpha channel
        // Show Rosco for TypeUser
        updateViewForUserType(unit, unitRoscoView);

        realmInstance.close();
    }

    private String sectionVerbalAction(SectionType sectionToLoad) {
        switch (sectionToLoad) {
            case kABAFilm:
                return getContext().getResources().getString(R.string.unitMenuTitle1Key).toUpperCase();
            case kHabla:
                return getContext().getResources().getString(R.string.unitMenuTitle2Key).toUpperCase();
            case kEscribe:
                return getContext().getResources().getString(R.string.unitMenuTitle3Key).toUpperCase();
            case kInterpreta:
                return getContext().getResources().getString(R.string.unitMenuTitle4Key).toUpperCase();
            case kVideoClase:
                return getContext().getResources().getString(R.string.unitMenuTitle5Key).toUpperCase();
            case kEjercicios:
                return getContext().getResources().getString(R.string.unitMenuTitle6Key).toUpperCase();
            case kVocabulario:
                return getContext().getResources().getString(R.string.unitMenuTitle7Key).toUpperCase();
            case kEvaluacion:
                return getContext().getResources().getString(R.string.unitMenuTitle8Key).toUpperCase();
            default:
                return "";
        }
    }

    private void initiateView() {

        // Calculating device width
        int screenWidth = BZUtils.getScreenWidht((ABAMasterActivity) getContext());
        int screenHeight = BZUtils.getScreenHeight((ABAMasterActivity) getContext());
        int calculatedWidth = Math.min(screenHeight, screenWidth);

        // Adjusting rosco size depending on the screen orientation
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            mainCircleRadius = (int) (calculatedWidth * 0.30 / 2);
            sectionAreaWidth = (int) (calculatedWidth * 0.2);
            sectionCircleRadius = (int) (mainCircleRadius * 0.33);
        } else {
            mainCircleRadius = (int) (calculatedWidth * 0.4 / 2);
            sectionAreaWidth = (int) (calculatedWidth * 0.3);
            sectionCircleRadius = (int) (mainCircleRadius * 0.33);
        }

        initMainCircle();

        post(new Runnable() {
            @Override
            public void run() {

                Realm realmInstance = Realm.getInstance(ABAApplication.get().getRealmConfiguration());
                ABAUnit unit = DataStore.getInstance().getLevelUnitController().getUnitWithId(realmInstance, currentUnitId);
                SectionType currentSection = DataStore.getInstance().getRoscoController().getCurrentSectionForUnit(unit);

                for (SectionType sectionType : DataStore.getInstance().getRoscoController().getAllSectionsAscendently()) {
                    configureSection(unit, sectionType);
                }

                updateRosco(unit, currentSection);
                realmInstance.close();
            }
        });
    }

    private void configureSection(ABAUnit unit, final SectionType sectionType) {

        // Creating section image
        final UnitSectionView sectionView = new UnitSectionView(getContext(), sectionType, sectionAreaWidth, sectionCircleRadius);
        sectionView.setTag(sectionType);
        sectionView.updateWithSectionStatus(DataStore.getInstance().getRoscoController().getStatusForSection(unit, sectionType));
        sectionView.setOnClickListener(new SectionActionOnClickListener());

        // To animate fade in
        sectionView.getNumberAndLockContrainerLayout().setAlpha(0);
        sectionView.getNameForSectionContainerLayout().setAlpha(0);
        sectionView.getSectionImageContainerLayout().setAlpha(0);

        // Calculating params and margins
        RelativeLayout.LayoutParams imgParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        double absoluteMarginFromCenter = mainCircleRadius + sectionCircleRadius;

        // Calculating angle and desired margins
        double angleInRadians = Math.toRadians(getAngleForSection(sectionType));
        int axis_margin_x = (int) Math.abs(Math.sin(angleInRadians) * absoluteMarginFromCenter);
        int axis_margin_y = (int) Math.abs(Math.cos(angleInRadians) * absoluteMarginFromCenter);

        int diff_axis_margin_x = (int) Math.abs(axis_margin_x - absoluteMarginFromCenter);
        int diff_axis_margin_y = (int) Math.abs(axis_margin_y - absoluteMarginFromCenter);

        // Serring
        switch (sectionType) {
            case kABAFilm:
                imgParams.addRule(ABOVE, CENTRAL_ROSCO_ID);
                imgParams.addRule(CENTER_HORIZONTAL);
                break;
            case kHabla:
                imgParams.setMargins(-diff_axis_margin_x, 0, 0, -diff_axis_margin_y);
                imgParams.addRule(ABOVE, CENTRAL_ROSCO_ID);
                imgParams.addRule(RIGHT_OF, CENTRAL_ROSCO_ID);
                break;
            case kEscribe:
                imgParams.addRule(RIGHT_OF, CENTRAL_ROSCO_ID);
                imgParams.addRule(CENTER_VERTICAL);
                break;
            case kInterpreta:
                imgParams.setMargins(-diff_axis_margin_x, -diff_axis_margin_y, 0, 0);
                imgParams.addRule(BELOW, CENTRAL_ROSCO_ID);
                imgParams.addRule(RIGHT_OF, CENTRAL_ROSCO_ID);
                break;
            case kVideoClase:
                imgParams.addRule(BELOW, CENTRAL_ROSCO_ID);
                imgParams.addRule(CENTER_HORIZONTAL);
                break;
            case kEjercicios:
                imgParams.addRule(BELOW, CENTRAL_ROSCO_ID);
                imgParams.addRule(LEFT_OF, CENTRAL_ROSCO_ID);
                imgParams.setMargins(0, -diff_axis_margin_y, -diff_axis_margin_x, 0);
                break;
            case kVocabulario:
                imgParams.addRule(LEFT_OF, CENTRAL_ROSCO_ID);
                imgParams.addRule(CENTER_VERTICAL);
                break;
            case kEvaluacion:
                imgParams.addRule(ABOVE, CENTRAL_ROSCO_ID);
                imgParams.addRule(LEFT_OF, CENTRAL_ROSCO_ID);
                imgParams.setMargins(0, 0, -diff_axis_margin_x, -diff_axis_margin_y);
                break;
        }
        addView(sectionView, imgParams);
    }

    private void initMainCircle() {
        // Calculating units to further use
        int grayCircleRadius = mainCircleRadius * 2;
        int progressCircleRadius = (int) (grayCircleRadius * 0.95);
        int blueCircleRadius = (int) (grayCircleRadius * 0.8);

        // Bitmap for gray circle
        Bitmap bigGrayCircleBitmap = Bitmap.createBitmap(grayCircleRadius, grayCircleRadius, Bitmap.Config.ARGB_8888);
        bigGrayCircleBitmap.eraseColor(ContextCompat.getColor(getContext(), R.color.abaGrey));

        // LayoutParams for circle
        RelativeLayout.LayoutParams grayCircleParams = new RelativeLayout.LayoutParams(grayCircleRadius, grayCircleRadius);
        grayCircleParams.addRule(CENTER_IN_PARENT);

        // Creating circle and setting properties
        RoundedImageView centerGrayCircle = new RoundedImageView(getContext());
        centerGrayCircle.setId(CENTRAL_ROSCO_ID);
        centerGrayCircle.setImageBitmap(bigGrayCircleBitmap);
        centerGrayCircle.setLayoutParams(grayCircleParams);
        centerGrayCircle.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.abaGrey));
        addView(centerGrayCircle);

        RelativeLayout.LayoutParams progressCircleParams = new RelativeLayout.LayoutParams(progressCircleRadius, progressCircleRadius);
        progressCircleParams.addRule(CENTER_IN_PARENT);

        progressView = new SemiCircleProgressBarView(getContext(), progressCircleRadius, progressCircleRadius);
        progressView.setLayoutParams(progressCircleParams);
        progressView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        addView(progressView);

        // Layoutparams for circle
        RelativeLayout.LayoutParams blueCircleParams = new RelativeLayout.LayoutParams(blueCircleRadius, blueCircleRadius);
        blueCircleParams.addRule(CENTER_IN_PARENT);

        FrameLayout centerBlueCircle = new FrameLayout(getContext());
        centerBlueCircle.setBackgroundResource(R.drawable.selector_rosco_central_background);
        centerBlueCircle.setLayoutParams(blueCircleParams);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            TypedValue outValue = new TypedValue();
            getContext().getTheme().resolveAttribute(android.support.design.R.attr.selectableItemBackground, outValue, true);
            centerBlueCircle.setForeground(getContext().getDrawable(outValue.resourceId));
        }
        centerBlueCircle.setOnClickListener(new RoscoActionOnClickListener());
        addView(centerBlueCircle);

        // Layoutparams for text within circle
        RelativeLayout.LayoutParams circleTextParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        circleTextParams.addRule(CENTER_IN_PARENT);

        LinearLayout linearLayout = new LinearLayout(getContext());
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.setLayoutParams(circleTextParams);
        linearLayout.setGravity(Gravity.CENTER_HORIZONTAL);
        addView(linearLayout);

        // Calculating text sizes
        int titlePx = (int) (mainCircleRadius * 0.25);
        int sectionPx = (int) (mainCircleRadius * 0.20);

        // Texts within the circle
        textViewTitleCenter = new ABATextView(getContext());
        textViewTitleCenter.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
        textViewTitleCenter.setTextColor(ContextCompat.getColor(getContext(), R.color.abaDarkBlue));
        textViewTitleCenter.setTextSize(TypedValue.COMPLEX_UNIT_PX, titlePx);
        textViewTitleCenter.setABATag(getContext().getResources().getInteger(R.integer.typefaceSans900));
        textViewTitleCenter.setGravity(Gravity.CENTER);
        linearLayout.addView(textViewTitleCenter);

        textViewSectionCenter = new ABATextView(getContext());
        textViewSectionCenter.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT));
        textViewSectionCenter.setTextColor(ContextCompat.getColor(getContext(), R.color.abaWhite));
        textViewSectionCenter.setTextSize(TypedValue.COMPLEX_UNIT_PX, sectionPx);
        textViewSectionCenter.setABATag(getContext().getResources().getInteger(R.integer.typefaceSans500));
        textViewSectionCenter.setGravity(Gravity.CENTER);
        textViewSectionCenter.setPadding(0, 10, 0, 0);
        linearLayout.addView(textViewSectionCenter);

        // The rosco starts with alpha 0
        setAlpha(0);
    }

    /**
     * Method that returns the angle formed by the section and the center of the rosco
     *
     * @param sectionType section to calculate the angle with
     * @return 0-360 degrees
     */
    private float getAngleForSection(SectionType sectionType) {
        switch (sectionType) {
            case kABAFilm:
                return 0;
            case kHabla:
                return 45;
            case kEscribe:
                return 90;
            case kInterpreta:
                return 135;
            case kVideoClase:
                return 180;
            case kEjercicios:
                return 225;
            case kVocabulario:
                return 270;
            case kEvaluacion:
                return 315;
        }
        return 0;
    }

    private void updateViewForUserType(ABAUnit unit, UnitRoscoView unitRoscoView) {

        // Reading accessible sections
        List<SectionType> accessibleSectionType = DataStore.getInstance().getRoscoController().getAccessibleSections(unit);
        List<SectionType> allSections = DataStore.getInstance().getRoscoController().getAllSectionsAscendently();

        // Iterating all sections
        for (SectionType sectionTypeToAnimate : allSections) {
            UnitSectionView sectionToAnimate = (UnitSectionView) unitRoscoView.findViewWithTag(sectionTypeToAnimate);
            int index = allSections.indexOf(sectionTypeToAnimate);

            if (accessibleSectionType.contains(sectionTypeToAnimate)) {
                sectionToAnimate.getNumberAndLockContrainerLayout().animate().alpha(1.0f).setDuration(UnitRoscoView.FADE_IN_DURATION).setStartDelay(UnitRoscoView.FADE_IN_DELAY * index);
                sectionToAnimate.getNameForSectionContainerLayout().animate().alpha(1.0f).setDuration(UnitRoscoView.FADE_IN_DURATION).setStartDelay(UnitRoscoView.FADE_IN_DELAY * index);
                sectionToAnimate.getSectionImageContainerLayout().animate().alpha(1.0f).setDuration(UnitRoscoView.FADE_IN_DURATION).setStartDelay(UnitRoscoView.FADE_IN_DELAY * index);
            } else {
                sectionToAnimate.getNumberAndLockContrainerLayout().animate().alpha(1.0f).setDuration(UnitRoscoView.FADE_IN_DURATION).setStartDelay(UnitRoscoView.FADE_IN_DELAY * index);
                sectionToAnimate.getNameForSectionContainerLayout().animate().alpha(0.6f).setDuration(UnitRoscoView.FADE_IN_DURATION).setStartDelay(UnitRoscoView.FADE_IN_DELAY * index);
                sectionToAnimate.getSectionImageContainerLayout().animate().alpha(0.6f).setDuration(UnitRoscoView.FADE_IN_DURATION).setStartDelay(UnitRoscoView.FADE_IN_DELAY * index);
            }
        }
    }

    // ================================================================================
    // Accessors
    // ================================================================================

    private UnitRoscoHandler getUnitRoscoHandler() {
        return unitRoscoHandler;
    }

    public void setUnitRoscoHandler(UnitRoscoHandler unitRoscoHandler) {
        this.unitRoscoHandler = unitRoscoHandler;
    }

    // ================================================================================
    // Public interfaces
    // ================================================================================

    public interface UnitRoscoHandler {
        void onSectionTap(SectionType sectionType);

        void onCentralTap();
    }

    // ================================================================================
    // Inner classes
    // ================================================================================

    private class SemiCircleProgressBarView extends View {

        private Path mClippingPath;
        private Context mContext;
        private Bitmap mBitmap;
        private int circleWidth;
        private int circleHeight;

        private SemiCircleProgressBarView(Context context, int circleWidth, int circleHeight) {
            super(context);
            mContext = context;
            this.circleHeight = circleHeight;
            this.circleWidth = circleWidth;
            initializeImage();
        }

        private void initializeImage() {
            mClippingPath = new Path();

            // Adjust the image size to support different screen sizes
            Bitmap bitmap = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.circle);
            mBitmap = Bitmap.createScaledBitmap(bitmap, circleWidth, circleHeight, false);
        }

        /**
         * Method to update progress
         *
         * @param progress from 0 to 100
         */
        private void setClipping(float progress) {
            float angle = (progress * 360) / 100;
            mClippingPath.reset();
            // Define a rectangle containing the image
            RectF oval = new RectF(0, 0, circleWidth, circleHeight);
            // Move the current position to center of rect
            mClippingPath.moveTo(oval.centerX(), oval.centerY());
            // Draw an arc from center to given angle
            mClippingPath.addArc(oval, 270, angle);
            // Draw a line from end of arc to center
            mClippingPath.lineTo(oval.centerX(), oval.centerY());
            // Redraw the canvas
            invalidate();
        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);

            // Clip the canvas
            canvas.clipPath(mClippingPath);
            canvas.drawBitmap(mBitmap, 0, 0, null);
        }
    }

    /**
     * Class to handle action within the rosco center
     */
    private class RoscoActionOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            getUnitRoscoHandler().onCentralTap();
        }
    }

    /**
     * Class to handle action on every section
     */
    private class SectionActionOnClickListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            getUnitRoscoHandler().onSectionTap((SectionType) view.getTag());
        }
    }
}
