package com.abaenglish.videoclass.presentation.base;

import com.abaenglish.videoclass.data.network.APIManager;
import com.abaenglish.videoclass.data.persistence.ABAAPIError;
import com.abaenglish.videoclass.domain.content.DataController;
import com.abaenglish.videoclass.domain.content.ProgressActionController;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.abaenglish.videoclass.presentation.base.custom.ABAProgressDialog;
import com.bzutils.LogBZ;
import com.crashlytics.android.Crashlytics;

import org.json.JSONException;
import org.json.JSONObject;

///TODO Delete this class ASAP

/**
 * Created by vmadalin on 11/3/16.
 * Refactored by Jesus on 28/07/16
 */
public abstract class ABAMasterSessionActivity extends ABAMasterActivity {

    protected void loginWithFacebook() {
        showProgressDialog(ABAProgressDialog.SimpleDialog);
        loginWithFacebook(new DataController.ABAAPIFacebookCallback() {
            @Override
            public void onSuccess(final Boolean isNewUser) {
                DataStore.getInstance().getEvaluationController().saveProgressActionForSection(getRealm(), null, null, null, null, false, false);
                APIManager.getInstance().getPublicIp(new APIManager.ABAAPIResponseCallback() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jObject = new JSONObject(response);
                            ProgressActionController.setUserIP(jObject.get("publicIp").toString());
                        } catch (JSONException e) {
                            Crashlytics.logException(e);
                            e.printStackTrace();
                        }
                        dismissProgressDialog();
                        LogBZ.d("Facebook Login API success");
                        startABAForUser(isNewUser);
                    }

                    @Override
                    public void onError(Exception e) {
                        dismissProgressDialog();
                        LogBZ.d("Facebook Login API success");
                        startABAForUser(isNewUser);
                    }
                });
            }

            @Override
            public void onError(ABAAPIError error) {
                dismissProgressDialog();
                if (error != null) {
                    error.showABANotificationError(ABAMasterSessionActivity.this);
                    LogBZ.e("Facebook Login API error: " + error.getError());
                }
            }
        });
    }
}
