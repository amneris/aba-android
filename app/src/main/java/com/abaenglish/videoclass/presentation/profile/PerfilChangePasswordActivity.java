package com.abaenglish.videoclass.presentation.profile;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.abaenglish.videoclass.ABAApplication;
import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.data.persistence.ABAAPIError;
import com.abaenglish.videoclass.data.persistence.ABAUser;
import com.abaenglish.videoclass.domain.content.DataController;
import com.abaenglish.videoclass.domain.content.UserController;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.abaenglish.videoclass.presentation.base.ABAMasterActivity;
import com.abaenglish.videoclass.presentation.base.custom.ABAEditText;
import com.abaenglish.videoclass.presentation.base.custom.ABAProgressDialog;
import com.abaenglish.videoclass.presentation.base.custom.ABATextView;
import com.abaenglish.videoclass.presentation.base.custom.FontCache;
import com.abaenglish.videoclass.session.SessionService;
import com.bzutils.LogBZ;
import com.crashlytics.android.Crashlytics;

/**
 * Created by madalin on 17/06/15.
 */
public class PerfilChangePasswordActivity extends ABAMasterActivity implements View.OnClickListener {

    private ABAEditText changePasswordKey1;
    private ABAEditText changePasswordKey2;
    private ABAEditText changePasswordKey3;
    private ABATextView changePasswordButton;
    private DataController.ABAAPICallback callback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        configActionBar();
        config();
        Crashlytics.log(Log.INFO, "Change Password", "User opens Change Password View thru Profile");
    }

    private void configActionBar() {
        findViewById(R.id.toolbarLeftButton).setOnTouchListener(((ABAApplication) getApplicationContext()).getImagePressedState(null));
        findViewById(R.id.toolbarLeftButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishActivity();
            }
        });

        TextView title = (TextView) findViewById(R.id.toolbarTitle);
        TextView title2 = (TextView) findViewById(R.id.toolbarSubTitle);

        title.setText(getResources().getString(R.string.menuKey));
        title2.setText(getResources().getString(R.string.changePasswordTitleKey));
    }

    private void config() {
        changePasswordKey1 = (ABAEditText) findViewById(R.id.changePasswordKey1);
        changePasswordKey2 = (ABAEditText) findViewById(R.id.changePasswordKey2);
        changePasswordKey3 = (ABAEditText) findViewById(R.id.changePasswordKey3);
        changePasswordButton = (ABATextView) findViewById(R.id.changePasswordButton);

        changePasswordKey1.setTypeface(fontCache.getTypeface(FontCache.ABATypeface.sans500));
        changePasswordKey2.setTypeface(fontCache.getTypeface(FontCache.ABATypeface.sans500));
        changePasswordKey3.setTypeface(fontCache.getTypeface(FontCache.ABATypeface.sans500));

        changePasswordButton.setOnClickListener(this);

    }

    private void finishActivity() {
        finish();
        overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_exit_unitanimation2);
    }

    public void onBackPressed() {
        finishActivity();
    }

    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.changePasswordButton:
                checkOldPassword();
                break;
        }
    }

    private void changePassword() {
        mSessionService.changePassword(changePasswordKey2.getText().toString(), new DataController.ABAAPICallback<Boolean>() {
            @Override
            public void onSuccess(Boolean response) {
                dismissProgressDialog();
                finishActivity();
            }

            @Override
            public void onError(ABAAPIError e) {
                dismissProgressDialog();
                showABAErrorNotification(e.toString());
            }
        });
    }

    private void checkFields() {

        if (!changePasswordKey2.getText().toString().equals(changePasswordKey3.getText().toString())) {
            showABAErrorNotification(getString(R.string.changePassErrorEqualPassword));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    changePasswordKey2.setSelected(true);
                }
            }, 150);
            focusFieldAfterError(changePasswordKey2);
        } else if (!UserController.isPasswordValid(changePasswordKey2.getText().toString())) {
            showABAErrorNotification(getString(R.string.regErrorNewPasswordMin));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    changePasswordKey2.setSelected(true);
                }
            }, 150);
            focusFieldAfterError(changePasswordKey2);
        } else {
            changePassword();
        }

    }

    private void checkOldPassword() {
        showProgressDialog(ABAProgressDialog.SimpleDialog);

        ABAUser abaUser = DataStore.getInstance().getUserController().getCurrentUser(getRealm());

        mSessionService.checkEmailAndPassword(abaUser.getEmail(), changePasswordKey1.getText().toString(), new DataController.ABAAPICallback<Boolean>() {
            @Override
            public void onSuccess(Boolean response) {
                dismissProgressDialog();

                if (response) {
                    checkFields();
                } else {
                    showABAErrorNotification(getString(R.string.changePassErrorOldPassword));
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            changePasswordKey1.setSelected(true);
                        }
                    }, 150);
                    focusFieldAfterError(changePasswordKey1);
                }
            }

            @Override
            public void onError(ABAAPIError error) {
                dismissProgressDialog();
                if (error != null) {
                    error.showABANotificationError(PerfilChangePasswordActivity.this);
                    LogBZ.e("Facebook Login API error: " + error.getError());
                }
            }
        });
    }
}