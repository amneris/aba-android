package com.abaenglish.videoclass.presentation.unit.variation;

import com.abaenglish.shepherd.ABAShepherdEditor;
import com.abaenglish.shepherd.plugin.plugins.ShepherdEvaluationPlugin;
import com.abaenglish.videoclass.ABAApplication;
import com.abaenglish.videoclass.data.persistence.ABAEvaluation;
import com.abaenglish.videoclass.data.persistence.ABAExercises;
import com.abaenglish.videoclass.data.persistence.ABAFilm;
import com.abaenglish.videoclass.data.persistence.ABAInterpret;
import com.abaenglish.videoclass.data.persistence.ABASpeak;
import com.abaenglish.videoclass.data.persistence.ABAVideoClass;
import com.abaenglish.videoclass.data.persistence.ABAVocabulary;
import com.abaenglish.videoclass.data.persistence.ABAWrite;
import com.abaenglish.videoclass.domain.content.LevelUnitController;
import com.abaenglish.videoclass.domain.content.UserController;
import com.abaenglish.videoclass.domain.datastore.DataStore;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

/**
 * Created by xabierlosada on 10/10/16.
 */

@SuppressWarnings("Unchecked")
public class SectionRepositoryImpl implements SectionRepository {

    final static String UNIT_ID_QUERY_PARAM = "unit.idUnit";
    private final boolean premiumUser;
    private String unitId;


    public SectionRepositoryImpl() {
        premiumUser = UserController.getInstance().isUserPremium();
    }

    @Override
    public List<SectionViewModel> getSections(String unitId) {

        this.unitId = unitId;

        List<SectionViewModel> sections = new ArrayList<>();
        Realm realm = Realm.getDefaultInstance();
        DataStore dataStore = DataStore.getInstance();
        boolean evaluationCompleted;
        int progress;

        ABAFilm film = realm.where(ABAFilm.class)
                .equalTo(UNIT_ID_QUERY_PARAM, unitId)
                .findFirst();

        progress = (int) dataStore.getFilmController().getProgressForSection(film);

        evaluationCompleted = addItemsAndGetCompleted(sections, film, progress);

        ABASpeak speak = realm.where(ABASpeak.class)
                .equalTo(UNIT_ID_QUERY_PARAM, unitId)
                .findFirst();

        progress = (int) dataStore.getSpeakController().getProgressForSection(speak);

        evaluationCompleted &= addItemsAndGetCompleted(sections, speak, progress);

        ABAWrite write = realm.where(ABAWrite.class)
                .equalTo(UNIT_ID_QUERY_PARAM, unitId)
                .findFirst();

        progress = (int) dataStore.getWriteController().getProgressForSection(write);

        evaluationCompleted &= addItemsAndGetCompleted(sections, write, progress);

        ABAInterpret interpret = realm.where(ABAInterpret.class)
                .equalTo(UNIT_ID_QUERY_PARAM, unitId)
                .findFirst();

        progress = (int) dataStore.getInterpretController().getProgressForSection(interpret);

        evaluationCompleted &= addItemsAndGetCompleted(sections, interpret, progress);

        ABAVideoClass videoClasses = realm.where(ABAVideoClass.class)
                .equalTo(UNIT_ID_QUERY_PARAM, unitId)
                .findFirst();

        progress = (int) dataStore.getVideoClassController().getProgressForSection(videoClasses);

        evaluationCompleted &= addItemsAndGetCompleted(sections, videoClasses, progress);

        ABAExercises exercise = realm.where(ABAExercises.class)
                .equalTo(UNIT_ID_QUERY_PARAM, unitId)
                .findFirst();

        progress = (int) dataStore.getExercisesController().getProgressForSection(exercise);

        evaluationCompleted &= addItemsAndGetCompleted(sections, exercise, progress);

        ABAVocabulary vocabulary = realm.where(ABAVocabulary.class)
                .equalTo(UNIT_ID_QUERY_PARAM, unitId)
                .findFirst();

        progress = (int) dataStore.getVocabularyController().getProgressForSection(vocabulary);
        evaluationCompleted &= addItemsAndGetCompleted(sections, vocabulary, progress);

        ABAEvaluation evaluation = realm.where(ABAEvaluation.class)
                .equalTo(UNIT_ID_QUERY_PARAM, unitId)
                .findFirst();

        addEvaluation(sections, evaluation, evaluationCompleted);

        realm.close();

        sections = calculateNextSection(sections);
        return sections;
    }

    private List<SectionViewModel> calculateNextSection(List<SectionViewModel> sections) {
        for (SectionViewModel section: sections) {
            if(section.isUnlocked() && section.getProgress()!= 100) {
                section.setNextSection();
                return sections;
            }
        }
        return sections;
    }

    private boolean addItemsAndGetCompleted(List<SectionViewModel> sections, Section section, int realProgress) {
        boolean unlocked = section.getType() == Section.SectionType.VIDEOCLASS || LevelUnitController.isFreeUnit(unitId) || premiumUser;
        sections.add(new SectionViewModel(section.getName(), realProgress, section.getIcon(), unlocked, section.getType()));
        return section.getCompletedPercentage() == 100;
    }

    private void addEvaluation(List<SectionViewModel> sections, Section section, boolean unlocked) {
        // Debug mode
        if (ABAShepherdEditor.isInternal()) {
            if (ShepherdEvaluationPlugin.isUnlockActive(ABAApplication.get())) {
                unlocked = true;
            }
        }
        sections.add(new SectionViewModel(section.getName(), section.getCompletedPercentage(), section.getIcon(), unlocked, section.getType()));
    }
}
