package com.abaenglish.videoclass.presentation.di;

import android.app.Application;
import android.content.Context;

import com.abaenglish.videoclass.ABAApplication;
import com.abaenglish.videoclass.analytics.TrackerContract;
import com.abaenglish.videoclass.data.CourseContentService;
import com.abaenglish.videoclass.data.network.oauth.OAuthTokenAccessor;
import com.abaenglish.videoclass.domain.ApplicationConfiguration;
import com.abaenglish.videoclass.domain.abTesting.ServerConfig;
import com.abaenglish.videoclass.domain.abTesting.ServerConfigImpl;
import com.abaenglish.videoclass.domain.abTesting.UnitDetailABTestingConfigurator;
import com.abaenglish.videoclass.presentation.base.custom.FontCache;
import com.abaenglish.videoclass.session.SessionService;
import com.nostra13.universalimageloader.core.ImageLoader;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.realm.RealmConfiguration;

/**
 * Created by xilosada on 27/06/16.
 */

@Module
public class ApplicationModule {

    private final Application application;

    public ApplicationModule(Application application) {
        this.application = application;
    }

    @Singleton
    @Provides
    Context providesContext() {
        return application;
    }

    @Singleton
    @Provides
    public RealmConfiguration providesRealm() {
        return ABAApplication.get().getRealmConfiguration();
    }

    @Singleton
    @Provides
    ImageLoader providesImageLoader() {
        return ImageLoader.getInstance();
    }

    @Singleton
    @Provides
    UnitDetailABTestingConfigurator providesABRouter(ServerConfig serverConfig) {
        return new UnitDetailABTestingConfigurator(serverConfig);
    }

    @Singleton
    @Provides
    FontCache providesFontCache() {
        return new FontCache(application);
    }

    @Singleton
    @Provides
    ServerConfig serverConfig(Context context, TrackerContract.Tracker tracker) {
        return new ServerConfigImpl(context, tracker);
    }

    @Singleton
    @Provides
    SessionService providesSessionService(CourseContentService contentService, ApplicationConfiguration appConfig) {
        return new SessionService(contentService, appConfig);
    }

    @Singleton
    @Provides
    CourseContentService provideContentService(ApplicationConfiguration appConfig) {
        return new CourseContentService(appConfig);
    }

    @Singleton
    @Provides
    OAuthTokenAccessor providesTokenAccessor(ApplicationConfiguration appConfig) {
        return new OAuthTokenAccessor(appConfig);
    }

    @Singleton
    @Provides
    ApplicationConfiguration providesAppConfiguration() {
        return new ApplicationConfiguration();
    }
}
