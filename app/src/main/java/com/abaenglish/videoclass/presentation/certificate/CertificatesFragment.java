package com.abaenglish.videoclass.presentation.certificate;

import android.util.Log;
import android.widget.ImageButton;
import android.widget.ListView;

import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.data.network.APIManager;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.abaenglish.videoclass.presentation.base.ABAMasterFragment;
import com.abaenglish.videoclass.presentation.base.custom.ABATextView;
import com.crashlytics.android.Crashlytics;

/**
 * Created by madalin on 16/04/15.
 */
public class CertificatesFragment extends ABAMasterFragment {

    @Override
    protected int onResLayout() {
        return R.layout.fragment_certificates;
    }

    @Override
    protected void onConfigView() {
        ListView certificatelist = $(R.id.certificatelist);

        if(APIManager.isTabletSize(getContext())) {
            certificatelist.setPadding((int) getResources().getDimension(R.dimen.padding10),(int) getResources().getDimension(R.dimen.padding10),
                    (int)getResources().getDimension(R.dimen.padding10),(int)getResources().getDimension(R.dimen.padding10));
        }

        certificatelist.setAdapter(new CertsAdapter(getContext(), DataStore.getInstance().getLevelUnitController().getAllLevelsDescending(getRealm())));
        Crashlytics.log(Log.INFO, "Certificates", "User opens Certificates view");
    }

    @Override
    protected void onConfigToolbar(ABATextView toolbarTitle, ABATextView toolbarSubTitle, ImageButton toolbarButton) {
        toolbarTitle.setText(getString(R.string.menuKey));
        toolbarSubTitle.setText(getString(R.string.yourCertificatesKey));
    }
}
