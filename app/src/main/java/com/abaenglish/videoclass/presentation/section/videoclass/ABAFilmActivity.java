package com.abaenglish.videoclass.presentation.section.videoclass;

import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.abaenglish.videoclass.ABAApplication;
import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.analytics.cooladata.navigation.CooladataNavigationTrackingManager;
import com.abaenglish.videoclass.data.ConnectionService;
import com.abaenglish.videoclass.data.persistence.ABAFilm;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.data.persistence.ABAUser;
import com.abaenglish.videoclass.data.persistence.ABAVideoClass;
import com.abaenglish.videoclass.domain.LanguageController;
import com.abaenglish.videoclass.domain.content.LevelUnitController;
import com.abaenglish.videoclass.domain.content.UserController;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.abaenglish.videoclass.presentation.base.ABAMasterActivity;
import com.abaenglish.videoclass.presentation.base.custom.ABATextView;
import com.abaenglish.videoclass.presentation.base.custom.TeacherBannerView;
import com.abaenglish.videoclass.presentation.section.NextSectionDialog_;
import com.abaenglish.videoclass.presentation.section.SectionActivityInterface;
import com.abaenglish.videoclass.presentation.section.SectionHelper;
import com.abaenglish.videoclass.presentation.section.SectionHelper.Section;
import com.abaenglish.videoclass.presentation.section.SectionType;
import com.bzutils.Connectivity;
import com.crashlytics.android.Crashlytics;
import com.nostra13.universalimageloader.core.ImageLoader;

import static com.abaenglish.videoclass.analytics.cooladata.CooladataTrackingDefinitions.CooladataSectionType.ABAFilm;
import static com.abaenglish.videoclass.analytics.cooladata.CooladataTrackingDefinitions.CooladataSectionType.ABAVideoClass;
import static com.abaenglish.videoclass.presentation.section.SectionHelper.SECTION_FILM;
import static com.abaenglish.videoclass.presentation.section.SectionHelper.SECTION_VIDEO_CLASS;
import static com.abaenglish.videoclass.presentation.section.SectionType.kABAFilm;

/**
 * Created by madalin on 4/05/15.
 */
public class ABAFilmActivity extends ABAMasterActivity implements SectionActivityInterface {

    public static final String SECTION_ID = "SECTION_ID";
    public static final String IS_DOWNLOADED = "IS_DOWNLOADED";
    public static final int FILM_SECTION = 0;
    public static final int VIDEOCLASS_SECTION = 1;

    public static final int VIDEO_PLAYER_REQUEST_CODE = 100;

    private ABAUnit currentUnit;
    private ABATextView subFirstText;
    private ABATextView subSecondText;
    private ABATextView subThreeText;
    private ImageView subArrow;
    private LinearLayout upLayout;
    private RelativeLayout downLayout;
    private TranslateAnimation upAnimation;
    private TranslateAnimation downAnimation;
    private ImageView playIcon;
    private ImageView separator;
    private boolean open = false;
    private String tmpString;
    private TeacherBannerView teacherBannerView;

    public SectionType sectionType;
    public boolean CompletedMode = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_film);

        currentUnit = DataStore.getInstance().getLevelUnitController().getUnitWithId(getRealm(), getIntent().getExtras().getString(EXTRA_UNIT_ID));

        if (getIntent().getExtras().getInt(SECTION_ID) == FILM_SECTION) {
            sectionType = kABAFilm;
        } else {
            sectionType = SectionType.kVideoClase;
        }

        configActionBar();
        configBackground();
        config();

        if (sectionType == kABAFilm) {
            configTeacher();
            CompletedMode = DataStore.getInstance().getFilmController().isSectionCompleted(currentUnit.getSectionFilm());
        } else {
            configTeacher();
            CompletedMode = DataStore.getInstance().getVideoClassController().isSectionCompleted(currentUnit.getSectionVideoClass());
        }

        trackSection();
        
    }

    private void configActionBar() {
        findViewById(R.id.toolbarLeftButton).setOnTouchListener(((ABAApplication) getApplicationContext()).getImagePressedState(null));
        findViewById(R.id.toolbarLeftButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishSection(false);
            }
        });

        updateSectionProgress();
    }

    @Override
    public void configTeacher() {
        ABAUser currentUser = DataStore.getInstance().getUserController().getCurrentUser(getRealm());

        teacherBannerView = (TeacherBannerView) findViewById(R.id.detailUnitTeacherView);

        if (DataStore.getInstance().getLevelUnitController().checkIfFileExist(currentUnit, currentUser.getTeacherImage())) {
            DataStore.getInstance().getLevelUnitController().displayImage(null, currentUser.getTeacherImage(), teacherBannerView.getImageView());
        } else {
            teacherBannerView.setImageUrl(currentUser.getTeacherImage());
        }

        if (sectionType == kABAFilm) {
            if (currentUnit.getSectionFilm().getProgress() == 0) {
                teacherBannerView.setText(getString(R.string.filmSectionTeacher1Key));
            } else {
                teacherBannerView.setVisibility(View.GONE);
            }

        } else {
            if (currentUnit.getSectionVideoClass().getProgress() == 0) {
                if (DataStore.getInstance().getUserController().isUserPremium())
                    teacherBannerView.setText(getString(R.string.videoClassSectionTeacher1Key));
                else
                    teacherBannerView.setText(getString(R.string.videoClassSectionTeacher1NOPremiumKey));
            } else {
                teacherBannerView.setVisibility(View.GONE);
            }

        }
    }

    @Override
    public void updateSectionProgress() {
        ABATextView unitSectionTitle = (ABATextView) findViewById(R.id.toolbarTitle);
        ABATextView unitSectionProgress = (ABATextView) findViewById(R.id.toolbarSubTitle);

        if (sectionType == kABAFilm) {
            unitSectionTitle.setText(R.string.filmSectionKey);
            unitSectionProgress.setText((int) currentUnit.getSectionFilm().getProgress() + "%");
        } else {
            unitSectionTitle.setText(R.string.unitMenuTitle5Key);
            unitSectionProgress.setText((int) currentUnit.getSectionVideoClass().getProgress() + "%");
        }
    }

    private void configBackground() {
        ImageView background = (ImageView) findViewById(R.id.sectionFilmBackground);
        if (sectionType == kABAFilm) {
            if (DataStore.getInstance().getLevelUnitController().checkIfFileExist(currentUnit, currentUnit.getFilmImageUrl())) {
                DataStore.getInstance().getLevelUnitController().displayImage(currentUnit, currentUnit.getFilmImageUrl(), background);
            } else {
                ImageLoader.getInstance().displayImage(currentUnit.getFilmImageUrl(), background);
            }
        } else {
            if (DataStore.getInstance().getLevelUnitController().checkIfFileExist(currentUnit, currentUnit.getVideoClassImageUrl())) {
                DataStore.getInstance().getLevelUnitController().displayImage(currentUnit, currentUnit.getVideoClassImageUrl(), background);
            } else {
                ImageLoader.getInstance().displayImage(currentUnit.getVideoClassImageUrl(), background);
            }
        }
    }

    private void config() {
        final ImageView playButton = (ImageView) findViewById(R.id.play_button);
        subFirstText = (ABATextView) findViewById(R.id.subFirstText);
        subSecondText = (ABATextView) findViewById(R.id.subSecondText);
        subThreeText = (ABATextView) findViewById(R.id.subThreeText);
        subArrow = (ImageView) findViewById(R.id.sub_arrow);
        upLayout = (LinearLayout) findViewById(R.id.upLayout);
        downLayout = (RelativeLayout) findViewById(R.id.downLayout);
        subArrow.setImageResource(R.mipmap.subs_button);
        separator = (ImageView) findViewById(R.id.separator);

        if (LanguageController.getCurrentLanguage().equals("en")) {
            subFirstText.setVisibility(View.GONE);
            separator.setVisibility(View.GONE);
            subThreeText.setText(getString(R.string.subsEnglishSelected));
        }

        subFirstText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (open) {
                    tmpString = subThreeText.getText().toString();
                    if (tmpString.equals(getResources().getString(R.string.subsNoneSelected))) {
                        String tmpString2 = subSecondText.getText().toString();
                        subThreeText.setText(subFirstText.getText());
                        subFirstText.setText(tmpString2);
                        subSecondText.setText(tmpString);
                    } else {
                        subThreeText.setText(subFirstText.getText());
                        subFirstText.setText(tmpString);
                    }
                    closeSubsOptions();
                }
            }
        });

        subSecondText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (open) {
                    tmpString = subThreeText.getText().toString();
                    subThreeText.setText(subSecondText.getText());
                    subSecondText.setText(tmpString);
                    closeSubsOptions();
                }
            }
        });


        downLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                upLayout.setTranslationY(0);
                closeSubsOptions();
            }
        });

        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!ConnectionService.isNetworkAvailable(getApplicationContext())
                        && !getIntent().getExtras().getBoolean(IS_DOWNLOADED)) {
                    showABAErrorNotification(getResources().getString(R.string.errorConnection));
                } else {
                    if (open) {
                        upLayout.setTranslationY(upLayout.getHeight());
                        subArrow.setImageResource(R.mipmap.subs_button);
                        open = false;
                    }
                    String subLenguageSelected = subThreeText.getText().toString();
                    Intent intent = new Intent(ABAFilmActivity.this, PlayerActivity.class);

                    intent.putExtra(PlayerActivity.IS_DOWNLOADED, getIntent().getExtras().getBoolean(IS_DOWNLOADED));
                    intent.putExtra(PlayerActivity.VIDEO_URL_EXTRA, getVideoURL());
                    intent.putExtra(PlayerActivity.VIDEO_SUBTITLE_EXTRA, getSubtitleURL(subLenguageSelected));
                    intent.putExtra(PlayerActivity.SUB_OPTION, subLenguageSelected);
                    intent.putExtra(SectionActivityInterface.EXTRA_UNIT_ID, getIntent().getExtras().getString(EXTRA_UNIT_ID));
                    intent.putExtra(PlayerActivity.SECTION_ID, getIntent().getExtras().getInt(SECTION_ID));
                    intent.putExtra(PlayerActivity.COMPLETED_MODE, CompletedMode);
                    startActivityForResult(intent, VIDEO_PLAYER_REQUEST_CODE);
                }
            }
        });
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        //Here you can get the size!
        upAnimation = new TranslateAnimation(0, 0, upLayout.getHeight(), 0);
        downAnimation = new TranslateAnimation(0, 0, 0, upLayout.getHeight());
        upAnimation.setDuration(300);
        upAnimation.setFillAfter(true);
        downAnimation.setDuration(300);
        downAnimation.setFillAfter(true);
        upLayout.setTranslationY(upLayout.getHeight());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == VIDEO_PLAYER_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {

                // Completing Film vs Videoclass
                if (sectionType == kABAFilm) {
                    DataStore.getInstance().getFilmController().setCompletedSection(getRealm(), currentUnit.getSectionFilm());
                } else {
                    DataStore.getInstance().getVideoClassController().setCompletedSection(getRealm(), currentUnit.getSectionVideoClass());
                }

                // Dialog vs finish
                if (DataStore.getInstance().getUserController().isUserPremium() || LevelUnitController.isFreeUnit(currentUnit.getIdUnit())) {
                    int section = sectionType.equals(kABAFilm) ? SECTION_FILM : SECTION_VIDEO_CLASS;
                    if (!serverConfig.isImprovedLinearCourseActive() || NextSectionDialog_.checkNextSectionIsDone(currentUnit.getIdUnit(), section)) {
                        finishSection(true);
                    } else {
                        showNextSectionDialog();
                    }
                } else {
                    finishSection(true);
                }
            }
        }
    }

    private void showNextSectionDialog() {
        FragmentManager fragmentManager = getFragmentManager();
        @Section int section = sectionType.equals(kABAFilm) ? SECTION_FILM : SECTION_VIDEO_CLASS;
        NextSectionDialog_ ndf = NextSectionDialog_.newInstance(section, currentUnit.getIdUnit());
        ndf.addListener(this);
        ndf.show(fragmentManager, "Film");
    }

    private void closeSubsOptions() {
        if (!open) {
            subArrow.setImageResource(R.mipmap.subs_button_invert);
            upLayout.setAnimation(upAnimation);
            upLayout.startAnimation(upAnimation);
            open = true;
        } else {
            subArrow.setImageResource(R.mipmap.subs_button);
            upLayout.setAnimation(downAnimation);
            upLayout.startAnimation(downAnimation);
            open = false;
        }
    }

    @Override
    public void finishSection(boolean sectionCompleted) {
        if (sectionCompleted) {
            tracker.trackWatchedFilm(currentUnit.getLevel().getIdLevel(), currentUnit.getIdUnit(), String.valueOf(sectionType.getValue()));
            unitDetailABTestingConfigurator.startUnitDetailIntent(this, getIntent().getExtras().getString(EXTRA_UNIT_ID), sectionType);
        } else {
            unitDetailABTestingConfigurator.startUnitDetailIntent(this, getIntent().getExtras().getString(EXTRA_UNIT_ID));
        }
        finish();
        overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_exit_unitanimation2);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishSection(false);
    }

    private String getVideoURL() {
        if (sectionType == kABAFilm) { //es abafilm
            ABAFilm film = currentUnit.getSectionFilm();

            if (Connectivity.isConnectedMobile(getApplicationContext())) {
                return film.getSdVideoURL();
            } else {
                return film.getHdVideoURL();
            }
        } else { //es videoclase
            ABAVideoClass videoClass = currentUnit.getSectionVideoClass();

            if (Connectivity.isConnectedMobile(getApplicationContext())) {
                return videoClass.getSdVideoURL();
            } else {
                return videoClass.getHdVideoURL();
            }
        }
    }

    private String getSubtitleURL(String selectedLang) {
        if (sectionType == kABAFilm) {
            ABAFilm film = currentUnit.getSectionFilm();

            if (selectedLang.equals(getResources().getString(R.string.subsEnglishSelected))) {
                return film.getEnglishSubtitles();
            } else if (selectedLang.equals(getResources().getString(R.string.subsTranslatedSelected))) {
                return film.getTranslatedSubtitles();
            } else {
                return "";
            }
        } else {
            ABAVideoClass videoClass = currentUnit.getSectionVideoClass();

            if (selectedLang.equals(getResources().getString(R.string.subsEnglishSelected))) {
                return videoClass.getEnglishSubtitles();
            } else if (selectedLang.equals(getResources().getString(R.string.subsTranslatedSelected))) {
                return videoClass.getTranslatedSubtitles();
            } else {
                return "";
            }
        }
    }


    // ================================================================================
    // Private methods - Tracking
    // ================================================================================

    private void trackSection() {
        ABAUser user = UserController.getInstance().getCurrentUser(getRealm());
        String sectionId;
        if (sectionType == kABAFilm) {
            CooladataNavigationTrackingManager.trackEnteredSection(user.getUserId(), currentUnit.getLevel().getIdLevel(), currentUnit.getIdUnit(), ABAFilm);
            Crashlytics.log(Log.INFO, "Section", "User opens Section ABAFilm");
            sectionId = ABAFilm.toString();
        } else {
            CooladataNavigationTrackingManager.trackEnteredSection(user.getUserId(), currentUnit.getLevel().getIdLevel(), currentUnit.getIdUnit(), ABAVideoClass);
            Crashlytics.log(Log.INFO, "Section", "User opens Section Videoclass");
            sectionId = ABAVideoClass.toString();
        }
        tracker.trackEnteredSection(currentUnit.getLevel().getIdLevel(), currentUnit.getIdUnit(), sectionId);

    }

    @Override
    public void goToNext(boolean result) {
        @Section int section;
        if (result) {
            switch (sectionType) {
                case kABAFilm:
                    section = SECTION_FILM;
                    break;
                case kVideoClase:
                    section = SECTION_VIDEO_CLASS;
                    break;
                default:
                    throw new UnsupportedOperationException();
            }
            startActivity(SectionHelper.getNextSectionIntent(this, Integer.parseInt(currentUnit.getIdUnit()), section));
            finish();
        } else {
            finishSection(true);
        }
    }
}