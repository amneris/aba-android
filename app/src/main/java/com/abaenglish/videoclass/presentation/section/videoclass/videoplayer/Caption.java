package com.abaenglish.videoclass.presentation.section.videoclass.videoplayer;

public class Caption {
	
	public Style style;
	public Region region;
	
	public Time start;
	public Time end;
	
	public String content="";

}
