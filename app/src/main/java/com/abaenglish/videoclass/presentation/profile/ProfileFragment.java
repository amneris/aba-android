package com.abaenglish.videoclass.presentation.profile;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.abaenglish.videoclass.ABAApplication;
import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.analytics.helpers.GenericTrackingInterface;
import com.abaenglish.videoclass.analytics.legacy.LegacyTrackingManager;
import com.abaenglish.videoclass.data.network.retrofit.clients.ABAMomentBadgeClient;
import com.abaenglish.videoclass.data.persistence.ABAAPIError;
import com.abaenglish.videoclass.data.persistence.ABAUser;
import com.abaenglish.videoclass.domain.content.DataController;
import com.abaenglish.videoclass.domain.content.PlanController;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.abaenglish.videoclass.presentation.base.ABAMasterActivity;
import com.abaenglish.videoclass.presentation.base.ABAMasterFragment;
import com.abaenglish.videoclass.presentation.base.custom.ABAButton;
import com.abaenglish.videoclass.presentation.base.custom.ABAProgressDialog;
import com.abaenglish.videoclass.presentation.base.custom.ABATextView;
import com.abaenglish.videoclass.presentation.plan.PlansActivity;
import com.abaenglish.videoclass.presentation.shell.MenuActivity;
import com.android.vending.billing.IInAppBillingService;
import com.bzutils.images.RoundedImageView;
import com.crashlytics.android.Crashlytics;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.text.SimpleDateFormat;

/**
 * Created by madalin on 16/06/15.
 */
public class ProfileFragment extends ABAMasterFragment implements View.OnClickListener {

    private ABATextView changePassword;
    private ABATextView perfilName;
    private ABATextView perfilEmail;
    private ABATextView perfilLanguage;
    private ABATextView perfilPremium;
    private ABATextView expirationDate;
    private ABATextView restorePurchases;
    private RelativeLayout perfilTermsButton;
    private RelativeLayout perfilPolicyButton;
    private ABATextView perfilTermsDescription;
    private ABATextView perfilPolicyDescription;
    private ImageView termsArrow;
    private ImageView policyArrow;
    private LinearLayout switchButton;
    private ABATextView switchYes;
    private ABATextView switchNo;
    private boolean switchState = false;
    private LinearLayout noPremiumView;
    private LinearLayout logOutButton;
    private ABAButton premiumButton;
    private ABAUser currentUser;
    private RoundedImageView perfilAvatar;

    @Override
    protected int onResLayout() {
        return R.layout.fragment_profile;
    }

    @Override
    protected void onConfigView() {
        changePassword = $(R.id.changePassword);
        perfilName = $(R.id.perfilName);
        perfilEmail = $(R.id.perfilEmail);
        perfilLanguage = $(R.id.perfilLanguage);
        perfilPremium = $(R.id.perfilIsPremium);
        expirationDate = $(R.id.expirationDate);
        perfilPolicyButton = $(R.id.profilePolicy);
        perfilTermsButton = $(R.id.profileTems);
        perfilPolicyDescription = $(R.id.profilePolicyDescription);
        perfilTermsDescription = $(R.id.profileTemsDescription);
        termsArrow = $(R.id.termsArrow);
        policyArrow = $(R.id.policyArrow);
        restorePurchases = $(R.id.restorePurchases);
        switchButton = $(R.id.switchButton);
        switchNo = $(R.id.switchNo);
        switchYes = $(R.id.switchYes);
        noPremiumView = $(R.id.premium_view);
        logOutButton = $(R.id.logOutButton);
        premiumButton = $(R.id.premiumbutton);
        perfilAvatar = $(R.id.perfilAvatar);

        perfilPolicyButton.setOnClickListener(this);
        perfilTermsButton.setOnClickListener(this);
        switchButton.setOnClickListener(this);
        logOutButton.setOnClickListener(this);
        premiumButton.setOnClickListener(this);
        changePassword.setOnClickListener(this);
        restorePurchases.setOnClickListener(this);

        Crashlytics.log(Log.INFO, "Profile", "User opens Profile Page");

        currentUser = DataStore.getInstance().getUserController().getCurrentUser(getRealm());
        setSwitchYes();

        if (currentUser.getUrlImage() != null && !currentUser.getUrlImage().equals("")) {
            ImageLoader.getInstance().loadImage(currentUser.getUrlImage(), new SimpleImageLoadingListener() {
                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    if (isAdded() && getActivity() != null) {
                        BitmapDrawable bitmapDrawable = new BitmapDrawable(getResources(), loadedImage);
                        perfilAvatar.setImageDrawable(bitmapDrawable);
                    }
                }
            });
        }
        perfilName.setText(currentUser.getName() + " " + currentUser.getSurnames());
        perfilEmail.setText(currentUser.getEmail());
        setUserLang(currentUser.getUserLang());

        if (DataStore.getInstance().getUserController().isUserPremium()) {
            perfilPremium.setText(getResources().getString(R.string.typeStudentkey) + " : " + "Premium");
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            String subscriptionEnd = sdf.format(currentUser.getExpirationDate());
            expirationDate.setVisibility(View.VISIBLE);
            expirationDate.setText(getResources().getString(R.string.profileRegister2Key) + ": " + subscriptionEnd);
            noPremiumView.setVisibility(View.GONE);
        } else {
            perfilPremium.setText(getResources().getString(R.string.typeStudentkey) + " : " + "Free");
            noPremiumView.setVisibility(View.VISIBLE);
            expirationDate.setVisibility(View.GONE);
        }


        String termDescriptionString = getString(R.string.profileTemsDescriptionKey);
        String policyDescriptionString = getResources().getString(R.string.profilePolicyDescriptionKey);

        String url = "ABAEnglish.com";
        String email = "info@abaenglish.com";
        termDescriptionString = termDescriptionString.replace
                (url, "<a href=\"" + "http://www.abaenglish.com" + "\">" + url + "</a>");
        termDescriptionString = termDescriptionString.replace
                (email, "<a href=\"" + "mailto:" + email + "\">" + email + "</a>");
        termDescriptionString = termDescriptionString.replace("\n", "<br><br>");
        policyDescriptionString = policyDescriptionString.replace("\n", "<br><br>");
        perfilPolicyDescription.setText(Html.fromHtml(policyDescriptionString));
        perfilTermsDescription.setMovementMethod(LinkMovementMethod.getInstance());
        perfilTermsDescription.setText(Html.fromHtml(termDescriptionString));


        restorePurchases.setText(getResources().getString(R.string.profileShoppingRestoreKey).toUpperCase());
        changePassword.setText(getResources().getString(R.string.profileChangePasswordKey).toUpperCase());
    }

    @Override
    protected void onConfigToolbar(ABATextView toolbarTitle, ABATextView toolbarSubTitle, ImageButton toolbarButton) {
        toolbarTitle.setText(getResources().getString(R.string.menuKey));
        toolbarSubTitle.setText(getResources().getString(R.string.yourProfileKey));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.profilePolicy:
                if (perfilPolicyDescription.getVisibility() == View.GONE) {
                    perfilPolicyDescription.setVisibility(View.VISIBLE);
                    policyArrow.animate().rotationBy(0).rotation(-180).setDuration(300).start();
                } else {
                    closePolicyTAB();
                }
                closeTermsTAB();
                break;
            case R.id.profileTems:
                if (perfilTermsDescription.getVisibility() == View.GONE) {
                    perfilTermsDescription.setVisibility(View.VISIBLE);
                    termsArrow.animate().rotationBy(0).rotation(-180).setDuration(300).start();
                } else {
                    closeTermsTAB();
                }
                closePolicyTAB();
                break;
            case R.id.switchButton:
                if (switchState) {
                    LegacyTrackingManager.getInstance().trackEvent(getRealm(), "account_push_ok", "account", "selecciona recibir notificaciones");
                    setSwitchYes();
                    switchState = false;
                } else {
                    LegacyTrackingManager.getInstance().trackEvent(getRealm(), "account_push_ko", "account", "selecciona no recibir notificaciones");
                    setSwitchNo();
                    switchState = true;
                }
                break;
            case R.id.premiumbutton:
                getABAActivity().trackPricingScreenWithOrigin(GenericTrackingInterface.PlansViewControllerOriginType.kPlansViewControllerOriginBanner);
                getActivity().startActivity(new Intent(getActivity(), PlansActivity.class));
                getActivity().overridePendingTransition(R.anim.slide_enter_unitanimation1, R.anim.slide_enter_unitanimation2);
                break;
            case R.id.logOutButton:
                Crashlytics.log(Log.INFO, "Logout", "User clicked in logout button - legacy profile");
                mSessionService.logout(getActivity(), new DataController.ABALogoutCallback() {
                    @Override
                    public void onLogoutFinished() {
                        // Resetting token so that oauth can be init again
                        tokenAccessor.reset();

                        if (getActivity() != null) {
                            ABAMomentBadgeClient.reset(getActivity());
                            if (!getActivity().isFinishing()) {
                                MenuActivity.resetFirstSessionMenuPerformance(ABAApplication.get());
                                getABAActivity().openTutorial();
                            }
                        }
                    }
                });
                break;
            case R.id.changePassword:
                Intent intent2 = new Intent(getContext(), PerfilChangePasswordActivity.class);
                getActivity().startActivity(intent2);
                getActivity().overridePendingTransition(R.anim.slide_enter_unitanimation1, R.anim.slide_enter_unitanimation2);
                break;
            case R.id.restorePurchases:
                Crashlytics.log(Log.INFO, "Restore Purchases", "User clicks Restore Purchases button");
                getABAActivity().showProgressDialog(ABAProgressDialog.SimpleDialog);
                DataStore.getInstance().getPlanController(getABAActivity().tracker).getBillingServices(getContext(), new DataController.ABAAPICallback<IInAppBillingService>() {
                    @Override
                    public void onSuccess(IInAppBillingService response) {
                        DataStore.getInstance().getPlanController(getABAActivity().tracker).prepareToRestorePurchases(currentUser, getRealm(), getABAActivity(), response, new PlanController.ABAPlanCallback() {
                            @Override
                            public void onResult(PlanController.SubscriptionResult type) {
                                getABAActivity().dismissProgressDialog();
                                getABAActivity().handlePurchaseResult(type);
                            }
                        });
                    }

                    @Override
                    public void onError(ABAAPIError error) {
                        ((ABAMasterActivity) getContext()).showABAErrorNotification(getString(R.string.errorFetchingSubscriptions));
                        getABAActivity().dismissProgressDialog();
                    }
                });

                break;
        }
    }

    private void closeTermsTAB() {
        perfilTermsDescription.setVisibility(View.GONE);
        termsArrow.animate().rotationBy(-180).rotation(0).setDuration(300).start();
    }

    private void closePolicyTAB() {
        perfilPolicyDescription.setVisibility(View.GONE);
        policyArrow.animate().rotationBy(-180).rotation(0).setDuration(300).start();
    }

    private void setSwitchYes() {
        switchYes.setTextColor(ContextCompat.getColor(getContext(), R.color.abaWhite));
        switchYes.setBackgroundResource(R.drawable.evaluation_lightblue_button);
        switchNo.setTextColor(ContextCompat.getColor(getContext(), R.color.abaGrey));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            switchNo.setBackground(null);
        } else {
            switchNo.setBackgroundDrawable(null);
        }
    }

    private void setSwitchNo() {
        switchNo.setTextColor(ContextCompat.getColor(getContext(), R.color.abaWhite));
        switchNo.setBackgroundResource(R.drawable.evaluation_lightblue_button);
        switchYes.setTextColor(ContextCompat.getColor(getContext(), R.color.abaGrey));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            switchYes.setBackground(null);
        } else {
            switchYes.setBackgroundDrawable(null);
        }
    }

    private void setUserLang(String language) {
        if (language.equals("es")) {
            perfilLanguage.setText(getResources().getString(R.string.languageSpainKey));
        } else if (language.equals("en")) {
            perfilLanguage.setText(getResources().getString(R.string.languageEnglishKey));
        } else if (language.equals("it")) {
            perfilLanguage.setText(getResources().getString(R.string.languageItalianKey));
        } else if (language.equals("de")) {
            perfilLanguage.setText(getResources().getString(R.string.languageDeutchKey));
        } else if (language.equals("fr")) {
            perfilLanguage.setText(getResources().getString(R.string.languageFrenchKey));
        } else if (language.equals("ru")) {
            perfilLanguage.setText(getResources().getString(R.string.languageRussianKey));
        } else if (language.equals("pt")) {
            perfilLanguage.setText(getResources().getString(R.string.languagePortugueseKey));
        }
    }
}
