package com.abaenglish.videoclass.presentation.base.custom;

import android.app.Activity;
import android.os.AsyncTask;

/**
 * Created by vmadalin on 20/1/16.
 */
public class ABAProgressTask extends AsyncTask<Void, Void, Void> {

    private Activity mActivity;
    private int mTypeProgressDialog;
    private ABAProgressDialog progressDialog;


    public ABAProgressTask(Activity mActivity, int mTypeProgressDialog) {
        this.mActivity = mActivity;
        this.mTypeProgressDialog = mTypeProgressDialog;
    }

    public void cancelProgressTask() {
        onCancelled();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ABAProgressDialog(mActivity, mTypeProgressDialog);
        progressDialog.show();
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        if(progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    protected Void doInBackground(Void... params) {
        return null;
    }
}
