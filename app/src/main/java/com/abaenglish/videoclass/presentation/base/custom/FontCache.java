package com.abaenglish.videoclass.presentation.base.custom;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by Jesus Espejo using mbp13jesus on 22/12/16.
 * Class to handle all the font loads.
 */

public class FontCache {

    private Context applicationContext;

    public enum ABATypeface {
        sans100, sans300, sans500, sans700, sans900,
        slab300, slab500, slab500i, slab700, slab900,
        rBold, rRegular, robotoSlabLight
    }

    private Typeface TypeSans100, TypeSans300, TypeSans500, TypeSans700, TypeSans900,
            TypeSlab300, TypeSlab500, TypeSlab500i, TypeSlab700, TypeSlab900,
            TypeRalewayBold, TypeRalewayRegular, TypeRobotoSlabLight;

    public FontCache(Context context) {
        super();
        this.applicationContext = context.getApplicationContext();
    }

    public Typeface getTypeface(ABATypeface type) {
        switch (type) {
            case sans100:
                return getMuseoSans100();
            case sans300:
                return getMuseoSans300();
            case sans500:
                return getMuseoSans500();
            case sans700:
                return getMuseoSans700();
            case sans900:
                return getMuseoSans900();
            case slab300:
                return getMuseoSlab300();
            case slab500:
                return getMuseoSlab500();
            case slab500i:
                return getMuseoSlab500italic();
            case slab700:
                return getMuseoSlab700();
            case slab900:
                return getMuseoSlab900();
            case rBold:
                return getRalewayBold();
            case rRegular:
                return getRalewayRegular();
            case robotoSlabLight:
                return getTypeRobotoSlabLight();
        }
        return getMuseoSans100();
    }

    private Typeface getMuseoSans100() {
        if (TypeSans100 == null) {
            TypeSans100 = Typeface.createFromAsset(applicationContext.getAssets(), "museosans100.ttf");
        }
        return TypeSans100;
    }

    private Typeface getMuseoSans300() {
        if (TypeSans300 == null) {
            TypeSans300 = Typeface.createFromAsset(applicationContext.getAssets(), "museosans300.ttf");
        }
        return TypeSans300;
    }

    private Typeface getMuseoSans500() {
        if (TypeSans500 == null) {
            TypeSans500 = Typeface.createFromAsset(applicationContext.getAssets(), "museosans500.ttf");
        }
        return TypeSans500;
    }


    private Typeface getMuseoSlab300() {
        if (TypeSlab300 == null) {
            TypeSlab300 = Typeface.createFromAsset(applicationContext.getAssets(), "museoslab300.ttf");
        }
        return TypeSlab300;
    }

    private Typeface getMuseoSlab500() {
        if (TypeSlab500 == null) {
            TypeSlab500 = Typeface.createFromAsset(applicationContext.getAssets(), "museoslab500.ttf");
        }
        return TypeSlab500;
    }

    private Typeface getMuseoSlab500italic() {
        if (TypeSlab500i == null) {
            TypeSlab500i = Typeface.createFromAsset(applicationContext.getAssets(), "museoslab500italic.ttf");
        }
        return TypeSlab500i;
    }

    private Typeface getMuseoSans700() {
        if (TypeSans700 == null) {
            TypeSans700 = Typeface.createFromAsset(applicationContext.getAssets(), "museosans700.ttf");
        }
        return TypeSans700;
    }

    private Typeface getMuseoSlab700() {
        if (TypeSlab700 == null) {
            TypeSlab700 = Typeface.createFromAsset(applicationContext.getAssets(), "museoslab700.ttf");
        }
        return TypeSlab700;
    }

    private Typeface getMuseoSans900() {
        if (TypeSans900 == null) {
            TypeSans900 = Typeface.createFromAsset(applicationContext.getAssets(), "museosans900.ttf");
        }
        return TypeSans900;
    }

    private Typeface getMuseoSlab900() {
        if (TypeSlab900 == null) {
            TypeSlab900 = Typeface.createFromAsset(applicationContext.getAssets(), "museoslab900.ttf");
        }
        return TypeSlab900;
    }

    private Typeface getRalewayBold() {
        if (TypeRalewayBold == null) {
            TypeRalewayBold = Typeface.createFromAsset(applicationContext.getAssets(), "Raleway-Bold.ttf");
        }
        return TypeRalewayBold;
    }

    private Typeface getRalewayRegular() {
        if (TypeRalewayRegular == null) {
            TypeRalewayRegular = Typeface.createFromAsset(applicationContext.getAssets(), "Raleway-Regular.ttf");
        }
        return TypeRalewayRegular;
    }

    public Typeface getTypeRobotoSlabLight() {
        if (TypeRobotoSlabLight == null) {
            TypeRobotoSlabLight = Typeface.createFromAsset(applicationContext.getAssets(), "RobotoSlab-Light.ttf");
        }
        return TypeRobotoSlabLight;
    }
}
