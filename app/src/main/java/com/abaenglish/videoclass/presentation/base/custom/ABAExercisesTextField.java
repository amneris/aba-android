package com.abaenglish.videoclass.presentation.base.custom;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;

import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.data.persistence.ABAExercisePhraseItem;
import com.abaenglish.videoclass.presentation.section.exercise.ABAExercisesActivity;

/**
 * Created by Marc Güell Segarra on 10/6/15.
 */
public class ABAExercisesTextField extends EditText {

    public ABAExercisesTextField(Context context) {
        super(context);
        init();
    }

    public ABAExercisesTextField(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ABAExercisesTextField(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ABAExercisesTextField(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        this.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                ((ABAExercisesActivity) getContext()).showCheckButton();
            }
        });
    }

    public void configTypeface() {
        if (getTag() == null)
            setTag(R.integer.typefaceSans100);
    }

    @Override
    public boolean onKeyPreIme(int keyCode, KeyEvent event) {
        if (event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            ((ABAExercisesActivity) getContext()).hideCheckButton();
            return false;

        }
        return super.dispatchKeyEvent(event);
    }

    private ABAExercisePhraseItem correctAnswer;

    public ABAExercisePhraseItem getCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(ABAExercisePhraseItem correctAnswer) {
        this.correctAnswer = correctAnswer;
    }
}
