package com.abaenglish.videoclass.presentation.plan;

import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.abaenglish.videoclass.ABAApplication;
import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.analytics.cooladata.navigation.CooladataNavigationTrackingManager;
import com.abaenglish.videoclass.analytics.modern.FabricTrackingManager;
import com.abaenglish.videoclass.analytics.modern.MixpanelTrackingManager;
import com.abaenglish.videoclass.data.persistence.ABAAPIError;
import com.abaenglish.videoclass.data.persistence.ABAPlan;
import com.abaenglish.videoclass.data.persistence.ABAUser;
import com.abaenglish.videoclass.domain.content.DataController;
import com.abaenglish.videoclass.domain.content.PlanController;
import com.abaenglish.videoclass.domain.content.UserController;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.abaenglish.videoclass.presentation.base.ABAMasterActivity;
import com.abaenglish.videoclass.presentation.base.ABAMasterFragment;
import com.abaenglish.videoclass.presentation.base.custom.ABAProgressDialog;
import com.abaenglish.videoclass.presentation.base.custom.ABATextView;
import com.abaenglish.videoclass.presentation.base.custom.FontCache;
import com.android.vending.billing.IInAppBillingService;
import com.crashlytics.android.Crashlytics;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

/**
 * Created by Jesus Espejo using mbp13jesus on 15/03/17.
 * Using Android Annotations.
 */
@EFragment
public class ImprovedPlansFragment extends ABAMasterFragment {

    @ViewById
    TextView textViewGoPremium,
            textView1000Exercises_1,
            textView1000Exercises_2,
            textView288Videos_1,
            textView288Videos_2,
            textView6CertifiedLevels_1,
            textView6CertifiedLevels_2,
            textViewUnlimitedMessages_1,
            textViewUnlimitedMessages_2,
            textViewPlansAndPrices,
            textViewRecurringBilling;

    @ViewById
    LinearLayout linearLayoutPlans,
            linearLayoutMessages;

    @ViewById
    ViewGroup layoutBottom;

    @ViewById
    RelativeLayout mainLayout;

    protected Snackbar reloadSnackbar;

    // ================================================================================
    // ABAMasterFragment
    // ================================================================================

    @Override
    protected int onResLayout() {
        return R.layout.fragment_improved_plans;
    }

    @Override
    protected void onConfigView() {

    }

    @Override
    protected void onConfigToolbar(ABATextView toolbarTitle, ABATextView toolbarSubTitle, ImageButton toolbarButton) {
        toolbarTitle.setText(getString(R.string.chooseYourSubscriptionKey));
        toolbarSubTitle.setVisibility(View.GONE);

        if (getActivity() instanceof PlansActivity) {
            toolbarButton.setImageResource(R.mipmap.back);
            toolbarButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // finish activity
                    getActivity().finish();
                    getActivity().overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_exit_unitanimation2);
                }
            });
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (reloadSnackbar != null) {
            reloadSnackbar.dismiss();
        }
    }

    // ================================================================================
    // Android Annotations
    // ================================================================================

    @AfterViews
    void calledAfterViewInjection() {
        textViewGoPremium.setTypeface(fontCache.getTypeface(FontCache.ABATypeface.slab700));

        textView1000Exercises_1.setTypeface(fontCache.getTypeface(FontCache.ABATypeface.slab700));
        textView288Videos_1.setTypeface(fontCache.getTypeface(FontCache.ABATypeface.slab700));
        textView6CertifiedLevels_1.setTypeface(fontCache.getTypeface(FontCache.ABATypeface.slab700));
        textViewUnlimitedMessages_1.setTypeface(fontCache.getTypeface(FontCache.ABATypeface.slab700));

        textView1000Exercises_2.setTypeface(fontCache.getTypeface(FontCache.ABATypeface.sans500));
        textView288Videos_2.setTypeface(fontCache.getTypeface(FontCache.ABATypeface.sans500));
        textView6CertifiedLevels_2.setTypeface(fontCache.getTypeface(FontCache.ABATypeface.sans500));
        textViewUnlimitedMessages_2.setTypeface(fontCache.getTypeface(FontCache.ABATypeface.sans500));

        textViewPlansAndPrices.setTypeface(fontCache.getTypeface(FontCache.ABATypeface.slab700));

        loadPlans();
    }

    // ================================================================================
    // Private methods
    // ================================================================================

    private List<ABAPlan> getPlans(Realm realmToLoad) {
        List<ABAPlan> plans = PlanController.getPlanContent(realmToLoad);
        List<ABAPlan> activePlans = new ArrayList<>();
        if (!serverConfig.isSixMonthsTierActive()) {
            for (ABAPlan plan : plans) {
                if (plan.getDays() != 180) {
                    activePlans.add(plan);
                }
            }
            return activePlans;
        } else {
            return plans;
        }
    }

    private void loadPlans() {
        if (getActivity() != null) {

            layoutBottom.setVisibility(View.GONE);
            linearLayoutMessages.setVisibility(View.GONE);
            getABAActivity().showProgressDialog(ABAProgressDialog.SimpleDialog);

            PlanController planController = DataStore.getInstance().getPlanController(((ABAMasterActivity) getActivity()).tracker);
            planController.fetchPlanContent(getActivity(), DataStore.getInstance().getUserController().getCurrentUser(getRealm()), new PlanController.ABAPlanCallback() {

                @Override
                public void onResult(PlanController.SubscriptionResult type) {

                    if (type == PlanController.SubscriptionResult.SUBSCRIPTION_RESULT_OK && getActivity() != null) {

                        // Success
                        getABAActivity().dismissProgressDialog();

                        Realm realmToRead = Realm.getInstance(ABAApplication.get().getRealmConfiguration());
                        List<ABAPlan> listOfPlans = getPlans(realmToRead);

                        // Detecting whether the cell to show should be the regular or the collapsed one
                        boolean shouldShowCompactCell = false;
                        for (ABAPlan plan : listOfPlans) {
                            float pricePerMonth = PlanController.getMonthlyPrice(plan.getDiscountPrice(), plan.getDays());
                            if (pricePerMonth > 99.999) {
                                shouldShowCompactCell = true;
                                break;
                            }
                        }

                        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                        param.weight = (float) 0.33;

                        for (ABAPlan plan : listOfPlans) {

                            int indexOfPlan = listOfPlans.indexOf(plan);
                            AbstractPlanCell cell;

                            if (shouldShowCompactCell) {
                                cell = PlanCellCompact_.build(getContext());
                            } else {
                                cell = PlanCell_.build(getContext());
                            }

                            cell.updateView(plan, fontCache, indexOfPlan);
                            cell.setPlanCellListener(listener);

                            // Last one should not have a separator
                            if (listOfPlans.size() - 1 != indexOfPlan) {
                                // API 15
                                if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                                    cell.setBackgroundDrawable(ContextCompat.getDrawable(getContext(), R.drawable.plan_cell_border));
                                }
                                else {
                                    // API 16 on
                                    cell.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.plan_cell_border));
                                }
                            }

                            // Adding plan to plan list
                            linearLayoutPlans.addView(cell, param);
                        }

                        realmToRead.close();

                        layoutBottom.setVisibility(View.VISIBLE);
                        linearLayoutMessages.setVisibility(View.VISIBLE);
                        Crashlytics.log(Log.INFO, "Plans", "Plans have been loaded");

                    } else {
                        // Error handling
                        if (getActivity() != null) {
                            getABAActivity().dismissProgressDialog();
                            reloadSnackbar = Snackbar.make(mainLayout, R.string.errorRegister, Snackbar.LENGTH_INDEFINITE)
                                    .setAction(R.string.reloadPlansAfterError, new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            reloadSnackbar = null;
                                            loadPlans();
                                        }
                                    });
                            reloadSnackbar.show();

                            Crashlytics.log(Log.INFO, "Plans", "Error loading plans");
                        }
                    }
                }
            });
        }
    }

    private void trackPurchaseIntention(ABAPlan selectedPlan, String userId) {
        Crashlytics.log(Log.INFO, "Plans", "User selects plan " + selectedPlan.getPlanTitle());

        MixpanelTrackingManager.getSharedInstance(getContext()).trackPricingSelected(selectedPlan);
        CooladataNavigationTrackingManager.trackSubscriptionSelected(userId, selectedPlan.getDays());
        FabricTrackingManager.trackPricingSelected(selectedPlan);
        ((ABAMasterActivity) getActivity()).tracker.trackSelectedPlan(selectedPlan.getDiscountPrice(), selectedPlan.getCurrency(), selectedPlan.getDiscountIdentifier(), String.valueOf(selectedPlan.getDays()));
    }

    // ================================================================================
    // Private classes
    // ================================================================================

    private PlanCellClickListener listener = new PlanCellClickListener() {

        @Override
        public void clickedItem(int index) {
            Realm realmToRead = Realm.getInstance(ABAApplication.get().getRealmConfiguration());
            List<ABAPlan> listOfPlans = getPlans(realmToRead);

            ABAPlan clickedPlan = listOfPlans.get(index);

            // Reading user
            final String identifier = clickedPlan.getDiscountIdentifier();
            ABAUser user = UserController.getInstance().getCurrentUser(realmToRead);

            // Sending 'purchase intention' to analytics
            trackPurchaseIntention(clickedPlan, user.getUserId());

            DataStore.getInstance().getPlanController(tracker).getBillingServices(getContext(), new DataController.ABAAPICallback<IInAppBillingService>() {

                @Override
                public void onSuccess(IInAppBillingService response) {
                    boolean purchaseAttemptSuccessful = DataStore.getInstance().getPlanController(tracker).subscribeToPlan(getContext(), response, identifier);
                    if (!purchaseAttemptSuccessful) {
                        ((ABAMasterActivity) getActivity()).showABAErrorNotification(getContext().getString(R.string.errorRegister));
                    }
                }

                @Override
                public void onError(ABAAPIError error) {
                    ((ABAMasterActivity) getActivity()).showABAErrorNotification(getContext().getString(R.string.errorRegister));
                }
            });

            realmToRead.close();
        }
    };
}
