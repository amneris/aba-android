package com.abaenglish.videoclass.presentation.abaMoment.customViews;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;

import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.presentation.abaMoment.ABAMomentActivity;

/**
 * Created by julien on 15/02/2017.
 * Custom view to create the circles enclosing the word or pictures in the ABAMoment.
 * See {@link ABAMomentActivity}
 */

public class CircleABAMomentView extends View {

    private Paint paint;
    private Paint dashPaint;
    private Paint backgroundPaint;
    private Paint textPaint;

    private String label = "";
    private Boolean isQuestion;
    private Boolean isCorrect = false;
    int answerNumber;

    int lineWidth = 5;

    final static int colorCircleBackground = R.color.colorABAMomentBackground;
    final static int colorCorrectAnswer = R.color.colorABAMomentDarkBlue;
    final static int colorWordBorder = R.color.colorABAMomentWordBorder;
    final static int colorQuestionBorder = R.color.colorABAMomentBlue;
    final static int colorScreenBackground = R.color.abaWhite;


    public CircleABAMomentView(Context context) {
        super(context);
        init(null);
    }

    public CircleABAMomentView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public CircleABAMomentView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    private void init(AttributeSet attrs) {

        TypedArray ta = getContext().obtainStyledAttributes(attrs, R.styleable.ABAMoment, 0, 0);
        try {
            isQuestion = ta.getBoolean(R.styleable.ABAMoment_question, false);
            answerNumber = ta.getInteger(R.styleable.ABAMoment_answerNumber, 0);
        } finally {
            ta.recycle();
        }

        paint = new Paint();
        dashPaint = new Paint();
        backgroundPaint = new Paint();
        textPaint = new Paint();

        paint.setStyle(Paint.Style.STROKE);
        paint.setFlags(Paint.ANTI_ALIAS_FLAG);

        dashPaint.setStyle(Paint.Style.STROKE);
        dashPaint.setFlags(Paint.ANTI_ALIAS_FLAG);

        //dashed border for question
        if (isQuestion()) {
            paint.setColor(ContextCompat.getColor(getContext(), colorQuestionBorder));
            dashPaint.setColor(ContextCompat.getColor(getContext(), colorScreenBackground));
        } else {
            paint.setColor(ContextCompat.getColor(getContext(), colorWordBorder));
        }

        //same background for question and answers
        backgroundPaint.setColor(ContextCompat.getColor(getContext(), colorCircleBackground));

        textPaint.setColor(ContextCompat.getColor(getContext(), colorCorrectAnswer));
        textPaint.setTextSize((int) (23 * getResources().getDisplayMetrics().density));
        textPaint.setAntiAlias(true);
        textPaint.setTextAlign(Paint.Align.CENTER);

        lineWidth = (int) (lineWidth * getResources().getDisplayMetrics().density);
    }


    @SuppressLint("DrawAllocation")
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int paddingLeft = getPaddingLeft();
        int paddingTop = getPaddingTop();
        int paddingRight = getPaddingRight();
        int paddingBottom = getPaddingBottom();

        int contentWidth = getWidth() - paddingLeft - paddingRight;
        int contentHeight = getHeight() - paddingTop - paddingBottom;

        int radius = (Math.min(contentHeight, contentWidth) / 2);
        int dash = ((int) (radius * 2 * 3.14) / 35);

        //dash circle for question
        if (isQuestion()) {
            if (isCorrect) {
                paint.setPathEffect(null);
            } else {
                paint.setPathEffect(new DashPathEffect(new float[]{dash, dash}, 0));
            }
        }

        paint.setStrokeWidth(lineWidth);
        dashPaint.setStrokeWidth(lineWidth);
        radius -= lineWidth / 2;

        //drawing a colorScreenBackground when a word is displayed
        if (!label.isEmpty()) {
            canvas.drawCircle(contentWidth / 2, contentHeight / 2, radius, backgroundPaint);
        }

        //drawing the circle around the image or the label
        if (isQuestion()) {
            canvas.drawCircle(contentWidth / 2, contentHeight / 2, radius, dashPaint);
            canvas.drawCircle(contentWidth / 2, contentHeight / 2, radius, paint);
        } else {
            canvas.drawCircle(contentWidth / 2, contentHeight / 2, radius, paint);
        }


        //drawing the text
        int yPos = (int) ((contentHeight / 2) - ((textPaint.descent() + textPaint.ascent()) / 2));
        canvas.drawText(label, contentWidth / 2, yPos, textPaint);

    }

    public void setText(String newLabel) {
        label = newLabel;
        invalidate();
    }

    public void setColors(boolean isCorrect) {
        this.isCorrect = isCorrect;
        if (isCorrect) {
            paint.setColor(ContextCompat.getColor(getContext(), colorCorrectAnswer));
            if (!isQuestion()) {
                backgroundPaint.setColor(ContextCompat.getColor(getContext(), colorCorrectAnswer));
                textPaint.setColor(ContextCompat.getColor(getContext(), colorScreenBackground));
            } else {
                dashPaint.setColor(ContextCompat.getColor(getContext(), colorCorrectAnswer));
            }
        } else {
            backgroundPaint.setColor(ContextCompat.getColor(getContext(), colorCircleBackground));
            if (!isQuestion()) {
                paint.setColor(ContextCompat.getColor(getContext(), colorWordBorder));
                textPaint.setColor(ContextCompat.getColor(getContext(), colorCorrectAnswer));
            } else {
                paint.setColor(ContextCompat.getColor(getContext(), colorQuestionBorder));
                dashPaint.setColor(ContextCompat.getColor(getContext(), colorScreenBackground));
            }
        }
        invalidate();
    }

    public Boolean isQuestion() {
        return isQuestion;
    }

    public int getAnswerNumber() {
        return answerNumber;
    }

}
