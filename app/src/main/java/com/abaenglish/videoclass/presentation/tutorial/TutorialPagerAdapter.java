package com.abaenglish.videoclass.presentation.tutorial;

import android.content.Context;
import android.content.res.Resources;
import android.support.v4.view.PagerAdapter;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.data.network.APIManager;
import com.abaenglish.videoclass.presentation.base.custom.ABATextView;
import com.bzutils.BZScreenHelper;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by vmadalin on 14/3/16.
 */
public class TutorialPagerAdapter extends PagerAdapter {

    private List<TutorialItem> items;
    private LayoutInflater mLayoutInflater;
    private Context mContext;
    private Resources mResources;

    public TutorialPagerAdapter(Context context) {
        this.mContext = context;
        this.mLayoutInflater = LayoutInflater.from(context);
        this.mResources = mContext.getResources();
        items = createTutorialItems(context);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public View instantiateItem(final ViewGroup container, final int position) {
        View layout = mLayoutInflater.inflate(R.layout.tutorial_ab_test_item_layout, null);
        RelativeLayout descContent = (RelativeLayout) layout.findViewById(R.id.tutorialDescriptionContent);
        ImageView background = (ImageView) layout.findViewById(R.id.tutorialItemBackground);
        ABATextView title = (ABATextView) layout.findViewById(R.id.tutorialItemTitle);
        ABATextView title2 = (ABATextView) layout.findViewById(R.id.tutorialItemTitle2);
        ABATextView desc = (ABATextView) layout.findViewById(R.id.tutorialItemDesc);

        TutorialItem currentItem = items.get(position);
        background.setImageResource(currentItem.getBackground());
        descContent.setTag(position);

        setTextForTutorial(currentItem, title, title2, desc);

        if (position == 0) { // First page
            configTextForFirstPage(title, title2);
        } else { //Medium pages
            configTextForMediumPages(position, title, title2);
        }

        container.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    private void configTextForFirstPage(ABATextView title, ABATextView title2) {
        if (Locale.getDefault().getLanguage().equals("ru")) {
            if (!APIManager.isTabletSize(mContext)) {
                title.setPadding((int) mResources.getDimension(R.dimen.padding10), 0, 0, 0);
                title2.setPadding((int) mResources.getDimension(R.dimen.padding10), 0, 0, 0);
            }
            title.setTextSize(BZScreenHelper.dpFromPx(mResources.getDimension(R.dimen.tutorialTitleLastPage), mContext));
            title2.setTextSize(BZScreenHelper.dpFromPx(mResources.getDimension(R.dimen.tutorialTitleLastPage), mContext));
        } else {
            title.setTextSize(BZScreenHelper.dpFromPx(mResources.getDimension(R.dimen.tutorialTitleTextBigSize), mContext));
            title2.setTextSize(BZScreenHelper.dpFromPx(mResources.getDimension(R.dimen.tutorialTitleTextBigSize), mContext));
        }

        title.setABATag(mResources.getInteger(R.integer.typefaceSlab500));
        title2.setABATag(mResources.getInteger(R.integer.typefaceSlab300));
    }

    private void configTextForMediumPages(int position, ABATextView title, ABATextView title2) {
        if (position == items.size() - 2) {
            title.setABATag(mResources.getInteger(R.integer.typefaceSlab500));
            title2.setABATag(mResources.getInteger(R.integer.typefaceSlab500));
        } else {
            title.setABATag(mResources.getInteger(R.integer.typefaceSlab300));
            title2.setABATag(mResources.getInteger(R.integer.typefaceSlab500));
        }

        title.setTextSize(BZScreenHelper.dpFromPx(mResources.getDimension(R.dimen.tutorialTitleTextBigSize), mContext));
        if (title2.getText().length() > 14 && !APIManager.isTablet(mContext)) {
            title2.setTextSize(BZScreenHelper.dpFromPx(mResources.getDimension(R.dimen.tutorialTitleLastPage), mContext));
        } else {
            title2.setTextSize(BZScreenHelper.dpFromPx(mResources.getDimension(R.dimen.tutorialTitleTextBigSize), mContext));
        }
    }

    private void setTextForTutorial(TutorialItem currentItem, ABATextView title, ABATextView title2, ABATextView desc) {
        title.setText(Html.fromHtml(currentItem.getTitle()));
        title2.setText(Html.fromHtml(currentItem.getTitle2()));
        if (currentItem.getSubtitle() != null) {
            desc.setText(currentItem.getSubtitle());
            desc.setVisibility(View.VISIBLE);
        } else {
            desc.setVisibility(View.GONE);
        }
    }

    public List<TutorialItem> createTutorialItems(Context context) {
        List<TutorialItem> items = new ArrayList<>();

        items.add(new TutorialItem.Builder()
                .withBackground(R.drawable.tutorial_screen_1)
                .withTitle(context.getString(R.string.startTitle1Key) + "<br>" + context.getString(R.string.startTitle2Key))
                .withTitle2("<font color='#07BCE6'>" + context.getString(R.string.startTitle3Key) + "</font>")
                .build());

        items.add(new TutorialItem.Builder()
                .withBackground(R.drawable.tutorial_screen_2)
                .withTitle(context.getString(R.string.tuto1Title1Key) + "<br>" + context.getString(R.string.tuto1Title2Key))
                .withTitle2("<font color='#07BCE6'>" + context.getString(R.string.tuto1Title3Key) + "</font>")
                .withSubtitle(context.getString(R.string.tuto1DescKey))
                .build());

        items.add(new TutorialItem.Builder()
                .withBackground(R.drawable.tutorial_screen_3)
                .withTitle(context.getString(R.string.tuto2Title1Key) + "<br>" + context.getString(R.string.tuto2Title2Key))
                .withTitle2("<font color='#07BCE6'>" + context.getString(R.string.tuto2Title3Key) + "</font>")
                .withSubtitle(context.getString(R.string.tuto2DescKey))
                .build());

        items.add(new TutorialItem.Builder()
                .withBackground(R.drawable.tutorial_screen_4)
                .withTitle("<font color='#07BCE6'>" + context.getString(R.string.tuto3Title1Key) + "</font><br>" + context.getString(R.string.tuto3Title2Key))
                .withTitle2("<font color='#07BCE6'>" + context.getString(R.string.tuto3Title3Key) + "</font>")
                .withSubtitle(context.getString(R.string.tuto3DescKey))
                .build());

        return items;
    }
}