package com.abaenglish.videoclass.presentation.section.assessment.result;

import com.abaenglish.videoclass.analytics.TrackerContract;
import com.abaenglish.videoclass.domain.content.EvaluationController;
import com.abaenglish.videoclass.domain.content.LevelUnitController;
import com.abaenglish.videoclass.domain.content.UserController;
import com.abaenglish.videoclass.presentation.section.assessment.ActivityScope;
import com.abaenglish.videoclass.presentation.section.assessment.result.interactor.FinishEvaluationUseCase;
import com.abaenglish.videoclass.presentation.section.assessment.result.interactor.QualifyAssessmentUseCase;

import dagger.Module;
import dagger.Provides;
import io.realm.RealmConfiguration;

/**
 * Created by xabierlosada on 27/07/16.
 */

@Module
public class AssessmentResultModule {

    @Provides @ActivityScope
    public AssessmentResultPresenter providesResultPresenter(QualifyAssessmentUseCase qualifyAssessmentUseCase,
                                                             FinishEvaluationUseCase finishEvaluationUseCase,
                                                             TrackerContract.Tracker assessmentTracker) {
        return new AssessmentResultPresenter(qualifyAssessmentUseCase, finishEvaluationUseCase, assessmentTracker);
    }

    @Provides @ActivityScope
    public QualifyAssessmentUseCase providesQualifyAssessmentUseCase(
                                                            RealmConfiguration realmConfiguration,
                                                            LevelUnitController levelUnitController,
                                                            UserController userController) {
        return new QualifyAssessmentUseCase(realmConfiguration, userController, levelUnitController);
    }

    @Provides @ActivityScope
    public FinishEvaluationUseCase providesFinishEvaluationUseCase(
            RealmConfiguration realmConfiguration,
            LevelUnitController levelUnitController,
            EvaluationController evaluationController) {
        return new FinishEvaluationUseCase(realmConfiguration, levelUnitController, evaluationController);
    }
}