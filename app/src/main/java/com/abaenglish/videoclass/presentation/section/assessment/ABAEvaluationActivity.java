package com.abaenglish.videoclass.presentation.section.assessment;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.abaenglish.videoclass.ABAApplication;
import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.analytics.cooladata.navigation.CooladataNavigationTrackingManager;
import com.abaenglish.videoclass.data.persistence.ABAEvaluation;
import com.abaenglish.videoclass.data.persistence.ABAEvaluationQuestion;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.data.persistence.ABAUser;
import com.abaenglish.videoclass.domain.content.EvaluationController;
import com.abaenglish.videoclass.domain.content.LevelUnitController;
import com.abaenglish.videoclass.domain.content.UserController;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.abaenglish.videoclass.presentation.base.ABAMasterActivity;
import com.abaenglish.videoclass.presentation.base.custom.ABATextView;
import com.abaenglish.videoclass.presentation.section.SectionActivityInterface;
import com.bzutils.images.RoundedImageView;
import com.crashlytics.android.Crashlytics;
import com.nostra13.universalimageloader.core.ImageLoader;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.abaenglish.videoclass.analytics.cooladata.CooladataTrackingDefinitions.CooladataSectionType.ABAEvaluation;
import static com.abaenglish.videoclass.presentation.section.SectionActivityInterface.EXTRA_UNIT_ID;

/**
 * Created by madalin on 7/05/15.
 */
public class ABAEvaluationActivity extends ABAMasterActivity {

    private ABAUnit currentUnit;

    @BindView(R.id.evaluationInfo)
    ABATextView evaluationInfo;
    @BindView(R.id.start_evaluation)
    Button continueEvaluation;

    @BindView(R.id.toolbarTitle)
    ABATextView unitSectionTitle;
    @BindView(R.id.toolbarSubTitle)
    ABATextView unitSectionProgress;

    @BindView(R.id.teacherImg)
    RoundedImageView img;

    @Inject
    LevelUnitController levelUnitController;
    @Inject
    EvaluationController evaluationController;
    @Inject
    UserController userController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String unitId = getIntent().getExtras().getString(EXTRA_UNIT_ID);
        currentUnit = DataStore.getInstance().getLevelUnitController().getUnitWithId(getRealm(), unitId);
        getComponent().inject(this);
        setContentView(R.layout.activity_evaluation);
        ButterKnife.bind(this);

        initApp(unitId);
        configActionBar();
        configTeacher();
        config();

        trackSection();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishSection();
    }

    // ================================================================================
    // Private methods
    // ================================================================================

    private void configActionBar() {
        findViewById(R.id.toolbarLeftButton).setOnTouchListener(((ABAApplication) getApplicationContext()).getImagePressedState(null));
        findViewById(R.id.toolbarLeftButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishSection();
            }
        });

        unitSectionTitle.setText(R.string.unitMenuTitle8Key);
        unitSectionProgress.setVisibility(View.GONE);
    }

    private void initApp(String UnitID) {
        currentUnit = levelUnitController.getUnitWithId(getRealm(), UnitID);
        ABAEvaluation abaEvaluation = currentUnit.getSectionEvaluation();

        int currentExercise = 1;
        for (ABAEvaluationQuestion question : abaEvaluation.getContent()) {
            if (question.isAnswered()) {
                currentExercise++;
            }
        }

        if (currentExercise == abaEvaluation.getContent().size() + 1) {
            evaluationController.resetEvaluationAnswers(getRealm(), abaEvaluation);
            currentExercise = 1;
        }

        if (currentExercise != 1) {
            continueEvaluation();
        }
    }

    private void configTeacher() {
        // :O ImageLoader cache for what?
        if (levelUnitController.checkIfFileExist(currentUnit, userController.getCurrentUser(getRealm()).getTeacherImage())) {
            levelUnitController.displayImage(null, userController.getCurrentUser(getRealm()).getTeacherImage(), img);
        } else {
            String url = (userController.getCurrentUser(getRealm()).getTeacherImage());
            ImageLoader.getInstance().displayImage(url, img);
        }
    }

    protected void finishSection() {
        // Navigating to next unit
        unitDetailABTestingConfigurator.startUnitDetailIntent(this, getIntent().getExtras().getString(EXTRA_UNIT_ID));
        finish();
        overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_exit_unitanimation2);
    }

    protected void continueEvaluation() {
        Intent intent = new Intent(ABAEvaluationActivity.this, EvaluationExerciseActivity.class);
        intent.putExtra(SectionActivityInterface.EXTRA_UNIT_ID, currentUnit.getIdUnit());
        intent.putExtra(EvaluationExerciseActivity.CURRENT_STATUS, EvaluationExerciseActivity.UserStatusType.kUserRepeatEvaluation);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_enter_unitanimation1, R.anim.slide_enter_unitanimation2);
        finish();
    }

    private void config() {
        continueEvaluation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                continueEvaluation();
            }
        });
        evaluationInfo.setText(Html.fromHtml(getResources().getString(R.string.evaluationInfoText1Key) + " " + "<b>" + getResources().getString(R.string.evaluationInfoText2Key) + "</b>"
                + "<br>" + "<br>" + getResources().getString(R.string.evaluationInfoText3Key)));
    }

    // ================================================================================
    // Private methods - Tracking
    // ================================================================================

    private void trackSection() {
        ABAUser user = userController.getCurrentUser(getRealm());
        CooladataNavigationTrackingManager.trackEnteredSection(user.getUserId(), currentUnit.getLevel().getIdLevel(), currentUnit.getIdUnit(), ABAEvaluation);
        Crashlytics.log(Log.INFO, "Section", "User opens Section Assessment");
        tracker.trackEnteredSection(currentUnit.getLevel().getIdLevel(), currentUnit.getIdUnit(), ABAEvaluation.toString());
    }

    protected EvaluationComponent getComponent() {
        return ABAApplication.get().getEvaluationComponent();
    }
}
