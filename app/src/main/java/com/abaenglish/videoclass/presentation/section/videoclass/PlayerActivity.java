package com.abaenglish.videoclass.presentation.section.videoclass;

import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.VideoView;

import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.data.network.APIManager;
import com.abaenglish.videoclass.data.persistence.ABAAPIError;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.domain.content.DataController;
import com.abaenglish.videoclass.domain.content.GenericController;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.abaenglish.videoclass.presentation.base.ABAMasterSectionActivity;
import com.abaenglish.videoclass.presentation.base.custom.ABATextView;
import com.abaenglish.videoclass.presentation.section.SectionType;
import com.abaenglish.videoclass.presentation.section.videoclass.videoplayer.Caption;
import com.abaenglish.videoclass.presentation.section.videoclass.videoplayer.FormatSRT;
import com.abaenglish.videoclass.presentation.section.videoclass.videoplayer.TimedTextObject;
import com.bzutils.LogBZ;

import java.io.FileInputStream;
import java.util.Collection;

import static android.content.res.Configuration.ORIENTATION_PORTRAIT;
import static com.abaenglish.videoclass.presentation.section.SectionActivityInterface.EXTRA_UNIT_ID;

public class PlayerActivity extends ABAMasterSectionActivity implements OnPreparedListener, OnCompletionListener, View.OnClickListener {

    // Key for the EXTRAS
    public static final String SUB_OPTION = "SUB_OPTION";
    public static final String VIDEO_URL_EXTRA = "VIDEO_URL";
    public static final String VIDEO_SUBTITLE_EXTRA = "VIDEO_SUBTITLE";
    public static final String SECTION_ID = "SECTION_ID";
    public static final String COMPLETED_MODE = "COMPLETED_MODE";
    public static final String IS_DOWNLOADED = "IS_DONWLOADED";

    // Posible values for the EXTRAS
    public static final int FILM_SECTION = 0;
    public static final int VIDEOCLASS_SECTION = 1;

    private VideoView videoView;
    private MediaController mController;
    private ProgressBar progressDialog;
    private TextView subtitleText;
    private LinearLayout topController;
    private Handler handler;
    private Runnable runnable;
    private ABAUnit currentUnit;
    public SectionType sectionType;

    // ================================================================================
    // Subtitle declarations
    // ================================================================================

    private TimedTextObject srt;
    private Handler subtitleDisplayHandler;
    private Runnable subtitleProcessor = new Runnable() {
        @Override
        public void run() {
            if (videoView != null && videoView.isPlaying()) {
                int currentPos = videoView.getCurrentPosition();
                Collection<Caption> subtitles = srt.captions.values();
                for (Caption caption : subtitles) {
                    if (currentPos >= caption.start.mseconds && currentPos <= caption.end.mseconds) {
                        onTimedText(caption);
                        break;
                    } else if (currentPos > caption.end.mseconds) {
                        onTimedText(null);
                    }
                }
            }
            subtitleDisplayHandler.postDelayed(this, 100);
        }
    };

    // ================================================================================
    // Activity lifecycle
    // ================================================================================

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);
        ABATextView playerBackButton = (ABATextView) findViewById(R.id.videoback);
        topController = (LinearLayout) findViewById(R.id.topController);
        subtitleText = (TextView) findViewById(R.id.offLine_subtitleText);
        progressDialog = (ProgressBar) findViewById(R.id.progressBar);
        videoView = (VideoView) findViewById(R.id.videoView);
        videoView.setKeepScreenOn(true);
        mController = new MediaController(this);

        currentUnit = DataStore.getInstance().getLevelUnitController().getUnitWithId(getRealm(), getIntent().getExtras().getString(EXTRA_UNIT_ID));
        playerBackButton.setOnClickListener(this);

        // Section type
        sectionType = SectionType.kVideoClase;
        if (getIntent().getExtras().getInt(SECTION_ID) == FILM_SECTION) {
            sectionType = SectionType.kABAFilm;
        }

        configTopController();
        prepareVideoForPlay();
    }

    @Override
    public void onPause() {
        if (subtitleDisplayHandler != null) {
            subtitleDisplayHandler.removeCallbacks(subtitleProcessor);
            subtitleDisplayHandler = null;
        }
        // Stopping player in order to avoid a continuous loop
        if (videoView != null) {
            videoView.pause();
        }
        super.onPause();
    }

    @Override
    public void onResume() {
        subtitleDisplayHandler = new Handler();
        super.onResume();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setUpMediaController();
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        // When the window loses focus (e.g. the action overflow is shown),
        // cancel any pending hide action. When the window gains focus,
        // hide the system UI.
        if (hasFocus) {
            delayedHide(300);
        } else {
            mHideHandler.removeMessages(0);
        }
    }

    private void hideSystemUI() {
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LOW_PROFILE
                | View.SYSTEM_UI_FLAG_IMMERSIVE);
    }


    private final Handler mHideHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            hideSystemUI();
        }
    };

    private void delayedHide(int delayMillis) {
        mHideHandler.removeMessages(0);
        mHideHandler.sendEmptyMessageDelayed(0, delayMillis);
    }

    private int getNavBarHeight() {
        Resources resources = getResources();
        int resourceId = resources.getIdentifier("navigation_bar_height", "dimen", "android");
        if (resourceId > 0) {
            return resources.getDimensionPixelSize(resourceId);
        }
        return 0;
    }

    @Override
    public void onStop() {
        super.onStop();
        if (videoView != null) {
            videoView.stopPlayback();
        }
    }

    // ================================================================================
    // Device Interaction Methods
    // ================================================================================

    @Override
    public void onClick(View view) {
        finishSectionWithResult(RESULT_CANCELED);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                topController.setVisibility(View.VISIBLE);
                handler.removeCallbacks(runnable);
                break;
            case MotionEvent.ACTION_UP:
                topControllerHide();
                break;
        }
        if (mController != null) {
            if (!mController.isShowing()) {
                mController.show();
            }
        }
        return false;
    }

    @Override
    public void onBackPressed() {
        finishSectionWithResult(RESULT_CANCELED);
    }

    @Override
    protected void setScreenOrientation() {
        //Default behaviour
    }

    // ================================================================================
    // Private methods
    // ================================================================================

    private void prepareVideoForPlay() {
        try {
            topControllerHide();

            setUpMediaController();
            String videoURL = getIntent().getExtras().getString(PlayerActivity.VIDEO_URL_EXTRA);
            if (DataStore.getInstance().getLevelUnitController().checkIfFileExist(currentUnit, videoURL)) {
                //Prepare Synchronously for playing storage video
                videoView.setVideoPath(GenericController.getLocalFilePath(currentUnit, videoURL));
            } else {
                //Prepare Asynchronously for playing streaming video
                videoView.setVideoURI(Uri.parse(videoURL));
            }

            videoView.setOnPreparedListener(this);

        } catch (IllegalArgumentException | SecurityException | IllegalStateException e) {
            LogBZ.printStackTrace(e);
        }
    }

    private void setUpMediaController() {
        mController.setBackgroundColor(Color.TRANSPARENT);
        mController.setFitsSystemWindows(false);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        if (hasNavBar(getResources()) && (getResources().getConfiguration().orientation == ORIENTATION_PORTRAIT || APIManager.isTablet(this))) {
            params.bottomMargin = getNavBarHeight();
        } else {
            params.bottomMargin = 0;
        }
        mController.setLayoutParams(params);
        mController.setAnchorView(videoView);
        mController.setMediaPlayer(new FilmControl());
        if (videoView != null) {
            videoView.setMediaController(mController);
        }
    }

    public boolean hasNavBar(Resources resources) {
        int id = resources.getIdentifier("config_showNavigationBar", "bool", "android");
        return id > 0 && resources.getBoolean(id);
    }

    private void configTopController() {
        handler = new Handler();
        runnable = new Runnable() {
            @Override
            public void run() {
                topController.setVisibility(View.INVISIBLE);
            }
        };
    }

    private void topControllerHide() {
        handler.postDelayed(runnable, 3000);
    }

    private void cleanUp() {
        if (videoView != null) {
            videoView.pause();
            videoView.suspend();
            videoView = null;
        }
    }

    private void finishSectionWithResult(int finishResult) {
        cleanUp();
        setResult(finishResult);
        finish();
    }

    private void onTimedText(Caption text) {
        if (text == null) {
            subtitleText.setVisibility(View.INVISIBLE);
            return;
        }
        subtitleText.setText(Html.fromHtml(text.content));
        subtitleText.setVisibility(View.VISIBLE);
    }

    // ================================================================================
    // Private unnamed classes
    // ================================================================================

    @Override
    public void onPrepared(MediaPlayer mp) {
        // And downloading subtitles
        String url = getIntent().getExtras().getString(PlayerActivity.VIDEO_SUBTITLE_EXTRA);
        if (DataStore.getInstance().getLevelUnitController().checkIfFileExist(currentUnit, url)) {
            try {
                FileInputStream stream = new FileInputStream(GenericController.getLocalFilePath(currentUnit, url));
                FormatSRT formatSRT = new FormatSRT();
                srt = formatSRT.parseFile("sample.srt", stream);
                subtitleDisplayHandler.post(subtitleProcessor);
            } catch (Exception e) {
                e.printStackTrace();
                LogBZ.e("error in downloading subs");
            }

        } else {
            DataStore.getInstance().getFilmController().downloadSubtitles(url, new DataController.ABAAPICallback<TimedTextObject>() {
                @Override
                public void onSuccess(TimedTextObject response) {
                    srt = response;
                    if (srt != null) {
                        // Making sure there is not 'null' references since this event could be raised after the activity is not present
                        if (subtitleText != null)
                            subtitleText.setText("");
                        if (subtitleDisplayHandler != null)
                            subtitleDisplayHandler.post(subtitleProcessor);
                    }
                }

                @Override
                public void onError(ABAAPIError error) {
                    LogBZ.e("error in download inf subs");
                }
            });
        }

        // Starting player
        videoView.setOnCompletionListener(this);
        mController.setEnabled(true);
        progressDialog.setVisibility(View.GONE);

        videoView.start();
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        finishSectionWithResult(RESULT_OK);
    }

    // ================================================================================
    // ABAMasterSectionActivity
    // ================================================================================

    @Override
    protected Object getSection() {
        if (sectionType == SectionType.kABAFilm) {
            return currentUnit.getSectionFilm();
        }
        return currentUnit.getSectionVideoClass();
    }

    public View getDecorView() {
        return getWindow().getDecorView();
    }

    // ================================================================================
    // Private classes
    // ================================================================================

    private class FilmControl implements MediaController.MediaPlayerControl {

        @Override
        public void start() {
            if (videoView != null) {
                videoView.start();
            }
        }

        @Override
        public void pause() {
            if (videoView != null) {
                videoView.pause();
            }
        }

        @Override
        public int getDuration() {
            if (videoView != null) {
                return videoView.getDuration();
            } else {
                return 0;
            }
        }

        @Override
        public int getCurrentPosition() {
            if (videoView != null) {
                return videoView.getCurrentPosition();
            } else {
                return 0;
            }
        }

        @Override
        public void seekTo(int pos) {
            if (videoView != null) {
                videoView.seekTo(pos);
            }
        }

        @Override
        public boolean isPlaying() {
            if (videoView != null) {
                return videoView.isPlaying();
            } else {
                return false;
            }
        }

        @Override
        public int getBufferPercentage() {
            return 0;
        }

        @Override
        public boolean canPause() {
            return true;
        }

        @Override
        public boolean canSeekBackward() {
            return true;
        }

        @Override
        public boolean canSeekForward() {
            return true;
        }

        @Override
        public int getAudioSessionId() {
            return 0;
        }
    }

}