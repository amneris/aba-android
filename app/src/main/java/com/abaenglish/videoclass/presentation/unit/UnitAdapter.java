package com.abaenglish.videoclass.presentation.unit;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.data.persistence.ABALevel;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.abaenglish.videoclass.presentation.base.custom.ABAExpandableListView;
import com.abaenglish.videoclass.presentation.base.custom.ABATextView;
import com.bzutils.BZScreenHelper;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

/**
 * Created by madalin on 17/04/15.
 */
public class UnitAdapter extends ABAExpandableListView.ABAExpandableListAdapter {

    private int CertCellHeight;
    private int StandardCellHeight;
    private int ActualCellHeight;

    private Context mContext;
    private LayoutInflater mLayoutInflate;
    private List<ABALevel> levels;

    private ABAUnit currentUnit;
    private ABALevel selectedLevel;
    private ArrayList<ABAUnit> unitsSorted;

    private final int abaColorWhite;
    private final int abaColorGreen;
    private final int abaColorLightGrey;
    private final int abaColorLightBlue;
    private final int abaColorBlue;
    private final int abaColorBlack;
    private final int screenHeight;

    public UnitAdapter(Context context, List<ABALevel> mLevel, Realm realm) {
        mContext = context;
        mLayoutInflate = LayoutInflater.from(context);
        screenHeight = BZScreenHelper.getScreenHeight(context);
        this.levels = mLevel;

        abaColorWhite = ContextCompat.getColor(context, R.color.abaWhite);
        abaColorGreen = ContextCompat.getColor(context, R.color.abaEmailGreen);
        abaColorLightGrey = ContextCompat.getColor(context, R.color.abaDescriptionLightGrey);
        abaColorBlack = ContextCompat.getColor(context, R.color.abaBlack);
        abaColorBlue = ContextCompat.getColor(context, R.color.abaUnitTitle);
        abaColorLightBlue = ContextCompat.getColor(context, R.color.abaLightBlue);
    }

    // ================================================================================
    // Configure ChildView
    // ================================================================================

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return getUnitFromPosition(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        if (childPosition != 0)
            return Long.parseLong(getUnitFromPosition(childPosition).getIdUnit());
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    @Override
    public View getRealChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        final ChildViewHolder mChildViewHolder;

        if (convertView == null) {
            mChildViewHolder = new ChildViewHolder();
            convertView = mLayoutInflate.inflate(R.layout.item_childunits, null);
            mChildViewHolder.lineUp = (ImageView) convertView.findViewById(R.id.line_up);
            mChildViewHolder.unitChapterNumber = (ABATextView) convertView.findViewById(R.id.unit_chapternumber);
            mChildViewHolder.lineDown = (ImageView) convertView.findViewById(R.id.line_down);
            mChildViewHolder.unitNumber = (ABATextView) convertView.findViewById(R.id.unit_number);
            mChildViewHolder.unitTitle = (ABATextView) convertView.findViewById(R.id.unit_title);
            mChildViewHolder.unitDescription = (ABATextView) convertView.findViewById(R.id.unit_description);
            mChildViewHolder.unitProgress = (ABATextView) convertView.findViewById(R.id.unit_progress);
            mChildViewHolder.lineDivider = (RelativeLayout) convertView.findViewById(R.id.line_divider);
            mChildViewHolder.unitBackground = (ImageView) convertView.findViewById(R.id.unit_background);
            mChildViewHolder.unitContent = (RelativeLayout) convertView.findViewById(R.id.unit_content);
            mChildViewHolder.lineDown2 = (ImageView) convertView.findViewById(R.id.line_down2);
            mChildViewHolder.cellChild = (LinearLayout) convertView.findViewById(R.id.cell_child);
            convertView.setTag(mChildViewHolder);
        } else {
            mChildViewHolder = (ChildViewHolder) convertView.getTag();
        }

        if (childPosition == 0) {
            //CERT POSITION
            configureCertCell(mChildViewHolder, convertView);
        } else {
            //FOR OTHER POSITION
            ABAUnit item = (ABAUnit) getChild(groupPosition, childPosition);
            configureUnitCell(mChildViewHolder, item, groupPosition, childPosition, convertView);
        }
        return convertView;
    }

    private class ChildViewHolder {
        private ImageView lineUp;
        private ABATextView unitChapterNumber;
        private ImageView lineDown;
        private ImageView lineDown2;
        private ABATextView unitNumber;
        private ABATextView unitTitle;
        private ABATextView unitDescription;
        private ABATextView unitProgress;
        private RelativeLayout lineDivider;
        private ImageView unitBackground;
        private RelativeLayout unitContent;
        private LinearLayout cellChild;

    }
    
    private void configureUnitCell(ChildViewHolder mChildViewHolder, ABAUnit item, int groupPosition, int childPosition, View convertView) {
        mChildViewHolder.lineUp.setVisibility(View.VISIBLE);
        mChildViewHolder.unitChapterNumber.setBackgroundResource(R.drawable.circle_white);

        if (getSelectedLevel().getUnits().get(0).equals(item)) {
            mChildViewHolder.lineDown.setVisibility(View.INVISIBLE);
            mChildViewHolder.lineDivider.setVisibility(View.GONE);
        } else {
            mChildViewHolder.lineDown.setVisibility(View.VISIBLE);
            mChildViewHolder.lineDivider.setVisibility(View.VISIBLE);
        }

        //LEFT PROGRESS BAR
        if (item.isCompleted()) {
            mChildViewHolder.unitChapterNumber.setBackgroundResource(R.drawable.circle_green);
            mChildViewHolder.unitChapterNumber.setTextColor(abaColorWhite);
            mChildViewHolder.lineUp.setBackgroundColor(abaColorGreen);
            mChildViewHolder.lineDown.setBackgroundColor(abaColorGreen);
            mChildViewHolder.lineDown2.setBackgroundColor(abaColorGreen);
        } else {
            mChildViewHolder.unitChapterNumber.setBackgroundResource(R.drawable.circle_white);
            mChildViewHolder.unitChapterNumber.setTextColor(abaColorBlack);
            mChildViewHolder.lineUp.setBackgroundColor(abaColorWhite);
            mChildViewHolder.lineDown.setBackgroundColor(abaColorWhite);
            mChildViewHolder.lineDown2.setBackgroundColor(abaColorWhite);
        }

        if (childPosition < getRealChildrenCount(groupPosition) - 1) {
            final ABAUnit item_down = (ABAUnit) getChild(groupPosition, childPosition + 1);

            if (childPosition >= 2) {
                final ABAUnit item_up = (ABAUnit) getChild(groupPosition, childPosition - 1);

                if (item_up.isCompleted()) {
                    mChildViewHolder.lineUp.setBackgroundColor(abaColorGreen);
                }

                if (item_down.isCompleted()) {
                    mChildViewHolder.lineDown.setBackgroundColor(abaColorGreen);
                    mChildViewHolder.lineDown2.setBackgroundColor(abaColorGreen);
                }

            } else if (childPosition == 1) {
                if (item_down.isCompleted()) {
                    mChildViewHolder.lineDown.setBackgroundColor(abaColorGreen);
                    mChildViewHolder.lineDown2.setBackgroundColor(abaColorGreen);
                }
            }
        } else {
            final ABAUnit item_up = (ABAUnit) getChild(groupPosition, childPosition - 1);
            if (item_up.isCompleted()) {
                mChildViewHolder.lineUp.setBackgroundColor(abaColorGreen);
            }
        }

        if (item.equals(getCurrentUnit())) {
            configureActualUnit(mChildViewHolder, item, convertView);
        } else {
            configureStandardUnit(mChildViewHolder, item, convertView);
        }

        mChildViewHolder.unitChapterNumber.setText(item.getIdUnit());
        mChildViewHolder.unitTitle.setText(item.getTitle());
        mChildViewHolder.unitDescription.setText(item.getDesc());
        mChildViewHolder.unitProgress.setVisibility(View.VISIBLE);
        mChildViewHolder.unitProgress.setText((int) item.getProgress() + "%");
    }

    private void configureActualUnit(ChildViewHolder mChildViewHolder, ABAUnit item, View convertView) {
        mChildViewHolder.unitContent.getLayoutParams().height = getActualCellHeight();
        getUnitImage(mChildViewHolder, item.getUnitImage());
        mChildViewHolder.unitNumber.setText(convertView.getResources().getString(R.string.unitKey).toUpperCase()
                + " " + (item.getIdUnit()));
        mChildViewHolder.unitNumber.setVisibility(View.VISIBLE);
        mChildViewHolder.unitNumber.setABATag(convertView.getResources().getInteger(R.integer.typefaceSans500));
        mChildViewHolder.unitTitle.setTextColor(abaColorLightBlue);
        mChildViewHolder.unitTitle.setTextSize(BZScreenHelper.dpFromPx(convertView.getResources().getDimension(R.dimen.dashboardTextsizeBig), convertView.getContext()));
        mChildViewHolder.unitTitle.setTypeface(Typeface.DEFAULT_BOLD);
        mChildViewHolder.unitTitle.setABATag(convertView.getResources().getInteger(R.integer.typefaceSans900));
        mChildViewHolder.unitDescription.setTextColor(abaColorWhite);
        mChildViewHolder.unitDescription.setABATag(convertView.getResources().getInteger(R.integer.typefaceSans500));
        mChildViewHolder.unitProgress.setTextColor(abaColorLightBlue);
        mChildViewHolder.unitProgress.setABATag(convertView.getResources().getInteger(R.integer.typefaceSans700));
    }

    private void configureStandardUnit(ChildViewHolder mChildViewHolder, ABAUnit item, View convertView) {
        mChildViewHolder.unitContent.getLayoutParams().height = getStandardCellHeight();
        getUnitImage(mChildViewHolder, item.getUnitImageInactive());
        mChildViewHolder.unitNumber.setVisibility(View.GONE);
        mChildViewHolder.unitTitle.setTextColor(abaColorBlue);
        mChildViewHolder.unitTitle.setTextSize(BZScreenHelper.dpFromPx(convertView.getResources().getDimension(R.dimen.dashboardTextsizeNormal), convertView.getContext()));
        mChildViewHolder.unitTitle.setTypeface(Typeface.DEFAULT);
        mChildViewHolder.unitTitle.setABATag(convertView.getResources().getInteger(R.integer.typefaceSans700));
        mChildViewHolder.unitDescription.setTextColor(abaColorLightGrey);
        mChildViewHolder.unitDescription.setABATag(convertView.getResources().getInteger(R.integer.typefaceSans700));
        mChildViewHolder.unitProgress.setTextColor(abaColorWhite);
        mChildViewHolder.unitProgress.setABATag(convertView.getResources().getInteger(R.integer.typefaceSans700));
    }

    private void configureCertCell(ChildViewHolder mChildViewHolder, View convertView) {
        mChildViewHolder.lineUp.setVisibility(View.INVISIBLE);
        mChildViewHolder.lineDown.setVisibility(View.VISIBLE);
        mChildViewHolder.lineDown2.setVisibility(View.VISIBLE);
        mChildViewHolder.unitTitle.setText(convertView.getResources().getString(R.string.certificateTitleKey).toUpperCase() + " " + getSelectedLevel().getIdLevel());
        mChildViewHolder.unitTitle.setTextSize(BZScreenHelper.dpFromPx(convertView.getResources().getDimension(R.dimen.dashboardTextsizeNormal), convertView.getContext()));
        mChildViewHolder.unitTitle.setTypeface(Typeface.DEFAULT_BOLD);
        mChildViewHolder.unitTitle.setTextColor(abaColorLightGrey);
        mChildViewHolder.unitTitle.setABATag(convertView.getResources().getInteger(R.integer.typefaceSans900));
        mChildViewHolder.unitDescription.setText(getSelectedLevel().getName());
        mChildViewHolder.unitDescription.setTextColor(abaColorLightGrey);
        mChildViewHolder.unitDescription.setABATag(convertView.getResources().getInteger(R.integer.typefaceSans700));
        mChildViewHolder.unitChapterNumber.setText("");
        mChildViewHolder.unitProgress.setVisibility(View.GONE);
        mChildViewHolder.lineDivider.setVisibility(View.VISIBLE);
        mChildViewHolder.unitNumber.setVisibility(View.GONE);

        mChildViewHolder.unitContent.getLayoutParams().height = getCertCellHeight();

        if (getCurrentUnit() == null || getCurrentUnit().isCompleted()) {
            mChildViewHolder.unitChapterNumber.setBackgroundResource(R.mipmap.circle_green_certificate);
            mChildViewHolder.lineDown.setBackgroundColor(abaColorGreen);
            mChildViewHolder.lineDown2.setBackgroundColor(abaColorGreen);
        } else {
            mChildViewHolder.unitChapterNumber.setBackgroundResource(R.mipmap.circle_white_certificate);
            mChildViewHolder.lineDown.setBackgroundColor(abaColorWhite);
            mChildViewHolder.lineDown2.setBackgroundColor(abaColorWhite);
        }

        mChildViewHolder.unitBackground.setImageResource(R.mipmap.cert_cell_background);
    }

    private void getUnitImage(final ChildViewHolder mChildViewHolder, final String unitImage) {
        mChildViewHolder.unitBackground.setImageDrawable(null);
        ImageLoader.getInstance().displayImage(unitImage, mChildViewHolder.unitBackground);
    }

    public ABAUnit getUnitFromPosition(int childPosition) {
        return unitsSorted.get(childPosition - 1);
    }

    // ================================================================================
    // Configure GroupView
    // ================================================================================

    @Override
    public Object getGroup(int groupPosition) {
        return getLevelFromGroupPosition(groupPosition);
    }

    @Override
    public int getGroupCount() {
        if (levels != null)
            return levels.size() + 1;
        return 0;
    }

    @Override
    public int getRealChildrenCount(int groupPosition) {
        if(levels.indexOf(selectedLevel) == groupPosition)
            return selectedLevel.getUnits().size() + 1;
        return 0;
    }

    @Override
    public long getGroupId(int groupPosition) {
        if (getLevelFromGroupPosition(groupPosition) == null)
            return 0;
        return Long.parseLong(getLevelFromGroupPosition(groupPosition).getIdLevel());
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        final GroupViewHolder mGroupViewHolder;
        if (convertView == null) {
            mGroupViewHolder = new GroupViewHolder();
            convertView = mLayoutInflate.inflate(R.layout.item_unit_dashboard, null);
            mGroupViewHolder.unitNumber = (ABATextView) convertView.findViewById(R.id.unitnumber);
            mGroupViewHolder.unitDifficulty = (ABATextView) convertView.findViewById(R.id.unitdifficulty);
            mGroupViewHolder.unitIconDropDown = (ABATextView) convertView.findViewById(R.id.unitdropdown);
            mGroupViewHolder.unitCertificate = (ImageView) convertView.findViewById(R.id.certificate_unit);
            mGroupViewHolder.lineSeparador = (ImageView) convertView.findViewById(R.id.line_separador);
            mGroupViewHolder.unitContainer = (LinearLayout) convertView.findViewById(R.id.unitlayout);
            convertView.setTag(mGroupViewHolder);

        } else {
            mGroupViewHolder = (GroupViewHolder) convertView.getTag();
        }

        if (groupPosition == 0) { //si es la primera, hacemos un fake group
            mGroupViewHolder.unitContainer.setVisibility(View.GONE);
            return convertView;
        }

        mGroupViewHolder.unitContainer.setVisibility(View.VISIBLE);

        final ABALevel level = getLevelFromGroupPosition(groupPosition);

        if (level.isCompleted() == true) {
            mGroupViewHolder.unitCertificate.setVisibility(View.VISIBLE);
        } else {
            mGroupViewHolder.unitCertificate.setVisibility(View.GONE);
        }

        if (groupPosition == levels.size()) {
            mGroupViewHolder.lineSeparador.setVisibility(View.GONE);
        } else {
            mGroupViewHolder.lineSeparador.setVisibility(View.VISIBLE);
        }

        mGroupViewHolder.unitNumber.setText(convertView.getResources().getText(R.string.levelKey).toString().toUpperCase() + " " + level.getIdLevel());
        mGroupViewHolder.unitDifficulty.setText(level.getName());
        mGroupViewHolder.unitIconDropDown.setTag(groupPosition);

        if (!level.equals(selectedLevel)) {
            mGroupViewHolder.unitIconDropDown.setText("+");
        } else {
            mGroupViewHolder.unitIconDropDown.setText("-");
        }

        return convertView;
    }

    private class GroupViewHolder {
        private ImageView unitCertificate;
        private ABATextView unitNumber;
        private ABATextView unitDifficulty;
        private ABATextView unitIconDropDown;
        private ImageView lineSeparador;
        private LinearLayout unitContainer;
    }

    // ================================================================================
    // Public Method Components Height
    // ================================================================================

    public int getCertCellHeight() {
        if(CertCellHeight != 0)
            return CertCellHeight;

        if (mContext.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
           return CertCellHeight = screenHeight / 8;
        return CertCellHeight = screenHeight / 7;
    }

    public int getStandardCellHeight() {
        if(StandardCellHeight != 0)
            return StandardCellHeight;

        if (mContext.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
            return StandardCellHeight = screenHeight / 6;
        return StandardCellHeight = screenHeight / 5;
    }

    public int getActualCellHeight() {
        if(ActualCellHeight != 0)
            return ActualCellHeight;

        if (mContext.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT)
            return  ActualCellHeight = (int) (screenHeight / 3.5);
        return ActualCellHeight = screenHeight / 3;
    }

    // ================================================================================
    // Public Methods
    // ================================================================================

    public ABALevel getLevelFromGroupPosition(int groupPosition) {
        if (groupPosition != 0)
            return levels.get(groupPosition - 1);
        return null;
    }

    public int getPositionOfLevel(ABALevel level) {
        return levels.indexOf(level) + 1;
    }

    public ABAUnit getCurrentUnit() {
        return this.currentUnit;
    }

    public ABALevel getSelectedLevel() {
        return selectedLevel;
    }

    public void setSelectedLevel(ABALevel selectedLevel) {
        this.selectedLevel = selectedLevel;
        if (selectedLevel != null) {
            unitsSorted = DataStore.getInstance().getLevelUnitController().getUnitsDescendingForLevel(selectedLevel);
            currentUnit = getCurrentUnitForSelectedLevel(selectedLevel);
        }
    }

    private ABAUnit getCurrentUnitForSelectedLevel(ABALevel selectedLevel) {
        ArrayList<ABAUnit> incompletedUnits = DataStore.getInstance().getLevelUnitController().getIncompletedUnitsForUnitsArray(unitsSorted);

        if (incompletedUnits.size() != 0)
            return incompletedUnits.get(incompletedUnits.size() - 1);
        //All Units Has Completed
        return null;
    }
}

