package com.abaenglish.videoclass.presentation.plan;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;

import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.analytics.cooladata.navigation.CooladataNavigationTrackingManager;
import com.abaenglish.videoclass.data.persistence.ABAUser;
import com.abaenglish.videoclass.domain.content.UserController;
import com.abaenglish.videoclass.presentation.base.ABAMasterActivity;

/**
 * Created by madalin on 6/11/15.
 * Added Cooladata analytics by Jesus on 22/04/2016
 */
public class PlansActivity extends ABAMasterActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        Fragment plansFragment;
        if (savedInstanceState == null) {
            if (serverConfig.shouldShowImprovedPlansPage()) {
                plansFragment = new ImprovedPlansFragment_();
            } else {
                plansFragment = new PlansFragment();
            }
        } else {
            StringBuilder targetFragmentName = new StringBuilder("Plans.");
            if (serverConfig.shouldShowImprovedPlansPage()) {
                targetFragmentName.append(ImprovedPlansFragment.class.getName());
            } else {
                targetFragmentName.append(PlansFragment.class.getName());
            }

            plansFragment = getSupportFragmentManager().findFragmentByTag(targetFragmentName.toString());
        }
        changeFragment(plansFragment);

        trackPayments();
    }

    private void changeFragment(Fragment targetFragment) {
        /* amora: targetFragment use Activity + FragmentClassName to be unique id depending on loaded fragment */
        StringBuilder targetFragmentName = new StringBuilder("Plans.");
        if (serverConfig.shouldShowImprovedPlansPage()) {
            targetFragmentName.append(ImprovedPlansFragment.class.getName());
        } else {
            targetFragmentName.append(PlansFragment.class.getName());
        }

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_fragment, targetFragment, targetFragmentName.toString())
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }

    public void onBackPressed() {
        finishActivity();
    }

    private void finishActivity() {
        finish();
        overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_exit_unitanimation2);
    }

    // ================================================================================
    // Private methods - Tracking
    // ================================================================================

    private void trackPayments() {
        ABAUser user = UserController.getInstance().getCurrentUser(getRealm());
        CooladataNavigationTrackingManager.trackOpenedPrices(user.getUserId());
        tracker.trackOpenedPrices();
    }
}
