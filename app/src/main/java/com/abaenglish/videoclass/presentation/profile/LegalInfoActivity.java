package com.abaenglish.videoclass.presentation.profile;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.abaenglish.videoclass.ABAApplication;
import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.data.network.APIManager;
import com.abaenglish.videoclass.presentation.base.custom.ABATextView;

import butterknife.BindColor;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LegalInfoActivity extends AppCompatActivity {

    @BindView(R.id.legalInfoTextView) TextView legalInfoTextView;
    @BindView(R.id.legalInfoTitle) TextView legalInfoTitle;
    @BindView(R.id.toolbarLeftButton) ImageButton toolbarLeftButton;
    @BindView(R.id.toolbarTitle) ABATextView toolbarTitle;
    @BindView(R.id.toolbarSubTitle) ABATextView toolbarSubtitle;
    @BindColor(R.color.abaLightBlue) int abaLightBlue;
    @BindColor(R.color.abaWhite) int abaWhite;
    @BindString(R.string.yourProfileKey) String toolbarTitleText;

    private static String TOOLBAR_TITLE_EXTRA_KEY = "TOOLBAR_TITLE_EXTRA_KEY";
    private static String LEGAL_INFO_EXTRA_KEY = "LEGAL_INFO_EXTRA_KEY";

    private String toolbarSubtitleResource;
    private String legalTextResource;

    public static Intent getPolicyInfoIntent(Context context) {
        return getLaunchingIntent(context, R.string.profile_privacy_policy_button, R.string.profilePolicyDescriptionKey);
    }

    public static Intent getTOUIntent(Context context) {
        return getLaunchingIntent(context, R.string.profile_term_of_use_button, R.string.profileTemsDescriptionKey);
    }

    private static Intent getLaunchingIntent(Context context, @StringRes int title, @StringRes int body) {
        Intent intent = new Intent(context, LegalInfoActivity.class);
        intent.putExtra(TOOLBAR_TITLE_EXTRA_KEY, context.getResources().getString(title));
        intent.putExtra(LEGAL_INFO_EXTRA_KEY, context.getResources().getString(body));
        return intent;
    }

    protected void setScreenOrientation() {
        ///Force portrait mode for phones
        if (!APIManager.isTablet(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(savedInstanceState == null) {
            toolbarSubtitleResource = getIntent().getStringExtra(TOOLBAR_TITLE_EXTRA_KEY);
            legalTextResource =  getIntent().getStringExtra(LEGAL_INFO_EXTRA_KEY);
        } else {
            toolbarSubtitleResource = savedInstanceState.getString(TOOLBAR_TITLE_EXTRA_KEY);
            legalTextResource =  savedInstanceState.getString(LEGAL_INFO_EXTRA_KEY);
        }
        setContentView(R.layout.activity_legal_info);
        ButterKnife.bind(this);
        configureToolbar();
        configView();
        setScreenOrientation();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(TOOLBAR_TITLE_EXTRA_KEY, toolbarSubtitleResource);
        outState.putString(LEGAL_INFO_EXTRA_KEY, legalTextResource);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        toolbarSubtitleResource = savedInstanceState.getString(TOOLBAR_TITLE_EXTRA_KEY);
        legalTextResource = savedInstanceState.getString(LEGAL_INFO_EXTRA_KEY);
        super.onRestoreInstanceState(savedInstanceState);
    }

    @OnClick(R.id.toolbarLeftButton) public void backButtonAction() {
        ((ABAApplication) getApplicationContext()).getImagePressedState(null);
        finish();
    }

    private void configView() {
        legalInfoTitle.setText(toolbarSubtitleResource);
        legalInfoTextView.setText(legalTextResource);
    }

    private void configureToolbar() {
        toolbarTitle.setTextColor(abaLightBlue);
        toolbarTitle.setVisibility(View.VISIBLE);
        toolbarSubtitle.setTextColor(abaWhite);
        toolbarSubtitle.setVisibility(View.VISIBLE);

        toolbarTitle.setText(toolbarTitleText);
        toolbarSubtitle.setText(toolbarSubtitleResource);
    }
}
