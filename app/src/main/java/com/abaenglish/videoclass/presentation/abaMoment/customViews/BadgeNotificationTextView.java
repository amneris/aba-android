package com.abaenglish.videoclass.presentation.abaMoment.customViews;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;

import com.abaenglish.videoclass.R;

/**
 * Created by julien on 02/03/2017.
 * Badge notifications to alert user that new AbaMoments are available.
 */


public class BadgeNotificationTextView extends android.support.v7.widget.AppCompatTextView {

    int solidColor = R.color.recordRedColor;

    public BadgeNotificationTextView(Context context) {
        super(context);
    }

    public BadgeNotificationTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BadgeNotificationTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void draw(Canvas canvas) {

        Paint circlePaint = new Paint();

        circlePaint.setColor(ContextCompat.getColor(getContext(), solidColor));
        circlePaint.setFlags(Paint.ANTI_ALIAS_FLAG);

        int h = this.getHeight();
        int w = this.getWidth();

        int diameter = ((h > w) ? h : w);
        int radius = diameter / 2;

        this.setHeight(diameter);
        this.setWidth(diameter);

        setText("1");

        canvas.drawCircle(diameter / 2, diameter / 2, radius, circlePaint);

        super.draw(canvas);
    }

}