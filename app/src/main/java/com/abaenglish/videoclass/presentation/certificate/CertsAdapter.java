package com.abaenglish.videoclass.presentation.certificate;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ProgressBar;

import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.data.persistence.ABALevel;
import com.abaenglish.videoclass.presentation.base.custom.ABATextView;

import java.util.List;

/**
 * Created by madalin on 16/04/15.
 */
public class CertsAdapter extends BaseAdapter implements ListAdapter {
    private Context mContext;
    private LayoutInflater mLayoutInflate;
    private List<ABALevel> certificats;
    private ObjectAnimator animation;

    public CertsAdapter(Context context, List<ABALevel> resource) {
        mContext = context;
        mLayoutInflate = LayoutInflater.from(context);
        this.certificats = resource;

    }

    @Override
    public int getCount() {
        if (certificats != null) {
            return certificats.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        if (certificats != null) {
            return certificats.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ItemViewHolder itemViewHolder;

        if (convertView == null) {
            itemViewHolder = new ItemViewHolder();
            convertView = mLayoutInflate.inflate(R.layout.item_certificates, parent, false);
            itemViewHolder.iconCertificate = (ImageView) convertView.findViewById(R.id.icon_certificate);
            itemViewHolder.titleCertificate = (ABATextView) convertView.findViewById(R.id.title_certificate);
            itemViewHolder.descriptionCertificate = (ABATextView) convertView.findViewById(R.id.description_certificate);
            itemViewHolder.progressCertificate = (ProgressBar) convertView.findViewById(R.id.progress_certificate);
            convertView.setTag(itemViewHolder);

        } else {
            itemViewHolder = (ItemViewHolder) convertView.getTag();
        }

        final ABALevel item = (ABALevel) getItem(position);

        if (item.isCompleted() == true) {
            itemViewHolder.iconCertificate.setImageResource(R.mipmap.icon_certificate);
        } else {
            itemViewHolder.iconCertificate.setImageResource(R.mipmap.icon_nocertificate);
        }

        itemViewHolder.titleCertificate.setText(convertView.getResources().getIdentifier("certificateNameLevel" + item.getIdLevel(),
                "string", convertView.getContext().getPackageName()));
        animation = ObjectAnimator.ofInt(itemViewHolder.progressCertificate, "progress", -10, (int) item.getProgress());
        animation.setDuration(1500);
        // animation.setInterpolator(new DecelerateInterpolator());
        animation.start();
        itemViewHolder.descriptionCertificate.setText(convertView.getResources().getString(R.string.certificateLevelKey) + " " + item.getIdLevel());


        return convertView;
    }

    private class ItemViewHolder {
        private ImageView iconCertificate;
        private ABATextView titleCertificate;
        private ABATextView descriptionCertificate;
        private ProgressBar progressCertificate;
    }
}
