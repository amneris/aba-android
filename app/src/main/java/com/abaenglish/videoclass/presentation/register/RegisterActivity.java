package com.abaenglish.videoclass.presentation.register;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.abaenglish.shepherd.ABAShepherdEditor;
import com.abaenglish.shepherd.configuration.configurators.GenericShepherdConfigurator;
import com.abaenglish.shepherd.configuration.configurators.abacore.ABACoreShepherdEnvironment;
import com.abaenglish.shepherd.plugin.plugins.ShepherdLoginPlugin;
import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.analytics.TrackerContract;
import com.abaenglish.videoclass.data.persistence.ABAAPIError;
import com.abaenglish.videoclass.data.persistence.ABAUser;
import com.abaenglish.videoclass.domain.content.DataController;
import com.abaenglish.videoclass.domain.content.UserController;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.abaenglish.videoclass.presentation.base.ABAMasterSessionActivity;
import com.abaenglish.videoclass.presentation.base.custom.ABAErrorNotification;
import com.abaenglish.videoclass.presentation.base.custom.ABAErrorOption;
import com.abaenglish.videoclass.presentation.base.custom.ABAProgressDialog;
import com.abaenglish.videoclass.presentation.base.custom.ABATextView;
import com.abaenglish.videoclass.presentation.base.custom.FontCache;
import com.abaenglish.videoclass.presentation.level.SelectLevelAcivity;
import com.abaenglish.videoclass.presentation.login.LoginActivity;
import com.abaenglish.videoclass.session.SessionService;
import com.bzutils.BZUtils;
import com.jakewharton.rxbinding.widget.RxTextView;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindColor;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func3;

/**
 * Created by xabierlosada on 12/07/16.
 * Development continued by jespejo on 21/07/16
 */
public class RegisterActivity extends ABAMasterSessionActivity {

    @BindView(R.id.main_layout)
    View mainLayout;

    @BindView(R.id.animation_layout)
    View animationLayout;

    @BindView(R.id.tutorialItemLogo)
    ImageView abaLogoImageView;

    @BindView(R.id.loginTextView)
    ABATextView loginTextView;

    // name input
    @BindView(R.id.nameEditTextLayout)
    TextInputLayout nameEditTextLayout;
    @BindView(R.id.nameEditTextInput)
    TextInputEditText nameInputText;

    // email input
    @BindView(R.id.emailEditTextLayout)
    TextInputLayout emailInputLayout;
    @BindView(R.id.emailEditTextInput)
    TextInputEditText emailInputText;

    // Password input
    @BindView(R.id.passwordEditTextLayout)
    TextInputLayout passwordInputLayout;
    @BindView(R.id.passwordEditTextInput)
    TextInputEditText passwordInputText;

    @BindString(R.string.funnelABtestLoginTextButton)
    String loginQuestionLabel;
    @BindString(R.string.funnelABtestLoginJoinTextButton)
    String loginLabel;

    @BindView(R.id.registerActionButton)
    Button registerButton;
    @BindView(R.id.registerActionButtonDisabled)
    Button registerButtonDisabled;
    @BindView(R.id.registerFacebookButton)
    Button registerFacebookButton;

    @BindColor(R.color.abaWhite)
    int whiteColorRes;

    // ================================================================================
    // Public methods - Activity lifecycle
    // ================================================================================
    @Inject
    TrackerContract.Tracker tracker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        configViews();
    }

    @Override
    public void onBackPressed() {
        // Opening tutorial
        openTutorial();
    }

    // ================================================================================
    // Private methods - UI
    // ================================================================================

    private void configViews() {

        registerFacebookButton.setTypeface(fontCache.getTypeface(FontCache.ABATypeface.sans500));

        loginTextView.setText(Html.fromHtml(loginQuestionLabel + "<font color='#07BCE6'> " + loginLabel + "</font>"));

        // continuing password field tuning
        passwordInputLayout.setTypeface(Typeface.DEFAULT);
        passwordInputText.setTransformationMethod(new PasswordTransformationMethod());
        passwordInputText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);

        // listener to finish the form
        passwordInputText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    validateFormAndRegister();
                }
                return false;
            }
        });

        // Setting focus change listener in order to animate the form up
        nameInputText.setOnFocusChangeListener(logoHiderFocusListener);
        emailInputText.setOnFocusChangeListener(logoHiderFocusListener);
        passwordInputText.setOnFocusChangeListener(logoHiderFocusListener);

        // Forcing the focus not to be in the first EditText by default
        mainLayout.requestFocus();
        updateRegisterButtonStyle(false); // Disabling register button
        passwordInputText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);

        nameEditTextLayout.setErrorEnabled(true);
        nameInputText.setError(null);
        passwordInputLayout.setErrorEnabled(true);
        passwordInputText.setError(null);
        emailInputLayout.setErrorEnabled(true);
        emailInputText.setError(null);

        Observable<CharSequence> nameSource = streamFromField(nameInputText, nameEditTextLayout);
        Observable<CharSequence> emailSource = streamFromField(emailInputText, emailInputLayout);
        Observable<CharSequence> passwordSource = streamFromField(passwordInputText, passwordInputLayout);

        rx.Observable.combineLatest(
                nameSource,
                emailSource,
                passwordSource,
                new Func3<CharSequence, CharSequence, CharSequence, Boolean>() {
                    @Override
                    public Boolean call(CharSequence nameSequence, CharSequence emailSequence, CharSequence passSequence) {
                        Log.d("chasequence", "CharSequence: " + nameSequence + ", " + emailSequence + ", " + passSequence);
//                        if (nameSequence.length() > 0) validateName();
//                        if (passSequence.length() > 0) validatePassword();
//                        if (emailSequence.length() > 0) validateEmail();
                        return isNameValid() && isEmailValid() && isPasswordValid();
                    }
                })
                .subscribe(new Action1<Boolean>() {
                    @Override
                    public void call(Boolean aBoolean) {
                        updateRegisterButtonStyle(aBoolean);
                    }
                });
    }

    // ================================================================================
    // Public methods - On click
    // ================================================================================

    @OnClick({R.id.registerFacebookButton, R.id.loginTextView, R.id.registerActionButton, R.id.registerActionButtonDisabled})
    protected void onClick(View v) {
        switch (v.getId()) {
            case R.id.loginTextView:
                openLogin();
                break;
            case R.id.registerActionButton:
            case R.id.registerActionButtonDisabled:
                if (!isViewDisableForClick()) {
                    validateFormAndRegister();
                }
                break;
            case R.id.registerFacebookButton:
                loginWithFacebook();
                break;
            default:
                break;
        }
    }

    // ================================================================================
    // Private methods - Form validation
    // ================================================================================

    private void validateFormAndRegister() {

        if (!isNameValid()) {
            validateName();
            highlightErrorAtInputText(nameInputText);
            return;
        }

        if (!isEmailValid()) {
            validateEmail();
            highlightErrorAtInputText(emailInputText);
            return;
        }

        if (!isPasswordValid()) {
            validatePassword();
            highlightErrorAtInputText(passwordInputText);
            return;
        }

        registerUserWithFields(nameInputText.getText().toString(), emailInputText.getText().toString(), passwordInputText.getText().toString());
    }

    private boolean isNameValid() {
        return UserController.isNameValid(nameInputText.getText().toString());
    }

    private boolean isEmailValid() {
        return !emailInputText.getText().toString().isEmpty() && UserController.isEmailValid(emailInputText.getText().toString());
    }

    /**
     * Password should be at least 6 chars
     *
     * @return true if password is not empty and has at least 6 chars
     */
    private boolean isPasswordValid() {
        return !passwordInputText.getText().toString().isEmpty() && UserController.isPasswordValid(passwordInputText.getText().toString());
    }

    // ================================================================================
    // Private methods - Error visualization
    // ================================================================================

    private void highlightErrorAtInputText(final TextInputEditText fieldError) {
        fieldError.setTextColor(ContextCompat.getColor(this, R.color.recordRedColor));
        fieldError.setHintTextColor(ContextCompat.getColor(this, R.color.recordRedColor));

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                fieldError.setTextColor(whiteColorRes);
                fieldError.setHintTextColor(whiteColorRes);
            }
        }, 3000);

        BZUtils.showKeyboard(this, fieldError);
    }

    private void validateName() {
        if (!isNameValid()) {
            nameEditTextLayout.setError(getString(R.string.regErrorNameNil));
        } else {
            nameEditTextLayout.setError(null);
        }
    }

    private void validateEmail() {
        if (!isEmailValid()) {
            emailInputLayout.setError(getString(R.string.regErrorEmailFormat));
        } else {
            emailInputLayout.setError(null);
        }
    }

    private void validatePassword() {
        if (!isPasswordValid()) {
            passwordInputLayout.setError(getString(R.string.regErrorPasswordMin));
        } else {
            passwordInputLayout.setError(null);
        }
    }

    private void updateRegisterButtonStyle(boolean isActive) {
        if (isActive) {
            registerButton.setVisibility(View.VISIBLE);
            registerButtonDisabled.setVisibility(View.GONE);
        } else {
            registerButton.setVisibility(View.GONE);
            registerButtonDisabled.setVisibility(View.VISIBLE);
        }
    }

    // ================================================================================
    // Private methods - Registration
    // ================================================================================

    private void registerUserWithFields(String userName, final String userEmail, final String userPassword) {
        showProgressDialog(ABAProgressDialog.SimpleDialog);
        BZUtils.hideKeyboard(this);
        mSessionService.register(this, userName, userEmail, userPassword, new DataController.ABASimpleCallback() {
            @Override
            public void onSuccess() {

                DataStore.getInstance().getEvaluationController().saveProgressActionForSection(getRealm(), null, null, null, null, false, false);
                dismissProgressDialog();

                // Storing successful key/password in order to get them the next time
                if (ABAShepherdEditor.isInternal()) {
                    SharedPreferences settings = getSharedPreferences(ShepherdLoginPlugin.LAST_LOGIN_SHARED_PREFERENCES, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = settings.edit();
                    editor.putString(ShepherdLoginPlugin.LAST_LOGIN_EMAIL_KEY, userEmail);
                    editor.putString(ShepherdLoginPlugin.LAST_LOGIN_PASS_KEY, userPassword);
                    editor.apply();

                    ABACoreShepherdEnvironment abaCoreEnvironment = (ABACoreShepherdEnvironment) ABAShepherdEditor.shared(RegisterActivity.this).environmentForShepherdConfigurationType(RegisterActivity.this, GenericShepherdConfigurator.ShepherdConfiguratorType.kShepherdConfiguratorTypeABACore);
                    ShepherdLoginPlugin.addLoginOption(RegisterActivity.this, userEmail, userPassword, abaCoreEnvironment.getEnvironmentName());
                }

                ABAUser user = DataStore.getInstance().getUserController().getCurrentUser(getRealm());
                RegisterActivity.this.appConfig.setUserEmail(user.getEmail());
                RegisterActivity.this.appConfig.setUserToken(user.getToken());

                openSelectLevel();
            }

            @Override
            public void onError(ABAAPIError error) {
                dismissProgressDialog();

                if (error.getError().equalsIgnoreCase(getString(R.string.regErrorEmailExist))) {
                    Snackbar.make(mainLayout, error.getError(), Snackbar.LENGTH_LONG)
                            .setAction(R.string.regButtonEnter, new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    openLogin();
                                }
                            })
                            .show();
                } else {
                    List<ABAErrorOption> options = new ArrayList<>();
                    final ABAErrorNotification notification = showABAErrorNotificationWithOptions(error.getError(), options);
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            notification.dismiss();
                        }
                    }, 3000);
                }
            }
        }, tracker);
    }

    // ================================================================================
    // Private methods - Navigation
    // ================================================================================

    private void openLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivityForResult(intent, LOGIN_FOR_RESULT);
        finish();
    }


    private void openSelectLevel() {
        Intent intent = new Intent(this, SelectLevelAcivity.class);
        startActivity(intent);

        if (DataStore.getInstance().getCurrentActivity() != null) {
            DataStore.getInstance().getCurrentActivity().finish();
        }

        finish();
    }

    // ================================================================================
    // Private methods - RX
    // ================================================================================

    private Observable<CharSequence> streamFromField(TextInputEditText editText, final TextInputLayout layout) {
        return RxTextView
                .textChanges(editText)
                .doOnNext(new Action1<CharSequence>() {
                    @Override
                    public void call(CharSequence charSequence) {
                        Log.d("field validation", "validate streamFromField - setError null");
                        layout.setError(null);
                    }
                })
                .debounce(500, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread());
    }

    // ================================================================================
    // Private classes - Listeners
    // ================================================================================

    /**
     * Listener to hide logo and move texts up
     */
    private View.OnFocusChangeListener logoHiderFocusListener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            abaLogoImageView.animate().alpha(0.0f);
            abaLogoImageView.setVisibility(View.GONE);

            if (nameInputText.length() > 0) validateName();
            if (passwordInputText.length() > 0) validatePassword();
            if (emailInputText.length() > 0) validateEmail();
        }
    };
}
