package com.abaenglish.videoclass.presentation.level;

import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;

import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.analytics.TrackerContract;
import com.abaenglish.videoclass.analytics.cooladata.profile.CooladataProfileTrackingManager;
import com.abaenglish.videoclass.analytics.legacy.LegacyTrackingManager;
import com.abaenglish.videoclass.analytics.modern.FabricTrackingManager;
import com.abaenglish.videoclass.analytics.modern.MixpanelTrackingManager;
import com.abaenglish.videoclass.data.CourseContentService;
import com.abaenglish.videoclass.data.network.APIManager;
import com.abaenglish.videoclass.data.persistence.ABAAPIError;
import com.abaenglish.videoclass.data.persistence.ABALevel;
import com.abaenglish.videoclass.domain.content.DataController;
import com.abaenglish.videoclass.domain.content.UserController;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.abaenglish.videoclass.presentation.base.ABAMasterActivity;
import com.abaenglish.videoclass.presentation.base.ABAMasterFragment;
import com.abaenglish.videoclass.presentation.base.LevelsAdapter;
import com.abaenglish.videoclass.presentation.base.custom.ABAProgressDialog;
import com.abaenglish.videoclass.presentation.base.custom.ABATextView;
import com.abaenglish.videoclass.presentation.shell.MenuActivity;
import com.crashlytics.android.Crashlytics;

import java.util.ArrayList;
import java.util.List;

public class LevelSelectionFragment extends ABAMasterFragment {

    private LevelsAdapter mItemAdapter;
    private List<ABALevel> mItems = new ArrayList<>();
    private ABATextView topBarText;
    private ListView list;


    @Override
    protected int onResLayout() {
        return R.layout.fragment_level_selection;
    }

    @Override
    protected void onConfigView() {
        topBarText = $(R.id.top_text);
        list = $(R.id.list);
        list.setEnabled(true);
        if (APIManager.isTabletSize(getContext())) {
            list.setPadding((int) getResources().getDimension(R.dimen.padding10), (int) getResources().getDimension(R.dimen.padding10), (int) getResources().getDimension(R.dimen.padding10), (int) getResources().getDimension(R.dimen.padding10));
        }

        if (getActivity() instanceof SelectLevelAcivity) {
            topBarText.setText(getString(R.string.chooseLevelFirstCommentKey));
            list.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.abaLightGrey));
        } else {
            topBarText.setText(getString(R.string.chooseLevelCommentKey));
            list.setBackgroundResource(R.mipmap.level_units_background);
        }

        configLevelsList();
        Crashlytics.log(Log.INFO, "Levels", "User opens Level view");
    }

    @Override
    protected void onConfigToolbar(ABATextView toolbarTitle, ABATextView toolbarSubTitle, ImageButton toolbarButton) {
        if (getActivity() instanceof SelectLevelAcivity) {
            toolbarTitle.setText(getString(R.string.chooseLevelKey));
            toolbarTitle.setTextColor(ContextCompat.getColor(getContext(), R.color.abaWhite));
            toolbarTitle.setGravity(Gravity.CENTER);
            toolbarSubTitle.setVisibility(View.GONE);
            toolbarButton.setVisibility(View.GONE);
        } else {
            toolbarTitle.setText(getString(R.string.menuKey));
            toolbarSubTitle.setText(getString(R.string.levelsKey));
        }
    }

    public void configLevelsList() {
        mItems = DataStore.getInstance().getLevelUnitController().getAllLevelsDescending(getRealm());
        mItemAdapter = new LevelsAdapter(getContext(), getRealm(), mItems, shouldShowLevelCEFR());

        View.OnClickListener infoListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mItemAdapter.isEnableToClick()) {
                    int openPosition = (Integer) v.getTag();
                    if (mItemAdapter.getOpenPosition() == openPosition) {
                        mItemAdapter.setOpenPosition(-1);
                    } else {
                        mItemAdapter.setOpenPosition(openPosition);
                    }
                    mItemAdapter.notifyDataSetChanged();
                }
            }
        };

        final View.OnClickListener cellListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (list.isEnabled()) {
                    list.setEnabled(false);
                    getABAActivity().showProgressDialog(ABAProgressDialog.SimpleDialog);

                    final int selectedPosition = (Integer) v.getTag();
                    final ABALevel selectedLevel = mItems.get(selectedPosition);
                    final ABALevel currentLevel = DataStore.getInstance().getUserController().getCurrentUser(getRealm()).getCurrentLevel();

                    if (currentLevel.getIdLevel().equals(selectedLevel.getIdLevel())) {
                        selectThisLevel(currentLevel, selectedLevel, selectedPosition);
                    } else {
                        mContentService.setCurrentLevel(getActivity(), selectedLevel, new DataController.ABASimpleCallback() {
                            @Override
                            public void onSuccess() {
                                if (getActivity() != null) {
                                    selectThisLevel(currentLevel, selectedLevel, selectedPosition);
                                    list.setEnabled(true);
                                }
                            }

                            @Override
                            public void onError(ABAAPIError error) {
                                if (error != null && getActivity() != null) {
                                    getABAActivity().dismissProgressDialog();
                                    error.showABANotificationError(getABAActivity());
                                }
                                list.setEnabled(true);
                            }
                        });
                    }
                }
            }
        };
        mItemAdapter.setInfoListener(infoListener);
        mItemAdapter.setCellListener(cellListener);
        list.setAdapter(mItemAdapter);
    }

    public void selectThisLevel(ABALevel fromLevel, ABALevel toLevel, int selectedPosition) {
        LegacyTrackingManager.getInstance().trackEvent(getRealm(), "level_change", "account", "cambio de nivel desde el menú");
        getABAActivity().dismissProgressDialog();

        MixpanelTrackingManager.getSharedInstance(getActivity()).trackLevelChange(fromLevel, toLevel);
        FabricTrackingManager.trackLevelChange(fromLevel, toLevel);
        CooladataProfileTrackingManager.trackLevelChange(UserController.getInstance().getCurrentUser(getRealm()).getUserId(), toLevel.getIdLevel());
        tracker.trackChangeLevel(toLevel.getIdLevel());

        if (mItemAdapter.getSelectedPosition() != selectedPosition) {
            mItemAdapter.setSelectedPosition(selectedPosition);
        }
        mItemAdapter.setOpenPosition(-1);
        mItemAdapter.notifyDataSetChanged();

        if (getActivity() instanceof SelectLevelAcivity) {
            Intent intent = new Intent(getActivity(), MenuActivity.class);
            intent.putExtra(MenuActivity.EXTRA_FRAGMENT, MenuActivity.FragmentType.kCurso);
            intent.putExtra(MenuActivity.USER_TYPE, MenuActivity.UserType.kUserRegister);
            startActivity(intent);
            getActivity().finish();
        } else {
            if (getActivity() != null && !getActivity().isFinishing()) {
                ((MenuActivity) getActivity()).selectItem(MenuActivity.FragmentType.kCurso);
            }
        }
    }

    private boolean shouldShowLevelCEFR() {
        return true;
    }
}
