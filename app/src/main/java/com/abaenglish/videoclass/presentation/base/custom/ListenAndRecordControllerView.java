package com.abaenglish.videoclass.presentation.base.custom;

import android.annotation.TargetApi;
import android.content.Context;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Handler;
import android.os.SystemClock;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Chronometer;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.abaenglish.shepherd.ABAShepherdEditor;
import com.abaenglish.shepherd.plugin.plugins.ShepherdAutomatorPlugin;
import com.abaenglish.videoclass.ABAApplication;
import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.data.persistence.ABAPhrase;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.abaenglish.videoclass.presentation.base.ABAMasterActivity;
import com.abaenglish.videoclass.presentation.base.AudioController;
import com.abaenglish.videoclass.presentation.section.SectionType;
import com.bzutils.images.RoundedImageView;

import java.io.IOException;

import javax.inject.Inject;

/**
 * Created by madalin on 18/05/15.
 */
public class ListenAndRecordControllerView extends LinearLayout implements View.OnClickListener, Chronometer.OnChronometerTickListener, AudioController.PlayerControllerInterface {

    public interface SectionControlsListener {
        void OnPausedSection();
    }

    public interface PlayerControlsListener {
        void onListened();

        void onRecorded();

        void onCompared();

        void finishCompared();

        void onRetryPhrase();
    }

    private static String maxTimeForRec = "00:31";

    private boolean isControllerPlay;

    private RoundedImageView circleLeftBackground;
    private RoundedImageView circleCenterBackground;
    private RoundedImageView circleRightBackground;
    private RoundedImageView recTimeIcon;
    private Chronometer recTime;
    private ImageView iconLeft;
    private ImageView iconCenter;
    private ImageView iconRight;
    private ImageView recCircleAnimation;
    private RelativeLayout rightButton;
    private RelativeLayout centerButton;
    private RelativeLayout leftButton;
    private LinearLayout rightControllerView;
    private LinearLayout leftControllerView;
    private LinearLayout timeControllerView;
    private ABATextView leftText;
    private ABATextView centerText;
    private ABATextView rightText;
    private Animation speakAnimation;
    private Animation recAnimation;

    private Handler handler = new Handler();

    public enum PlayerControllerStatus {
        kWaitForRecording,
        kRecording,
        kWaitForPlaying,
        kPlaying,
        kComparingMyVoice,
        kComparingOriginal,
        kFinishedComparing,
        kWaiting
    }

    public enum PlayerControllerBehaviour {
        kListenRecord,
        kListenRecordCompare,
    }

    /**
     * Controla el estado de reproducción
     */
    private PlayerControllerStatus currentStatus;

    private PlayerControllerBehaviour behaviour;
    private SectionType sectionType;

    /**
     * La frase que estamos gestionando en el momento
     */
    private ABAPhrase currentPhrase;

    private ABAUnit currentUnit;

    private PlayerControlsListener playerControlsListener;

    private PlayerControlsListener currentSectionPlayerControlsListener;

    private SectionControlsListener sectionControlsListener;

    private Context aContext;

    @Inject
    protected FontCache fontCache;

    public ListenAndRecordControllerView(Context context) {
        super(context);
        aContext = context;
        configView();
    }

    public ListenAndRecordControllerView(Context context, AttributeSet attrs) {
        super(context, attrs);
        aContext = context;
        configView();
    }

    public ListenAndRecordControllerView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        aContext = context;
        configView();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ListenAndRecordControllerView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        aContext = context;
        configView();
    }

    private void configView() {
        inflate(getContext(), R.layout.view_listenrecord_controller, this);

        ABAApplication.get().getApplicationComponent().inject(this);

        currentStatus = PlayerControllerStatus.kWaiting;
        isControllerPlay = false;
        leftButton = (RelativeLayout) findViewById(R.id.leftButton);
        centerButton = (RelativeLayout) findViewById(R.id.centerButton);
        rightButton = (RelativeLayout) findViewById(R.id.rightButton);
        rightControllerView = (LinearLayout) findViewById(R.id.rightButtonView);
        leftControllerView = (LinearLayout) findViewById(R.id.leftButtonView);
        timeControllerView = (LinearLayout) findViewById(R.id.timeView);
        leftText = (ABATextView) findViewById(R.id.leftText);
        leftText.setTypeface(fontCache.getTypeface(FontCache.ABATypeface.sans500));

        centerText = (ABATextView) findViewById(R.id.centerText);
        centerText.setTypeface(fontCache.getTypeface(FontCache.ABATypeface.sans500));

        rightText = (ABATextView) findViewById(R.id.rightText);
        rightText.setTypeface(fontCache.getTypeface(FontCache.ABATypeface.sans500));

        iconLeft = (ImageView) findViewById(R.id.icon_left);
        iconCenter = (ImageView) findViewById(R.id.icon_center);
        iconRight = (ImageView) findViewById(R.id.icon_right);
        circleLeftBackground = (RoundedImageView) findViewById(R.id.circle_left_background);
        circleCenterBackground = (RoundedImageView) findViewById(R.id.circle_center_background);
        circleRightBackground = (RoundedImageView) findViewById(R.id.circle_right_background);
        recTime = (Chronometer) findViewById(R.id.recordTime);
        recTimeIcon = (RoundedImageView) findViewById(R.id.recordTimeIcon);
        recCircleAnimation = (ImageView) findViewById(R.id.recCircleAnimation);

        recTime.setOnChronometerTickListener(this);
        speakAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.speak_animation);
        recAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.rec_anim);
        setPhaseListenerView();

        //Enable listenAndController Button if PermissionRecorded has Granted
        if (((ABAMasterActivity) aContext).hasAllPermisssion()) {
            leftButton.setOnClickListener(this);
            centerButton.setOnClickListener(this);
            rightButton.setOnClickListener(this);
        } else {
            centerButton.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    enableCenterButtonClickListener();
                }
            });
        }

        DataStore.getInstance().getAudioController().addPlayerControllerListener(this);
    }

    public void hideLeftButtonView() {
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.leftButtonView);
        if (linearLayout.getVisibility() == VISIBLE) {
            setVisibility(GONE);
        } else {
            setVisibility(VISIBLE);
        }
    }

    public void hideCenterButtonView() {
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.centerButtonView);
        if (linearLayout.getVisibility() == VISIBLE) {
            setVisibility(GONE);
        } else {
            setVisibility(VISIBLE);
        }
    }

    public void hideRightButtonView() {
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.rightButtonView);
        if (linearLayout.getVisibility() == VISIBLE) {
            setVisibility(GONE);
        } else {
            setVisibility(VISIBLE);
        }
    }

    public void hideTimeView() {
        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.timeView);
        if (linearLayout.getVisibility() == VISIBLE) {
            setVisibility(GONE);
        } else {
            setVisibility(VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.centerButton && currentStatus != PlayerControllerStatus.kPlaying) {
            switch (currentStatus) {
                case kWaitForPlaying:
                case kWaiting:
                    startListenCurrentPhrase();
                    break;
                case kWaitForRecording:
                    startRecording();
                    break;
                case kRecording:
                    stopRecording();
                    break;
                case kFinishedComparing:
                    if (getPlayerControlsListener() != null) {
                        getPlayerControlsListener().onCompared();
                    }
                    break;
                default:
                    break;
            }
        } else if (v.getId() == R.id.leftButton) {
            if (getPlayerControlsListener() != null) {
                getPlayerControlsListener().onRetryPhrase();
                startListenCurrentPhrase();
            }
        } else if (v.getId() == R.id.rightButton) {
            startCompare();
        }
    }

    public void startListenCurrentPhrase() {
        if (((ABAMasterActivity) aContext).checkAllPermissions()) {
            setPhaseListenerView();
            DataStore.getInstance().getAudioController().playPhrase(aContext, getCurrentPhrase(), getCurrentUnit());
            isControllerPlay = true;
        }
    }

    public void startListenRecordedPhrase() {
        setPhaseListenerView();
        startListenPhrase(getCurrentPhrase());

        try {
            DataStore.getInstance().getAudioController().playRecordedPhrase(getCurrentPhrase(), getCurrentUnit(), new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    phraseListened();
                }
            });
        } catch (IOException e) {
            phraseListened();
            e.printStackTrace();
        }
    }

    private void stopRecording() {
        centerButton.setOnClickListener(null);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                recTime.stop();
                DataStore.getInstance().getAudioController().stopRecording();
                if (playerControlsListener != null) {
                    playerControlsListener.onRecorded();
                }
                recTime.setText("00:00");
            }
        }, 200);

    }

    public void startCompare() {
        currentStatus = PlayerControllerStatus.kComparingMyVoice;
        setPhaseCompareView();
        DataStore.getInstance().getAudioController().comparePhrases(aContext, getCurrentPhrase(), getCurrentUnit());
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            recCircleAnimation.setVisibility(VISIBLE);
            recCircleAnimation.setAnimation(recAnimation);
        }
    };

    private void recAnimation() {
        recCircleAnimation.setVisibility(GONE);
        recCircleAnimation.setAnimation(null);
        handler.postDelayed(runnable, 500);
    }

    private void setPhaseListenerView() {
        circleCenterBackground.setImageResource(R.drawable.circle_blue);
        iconCenter.setImageResource(R.mipmap.escuchariconbutton);
        centerText.setText(getCaption());
        leftControllerView.setVisibility(GONE);
        rightControllerView.setVisibility(GONE);
    }

    private void setPhaseSpeakView() {
        circleCenterBackground.setImageResource(R.drawable.red_record_circle);
        iconCenter.setImageResource(R.mipmap.rec);
        centerText.setText(getResources().getString(R.string.sectionSpeakTitleKey).toUpperCase());
        timeControllerView.setVisibility(VISIBLE);
        startRec();
        recAnimation();
    }

    private void setPhaseCompareView() {
        circleCenterBackground.setImageResource(R.drawable.circle_blue);
        iconCenter.setImageResource(R.mipmap.compare_center);
        centerText.setText(getResources().getString(R.string.sectionSpeakCompareLabelKey).toUpperCase());
        timeControllerView.setVisibility(GONE);
        leftControllerView.setVisibility(GONE);
        rightControllerView.setVisibility(GONE);
        recAnimation();
    }

    private void setPhaseContinueView() {
        circleCenterBackground.setImageResource(R.drawable.circle_blue);
        iconCenter.setImageResource(R.mipmap.continuar);
        centerText.setText(getResources().getString(R.string.sectionSpeakPlayLContinueKey).toUpperCase());
        leftControllerView.setVisibility(VISIBLE);
        rightControllerView.setVisibility(VISIBLE);
        recCircleAnimation.setVisibility(GONE);
        recCircleAnimation.setAnimation(null);
    }

    private void startRec() {
        recTimeIcon.setAnimation(speakAnimation);
        recTime.setBase(SystemClock.elapsedRealtime());
        recTime.start();

        if (ABAShepherdEditor.isInternal() && ShepherdAutomatorPlugin.isAutomationEnabled(aContext)) {
            ShepherdAutomatorPlugin.automateMethod(aContext, new ShepherdAutomatorPlugin.ABAShepherdAutomatorPluginAction() {
                @Override
                public void automate() {
                    stopRecording();
                }
            });
        }
    }

    public void prepareForRecord() {
        recTime.stop();
        recCircleAnimation.setVisibility(GONE);
        recCircleAnimation.setAnimation(null);
        timeControllerView.setVisibility(GONE);
        currentStatus = PlayerControllerStatus.kWaitForRecording;
        circleCenterBackground.setImageResource(R.drawable.circle_blue);
        iconCenter.setImageResource(R.mipmap.rec);
        centerText.setText(getCaption());
    }

    public void prepareForListen() {
        recTime.stop();
        currentStatus = PlayerControllerStatus.kWaitForPlaying;
        recCircleAnimation.setVisibility(GONE);
        recCircleAnimation.setAnimation(null);
        timeControllerView.setVisibility(GONE);
        circleCenterBackground.setImageResource(R.drawable.circle_blue);
        centerText.setText(getCaption());
    }


    public void startRecording() {
        centerButton.setOnClickListener(null);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (((ABAMasterActivity) aContext).hasAllPermisssion()) {
                    DataStore.getInstance().getAudioController().recordPhrase(getCurrentPhrase(), currentUnit);
                }
                circleCenterBackground.setImageResource(R.drawable.red_record_circle);
                iconCenter.setImageResource(R.mipmap.rec);
                centerText.setText(getCaption());
                timeControllerView.setVisibility(VISIBLE);
                startRec();
                recAnimation();
            }
        }, 1000);
    }

    public void startCompleteMode() {
        setPhaseListenerView();
        centerButton.setOnClickListener(null);
    }

    private String getCaption() {

        if (sectionType == null) {
            return getResources().getString(R.string.listenButtonSpeakVocKey).toUpperCase();
        } else {
            switch (sectionType) {
                case kInterpreta: {
                    switch (currentStatus) {
                        case kWaiting:
                        case kPlaying:
                        case kWaitForPlaying: {
                            return getResources().getString(R.string.listenButtonAssesKey).toUpperCase();
                        }
                        case kWaitForRecording: {
                            return getResources().getString(R.string.startRecordingButtonKey).toUpperCase();
                        }
                        case kRecording: {
                            return getResources().getString(R.string.stopRecordingButtonKey).toUpperCase();
                        }
                        default:
                            return "";
                    }
                }
                case kHabla:
                case kVocabulario: {
                    switch (currentStatus) {
                        case kPlaying:
                        case kWaitForPlaying: {
                            return getResources().getString(R.string.listenButtonSpeakVocKey).toUpperCase();
                        }
                        case kRecording: {
                            return getResources().getString(R.string.stopRecordingButtonKey).toUpperCase();
                        }
                        default: {
                            return getResources().getString(R.string.listenButtonSpeakVocKey).toUpperCase();
                        }
                    }
                }
                default: {
                    // TODO: Must implement captions for other sections
                    return getResources().getString(R.string.listenButtonSpeakVocKey).toUpperCase();
                }
            }
        }
    }


    @Override
    public void onChronometerTick(Chronometer chronometer) {
        if (chronometer.getText().toString().equals(maxTimeForRec) && currentStatus == PlayerControllerStatus.kRecording) {
            String seconds = maxTimeForRec.substring(maxTimeForRec.length() - 2);
            recTime.setText(maxTimeForRec.replace(seconds, String.valueOf(Integer.valueOf(seconds) - 1)));
            chronometer.stop();
            stopRecording();
        }
    }

    public boolean getIsControllerPlay() {
        return this.isControllerPlay;
    }

    @Override
    public void startListenPhrase(ABAPhrase phrase) {
        if (currentStatus == PlayerControllerStatus.kComparingMyVoice || currentStatus == PlayerControllerStatus.kComparingOriginal)
            return;

        recAnimation();

        currentStatus = PlayerControllerStatus.kPlaying;
    }

    @Override
    public void phraseListened() {

        if (currentStatus == PlayerControllerStatus.kComparingMyVoice) {
            currentStatus = PlayerControllerStatus.kComparingOriginal;
            return;
        }

        if (currentStatus == PlayerControllerStatus.kComparingOriginal) {
            phraseCompared();
            return;
        }

        if (getPlayerControlsListener() != null) {
            getPlayerControlsListener().onListened();
        }

        if (behaviour == PlayerControllerBehaviour.kListenRecord) {
            currentStatus = PlayerControllerStatus.kWaiting;
            recCircleAnimation.setVisibility(GONE);
            recCircleAnimation.setAnimation(null);
        } else {
            setPhaseSpeakView();

            if (((ABAMasterActivity) aContext).hasAllPermisssion()) {
                DataStore.getInstance().getAudioController().recordPhrase(getCurrentPhrase(), currentUnit);
            }
        }
    }

    @Override
    public void startRecordPhrase(ABAPhrase phrase) {
        currentStatus = PlayerControllerStatus.kRecording;

        centerText.setText(getCaption());

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                centerButton.setOnClickListener(ListenAndRecordControllerView.this);
            }
        }, 500);
    }

    @Override
    public void phraseRecorded() {

        if (behaviour == PlayerControllerBehaviour.kListenRecord) {
            currentStatus = PlayerControllerStatus.kWaiting;
        } else {
            currentStatus = PlayerControllerStatus.kComparingMyVoice;
        }

        centerButton.setOnClickListener(this);
    }

    @Override
    public void startPhraseCompare(ABAPhrase phrase) {

    }

    @Override
    public void phraseCompared() {
        setPhaseContinueView();
        currentStatus = PlayerControllerStatus.kFinishedComparing;
        getPlayerControlsListener().finishCompared();
        isControllerPlay = false;


        if (ABAShepherdEditor.isInternal() && ShepherdAutomatorPlugin.isAutomationEnabled(aContext)) {
            ShepherdAutomatorPlugin.automateMethod(aContext, new ShepherdAutomatorPlugin.ABAShepherdAutomatorPluginAction() {
                @Override
                public void automate() {
                    if (getPlayerControlsListener() != null) {
                        getPlayerControlsListener().onCompared();
                    }
                }
            });
        }
    }

    @Override
    public void listenError(AudioController.AudioControllerError error) {

        int errorKey = 0;

        switch (error) {
            case kAudioControllerErrorAlreadyPlaying: {
                errorKey = R.string.audioPlayerAlreadyPlayingErrorKey;
                break;
            }
            case kAudioControllerNotEnoughSpaceError: {
                errorKey = R.string.audioPlayerNotEnoughSpaceErrorKey;
                break;
            }
            case kAudioControllerDownloadError: {
                errorKey = R.string.audioPlayerDownloadErrorKey;
                break;
            }
            case kAudioControllerBadAudioFileError: {
                errorKey = R.string.audioPlayerBadAudioFileErrorKey;
                break;
            }
            case kAudioControllerLibraryFailure: {
                errorKey = R.string.audioPlayerBadAudioFileErrorKey;
                break;
            }
        }
        ABAErrorNotification errorNotification = new ABAErrorNotification(getContext(), ABAErrorNotification.ABAErrorNotificationType.ErrorNotification);
        errorNotification.setText(getResources().getString(errorKey));
        errorNotification.show();
    }

    public ABAPhrase getCurrentPhrase() {
        return currentPhrase;
    }

    public void setCurrentPhrase(ABAPhrase currentPhrase) {
        this.currentPhrase = currentPhrase;
    }

    public ABAUnit getCurrentUnit() {
        return currentUnit;
    }

    public void setCurrentUnit(ABAUnit currentUnit) {
        this.currentUnit = currentUnit;
    }

    public PlayerControlsListener getPlayerControlsListener() {
        return playerControlsListener;
    }

    public void setPlayerControlsListener(PlayerControlsListener playerControlsListener) {
        this.playerControlsListener = playerControlsListener;
        this.currentSectionPlayerControlsListener = playerControlsListener;
    }

    public void setSectionControlsListener(SectionControlsListener sectionControlsListener) {
        this.sectionControlsListener = sectionControlsListener;
    }

    public SectionControlsListener getSectionControlsListener() {
        return this.sectionControlsListener;
    }

    public PlayerControllerBehaviour getBehaviour() {
        return behaviour;
    }

    public void setBehaviour(PlayerControllerBehaviour behaviour) {
        this.behaviour = behaviour;
    }

    public SectionType getSectionType() {
        return sectionType;
    }

    public void setSectionType(SectionType sectionType) {
        this.sectionType = sectionType;
    }

    public void unregisterListeners() {
        DataStore.getInstance().getAudioController().removePlayerControllerListener();
        this.playerControlsListener = null;
        DataStore.getInstance().getAudioController().stopAudioController();
        DataStore.getInstance().getAudioController().stopOnCompletionListener();
    }

    public void restoreListenersForCurrentSection() {
        DataStore.getInstance().getAudioController().addPlayerControllerListener(this);
        this.playerControlsListener = this.currentSectionPlayerControlsListener;
    }

    public void enableCenterButtonClickListener() {
        if (((ABAMasterActivity) aContext).checkAllPermissions()) {
            centerButton.setOnClickListener(this);
        }
    }

    public void disableCenterButtonClickListener() {
        centerButton.setOnClickListener(null);
    }

    public void resetControlerStateForCurrentPhrase() {
        unregisterListeners();
        removeAllViews();
        handler.removeCallbacks(runnable);
        configView();
        restoreListenersForCurrentSection();
    }
}
