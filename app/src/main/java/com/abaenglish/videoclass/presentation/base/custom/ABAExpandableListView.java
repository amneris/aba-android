package com.abaenglish.videoclass.presentation.base.custom;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.Transformation;
import android.widget.AbsListView;
import android.widget.BaseExpandableListAdapter;
import android.widget.ExpandableListView;

import com.abaenglish.videoclass.presentation.unit.UnitAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ABAExpandableListView extends ExpandableListView {

    private static final int ANIM_GROUP_TYPE = 0;
    private static final int SAMPLE_GROUP_TYPE = 1;
    private static List<Integer> mChildTypes =  Arrays.asList(ANIM_GROUP_TYPE, SAMPLE_GROUP_TYPE);

    private static ExpandableType currentExpandableType = ExpandableType.STATE_IDLE;
    private static ExpandableAnimationListener expandableAnimationListener;
    private static int collapsedGroup;

    public static final int ANIMATION_DURATION = 500;

    private enum ExpandableType {
        STATE_IDLE(0),
        STATE_EXPANDING(1),
        STATE_COLLAPSING(2);

        private int value;

        ExpandableType(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }
    }

    public interface ExpandableAnimationListener {
        void onFinished();
    }

    private ABAExpandableListAdapter adapter;

    public ABAExpandableListView(Context context) {
        super(context);
    }

    public ABAExpandableListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ABAExpandableListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setExpandibleAdapter(ABAExpandableListAdapter adapter) {
        super.setAdapter(adapter);
        this.adapter = adapter;
    }

    public void expandGroupWithAnimation(int groupPos) {
        currentExpandableType = ExpandableType.STATE_EXPANDING;
        expandGroup(groupPos);
    }

    public void collapseGroupWithAnimation(int groupPos, ExpandableAnimationListener expandableAnimationListener) {
        currentExpandableType = ExpandableType.STATE_COLLAPSING;
        this.expandableAnimationListener = expandableAnimationListener;
        this.collapsedGroup = groupPos;
        adapter.notifyDataSetChanged();
    }

    // ================================================================================
    //  Abstract ABAExpandableAdapter For ABAExpandableListView
    // ================================================================================

    public static abstract class ABAExpandableListAdapter extends BaseExpandableListAdapter {

        public abstract View getRealChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent);

        public abstract int getRealChildrenCount(int groupPosition);

        @Override
        public final int getChildrenCount(int groupPosition) {
            if (currentExpandableType != ExpandableType.STATE_IDLE)
                return 1;
            return getRealChildrenCount(groupPosition);
        }

        @Override
        public final int getChildType(int groupPosition, int childPosition) {
            if (currentExpandableType != ExpandableType.STATE_IDLE)
                return ANIM_GROUP_TYPE;
            return SAMPLE_GROUP_TYPE;
        }

        @Override
        public final int getChildTypeCount() {
            return mChildTypes.size();
        }

        @Override
        public final View getChildView(final int groupPosition, int childPosition, boolean isLastChild, View convertView, final ViewGroup parent) {
            //If Group Is not Animation
            if (currentExpandableType == ExpandableType.STATE_IDLE)
                return getRealChildView(groupPosition, childPosition, isLastChild, convertView, parent);

            convertView = new DummyView(parent.getContext());
            convertView.setLayoutParams(new AbsListView.LayoutParams(LayoutParams.MATCH_PARENT, 0));

            final ExpandableListView listView = (ExpandableListView) parent;
            final DummyView dummyView = (DummyView) convertView;

            dummyView.clearViews();
            dummyView.setDivider(listView.getDivider(), parent.getMeasuredWidth(), listView.getDividerHeight());

            int totalHeight = 0;
            
            for (int i = 0; i < getRealChildrenCount(groupPosition); i++) {
                View childView = getRealChildView(groupPosition, i, (i == getRealChildrenCount(groupPosition) - 1), null, parent);
                childView.setLayoutParams(new AbsListView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

                if (childView.getLayoutParams().height > 0)
                    childView.measure(MeasureSpec.makeMeasureSpec(parent.getWidth(), MeasureSpec.EXACTLY), MeasureSpec.makeMeasureSpec(childView.getLayoutParams().height, MeasureSpec.EXACTLY));
                else
                    childView.measure(MeasureSpec.makeMeasureSpec(parent.getWidth(), MeasureSpec.EXACTLY), MeasureSpec.makeMeasureSpec(0, MeasureSpec.UNSPECIFIED));

                totalHeight += childView.getMeasuredHeight();
                dummyView.addFakeView(childView);
                if(totalHeight >= listView.getHeight())
                    break;
            }

            final ExpandableType currentState = currentExpandableType;
            AnimationListener animationListener = new AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    currentExpandableType = ExpandableType.STATE_IDLE;
                    if (currentState == ExpandableType.STATE_COLLAPSING) {
                        listView.collapseGroup(groupPosition);
                        ((UnitAdapter) listView.getExpandableListAdapter()).setSelectedLevel(null);
                        if (expandableAnimationListener != null)
                            expandableAnimationListener.onFinished();
                    }
                    notifyDataSetChanged();
                }

                @Override
                public void onAnimationRepeat(Animation animation) {
                }
            };

            if (currentExpandableType == ExpandableType.STATE_EXPANDING) {
                //Start Expandable Animation
                dummyView.startAnimation(new ExpandAnimation().ExpandCollapseAnimation(dummyView, 0, totalHeight, animationListener));

            } else if (currentExpandableType == ExpandableType.STATE_COLLAPSING) {
                //Start Collapse Animation
                dummyView.startAnimation(new ExpandAnimation().ExpandCollapseAnimation(dummyView, totalHeight, 0, animationListener));
            }
            return convertView;
        }
    }

    // ================================================================================
    //  Class DummyView targetExpandableView
    // ================================================================================

    private static class DummyView extends View {
        private List<View> views = new ArrayList<View>();
        private Drawable divider;
        private int dividerWidth;
        private int dividerHeight;

        public DummyView(Context context) {
            super(context);
        }

        @Override
        public void dispatchDraw(Canvas canvas) {
            canvas.save();
            if (divider != null)
                divider.setBounds(0, 0, dividerWidth, dividerHeight);

            for (View view : views) {
                canvas.save();
                canvas.clipRect(0, 0, getWidth(), view.getMeasuredHeight());
                view.draw(canvas);
                canvas.restore();

                if (divider != null) {
                    divider.draw(canvas);
                    canvas.translate(0, dividerHeight);
                }
                canvas.translate(0, view.getMeasuredHeight());
            }
            canvas.restore();
        }

        public void setDivider(Drawable divider, int dividerWidth, int dividerHeight) {
            if (divider != null) {
                this.divider = divider;
                this.dividerWidth = dividerWidth;
                this.dividerHeight = dividerHeight;

                divider.setBounds(0, 0, dividerWidth, dividerHeight);
            }
        }

        public void addFakeView(View childView) {
            childView.layout(0, 0, childView.getMeasuredWidth(), childView.getMeasuredHeight());
            views.add(childView);
        }

        public void clearViews() {
            views.clear();
        }
    }

    // ================================================================================
    //  Class for Expand/Collapsed Animation
    // ================================================================================

    private static class ExpandAnimation extends Animation {
        private int startHeight;
        private int targetHeight;
        private View view;

        private ExpandAnimation() {
        }

        private ExpandAnimation(View view, int startHeight, int targetHeight) {
            this.startHeight = startHeight;
            this.targetHeight = targetHeight;
            this.view = view;

            view.getLayoutParams().height = startHeight;
            view.requestLayout();
        }

        private ExpandAnimation ExpandCollapseAnimation(View view, int startHeight, int targetHeight, AnimationListener animationListener) {
            ExpandAnimation expandAnimation = new ExpandAnimation(view, startHeight, targetHeight);
            expandAnimation.setDuration(ANIMATION_DURATION);
            expandAnimation.setFillAfter(true);
            expandAnimation.setAnimationListener(animationListener);
            return expandAnimation;
        }

        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            super.applyTransformation(interpolatedTime, t);
            view.getLayoutParams().height = (int) (startHeight + (targetHeight - startHeight) * interpolatedTime);
            view.requestLayout();
        }
    }
}