package com.abaenglish.videoclass.presentation.shell;

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.analytics.TrackerContract;
import com.abaenglish.videoclass.analytics.cooladata.navigation.CooladataNavigationTrackingManager;
import com.abaenglish.videoclass.analytics.helpers.GenericTrackingInterface;
import com.abaenglish.videoclass.analytics.legacy.LegacyTrackingManager;
import com.abaenglish.videoclass.data.persistence.ABAUser;
import com.abaenglish.videoclass.domain.content.UserController;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.abaenglish.videoclass.helpdesk.ZendeskHelpCenterFragment;
import com.abaenglish.videoclass.presentation.abaMoment.ABAMomentsListFragment_;
import com.abaenglish.videoclass.presentation.base.ABAMasterActivity;
import com.abaenglish.videoclass.presentation.base.ABAMasterFragment;
import com.abaenglish.videoclass.presentation.base.custom.ABATextView;
import com.abaenglish.videoclass.presentation.base.custom.FontCache;
import com.abaenglish.videoclass.presentation.certificate.CertificatesFragment;
import com.abaenglish.videoclass.presentation.course.DashboardFragment;
import com.abaenglish.videoclass.presentation.level.LevelSelectionFragment;
import com.abaenglish.videoclass.presentation.plan.ImprovedPlansFragment;
import com.abaenglish.videoclass.presentation.plan.ImprovedPlansFragment_;
import com.abaenglish.videoclass.presentation.plan.PlansFragment;
import com.abaenglish.videoclass.presentation.profile.ProfileFragmentNew;
import com.abaenglish.videoclass.presentation.shell.SlideMenuAdapter.SlideMenuItem;
import com.bzutils.images.RoundedImageView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.ArrayList;

/**
 * Created by vmadalin on 27/1/16.
 * Refactored by Jesus on 01/04/2016.
 */
public class SlideMenuFragment extends ABAMasterFragment implements View.OnClickListener, AdapterView.OnItemClickListener {

    public static final String SHOULD_SHOW_ABAMOMENT_TAB = "SHOULD_SHOW_ABAMOMENT_TAB";

    private RelativeLayout mMainLayout;
    private LinearLayout mProfileLayout;
    private RoundedImageView mProfileImageView;
    private ABATextView mProfileNameTextView;
    private LinearLayout mPremiumLinearLayout;
    private Button mPremiumButton;
    private ListView mListViewMenu;
    private SlideMenuAdapter mMenuSlideAdapter;
    private MenuActivity mMenuActivity;

    private ArrayList<SlideMenuItem> mMenuItemsList = new ArrayList<>();
    private ABAUser mCurrentUser;
    private boolean shouldShowMoments;

    private TrackerContract.Tracker tracker;

    // ================================================================================
    // Lifecycle methods
    // ================================================================================

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            shouldShowMoments = getArguments().getBoolean(SHOULD_SHOW_ABAMOMENT_TAB, true);
        }
    }

    @Override
    protected int onResLayout() {
        return R.layout.fragment_slide_menu;
    }

    @Override
    protected void onConfigView() {
        mProfileLayout = $(R.id.profile_view_menu);
        mProfileImageView = $(R.id.profile_icon_menu);
        mProfileNameTextView = $(R.id.profile_name_menu);
        mPremiumLinearLayout = $(R.id.premium_view_menu);
        mPremiumButton = $(R.id.premiumButton_menu);
        mListViewMenu = $(R.id.listview_menu);
        mMainLayout = $(R.id.slide_menu_main_layout);
        mMenuActivity = ((MenuActivity) getActivity());

        // Onclick listeners
        mMainLayout.setOnClickListener(this);
        mPremiumButton.setOnClickListener(this);
        mProfileLayout.setOnClickListener(this);

        // Reading user
        mCurrentUser = DataStore.getInstance().getUserController().getCurrentUser(getRealm());
        if (mCurrentUser != null) {
            if (DataStore.getInstance().getUserController().isUserPremium()) {
                mPremiumLinearLayout.setVisibility(View.GONE);
            }

            mProfileNameTextView.setText(mCurrentUser.getName());
            setUserProfileAvatar(mCurrentUser.getUrlImage());
        }

        // fonts
        mPremiumButton.setTypeface(fontCache.getTypeface(FontCache.ABATypeface.sans700));

        configMenuList();
        tracker = ((ABAMasterActivity) getActivity()).tracker;
    }

    @Override
    protected void onConfigToolbar(ABATextView toolbarTitle, ABATextView toolbarSubTitle, ImageButton toolbarButton) {
        //Nothing
    }

    public void setShouldShowMoments(boolean shouldShowMoments) {
        this.shouldShowMoments = shouldShowMoments;
    }

    // ================================================================================
    // Public methods
    // ================================================================================

    public void update() {
        configMenuList();
        mMenuSlideAdapter.notifyDataSetChanged();
    }

    public void selectedItemTypeFromMenu(MenuType menuType) {
        if (mMenuSlideAdapter != null && !mMenuItemsList.isEmpty()) {
            if (menuType != null) {
                for (SlideMenuItem slideMenuItem : mMenuItemsList) {
                    if (slideMenuItem.getMenuType().equals(menuType)) {
                        mMenuSlideAdapter.setMenuItemSelected(mMenuItemsList.get(mMenuItemsList.indexOf(slideMenuItem)));
                        mMenuSlideAdapter.notifyDataSetChanged();
                        return;
                    }
                }
            } else {
                mMenuSlideAdapter.setMenuItemSelected(null);
            }
        }
    }

    public void openMoments() {
        if (mMenuActivity.getSupportFragmentManager().findFragmentByTag(ABAMomentsListFragment_.class.getName()) != null) {
            mMenuActivity.toggleNavigationMenu();
        } else {

            String userId = UserController.getInstance().getCurrentUser(getRealm()).getUserId();

            Bundle args = new Bundle();
            args.putString(ABAMomentsListFragment_.USER_ID_STRING_EXTRA_KEY, userId);

            ABAMomentsListFragment_ listOfMomentFragment = new ABAMomentsListFragment_();
            listOfMomentFragment.setArguments(args);
            mMenuActivity.changeFragmentWithDelay(listOfMomentFragment, true);

            trackMenuOption(MenuType.menuABAMoment);
        }
    }

    // ================================================================================
    // Private methods
    // ================================================================================

    private void configMenuList() {
        mMenuSlideAdapter = new SlideMenuAdapter(getContext(), addMenuItemsFromUserType());
        mListViewMenu.setAdapter(mMenuSlideAdapter);
        mListViewMenu.setOnItemClickListener(this);
    }

    private ArrayList<SlideMenuItem> addMenuItemsFromUserType() {
        mMenuItemsList = new ArrayList<>();
        mMenuItemsList.add(new SlideMenuItem(getContext(), R.mipmap.icon_menu_profile, R.string.yourProfileKey, MenuType.menuProfile));
        mMenuItemsList.add(new SlideMenuItem(getContext(), R.mipmap.course_menu_icon, R.string.unitsMenuItemKey, MenuType.menuCourse));
        mMenuItemsList.add(new SlideMenuItem(getContext(), R.mipmap.levels_menu_icon, R.string.levelsMenuItemKey, MenuType.menuLevels));
        mMenuItemsList.add(new SlideMenuItem(getContext(), R.mipmap.certificates_menu_icon, R.string.yourCertsMenuItemKey, MenuType.menuCerts));

        if (shouldShowMoments) {
            mMenuItemsList.add(new SlideMenuItem(getContext(), R.mipmap.aba_logo, R.string.abaMomentSectionMenu, MenuType.menuABAMoment));
        }

        if (!DataStore.getInstance().getUserController().isUserPremium()) {
            mMenuItemsList.add(new SlideMenuItem(getContext(), R.mipmap.premium_menu_icon, R.string.programsMenuItemKey, MenuType.menuPremium));
        }

        mMenuItemsList.add(new SlideMenuItem(getContext(), R.mipmap.help_menu_icon, R.string.helpMenuItemKey, MenuType.menuHelp));

        return mMenuItemsList;
    }

    private void setUserProfileAvatar(String url) {
        if (url == null || url.equals("")) {
            return;
        }

        ImageLoader.getInstance().loadImage(url, new SimpleImageLoadingListener() {
            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                if (isAdded() && getActivity() != null) {
                    BitmapDrawable bitmapDrawable = new BitmapDrawable(getResources(), loadedImage);
                    mProfileImageView.setImageDrawable(bitmapDrawable);
                }
            }
        });
    }

    // ================================================================================
    // OnClickListener
    // ================================================================================

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.premiumButton_menu: {
                if (serverConfig.shouldShowImprovedPlansPage()) {
                    if (getActivity().getSupportFragmentManager().findFragmentByTag(ImprovedPlansFragment.class.getName()) != null) {
                        mMenuActivity.toggleNavigationMenu();
                    } else {
                        mMenuActivity.changeFragmentWithDelay(new ImprovedPlansFragment_(), true);
                        mMenuSlideAdapter.notifyDataSetChanged();
                        mMenuActivity.toggleNavigationMenu();
                    }
                } else {
                    if (getActivity().getSupportFragmentManager().findFragmentByTag(PlansFragment.class.getName()) != null) {
                        mMenuActivity.toggleNavigationMenu();
                    } else {
                        mMenuActivity.changeFragmentWithDelay(new PlansFragment(), true);
                        mMenuSlideAdapter.notifyDataSetChanged();
                        mMenuActivity.toggleNavigationMenu();
                    }
                }
                mMenuActivity.trackPricingScreenWithOrigin(GenericTrackingInterface.PlansViewControllerOriginType.kPlansViewControllerOriginBanner);
                break;
            }
            case R.id.profile_view_menu: {
                if (getActivity().getSupportFragmentManager().findFragmentByTag(ProfileFragmentNew.class.getName()) != null) {
                    mMenuActivity.toggleNavigationMenu();
                } else {
                    mMenuActivity.changeFragmentWithDelay(new ProfileFragmentNew(), true);
                    mMenuSlideAdapter.notifyDataSetChanged();
                    mMenuActivity.toggleNavigationMenu();
                }
                LegacyTrackingManager.getInstance().trackEvent(getRealm(), "account_menu", "account", "click en account");
                break;
            }
            case R.id.slide_menu_main_layout: {
                // Doing nothing to avoid clicks in the layout below
                break;
            }
            default:
                break;
        }
    }

    // ================================================================================
    // OnItemClickListener
    // ================================================================================

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        SlideMenuItem slideMenuItem = (SlideMenuItem) parent.getItemAtPosition(position);

        switch (slideMenuItem.getMenuType()) {
            case menuCourse: {
                if (mMenuActivity.getSupportFragmentManager().findFragmentByTag(DashboardFragment.class.getName()) != null) {
                    mMenuActivity.toggleNavigationMenu();
                } else {
                    mMenuActivity.changeFragmentWithDelay(new DashboardFragment(), true);
                    // Tracking menu option
                    trackMenuOption(slideMenuItem.getMenuType());
                }
                break;
            }
            case menuLevels: {
                if (mMenuActivity.getSupportFragmentManager().findFragmentByTag(LevelSelectionFragment.class.getName()) != null) {
                    mMenuActivity.toggleNavigationMenu();
                } else {
                    mMenuActivity.changeFragmentWithDelay(new LevelSelectionFragment(), true);
                    // Tracking menu option
                    trackMenuOption(slideMenuItem.getMenuType());
                }
                break;
            }
            case menuCerts: {
                if (mMenuActivity.getSupportFragmentManager().findFragmentByTag(CertificatesFragment.class.getName()) != null) {
                    mMenuActivity.toggleNavigationMenu();
                } else {
                    mMenuActivity.changeFragmentWithDelay(new CertificatesFragment(), true);
                    // Tracking menu option
                    trackMenuOption(slideMenuItem.getMenuType());
                }
                break;
            }
            case menuPremium: {
                if (mMenuActivity.getSupportFragmentManager().findFragmentByTag(PlansFragment.class.getName()) != null) {
                    mMenuActivity.toggleNavigationMenu();
                } else if (mMenuActivity.getSupportFragmentManager().findFragmentByTag(ImprovedPlansFragment.class.getName()) != null) {
                    mMenuActivity.toggleNavigationMenu();
                } else {
                    Fragment aFragment;
                    if (serverConfig.shouldShowImprovedPlansPage()) {
                        aFragment = new ImprovedPlansFragment_();
                    } else {
                        aFragment = new PlansFragment();
                    }

                    mMenuActivity.changeFragmentWithDelay(aFragment, true);
                    // Tracking menu option
                    trackMenuOption(slideMenuItem.getMenuType());
                }
                break;
            }
            case menuHelp: {
                if (mMenuActivity.getSupportFragmentManager().findFragmentByTag(ZendeskHelpCenterFragment.class.getName()) != null) {
                    mMenuActivity.toggleNavigationMenu();
                } else {
                    mMenuActivity.changeFragmentWithDelay(new ZendeskHelpCenterFragment(), true);
                    // Tracking menu option
                    trackMenuOption(slideMenuItem.getMenuType());
                }
                break;
            }
            case menuProfile: {
                if (getActivity().getSupportFragmentManager().findFragmentByTag(ProfileFragmentNew.class.getName()) != null) {
                    mMenuActivity.toggleNavigationMenu();
                } else {
                    mMenuActivity.changeFragmentWithDelay(new ProfileFragmentNew(), true);
                    // Tracking menu option
                    trackMenuOption(slideMenuItem.getMenuType());
                }
                break;
            }
            case menuABAMoment: {
                openMoments();
                break;
            }
            default:
                break;

        }
        mMenuActivity.toggleNavigationMenu();
    }


    // ================================================================================
    // Private methods - Tracking
    // ================================================================================

    private void trackMenuOption(MenuType type) {
        ABAUser user = UserController.getInstance().getCurrentUser(getRealm());
        switch (type) {
            case menuCourse:
                LegacyTrackingManager.getInstance().trackEvent(getRealm(), "course_menu", "menu", "click en menú curso");
                CooladataNavigationTrackingManager.trackEnteredCourseIndex(user.getUserId(), user.getCurrentLevel().getIdLevel());
                tracker.trackEnteredCourseIndex(user.getCurrentLevel().getIdLevel());
                break;
            case menuLevels:
                LegacyTrackingManager.getInstance().trackEvent(getRealm(), "level_menu", "menu", "click en el menú Niveles");
                break;
            case menuCerts:
                LegacyTrackingManager.getInstance().trackEvent(getRealm(), "certificates_menu", "menu", "click en menú Certificados");
                break;
            case menuPremium:
                LegacyTrackingManager.getInstance().trackEvent(getRealm(), "fullaccess_menu", "getpremium", "click en acceso completo");
                mMenuActivity.trackPricingScreenWithOrigin(GenericTrackingInterface.PlansViewControllerOriginType.kPlansViewControllerOriginMenu);
                break;
            case menuHelp:
                CooladataNavigationTrackingManager.trackOpenedHelp(user.getUserId());
                tracker.trackOpenedHelp();
                break;
            case menuProfile:
                LegacyTrackingManager.getInstance().trackEvent(getRealm(), "account_menu", "account", "click en account");
                break;
            case menuABAMoment:
                break;
            default:
                break;
        }
    }

}
