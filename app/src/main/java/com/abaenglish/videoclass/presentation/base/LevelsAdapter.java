package com.abaenglish.videoclass.presentation.base;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;

import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.data.network.APIManager;
import com.abaenglish.videoclass.data.persistence.ABALevel;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.abaenglish.videoclass.presentation.base.custom.ABATextView;

import java.util.List;

import io.realm.Realm;

/**
 * Created by Kuge on 04/04/2015.
 */
public class LevelsAdapter extends BaseAdapter implements ListAdapter {

    private Context mContext;
    private LayoutInflater mLayoutInflate;
    private List<ABALevel> levels;
    private int openPosition = -1;
    private int selectedPosition = -1;
    private View.OnClickListener cellListener;
    private View.OnClickListener infoListener;
    private boolean isEnableToClick = true;
    private boolean displayCertificate;

    public enum AnimationType {
        Expand,
        Collapse
    }

    public LevelsAdapter(Context context, Realm realm, List<ABALevel> resource, boolean displayCertificate) {
        mContext = context;
        mLayoutInflate = LayoutInflater.from(context);
        this.levels = resource;
        this.displayCertificate = displayCertificate;
        selectedPosition = resource.indexOf(DataStore.getInstance().getUserController().getCurrentUser(realm).getCurrentLevel());
    }

    @Override
    public int getCount() {
        if (levels != null)
            return levels.size();
        return 0;
    }

    @Override
    public ABALevel getItem(int position) {
        if (levels != null)
            return levels.get(position);
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {

        final ItemViewHolder mItemViewHolder;

        if (convertView == null) {
            mItemViewHolder = new ItemViewHolder();
            convertView = mLayoutInflate.inflate(R.layout.item_level_selection, parent, false);
            mItemViewHolder.mIconDifficulty = (ABATextView) convertView.findViewById(R.id.unity);
            mItemViewHolder.mDifficulty = (ABATextView) convertView.findViewById(R.id.level_difficulty);
            mItemViewHolder.mDescription = (ABATextView) convertView.findViewById(R.id.level_info);
            mItemViewHolder.mArrow = (ImageView) convertView.findViewById(R.id.arrow);
            mItemViewHolder.mLayout = (RelativeLayout) convertView.findViewById(R.id.layout);
            mItemViewHolder.mIconInfo = (ImageView) convertView.findViewById(R.id.info);
            convertView.setTag(mItemViewHolder);
        } else {
            mItemViewHolder = (ItemViewHolder) convertView.getTag();
        }


        final ABALevel item = getItem(position);

        mItemViewHolder.mIconDifficulty.setText(item.getIdLevel());
        mItemViewHolder.mDifficulty.setText(displayCertificate ? mContext.getString(mContext.getResources().getIdentifier("certificateNameLevel" + item.getIdLevel(),
                "string", mContext.getPackageName())) : item.getName());

        switch (position) {
            case 0:
                mItemViewHolder.mDescription.setText(R.string.descLevelBusiness);
                break;
            case 1:
                mItemViewHolder.mDescription.setText(R.string.descLevelAdvanced);
                break;
            case 2:
                mItemViewHolder.mDescription.setText(R.string.descLevelUIntermediate);
                break;
            case 3:
                mItemViewHolder.mDescription.setText(R.string.descLevelIntermediate);
                break;
            case 4:
                mItemViewHolder.mDescription.setText(R.string.descLevelLIntermediate);
                break;
            case 5:
                mItemViewHolder.mDescription.setText(R.string.descLevelBeginner);
                break;
        }


        if (APIManager.isTabletSize(convertView.getContext())) {
            mItemViewHolder.mIconInfo.setTag(position);
            mItemViewHolder.mIconInfo.setOnClickListener(getInfoListener());
            mItemViewHolder.mLayout.setTag(position);
            mItemViewHolder.mLayout.setOnClickListener(getCellListener());
            if (openPosition != position) {
                mItemViewHolder.mArrow.setVisibility(View.INVISIBLE);
                mItemViewHolder.mIconInfo.setBackgroundResource(R.mipmap.levels_info_blue);
            } else {
                mItemViewHolder.mDescription.setVisibility(View.VISIBLE);
                mItemViewHolder.mArrow.setVisibility(View.VISIBLE);
                mItemViewHolder.mIconInfo.setBackgroundResource(R.mipmap.levels_info_grey);
            }

            if (selectedPosition != position) {
                mItemViewHolder.mLayout.setBackgroundResource(R.color.abaWhite);
                mItemViewHolder.mDifficulty.setTextColor(ContextCompat.getColor(mContext, R.color.abaGrey));
                mItemViewHolder.mDescription.setTextColor(ContextCompat.getColor(mContext, R.color.abaGrey));
            } else {
                mItemViewHolder.mLayout.setBackgroundResource(R.color.abaGrey);
                mItemViewHolder.mDifficulty.setTextColor(ContextCompat.getColor(mContext, R.color.abaWhite));
                mItemViewHolder.mDescription.setTextColor(ContextCompat.getColor(mContext, R.color.abaWhite));
            }

        } else {
            mItemViewHolder.mIconInfo.setTag(position);
            mItemViewHolder.mIconInfo.setOnClickListener(getInfoListener());
            mItemViewHolder.mLayout.setTag(position);
            mItemViewHolder.mLayout.setOnClickListener(getCellListener());
            if (openPosition != position) {
                mItemViewHolder.mArrow.setVisibility(View.INVISIBLE);
                mItemViewHolder.mIconInfo.setBackgroundResource(R.mipmap.levels_info_blue);
                collapseAnimationFromView(mItemViewHolder.mLayout, mItemViewHolder.mDescription);
            } else {
                mItemViewHolder.mArrow.setVisibility(View.VISIBLE);
                mItemViewHolder.mIconInfo.setBackgroundResource(R.mipmap.levels_info_grey);
                expandAnimationFromView(mItemViewHolder.mLayout, mItemViewHolder.mDescription);
            }

            if (selectedPosition != position) {
                mItemViewHolder.mLayout.setBackgroundResource(R.color.abaWhite);
                mItemViewHolder.mDifficulty.setTextColor(ContextCompat.getColor(mContext, R.color.abaGrey));
                mItemViewHolder.mDifficulty.setABATag(mContext.getResources().getInteger(R.integer.typefaceSans500));
            } else {
                mItemViewHolder.mLayout.setBackgroundResource(R.color.abaGrey);
                mItemViewHolder.mDifficulty.setTextColor(ContextCompat.getColor(mContext, R.color.abaWhite));
                mItemViewHolder.mDifficulty.setABATag(mContext.getResources().getInteger(R.integer.typefaceSans900));
            }
        }

        return convertView;
    }

    public View.OnClickListener getCellListener() {
        return cellListener;
    }

    public View.OnClickListener getInfoListener() {
        return infoListener;
    }

    public void setInfoListener(View.OnClickListener infoListener) {
        this.infoListener = infoListener;
    }

    public int getOpenPosition() {
        return openPosition;
    }

    public void setOpenPosition(int openPosition) {
        this.openPosition = openPosition;
    }

    public void setCellListener(View.OnClickListener cellListener) {
        this.cellListener = cellListener;
    }

    public int getSelectedPosition() {
        return selectedPosition;
    }

    public void setSelectedPosition(int selectedPosition) {
        this.selectedPosition = selectedPosition;
    }

    public boolean isEnableToClick() {
        return isEnableToClick;
    }


    public class ItemViewHolder {
        public ABATextView mIconDifficulty;
        public ABATextView mDifficulty;
        public ABATextView mDescription;
        public RelativeLayout mLayout;
        public ImageView mIconInfo;
        public ImageView mArrow;
    }

    private void expandAnimationFromView(RelativeLayout cellContent, ABATextView targetView) {
        if (!targetView.isSelected()) {
            targetView.setSelected(true);
            targetView.startAnimation(new ExpandedViewAnimation().ExpandedCollapsedAnimation(cellContent, targetView, AnimationType.Expand));
        }
    }

    private void collapseAnimationFromView(RelativeLayout cellContent, ABATextView targetView) {
        if (targetView.isSelected()) {
            targetView.setSelected(false);
            targetView.startAnimation(new ExpandedViewAnimation().ExpandedCollapsedAnimation(cellContent, targetView, AnimationType.Collapse));
        } else {
            targetView.setSelected(false);
            targetView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0));
        }
    }

    // ================================================================================
    // Private methods - Subscription
    // ================================================================================

    private class ExpandedViewAnimation extends Animation {
        View targetView;
        int startHeight;
        int targetHeight;

        public ExpandedViewAnimation() {
        }

        public ExpandedViewAnimation(View cellView, View targetView, AnimationType animationType) {
            targetView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            if (targetView.getLayoutParams().height > 0)
                targetView.measure(View.MeasureSpec.makeMeasureSpec(cellView.getWidth(), View.MeasureSpec.EXACTLY), View.MeasureSpec.makeMeasureSpec(targetView.getLayoutParams().height, View.MeasureSpec.EXACTLY));
            else
                targetView.measure(View.MeasureSpec.makeMeasureSpec(cellView.getWidth(), View.MeasureSpec.EXACTLY), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));

            this.targetView = targetView;
            this.startHeight = 0;
            this.targetHeight = targetView.getMeasuredHeight();
            if (animationType.equals(AnimationType.Collapse)) {
                this.startHeight = targetView.getMeasuredHeight();
                this.targetHeight = 0;
            }
        }

        public ExpandedViewAnimation ExpandedCollapsedAnimation(View cellView, final View targetView, AnimationType animationType) {
            ExpandedViewAnimation expandedViewAnimation = new ExpandedViewAnimation(cellView, targetView, animationType);
            expandedViewAnimation.setDuration(350);
            expandedViewAnimation.setFillAfter(true);
            final AnimationType currentAnimationType = animationType;
            expandedViewAnimation.setAnimationListener(new AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {
                    if (currentAnimationType.equals(AnimationType.Expand))
                        targetView.setVisibility(View.VISIBLE);
                    isEnableToClick = false;
                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    if (currentAnimationType.equals(AnimationType.Collapse))
                        targetView.setVisibility(View.GONE);
                    isEnableToClick = true;
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            return expandedViewAnimation;
        }

        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            int newHeight = (int) (startHeight + (targetHeight - startHeight) * interpolatedTime);
            targetView.getLayoutParams().height = newHeight;
            targetView.requestLayout();
        }

        @Override
        public void initialize(int width, int height, int parentWidth, int parentHeight) {
            super.initialize(width, height, parentWidth, parentHeight);
        }

        @Override
        public boolean willChangeBounds() {
            return true;
        }
    }
}
