package com.abaenglish.videoclass.presentation.unit;

import android.app.ActivityOptions;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.abaenglish.shepherd.ABAShepherdEditor;
import com.abaenglish.shepherd.plugin.plugins.ShepherdEvaluationPlugin;
import com.abaenglish.videoclass.ABAApplication;
import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.analytics.legacy.LegacyTrackingManager;
import com.abaenglish.videoclass.data.ConnectionService;
import com.abaenglish.videoclass.data.network.APIManager;
import com.abaenglish.videoclass.data.persistence.ABAAPIError;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.data.persistence.ABAUser;
import com.abaenglish.videoclass.domain.LanguageController;
import com.abaenglish.videoclass.domain.ProgressService;
import com.abaenglish.videoclass.domain.content.SectionController;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.abaenglish.videoclass.presentation.base.custom.ABAProgressDialog;
import com.abaenglish.videoclass.presentation.base.custom.TeacherBannerView;
import com.abaenglish.videoclass.presentation.base.custom.UnitRoscoView;
import com.abaenglish.videoclass.presentation.plan.InitialPlansActivity;
import com.abaenglish.videoclass.presentation.section.SectionType;
import com.abaenglish.videoclass.presentation.section.SectionsService;
import com.abaenglish.videoclass.presentation.section.assessment.ABAEvaluationActivity;
import com.abaenglish.videoclass.presentation.section.exercise.ABAExercisesActivity;
import com.abaenglish.videoclass.presentation.section.interpret.ABAInterpretationActivity;
import com.abaenglish.videoclass.presentation.section.speak.ABASpeakActivity;
import com.abaenglish.videoclass.presentation.section.videoclass.ABAFilmActivity;
import com.abaenglish.videoclass.presentation.section.vocabulary.ABAVocabularyActivity;
import com.abaenglish.videoclass.presentation.section.write.ABAWriteActivity;
import com.bzutils.LogBZ;
import com.crashlytics.android.Crashlytics;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.zendesk.sdk.rating.ui.RateMyAppDialog;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;

import static com.abaenglish.videoclass.domain.content.DataController.ABASimpleCallback;
import static com.abaenglish.videoclass.presentation.section.SectionActivityInterface.EXTRA_UNIT_ID;

/**
 * Created by iaguila on 29/4/15.
 */
public class DetailUnitActivity extends DetailUnitBase {

    public static final String EXTRA_COMPLETED_SECTION = "EXTRA_COMPLETED_SECTION";
    public static String OPEN_SECTION = "OPEN_SECTION";

    // Domain
    private boolean unitRefreshed; // It will be true as soon as all the needed data to draw the rosco is properly set

    // UI
    private UnitRoscoView roscoView;
    @BindView(R.id.detailUnitTeacherView)
    TeacherBannerView teacherBannerView;
    @BindView(R.id.finishSectionNotification)
    LinearLayout finishSectionNotificationLayout;
    @BindView(R.id.notification_message)
    TextView notificationMessage;

    protected Animation slideInDownAnimation;
    protected Animation slideOutUp;

    // ================================================================================
    // Lifecycle
    // ================================================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        unitId = getIntent().getExtras().getString(EXTRA_UNIT_ID);

        assert unitId != null;

        setContentView(R.layout.activity_detail_unit);
        ButterKnife.bind(this);
        loadAnimations();

        assert unitId != null;

        ABAUnit currentUnit = DataStore.getInstance().getLevelUnitController().getUnitWithId(getRealm(), unitId);
        levelId = currentUnit.getLevel().getIdLevel();

        initUnitDetail();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Crashlytics.log(Log.INFO, "Unit", "User opens Unit number " + unitId);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        LanguageController.currentLanguageOverride();

        // If the unit was already refreshed then refreshing rosco.
        if (unitRefreshed) {
            addRosco();
        }
    }

    @Override
    public void onDestroy() {
        DataStore.getInstance().getDownloadController().getDownloadThread().stopDownloadProcess(this);
        DataStore.getInstance().getDownloadController().getDownloadThread().hideDownloadDialog();
        super.onDestroy();
    }

    public void onBackPressed() {
        super.onBackPressed();
        finishUnit();
    }

    // ================================================================================
    // Overriding methods from DetailUnitBase
    // ================================================================================

    protected void refreshScreen() {
        addRosco();
    }

    // ================================================================================
    // Overriding methods from ABAMasterActivity
    // ================================================================================

    @Override
    protected void updateAfterLogin() {
        super.updateAfterLogin();
        addRosco();
    }

    // ================================================================================
    // Private methods
    // ================================================================================

    private void loadAnimations() {
        slideInDownAnimation = AnimationUtils.loadAnimation(this, R.anim.slide_in_down);
        slideOutUp = AnimationUtils.loadAnimation(this, R.anim.slide_out_up);
    }

    private void initUnitDetail() {
        if (!APIManager.isTablet(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }

        LanguageController.currentLanguageOverride();
        initApp();
        configActionBar();
        configBackground();
        configTeacher();
    }

    private void checkForCompletedSection() {
        if (getIntent().getExtras().containsKey(EXTRA_COMPLETED_SECTION)) {
            final SectionType finishedSection = SectionType.fromInt(getIntent().getExtras().getInt(EXTRA_COMPLETED_SECTION));
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showFinishedSection(finishedSection);
                }
            });
        }
    }

    protected void showFinishedSection(final SectionType sectionType) {
        notificationMessage.setText(getString(R.string.youHaveFinishSectionKey) + " " + sectionType.getSectionName(this));
        finishSectionNotificationLayout.setVisibility(View.VISIBLE);
        finishSectionNotificationLayout.startAnimation(slideInDownAnimation);

        Handler handlerNotification = new android.os.Handler();
        handlerNotification.postDelayed(new Runnable() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dismissFinishedSection();
                    }
                });

                if (sectionType == SectionType.kInterpreta) {
                    try {
                        RateMyAppDialog mRateMyAppDialog = new RateMyAppDialog.Builder(DetailUnitActivity.this)
                                .withAndroidStoreRatingButton()
                                .withDontRemindMeAgainButton()
                                .build();
                        mRateMyAppDialog.showAlways(DetailUnitActivity.this);
                    } catch (RuntimeException e) {
                        Crashlytics.logException(e);
                        LogBZ.printStackTrace(e);
                    }
                }
            }
        }, 3000);
    }

    protected void dismissFinishedSection() {
        finishSectionNotificationLayout.startAnimation(slideOutUp);
        finishSectionNotificationLayout.setVisibility(View.GONE);
    }

    private void addRosco() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // Removing previous rosco instance (new calculations to be done)
                if (roscoView != null) {
                    RelativeLayout roscoLayout = (RelativeLayout) findViewById(R.id.roscoLayout);
                    roscoLayout.removeAllViews();
                }

                Realm instance = Realm.getInstance(ABAApplication.get().getRealmConfiguration());
                ABAUnit currentUnit = DataStore.getInstance().getLevelUnitController().getUnitWithId(instance, unitId);

                roscoView = new UnitRoscoView(DetailUnitActivity.this, currentUnit.getIdUnit(), currentUnit.getLevel().getIdLevel());
                roscoView.setUnitRoscoHandler(new RoscoHandler());

                RelativeLayout roscoLayout = (RelativeLayout) findViewById(R.id.roscoLayout);
                roscoLayout.addView(roscoView, new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

                instance.close();
            }
        });
    }

    private void getCompletedActions(final SectionType sectionToLoad) {
        ABAUser currentUser = DataStore.getInstance().getUserController().getCurrentUser(getRealm());
        ABAUnit currentUnit = DataStore.getInstance().getLevelUnitController().getUnitWithId(getRealm(), unitId);
        ProgressService.getInstance().updateCompletedActions(currentUser, currentUnit, new ABASimpleCallback() {
                    @Override
                    public void onSuccess() {
                        dismissProgressDialog();
                        unitRefreshed = true;
                        addRosco();

                        checkForCompletedSection();
                        if (sectionToLoad != null) {
                            checkSubscriptionAndStartSection(sectionToLoad);
                        }
                    }

                    @Override
                    public void onError(ABAAPIError error) {
                        dismissProgressDialog();
                        showABAErrorNotification(error.getError());
                    }
                }
        );
    }

    private void configBackground() {
        ImageView background = (ImageView) findViewById(R.id.detailUnitBackground);
        ABAUnit currentUnit = DataStore.getInstance().getLevelUnitController().getUnitWithId(getRealm(), unitId);

        if (DataStore.getInstance().getLevelUnitController().checkIfFileExist(currentUnit, currentUnit.getFilmImageInactiveUrl())) {
            DataStore.getInstance().getLevelUnitController().displayImage(currentUnit, currentUnit.getFilmImageInactiveUrl(), background);
        } else {
            ImageLoader.getInstance().displayImage(currentUnit.getFilmImageInactiveUrl(), background);
        }
    }

    private void configTeacher() {
        ABAUnit currentUnit = DataStore.getInstance().getLevelUnitController().getUnitWithId(getRealm(), unitId);
        if (DataStore.getInstance().getLevelUnitController().checkIfFileExist(currentUnit, DataStore.getInstance().getUserController().getCurrentUser(getRealm()).getTeacherImage())) {
            DataStore.getInstance().getLevelUnitController().displayImage(null, DataStore.getInstance().getUserController().getCurrentUser(getRealm()).getTeacherImage(), teacherBannerView.getImageView());
        } else {
            teacherBannerView.setImageUrl(DataStore.getInstance().getUserController().getCurrentUser(getRealm()).getTeacherImage());
        }

        teacherBannerView.setText(getTeacherTip());
    }

    private String getTeacherTip() {
        ABAUnit currentUnit = DataStore.getInstance().getLevelUnitController().getUnitWithId(getRealm(), unitId);
        double unitProgress = DataStore.getInstance().getLevelUnitController().getGlobalProgressForUnit(currentUnit);
        SectionType sectiontoLoad = DataStore.getInstance().getRoscoController().getCurrentSectionForUnit(currentUnit);

        SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = settings.edit();
        boolean isNewUser = settings.getBoolean("newUser", false);
        if (isNewUser) {
            String teacherNewRegister = getResources().getString(R.string.unitTeacherNewRegister);
            teacherNewRegister = teacherNewRegister.replace("%@", DataStore.getInstance().getUserController().getCurrentUser(getRealm()).getTeacherName());
            editor.putBoolean("newUser", false);
            editor.commit();
            return teacherNewRegister;
        } else if (unitProgress > 0 && !currentUnit.isCompleted()) {
            if (sectiontoLoad == SectionType.kABAFilm) {
                if (currentUnit.getSectionFilm().isCompleted()) {
                    return getString(R.string.unitTeacherFilmContinueKey);
                }
            } else {
                return getString(R.string.unitContinueSectionTeacherText) + " " + sectiontoLoad.getSectionName(this);
            }
        } else if (currentUnit.isCompleted()) {
            return getString(getResources().getIdentifier("unitCompletedTeacherText" + currentUnit.getIdUnit(), "string",
                    getPackageName()));
        }

        //TODO new registers

        return getString(getResources().getIdentifier("unitTeacherText" + currentUnit.getIdUnit(), "string",
                getPackageName()));
    }

    private void initApp() {

        if (roscoView != null) {
            Toast.makeText(this, "Removing rosco", Toast.LENGTH_SHORT).show();
            RelativeLayout roscoLayout = (RelativeLayout) findViewById(R.id.roscoLayout);
            roscoLayout.removeAllViews();
        }

        if (getIntent().getExtras().get(OPEN_SECTION) != null) {
            getSectionsContentForCurrentUnit(SectionType.fromInt((int) getIntent().getExtras().getLong(OPEN_SECTION)));
            getIntent().getExtras().putString(OPEN_SECTION, null);
        } else {
            getSectionsContentForCurrentUnit(null);
        }

        ABAUnit currentUnit = DataStore.getInstance().getLevelUnitController().getUnitWithId(getRealm(), unitId);
        if (currentUnit.getIdUnit() != null) {
            Crashlytics.setString("Selected Unit", currentUnit.getIdUnit());
        }
        if (currentUnit.getLastChanged() != null) {
            Crashlytics.setString("Selected Unit update date", currentUnit.getLastChanged().toString());
        }
    }

    private void checkSubscriptionAndStartSection(SectionType sectionType) {

        // Getting unit index and user type
        ABAUnit currentUnit = DataStore.getInstance().getLevelUnitController().getUnitWithId(getRealm(), unitId);
        int unitIndexWithinLevel = DataStore.getInstance().getLevelUnitController().getUnitIndexWithinLevel(currentUnit.getIdUnit(), currentUnit.getLevel().getIdLevel());
        boolean isPremium = DataStore.getInstance().getUserController().isUserPremium();
        SectionController.SectionStatus status = DataStore.getInstance().getRoscoController().getStatusForSection(currentUnit, sectionType);

        // Plans view should be shown if user is not premium, at level 2+ and for locked sections
        boolean showPlansView = !isPremium && unitIndexWithinLevel != 1 && status == SectionController.SectionStatus.kABAStatusLocked;
        if (showPlansView) {
            LegacyTrackingManager.getInstance().trackEvent(getRealm(), "unit_premium", "getpremium", "click en sección con contenido bloqueado");

            startInitialPlansActivity();
        } else {
            // If user can proceed with next section only if user have access to this Section
            checkIfSectionIsAccesibleAndStartIntent(sectionType);
        }
    }

    private void startInitialPlansActivity() {
        overridePendingTransition(R.anim.slide_enter_unitanimation1, R.anim.slide_enter_unitanimation2);
        startActivity(InitialPlansActivity.getCallingIntent(this));
    }

    private void checkIfSectionIsAccesibleAndStartIntent(SectionType sectionType) {

        boolean forceUnlock = false;
        if (ABAShepherdEditor.isInternal() && ShepherdEvaluationPlugin.isUnlockActive(DetailUnitActivity.this)) {
            forceUnlock = true;
        }

        ABAUnit currentUnit = DataStore.getInstance().getLevelUnitController().getUnitWithId(getRealm(), unitId);
        List<SectionType> accesibleSectionType = DataStore.getInstance().getRoscoController().getAccessibleSections(currentUnit);
        if (accesibleSectionType.contains(sectionType) || (sectionType.equals(SectionType.kEvaluacion) && forceUnlock)) {
            startIntentUnitSection(sectionType);
        } else {
            showLockMessage(sectionType);
        }
    }

    private void showLockMessage(SectionType sectionType) {

        if (sectionType.equals(SectionType.kEvaluacion)) {
            showAssessmentLockMessage();
        } else {
            //TODO Unreacheable Code?!?!?
            ABAUnit currentUnit = DataStore.getInstance().getLevelUnitController().getUnitWithId(getRealm(), unitId);
            SectionType currentSectionToLoad = DataStore.getInstance().getRoscoController().getCurrentSectionForUnit(currentUnit);
            showABAAlertNotification(getString(R.string.sectionLockMessageAlert) + " " + currentSectionToLoad.getSectionName(getApplicationContext()).toUpperCase());
        }
    }

    private void showAssessmentLockMessage() {
        showABAErrorNotification(getString(R.string.cannotEnterAssesmentKey));
    }

    private void startIntentUnitSection(SectionType sectionType) {

        Intent intent = null;
        switch (sectionType) {

            case kABAFilm:
                intent = new Intent(this, ABAFilmActivity.class);
                intent.putExtra(ABAFilmActivity.IS_DOWNLOADED, isUnitDownloaded);
                intent.putExtra(ABAFilmActivity.SECTION_ID, ABAFilmActivity.FILM_SECTION);
                LegacyTrackingManager.getInstance().trackEvent(getRealm(), "unit_s1", "course", "click en abafilm");
                break;

            case kHabla:
                intent = new Intent(this, ABASpeakActivity.class);
                LegacyTrackingManager.getInstance().trackEvent(getRealm(), "unit_s2", "course", "click en habla");
                break;

            case kEscribe:
                intent = new Intent(this, ABAWriteActivity.class);
                LegacyTrackingManager.getInstance().trackEvent(getRealm(), "unit_s3", "course", "click en escribe");
                break;

            case kInterpreta:
                intent = new Intent(this, ABAInterpretationActivity.class);
                LegacyTrackingManager.getInstance().trackEvent(getRealm(), "unit_s4", "course", "click en interpreta");
                break;

            case kVideoClase:
                intent = new Intent(this, ABAFilmActivity.class);
                intent.putExtra(ABAFilmActivity.IS_DOWNLOADED, isUnitDownloaded);
                intent.putExtra(ABAFilmActivity.SECTION_ID, ABAFilmActivity.VIDEOCLASS_SECTION);
                LegacyTrackingManager.getInstance().trackEvent(getRealm(), "unit_s5", "course", "click en videoclase");

                break;

            case kEjercicios:
                intent = new Intent(this, ABAExercisesActivity.class);
                LegacyTrackingManager.getInstance().trackEvent(getRealm(), "unit_s6", "course", "click en ejercicios");
                break;

            case kVocabulario:
                intent = new Intent(this, ABAVocabularyActivity.class);
                LegacyTrackingManager.getInstance().trackEvent(getRealm(), "unit_s7", "course", "click en vocabulario");
                break;

            case kEvaluacion: {
                intent = new Intent(this, ABAEvaluationActivity.class);
                LegacyTrackingManager.getInstance().trackEvent(getRealm(), "unit_s8", "course", "click en evaluación");
                break;
            }
            default:
                intent = null;
                break;
        }

        if (intent != null) {
            intent.putExtra(EXTRA_UNIT_ID, unitId);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.slide_enter_unitanimation1, R.anim.slide_enter_unitanimation2).toBundle();
                startActivity(intent, bndlanimation);
                finish();
            } else {
                startActivity(intent);
                finish();
            }
        }
    }

    // ================================================================================
    // Private methods - Download related
    // ================================================================================


    // ================================================================================
    // Inner classes
    // ================================================================================

    private class RoscoHandler implements UnitRoscoView.UnitRoscoHandler {

        @Override
        public void onSectionTap(SectionType sectionType) {
            Crashlytics.log(Log.INFO, "Unit", "User tries open Section " + sectionType.toString());

            ABAUnit currentUnit = DataStore.getInstance().getLevelUnitController().getUnitWithId(getRealm(), unitId);
            if (!isDownloadStarted) {
                if (currentUnit.isDataDownloaded()) {
                    checkSubscriptionAndStartSection(sectionType);
                } else {
                    if (ConnectionService.isNetworkAvailable(getApplicationContext())) {
                        getSectionsContentForCurrentUnit(sectionType);
                    } else {
                        LogBZ.e("Section will not be loaded since there is not network connection");
                        showABAErrorNotification(getString(R.string.getAllSectionsForUnitErrorKey));
                    }
                }
            }
        }

        @Override
        public void onCentralTap() {
            if (!isDownloadStarted) {
                ABAUnit currentUnit = DataStore.getInstance().getLevelUnitController().getUnitWithId(getRealm(), unitId);
                // If section completed
                if (currentUnit.isCompleted()) {
                    // Loading evaluation
                    if (currentUnit.isDataDownloaded()) {
                        startIntentUnitSection(SectionType.kEvaluacion);
                    } else {
                        if (ConnectionService.isNetworkAvailable(getApplicationContext())) {
                            getSectionsContentForCurrentUnit(SectionType.kEvaluacion);
                        } else {
                            LogBZ.e("Parse Unit Error");
                            showABAErrorNotification(getString(R.string.getAllSectionsForUnitErrorKey));
                        }
                    }
                } else {

                    // Otherwise we load the first non-finished section
                    SectionType sectionToLoad = DataStore.getInstance().getRoscoController().getCurrentSectionForUnit(currentUnit);

                    if (currentUnit.isDataDownloaded()) {
                        checkSubscriptionAndStartSection(sectionToLoad);
                    } else {
                        if (ConnectionService.isNetworkAvailable(getApplicationContext())) {
                            getSectionsContentForCurrentUnit(sectionToLoad);
                        } else {
                            LogBZ.e("Parse Unit Error");
                            showABAErrorNotification(getString(R.string.getAllSectionsForUnitErrorKey));
                        }
                    }
                }
            }
        }
    }

    public void getSectionsContentForCurrentUnit(final SectionType sectionType) {
        showProgressDialog(ABAProgressDialog.ComplexDialog);
        ABAUnit currentUnit = DataStore.getInstance().getLevelUnitController().getUnitWithId(getRealm(), unitId);
        SectionsService.getInstance().fetchSections(DetailUnitActivity.this, currentUnit, new ABASimpleCallback() {
            @Override
            public void onSuccess() {

                Realm instance = Realm.getInstance(ABAApplication.get().getRealmConfiguration());
                ABAUnit callbackUnit = DataStore.getInstance().getLevelUnitController().getUnitWithId(instance, unitId);

                getCompletedActions(sectionType);
                totalElementsForDownload = DataStore.getInstance().getDownloadController().getTotalElementsForDownloadAtUnit(getRealm(), callbackUnit);
                isUnitDownloaded = DataStore.getInstance().getDownloadController().isAllFileDownloaded(callbackUnit, totalElementsForDownload);

                instance.close();
            }

            @Override
            public void onError(ABAAPIError error) {
                dismissProgressDialog();
                if (error != null) {
                    error.showABANotificationError(DetailUnitActivity.this);
                    LogBZ.e("Unit Sections API error: " + error.getError());
                }
            }
        });
    }

}