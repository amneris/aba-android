package com.abaenglish.videoclass.presentation.shell;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.transition.Fade;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;

import com.abaenglish.videoclass.ABAApplication;
import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.analytics.cooladata.navigation.CooladataNavigationTrackingManager;
import com.abaenglish.videoclass.analytics.helpers.GenericTrackingInterface;
import com.abaenglish.videoclass.data.network.oauth.OAuthClient;
import com.abaenglish.videoclass.data.network.retrofit.abaMomentsUtils.ABAMomentClientDefinitions.ABAMomentErrorType;
import com.abaenglish.videoclass.data.network.retrofit.clients.ABAMomentBadgeClient;
import com.abaenglish.videoclass.data.persistence.ABALevel;
import com.abaenglish.videoclass.data.persistence.ABAUser;
import com.abaenglish.videoclass.domain.content.LevelUnitController;
import com.abaenglish.videoclass.domain.content.UserController;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.abaenglish.videoclass.helpdesk.ZendeskHelpCenterFragment;
import com.abaenglish.videoclass.presentation.abaMoment.ABAMomentsListFragment_;
import com.abaenglish.videoclass.presentation.base.ABAMasterActivity;
import com.abaenglish.videoclass.presentation.certificate.CertificatesFragment;
import com.abaenglish.videoclass.presentation.course.DashboardFragment;
import com.abaenglish.videoclass.presentation.level.LevelSelectionFragment;
import com.abaenglish.videoclass.presentation.plan.ImprovedPlansFragment;
import com.abaenglish.videoclass.presentation.plan.ImprovedPlansFragment_;
import com.abaenglish.videoclass.presentation.plan.PlansFragment;
import com.abaenglish.videoclass.presentation.profile.ProfileFragmentNew;
import com.zendesk.sdk.model.access.AnonymousIdentity;
import com.zendesk.sdk.model.access.Identity;
import com.zendesk.sdk.network.impl.ZendeskConfig;

/**
 * Created by madalin on 13/04/15.
 */
public class MenuActivity extends ABAMasterActivity implements View.OnClickListener {

    private static final String MENU_PREFERENCES = "MENU_PREFERENCES";
    private static final String FIRST_SESSION_OPENED_MENU_IS_PERFORMED = "FIRST_SESSION_OPENED_MENU_IS_PERFORMED";

    public static final String EXTRA_FRAGMENT = "fragment";
    public static final String OPEN_UNIT = "openUnit";
    public static final String OPEN_SECTION = "openSection";
    public static final String OPEN_PRICES_IN_WEB = "pricesInWeb";
    public static final String OPEN_MOMENTS = "openMoments";
    public static final String USER_TYPE = "USER_TYPE";

    public static final Integer DETAIL_UNIT_REQUEST_CODE = 400;
    public static final Integer DETAIL_UNIT_GO_TO_PROFILE_RESULT_CODE = 500;
    public static final Integer ABA_MOMENT_REQUEST_CODE = 600;

    private DrawerLayout mDrawerLayout;
    private FrameLayout mDrawerFrameLayout;
    private SlideMenuFragment mSlideMenuFragment;

    private boolean shouldShowAbaMoments = false;
    private boolean shouldOpenMoments = false;

    public static int BADGE_NUMBER = 0;

    public enum FragmentType {
        kCurso,
        kLevels,
        kCerts,
        kProfile
    }

    public enum UserType {
        kUserRegister,
        kUserJoinApp
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        DataStore.getInstance().setCurrentActivity(this);
        configToolbar();
        setUpNavigationMenu();

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        if (UserType.kUserRegister.equals(bundle.get(USER_TYPE))) {
            unitDetailABTestingConfigurator.startUnitDetailIntent(this, firstUnitAtCurrentLevel());
            getIntent().putExtra(MenuActivity.USER_TYPE, MenuActivity.UserType.kUserJoinApp);
        }

        if ((savedInstanceState == null && bundle != null && bundle.get(EXTRA_FRAGMENT) != null) || (findViewById(R.id.main_fragment).getClass().getName() == DashboardFragment.class.getName())) {
            selectItem((FragmentType) bundle.get(EXTRA_FRAGMENT));
//            displayAllRequestPermissionsForApp();
        } else if (savedInstanceState == null) {
            selectItem(FragmentType.kLevels);
        }

        String deeplinkUnitId = bundle.getString(OPEN_UNIT);
        if (deeplinkUnitId != null) {

            String unitId;
            if (bundle.getString(OPEN_SECTION) != null) {
                unitId = deeplinkUnitId;
                unitDetailABTestingConfigurator.startUnitDetailIntent(this, deeplinkUnitId, Long.parseLong(bundle.getString(OPEN_SECTION)));
            } else {
                unitId = bundle.getString(OPEN_UNIT);
                unitDetailABTestingConfigurator.startUnitDetailIntent(this, unitId);
            }

            tracker.trackEnteredUnit(LevelUnitController.getIdLevelForUnitId(unitId), unitId);

            // Nullifying values to avoid continuous dee
            bundle.putString(OPEN_UNIT, null);
            bundle.putString(OPEN_SECTION, null);
        }

        if (bundle.getBoolean(OPEN_PRICES_IN_WEB) && !DataStore.getInstance().getUserController().isUserPremium()) {
            Fragment plansFragment;
            if (serverConfig.shouldShowImprovedPlansPage()) {
                plansFragment = new ImprovedPlansFragment_();
            } else {
                plansFragment = new PlansFragment();
            }

            changeFragmentWithDelay(plansFragment, false);
            trackPricingScreenWithOrigin(GenericTrackingInterface.PlansViewControllerOriginType.kPlansViewControllerOriginMenu);
            bundle.putBoolean(OPEN_PRICES_IN_WEB, false);
        }

        if (bundle.getBoolean(OPEN_MOMENTS)) {
            bundle.putBoolean(OPEN_MOMENTS, false);
            shouldOpenMoments = true;
        }

        ABAUser currentUser = DataStore.getInstance().getUserController().getCurrentUser(getRealm());
        if (currentUser != null) {
            Identity anonymousIdentity = new AnonymousIdentity.Builder()
                    .withEmailIdentifier(currentUser.getEmail())
                    .withNameIdentifier(currentUser.getName())
                    .build();
            ZendeskConfig.INSTANCE.setIdentity(anonymousIdentity);
        }

        SharedPreferences preferences = getSharedPreferences(ABAApplication.CRASH_SHARED_PREFERENCES, Context.MODE_PRIVATE);
        if (preferences.getBoolean(ABAApplication.CURRENT_APP_STATUS, false)) {
            showZendeskCrashReportDialog();
        }

        //SlideMenu Rotate Screen
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.main_fragment);
                if (currentFragment != null && mSlideMenuFragment != null) {
                    selectedMenuItemFromCurrentFragment(currentFragment.getClass().getName());
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        // to know if we display abaMoments menu and badge
        updateBadge();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        if (isDrawerMenuOpen()) {
            toggleNavigationMenu();
        } else {
            super.onConfigurationChanged(newConfig);
        }
    }

    @Override
    public void onBackPressed() {
        if (isDrawerMenuOpen()) {
            toggleNavigationMenu();
        } else {
            Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.main_fragment);
            if (currentFragment.getClass().getName().equalsIgnoreCase(DashboardFragment.class.getName())) {
                super.onBackPressed();
            } else {
                this.selectItem(FragmentType.kCurso);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == MenuActivity.DETAIL_UNIT_REQUEST_CODE && resultCode == MenuActivity.DETAIL_UNIT_GO_TO_PROFILE_RESULT_CODE) {
            this.selectItem(FragmentType.kProfile);
        } else if (requestCode == MenuActivity.ABA_MOMENT_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Fragment currentFragment = getSupportFragmentManager().findFragmentById(R.id.main_fragment);
            if (currentFragment != null) {
                currentFragment.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    // ================================================================================
    // Public methods
    // ================================================================================

    public void updateBadge() {

        ABAUser user = UserController.getInstance().getCurrentUser(getRealm());
        ABAMomentBadgeClient abaMomentBadgeClient = new ABAMomentBadgeClient(appConfig, tokenAccessor, this);

        Log.e("AppConfiguration", appConfig.toString());

        abaMomentBadgeClient.fetchBadge(user.getUserId(), new OAuthClient.OnResponseListener<Integer>() {
            @Override
            public void onResponseReceived(Integer badgeCount, String error) {
                Log.e("onResponseReceived", "response : " + badgeCount + ", error : " + error);

                if (ABAMomentErrorType.NONE.toString().compareTo(error) == 0) {
                    shouldShowAbaMoments = true;
                    if (mSlideMenuFragment != null) {
                        mSlideMenuFragment.setShouldShowMoments(true);
                    }

                    onHasNewABAMomentFetched(badgeCount, false);

                } else if (ABAMomentErrorType.USER_NOT_EXIST.toString().compareTo(error) == 0) {

                    onHasNewABAMomentFetched(0, false);

                } else {
                    // Do nothing
                }
            }
        });
    }

    public void onHasNewABAMomentFetched(int numberOfNewAbaMoments, boolean error) {
        if (!error) {
            BADGE_NUMBER = numberOfNewAbaMoments;
        }

        if (shouldShowAbaMoments) {
            if (findViewById(R.id.toolbarBadgeNotificationTextView) != null) {
                int visibility = BADGE_NUMBER > 0 ? View.VISIBLE : View.INVISIBLE;
                findViewById(R.id.toolbarBadgeNotificationTextView).setVisibility(visibility);
            }
            if (mSlideMenuFragment != null) {
                mSlideMenuFragment.update();

                if (shouldOpenMoments) {
                    shouldOpenMoments = false;
                    mSlideMenuFragment.selectedItemTypeFromMenu(MenuType.menuABAMoment);
                    mSlideMenuFragment.openMoments();
                }
            }
        }
    }

    // ================================================================================
    // Private methods
    // ================================================================================

    private void configToolbar() {
        addToolbar();
        if (findViewById(R.id.toolbarLeftButton) != null) {
            findViewById(R.id.toolbarLeftButton).setOnClickListener(this);
            findViewById(R.id.toolbarLeftButton).setOnTouchListener(((ABAApplication) getApplication()).getImagePressedState(null));
        }
        onHasNewABAMomentFetched(BADGE_NUMBER, false);

    }

    private String firstUnitAtCurrentLevel() {
        ABALevel currentLevel = DataStore.getInstance().getUserController().getCurrentUser(getRealm()).getCurrentLevel();
        int currentLevelNumber = Integer.parseInt(currentLevel.getIdLevel()) - 1;
        return Integer.toString(currentLevelNumber * currentLevel.getUnits().size() + 1);
    }

    private void setUpNavigationMenu() {

        Bundle args = new Bundle();
        args.putBoolean(SlideMenuFragment.SHOULD_SHOW_ABAMOMENT_TAB, shouldShowAbaMoments);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerFrameLayout = (FrameLayout) findViewById(R.id.drawer_nav_fragment);
        mSlideMenuFragment = new SlideMenuFragment();
        mSlideMenuFragment.setArguments(args);

        getSupportFragmentManager().beginTransaction().replace(mDrawerFrameLayout.getId(), mSlideMenuFragment).commit();

        // Opening the menu for the first time
        if (shouldOpenMenuAtFirstSession(this)) {
            toggleNavigationMenu();
            saveFirstSessionMenuOpenedPerformed(this);
        }
    }

    public void toggleNavigationMenu() {
        if (isDrawerMenuOpen()) {
            mDrawerLayout.closeDrawer(GravityCompat.START);
        } else {
            mDrawerLayout.openDrawer(GravityCompat.START);
        }
    }

    public boolean isDrawerMenuOpen() {
        return mDrawerLayout.isDrawerOpen(mDrawerFrameLayout);
    }

    public void selectItem(FragmentType type) {
        switch (type) {
            case kCurso: {
                changeFragmentWithDelay(new DashboardFragment(), false);
                // Tracking navigation to Dashboard
                ABAUser user = UserController.getInstance().getCurrentUser(getRealm());
                CooladataNavigationTrackingManager.trackEnteredCourseIndex(user.getUserId(), user.getCurrentLevel().getIdLevel());
                break;
            }
            case kLevels: {
                changeFragmentWithDelay(new LevelSelectionFragment(), false);
                break;
            }
            case kCerts: {
                changeFragmentWithDelay(new CertificatesFragment(), false);
                break;
            }
            case kProfile: {
                changeFragmentWithDelay(new ProfileFragmentNew(), false);
                break;
            }
        }
    }

    public void changeFragment(final Fragment targetFragment) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            targetFragment.setEnterTransition(new Fade(Fade.IN));
            targetFragment.setExitTransition(new Fade(Fade.OUT));
        }

        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_fragment, targetFragment, targetFragment.getClass().getName())
                .commitAllowingStateLoss();

        selectedMenuItemFromCurrentFragment(targetFragment.getClass().getName());
    }

    public void changeFragmentWithDelay(final Fragment targetFragment, boolean withDelayed) {
        if (withDelayed) {
            int delayTime = 275;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (!isFinishing()) {
                        try {
                            changeFragment(targetFragment);
                        } catch (IllegalStateException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }, delayTime);
        } else {
            changeFragment(targetFragment);
        }


    }

    private void selectedMenuItemFromCurrentFragment(String currentFragment) {
        if (mSlideMenuFragment == null)
            return;

        if (currentFragment.equals(DashboardFragment.class.getName())) {
            mSlideMenuFragment.selectedItemTypeFromMenu(MenuType.menuCourse);
        } else if (currentFragment.equals(LevelSelectionFragment.class.getName())) {
            mSlideMenuFragment.selectedItemTypeFromMenu(MenuType.menuLevels);
        } else if (currentFragment.equals(CertificatesFragment.class.getName())) {
            mSlideMenuFragment.selectedItemTypeFromMenu(MenuType.menuCerts);
        } else if (currentFragment.equals(PlansFragment.class.getName()) || currentFragment.equals(ImprovedPlansFragment.class.getName())) {
            mSlideMenuFragment.selectedItemTypeFromMenu(MenuType.menuPremium);
        } else if (currentFragment.equals(ZendeskHelpCenterFragment.class.getName())) {
            mSlideMenuFragment.selectedItemTypeFromMenu(MenuType.menuHelp);
        } else if (currentFragment.equals(ProfileFragmentNew.class.getName())) {
            mSlideMenuFragment.selectedItemTypeFromMenu(MenuType.menuProfile);
        } else if (currentFragment.equals(ABAMomentsListFragment_.class.getName())) {
            mSlideMenuFragment.selectedItemTypeFromMenu(MenuType.menuABAMoment);
        } else {
            mSlideMenuFragment.selectedItemTypeFromMenu(null);
        }
    }

    // ================================================================================
    // OnClickListener
    // ================================================================================

    @Override
    public void onClick(View v) {
        toggleNavigationMenu();
    }

    // ================================================================================
    // Static methods to handle menu status (closed vs opened) the first time the user logs in
    // ================================================================================

    private static boolean shouldOpenMenuAtFirstSession(Context appContext) {
        SharedPreferences sharedPreferences = appContext.getSharedPreferences(MENU_PREFERENCES, MODE_PRIVATE);
        boolean isPerformed = sharedPreferences.getBoolean(FIRST_SESSION_OPENED_MENU_IS_PERFORMED, false);
        return !isPerformed;
    }

    private static void saveFirstSessionMenuOpenedPerformed(Context appContext) {
        SharedPreferences sharedPreferences = appContext.getSharedPreferences(MENU_PREFERENCES, MODE_PRIVATE);
        sharedPreferences.edit().putBoolean(FIRST_SESSION_OPENED_MENU_IS_PERFORMED, true).apply();
    }

    public static void resetFirstSessionMenuPerformance(Context appContext) {
        SharedPreferences sharedPreferences = appContext.getSharedPreferences(MENU_PREFERENCES, MODE_PRIVATE);
        sharedPreferences.edit().putBoolean(FIRST_SESSION_OPENED_MENU_IS_PERFORMED, false).apply();
    }
}
