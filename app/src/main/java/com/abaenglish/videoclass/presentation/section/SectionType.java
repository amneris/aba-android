package com.abaenglish.videoclass.presentation.section;

import android.content.Context;

import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.data.persistence.ABAEvaluation;
import com.abaenglish.videoclass.data.persistence.ABAExercises;
import com.abaenglish.videoclass.data.persistence.ABAFilm;
import com.abaenglish.videoclass.data.persistence.ABAInterpret;
import com.abaenglish.videoclass.data.persistence.ABASpeak;
import com.abaenglish.videoclass.data.persistence.ABAVideoClass;
import com.abaenglish.videoclass.data.persistence.ABAVocabulary;
import com.abaenglish.videoclass.data.persistence.ABAWrite;

/**
 * Created by jaume on 21/5/15.
 */
public enum SectionType {
    kABAFilm(1),
    kHabla(2),
    kEscribe(3),
    kInterpreta(4),
    kVideoClase(5),
    kEjercicios(6),
    kVocabulario(7),
    kEvaluacion(8),
    kSectionNotFound(0);

    private final int value;

    SectionType(final int newValue) {
        value = newValue;
    }

    public int getValue() {
        return value;
    }

    public String getSectionName(Context context) {
        switch (this) {
            case kABAFilm:
                return context.getString(R.string.filmSectionKey);
            case kHabla:
                return context.getString(R.string.speakSectionKey);
            case kEscribe:
                return context.getString(R.string.writeSectionKey);
            case kEjercicios:
                return context.getString(R.string.exercisesSectionKey);
            case kInterpreta:
                return context.getString(R.string.interpretSectionKey);
            case kEvaluacion:
                return context.getString(R.string.assessmentSectionKey);
            case kVideoClase:
                return context.getString(R.string.videoClassSectionKey);
            case kVocabulario:
                return context.getString(R.string.vocabularySectionKey);
            default:
                return "NOTFOUND";
        }
    }


    public static SectionType fromClass(Class classType) {
        if (classType.equals(ABAFilm.class) || classType.getSuperclass().equals(ABAFilm.class))
            return kABAFilm;
        if (classType.equals(ABASpeak.class) || classType.getSuperclass().equals(ABASpeak.class))
            return kHabla;
        if (classType.equals(ABAWrite.class) || classType.getSuperclass().equals(ABAWrite.class))
            return kEscribe;
        if (classType.equals(ABAInterpret.class) || classType.getSuperclass().equals(ABAInterpret.class))
            return kInterpreta;
        if (classType.equals(ABAVideoClass.class) || classType.getSuperclass().equals(ABAVideoClass.class))
            return kVideoClase;
        if (classType.equals(ABAExercises.class) || classType.getSuperclass().equals(ABAExercises.class))
            return kEjercicios;
        if (classType.equals(ABAVocabulary.class) || classType.getSuperclass().equals(ABAVocabulary.class))
            return kVocabulario;
        if (classType.equals(ABAEvaluation.class) || classType.getSuperclass().equals(ABAEvaluation.class))
            return kEvaluacion;

        return kSectionNotFound;
    }

    public static SectionType fromInt(int value) {
        for (SectionType st : SectionType.values()) {
            if (st.getValue() == value)
                return st;
        }

        return kSectionNotFound;
    }

    @Override
    public String toString() {
        return Integer.toString(getValue());
    }
}
