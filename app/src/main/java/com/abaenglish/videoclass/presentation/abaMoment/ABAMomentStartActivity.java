package com.abaenglish.videoclass.presentation.abaMoment;

import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.view.View;
import android.view.ViewGroup;

import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.UrlUtils;
import com.abaenglish.videoclass.analytics.cooladata.abaMoments.CooladataMomentsTrackingManager;
import com.abaenglish.videoclass.data.network.retrofit.abaMomentsUtils.ABAMoment;
import com.abaenglish.videoclass.media.ABAMomentPlayer;
import com.abaenglish.videoclass.presentation.base.ABAMasterActivity;
import com.abaenglish.videoclass.presentation.base.custom.ABATextView;
import com.abaenglish.videoclass.presentation.shell.MenuActivity;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;


@EActivity(R.layout.activity_aba_moment_start)
public class ABAMomentStartActivity extends ABAMasterActivity {

    public static String USER_ID_STRING_EXTRA_KEY = "user_uuid";
    public static String MOMENT_SERIALIZE_EXTRA_KEY = "moment";
    public static String MOMENT_ID_RESULT = "moment_id";

    private static final int MAX_WAITING_TIME_IN_SECONDS = 1;

    @ViewById
    ABATextView countDownTextView;

    //if the ABA Moment has to be launched or it has been cancelled
    private boolean hasToBeLaunched = true;

    private String userId;
    private ABAMoment abaMoment;

    // Sound play
    private ABAMomentPlayer mediaPlayer;

    @AfterViews
    public void afterViews() {

        this.userId = getIntent().getStringExtra(USER_ID_STRING_EXTRA_KEY);
        abaMoment = (ABAMoment) getIntent().getSerializableExtra(MOMENT_SERIALIZE_EXTRA_KEY);

        String audioUrl = UrlUtils.getAudioUrl(this, abaMoment.getId(), abaMoment.getAudio());
        mediaPlayer = new ABAMomentPlayer(this, audioUrl);

        // for fullscreen
        hideSystemUI();
        View decorView = getWindow().getDecorView();
        decorView.setOnSystemUiVisibilityChangeListener
                (new View.OnSystemUiVisibilityChangeListener() {
                    @Override
                    public void onSystemUiVisibilityChange(int visibility) {
                        if (visibility == ViewGroup.VISIBLE) {
                            hideUIThread();
                        }
                    }
                });

        // Countdown before the ABAMoment
        countDownTextView.setText(String.valueOf(3));
        int seconds = 3;
        while (seconds != 0) {
            seconds--;
            final int finalSeconds = seconds;

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {

                    if (finalSeconds == 0) {
                        setText(abaMoment.getDefaultName(), true);
                    } else {
                        setText(String.valueOf(finalSeconds), false);
                    }

                }
            }, 1000 * (3 - finalSeconds));
        }

        // Go to the next screen
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (hasToBeLaunched) {

                    // Playing the audio and waiting until the audio finished playing
                    mediaPlayer.playAudio(MAX_WAITING_TIME_IN_SECONDS, new ABAMomentPlayer.ABAMomentPlayerListener() {
                        @Override
                        public void audioFinishedPlaying(boolean success) {
                            navigateToMomentScreen();
                        }
                    });
                }
            }
        }, 4000);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        hasToBeLaunched = false;
        setResult(RESULT_OK, data);
        finish();
    }

    @Override
    public void onBackPressed() {
        hasToBeLaunched = false;
        super.onBackPressed();
    }

    @UiThread
    public void setText(String text, boolean changeSize) {
        if (changeSize) {
            countDownTextView.setTextSize(50);
        }
        countDownTextView.setText(text);
    }

    @UiThread
    public void hideSystemUI() {
        int uiOptions =
                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LOW_PROFILE;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {

            uiOptions = uiOptions | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_FULLSCREEN;
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {

                uiOptions = uiOptions | View.SYSTEM_UI_FLAG_IMMERSIVE;
            }
        }

        getWindow().getDecorView().setSystemUiVisibility(uiOptions);
    }

    // ================================================================================
    // Private methods
    // ================================================================================

    private void hideUIThread() {
        final Runnable runE = new Runnable() {
            @Override
            public void run() {
                hideSystemUI();
            }
        };
        new Handler().postDelayed(runE, 1000);
    }

    private void navigateToMomentScreen() {
        CooladataMomentsTrackingManager.trackMomentStarted(userId, abaMoment.getMomentType(), abaMoment.getId());

        Intent intent = new Intent(ABAMomentStartActivity.this, ABAMomentActivity_.class);
        intent.putExtra(ABAMomentActivity.MOMENT_ID_STRING_EXTRA_KEY, abaMoment.getId());
        intent.putExtra(ABAMomentActivity.USER_ID_STRING_EXTRA_KEY, userId);
        intent.putExtra(ABAMomentActivity.MOMENT_TYPE_STRING_EXTRA_KEY, abaMoment.getMomentType());

        startActivityForResult(intent, MenuActivity.ABA_MOMENT_REQUEST_CODE);
        overridePendingTransition(R.anim.slide_enter_unitanimation1, R.anim.slide_enter_unitanimation2);
    }
}
