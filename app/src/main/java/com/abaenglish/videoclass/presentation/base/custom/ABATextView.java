package com.abaenglish.videoclass.presentation.base.custom;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.TextView;

import com.abaenglish.videoclass.ABAApplication;
import com.abaenglish.videoclass.R;
import com.bzutils.LogBZ;

import javax.inject.Inject;

/**
 * Created by iaguila on 23/3/15.
 */
public class ABATextView extends TextView {

    @Inject
    protected FontCache fontCache;

    public ABATextView(Context context) {
        super(context);
        ABAApplication.get().getApplicationComponent().inject(this);
    }

    public ABATextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public ABATextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ABATextView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        if (!isInEditMode()) {
            ABAApplication.get().getApplicationComponent().inject(this);
            configTypeface();
        }
    }

    private void configTypeface() {
        // Normalising ABATag
        if (getTag() == null) {
            setABATag(R.integer.typefaceSans500);
        }

        try {

            switch ((Integer.parseInt(getTag().toString()))) {
                case 1100:
                    setTypeface(fontCache.getTypeface(FontCache.ABATypeface.sans100));
                    break;
                case 1300:
                    setTypeface(fontCache.getTypeface(FontCache.ABATypeface.sans300));
                    break;
                case 1500:
                    setTypeface(fontCache.getTypeface(FontCache.ABATypeface.sans500));
                    break;
                case 1700:
                    setTypeface(fontCache.getTypeface(FontCache.ABATypeface.sans700));
                    break;
                case 1900:
                    setTypeface(fontCache.getTypeface(FontCache.ABATypeface.sans900));
                    break;
                case 2300:
                    setTypeface(fontCache.getTypeface(FontCache.ABATypeface.slab300));
                    break;
                case 2500:
                    setTypeface(fontCache.getTypeface(FontCache.ABATypeface.slab500));
                    break;
                case 2501:
                    setTypeface(fontCache.getTypeface(FontCache.ABATypeface.slab500i));
                    break;
                case 2700:
                    setTypeface(fontCache.getTypeface(FontCache.ABATypeface.slab700));
                    break;
                case 2900:
                    setTypeface(fontCache.getTypeface(FontCache.ABATypeface.slab900));
                    break;
                case 1:
                    setTypeface(fontCache.getTypeface(FontCache.ABATypeface.rBold));
                    break;
                case 0:
                     setTypeface(fontCache.getTypeface(FontCache.ABATypeface.rRegular));
                     break;
                case 100:
                    setTypeface(fontCache.getTypeface(FontCache.ABATypeface.robotoSlabLight));
                    break;

            }
        } catch (Exception e){
            LogBZ.printStackTrace(e);
            setTypeface(fontCache.getTypeface(FontCache.ABATypeface.sans300));
        }
    }

    public void setABATag(Object tag){
      setTag(tag);
      configTypeface();
    }
}
