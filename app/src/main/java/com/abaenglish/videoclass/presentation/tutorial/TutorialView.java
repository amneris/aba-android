package com.abaenglish.videoclass.presentation.tutorial;

/**
 * Created by xabierlosada on 13/07/16.
 */
public interface TutorialView {
    void onRegisterClicked();
    void onLoginClicked();
}
