package com.abaenglish.videoclass.presentation.section.assessment.result.interactor;

import com.google.auto.value.AutoValue;

/**
 * Created by xabierlosada on 26/07/16.
 */
@AutoValue public abstract class AssessmentResult {

    public enum Grade {
        PERFECT,
        PASS,
        FAIL
    }

    public String unitName() {
        return String.valueOf(unitId());
    }
    public abstract Grade grade();
    public abstract boolean isFirstCompletedUnit();
    public abstract String username();
    public abstract boolean isLastUnitOfLevel();
    public abstract boolean isCourseFinished();
    public abstract int numberOfQuestions();
    public abstract int correctAnswers();
    public abstract String teacherImageUrl();
    public abstract String levelName();
    public abstract String levelId();
    public abstract String unitId();
    public abstract String unitTitle();
    public abstract String nextUnitId();

    public static Builder builder() {
        return new AutoValue_AssessmentResult.Builder()
                .isLastUnitOfLevel(false)
                .isCourseFinished(false)
                .isFirstCompletedUnit(false);
    }

    @AutoValue.Builder
    public static abstract class Builder {
        public abstract Builder isFirstCompletedUnit(boolean isFirstCompletedUnit);
        public abstract Builder isLastUnitOfLevel(boolean isLastUnitOfLevel);
        public abstract Builder isCourseFinished(boolean isCourseFinished);
        public abstract Builder numberOfQuestions(int numberOfQuestions);
        public abstract Builder username(String username);
        public abstract Builder correctAnswers(int validResponses);
        public abstract Builder grade(Grade grade);
        public abstract Builder teacherImageUrl(String teacherImageUrl);
        public abstract Builder levelName(String levelName);
        public abstract Builder levelId(String levelId);
        public abstract Builder unitId(String unitId);
        public abstract Builder unitTitle(String unitTitle);
        public abstract Builder nextUnitId(String nextUnitId);
        public abstract AssessmentResult build();
    }






}
