package com.abaenglish.videoclass.presentation.base;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.abaenglish.videoclass.ABAApplication;
import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.analytics.TrackerContract.Tracker;
import com.abaenglish.videoclass.data.CourseContentService;
import com.abaenglish.videoclass.data.network.oauth.OAuthTokenAccessor;
import com.abaenglish.videoclass.domain.ApplicationConfiguration;
import com.abaenglish.videoclass.domain.abTesting.ServerConfig;
import com.abaenglish.videoclass.domain.abTesting.UnitDetailABTestingConfigurator;
import com.abaenglish.videoclass.presentation.base.custom.ABATextView;
import com.abaenglish.videoclass.presentation.base.custom.FontCache;
import com.abaenglish.videoclass.session.SessionService;

import javax.inject.Inject;

import io.realm.Realm;

/**
 * Created by Marc Güell Segarra on 22/7/15.
 */
public abstract class ABAMasterFragment extends Fragment {

    private Realm realm;
    private View parentView;
    private ABATextView toolbarTitle;
    private ABATextView toolbarSubtitle;
    private ImageButton toolbarButton;

    @Inject
    protected ServerConfig serverConfig;

    @Inject
    protected UnitDetailABTestingConfigurator unitDetailABTestingConfigurator;

    @Inject
    protected FontCache fontCache;

    @Inject
    protected Tracker tracker;

    @Inject
    protected SessionService mSessionService;

    @Inject
    protected CourseContentService mContentService;

    @Inject
    protected ApplicationConfiguration appConfig;

    @Inject
    protected OAuthTokenAccessor tokenAccessor;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ABAApplication.get().getApplicationComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        if (parentView == null) {
            parentView = inflater.inflate(onResLayout(), container, false);
        }

        toolbarTitle = (ABATextView) getABAActivity().findViewById(R.id.toolbarTitle);
        toolbarSubtitle = (ABATextView) getABAActivity().findViewById(R.id.toolbarSubTitle);
        toolbarButton = (ImageButton) getABAActivity().findViewById(R.id.toolbarLeftButton);

        onConfigView();
        if (onResLayout() != R.layout.fragment_slide_menu) {
            configDefaultToolbar();
            onConfigToolbar(toolbarTitle, toolbarSubtitle, toolbarButton);
        } else {
            ViewCompat.setFitsSystemWindows(parentView, true);
        }

        return parentView;
    }

    protected void configDefaultToolbar() {
        toolbarTitle.setTextColor(ContextCompat.getColor(getContext(), R.color.abaLightBlue));
        toolbarSubtitle.setTextColor(ContextCompat.getColor(getContext(), R.color.abaWhite));
        toolbarTitle.setVisibility(View.VISIBLE);
        toolbarSubtitle.setVisibility(View.VISIBLE);
        toolbarButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (realm != null) {
            realm.close();
            realm = null;
        }
    }

    public Realm getRealm() {
        if (realm == null) {
            realm = Realm.getInstance(ABAApplication.get().getRealmConfiguration());
        }
        return realm;
    }

    public ABAMasterActivity getABAActivity() {
        return (ABAMasterActivity) getActivity();
    }

    protected <T extends View> T $(@IdRes int id) {
        if (parentView != null)
            return (T) parentView.findViewById(id);
        return null;
    }

    @LayoutRes
    protected abstract int onResLayout();

    protected abstract void onConfigView();

    protected abstract void onConfigToolbar(ABATextView toolbarTitle, ABATextView toolbarSubTitle, ImageButton toolbarButton);
}
