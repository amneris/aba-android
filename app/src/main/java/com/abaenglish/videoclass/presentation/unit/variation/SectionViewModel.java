package com.abaenglish.videoclass.presentation.unit.variation;

import com.abaenglish.videoclass.presentation.unit.variation.Section.SectionType;

/**
 * Created by xabierlosada on 13/10/16.
 */

public class SectionViewModel {

    private int icon;
    private int progress;
    private int name;
    private boolean isNextSection;
    private boolean unlocked;
    private SectionType type;

    public SectionViewModel(int name, int progress, int icon, boolean unlocked, SectionType type) {
        this.name = name;
        this.progress = progress;
        this.icon = icon;
        this.unlocked = unlocked;
        this.isNextSection = false;
        this.type = type;
    }

    public int getIcon() {
        return icon;
    }

    public int getProgress() {
        return progress;
    }

    public int getName() {
        return name;
    }

    public boolean isUnlocked() {
        return unlocked;
    }

    public void setNextSection() {
        this.isNextSection = true;
    }

    public SectionType getType() {
        return type;
    }

    public boolean isNextSection() {
        return isNextSection;
    }
}
