package com.abaenglish.videoclass.presentation.shell;

/**
 * Created by vmadalin on 28/1/16.
 */
public enum MenuType {
    menuCourse(0),
    menuLevels(1),
    menuCerts(2),
    menuPremium(3),
    menuHelp(4),
    menuProfile(5),
    menuABAMoment(6);

    private final int value;

    MenuType(int value) {
        this.value = value;
    }

    public int getValue() {
        return this.value;
    }
}
