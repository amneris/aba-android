package com.abaenglish.videoclass.presentation.abaMoment;

import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.BounceInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.TranslateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.abaenglish.videoclass.ABAApplication;
import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.analytics.cooladata.abaMoments.CooladataMomentsTrackingManager;
import com.abaenglish.videoclass.data.network.retrofit.clients.ABAMomentClient;
import com.abaenglish.videoclass.data.network.retrofit.clients.ABAMomentSaveProgressClient;
import com.abaenglish.videoclass.domain.content.ABAMomentController;
import com.abaenglish.videoclass.domain.content.ABAMomentController.Element;
import com.abaenglish.videoclass.domain.content.ABAMomentController.QuestionType;
import com.abaenglish.videoclass.media.ABAMomentPlayer;
import com.abaenglish.videoclass.presentation.abaMoment.customViews.CircleABAMomentView;
import com.abaenglish.videoclass.presentation.base.ABAMasterActivity;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.display.CircleBitmapDisplayer;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Julien on 15/02/2017.
 * Activity to show one ABAMoment.
 */


@EActivity(R.layout.activity_aba_moment)
public class ABAMomentActivity extends ABAMasterActivity {

    public static String MOMENT_ID_STRING_EXTRA_KEY = "moment_uuid";
    public static String USER_ID_STRING_EXTRA_KEY = "user_uuid";
    public static String MOMENT_TYPE_STRING_EXTRA_KEY = "moment_type";

    private static final int MAX_WAITING_TIME_IN_SECONDS = 2;

    //header
    @ViewById
    ImageButton quitButton;

    @ViewById
    ProgressBar progressBar, startProgressBar;

    @ViewById
    CircleABAMomentView questionCircleABAMomentView, firstCircleABAMomentView, secondCircleABAMomentView, thirdCircleABAMomentView;

    @ViewById
    ImageView circleAnimationImageView;

    @ViewById
    ImageView questionImageView, firstAnswerImageView, secondAnswerImageView, thirdAnswerImageView;

    @ViewById
    ImageView firstGoodAnswerImageView, secondGoodAnswerImageView, thirdGoodAnswerImageView;

    @ViewById
    FrameLayout questionLayout;

    @ViewById
    RelativeLayout firstAnswerLayout, secondAnswerLayout, thirdAnswerLayout;

    RelativeLayout answerLayouts[];
    ImageView goodAnswerImageViews[];
    ImageView answersImageViews[];
    CircleABAMomentView circleABAMomentViews[];

    ABAMomentController abaMomentController;

    ABAMomentPlayer mediaPlayer;

    ImageLoader imageLoader = ImageLoader.getInstance();
    DisplayImageOptions displayImageOptions;

    boolean isTouchAvailable = true;
    String userId;
    String abaMomentId;
    String momentType;

    @InstanceState
    int state = 0;

    @InstanceState
    String abaMomentsAsJsonString;


    @AfterViews
    void afterViews() {

        imageLoader = ImageLoader.getInstance();
        displayImageOptions = new DisplayImageOptions.Builder().displayer(new CircleBitmapDisplayer()).cacheOnDisk(true).resetViewBeforeLoading(true).delayBeforeLoading(50).build();
        answerLayouts = new RelativeLayout[]{firstAnswerLayout, secondAnswerLayout, thirdAnswerLayout};
        answersImageViews = new ImageView[]{firstAnswerImageView, secondAnswerImageView, thirdAnswerImageView};
        circleABAMomentViews = new CircleABAMomentView[]{firstCircleABAMomentView, secondCircleABAMomentView, thirdCircleABAMomentView};
        goodAnswerImageViews = new ImageView[]{firstGoodAnswerImageView, secondGoodAnswerImageView, thirdGoodAnswerImageView};

        //add the touch listener
        for (CircleABAMomentView circleABAMomentView : circleABAMomentViews) {
            circleABAMomentView.setOnTouchListener(new ABAMomentActivity.OnCircleABAMomentTouchListener());
        }

        //to adjust the width and height
        initViews();

        // Reading parameters
        Intent intent = getIntent();
        this.abaMomentId = intent.getStringExtra(MOMENT_ID_STRING_EXTRA_KEY);
        this.userId = intent.getStringExtra(USER_ID_STRING_EXTRA_KEY);
        this.momentType = intent.getStringExtra(MOMENT_TYPE_STRING_EXTRA_KEY);
        abaMomentController = new ABAMomentController(
                this,
                abaMomentId,
                momentType,
                state,
                this.userId,
                new ABAMomentClient(this.appConfig, this.tokenAccessor));
    }

    @UiThread
    @SuppressWarnings("deprecation")
    void initViews() {

        setTouchAvailable(false);

        getWindow().getDecorView().getRootView().getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    getWindow().getDecorView().getRootView().getViewTreeObserver().removeOnGlobalLayoutListener(this);
                } else {
                    getWindow().getDecorView().getRootView().getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }

                circleAnimationImageView.getLayoutParams().height = questionCircleABAMomentView.getWidth() + (int) getResources().getDimension(R.dimen.padding20);
                circleAnimationImageView.getLayoutParams().width = questionCircleABAMomentView.getWidth() + (int) getResources().getDimension(R.dimen.padding20);
                circleAnimationImageView.requestLayout();

                questionLayout.getLayoutParams().height = questionLayout.getWidth();
                questionLayout.requestLayout();

                for (RelativeLayout answerLayout : answerLayouts) {
                    answerLayout.getLayoutParams().height = answerLayout.getWidth();
                    answerLayout.requestLayout();
                }
            }
        });

        startProgressBar.setVisibility(View.VISIBLE);
    }


    @UiThread
    public void loadContent(ABAMomentController.QuestionType type, ArrayList<Element> elements, String audio, int progress) {

        circleAnimationImageView.setVisibility(View.INVISIBLE);
        circleAnimationImageView.clearAnimation();

        //add the id for testing automator
        questionCircleABAMomentView.setContentDescription(elements.get(0).getId());
        for (int i = 0; i < circleABAMomentViews.length; i++) {
            circleABAMomentViews[i].setContentDescription(elements.get(i + 1).getId());
        }

        //change the question and answers circles color
        questionCircleABAMomentView.setColors(false);
        for (CircleABAMomentView circleABAMomentView : circleABAMomentViews) {
            circleABAMomentView.setColors(false);
        }

        state = abaMomentController.getCurrentIndex();

        //animate the progress bar
        ObjectAnimator progressAnimator;
        progressAnimator = ObjectAnimator.ofInt(progressBar, "progress", progressBar.getProgress(), progress);
        progressBar.setContentDescription("" + state);
        progressAnimator.setDuration(200);
        progressAnimator.start();

        //clear the "wrong answer" alpha animation
        for (RelativeLayout answerLayout : answerLayouts) answerLayout.clearAnimation();

        //if the question is a image and the answers are words
        if (type == QuestionType.imageto3text) {

            questionCircleABAMomentView.setText("");

            for (int i = 0; i < 3; i++)
                circleABAMomentViews[i].setText(elements.get(i + 1).getValue());

            imageLoader.displayImage(elements.get(0).getValue(), questionImageView, displayImageOptions);

            questionImageView.setVisibility(View.VISIBLE);
            for (ImageView imageView : answersImageViews) {
                imageView.setVisibility(View.INVISIBLE);
            }

        } else {

            questionCircleABAMomentView.setText(elements.get(0).getValue());

            for (CircleABAMomentView circleABAMomentView : circleABAMomentViews)
                circleABAMomentView.setText("");

            imageLoader.displayImage(elements.get(1).getValue(), firstAnswerImageView, displayImageOptions);
            imageLoader.displayImage(elements.get(2).getValue(), secondAnswerImageView, displayImageOptions);
            imageLoader.displayImage(elements.get(3).getValue(), thirdAnswerImageView, displayImageOptions);

            questionImageView.setVisibility(View.INVISIBLE);
            imageLoader.displayImage("", questionImageView);
            for (ImageView imageView : answersImageViews){
                imageView.setVisibility(View.VISIBLE);
            }

        }

        //prepare audio
        mediaPlayer = new ABAMomentPlayer(this, audio);

        setTouchAvailable(true);

    }

    @UiThread
    public void startNextQuestionAnimation(final ABAMomentController.QuestionType currentType, final ABAMomentController.QuestionType type, final ArrayList<Element> elements, final String audio, final int progress) {

        startProgressBar.setVisibility(View.INVISIBLE);

        circleAnimationImageView.setVisibility(View.INVISIBLE);
        circleAnimationImageView.clearAnimation();

        //clear the "good answer" check icon
        for (ImageView goodAnswerImageView : goodAnswerImageViews)
            goodAnswerImageView.setVisibility(View.INVISIBLE);

        AlphaAnimation transitionAnimation;
        if (abaMomentController.getCurrentIndex() != 0) {
            transitionAnimation = new AlphaAnimation(1f, 0.0f);
            transitionAnimation.setRepeatMode(Animation.REVERSE);
            transitionAnimation.setRepeatCount(1);
        } else {
            transitionAnimation = new AlphaAnimation(0.0f, 1.0f);
        }
        transitionAnimation.setDuration(500);
        transitionAnimation.setInterpolator(new DecelerateInterpolator());
        for (CircleABAMomentView circleABAMomentView : circleABAMomentViews) {
            circleABAMomentView.setVisibility(View.VISIBLE);
            circleABAMomentView.startAnimation(transitionAnimation);
        }

        questionCircleABAMomentView.setVisibility(View.VISIBLE);
        questionCircleABAMomentView.startAnimation(transitionAnimation);

        //different animation for the answers images
        AlphaAnimation imagesTransitionsAnimation = transitionAnimation;
        if (currentType == QuestionType.imageto3text && type == QuestionType.textto3image) {
            imagesTransitionsAnimation = new AlphaAnimation(0.0f, 1.0f);
            imagesTransitionsAnimation.setStartOffset(500);
        } else if (currentType == QuestionType.textto3image && type == QuestionType.imageto3text) {
            imagesTransitionsAnimation = new AlphaAnimation(1.0f, 0.0f);
        } else if (currentType == QuestionType.imageto3text && type == QuestionType.imageto3text) {
            imagesTransitionsAnimation = new AlphaAnimation(0.0f, 0.0f);
        }
        imagesTransitionsAnimation.setDuration(500);
        imagesTransitionsAnimation.setInterpolator(new DecelerateInterpolator());

        for (ImageView imageView : answersImageViews) {
            imageView.setVisibility(View.VISIBLE);
            imageView.startAnimation(imagesTransitionsAnimation);
        }

        questionImageView.setVisibility(View.VISIBLE);
        questionImageView.startAnimation(transitionAnimation);

        if (abaMomentController.getCurrentIndex() != 0) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    loadContent(type, elements, audio, progress);
                }
            }, 500);
        } else {
            loadContent(type, elements, audio, progress);
        }

    }

    @UiThread
    public void startCorrectAnswerAnimation(int index) {

        setTouchAvailable(false);

        //show the green check in the corner
        goodAnswerImageViews[index - 1].setVisibility(View.VISIBLE);

        //change the question and good answer circles color
        circleABAMomentViews[index - 1].setColors(true);
        questionCircleABAMomentView.setColors(true);

        circleAnimationImageView.setVisibility(View.VISIBLE);
        circleAnimationImageView.startAnimation(AnimationUtils.loadAnimation(this, R.anim.correct_anim));

        mediaPlayer.playAudio(MAX_WAITING_TIME_IN_SECONDS, new ABAMomentPlayer.ABAMomentPlayerListener() {
            @Override
            public void audioFinishedPlaying(boolean success) {
                abaMomentController.loadNextQuestion();
            }
        });
    }

    @UiThread
    public void startWrongAnswerAnimation(int index, boolean animatedQuestion) {

        boolean hasToFade = (answerLayouts[index].getAnimation() == null);

        if (animatedQuestion && hasToFade) {
            TranslateAnimation animation1 = new TranslateAnimation(-50, 0, 0, 0);
            animation1.setDuration(200);
            animation1.setFillAfter(true);
            animation1.setInterpolator(new BounceInterpolator());
            questionLayout.startAnimation(animation1);

            //play the error song
            try {
                AssetFileDescriptor afd = ABAApplication.get().getAssets().openFd("error.mp3");
                MediaPlayer player = new MediaPlayer();
                player.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                player.prepare();
                player.start();
                player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    @Override
                    public void onCompletion(MediaPlayer mp) {
                        mp.release();
                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (hasToFade) {
            AlphaAnimation animation1 = new AlphaAnimation(1f, 0.2f);
            animation1.setDuration(500);
            animation1.setFillAfter(true);
            answerLayouts[index].startAnimation(animation1);
        }
    }

    @UiThread
    public void showError(String error) {
        startProgressBar.setVisibility(View.INVISIBLE);

        View decorView = getWindow().getDecorView();
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
            int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        } else {
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }
        setTouchAvailable(false);
        Snackbar.make(decorView, error, Snackbar.LENGTH_INDEFINITE).setAction("OK", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        }).show();
    }

    @Click({R.id.quitButton})
    void quitClicked() {
        finish();
        CooladataMomentsTrackingManager.trackMomentFinishedClosed(userId, this.momentType, abaMomentId);
    }

    @Background
    public void loadImages(ArrayList<String> images) {
        for (String imageUrl : images) {
            imageLoader.loadImageSync(imageUrl);
        }

    }

    public void finishWithResult() {
        setResult(RESULT_OK, getIntent().putExtra(ABAMomentStartActivity_.MOMENT_ID_RESULT,abaMomentId));
        finish();
        CooladataMomentsTrackingManager.trackMomentFinished(userId, this.momentType, abaMomentId);
    }

    public String getAbaMomentsAsJsonString() {
        return abaMomentsAsJsonString;
    }

    public void setAbaMomentsAsJsonString(String abaMomentsAsJsonString) {
        this.abaMomentsAsJsonString = abaMomentsAsJsonString;
    }

    private class OnCircleABAMomentTouchListener implements View.OnTouchListener {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            CircleABAMomentView view = (CircleABAMomentView) v;
            int margin = (view.getHeight() - view.getWidth()) / 2;
            if (isTouchAvailable && !view.isQuestion() && (event.getY() > margin) && (event.getY() < (margin + view.getWidth()))) {
                abaMomentController.checkAnswer(view.getAnswerNumber());
            }
            return false;
        }
    }

    private void setTouchAvailable(boolean touchAvailable) {
        this.isTouchAvailable = touchAvailable;
    }

}
