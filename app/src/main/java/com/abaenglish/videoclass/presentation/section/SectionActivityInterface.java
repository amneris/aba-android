package com.abaenglish.videoclass.presentation.section;

/**
 * Created by jaume on 21/5/15.
 */
public interface SectionActivityInterface extends NextSectionDialog.ResultListener{

    String EXTRA_UNIT_ID = "UNIT_ID";

    void configTeacher();
    void finishSection(boolean sectionCompleted);
    void updateSectionProgress();
}
