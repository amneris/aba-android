package com.abaenglish.videoclass.presentation.section.assessment.result.interactor;

import android.os.Parcelable;

import com.google.auto.value.AutoValue;

/**
 * Created by xabierlosada on 26/07/16.
 */
@AutoValue public abstract class QualifyInput implements Parcelable {

    public abstract String unitId();
    public abstract int correctAnswers();

    public static Builder builder() {
        return new AutoValue_QualifyInput.Builder();
    }

    @AutoValue.Builder
    public static abstract class Builder {
        public abstract Builder unitId(String unitId);
        public abstract Builder correctAnswers(int correctAnswers);
        public abstract QualifyInput build();
    }
}