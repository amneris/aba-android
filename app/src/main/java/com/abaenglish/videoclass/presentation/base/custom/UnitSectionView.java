package com.abaenglish.videoclass.presentation.base.custom;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.domain.content.SectionController.SectionStatus;
import com.abaenglish.videoclass.presentation.base.ABAMasterActivity;
import com.abaenglish.videoclass.presentation.section.SectionType;

/**
 * Created by iaguila on 30/4/15.
 */
public class UnitSectionView extends RelativeLayout {

    private ImageView sectionImg;
    private ABATextView sectionNumber;
    private ABATextView sectionName;
    private ImageView sectionLocked;

    private RelativeLayout numberAndLockContrainerLayout;
    private RelativeLayout numberLockImageContrainerLayout;
    private RelativeLayout sectionImageContainer;
    private RelativeLayout masterLayout;

    private int sectionAreaWidth = 0;
    private int sectionCircleRadius = 0;
    private SectionType sectionType;

    public UnitSectionView(Context context, SectionType sectionType, int sectionAreaWidth, int sectionCircleRadius) {
        super(context);
        this.sectionType = sectionType;
        this.sectionAreaWidth = sectionAreaWidth;
        this.sectionCircleRadius = sectionCircleRadius;
        initiateView();
    }

    public UnitSectionView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initiateView();
    }

    public UnitSectionView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initiateView();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public UnitSectionView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initiateView();
    }

    // ================================================================================
    // Public methods
    // ================================================================================

    public RelativeLayout getNumberAndLockContrainerLayout() {
        return numberAndLockContrainerLayout;
    }

    public RelativeLayout getSectionImageContainerLayout() {
        return sectionImageContainer;
    }

    public ABATextView getNameForSectionContainerLayout() {
        return sectionName;
    }

    // ================================================================================
    // Private methods
    // ================================================================================

    private void initiateView() {
        if (sectionType != null) {
            // Adding layout
            addView(((ABAMasterActivity) getContext()).getLayoutInflater().inflate(R.layout.item_unit_section, null));

            // Important layouts
            numberLockImageContrainerLayout = (RelativeLayout) findViewById(R.id.unitSectionImgNumberLayout);
            masterLayout = (RelativeLayout) findViewById(R.id.unit_section_master_layout);
            numberAndLockContrainerLayout = (RelativeLayout) findViewById(R.id.unitSectionNumberLayout);

            // Section lock icon
            sectionLocked = (ImageView) findViewById(R.id.unitSectionLocked);

            // Section number icon
            sectionNumber = (ABATextView) findViewById(R.id.unitSectionNumber);
            sectionNumber.setABATag(getContext().getResources().getInteger(R.integer.typefaceSans700));

            // Section name
            sectionName = (ABATextView) findViewById(R.id.unitSectionTitle);
            sectionName.setABATag(getContext().getResources().getInteger(R.integer.typefaceSans700));

            // Section image
            sectionImg = (ImageView) findViewById(R.id.unitSectionImg);
            sectionImageContainer = (RelativeLayout) findViewById(R.id.sectionImageContainer);

            configSection();
        }
    }

    private void configSection() {

        // Layout param for small section number
        RelativeLayout.LayoutParams numberParamLayout = new RelativeLayout.LayoutParams(getSectionNumberImgRadius() * 2, getSectionNumberImgRadius() * 2);

        // Layout param for section bubble
        RelativeLayout.LayoutParams sectionImageContainerLayoutParams = (LayoutParams) sectionImageContainer.getLayoutParams();
        sectionImageContainerLayoutParams.width = sectionCircleRadius * 2;
        sectionImageContainerLayoutParams.height = sectionCircleRadius * 2;

        // Layout params for the whose section layout
        RelativeLayout.LayoutParams masterWriteLayoutParams = new RelativeLayout.LayoutParams(sectionAreaWidth, sectionAreaWidth);
        masterLayout.setLayoutParams(masterWriteLayoutParams);

        sectionLocked.setVisibility(INVISIBLE);
        sectionNumber.setVisibility(VISIBLE);
        sectionNumber.setText(String.valueOf(sectionType));
        sectionName.setText(sectionType.getSectionName(getContext()).toUpperCase());

        switch (sectionType) {
            case kABAFilm:
                sectionImg.setImageResource(R.mipmap.icon_unit_film);
                sectionImageContainerLayoutParams.setMargins(0, 0, 0, getSectionNumberImgRadius());

                numberParamLayout.addRule(ALIGN_BOTTOM, R.id.sectionImageContainer);
                numberParamLayout.addRule(CENTER_HORIZONTAL);
                numberParamLayout.setMargins(0, 0, 0, -getSectionNumberImgRadius());
                break;
            case kHabla:
                sectionImg.setImageResource(R.mipmap.icon_unit_speak);
                numberParamLayout.addRule(ALIGN_BOTTOM, R.id.sectionImageContainer);
                numberParamLayout.addRule(ALIGN_LEFT, R.id.sectionImageContainer);
                break;
            case kEscribe:
                sectionImg.setImageResource(R.mipmap.icon_unit_write);
                sectionImageContainerLayoutParams.setMargins(getSectionNumberImgRadius(), 0, 0, 0);

                numberParamLayout.addRule(ALIGN_LEFT, R.id.sectionImageContainer);
                numberParamLayout.addRule(CENTER_VERTICAL);
                numberParamLayout.setMargins(-getSectionNumberImgRadius(), 0, 0, 0);
                break;
            case kInterpreta:
                sectionImg.setImageResource(R.mipmap.icon_unit_interpret);
                numberParamLayout.addRule(ALIGN_TOP, R.id.sectionImageContainer);
                numberParamLayout.addRule(ALIGN_LEFT, R.id.sectionImageContainer);
                break;
            case kVideoClase:
                sectionImg.setImageResource(R.mipmap.icon_unit_videoclass);
                sectionImageContainerLayoutParams.setMargins(0, getSectionNumberImgRadius(), 0, 0);

                numberParamLayout.addRule(ALIGN_TOP, R.id.sectionImageContainer);
                numberParamLayout.addRule(CENTER_HORIZONTAL);
                numberParamLayout.setMargins(0, -getSectionNumberImgRadius(), 0, 0);
                break;
            case kEjercicios:
                sectionImg.setImageResource(R.mipmap.icon_unit_exercise);
                numberParamLayout.addRule(ALIGN_TOP, R.id.sectionImageContainer);
                numberParamLayout.addRule(ALIGN_RIGHT, R.id.sectionImageContainer);
                break;
            case kVocabulario:
                sectionImg.setImageResource(R.mipmap.icon_unit_vocabulary);
                sectionImageContainerLayoutParams.setMargins(0, 0, getSectionNumberImgRadius(), 0);

                numberParamLayout.addRule(ALIGN_RIGHT, R.id.sectionImageContainer);
                numberParamLayout.addRule(CENTER_VERTICAL);
                numberParamLayout.setMargins(0, 0, -getSectionNumberImgRadius(), 0);
                break;
            case kEvaluacion:
                sectionNumber.setVisibility(INVISIBLE);
                sectionLocked.setVisibility(VISIBLE);
                sectionImg.setImageResource(R.mipmap.icon_unit_evaluation);
                numberParamLayout.addRule(ALIGN_BOTTOM, R.id.sectionImageContainer);
                numberParamLayout.addRule(ALIGN_RIGHT, R.id.sectionImageContainer);
                break;
        }

        numberAndLockContrainerLayout.setLayoutParams(numberParamLayout);
        sectionImageContainer.setLayoutParams(sectionImageContainerLayoutParams);
    }

    // ================================================================================
    // Public methods
    // ================================================================================

    public void setSectionType(SectionType sectionType) {
        this.sectionType = sectionType;
        removeAllViews();
        initiateView();
    }

    public void updateWithSectionStatus(SectionStatus status) {

        switch (status) {
            case kABAStatusLocked: {
                sectionNumber.setVisibility(INVISIBLE);
                sectionLocked.setVisibility(VISIBLE);
                numberAndLockContrainerLayout.setSelected(false); // white
                sectionImageContainer.setBackgroundResource(R.drawable.selector_unit_number_background); // gray
                break;
            }
            case kABAStatusPending: {
                sectionNumber.setVisibility(VISIBLE);
                sectionLocked.setVisibility(INVISIBLE);
                numberAndLockContrainerLayout.setSelected(false); // white
                sectionImageContainer.setBackgroundResource(R.drawable.selector_unit_number_background); // gray
                break;
            }
            case kABAStatusActual: {
                sectionNumber.setVisibility(VISIBLE);
                sectionLocked.setVisibility(INVISIBLE);
                numberAndLockContrainerLayout.setSelected(true); // blue
                sectionImageContainer.setBackgroundResource(R.drawable.selector_unit_number_background); // gray
                break;
            }
            case kABAStatusCompleted: {
                sectionNumber.setVisibility(VISIBLE);
                sectionLocked.setVisibility(INVISIBLE);
                numberAndLockContrainerLayout.setSelected(false); // white
                sectionImageContainer.setBackgroundResource(R.drawable.selector_unit_number_background_completed); // green
//                sectionImageContainer.setBackground(ContextCompat.getDrawable(getContext(), R.drawable.selector_unit_number_background_completed)); // green
                break;
            }
        }
    }

    // ================================================================================
    // Private methods
    // ================================================================================

    private int getSectionNumberImgRadius() {
        return sectionCircleRadius / 3;
    }
}
