package com.abaenglish.videoclass.presentation.login;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.abaenglish.shepherd.ABAShepherdEditor;
import com.abaenglish.shepherd.configuration.configurators.abacore.ABACoreShepherdEnvironment;
import com.abaenglish.shepherd.plugin.plugins.ShepherdLoginPlugin;
import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.data.ConnectionService;
import com.abaenglish.videoclass.data.persistence.ABAAPIError;
import com.abaenglish.videoclass.domain.content.DataController;
import com.abaenglish.videoclass.domain.content.UserController;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.abaenglish.videoclass.presentation.base.ABAMasterSessionActivity;
import com.abaenglish.videoclass.presentation.base.custom.ABAProgressDialog;
import com.abaenglish.videoclass.presentation.base.custom.ABATextView;
import com.abaenglish.videoclass.presentation.base.custom.FontCache;
import com.abaenglish.videoclass.presentation.profile.ChangePasswordActivity;
import com.abaenglish.videoclass.presentation.register.RegisterActivity;
import com.abaenglish.videoclass.session.SessionService;
import com.bzutils.BZUtils;
import com.bzutils.LogBZ;
import com.jakewharton.rxbinding.widget.RxTextView;

import java.util.concurrent.TimeUnit;

import butterknife.BindColor;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func2;

import static com.abaenglish.shepherd.configuration.configurators.GenericShepherdConfigurator.ShepherdConfiguratorType;

/**
 * Created by iaguila on 24/3/15.
 * Refactored by Jesus 01/08/2016
 */
public class LoginActivity extends ABAMasterSessionActivity {

    // Resources
    @BindString(R.string.loginTitleSection)
    String loginActivityTitle;
    @BindString(R.string.regErrorEmailNil)
    String emptyEmailMessage;
    @BindString(R.string.regErrorEmailFormat)
    String invalidEmailFormatMessage;
    @BindString(R.string.regErrorPasswordNil)
    String emptyPasswordMessage;
    @BindString(R.string.registerButtonQuestion)
    String registerQuestion;
    @BindString(R.string.registerButtonAnswer)
    String registerAnswer;
    @BindColor(R.color.abaWhite)
    int whiteColorRes;

    @BindView(R.id.main_layout)
    View mainLayout;

    @BindView(R.id.tutorialItemLogo)
    ImageView abaLogoImageView;

    // email input
    @BindView(R.id.emailEditTextLayout)
    TextInputLayout emailInputLayout;
    @BindView(R.id.emailEditTextInput)
    TextInputEditText emailInputText;

    // Change password
    @BindView(R.id.loginChangePasswordButton)
    ABATextView changePasswordButton;

    // Password input
    @BindView(R.id.passwordEditTextLayout)
    TextInputLayout passwordInputLayout;
    @BindView(R.id.passwordEditTextInput)
    TextInputEditText passwordInputText;

    @BindView(R.id.loginActionButton)
    Button loginButton;
    @BindView(R.id.loginActionButtonDisabled)
    Button loginButtonDisabled;
    @BindView(R.id.loginFacebookButton)
    Button loginFacebookButton;

    // Register form option
    @BindView(R.id.tutorialItemLoginText)
    ABATextView registerTextView;

    // ================================================================================
    // Public methods - Activity lifecycle
    // ================================================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        configureUI();
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Storing successful key/password in order to get them the next time
        if (ABAShepherdEditor.isInternal()) {
            SharedPreferences settings = getSharedPreferences(ShepherdLoginPlugin.LAST_LOGIN_SHARED_PREFERENCES, Context.MODE_PRIVATE);
            emailInputText.setText(settings.getString(ShepherdLoginPlugin.LAST_LOGIN_EMAIL_KEY, ""));
            passwordInputText.setText(settings.getString(ShepherdLoginPlugin.LAST_LOGIN_PASS_KEY, ""));
        }
    }

    @Override
    public void onBackPressed() {
        // Opening tutorial
        openTutorial();
    }

    // ================================================================================
    // Private methods - UI
    // ================================================================================

    private void configureUI() {

        loginFacebookButton.setTypeface(fontCache.getTypeface(FontCache.ABATypeface.sans500));

        registerTextView.setText(Html.fromHtml(registerQuestion + "<font color='#07BCE6'> " + registerAnswer + "</font>"));

        // continuing password field tuning
        passwordInputLayout.setTypeface(Typeface.DEFAULT);
        passwordInputText.setTransformationMethod(new PasswordTransformationMethod());
        passwordInputText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);

        // listener to finish the form
        passwordInputText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    validateFormAndLogin();
                }
                return false;
            }
        });

        // Setting focus change listener in order to animate the form up
        emailInputText.setOnFocusChangeListener(logoHiderFocusListener);
        passwordInputText.setOnFocusChangeListener(logoHiderFocusListener);

        // Forcing the focus not to be in the first EditText by default
        mainLayout.requestFocus();
        updateLoginButtonStyle(false); // Disabling register button
        passwordInputText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        passwordInputLayout.setErrorEnabled(true);
        passwordInputText.setError(null);
        emailInputLayout.setErrorEnabled(true);
        emailInputText.setError(null);

        // RxTextView.textChanges(emailInputText).compose(invalidateInput(emailInputLayout));
        Observable<CharSequence> emailSource = streamFromField(emailInputText, emailInputLayout);
        //RxTextView.textChanges(passwordInputText).compose(invalidateInput(passwordInputLayout));
        Observable<CharSequence> passwordSource = streamFromField(passwordInputText, passwordInputLayout);

        rx.Observable.combineLatest(
                emailSource,
                passwordSource,
                new Func2<CharSequence, CharSequence, Boolean>() {
                    @Override
                    public Boolean call(CharSequence emailSequence, CharSequence passSequence) {
//                        if (passSequence.length() > 0) validatePassword();
//                        if (emailSequence.length() > 0) validateEmail();
                        return isEmailValid() && isPasswordValid();
                    }
                })
                .subscribe(new Action1<Boolean>() {
                    @Override
                    public void call(Boolean aBoolean) {
                        updateLoginButtonStyle(aBoolean);
                    }
                });
    }

    // ================================================================================
    // Public methods - Onclick
    // ================================================================================

    @OnClick({R.id.loginActionButton, R.id.loginActionButtonDisabled})
    protected void login() {
        if (!isViewDisableForClick()) {
            validateFormAndLogin();
        }
    }

    @OnClick(R.id.tutorialItemLoginText)
    protected void register() {
        startActivity(new Intent(this, RegisterActivity.class));
        finish();
    }

    @OnClick(R.id.loginChangePasswordButton)
    protected void changePassword() {
        Intent intent = new Intent(this, ChangePasswordActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.loginFacebookButton)
    protected void facebookLogin() {
        loginWithFacebook();
    }

    // ================================================================================
    // Private methods - Form validation
    // ================================================================================

    private void validateFormAndLogin() {

        if (!isEmailValid()) {
            validateEmail();
            highlightErrorAtInputText(emailInputText);
            return;
        }

        if (!isPasswordValid()) {
            validatePassword();
            highlightErrorAtInputText(passwordInputText);
            return;
        }

        loginUser();
    }

    private boolean isEmailValid() {
        return !emailInputText.getText().toString().isEmpty() && UserController.isEmailValid(emailInputText.getText().toString());
    }

    /**
     * Password should not be empty
     *
     * @return true if password is not empty
     */
    private boolean isPasswordValid() {
        return !passwordInputText.getText().toString().isEmpty() && UserController.isPasswordValid(passwordInputText.getText().toString());
    }


    // ================================================================================
    // Private methods - Error visualization
    // ================================================================================

    private void highlightErrorAtInputText(final TextInputEditText fieldError) {
        fieldError.setTextColor(ContextCompat.getColor(this, R.color.recordRedColor));
        fieldError.setHintTextColor(ContextCompat.getColor(this, R.color.recordRedColor));

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                fieldError.setTextColor(whiteColorRes);
                fieldError.setHintTextColor(whiteColorRes);
            }
        }, 3000);

        BZUtils.showKeyboard(this, fieldError);
    }

    private void validateEmail() {
        if (!isEmailValid()) {
            emailInputLayout.setError(getString(R.string.regErrorEmailFormat));
        } else {
            emailInputLayout.setError(null);
        }
    }

    private void validatePassword() {
        if (!isPasswordValid()) {
            passwordInputLayout.setError(getString(R.string.regErrorPasswordMin));
        } else {
            passwordInputLayout.setError(null);
        }
    }

    private void updateLoginButtonStyle(boolean isActive) {
        if (isActive) {
            loginButton.setVisibility(View.VISIBLE);
            loginButtonDisabled.setVisibility(View.GONE);
        } else {
            loginButton.setVisibility(View.GONE);
            loginButtonDisabled.setVisibility(View.VISIBLE);
        }
    }

    // ================================================================================
    // Private methods - Interactor stuff
    // ================================================================================

    private void loginUser() {
        showProgressDialog(ABAProgressDialog.SimpleDialog);
        mSessionService.loginWithEmail(this, getEmail(), getPassword(), new DataController.ABASimpleCallback() {
            @Override
            public void onSuccess() {
                DataStore.getInstance().getEvaluationController().saveProgressActionForSection(getRealm(), null, null, null, null, false, false);
                ConnectionService.getInstance().getPublicIP(new ConnectionService.ConnectionServiceCallback() {
                    @Override
                    public void finished(String ipAddress) {
                        logInForUser();
                    }
                });
            }

            @Override
            public void onError(ABAAPIError error) {
                dismissProgressDialog();
                LogBZ.e(error.getError());
                error.showABANotificationError(LoginActivity.this);
            }
        }, tracker);
    }

    private void logInForUser() {
        // Storing successful key/password in order to get them the next time
        if (ABAShepherdEditor.isInternal()) {
            SharedPreferences settings = getSharedPreferences(ShepherdLoginPlugin.LAST_LOGIN_SHARED_PREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = settings.edit();
            editor.putString(ShepherdLoginPlugin.LAST_LOGIN_EMAIL_KEY, getEmail());
            editor.putString(ShepherdLoginPlugin.LAST_LOGIN_PASS_KEY, getPassword());
            editor.apply();

            ABACoreShepherdEnvironment abaCoreEnvironment = (ABACoreShepherdEnvironment) ABAShepherdEditor.shared(LoginActivity.this).environmentForShepherdConfigurationType(LoginActivity.this, ShepherdConfiguratorType.kShepherdConfiguratorTypeABACore);
            ShepherdLoginPlugin.addLoginOption(LoginActivity.this, getEmail(), getPassword(), abaCoreEnvironment.getEnvironmentName());
        }

        dismissProgressDialog();
        tracker.trackUserLogin(UserController.getInstance().getCurrentUser(getRealm()).getUserId(), "mail");
        startABAForUser(false);
    }

    private String getEmail() {
        return emailInputText.getText().toString();
    }

    private String getPassword() {
        return passwordInputText.getText().toString();
    }

    // ================================================================================
    // Private methods - RX
    // ================================================================================

    private Observable<CharSequence> streamFromField(TextInputEditText editText, final TextInputLayout layout) {
        return RxTextView
                .textChanges(editText)
                .doOnNext(new Action1<CharSequence>() {
                    @Override
                    public void call(CharSequence charSequence) {
                        layout.setError(null);
                    }
                })
                .debounce(500, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread());
    }

    // ================================================================================
    // Private classes - Listeners
    // ================================================================================

    /**
     * Listener to hide logo and move texts up
     */
    private View.OnFocusChangeListener logoHiderFocusListener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            abaLogoImageView.animate().alpha(0.0f);
            abaLogoImageView.setVisibility(View.GONE);

            if (passwordInputText.length() > 0) validatePassword();
            if (emailInputText.length() > 0) validateEmail();
        }
    };

}