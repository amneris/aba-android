package com.abaenglish.videoclass.presentation.base.custom;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.EditText;

/**
 * Created by iaguila on 25/3/15.
 */
public class ABAEditText extends EditText {
    public ABAEditText(Context context) {
        super(context);
    }

    public ABAEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ABAEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ABAEditText(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }
}
