package com.abaenglish.videoclass.presentation.plan;

import android.content.Context;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.data.persistence.ABAPlan;
import com.abaenglish.videoclass.presentation.base.custom.FontCache;

import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

/**
 * Created by Jesus Espejo using mbp13jesus on 21/03/17.
 */
@EViewGroup
abstract class AbstractPlanCell extends RelativeLayout {

    @ViewById
    TextView textViewPriceInteger,
            textViewPriceDecimal,
            textViewPriceMonth,
            textViewPeriod;

    @ViewById
    ImageView itemPlanButton;

    @ViewById
    ViewGroup itemPlanMainLayout;

    protected PlanCellClickListener listener;
    protected int cellIndex;

    public AbstractPlanCell(Context context) {
        super(context);
    }

    void updateView(ABAPlan planToRender, FontCache fontCache, int index) {

        this.cellIndex = index;

        textViewPriceInteger.setTypeface(fontCache.getTypeface(FontCache.ABATypeface.slab700));
        textViewPriceDecimal.setTypeface(fontCache.getTypeface(FontCache.ABATypeface.slab700));
        textViewPriceMonth.setTypeface(fontCache.getTypeface(FontCache.ABATypeface.sans500));
        textViewPeriod.setTypeface(fontCache.getTypeface(FontCache.ABATypeface.slab500));

        switch (planToRender.getDays()) {
            case 30:
                textViewPeriod.setText(getContext().getText(R.string.planCellPeriodMontly));
                itemPlanButton.setImageResource(R.drawable.bt_off_mobile);
                break;
            case 180:
                textViewPeriod.setText(getContext().getText(R.string.planCellPeriodSixMontly));
                itemPlanButton.setImageResource(R.drawable.bt_off_mobile);
                break;
            case 360:
                textViewPeriod.setText(getContext().getText(R.string.planCellPeriodYearly));
                itemPlanButton.setImageResource(R.drawable.bt_on_mobile);
                break;
        }
    }

    public void setPlanCellListener(PlanCellClickListener listener) {
        this.listener = listener;
    }

    // ================================================================================
    // Private methods
    // ================================================================================

    @Click(R.id.itemPlanMainLayout)
    protected void cellClicked() {
        if (listener != null) {
            listener.clickedItem(this.cellIndex);
        }
    }
}


