package com.abaenglish.videoclass.presentation.plan;

import android.content.Context;
import android.text.Spannable;
import android.text.Spanned;
import android.text.style.StrikethroughSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.analytics.TrackerContract;
import com.abaenglish.videoclass.analytics.cooladata.navigation.CooladataNavigationTrackingManager;
import com.abaenglish.videoclass.analytics.modern.FabricTrackingManager;
import com.abaenglish.videoclass.analytics.modern.MixpanelTrackingManager;
import com.abaenglish.videoclass.data.persistence.ABAAPIError;
import com.abaenglish.videoclass.data.persistence.ABAPlan;
import com.abaenglish.videoclass.data.persistence.ABAUser;
import com.abaenglish.videoclass.domain.content.DataController;
import com.abaenglish.videoclass.domain.content.PlanController;
import com.abaenglish.videoclass.domain.content.UserController;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.abaenglish.videoclass.presentation.base.ABAMasterActivity;
import com.abaenglish.videoclass.presentation.base.custom.ABATextView;
import com.android.vending.billing.IInAppBillingService;
import com.crashlytics.android.Crashlytics;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by madalin on 17/09/15.
 * Refactored by Xilosada and Jespejo on 03/08/2016
 */
class PlansAdapter extends BaseAdapter implements View.OnClickListener {

    private LayoutInflater mLayoutInflate;
    private Context mContext;

    private RealmConfiguration realmConfiguration;
    private final TrackerContract.Tracker tracker;
    private final boolean sixMonthsTierActive;

    PlansAdapter(Context context, RealmConfiguration realmConfiguration, TrackerContract.Tracker tracker, boolean sixMonthsTierActive) {
        mContext = context;
        mLayoutInflate = LayoutInflater.from(context);
        this.realmConfiguration = realmConfiguration;
        this.tracker = tracker;
        this.sixMonthsTierActive = sixMonthsTierActive;
    }


    // ================================================================================
    // Public methods - BaseAdapter lifecycle
    // ================================================================================

    @Override
    public int getCount() {
        Realm realmToRead = Realm.getInstance(this.realmConfiguration);
        int numberOfPlans = getPlans(realmToRead).size();
        realmToRead.close();
        return numberOfPlans;
    }

    @Override
    public Object getItem(int position) {
        // Returning Objecto due to realm problems
        return new Object();
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    private static final StrikethroughSpan STRIKE_THROUGH_SPAN = new StrikethroughSpan();

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        PlanCell itemViewHolder;
        //WHAT THE FUCK
        convertView = mLayoutInflate.inflate(R.layout.item_payment, parent, false);

        itemViewHolder = new PlanCell();
        itemViewHolder.planLayout = (ViewGroup) convertView.findViewById(R.id.plan_layout);
        itemViewHolder.monthText = (ABATextView) convertView.findViewById(R.id.monthText);
        itemViewHolder.discountPriceForMonthText = (ABATextView) convertView.findViewById(R.id.discountPriceForMonthtext);
        itemViewHolder.originalPriceForMonthText = (ABATextView) convertView.findViewById(R.id.originalPriceForMonthtext);
        itemViewHolder.infoText1 = (ABATextView) convertView.findViewById(R.id.infoText1);
        itemViewHolder.infoText2 = (ABATextView) convertView.findViewById(R.id.infoText2);
        itemViewHolder.selectButton = (Button) convertView.findViewById(R.id.selectButton);
        itemViewHolder.planIs2x1 = (ImageView) convertView.findViewById(R.id.planIs2x1);

        Realm realmToRead = Realm.getInstance(this.realmConfiguration);

        ABAPlan abaPlan = getPlanAtIndex(position, realmToRead);
        if (abaPlan != null) {

            String discountMonthlyFee = abaPlan.getCurrencySymbol() + PlanController.getPriceForOneMonth(abaPlan.getDiscountPrice(), abaPlan.getDays()) + "/" + convertView.getResources().getString(R.string.mes);
            String originalMonthlyFee = abaPlan.getCurrencySymbol() + PlanController.getPriceForOneMonth(abaPlan.getOriginalPrice(), abaPlan.getDays()) + "/" + convertView.getResources().getString(R.string.mes);

            itemViewHolder.monthText.setText(PlanController.getMonthText(abaPlan, convertView.getResources()));
            itemViewHolder.discountPriceForMonthText.setText(discountMonthlyFee);
            itemViewHolder.originalPriceForMonthText.setText(originalMonthlyFee);
            itemViewHolder.infoText1.setText(abaPlan.getCurrencySymbol() + PlanController.getPriceWithCommaFormat(Float.toString(abaPlan.getDiscountPrice())) + " " + convertView.getResources().getString(R.string.chargedEveryKey));

            // Hiding original price if there is not any discount
            if (abaPlan.getDiscountIdentifier().equalsIgnoreCase(abaPlan.getOriginalIdentifier())) {
                // Hiding
                itemViewHolder.originalPriceForMonthText.setVisibility(View.GONE);
            } else {
                // Otherwise adding strike-through
                itemViewHolder.originalPriceForMonthText.setText(originalMonthlyFee, TextView.BufferType.SPANNABLE);
                Spannable spannable = (Spannable) itemViewHolder.originalPriceForMonthText.getText();
                spannable.setSpan(STRIKE_THROUGH_SPAN, 0, itemViewHolder.originalPriceForMonthText.getText().length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            }

            // '2 x 1' option
            if (abaPlan.getPlanIs2x1() == 0) {
                itemViewHolder.planIs2x1.setVisibility(View.GONE);
            } else {
                itemViewHolder.planIs2x1.setVisibility(View.VISIBLE);
            }

            itemViewHolder.selectButton.setTag(position);
            itemViewHolder.planLayout.setTag(position);

            itemViewHolder.selectButton.setOnClickListener(this);
            itemViewHolder.planLayout.setOnClickListener(this);
        }

        realmToRead.close();

        return convertView;
    }

    // ================================================================================
    // Public methods - View.OnClickListener
    // ================================================================================

    @Override
    public void onClick(View v) {
        int id = v.getId();
        if (id == R.id.selectButton || id == R.id.plan_layout) {
            // For Test Purchase with Google productsExample
            // DataStore.getInstance().getPlanController().deleteTestPurchase(mContext, MenuActivity.mService);
            // DataStore.getInstance().getPlanController().subscribeABAPlan(mContext, MenuActivity.mService, "android.test.purchased");

            Integer selectedPlanIndex = (Integer) v.getTag();

            // Reading plan if there is any
            Realm realmToRead = Realm.getInstance(this.realmConfiguration);
            ABAPlan selectedPlan = getPlanAtIndex(selectedPlanIndex, realmToRead);
            if (selectedPlan != null) {

                // Reading user
                final String identifier = selectedPlan.getDiscountIdentifier();
                ABAUser user = UserController.getInstance().getCurrentUser(realmToRead);

                // Sending 'purchase intention' to analytics
                trackPurchaseIntention(selectedPlan, user.getUserId());

                DataStore.getInstance().getPlanController(tracker).getBillingServices(mContext, new DataController.ABAAPICallback<IInAppBillingService>() {
                    @Override
                    public void onSuccess(IInAppBillingService response) {
                        boolean purchaseAttemptSuccessful = DataStore.getInstance().getPlanController(tracker).subscribeToPlan(mContext, response, identifier);
                        if (!purchaseAttemptSuccessful) {
                            ((ABAMasterActivity) mContext).showABAErrorNotification(mContext.getString(R.string.errorRegister));
                        }
                    }

                    @Override
                    public void onError(ABAAPIError error) {
                        ((ABAMasterActivity) mContext).showABAErrorNotification(mContext.getString(R.string.errorRegister));
                    }
                });
            }

            realmToRead.close();
        }
    }

    // ================================================================================
    // Private methods
    // ================================================================================

    private List<ABAPlan> getPlans(Realm realmToLoad) {
        List<ABAPlan> plans = PlanController.getPlanContent(realmToLoad);
        List<ABAPlan> activePlans = new ArrayList<>();
        if (!sixMonthsTierActive) {
            for (ABAPlan plan : plans) {
                if (plan.getDays() != 180) {
                    activePlans.add(plan);
                }
            }
            return activePlans;
        } else {
            return plans;
        }
    }


    private ABAPlan getPlanAtIndex(int index, Realm realmToLoad) {
        List<ABAPlan> availablePlans = getPlans(realmToLoad);
        if (index < availablePlans.size()) {
            return availablePlans.get(index);
        } else {
            return null;
        }
    }

    private void trackPurchaseIntention(ABAPlan selectedPlan, String userId) {
        Crashlytics.log(Log.INFO, "Plans", "User selects plan " + selectedPlan.getPlanTitle());

        MixpanelTrackingManager.getSharedInstance(mContext).trackPricingSelected(selectedPlan);
        CooladataNavigationTrackingManager.trackSubscriptionSelected(userId, selectedPlan.getDays());
        FabricTrackingManager.trackPricingSelected(selectedPlan);
        ((ABAMasterActivity) mContext).tracker.trackSelectedPlan(selectedPlan.getDiscountPrice(), selectedPlan.getCurrency(), String.valueOf(selectedPlan.getOriginalPrice() - selectedPlan.getDiscountPrice()), String.valueOf(selectedPlan.getDays()));

    }

    // ================================================================================
    // Private classes
    // ================================================================================

    private class PlanCell {
        private ViewGroup planLayout;
        private ABATextView monthText;
        private ABATextView discountPriceForMonthText;
        private ABATextView originalPriceForMonthText;
        private ABATextView infoText1;
        private ABATextView infoText2;
        private Button selectButton;
        private ImageView planIs2x1;
    }
}
