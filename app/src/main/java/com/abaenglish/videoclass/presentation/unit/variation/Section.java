package com.abaenglish.videoclass.presentation.unit.variation;

import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;

/**
 * Created by xabierlosada on 10/10/16.
 */

public interface Section {

    enum SectionType {
        FILM(1), SPEAK(2), WRITE(3), INTERPRET(4), VIDEOCLASS(5), VOCABULARY(6), EXERCISE(7), ASSESSMENT(8);


        private final int value;

        SectionType(final int newValue) {
            value = newValue;
        }

        public int getValue() { return value; }


    }

    @StringRes int getName();

    @DrawableRes int getIcon();

    int getCompletedPercentage();

    boolean isUnlocked();

    SectionType getType();

}
