package com.abaenglish.videoclass.presentation.section.vocabulary;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListAdapter;

import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.data.persistence.ABAPhrase;
import com.abaenglish.videoclass.presentation.base.custom.ABATextView;

import io.realm.RealmList;

/**
 * Created by madalin on 19/05/15.
 */
public class VocabularyAdapter extends BaseAdapter implements ListAdapter {

    private Context mContext;
    private LayoutInflater mLayoutInflate;
    private RealmList<ABAPhrase> listVocabulary;
    private int currentWord;
    private boolean CompletedMode;

    public VocabularyAdapter(Context context, RealmList<ABAPhrase> listVocabulary, boolean completedMode) {
        mContext = context;
        mLayoutInflate = LayoutInflater.from(context);
        this.listVocabulary = listVocabulary;
        this.CompletedMode = completedMode;
    }

    @Override
    public int getCount() {
        if (listVocabulary != null) {
            return listVocabulary.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int position) {
        if (listVocabulary != null) {
            return listVocabulary.get(position);
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public int getPhrasePosition(ABAPhrase phrase) {
        return listVocabulary.indexOf(phrase);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ItemViewHolder mItemViewHolder;

        if (convertView == null) {
            mItemViewHolder = new ItemViewHolder();
            convertView = mLayoutInflate.inflate(R.layout.item_vocabulary_word, parent, false);
            mItemViewHolder.englishText = (ABATextView) convertView.findViewById(R.id.englishWordText);
            mItemViewHolder.translationText = (ABATextView) convertView.findViewById(R.id.translationText);
            mItemViewHolder.vocabularyBackground = (LinearLayout) convertView.findViewById(R.id.vocabularyBackground);
            convertView.setTag(mItemViewHolder);

        } else {
            mItemViewHolder = (ItemViewHolder) convertView.getTag();
        }


        ABAPhrase vocabularyItem = (ABAPhrase) getItem(position);

        mItemViewHolder.englishText.setText(vocabularyItem.getText());
        if (!("null").equals(vocabularyItem.getTranslation())) {
            mItemViewHolder.translationText.setText(vocabularyItem.getWordType() + " " + vocabularyItem.getTranslation());
        } else {
            mItemViewHolder.translationText.setText("");
        }

        if (vocabularyItem.isDone()) {
            mItemViewHolder.englishText.setTextColor(ContextCompat.getColor(mContext, R.color.abaEmailGreen));
            if (!CompletedMode) {
                mItemViewHolder.englishText.setTypeface(Typeface.DEFAULT_BOLD);
            }
        } else {
            mItemViewHolder.englishText.setTypeface(Typeface.DEFAULT);
            mItemViewHolder.englishText.setTextColor(ContextCompat.getColor(mContext, R.color.abaGrey));
        }


        if (currentWord != position) {
            mItemViewHolder.vocabularyBackground.setBackgroundColor(ContextCompat.getColor(mContext, R.color.abaWhite));
            mItemViewHolder.englishText.setTypeface(Typeface.DEFAULT);
        } else if (!CompletedMode) {
            mItemViewHolder.englishText.setTypeface(Typeface.DEFAULT_BOLD);
            mItemViewHolder.vocabularyBackground.setBackgroundColor(ContextCompat.getColor(mContext, R.color.abaBackgroundGrey));
        } else {
            mItemViewHolder.vocabularyBackground.setBackgroundColor(ContextCompat.getColor(mContext, R.color.abaBackgroundGrey));
        }


        return convertView;
    }

    private class ItemViewHolder {
        private ABATextView englishText;
        private ABATextView translationText;
        private LinearLayout vocabularyBackground;
    }

    public void setCurrentWord(int pressedPosition) {
        this.currentWord = pressedPosition;
    }

    public int getCurrentWord() {
        return this.currentWord;
    }
}
