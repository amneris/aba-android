package com.abaenglish.videoclass.presentation.section.vocabulary;

import android.app.FragmentManager;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.abaenglish.videoclass.ABAApplication;
import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.analytics.cooladata.navigation.CooladataNavigationTrackingManager;
import com.abaenglish.videoclass.analytics.cooladata.study.CooladataStudyTrackingManager;
import com.abaenglish.videoclass.data.persistence.ABAPhrase;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.data.persistence.ABAUser;
import com.abaenglish.videoclass.data.persistence.ABAVocabulary;
import com.abaenglish.videoclass.domain.content.SectionController;
import com.abaenglish.videoclass.domain.content.UserController;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.abaenglish.videoclass.presentation.base.ABAMasterSectionActivity;
import com.abaenglish.videoclass.presentation.base.custom.ABATextView;
import com.abaenglish.videoclass.presentation.base.custom.ListenAndRecordControllerView;
import com.abaenglish.videoclass.presentation.base.custom.TeacherBannerView;
import com.abaenglish.videoclass.presentation.section.NextSectionDialog_;
import com.abaenglish.videoclass.presentation.section.SectionActivityInterface;
import com.abaenglish.videoclass.presentation.section.SectionHelper;
import com.abaenglish.videoclass.presentation.section.SectionType;
import com.bzutils.BZScreenHelper;
import com.crashlytics.android.Crashlytics;

import io.realm.RealmList;

import static com.abaenglish.videoclass.analytics.cooladata.CooladataTrackingDefinitions.CooladataSectionType.ABAVocabulary;

/**
 * Created by madalin on 7/05/15.
 * Edited by Jesus with regard of Cooladata on 25/04/16
 */
public class ABAVocabularyActivity extends ABAMasterSectionActivity implements View.OnClickListener, AdapterView.OnItemClickListener, SectionActivityInterface, ListenAndRecordControllerView.PlayerControlsListener, ListenAndRecordControllerView.SectionControlsListener {

    private ABAUnit currentUnit;
    private ABAUser currentUser;
    private ABAPhrase currentPhrase;
    private ABAVocabulary section;

    private LinearLayout topBarWordtCounter;
    private ABATextView textTopWordsCounter;
    private int currentVocabularyWords;
    private TeacherBannerView teacherBannerView;
    private ListView vocabularyListview;
    private VocabularyAdapter vocabularyAdapter;
    private RealmList<ABAPhrase> realmListVocabulary;
    private RelativeLayout teacherView;
    private boolean completedMode = false; // para el modo completed

    // ================================================================================
    // Lifecycle
    // ================================================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vocabulary);

        initApp(getIntent().getExtras().getString(EXTRA_UNIT_ID));
        configActionBar();
        configTeacher();
        configTopBar();
        configBottomController();
        config();

        setCurrentPhrase();

        setNextAudio(false);

        trackSection();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishSection(false);
    }

    // ================================================================================
    // Private methods - Activity configuration
    // ================================================================================

    private void initApp(final String UnitID) {
        currentUnit = DataStore.getInstance().getLevelUnitController().getUnitWithId(getRealm(), UnitID);
        currentUser = DataStore.getInstance().getUserController().getCurrentUser(getRealm());
        section = currentUnit.getSectionVocabulary();
        realmListVocabulary = section.getContent();
        completedMode = DataStore.getInstance().getVocabularyController().isSectionCompleted(currentUnit.getSectionVocabulary());
    }

    private void configActionBar() {
        findViewById(R.id.toolbarLeftButton).setOnTouchListener(((ABAApplication) getApplicationContext()).getImagePressedState(null));
        findViewById(R.id.toolbarLeftButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishSection(false);
            }
        });

        updateSectionProgress();
    }

    // ================================================================================
    // Private methods
    // ================================================================================

    private void setCurrentPhrase() {
        if (completedMode) {
            currentPhrase = null;
        } else {
            currentPhrase = DataStore.getInstance().getVocabularyController().getCurrentPhraseForSection(currentUnit.getSectionVocabulary(), SectionController.ContinueWithFirstUndoneWord);
        }
    }

    private void setNextAudio(boolean autoPlay) {
        if (currentPhrase != null) {
            bottomControllerView.setCurrentPhrase(currentPhrase);
            if (vocabularyListview != null) { // if it's init
                vocabularyListview.post(new Runnable() {
                    @Override
                    public void run() {
                        int position = vocabularyAdapter.getPhrasePosition(currentPhrase);
                        selectWord(position);
                    }
                });
            }

            if (autoPlay)
                bottomControllerView.startListenCurrentPhrase();
        } else {
            bottomControllerView.startCompleteMode();
            scrollToLastPosition();
            vocabularyAdapter.setCurrentWord(-1);
        }
    }

    private void configTopBar() {
        ABAVocabulary abaVocabulary = realmListVocabulary.get(0).getAbaVocabulary();
        currentVocabularyWords = DataStore.getInstance().getVocabularyController().getTotalPhrasesDoneForVocabulary(abaVocabulary);
        topBarWordtCounter = (LinearLayout) findViewById(R.id.topWordsCounter);
        textTopWordsCounter = (ABATextView) findViewById(R.id.textTopWordsCounter);
        textTopWordsCounter.setText(Html.fromHtml("<b>" + "<big>" + currentVocabularyWords + "</big>" + " " + "<small>" + getResources().getString(R.string.sectionVocabularyTitledeKey) + "</small>" + " "
                + "<big>" + "<font color='#FFFFFF'>" + realmListVocabulary.size() + "</font>" + "</big>" + "</b>"));
    }

    private void configBottomController() {
        bottomControllerView = (ListenAndRecordControllerView) findViewById(R.id.listenAndRecordController);
        bottomControllerView.unregisterListeners();
        bottomControllerView.restoreListenersForCurrentSection();
        bottomControllerView.setCurrentUnit(currentUnit);
        bottomControllerView.setPlayerControlsListener(this);
        bottomControllerView.setSectionControlsListener(this);
        bottomControllerView.setSectionType(SectionType.kVocabulario);
    }

    private void config() {
        vocabularyListview = (ListView) findViewById(R.id.vocabularyList);
        vocabularyListview.setDividerHeight(0);

        vocabularyAdapter = new VocabularyAdapter(this, realmListVocabulary, completedMode);
        vocabularyListview.setAdapter(vocabularyAdapter);

        vocabularyListview.setVisibility(View.VISIBLE);
        vocabularyListview.setOnItemClickListener(this);

        vocabularyListview.post(new Runnable() {
            @Override
            public void run() {
                topBarWordtCounter.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
                int mActionBarSize = (int) getTheme().obtainStyledAttributes(new int[]{android.R.attr.actionBarSize}).getDimension(0, 0);
                int listviewHeight = BZScreenHelper.getScreenHeight(getApplicationContext()) - topBarWordtCounter.getHeight()
                        - getStatusBarHeight() - mActionBarSize;
                int topPadding = listviewHeight - (int) getResources().getDimension(R.dimen.listenAndrecordControllerHeight) - getItemHeight(vocabularyListview, 0);
                vocabularyListview.setPadding(0, topPadding, 0, (int) getResources().getDimension(R.dimen.listenAndrecordControllerHeight));
                vocabularyListview.setClipToPadding(false);
                vocabularyListview.setVisibility(View.INVISIBLE);


                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (currentUnit.getSectionVocabulary().getProgress() == 0) {
                            Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_in_from_bottom);
                            vocabularyListview.startAnimation(animation);
                        }
                        if (!DataStore.getInstance().getVocabularyController().isSectionCompleted(section)) {
                            int position = vocabularyAdapter.getPhrasePosition(currentPhrase);
                            selectWord(position);
                            vocabularyListview.setVisibility(View.VISIBLE);
                        } else {
                            scrollToLastPosition();
                        }

                    }
                }, 100);
            }
        });
    }

    private int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    private boolean areOtherSectionsCompleted(ABAUnit currentUnit) {
        return currentUnit.getSectionFilm().isCompleted()
                && currentUnit.getSectionSpeak().isCompleted()
                && currentUnit.getSectionWrite().isCompleted()
                && currentUnit.getSectionInterpret().isCompleted()
                && currentUnit.getSectionVideoClass().isCompleted()
                && currentUnit.getSectionExercises().isCompleted();
    }

    private void scrollToLastPosition() {
        selectWord(realmListVocabulary.size() - 1);
        vocabularyListview.setVisibility(View.VISIBLE);
    }

    // ================================================================================
    // OnClickListener
    // ================================================================================

    @Override
    public void onClick(View v) {
        teacherBannerView.setVisibility(View.GONE);
        topBarWordtCounter.setVisibility(View.VISIBLE);
    }

    // ================================================================================
    // SessionActivityInterface
    // ================================================================================

    @Override
    public void updateSectionProgress() {
        ABATextView unitSectionTitle = (ABATextView) findViewById(R.id.toolbarTitle);
        ABATextView unitSectionProgress = (ABATextView) findViewById(R.id.toolbarSubTitle);

        unitSectionTitle.setText(R.string.unitMenuTitle7Key);
        unitSectionProgress.setText(DataStore.getInstance().getVocabularyController().getPercentageForSection(section));
        configTopBar();
    }

    @Override
    public void configTeacher() {
        ABAUser currentUser = DataStore.getInstance().getUserController().getCurrentUser(getRealm());
        teacherBannerView = (TeacherBannerView) findViewById(R.id.detailUnitTeacherView);
        teacherView = (RelativeLayout) findViewById(R.id.TeacherView);

        if (currentUnit.getSectionVocabulary().getProgress() == 0 && currentVocabularyWords == 0) {
            if (DataStore.getInstance().getLevelUnitController().checkIfFileExist(currentUnit, currentUser.getTeacherImage())) {
                DataStore.getInstance().getLevelUnitController().displayImage(null, currentUser.getTeacherImage(), teacherBannerView.getImageView());
            } else {
                teacherBannerView.setImageUrl(currentUser.getTeacherImage());
            }
            teacherBannerView.setText(getString(R.string.sectioVocabularyTeacherKey));
            teacherView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    teacherView.setOnClickListener(null);
                    teacherBannerView.dismiss(ABAVocabularyActivity.this);
                    topBarWordtCounter.setVisibility(View.VISIBLE);
                }
            });
        } else {
            teacherBannerView.setVisibility(View.GONE);
        }
    }

    @Override
    public void finishSection(boolean sectionCompleted) {
        if (sectionCompleted) {
            DataStore.getInstance().getVocabularyController().setCompletedSection(getRealm(), currentUnit.getSectionVocabulary());
            unitDetailABTestingConfigurator.startUnitDetailIntent(this, getIntent().getExtras().getString(EXTRA_UNIT_ID), SectionType.kVocabulario);
        } else {
        unitDetailABTestingConfigurator.startUnitDetailIntent(this, getIntent().getExtras().getString(EXTRA_UNIT_ID));
        }
        finish();
        bottomControllerView.unregisterListeners();
        overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_exit_unitanimation2);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (!bottomControllerView.getIsControllerPlay()) {
            currentPhrase = DataStore.getInstance().getVocabularyController().getCurrentPhraseForSection(currentUnit.getSectionVocabulary(), position);
            selectWord(position);
            setNextAudio(true);
            vocabularyAdapter.notifyDataSetChanged();
        }
    }

    private void selectWord(int position) {
        vocabularyAdapter.setCurrentWord(position);
        vocabularyAdapter.notifyDataSetChanged();
        scrollAtPosition(position);
    }

    private void scrollAtPosition(int position) {
        vocabularyListview.setSelectionFromTop(position, getItemHeight(vocabularyListview, 0) - getItemHeight(vocabularyListview, position));
    }


    public int getItemHeight(ListView listView, int item) {
        View childView = vocabularyAdapter.getView(item, null, listView);
        childView.measure(View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.AT_MOST), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        return childView.getMeasuredHeight();
    }

    @Override
    public void onListened() {
        teacherView.setOnClickListener(null);
        teacherBannerView.dismiss(ABAVocabularyActivity.this);
        topBarWordtCounter.setVisibility(View.VISIBLE);

        // Tracking
        if (currentUnit != null && currentPhrase != null) {
            CooladataStudyTrackingManager.trackListenedToAudio(currentUser.getUserId(), currentUnit.getLevel().getIdLevel(), currentUnit.getIdUnit(), ABAVocabulary, currentPhrase.getAudioFile());
        }
    }

    @Override
    public void onRecorded() {
        bottomControllerView.startCompare();

        // Tracking
        if (currentUnit != null && currentPhrase != null) {
            CooladataStudyTrackingManager.trackRecordedAudio(currentUser.getUserId(), currentUnit.getLevel().getIdLevel(), currentUnit.getIdUnit(), ABAVocabulary, currentPhrase.getAudioFile());
        }
    }

    @Override
    public void onCompared() {
        setCurrentPhrase();
        setNextAudio(true);
    }

    @Override
    public void finishCompared() {
        DataStore.getInstance().getVocabularyController().setPhraseDone(getRealm(), currentPhrase, currentUnit.getSectionVocabulary(), true);
        vocabularyAdapter.notifyDataSetChanged();
        updateSectionProgress();

        // Tracking
        if (currentUnit != null && currentPhrase != null) {
            CooladataStudyTrackingManager.trackComparedAudio(currentUser.getUserId(), currentUnit.getLevel().getIdLevel(), currentUnit.getIdUnit(), ABAVocabulary, currentPhrase.getAudioFile());
        }

        // Finishing section
        if (!completedMode && DataStore.getInstance().getVocabularyController().isSectionCompleted(currentUnit.getSectionVocabulary())) {
            if (serverConfig.isImprovedLinearCourseActive() && areOtherSectionsCompleted(currentUnit) && !NextSectionDialog_.checkNextSectionIsDone(currentUnit.getIdUnit(), 7)) {
                showNextSectionDialog();
            } else {
                finishSection(true);
            }
        }
    }

    @Override
    public void onRetryPhrase() {
        selectWord(vocabularyAdapter.getPhrasePosition(currentPhrase));
    }

    // ================================================================================
    // ABAMasterSectionActivity
    // ================================================================================

    protected Object getSection() {
        return currentUnit.getSectionVocabulary();
    }

    @Override
    public void OnPausedSection() {
        bottomControllerView.resetControlerStateForCurrentPhrase();
    }

    // ================================================================================
    // Private methods - Tracking
    // ================================================================================

    private void trackSection() {
        ABAUser user = UserController.getInstance().getCurrentUser(getRealm());
        CooladataNavigationTrackingManager.trackEnteredSection(user.getUserId(), currentUnit.getLevel().getIdLevel(), currentUnit.getIdUnit(), ABAVocabulary);
        tracker.trackEnteredSection(currentUnit.getLevel().getIdLevel(), currentUnit.getIdUnit(), ABAVocabulary.toString());

        Crashlytics.log(Log.INFO, "Section", "User opens Section Vocabulary");
    }

    private void showNextSectionDialog() {
        FragmentManager fragmentManager = getFragmentManager();
        NextSectionDialog_ ndf = NextSectionDialog_.newInstance(SectionHelper.SECTION_VOCABULARY, currentUnit.getIdUnit());
        ndf.addListener(this);
        ndf.show(fragmentManager, "Vocabulary");
    }

    @Override
    public void goToNext(boolean result) {
        if (result) {
            startActivity(SectionHelper.getNextSectionIntent(this, Integer.parseInt(currentUnit.getIdUnit()), SectionHelper.SECTION_VOCABULARY));
            finish();
        } else {
            finishSection(true);
        }
    }
}