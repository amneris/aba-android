package com.abaenglish.videoclass.presentation.section.assessment.result;

/**
 * Created by xabierlosada on 27/07/16.
 */

public interface AssessmentResultComponent {
    void inject(AssessmentResultActivity activity);
}