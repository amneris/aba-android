package com.abaenglish.videoclass.presentation.profile;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;

import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.analytics.legacy.LegacyTrackingManager;
import com.abaenglish.videoclass.data.persistence.ABAAPIError;
import com.abaenglish.videoclass.domain.content.DataController;
import com.abaenglish.videoclass.domain.content.UserController;
import com.abaenglish.videoclass.presentation.base.ABAMasterActivity;
import com.abaenglish.videoclass.presentation.base.custom.ABAButton;
import com.abaenglish.videoclass.presentation.base.custom.ABAEditText;
import com.abaenglish.videoclass.presentation.base.custom.ABAProgressDialog;
import com.abaenglish.videoclass.presentation.base.custom.ABATextView;
import com.abaenglish.videoclass.session.SessionService;
import com.crashlytics.android.Crashlytics;

/**
 * Created by iaguila on 25/3/15.
 */
public class ChangePasswordActivity extends ABAMasterActivity implements View.OnClickListener {

    private ABAEditText email;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        //configResizeView(R.id.rootLayout);

        configActionBar();
        configView();
        Crashlytics.log(Log.INFO, "Change Password", "User opens Change Password View");
    }

    private void configView() {
        email = (ABAEditText) findViewById(R.id.changePassword);
        ABAButton send = (ABAButton) findViewById(R.id.changePasswordSend);
        send.setText(getString(R.string.keyboardTitleSend).toUpperCase());
        send.setOnClickListener(this);
    }

    private void recoverPassword(){
        showProgressDialog(ABAProgressDialog.SimpleDialog);
        mSessionService.recoverPassword(ChangePasswordActivity.this, email.getText().toString(), new DataController.ABAAPICallback<Boolean>() {
            @Override
            public void onSuccess(Boolean response) {
                dismissProgressDialog();

                AlertDialog.Builder builder = new AlertDialog.Builder(ChangePasswordActivity.this);
                builder.setMessage(getString(R.string.forgotPassAlertkey))
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                finish();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }

            @Override
            public void onError(ABAAPIError error) {
                dismissProgressDialog();
                if (error != null) {
                    error.showABANotificationError(ChangePasswordActivity.this);
                }
            }
        });
    }

    private boolean checkFields(String mail){
        if (("").equals(mail)){
            showABAErrorNotification(getString(R.string.regErrorEmailNil));
            focusFieldAfterError(email);
            return false;
        }
        if (!UserController.isEmailValid(mail)){
            showABAErrorNotification(getString(R.string.regErrorEmailFormat));
            focusFieldAfterError(email);
            return false;
        }
        return true;
    }

    private void configActionBar(){
        addBackListener();

        ABATextView title = (ABATextView) findViewById(R.id.toolbarTitle);
        title.setText(getString(R.string.forgotTitleSection).toUpperCase());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.changePasswordSend:
                email.setSelected(false);
                if (checkFields(email.getText().toString())) {
                    LegacyTrackingManager.getInstance().trackEvent(getRealm(), "account_change_pass", "account", "cambiar password");
                    recoverPassword();
                }
                break;
        }
    }
}
