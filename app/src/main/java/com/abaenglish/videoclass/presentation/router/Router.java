package com.abaenglish.videoclass.presentation.router;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;

import com.abaenglish.videoclass.presentation.login.LoginActivity;
import com.abaenglish.videoclass.presentation.register.RegisterActivity;
import com.abaenglish.videoclass.presentation.tutorial.TutorialActivity;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by xabierlosada on 13/07/16.
 */
@Singleton
public class Router {

    public static final String ROUTE_TO_TUTORIAL = "tutorialModule";
    public static final String ROUTE_TO_LOGIN = "login";
    public static final String ROUTE_TO_REGISTER = "register";

    private  final Map<String, Intent> routeMap;
    private final Context context;

    @Inject
    public Router(Context context) {
        this.context = context;
        routeMap = new HashMap<>();
        routeMap.put(ROUTE_TO_LOGIN, new Intent(context, LoginActivity.class));
        routeMap.put(ROUTE_TO_REGISTER, new Intent(context, RegisterActivity.class));
        routeMap.put(ROUTE_TO_TUTORIAL, new Intent(context, TutorialActivity.class));
    }

    public void route(String route) {
        Intent intent = routeMap.get(route);
        if(route != null){
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }
    }

    public void route(String route, Activity originActivity) {
        Intent intent = routeMap.get(route);
        if(route != null){
            originActivity.startActivity(intent);
            originActivity.finish();
        }
    }


}
