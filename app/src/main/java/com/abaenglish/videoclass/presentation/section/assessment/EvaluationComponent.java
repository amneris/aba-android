package com.abaenglish.videoclass.presentation.section.assessment;

import android.content.Context;

import com.abaenglish.videoclass.analytics.TrackerContract;
import com.abaenglish.videoclass.data.CourseContentService;
import com.abaenglish.videoclass.data.network.oauth.OAuthTokenAccessor;
import com.abaenglish.videoclass.domain.ApplicationConfiguration;
import com.abaenglish.videoclass.domain.abTesting.ServerConfig;
import com.abaenglish.videoclass.domain.abTesting.UnitDetailABTestingConfigurator;
import com.abaenglish.videoclass.domain.content.EvaluationController;
import com.abaenglish.videoclass.domain.content.LevelUnitController;
import com.abaenglish.videoclass.domain.content.UserController;
import com.abaenglish.videoclass.presentation.base.ABAMasterActivity;
import com.abaenglish.videoclass.presentation.base.custom.FontCache;
import com.abaenglish.videoclass.presentation.di.ApplicationComponent;
import com.abaenglish.videoclass.session.SessionService;
import com.nostra13.universalimageloader.core.ImageLoader;

import dagger.Component;
import io.realm.RealmConfiguration;

/**
 * Created by xabierlosada on 21/07/16.
 */
@EvaluationScope
@Component(dependencies = ApplicationComponent.class, modules = EvaluationModule.class)
public interface EvaluationComponent {

    UserController userController();

    LevelUnitController levelUnitController();

    EvaluationController evaluationController();

    UnitDetailABTestingConfigurator abRouter();

    Context context();

    RealmConfiguration realmConfiguration();

    TrackerContract.Tracker tracker();

    ImageLoader imageLoader();

    ServerConfig serverConfig();

    FontCache fontCache();

    SessionService sessionService();
    
//    CourseContentService contentService();

    ApplicationConfiguration appConfig();

    OAuthTokenAccessor tokenAccessor();

    void inject(EvaluationExerciseActivity target);

    void inject(ABAEvaluationActivity target);

    void inject(ABAMasterActivity target);
}