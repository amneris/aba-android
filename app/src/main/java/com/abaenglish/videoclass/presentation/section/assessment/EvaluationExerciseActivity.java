package com.abaenglish.videoclass.presentation.section.assessment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;

import com.abaenglish.videoclass.ABAApplication;
import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.analytics.cooladata.study.CooladataStudyTrackingManager;
import com.abaenglish.videoclass.data.persistence.ABAEvaluation;
import com.abaenglish.videoclass.data.persistence.ABAEvaluationOption;
import com.abaenglish.videoclass.data.persistence.ABAEvaluationQuestion;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.data.persistence.ABAUser;
import com.abaenglish.videoclass.domain.content.EvaluationController;
import com.abaenglish.videoclass.domain.content.LevelUnitController;
import com.abaenglish.videoclass.domain.content.UserController;
import com.abaenglish.videoclass.presentation.base.ABAMasterSectionActivity;
import com.abaenglish.videoclass.presentation.base.custom.ABATextView;
import com.abaenglish.videoclass.presentation.section.assessment.result.AssessmentResultActivity;
import com.crashlytics.android.Crashlytics;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.realm.RealmList;

import static com.abaenglish.videoclass.presentation.section.SectionActivityInterface.EXTRA_UNIT_ID;

/**
 * Created by madalin on 14/05/15.
 */
public class EvaluationExerciseActivity extends ABAMasterSectionActivity implements View.OnClickListener, Animation.AnimationListener {


    public static final String CURRENT_STATUS = "CURRENT_USER_STATUS";
    public static UserStatusType userStatusType;

    private int currentExercise = 1;
    private ABAUnit currentUnit;
    private ABAEvaluation abaEvaluation;
    private RealmList<ABAEvaluationQuestion> realmListEvaluationQuestion;
    private RealmList<ABAEvaluationOption> realmListEvaluationOption;

    private ABATextView evaluationNumberOfQuestions;
    private ABATextView evaluationExercise;
    private LinearLayout evaluationOption1;
    private LinearLayout evaluationOption2;
    private LinearLayout evaluationOption3;
    private ABATextView evaluationTextOption1;
    private ABATextView evaluationTextOption2;
    private ABATextView evaluationTextOption3;
    private ArrayList<Integer> listWithMistakeAnswers = new ArrayList<>();

    private Animation fadeInAnimation;
    private Animation fadeOutAnimation;

    private int currentEvaluationType = 0;
    private boolean isClickable = true;

    @Inject LevelUnitController levelUnitController;
    @Inject EvaluationController evaluationController;
    @Inject UserController userController;

    public enum UserStatusType {
        kUserRepeatEvaluation(0),
        kUserMistakeEvaluation(1);

        private final int value;

        UserStatusType(final int newValue) {
            value = newValue;
        }

        public int getValue() {
            return value;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String unitId = getIntent().getExtras().getString(EXTRA_UNIT_ID);
        getComponent().inject(this);

        setContentView(R.layout.activity_evaluation_exercise);

        initApp(unitId);

        configActionBar();
        config();
    }

    private void configActionBar() {
        findViewById(R.id.toolbarLeftButton).setOnTouchListener(((ABAApplication) getApplicationContext()).getImagePressedState(null));
        findViewById(R.id.toolbarLeftButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishActivity();
            }
        });

        ABATextView unitSectionTitle = (ABATextView) findViewById(R.id.toolbarTitle);
        ABATextView unitSectionProgress = (ABATextView) findViewById(R.id.toolbarSubTitle);
        unitSectionTitle.setText(R.string.unitMenuTitle8Key);
        unitSectionProgress.setVisibility(View.GONE);
    }

    private void initApp(String unitID) {
        currentUnit = levelUnitController.getUnitWithId(getRealm(), unitID);
        int status = getIntent().getExtras().getInt(CURRENT_STATUS);
        currentEvaluationType = status;
        abaEvaluation = currentUnit.getSectionEvaluation();
        realmListEvaluationQuestion = abaEvaluation.getContent();
    }

    private void config() {
        evaluationNumberOfQuestions = (ABATextView) findViewById(R.id.evaluationNumber);
        evaluationExercise = (ABATextView) findViewById(R.id.evaluationExercise);
        evaluationOption1 = (LinearLayout) findViewById(R.id.evaluationOption1);
        evaluationOption2 = (LinearLayout) findViewById(R.id.evaluationOption2);
        evaluationOption3 = (LinearLayout) findViewById(R.id.evaluationOption3);
        evaluationTextOption1 = (ABATextView) findViewById(R.id.evaluationTextOption1);
        evaluationTextOption2 = (ABATextView) findViewById(R.id.evaluationTextOption2);
        evaluationTextOption3 = (ABATextView) findViewById(R.id.evaluationTextOption3);

        fadeInAnimation = AnimationUtils.loadAnimation(this, R.anim.fade_in_evaluation);
        fadeOutAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_out_evaluation);
        fadeOutAnimation.setAnimationListener(this);

        evaluationOption1.setOnClickListener(this);
        evaluationOption2.setOnClickListener(this);
        evaluationOption3.setOnClickListener(this);

        if (currentEvaluationType == userStatusType.kUserMistakeEvaluation.getValue()) {
            if (evaluationController.getExercisesWithNoCorrectAnswers(abaEvaluation).size() == 0) {
                evaluationController.resetEvaluationAnswers(getRealm(), abaEvaluation);
                currentEvaluationType = userStatusType.kUserRepeatEvaluation.getValue();
            }
        }

        if (currentEvaluationType == userStatusType.kUserRepeatEvaluation.getValue()) {
            for (ABAEvaluationQuestion question : abaEvaluation.getContent()) {
                if (question.isAnswered())
                    currentExercise++;
            }
        } else {
            listWithMistakeAnswers = evaluationController.getExercisesWithNoCorrectAnswers(abaEvaluation);
            currentExercise = listWithMistakeAnswers.get(0);
        }

        evaluationNumberOfQuestions.setText(Html.fromHtml(getResources().getString(R.string.evaluationPreguntaLabelKey) + " " + "<font color='#07BCE6'>"
                + currentExercise + "</font>" + " " + getResources().getString(R.string.evaluationPreguntaLabelDeKey) + " " + abaEvaluation.getContent().size()));
        setAllTextEvaluation(currentExercise - 1);
    }

    protected void finishActivity() {
        if (currentEvaluationType == userStatusType.kUserMistakeEvaluation.getValue() && abaEvaluation != null) {
            evaluationController.resetEvaluationAnswers(getRealm(), abaEvaluation);
        }

        unitDetailABTestingConfigurator.startUnitDetailIntent(this, getIntent().getExtras().getString(EXTRA_UNIT_ID));
        finish();
        overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_exit_unitanimation2);
    }

    protected void checkExerciseActivity(ABAEvaluation abaEvaluation) {
        // Cooladata Tracking
        trackEvaluationFinished();

        // Starting intent
        Intent intent = new Intent(this, AssessmentResultActivity.class);
        intent.putExtra(AssessmentResultActivity.EXTRA_CORRECT_EXERCISE, evaluationController.getElementsCompletedForSection(abaEvaluation));
        intent.putExtra(EXTRA_UNIT_ID, getIntent().getExtras().getString(EXTRA_UNIT_ID));
        startActivity(intent);
        overridePendingTransition(R.anim.slide_enter_unitanimation1, R.anim.slide_enter_unitanimation2);
        finish();
    }

    public void onClick(View v) {
        if (isClickable) {
            //Reset Mistake Answer
            if (currentEvaluationType == userStatusType.kUserMistakeEvaluation.getValue())
                evaluationController.resetEvaluationAtQuestion(getRealm(), abaEvaluation, currentExercise - 1);

            isClickable = false;
            int OptionSelected;

            if (v.getId() == R.id.evaluationOption1) {
                //OPTION 1
                OptionSelected = 0;
                evaluationOption1.setBackgroundResource(R.drawable.evaluation_pressed_button);
            } else if (v.getId() == R.id.evaluationOption2) {
                //OPTION 2
                OptionSelected = 1;
                evaluationOption2.setBackgroundResource(R.drawable.evaluation_pressed_button);
            } else {
                //OPTION 3
                OptionSelected = 2;
                evaluationOption3.setBackgroundResource(R.drawable.evaluation_pressed_button);
            }

            realmListEvaluationOption = realmListEvaluationQuestion.get(currentExercise - 1).getOptions();
            evaluationController.evaluationQuestionAnsweredWithOption(getRealm(), realmListEvaluationOption.get(OptionSelected));

            if (currentExercise >= abaEvaluation.getContent().size()) {
                checkExerciseActivity(abaEvaluation);
            } else {
                if (currentEvaluationType == userStatusType.kUserRepeatEvaluation.getValue()) {
                    currentExercise++;
                    startFadeOutAnimation();
                } else {
                    evaluationExercise.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                    listWithMistakeAnswers.remove(0);
                    if (listWithMistakeAnswers.size() != 0) {
                        currentExercise = listWithMistakeAnswers.get(0);
                        startFadeOutAnimation();
                    } else {
                        checkExerciseActivity(abaEvaluation);
                    }
                }
            }
        }
    }

    private void startFadeOutAnimation() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                evaluationExercise.startAnimation(fadeOutAnimation);
                evaluationNumberOfQuestions.startAnimation(fadeOutAnimation);
                evaluationTextOption1.startAnimation(fadeOutAnimation);
                evaluationTextOption2.startAnimation(fadeOutAnimation);
                evaluationTextOption3.startAnimation(fadeOutAnimation);
            }
        }, 500);
    }

    public void onBackPressed() {
        super.onBackPressed();
        finishActivity();
    }

    @Override
    public void onAnimationStart(Animation animation) {

    }

    @Override
    public void onAnimationEnd(Animation animation) {
        evaluationNumberOfQuestions.setText(Html.fromHtml(getResources().getString(R.string.evaluationPreguntaLabelKey) + " " + "<font color='#07BCE6'>"
                + currentExercise + "</font>" + " " + getResources().getString(R.string.evaluationPreguntaLabelDeKey) + " " + abaEvaluation.getContent().size()));

        setAllTextEvaluation(currentExercise - 1);
        isClickable = true;
        evaluationOption1.setBackgroundResource(R.drawable.evaluation_lightblue_button);
        evaluationOption2.setBackgroundResource(R.drawable.evaluation_lightblue_button);
        evaluationOption3.setBackgroundResource(R.drawable.evaluation_lightblue_button);

        evaluationExercise.startAnimation(fadeInAnimation);
        evaluationNumberOfQuestions.startAnimation(fadeInAnimation);
        evaluationTextOption1.startAnimation(fadeInAnimation);
        evaluationTextOption2.startAnimation(fadeInAnimation);
        evaluationTextOption3.startAnimation(fadeInAnimation);
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    private void setAllTextEvaluation(int exercise) {
        evaluationExercise.setText(Html.fromHtml(setEvaluationExercise(exercise)));
        evaluationTextOption1.setText(abaEvaluation.getContent().get(exercise).getOptions().get(0).getText());
        evaluationTextOption2.setText(abaEvaluation.getContent().get(exercise).getOptions().get(1).getText());
        evaluationTextOption3.setText(abaEvaluation.getContent().get(exercise).getOptions().get(2).getText());

        if (currentEvaluationType == userStatusType.kUserMistakeEvaluation.getValue())
            evaluationExercise.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.mipmap.evaluation_mistake_cross, 0);
    }

    private String setEvaluationExercise(int exercise) {
        String textEvaluationExercise = realmListEvaluationQuestion.get(exercise).getQuestion();
        if (currentEvaluationType == userStatusType.kUserMistakeEvaluation.getValue() && textEvaluationExercise.contains("_")) {
            String line = textEvaluationExercise.substring(textEvaluationExercise.indexOf("_"), textEvaluationExercise.lastIndexOf("_") + 1);
            String userAnswer = evaluationController.getUserAnswersAtExercise(abaEvaluation, exercise).getText();
            userAnswer = toBold(userAnswer);
            textEvaluationExercise = textEvaluationExercise.replace(line, userAnswer);
        }
        return textEvaluationExercise;
    }

    public String toBold(String origin) {
        return "<b> " + origin + " </b>";
    }

    // ================================================================================
    // ABAMasterSectionActivity
    // ================================================================================

    protected Object getSection() {
        return abaEvaluation;
    }

    // ================================================================================
    // Cooladata tracking
    // ================================================================================

    protected void trackEvaluationFinished() {
        ABAUser currentUser = userController.getCurrentUser(getRealm());
        int correctAnswersCount = evaluationController.getElementsCompletedForSection(abaEvaluation);

        StringBuilder optionsBuffer = new StringBuilder("");
        List<ABAEvaluationOption> answers = EvaluationController.getEvaluationAnswers(abaEvaluation);
        for (ABAEvaluationOption option : answers) {
            optionsBuffer.append(option.getOptionLetter());
            boolean isLastAnswer = answers.indexOf(option) == (answers.size() - 1);
            if (!isLastAnswer) {
                optionsBuffer.append(",");
            }
        }
        CooladataStudyTrackingManager.trackEvaluation(currentUser.getUserId(), currentUnit.getLevel().getIdLevel(), currentUnit.getIdUnit(), optionsBuffer.toString(), Integer.toString(correctAnswersCount));
        tracker.trackReviewedTestResults(currentUnit.getLevel().getIdLevel(), currentUnit.getIdUnit(), optionsBuffer.toString(), correctAnswersCount);

        Crashlytics.log(Log.INFO, "Section", "User opens Curse Evaluation Finished View");
    }

    protected EvaluationComponent getComponent() {
        return ((ABAApplication) getApplication()).getEvaluationComponent();
    }
}