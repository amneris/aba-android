package com.abaenglish.videoclass.presentation.plan;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.abaenglish.videoclass.ABAApplication;
import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.analytics.helpers.GenericTrackingInterface;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.abaenglish.videoclass.presentation.base.ABAMasterActivity;
import com.abaenglish.videoclass.presentation.base.custom.ABATextView;
import com.abaenglish.videoclass.presentation.base.custom.FontCache;
import com.crashlytics.android.Crashlytics;

import javax.inject.Inject;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Jesus Espejo using mbp13jesus on 08/06/15.
 */
public class InitialPlansActivity extends ABAMasterActivity {

    // Main
    @BindView(R.id.relativelayout_main)
    RelativeLayout relativeLayoutMain;

    // Entitlement
    @BindView(R.id.relativelayout_entitlement)
    RelativeLayout relativeLayoutEntitlement;
    @BindView(R.id.imageview_lock)
    ImageView imageViewLock;
    @BindView(R.id.textview_hello_username)
    ABATextView textViewHelloUserName;
    @BindView(R.id.textview_title)
    ABATextView textViewTitle;
    @BindView(R.id.textview_content)
    ABATextView textViewContent;

    // Footer
    @BindView(R.id.textview_unlock)
    ABATextView textViewUnlockPremium;
    @BindView(R.id.textview_unlock_subtitle)
    ABATextView textViewUnlockPremiumSubtitle;
    @BindView(R.id.button_view_prices)
    Button buttonViewPrices;

    @BindView(R.id.toolbarLeftButton)
    ImageButton leftToolbarButton;

    @BindView(R.id.toolbarTitle)
    ABATextView toolbarTitle;
    @BindView(R.id.toolbarSubTitle)
    ABATextView toolbarSubtitle;

    @BindString(R.string.helloInitialPlansKey)
    String helloMessage;
    @BindString(R.string.subtitle1InitialPlansKey)
    String initialPlanMessage;
    @BindString(R.string.subtitle1BoldInitialPlansKey)
    String abaPremiumMessage;

    public static Intent getCallingIntent(Context context) {
        return new Intent(context, InitialPlansActivity.class);
    }

    // ================================================================================
    // Lifecycle
    // ================================================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_initial_plans);
        ButterKnife.bind(this);

        configureUI();
        configureToolbar();
        Crashlytics.log(Log.INFO, "Plans", "User opens Initial Plans view");
    }

    // ================================================================================
    // Private methods
    // ================================================================================

    private void configureUI() {
        // Fonts
        textViewHelloUserName.setABATag(getResources().getInteger(R.integer.typefaceSans900));
        textViewTitle.setABATag(getResources().getInteger(R.integer.typefaceSans500));
        textViewContent.setABATag(getResources().getInteger(R.integer.typefaceSans500));
        textViewUnlockPremium.setABATag(getResources().getInteger(R.integer.typefaceSans900));
        textViewUnlockPremiumSubtitle.setABATag(getResources().getInteger(R.integer.typefaceSans500));

//        buttonViewPrices.setABATag(getResources().getInteger(R.integer.typefaceRalewayBold));
        buttonViewPrices.setTypeface(fontCache.getTypeface(FontCache.ABATypeface.rBold));

        // User name
        String userName = DataStore.getInstance().getUserController().getCurrentUser(getRealm()).getName();
        textViewHelloUserName.setText(helloMessage + " " + userName + "!");

        // Bold 'ABA Premium'
        textViewTitle.setText(Html.fromHtml(initialPlanMessage + " <b>" + abaPremiumMessage + "</b>"));

        // Playing with margins to center the lock in the horizontal border
        Drawable d = imageViewLock.getDrawable();
        int blueBubbleWidth = d.getIntrinsicWidth();

        FrameLayout.LayoutParams frameParams = (FrameLayout.LayoutParams) relativeLayoutMain.getLayoutParams();
        frameParams.setMargins(frameParams.leftMargin, blueBubbleWidth / 2, frameParams.rightMargin, frameParams.bottomMargin);
        relativeLayoutMain.setLayoutParams(frameParams);
    }

    private void configureToolbar() {
        leftToolbarButton.setOnTouchListener(((ABAApplication) getApplicationContext()).getImagePressedState(null));
        leftToolbarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_exit_unitanimation2);
            }
        });
        toolbarTitle.setText(abaPremiumMessage);
        toolbarSubtitle.setVisibility(View.GONE);
    }

    // ================================================================================
    // Actions
    // ================================================================================

    @OnClick(R.id.button_view_prices)
    protected void goToViewPrices() {
        trackPricingScreenWithOrigin(GenericTrackingInterface.PlansViewControllerOriginType.kPlansViewControllerOriginUnlockContent);
        startActivity(new Intent(this, PlansActivity.class));
        finish();
        overridePendingTransition(R.anim.slide_enter_unitanimation1, R.anim.slide_enter_unitanimation2);
    }
}