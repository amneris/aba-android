package com.abaenglish.videoclass.presentation.section.speak;

import android.app.FragmentManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.abaenglish.videoclass.ABAApplication;
import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.analytics.cooladata.navigation.CooladataNavigationTrackingManager;
import com.abaenglish.videoclass.analytics.cooladata.study.CooladataStudyTrackingManager;
import com.abaenglish.videoclass.data.persistence.ABAPhrase;
import com.abaenglish.videoclass.data.persistence.ABARole;
import com.abaenglish.videoclass.data.persistence.ABASpeak;
import com.abaenglish.videoclass.data.persistence.ABASpeakDialog;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.data.persistence.ABAUser;
import com.abaenglish.videoclass.domain.content.SectionController;
import com.abaenglish.videoclass.domain.content.UserController;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.abaenglish.videoclass.presentation.base.ABAMasterSectionActivity;
import com.abaenglish.videoclass.presentation.base.custom.ABATextView;
import com.abaenglish.videoclass.presentation.base.custom.ListenAndRecordControllerView;
import com.abaenglish.videoclass.presentation.base.custom.RecyclerItemClickListener;
import com.abaenglish.videoclass.presentation.base.custom.TeacherBannerView;
import com.abaenglish.videoclass.presentation.section.NextSectionDialog_;
import com.abaenglish.videoclass.presentation.section.SectionActivityInterface;
import com.abaenglish.videoclass.presentation.section.SectionHelper;
import com.abaenglish.videoclass.presentation.section.SectionType;
import com.bzutils.LogBZ;
import com.crashlytics.android.Crashlytics;

import io.realm.RealmList;

import static com.abaenglish.videoclass.analytics.cooladata.CooladataTrackingDefinitions.CooladataSectionType.ABASpeak;

/**
 * Created by madalin on 7/05/15.
 * Edited by Jesus with regard of Cooladata on 25/04/16
 */
public class ABASpeakActivity extends ABAMasterSectionActivity implements ListenAndRecordControllerView.PlayerControlsListener, SectionActivityInterface, ListenAndRecordControllerView.SectionControlsListener {

    private ABAUser currentUser;
    private ABAUnit currentUnit;
    private RealmList<ABASpeakDialog> realmListSpeak;
    private RealmList<ABARole> realmListRole;
    private ABASpeak speak;
    private RecyclerView speakListview;
    private SpeakAdapter speakAdapter;

    private TeacherBannerView mTeacherBannerView;

    private ABAPhrase currentPhrase;
    private LinearLayoutManager mLayoutManager;
    private boolean completedMode = false;

    // ================================================================================
    // Lifecycle
    // ================================================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_speak);

        initApp(getIntent().getExtras().getString(EXTRA_UNIT_ID));
        configActionBar();
        configBottomController();
        configTeacher();
        config();

        completedMode = DataStore.getInstance().getSpeakController().isSectionCompleted(currentUnit.getSectionSpeak());
        setCurentPhrase();

        trackSection();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishSection(false);
    }

    // ================================================================================
    // Private methods - Activity configuration
    // ================================================================================

    private void initApp(final String UnitID) {
        currentUnit = DataStore.getInstance().getLevelUnitController().getUnitWithId(getRealm(), UnitID);
        currentUser = DataStore.getInstance().getUserController().getCurrentUser(getRealm());
        speak = currentUnit.getSectionSpeak();
        realmListSpeak = currentUnit.getSectionSpeak().getContent();
        realmListRole = currentUnit.getRoles();
    }

    private void config() {
        speakListview = (RecyclerView) findViewById(R.id.speakListView);
        mLayoutManager = new LinearLayoutManager(ABASpeakActivity.this);
        speakListview.setLayoutManager(mLayoutManager);

        speakAdapter = new SpeakAdapter(getApplicationContext(), currentUnit, realmListSpeak, realmListRole);
        speakAdapter.setCurrentUnit(speak.getUnit());

        speakListview.setVisibility(View.VISIBLE);
        speakListview.setAdapter(speakAdapter);

        speakListview.post(new Runnable() {
            @Override
            public void run() {
                //updatePaddingFromPosition(getCurrentPositionToPlay());
                speakListview.setVisibility(View.INVISIBLE);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (!ABASpeakActivity.this.isFinishing()) {
                            setNextAudio(false);
                            if (currentUnit.getSectionSpeak().getProgress() == 0) {
                                Animation animation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_in_from_bottom);
                                speakListview.startAnimation(animation);
                            }
                            speakListview.setVisibility(View.VISIBLE);
                            refreshPadding();
                        }
                    }
                }, 100);
            }
        });

        speakListview.addOnItemTouchListener(
                new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {

                        if (!bottomControllerView.getIsControllerPlay() && position != RecyclerView.NO_POSITION) {
                            speakAdapter.setIsTouchMode(true);
                            currentPhrase = speakAdapter.getPhraseFromPosition(position);
                            speakAdapter.setSelectedPhrase(currentPhrase);
                            speakAdapter.notifyDataSetChanged();
                            if (currentPhrase != null) {
                                setNextAudio(true);
                            }
                        }
                    }
                })
        );
    }

    private void configActionBar() {
        findViewById(R.id.toolbarLeftButton).setOnTouchListener(((ABAApplication) getApplicationContext()).getImagePressedState(null));
        findViewById(R.id.toolbarLeftButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishSection(false);
            }
        });

        updateSectionProgress();
    }

    // ================================================================================
    // Private methods
    // ================================================================================

    private void updatePaddingWithInset(int inset) {
        int topPadding = speakListview.getHeight() - (int) getResources().getDimension(R.dimen.listenAndrecordControllerHeight) - inset;
        speakListview.setPadding(0, topPadding, 0, (int) getResources().getDimension(R.dimen.listenAndrecordControllerHeight));
        speakListview.setClipToPadding(false);
        mLayoutManager.scrollToPosition(getCurrentPositionToPlay());
    }

    private void setCurentPhrase() {
        if (completedMode) {
            currentPhrase = speakAdapter.getPhraseFromPosition(speakAdapter.getItemCount() - 1);
        } else {
            currentPhrase = DataStore.getInstance().getSpeakController().getCurrentPhraseForSection(speak, SectionController.ContinueWithFirstUndoneWord);
        }
    }

    private void configBottomController() {
        bottomControllerView = (ListenAndRecordControllerView) findViewById(R.id.listenAndRecorderControllerView);
        bottomControllerView.unregisterListeners();
        bottomControllerView.restoreListenersForCurrentSection();
        bottomControllerView.setCurrentUnit(currentUnit);
        bottomControllerView.setPlayerControlsListener(this);
        bottomControllerView.setSectionControlsListener(this);
        bottomControllerView.setSectionType(SectionType.kHabla);
    }

    private int getCurrentPositionToPlay() {
        ABAPhrase sampleToPlay = DataStore.getInstance().getSpeakController().getCurrentSampleToPlay(currentPhrase);
        return speakAdapter.getPositionOfSample(sampleToPlay);
    }

    /**
     * Esta cochinada la tenemos que hacer por que hasta que no hace el layout, el recycleview no sabe el tamaño
     * de los elementos que contiene.
     */
    private void refreshPadding() {
        speakListview.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                View item = speakListview.getLayoutManager().findViewByPosition(getCurrentPositionToPlay());
                int itemHeight = getItemHeight(item);
                updatePaddingWithInset(itemHeight);

                int width = speakListview.getWidth();
                int height = speakListview.getHeight();
                if (itemHeight > 0 && width > 0 && height > 0) {
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                        speakListview.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    } else {
                        speakListview.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    }
                }
            }
        });
    }

    private void setNextAudio(boolean autoPlay) {

        refreshPadding();

        ABAPhrase sampleToPlay = DataStore.getInstance().getSpeakController().getCurrentSampleToPlay(currentPhrase);
        if (sampleToPlay == null) {
            return;
        }

        bottomControllerView.setCurrentPhrase(sampleToPlay);

        if (speakListview != null) { // if it's init
            int position = speakAdapter.getPositionOfSample(sampleToPlay);
            speakAdapter.setPositionOfSpecialSample(position);
            LogBZ.d("Scrolling to position " + position);

            mLayoutManager.scrollToPosition(position);
        }

        speakAdapter.setSelectedPhrase(sampleToPlay);
        speakAdapter.seedLogicalDataSource();
        speakAdapter.notifyDataSetChanged();
        if (autoPlay) {
            bottomControllerView.startListenCurrentPhrase();
        }
    }

    private int getItemHeight(View childView) {
        if (childView == null)
            return 0;
        childView.measure(View.MeasureSpec.makeMeasureSpec(speakListview.getWidth(), View.MeasureSpec.AT_MOST), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        int measuredHeight = childView.getMeasuredHeight();
        return measuredHeight;
    }

    // ================================================================================
    // SessionActivityInterface
    // ================================================================================

    @Override
    public void updateSectionProgress() {
        ABATextView unitSectionTitle = (ABATextView) findViewById(R.id.toolbarTitle);
        ABATextView unitSectionProgress = (ABATextView) findViewById(R.id.toolbarSubTitle);

        unitSectionTitle.setText(R.string.unitMenuTitle2Key);
        unitSectionProgress.setText(DataStore.getInstance().getSpeakController().getPercentageForSection(currentUnit.getSectionSpeak()));
    }

    @Override
    public void configTeacher() {
        ABAUser currentUser = DataStore.getInstance().getUserController().getCurrentUser(getRealm());
        mTeacherBannerView = (TeacherBannerView) findViewById(R.id.detailUnitTeacherView);
        if (currentUnit.getSectionSpeak().getProgress() == 0) {

            if (DataStore.getInstance().getLevelUnitController().checkIfFileExist(currentUnit, currentUser.getTeacherImage())) {
                DataStore.getInstance().getLevelUnitController().displayImage(null, currentUser.getTeacherImage(), mTeacherBannerView.getImageView());
            } else {
                mTeacherBannerView.setImageUrl(currentUser.getTeacherImage());
            }
            mTeacherBannerView.setText(getString(R.string.sectioSpeakTeacherKey));
            mTeacherBannerView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mTeacherBannerView.setOnClickListener(null);
                    mTeacherBannerView.dismiss(ABASpeakActivity.this);
                }
            });

        } else {
            mTeacherBannerView.setVisibility(View.GONE);
        }
    }

    @Override
    public void finishSection(boolean sectionCompleted) {
        if (sectionCompleted) {
            DataStore.getInstance().getSpeakController().setCompletedSection(getRealm(), currentUnit.getSectionSpeak());
        }

        if (serverConfig.isImprovedLinearCourseActive() && sectionCompleted && !NextSectionDialog_.checkNextSectionIsDone(currentUnit.getIdUnit(), 2)) {
            showNextSectionDialog();
        } else {
            unitDetailABTestingConfigurator.startUnitDetailIntent(this, getIntent().getExtras().getString(EXTRA_UNIT_ID));
            finish();
            overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_exit_unitanimation2);
        }

        bottomControllerView.unregisterListeners();
    }

    @Override
    public void onListened() {
        mTeacherBannerView.dismiss(this);

        // Tracking
        CooladataStudyTrackingManager.trackListenedToAudio(currentUser.getUserId(), currentUnit.getLevel().getIdLevel(), currentUnit.getIdUnit(), ABASpeak, currentPhrase.getIdPhrase());
        tracker.trackListenedAudio(currentUnit.getLevel().getIdLevel(), currentUnit.getIdUnit(), ABASpeak.toString(), currentPhrase.getIdPhrase());

    }

    @Override
    public void onRecorded() {
        bottomControllerView.startCompare();

        // Tracking
        CooladataStudyTrackingManager.trackRecordedAudio(currentUser.getUserId(), currentUnit.getLevel().getIdLevel(), currentUnit.getIdUnit(), ABASpeak, currentPhrase.getIdPhrase());
        tracker.trackRecordedAudio(currentUnit.getLevel().getIdLevel(), currentUnit.getIdUnit(), ABASpeak.toString(), currentPhrase.getIdPhrase());
    }

    @Override
    public void onCompared() {
        setCurentPhrase();
        setNextAudio(true);
        speakAdapter.notifyDataSetChanged();
    }

    @Override
    public void finishCompared() {
        speakAdapter.setIsTouchMode(false);
        DataStore.getInstance().getSpeakController().setPhraseDone(getRealm(), currentPhrase, speak, true);
        speakAdapter.notifyDataSetChanged();
        updateSectionProgress();
        speakAdapter.setElemets(DataStore.getInstance().getSpeakController().getCurrentDialogFromSection(currentUnit.getSectionSpeak()));

        // Tracking
        CooladataStudyTrackingManager.trackComparedAudio(currentUser.getUserId(), currentUnit.getLevel().getIdLevel(), currentUnit.getIdUnit(), ABASpeak, currentPhrase.getIdPhrase());
        tracker.trackComparedAudio(currentUnit.getLevel().getIdLevel(), currentUnit.getIdUnit(), ABASpeak.toString(), currentPhrase.getIdPhrase());

        // Finishing section
        if (!completedMode && DataStore.getInstance().getSpeakController().isSectionCompleted(speak)) {
            finishSection(true);
        }
    }

    @Override
    public void onRetryPhrase() {
        refreshPadding();
    }

    // ================================================================================
    // ABAMasterSectionActivity
    // ================================================================================

    protected Object getSection() {
        return speak;
    }


    @Override
    public void OnPausedSection() {
        bottomControllerView.resetControlerStateForCurrentPhrase();
    }

    // ================================================================================
    // Private methods - Tracking
    // ================================================================================

    private void trackSection() {
        ABAUser user = UserController.getInstance().getCurrentUser(getRealm());
        CooladataNavigationTrackingManager.trackEnteredSection(user.getUserId(), currentUnit.getLevel().getIdLevel(), currentUnit.getIdUnit(), ABASpeak);
        Crashlytics.log(Log.INFO, "Section", "User opens Section Speak");
        tracker.trackEnteredSection(currentUnit.getLevel().getIdLevel(), currentUnit.getIdUnit(), ABASpeak.toString());
    }

    private void showNextSectionDialog() {
        FragmentManager fragmentManager = getFragmentManager();
        NextSectionDialog_ ndf = NextSectionDialog_.newInstance(SectionHelper.SECTION_SPEAK, currentUnit.getIdUnit());
        ndf.addListener(this);
        ndf.show(fragmentManager, "Write");
    }

    // ================================================================================
    // NextSectionDialog
    // ================================================================================

    @Override
    public void goToNext(boolean result) {
        if (result) {
            startActivity(SectionHelper.getNextSectionIntent(this, Integer.parseInt(currentUnit.getIdUnit()), SectionHelper.SECTION_SPEAK));
            finish();
            overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_exit_unitanimation2);
        } else {
            unitDetailABTestingConfigurator.startUnitDetailIntent(this, getIntent().getExtras().getString(EXTRA_UNIT_ID), SectionType.kHabla);
        }
    }
}
