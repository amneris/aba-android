package com.abaenglish.videoclass.presentation.section.speak;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.abaenglish.videoclass.ABAApplication;
import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.data.persistence.ABAPhrase;
import com.abaenglish.videoclass.data.persistence.ABARole;
import com.abaenglish.videoclass.data.persistence.ABASpeakDialog;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.abaenglish.videoclass.presentation.base.custom.ABATextView;
import com.bzutils.LogBZ;
import com.bzutils.images.RoundedImageView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;

/**
 * Created by madalin on 21/05/15.
 */
public class SpeakAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public static final int VIEW_TYPE_DIALOG = 0;
    public static final int VIEW_TYPE_SAMPLE = 1;

    private Context mContext;
    private LayoutInflater mLayoutInflate;
    private RealmList<ABASpeakDialog> listSpeak;
    private RealmList<ABARole> listRole;
    private ABAPhrase selectedPhrase;
    private ABAUnit abaUnit;
    public int positionOfSpecialSample = 0;

    private List<Object> phrasesDataSource;
    private List<Object> logicalDataSource;
    private ABAUnit currentUnit;

    boolean isTouchMode = false;
    private int elementsOfList;

    public SpeakAdapter(Context context, ABAUnit unit, RealmList<ABASpeakDialog> listSpeak, RealmList<ABARole> listRoles) {
        mContext = context;
        mLayoutInflate = LayoutInflater.from(context);
        this.listSpeak = listSpeak;
        this.listRole = listRoles;
        this.currentUnit = unit;
        this.elementsOfList = DataStore.getInstance().getSpeakController().getCurrentDialogFromSection(currentUnit.getSectionSpeak());
        seedDataSource();
        seedLogicalDataSource();
    }

    private final String TAG = "SpeakAdapter";

    private void seedDataSource() {
        phrasesDataSource = new ArrayList<>();
        for (ABASpeakDialog dialog : listSpeak) {

            phrasesDataSource.add(dialog);

            for (ABAPhrase samplePhrase : dialog.getSample()) {
                phrasesDataSource.add(samplePhrase);
            }
        }
    }

    public void setCurrentUnit(ABAUnit unit) {
        this.abaUnit = unit;
    }

    public void setIsTouchMode(boolean isTouchMode){
        this.isTouchMode = isTouchMode;
    }

    public void seedLogicalDataSource() {
        logicalDataSource = new ArrayList<>();

        for (ABASpeakDialog dialog : listSpeak) {
            if (dialog.getDialog().size() > 0) {
                logicalDataSource.add(dialog.getDialog().get(firstNoDonePhrase(dialog.getDialog())));

                for (ABAPhrase phrase : dialog.getSample()) {
                    if (phrase.getSubPhrases().size() > 0) {
                        logicalDataSource.add(phrase.getSubPhrases().get(firstNoDonePhrase(phrase.getSubPhrases())));
                    } else {
                        logicalDataSource.add(phrase);
                    }
                }
            }
        }
    }

    private int firstNoDonePhrase(RealmList<ABAPhrase> subPhrases) {
        for (int i = 0; i < subPhrases.size(); i++) {
            if (!subPhrases.get(i).isDone()) {
                return i;
            }
        }
        return 0;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View convertView = null;

        switch (viewType) {
            case VIEW_TYPE_DIALOG:

                convertView = mLayoutInflate.inflate(R.layout.item_speak_dialog, parent, false);

                DialogViewHolder mItemViewHolder = new DialogViewHolder(convertView);
                mItemViewHolder.dialogSpeak = (ABATextView) convertView.findViewById(R.id.DialogText);
                mItemViewHolder.translationDialogSpeak = (ABATextView) convertView.findViewById(R.id.TranslationText);
                mItemViewHolder.imageSpeak = (RoundedImageView) convertView.findViewById(R.id.speakImage);
                mItemViewHolder.separadorDialog = (ImageView) convertView.findViewById(R.id.separadorDialog);
                mItemViewHolder.dialogBackground = (LinearLayout) convertView.findViewById(R.id.DialogContent);
                return mItemViewHolder;


            case VIEW_TYPE_SAMPLE:

                convertView = mLayoutInflate.inflate(R.layout.item_vocabulary_word, parent, false);

                SampleViewHolder mSampleViewHolder = new SampleViewHolder(convertView);
                mSampleViewHolder.englishText = (ABATextView) convertView.findViewById(R.id.englishWordText);
                mSampleViewHolder.translationText = (ABATextView) convertView.findViewById(R.id.translationText);
                mSampleViewHolder.vocabularyBackground = (LinearLayout) convertView.findViewById(R.id.vocabularyBackground);

                return mSampleViewHolder;


        }
        return null;
    }

    //The cyclomatic complexity of this method is really nasty... When someone is more inspired and less time pressured... be free ;)
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, int position) {

        Object item = phrasesDataSource.get(position);

        int viewType = getItemViewType(position);

        switch (viewType) {

            case VIEW_TYPE_DIALOG:

                final DialogViewHolder mItemViewHolder = (DialogViewHolder) viewHolder;

                ABASpeakDialog dialog = (ABASpeakDialog) item;

                String dialogText = "";

                final int dialogSize = dialog.getDialog().size();
                int count = 1;
                for (ABAPhrase dialogPhrase : dialog.getDialog()) {
                    if (dialogPhrase.isDone()) {
                        dialogText = dialogText.concat(toGreen(dialogPhrase.getText()));
                    } else if (selectedPhrase != null && selectedPhrase.getAudioFile().equals(dialogPhrase.getAudioFile())) {
                        dialogText = dialogText.concat(toBold(dialogPhrase.getText()));
                    } else {
                        dialogText = dialogText.concat(dialogPhrase.getText());
                    }

                    if (dialogSize != 1 && dialogSize != count) {
                        dialogText += "<br/>";
                    }

                    if (selectedPhrase != null) {
                        if (getPositionOfSample(selectedPhrase) == position) {
                            mItemViewHolder.dialogBackground.setBackgroundColor(ContextCompat.getColor(mContext, R.color.abaBackgroundGrey));
                        } else {
                            mItemViewHolder.dialogBackground.setBackgroundColor(ContextCompat.getColor(mContext, R.color.abaWhite));
                        }
                    }

                    //We control the first element only... nice structure hehe :)
                    if (dialogPhrase.equals(dialog.getDialog().first())) {
                        if (!("null").equals(dialogPhrase.getTranslation())) {
                            mItemViewHolder.translationDialogSpeak.setText(dialogPhrase.getTranslation());
                        } else {
                            mItemViewHolder.translationDialogSpeak.setVisibility(View.GONE);
                        }

                        /*if (dialogPhrase.isDone()) {
                            mItemViewHolder.dialogSpeak.setTypeface(Typeface.DEFAULT_BOLD);
                            mItemViewHolder.dialogSpeak.setTextColor(mItemViewHolder.convertView.getResources().getColor(R.color.abaEmailGreen));
                        } else {
                            mItemViewHolder.dialogSpeak.setTypeface(Typeface.DEFAULT);
                            mItemViewHolder.dialogSpeak.setTextColor(mItemViewHolder.convertView.getResources().getColor(R.color.abaGrey));
                        }*/

                        int pos = 0;
                        while (pos < listRole.size() && !dialogPhrase.getSpeakRole().equals(listRole.get(pos).getName()) && pos != listRole.size() - 1) {
                            pos++;
                        }

                        if (DataStore.getInstance().getLevelUnitController().checkIfFileExist(currentUnit, listRole.get(pos).getImageUrl())) {
                            DataStore.getInstance().getLevelUnitController().displayImage(currentUnit, listRole.get(pos).getImageUrl(), mItemViewHolder.imageSpeak);
                        } else {
                            ImageLoader.getInstance().loadImage(listRole.get(pos).getImageUrl(), new ImageLoadingListener() {
                                @Override
                                public void onLoadingStarted(String imageUri, View view) {
                                }

                                @Override
                                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                                }

                                @Override
                                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                                    BitmapDrawable ob = new BitmapDrawable(mItemViewHolder.imageSpeak.getResources(), loadedImage);
                                    mItemViewHolder.imageSpeak.setImageDrawable(ob);
                                }

                                @Override
                                public void onLoadingCancelled(String imageUri, View view) {
                                }
                            });
                        }

                    }

                    count++;
                }
                mItemViewHolder.dialogSpeak.setText(Html.fromHtml(dialogText));
                break;
            case VIEW_TYPE_SAMPLE:

                SampleViewHolder mSampleViewHolder = (SampleViewHolder) viewHolder;

                ABAPhrase samplePhrase = (ABAPhrase) item;

                mSampleViewHolder.translationText.setTextColor(ContextCompat.getColor(mContext, R.color.abaSpeakColor));
                if (samplePhrase.getSubPhrases().size() == 0) {

                    //SAMPLEPHRASE
                    mSampleViewHolder.englishText.setVisibility(View.VISIBLE);
                    mSampleViewHolder.englishText.setText(samplePhrase.getText());
                    if (!samplePhrase.getTranslation().equals("null")) {
                        mSampleViewHolder.translationText.setText(samplePhrase.getTranslation());
                    } else {
                        mSampleViewHolder.translationText.setText("");
                    }


                    if (samplePhrase.isDone()) {
                        mSampleViewHolder.englishText.setTypeface(Typeface.DEFAULT_BOLD);
                        mSampleViewHolder.englishText.setTextColor(ContextCompat.getColor(mContext, R.color.abaEmailGreen));
                    } else {
                        mSampleViewHolder.englishText.setTypeface(Typeface.DEFAULT);
                        mSampleViewHolder.englishText.setTextColor(ContextCompat.getColor(mContext, R.color.abaSpeakColor));
                    }

                    if (selectedPhrase != null && selectedPhrase.getAudioFile().equals(samplePhrase.getAudioFile())) {
                        mSampleViewHolder.vocabularyBackground.setBackgroundColor(ContextCompat.getColor(mContext, R.color.abaBackgroundGrey));
                        mSampleViewHolder.englishText.setTypeface(Typeface.DEFAULT_BOLD);
                    } else {
                        mSampleViewHolder.vocabularyBackground.setBackgroundColor(ContextCompat.getColor(mContext, R.color.abaWhite));
                        // mSampleViewHolder.englishText.setTypeface(Typeface.DEFAULT);
                    }


                } else {

                    //MULTYPHRASE
                    if (selectedPhrase != null) {
                        if (getPositionOfSample(selectedPhrase) == position) {
                            mSampleViewHolder.vocabularyBackground.setBackgroundColor(ContextCompat.getColor(mContext, R.color.abaBackgroundGrey));
                        } else {
                            mSampleViewHolder.vocabularyBackground.setBackgroundColor(ContextCompat.getColor(mContext, R.color.abaWhite));
                        }
                    }

                    String multyPhase = "";
                    for (int i = 0; i < samplePhrase.getSubPhrases().size(); i++) {

                        String sampleText = "";

                        if (!samplePhrase.getSubPhrases().get(i).getText().equals("null")) {
                            if (samplePhrase.getSubPhrases().get(i).isDone()) {
                                sampleText = sampleText.concat(toGreen(toBold(toBig(samplePhrase.getSubPhrases().get(i).getText())) + " "));
                            } else if (selectedPhrase != null && selectedPhrase.getAudioFile().equals(samplePhrase.getSubPhrases().get(i).getAudioFile())) {
                                sampleText = sampleText.concat(toBold(toBig(samplePhrase.getSubPhrases().get(i).getText())) + " ");
                            } else
                                sampleText = sampleText.concat(toBig(samplePhrase.getSubPhrases().get(i).getText()) + " ");

                        } else {
                            sampleText = sampleText.concat("");
                        }

                        if (!samplePhrase.getSubPhrases().get(i).getTranslation().equals("null")) {
                            sampleText = sampleText.concat(samplePhrase.getSubPhrases().get(i).getTranslation() + " ");
                        } else {
                            sampleText = sampleText.concat("");
                        }

                        multyPhase = multyPhase.concat(sampleText);
                    }
                    mSampleViewHolder.translationText.setText(Html.fromHtml(multyPhase));
                    mSampleViewHolder.englishText.setVisibility(View.GONE);
                }
                break;
        }
    }

    public String toGreen(String origin) {
        return "<b>" + "<font color='#3BBC72'>" + origin + "</font>" + "</b>";
    }

    public String toBold(String origin) {
        return "<b>" + origin + "</b>";
    }

    public String toBig(String origin) {
        return "<big>" + origin + "</big>";
    }

    public int getPositionOfSample(ABAPhrase sample) {
        if (sample != null) {

            addTrackingToCrashlytics(sample);

            for (Object row : logicalDataSource) {
                if (row.getClass().getSuperclass().equals(ABASpeakDialog.class)) {
                    if (!sample.isSpeakDialogPhrase())
                        continue;
                    ABASpeakDialog dialog = (ABASpeakDialog) row;
                    for (ABAPhrase dialogPhrase : dialog.getDialog()) {
                        if (dialogPhrase.getAudioFile().equals(sample.getAudioFile()))

                            return logicalDataSource.indexOf(row);
                    }

                } else {
                    ABAPhrase phrase = (ABAPhrase) row;
                    if (phrase.getSubPhrases().size() == 0 && phrase.getAudioFile().equals(sample.getAudioFile()))
                        return logicalDataSource.indexOf(row);
                    else {
                        for (ABAPhrase subPhrase : phrase.getSubPhrases()) {
                            if (subPhrase.getAudioFile().equals(sample.getAudioFile()))
                                return logicalDataSource.indexOf(row);
                        }
                    }
                }
            }
            // if not in logical datasource is especial sample.
            if (isTouchMode){
                return positionOfSpecialSample;
            }
        } else {

            LogBZ.d("Sample is null");
        }

        LogBZ.d("Sample not found: " + sample.getAudioFile());

        return -1;
    }

    private void addTrackingToCrashlytics(ABAPhrase phrase) {
        ((ABAApplication) mContext.getApplicationContext()).addCrashlyticsTracking(TAG, "Phrase: " + phrase.getText() + " CurrentUnit: " + (abaUnit == null ? "undefined" : abaUnit.getIdUnit()));
    }

    public ABAPhrase getPhraseFromPosition(int position) {
        seedLogicalDataSource();

        positionOfSpecialSample = position;
        Object selected = logicalDataSource.get(position);
        if (selected instanceof ABASpeakDialog) {
            ABASpeakDialog dialog = (ABASpeakDialog) selected;
            return dialog.getDialog().get(firstNoDonePhrase(dialog.getDialog()));
        } else if (selected instanceof ABAPhrase) {
            ABAPhrase phrase = (ABAPhrase) selected;
            if (phrase.getSubPhrases() == null || phrase.getSubPhrases().isEmpty())
                return firstNotDonePhraseWithAudio(phrase, position);
            return phrase.getSubPhrases().get(firstNoDonePhrase(phrase.getSubPhrases()));
        }
        return null;
    }


    private ABAPhrase firstNotDonePhraseWithAudio(ABAPhrase phrase, int position) {

        if ((phrase.getAudioFile() == null || phrase.getAudioFile().equals("")) && phrase.getFatherPhrase() != null) {
            List<ABAPhrase> fatherPhrases = phrase.getFatherPhrase().getSubPhrases();
            ABAPhrase currentPhrase;
            List<ABAPhrase> phrasesWithAudio = new ArrayList<>();
            for (int i = 0; i < fatherPhrases.size(); i++) {
                currentPhrase = fatherPhrases.get(i);
                if (currentPhrase.getAudioFile() != null && !currentPhrase.getAudioFile().equals("")) {
                    phrasesWithAudio.add(currentPhrase);
                    if (!currentPhrase.isDone()) {
                        return currentPhrase;
                    }
                }
            }
            if (!phrasesWithAudio.isEmpty()) {
                return phrasesWithAudio.get(0);

            }
        }
        return phrase;
    }

    public void setElemets(int nrOfElements){
        this.elementsOfList = nrOfElements;
    }


    @Override
    public int getItemViewType(int position) {
        Object item = phrasesDataSource.get(position);

        if (item.getClass().getSuperclass().equals(ABASpeakDialog.class))
            return VIEW_TYPE_DIALOG;

        return VIEW_TYPE_SAMPLE;
    }


    @Override
    public int getItemCount() {
        return elementsOfList;
    }

    public void setPositionOfSpecialSample(int positionOfSpecialSample) {
        this.positionOfSpecialSample = positionOfSpecialSample;
    }

    public void setSelectedPhrase(ABAPhrase phrase) {
        this.selectedPhrase = phrase;
    }

    public static class DialogViewHolder extends RecyclerView.ViewHolder {
        private View convertView;
        private ABATextView dialogSpeak;
        private ABATextView translationDialogSpeak;
        private RoundedImageView imageSpeak;
        private ImageView separadorDialog;
        private LinearLayout dialogBackground;

        public DialogViewHolder(View convertView) {
            super(convertView);
            this.convertView = convertView;
        }
    }

    public static class SampleViewHolder extends RecyclerView.ViewHolder {
        private View convertView;
        private ABATextView englishText;
        private ABATextView translationText;
        private LinearLayout vocabularyBackground;

        public SampleViewHolder(View convertView) {
            super(convertView);
            this.convertView = convertView;
        }
    }

}