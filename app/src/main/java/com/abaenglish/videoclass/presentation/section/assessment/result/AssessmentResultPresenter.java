package com.abaenglish.videoclass.presentation.section.assessment.result;

import android.text.TextUtils;
import android.util.Log;

import com.abaenglish.videoclass.analytics.TrackerContract.Tracker;
import com.abaenglish.videoclass.clean.interactor.DefaultSingleSubscriber;
import com.abaenglish.videoclass.domain.content.LevelUnitController;
import com.abaenglish.videoclass.presentation.section.assessment.EvaluationExerciseActivity;
import com.abaenglish.videoclass.presentation.section.assessment.result.interactor.AssessmentResult;
import com.abaenglish.videoclass.presentation.section.assessment.result.interactor.FinishEvaluationUseCase;
import com.abaenglish.videoclass.presentation.section.assessment.result.interactor.QualifyAssessmentUseCase;
import com.abaenglish.videoclass.presentation.section.assessment.result.interactor.QualifyInput;
import com.crashlytics.android.Crashlytics;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by xabierlosada on 26/07/16.
 */

public class AssessmentResultPresenter extends AssessmentResultViewPresenter<AssessmentResultView, ResultRouter> {

    private QualifyAssessmentUseCase qualifyAssessment;
    private FinishEvaluationUseCase finishEvaluationUseCase;

    private AssessmentResult assessmentResult;
    private Tracker tracker;

    @Override
    public void initialize(AssessmentResultView view, ResultRouter router) {
        super.initialize(view, router);
    }

    public AssessmentResultPresenter(QualifyAssessmentUseCase qualifyAssessmentUseCase,
                                     FinishEvaluationUseCase finishEvaluationUseCase,
                                     Tracker tracker) {
        this.qualifyAssessment = qualifyAssessmentUseCase;
        this.finishEvaluationUseCase = finishEvaluationUseCase;
        this.tracker = tracker;
    }

    @Override
    void redoAssessment() {
        if (checkAssessment()) {
            if (assessmentResult.grade().equals(AssessmentResult.Grade.PASS)) {
                getRouter().repeatEvaluation(assessmentResult.unitId(), EvaluationExerciseActivity.UserStatusType.kUserMistakeEvaluation);
            } else {
                finishEvaluation(assessmentResult);
                getRouter().repeatEvaluation(assessmentResult.unitId(), EvaluationExerciseActivity.UserStatusType.kUserRepeatEvaluation);
            }
        }
    }

    @Override
    public void shareWithFacebook() {
        getRouter().shareWithFacebook();
    }

    @Override
    public void shareCertificate() {
        if (!checkAssessment()) {
            return;
        }
        getRouter().goToLinkedin(getLevelId());
    }

    @Override
    public void shareWithTwitter() {
        if (!checkAssessment()) {
            return;
        }
        getRouter().shareWithTwitter(assessmentResult.unitName(), assessmentResult.unitTitle());
    }

    @Override
    public void confirmResult() {
        if (!checkAssessment()) {
            return;
        }
        finishEvaluation(assessmentResult);
        if (assessmentResult.isCourseFinished()) {
            tracker.trackCourseFinished();
            getRouter().goToCourseFinished(assessmentResult.username(), assessmentResult.teacherImageUrl());
        } else {
            getRouter().continueWithUnit(assessmentResult.nextUnitId());
            if (!TextUtils.isEmpty(assessmentResult.nextUnitId())) {
                tracker.trackEnteredUnit(LevelUnitController.getIdLevelForUnitId(assessmentResult.nextUnitId()), assessmentResult.nextUnitId());
            }
        }
    }

    @Override
    void goBack() {
        if (!checkAssessment()) {
            return;
        }
        if (assessmentResult.grade().equals(AssessmentResult.Grade.FAIL)) {
            redoAssessment();
        } else {
            confirmResult();
        }
    }

    private boolean checkAssessment() {
        return assessmentResult != null;
    }

    public void processAnswers(String id, int validResponses) {
        QualifyInput input = QualifyInput.builder()
                .unitId(id)
                .correctAnswers(validResponses)
                .build();
        executeSingle(qualifyAssessment.build(input)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread()),
                new QualificationSubscriber());
    }

    public void finishEvaluation(AssessmentResult assessmentResult) {
        executeSingle(finishEvaluationUseCase.build(assessmentResult), new FinishEvaluationSubscriber());
    }

    public int getLevelId() {
        return Long.valueOf(assessmentResult.levelId()).intValue();
    }

    private void renderResult(AssessmentResult result) {
        getView().renderScore(result.correctAnswers(), result.numberOfQuestions());
        switch (result.grade()) {
            case FAIL:
                getView().renderFailMessage();
                getView().renderTeacher(result.teacherImageUrl());
                getView().hideSocial();
                getView().showRepeatEvaluation();
                getView().hideNextUnit();
                break;
            case PASS:
                getView().renderPassMessage(result.unitName());
                showSocial(result);
                getView().showRepeatMistakes();
                showNextUnit(result);
                break;
            case PERFECT:
                getView().renderPerfectMessage(result.unitName());
                showSocial(result);
                showNextUnit(result);
        }
    }

    private void showSocial(AssessmentResult result) {
        if (result.isLastUnitOfLevel()) {
            getView().showLinkedinShare();
        } else {
            getView().showNextUnit(result.nextUnitId());
        }
    }

    private void showNextUnit(AssessmentResult result) {
        if (result.isCourseFinished()) {
            getView().showCourseFinished();
        } else {
            getView().showNextUnit(result.nextUnitId());
        }
    }

    private class QualificationSubscriber extends DefaultSingleSubscriber<AssessmentResult> {

        @Override
        public void onSuccess(AssessmentResult result) {
            assessmentResult = result;
            renderResult(result);
        }
    }

    private class FinishEvaluationSubscriber extends DefaultSingleSubscriber<Void> {
        @Override
        public void onSuccess(Void value) {
            Crashlytics.log(Log.INFO, "finishUseCase", "Evaluation finished successfully");
        }
    }
}