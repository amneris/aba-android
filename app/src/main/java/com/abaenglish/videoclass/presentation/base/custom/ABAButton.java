package com.abaenglish.videoclass.presentation.base.custom;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.Button;

import com.abaenglish.videoclass.R;

/**
 * Created by iaguila on 24/3/15.
 */
public class ABAButton extends Button {

    private Object abaTag;

    public ABAButton(Context context) {
        super(context);
    }

    public ABAButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        configTypeface();
    }

    public ABAButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        configTypeface();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ABAButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        configTypeface();
    }

    public void configTypeface() {
        if (getTag() == null)
            setTag(R.integer.typefaceSans100);
    }

    public Object getAbaTag() {
        return abaTag;
    }

    public void setAbaTag(Object abaTag) {
        this.abaTag = abaTag;
    }
}
