package com.abaenglish.videoclass.presentation.abaMoment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.analytics.cooladata.abaMoments.CooladataMomentsTrackingManager;
import com.abaenglish.videoclass.data.network.oauth.OAuthClient;
import com.abaenglish.videoclass.data.network.retrofit.abaMomentsUtils.ABAMoment;
import com.abaenglish.videoclass.data.network.retrofit.abaMomentsUtils.ABAMomentClientDefinitions;
import com.abaenglish.videoclass.data.network.retrofit.abaMomentsUtils.ABAMomentProgressRequest;
import com.abaenglish.videoclass.data.network.retrofit.abaMomentsUtils.ABAMomentProgressResponse;
import com.abaenglish.videoclass.data.network.retrofit.clients.ABAMomentListClient;
import com.abaenglish.videoclass.data.network.retrofit.clients.ABAMomentSaveProgressClient;
import com.abaenglish.videoclass.presentation.abaMoment.ABAMomentRecyclerViewAdapter.OnListFragmentInteractionListener;
import com.abaenglish.videoclass.presentation.base.ABAMasterFragment;
import com.abaenglish.videoclass.presentation.base.custom.ABATextView;
import com.abaenglish.videoclass.presentation.course.DashboardFragment;
import com.abaenglish.videoclass.presentation.shell.MenuActivity;
import com.google.gson.Gson;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.InstanceState;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static android.app.Activity.RESULT_OK;


/**
 * Created by Julien on 28/02/2017.
 * A fragment representing the list of ABAMoments available with three possible states : done, active and inactive.
 */
@EFragment(R.layout.fragment_abamoments_list)
public class ABAMomentsListFragment extends ABAMasterFragment implements OnListFragmentInteractionListener, GoToUnitDialog_.ResultListener, OAuthClient.OnResponseListener<List<ABAMoment>> {


    public static String USER_ID_STRING_EXTRA_KEY = "user_uuid";

    //last ABA Moment clicked needed to animate it if finished
    @InstanceState
    Integer lastItemClick = 0;

    @InstanceState
    String abaMomentsAsJsonString;

    private OnListFragmentInteractionListener mListener;
    private String userId;

    private ABAMomentListClient mMomentListClient;
    private ABAMomentSaveProgressClient mMomentSaveClient;

    @ViewById
    RecyclerView abaMomentsRecyclerView;
    @ViewById
    ProgressBar progressBar;
    @ViewById
    ABATextView abaMomentsTextView, vocabularyTextView, timeTextView, errorTextView, errorButton;
    @ViewById
    ImageView errorImageView;

    private boolean enabledClick = true;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public ABAMomentsListFragment() {

    }

    @Override
    public void onResume() {
        super.onResume();

        CooladataMomentsTrackingManager.trackMomentTypeOpened(userId, "vocabulary");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mMomentListClient = new ABAMomentListClient(appConfig, tokenAccessor);
        mMomentSaveClient = new ABAMomentSaveProgressClient(appConfig, tokenAccessor);
    }

    @AfterViews
    void afterViews() {

        this.userId = getArguments().getString(USER_ID_STRING_EXTRA_KEY);

        abaMomentsTextView.setText(getResources().getString(R.string.abaMomentStartText));
        vocabularyTextView.setText(getResources().getString(R.string.abaMomentStartVocabulary));
        timeTextView.setText(getResources().getString(R.string.abaMomentStart1Minute));

        progressBar.setVisibility(View.VISIBLE);

        //check if we already have the ABAMoments
        if (abaMomentsAsJsonString == null) {
            mMomentListClient.fetchABAMomentsList(this);
        } else {
            List<ABAMoment> abaMomentsAsList = Arrays.asList(new Gson().fromJson(this.abaMomentsAsJsonString, ABAMoment[].class));
            initRecyclerView(abaMomentsAsList);
        }
    }

    @UiThread
    public void initRecyclerView(List<ABAMoment> abaMoments) {
        if (progressBar != null) {
            progressBar.setVisibility(View.INVISIBLE);
            abaMomentsRecyclerView.setLayoutManager(new GridLayoutManager(getContext(), 3));
            abaMomentsRecyclerView.setAdapter(new ABAMomentRecyclerViewAdapter(getActivity(), abaMoments, mListener));
        }
    }

    @UiThread
    public void showError() {
        if (progressBar != null) {
            progressBar.setVisibility(View.INVISIBLE);
            errorTextView.setVisibility(View.VISIBLE);
            errorImageView.setVisibility(View.VISIBLE);
            errorButton.setVisibility(View.VISIBLE);
        }
    }

    @Click(R.id.errorButton)
    public void retry() {
        errorTextView.setVisibility(View.INVISIBLE);
        errorButton.setVisibility(View.INVISIBLE);
        errorImageView.setVisibility(View.INVISIBLE);
        progressBar.setVisibility(View.VISIBLE);
        mMomentListClient.fetchABAMomentsList(this);
    }

    @UiThread
    public void animateABAMoment() {

        if ((abaMomentsRecyclerView.getAdapter()) != null) {
            enabledClick = false;
            abaMomentsRecyclerView.scrollToPosition(lastItemClick);
            ((ABAMomentRecyclerViewAdapter) abaMomentsRecyclerView.getAdapter()).animateItemAt(lastItemClick);
            abaMomentsRecyclerView.getAdapter().notifyDataSetChanged();

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    showNextSectionDialog();
                }
            }, 3000);
        } else {
            showNextSectionDialog();
        }

    }

    @Override
    protected int onResLayout() {
        return R.layout.fragment_abamoments_list;
    }

    @Override
    protected void onConfigView() {

    }

    @Override
    protected void onConfigToolbar(ABATextView toolbarTitle, ABATextView toolbarSubTitle, ImageButton toolbarButton) {
        toolbarTitle.setText(R.string.startTitle2Key);
        toolbarTitle.setTextColor(ContextCompat.getColor(getContext(), R.color.abaWhite));
        toolbarSubTitle.setVisibility(View.GONE);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener = this;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == MenuActivity.ABA_MOMENT_REQUEST_CODE && resultCode == RESULT_OK) {

            if (data != null) {

                animateABAMoment();

                String momentId = data.getStringExtra(ABAMomentStartActivity.MOMENT_ID_RESULT);
                Log.e("MOMENT_ID_RESULT", momentId);

                ABAMomentProgressRequest progressRequest = new ABAMomentProgressRequest(momentId);

                mMomentSaveClient.saveMomentProgress(progressRequest, new OAuthClient.OnResponseListener<ABAMomentProgressResponse>() {
                    @Override
                    public void onResponseReceived(ABAMomentProgressResponse response, String error) {
                        Log.e("onResponseReceived", "response : " + response + ", error : " + error);
                    }
                });
            }
        }
    }

    private void showNextSectionDialog() {
        if (getABAActivity() != null) {
            GoToUnitDialog_ goToUnitDialog = new GoToUnitDialog_();
            goToUnitDialog.addListener(this);
            goToUnitDialog.show(getABAActivity().getSupportFragmentManager(), "TAG");
        }
        enabledClick = true;
    }

    @Override
    public void goToCurrentUnit(boolean result, String unitID, String name) {
        if (result) {
            if (getActivity().getSupportFragmentManager().findFragmentByTag(DashboardFragment.class.getName()) != null) {
                ((MenuActivity) getActivity()).toggleNavigationMenu();
            } else {
                ((MenuActivity) getActivity()).changeFragmentWithDelay(new DashboardFragment(), true);
            }
            unitDetailABTestingConfigurator.startUnitDetailIntent(getActivity(), unitID);
            tracker.trackEnteredUnit(name, unitID);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onListFragmentInteraction(ABAMoment item, int position) {

        if (enabledClick) {

            CooladataMomentsTrackingManager.trackMomentOpened(this.userId, item.getMomentType(), item.getId());

            lastItemClick = position;
            Intent intent = new Intent(this.getActivity(), ABAMomentStartActivity_.class);
            intent.putExtra(ABAMomentStartActivity.USER_ID_STRING_EXTRA_KEY, this.userId);
            intent.putExtra(ABAMomentStartActivity.MOMENT_SERIALIZE_EXTRA_KEY, item);

            startActivityForResult(intent, MenuActivity.ABA_MOMENT_REQUEST_CODE);

        }
    }

    @Override
    public void onResponseReceived(List<ABAMoment> response, String error) {
        if (error.compareTo(ABAMomentClientDefinitions.ABAMomentErrorType.NONE.toString()) != 0) {
            showError();
        } else {
            // List<ABAMoment> abaMomentsSorted = sortAbaMoments(abaMoments);
            this.abaMomentsAsJsonString = new Gson().toJson(response);
            initRecyclerView(response);
        }
    }

    // ================================================================================
    // Private methods
    // ================================================================================

    private List<ABAMoment> sortAbaMoments(List<ABAMoment> abaMoments) {

        List<ABAMoment> abaMomentsSorted = new ArrayList<>();
        for (ABAMoment abaMoment : abaMoments) {
            if (abaMoment.getStatus().compareTo("Done") == 0) {
                abaMomentsSorted.add(abaMoment);
            }
        }
        for (ABAMoment abaMoment : abaMoments) {
            if (abaMoment.getStatus().compareTo("Active") == 0) {
                abaMomentsSorted.add(abaMoment);
            }
        }
        for (ABAMoment abaMoment : abaMoments) {
            if (abaMoment.getStatus().compareTo("New") == 0) {
                abaMomentsSorted.add(abaMoment);
            }
        }
        for (ABAMoment abaMoment : abaMoments) {
            if (abaMoment.getStatus().compareTo("Inactive") == 0) {
                abaMomentsSorted.add(abaMoment);
            }
        }
        return abaMomentsSorted;
    }
}
