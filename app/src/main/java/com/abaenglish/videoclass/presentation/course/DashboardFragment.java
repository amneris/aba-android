package com.abaenglish.videoclass.presentation.course;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageButton;

import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.analytics.modern.MixpanelTrackingManager;
import com.abaenglish.videoclass.data.persistence.ABALevel;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.data.persistence.ABAUser;
import com.abaenglish.videoclass.domain.content.LevelUnitController;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.abaenglish.videoclass.presentation.base.ABAMasterFragment;
import com.abaenglish.videoclass.presentation.base.custom.ABAExpandableListView;
import com.abaenglish.videoclass.presentation.base.custom.ABAExpandableListView.ExpandableAnimationListener;
import com.abaenglish.videoclass.presentation.base.custom.ABATextView;
import com.abaenglish.videoclass.presentation.unit.UnitAdapter;
import com.bzutils.BZScreenHelper;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.PauseOnScrollListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by madalin on 17/04/15.
 */
public class DashboardFragment extends ABAMasterFragment implements ExpandableListView.OnGroupClickListener, ExpandableListView.OnChildClickListener {

    private ABAExpandableListView mUnitList;
    private UnitAdapter mUnitAdapter;
    private List<ABALevel> mLevel = new ArrayList<>();
    private ABAUser currentUser;

    private enum DashboardType {
        AnimationTYPE,
        SimpleTYPE
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("destroyed", "destroy");
    }

    @Override
    protected int onResLayout() {
        return R.layout.fragment_dashboard;
    }


    @Override
    protected void onConfigView() {
        mUnitList = $(R.id.dashboradlist);
        mUnitList.setGroupIndicator(null);
        mUnitList.setOnGroupClickListener(this);
        mUnitList.setOnChildClickListener(this);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            mUnitList.setOnScrollListener(new PauseOnScrollListener(ImageLoader.getInstance(), true, true));
        }

        currentUser = DataStore.getInstance().getUserController().getCurrentUser(getRealm());
        mLevel = DataStore.getInstance().getLevelUnitController().getAllLevelsDescending(getRealm());
        mUnitAdapter = new UnitAdapter(getContext(), mLevel, getRealm());
        mUnitList.setExpandibleAdapter(mUnitAdapter);

        //Collapse All Groups
        for (int i = 0; i < mUnitAdapter.getGroupCount(); i++) {
            mUnitList.collapseGroup(i);
        }

        if (currentUser.getCurrentLevel() != null) {
            selectLevel(currentUser.getCurrentLevel(), DashboardType.SimpleTYPE);
        } else {
            selectLevel(mLevel.get(0), DashboardType.SimpleTYPE);
        }

        Activity currentActivity = getActivity();
        MixpanelTrackingManager.getSharedInstance(currentActivity).showNotificationIfAvailable(currentActivity);
        MixpanelTrackingManager.getSharedInstance(currentActivity).showSurveyIfAvailable(currentActivity);
    }

    @Override
    protected void onConfigToolbar(ABATextView toolbarTitle, ABATextView toolbarSubTitle, ImageButton toolbarButton) {
        toolbarTitle.setText(currentUser.getCurrentLevel().getName() + " " + (int) currentUser.getCurrentLevel().getProgress() + "%");
        toolbarTitle.setTextColor(ContextCompat.getColor(getContext(), R.color.abaWhite));
        toolbarSubTitle.setVisibility(View.GONE);

    }

    @Override
    public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
        final ABALevel newLevel = mUnitAdapter.getLevelFromGroupPosition(groupPosition);

        if (mUnitAdapter.getSelectedLevel() != null && newLevel.getIdLevel().equals(mUnitAdapter.getSelectedLevel().getIdLevel())) {
            mUnitList.collapseGroupWithAnimation(groupPosition, null);
        } else if (mUnitAdapter.getSelectedLevel() != null) {
            mUnitList.collapseGroupWithAnimation(groupPosition, new ExpandableAnimationListener() {
                @Override
                public void onFinished() {
                    selectLevel(newLevel, DashboardType.AnimationTYPE);
                }
            });
        } else {
            selectLevel(newLevel, DashboardType.AnimationTYPE);
        }
        return true;
    }

    @Override
    public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
        if (childPosition != 0) {
            String unitID = Long.toString(mUnitAdapter.getChildId(groupPosition, childPosition));

            mUnitList.setSelected(true);
            unitDetailABTestingConfigurator.startUnitDetailIntent(getActivity(), unitID);
            tracker.trackEnteredUnit(LevelUnitController.getIdLevelForUnitId(unitID), unitID);
        }
        return true;
    }

    private void selectLevel(final ABALevel level, DashboardType dashboardType) {
        if (!level.equals(mUnitAdapter.getSelectedLevel())) {
            mUnitAdapter.setSelectedLevel(level);

            if (dashboardType.equals(DashboardType.AnimationTYPE)) {
                mUnitList.expandGroupWithAnimation(mLevel.indexOf(level));
            } else {
                mUnitList.expandGroup(mLevel.indexOf(level));
            }

            scrollAtUnitWithAnimation(DataStore.getInstance().getLevelUnitController().getCurrentUnitForLevel(mUnitAdapter.getSelectedLevel()));
        }
    }

    private void scrollAtUnitWithAnimation(final ABAUnit currentUnitForLevel) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (getActivity() != null && !getActivity().isFinishing()) {
                    int distanceToScroll = calculateDistanceToUnit(currentUnitForLevel);
                    int durationAnimScroll = (int) (((double) distanceToScroll / getExpandibleListViewHeight()) * ABAExpandableListView.ANIMATION_DURATION);
                    mUnitList.smoothScrollBy(distanceToScroll, durationAnimScroll);
                }
            }
        }, ABAExpandableListView.ANIMATION_DURATION);
    }

    private int calculateDistanceToUnit(ABAUnit currentUnitForLevel) {
        int totalHeight = 0;
        if (!currentUnitForLevel.getLevel().isCompleted()) {
            totalHeight += mUnitAdapter.getActualCellHeight();
        } else {
            totalHeight += getContext().getResources().getDimension(R.dimen.dashboardUnitsHeight) + getContext().getResources().getDimension(R.dimen.padding3);
            totalHeight += mUnitAdapter.getStandardCellHeight();
        }
        totalHeight += mUnitAdapter.getCertCellHeight();
        totalHeight += mUnitAdapter.getStandardCellHeight() * DataStore.getInstance().getLevelUnitController().getUnitsDescendingForLevel(currentUnitForLevel.getLevel()).indexOf(currentUnitForLevel);
        totalHeight += (getContext().getResources().getDimension(R.dimen.dashboardUnitsHeight) + getContext().getResources().getDimension(R.dimen.padding3)) * DataStore.getInstance().getLevelUnitController().getAllLevelsDescending(getRealm()).indexOf(currentUnitForLevel.getLevel());
        totalHeight -= getExpandibleListViewHeight();
        return totalHeight;
    }

    private int getExpandibleListViewHeight() {
        int mActionBarSize = (int) getActivity().getTheme().obtainStyledAttributes(new int[]{android.R.attr.actionBarSize}).getDimension(0, 0);
        return BZScreenHelper.getScreenHeight(getContext()) - mActionBarSize - getStatusBarHeight();
    }

    private int getStatusBarHeight() {
        int resourceId = getContext().getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            return getContext().getResources().getDimensionPixelSize(resourceId);
        }
        return 0;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mUnitList.isSelected()) {
            mUnitAdapter.notifyDataSetChanged();
            mUnitList.setSelected(false);
        }
    }
}