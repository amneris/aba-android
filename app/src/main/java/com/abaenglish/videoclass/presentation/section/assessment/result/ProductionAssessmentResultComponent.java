package com.abaenglish.videoclass.presentation.section.assessment.result;

import com.abaenglish.videoclass.presentation.section.assessment.ActivityScope;
import com.abaenglish.videoclass.presentation.section.assessment.EvaluationComponent;

import dagger.Component;

/**
 * Created by xabierlosada on 27/07/16.
 */

@ActivityScope
@Component(modules = AssessmentResultModule.class,
        dependencies = {EvaluationComponent.class})
public interface ProductionAssessmentResultComponent extends AssessmentResultComponent {}
