package com.abaenglish.videoclass.presentation.section.assessment.result;

import com.abaenglish.videoclass.clean.CleanView;

/**
 * Created by xabierlosada on 26/07/16.
 */

public interface AssessmentResultView extends CleanView {

    void renderTeacher(String imageUrl);

    void renderScore(int correctAnswers, int numberOfQuestions);

    void renderPerfectMessage(String unitName);

    void renderPassMessage(String unitName);

    void renderFailMessage();

    void showRepeatEvaluation();

    void showLinkedinShare();

    void hideSocial();

    void hideNextUnit();

    void showRepeatMistakes();

    void showNextUnit(String nextUnitId);

    void showCourseFinished();
}