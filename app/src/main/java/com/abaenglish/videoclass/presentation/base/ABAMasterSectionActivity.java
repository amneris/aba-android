package com.abaenglish.videoclass.presentation.base;

import android.content.res.Configuration;
import android.os.Bundle;

import com.abaenglish.videoclass.analytics.modern.FabricTrackingManager;
import com.abaenglish.videoclass.analytics.modern.MixpanelTrackingManager;
import com.abaenglish.videoclass.domain.LanguageController;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.abaenglish.videoclass.presentation.base.custom.ListenAndRecordControllerView;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Date;

/**
 * Created by Jesus Espejo using mbp13jesus on 18/06/15.
 */
public abstract class ABAMasterSectionActivity extends ABAMasterActivity {

    private double mStudyTimeInSeconds;
    private float mStartProgress;
    private Date mDateTimer;

    protected ListenAndRecordControllerView bottomControllerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mStudyTimeInSeconds = 0;
        LanguageController.currentLanguageOverride();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mStartProgress = getSectionProgress();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (newConfig.orientation == newConfig.ORIENTATION_LANDSCAPE || newConfig.orientation == newConfig.ORIENTATION_PORTRAIT) {
            LanguageController.currentLanguageOverride();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        createTimer();
    }

    @Override
    public void onPause() {
        super.onPause();
        calculateElapsedTimeAndAggregate();

        if(bottomControllerView != null && bottomControllerView.getPlayerControlsListener() != null && bottomControllerView.getSectionControlsListener() != null) {
            bottomControllerView.getSectionControlsListener().OnPausedSection();
        }
    }

    @Override
    public void onDestroy() {
        Object sectionForThisActivity = getSection();
        double finalProgress = getSectionProgress();
        MixpanelTrackingManager.getSharedInstance(this).trackSection(DataStore.getInstance().getUserController().getCurrentUser(getRealm()), sectionForThisActivity, mStudyTimeInSeconds, mStartProgress, finalProgress);
        FabricTrackingManager.trackSection(getRealm(), sectionForThisActivity, mStudyTimeInSeconds, mStartProgress, finalProgress);
        super.onDestroy();
    }

    /**
     * Method to get the section for the activity.
     *
     * @return ABAFilm, ABAWrite, ABASpeak, etc...
     */
    protected abstract Object getSection();

    // ================================================================================
    // Private methods
    // ================================================================================

    private float getSectionProgress() {
        Object sectionForThisActivity = getSection();
        float initialProgress = 0;
        try {
            Method methodToGetProgress = sectionForThisActivity.getClass().getMethod("getProgress");
            initialProgress = (float) methodToGetProgress.invoke(sectionForThisActivity);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return initialProgress;
    }

    private void createTimer() {
        mDateTimer = new Date();
    }

    private void calculateElapsedTimeAndAggregate() {
        Date currentDate = new Date();
        long diffMillis = currentDate.getTime() - mDateTimer.getTime();
        double seconds = diffMillis * 1.0 / 1000.0;
        mStudyTimeInSeconds += seconds;
    }

}
