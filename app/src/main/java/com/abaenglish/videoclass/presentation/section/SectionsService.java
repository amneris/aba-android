package com.abaenglish.videoclass.presentation.section;

import android.content.Context;
import android.support.annotation.NonNull;

import com.abaenglish.videoclass.ABAApplication;
import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.data.ConnectionService;
import com.abaenglish.videoclass.data.network.APIManager;
import com.abaenglish.videoclass.data.network.parser.ABASectionParser;
import com.abaenglish.videoclass.data.persistence.ABAAPIError;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.domain.content.DataController;
import com.android.volley.NoConnectionError;
import com.bzutils.LogBZ;

import org.json.JSONException;

import io.realm.Realm;

/**
 * Created by Jesus Espejo using mbp13jesus on 03/03/16.
 * Class to handle the sections download routine for a given unit
 */
public class SectionsService {

    private static volatile SectionsService instance;

    public static SectionsService getInstance() {
        if (instance == null) {
            synchronized (SectionsService.class) {
                if (instance == null) {
                    instance = new SectionsService();
                }
            }
        }

        return instance;
    }

    public void fetchSections(final Context context, @NonNull ABAUnit currentUnit, final DataController.ABASimpleCallback callback) {

        // First we check if we had already all the sections parsed
        if (currentUnit.isDataDownloaded()) {
            LogBZ.d("Returning unit data from DB");
            callback.onSuccess();
            return;
        }

        // If we reach here, we have to go to the network for getting section content
        final String currentUnitID = currentUnit.getIdUnit();
        APIManager.getInstance().getSectionsContent(currentUnit.getIdUnit(), new APIManager.ABAAPIResponseCallback() {
            @Override
            public void onResponse(String response) {
                Realm realm = Realm.getInstance(ABAApplication.get().getRealmConfiguration());
                try {
                    realm.beginTransaction();
                    ABASectionParser.parseSections(realm, response, currentUnitID);
                    realm.commitTransaction();
                    callback.onSuccess();
                } catch (JSONException e) {
                    if (realm.isInTransaction()) {
                        realm.cancelTransaction();
                    }
                    callback.onError(new ABAAPIError(e.getLocalizedMessage()));
                }
                realm.close();
            }

            @Override
            public void onError(Exception e) {
                if (ConnectionService.isNetworkAvailable(context)) {
                    if (e instanceof NoConnectionError) {
                        callback.onError(new ABAAPIError(context.getString(R.string.errorConnection)));
                    } else {
                        callback.onError(new ABAAPIError("getSections - Error"));
                    }
                } else {
                    callback.onSuccess();
                }
            }
        });
    }


}
