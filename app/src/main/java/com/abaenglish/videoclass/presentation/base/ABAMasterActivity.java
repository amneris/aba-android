package com.abaenglish.videoclass.presentation.base;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog.Builder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.abaenglish.shepherd.ABAShepherdEditor;
import com.abaenglish.shepherd.activities.ShepherdActivity;
import com.abaenglish.shepherd.configuration.configurators.GenericShepherdConfigurator.ShepherdConfiguratorType;
import com.abaenglish.shepherd.configuration.configurators.zendesk.ZendeskShepherdEnvironment;
import com.abaenglish.shepherd.utils.ShakeDetector;
import com.abaenglish.shepherd.utils.ShakeDetector.OnShakeListener;
import com.abaenglish.videoclass.ABAApplication;
import com.abaenglish.videoclass.ForceUpdateActivity;
import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.SplashActivity;
import com.abaenglish.videoclass.analytics.TrackerContract;
import com.abaenglish.videoclass.analytics.cooladata.CooladataTrackingConfiguration;
import com.abaenglish.videoclass.analytics.cooladata.navigation.CooladataNavigationTrackingManager;
import com.abaenglish.videoclass.analytics.helpers.GenericTrackingInterface.PlansViewControllerOriginType;
import com.abaenglish.videoclass.analytics.modern.FabricTrackingManager;
import com.abaenglish.videoclass.analytics.modern.MixpanelTrackingManager;
import com.abaenglish.videoclass.data.CourseContentService;
import com.abaenglish.videoclass.data.network.APIManager;
import com.abaenglish.videoclass.data.network.oauth.OAuthTokenAccessor;
import com.abaenglish.videoclass.data.network.retrofit.clients.ABAMomentBadgeClient;
import com.abaenglish.videoclass.data.persistence.ABAAPIError;
import com.abaenglish.videoclass.data.persistence.ABAUser;
import com.abaenglish.videoclass.domain.ApplicationConfiguration;
import com.abaenglish.videoclass.domain.LanguageController;
import com.abaenglish.videoclass.domain.SupportedVersionManager;
import com.abaenglish.videoclass.domain.abTesting.ServerConfig;
import com.abaenglish.videoclass.domain.abTesting.UnitDetailABTestingConfigurator;
import com.abaenglish.videoclass.domain.content.DataController;
import com.abaenglish.videoclass.domain.content.GenericController;
import com.abaenglish.videoclass.domain.content.LevelUnitController;
import com.abaenglish.videoclass.domain.content.PlanController;
import com.abaenglish.videoclass.domain.content.PlanController.SubscriptionResult;
import com.abaenglish.videoclass.domain.content.UserController;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.abaenglish.videoclass.helpdesk.ZendeskFeedbackConfigurationFactory;
import com.abaenglish.videoclass.presentation.base.custom.ABAErrorNotification;
import com.abaenglish.videoclass.presentation.base.custom.ABAErrorOption;
import com.abaenglish.videoclass.presentation.base.custom.ABAProgressDialog;
import com.abaenglish.videoclass.presentation.base.custom.ABAProgressTask;
import com.abaenglish.videoclass.presentation.base.custom.FontCache;
import com.abaenglish.videoclass.presentation.level.SelectLevelAcivity;
import com.abaenglish.videoclass.presentation.shell.MenuActivity;
import com.abaenglish.videoclass.presentation.tutorial.TutorialActivity;
import com.abaenglish.videoclass.session.SessionService;
import com.bzutils.BZUtils;
import com.bzutils.LogBZ;
import com.crashlytics.android.Crashlytics;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.zendesk.sdk.network.impl.ZendeskConfig;
import com.zendesk.sdk.rating.ui.RateMyAppDialog;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import io.realm.Realm;

/**
 * Created by iaguila on 23/3/15.
 * Modified several times by jespejo, vmadalin and xilosada
 */
public class ABAMasterActivity extends AppCompatActivity {

    // Permissions
    private static final int REQUEST_ID_MICROPHONE_PERMISSION = 3001;
    private static final int REQUEST_ID_STORAGE_PERMISSION = 3002;
    public static final int REQUEST_ID_ALL_PERMISSIONS = 3003;
    private static final int CLICK_INTERVAL = 500;
    public static final int LOGIN_FOR_RESULT = 1111;

    private CallbackManager callbackManager;
    private AccessTokenTracker accessTokenTracker;
    private ProfileTracker profileTracker;
    private ABAProgressTask dialogTask;
    private int mLastHeightDifferece = 30;
    private long mLastClickTime;
    private static boolean forceRefreshLogin = false;

    // Shake
    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private ShakeDetector mShakeDetector;
    private SharedPreferences mFirstRunSharedPreferences = null;
    public static final String RUN_SHARED_PREFERENCES = "isFirstRunSharedPreferences";

    // Realm
    private Realm realm;

    @Inject
    protected UnitDetailABTestingConfigurator unitDetailABTestingConfigurator;

    @Inject
    protected FontCache fontCache;

    @Inject
    public TrackerContract.Tracker tracker;

    @Inject
    public ServerConfig serverConfig;

    @Inject
    protected SessionService mSessionService;

    @Inject
    protected ApplicationConfiguration appConfig;

    @Inject
    protected OAuthTokenAccessor tokenAccessor;

    // ================================================================================
    // Lifecycle
    // ================================================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ABAApplication.get().getApplicationComponent().inject(this);

        // updating remote values
        serverConfig.updateConfigValuesIfNecessary();

        // Tells the OS that the volume buttons should affect the "media" volume
        setVolumeControlStream(AudioManager.STREAM_MUSIC);

        setScreenOrientation();

        LanguageController.currentLanguageOverride();

        callbackManager = CallbackManager.Factory.create();
        addToolbar();

        ABAShepherdEditor shepardEditor = ABAShepherdEditor.shared(super.getBaseContext());

        ZendeskShepherdEnvironment zenDeskShepherdEnvironment =
                (ZendeskShepherdEnvironment) shepardEditor.environmentForShepherdConfigurationType(this, ShepherdConfiguratorType.kShepherdConfiguratorTypeZendesk);

        String zendeskUrl = zenDeskShepherdEnvironment.getBaseUrl();
        String zendeskApplicationId = zenDeskShepherdEnvironment.getApplicationId();
        String zendeskOauthClientId = zenDeskShepherdEnvironment.getOauthClientId();

        ZendeskConfig.INSTANCE.init(this, zendeskUrl, zendeskApplicationId, zendeskOauthClientId);

        // Shake detection for Shepherd
        if (ABAShepherdEditor.isInternal()) {
            // Shake detection

            mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
            mAccelerometer = mSensorManager
                    .getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            mShakeDetector = new ShakeDetector();
            mShakeDetector.setOnShakeListener(new OnShakeListener() {
                @Override
                public void onShake(int count) {
                    Intent shepherdIntent = new Intent(ABAMasterActivity.this, ShepherdActivity.class);
                    startActivity(shepherdIntent);
                }
            });
        }

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {

                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        LogBZ.d("Facebook login success");
                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {
                                        try {
                                            LogBZ.d("Facebook requestMe");
                                            mSessionService.loginWithFacebook(ABAMasterActivity.this, object.getString("email"), object.getString("id"), object.getString("first_name"), object.getString("last_name"), object.getString("gender"), tracker);
                                        } catch (JSONException | NullPointerException e) {
                                            e.printStackTrace();
                                            DataStore.getInstance().getFacebookCallbackController().showFacebookError(getResources().getString(R.string.errorRegister));
                                        }
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,first_name,last_name,link,email,gender");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        LogBZ.d("Facebook login cancel");
                        DataStore.getInstance().getFacebookCallbackController().showFacebookError(getResources().getString(R.string.errorRegister));
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        LogBZ.e("Facebook login error");
                        LogBZ.printStackTrace(exception);
                        DataStore.getInstance().getFacebookCallbackController().showFacebookError(getResources().getString(R.string.errorRegister));
                    }
                });
        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(
                    AccessToken oldAccessToken,
                    AccessToken currentAccessToken) {
                LogBZ.d("Facebook accessToken");
            }
        };
        profileTracker = new ProfileTracker() {
            @Override
            protected void onCurrentProfileChanged(
                    Profile oldProfile,
                    Profile currentProfile) {
                LogBZ.d("Facebook profileChanged");
            }
        };

        // Delete abaEnglishFolder when user reinstall app
        mFirstRunSharedPreferences = getSharedPreferences(RUN_SHARED_PREFERENCES, MODE_PRIVATE);
        if (mFirstRunSharedPreferences.getBoolean(RUN_SHARED_PREFERENCES, true)) {
            File abaEnglishFolder = new File(GenericController.abaEnglishFolderPath);
            File abaEnglishNoMediaFolder = new File(LevelUnitController.abaFolderNoMediaPath);
            GenericController.deleteDirectory(abaEnglishFolder);
            GenericController.deleteDirectory(abaEnglishNoMediaFolder);
        }

        // Checking app support
        if (!(this instanceof ForceUpdateActivity) && !(this instanceof SplashActivity)) {
            SupportedVersionManager.getSharedInstance(this).isCurrentVersionSupported(new SupportedVersionManager.SupportedVersionResponse() {
                @Override
                public void isSupportedVersion(boolean supported, String lastSupportedVersion) {
                    if (!supported) {
                        // Version not supported. Blocking app usage.
                        startForceUpdateActivity();
                    }
                }
            });
        }
    }

    protected void setScreenOrientation() {
        ///Force portrait mode for phones
        if (!APIManager.isTablet(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (ABAShepherdEditor.isInternal()) {
            if (keyCode == KeyEvent.KEYCODE_MENU) {
                Intent shepherdIntent = new Intent(ABAMasterActivity.this, ShepherdActivity.class);
                startActivity(shepherdIntent);
                return true;
            }
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        // Jelly bean code
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            if (newConfig.getLayoutDirection() == Configuration.ORIENTATION_LANDSCAPE || newConfig.getLayoutDirection() == Configuration.ORIENTATION_PORTRAIT) {
                LanguageController.currentLanguageOverride();
            }
        } else { // Before Jelly bean
            LanguageController.currentLanguageOverride();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        Crashlytics.setString("Last onResume", this.getClass().getName());

        updateLoginIfNeeded();
        if (ABAShepherdEditor.isInternal()) {
            mSensorManager.registerListener(mShakeDetector, mAccelerometer, SensorManager.SENSOR_DELAY_UI);
        }

        if (mFirstRunSharedPreferences.getBoolean(RUN_SHARED_PREFERENCES, true)) {
            mFirstRunSharedPreferences.edit().putBoolean(RUN_SHARED_PREFERENCES, false).commit();
        }
    }

    @Override
    public void onDestroy() {
        Crashlytics.setString("Last onDestroy", this.getClass().getName());

        MixpanelTrackingManager.getSharedInstance(this).flush();
        CooladataTrackingConfiguration.flush();

        if (dialogTask != null && !dialogTask.isCancelled()) {
            dialogTask.cancelProgressTask();
        }

        if (realm != null) {
            realm.close();
            realm = null;
        }

        accessTokenTracker.stopTracking();
        profileTracker.stopTracking();
        BZUtils.hideKeyboard(this);
        super.onDestroy();
    }

    public Realm getRealm() {
        if (realm == null) {
            realm = Realm.getInstance(ABAApplication.get().getRealmConfiguration());
        }
        return realm;
    }

    @Override
    public void onPause() {
        if (ABAShepherdEditor.isInternal()) {
            mSensorManager.unregisterListener(mShakeDetector);
        }

        MixpanelTrackingManager.getSharedInstance(this).flush();
        super.onPause();
        BZUtils.hideKeyboard(this);
    }


    // ================================================================================
    // Public methods - Progress dialog
    // ================================================================================

    public void showProgressDialog(int typeProgressDialog) {
        dialogTask = new ABAProgressTask(this, typeProgressDialog);
        dialogTask.execute();
    }

    public void dismissProgressDialog() {
        if (dialogTask != null && !dialogTask.isCancelled()) {
            dialogTask.cancelProgressTask();
            dialogTask = null;
        }
    }

    // ================================================================================
    // Public methods - Action bar
    // ================================================================================

    public void addToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    public void addBackListener() {
        findViewById(R.id.toolbarLeftButton).setOnTouchListener(((ABAApplication) getApplicationContext()).getImagePressedState(null));
        findViewById(R.id.toolbarLeftButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    // ================================================================================
    // Public methods - Notifications
    // ================================================================================

    public ABAErrorNotification showABAAlertNotification(String alert) {
        return showABAErrorNotification(alert, ABAErrorNotification.ABAErrorNotificationType.AlertNotification);
    }

    public ABAErrorNotification showABAErrorNotification(String error) {
        return showABAErrorNotification(error, ABAErrorNotification.ABAErrorNotificationType.ErrorNotification);
    }

    public ABAErrorNotification showABAErrorNotification(String error, ABAErrorNotification.ABAErrorNotificationType abaNotificationType) {
        ABAErrorNotification errorNotification = new ABAErrorNotification(this, abaNotificationType);
        errorNotification.setText(error);
        errorNotification.show();
        return errorNotification;
    }

    public ABAErrorNotification showABAErrorNotificationWithOptions(String error, List<ABAErrorOption> options) {
        ABAErrorNotification errorNotification = new ABAErrorNotification(this, ABAErrorNotification.ABAErrorNotificationType.ErrorNotification);
        errorNotification.setText(error);
        errorNotification.setOptions(options);
        errorNotification.show();
        return errorNotification;
    }


    // ================================================================================
    // Public methods
    // ================================================================================

    public void trackPricingScreenWithOrigin(PlansViewControllerOriginType originType) {
        // Tracking
        tracker.trackSubscriptionShown(originType);
        MixpanelTrackingManager.getSharedInstance(this).trackPricingScreenArrivalWithOriginController(originType);
        FabricTrackingManager.trackPricingShownWithOriginController(originType);

        // Tracking cooladata
        ABAUser user = UserController.getInstance().getCurrentUser(getRealm());
        CooladataNavigationTrackingManager.trackOpenedPrices(user.getUserId());
    }

    // ================================================================================
    // Private methods
    // ================================================================================

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Facebook login
        callbackManager.onActivityResult(requestCode, resultCode, data);

        // Purchases
        if (requestCode == PlanController.PURCHASE_REQUEST_CODE && resultCode == RESULT_OK) {

            int responseCode = data.getIntExtra("RESPONSE_CODE", 0);
            String purchaseData = data.getStringExtra("INAPP_PURCHASE_DATA");
            String purchaseSignature = data.getStringExtra("INAPP_DATA_SIGNATURE");

            showProgressDialog(ABAProgressDialog.SimpleDialog);

            ABAUser user = DataStore.getInstance().getUserController().getCurrentUser(getRealm());
            DataStore.getInstance().getPlanController(tracker).updateUserToPremiumWithReceipt(this, user, purchaseData, purchaseSignature, new PlanController.ABAPlanCallback() {
                @Override
                public void onResult(PlanController.SubscriptionResult type) {

                    dismissProgressDialog();
                    handlePurchaseResult(type);
                }
            });
        } else {
            PlanController.resetOriginalProductIdentifier(this);
            PlanController.resetBoughtProductPromocode(this);
        }
    }

    protected void loginWithFacebook(DataController.ABAAPIFacebookCallback callback) {
        // Removing user for facebook
        LoginManager.getInstance().logOut();

        DataStore.getInstance().getFacebookCallbackController().setFacebookLoginCallback(callback);
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "user_birthday", "email"));
    }

    protected void startABAForUser(boolean isNewUser) {

        ABAUser user = DataStore.getInstance().getUserController().getCurrentUser(getRealm());
        Intent intent;
        if (user.getCurrentLevel() == null || isNewUser) {
            intent = new Intent(this, SelectLevelAcivity.class);
        } else {
            intent = new Intent(this, MenuActivity.class);
            intent.putExtra(MenuActivity.EXTRA_FRAGMENT, MenuActivity.FragmentType.kCurso);
        }
        intent.putExtra(MenuActivity.USER_TYPE, MenuActivity.UserType.kUserJoinApp);
        startActivity(intent);
        setResult(RESULT_OK);
        if (DataStore.getInstance().getCurrentActivity() != null) {
            DataStore.getInstance().getCurrentActivity().finish();
        }

        this.appConfig.setUserEmail(user.getEmail());
        this.appConfig.setUserToken(user.getToken());

        finish();
    }

    protected void focusFieldAfterError(EditText field) {
        field.setSelected(true);
        BZUtils.showKeyboard(this, field);
    }

    protected boolean isViewDisableForClick() {
        long currentClickTime = SystemClock.uptimeMillis();
        long elapsedTime = currentClickTime - mLastClickTime;
        mLastClickTime = currentClickTime;
        return (elapsedTime <= CLICK_INTERVAL);
    }

    /**
     * Función para hacer que la barra inferior quede por encima del teclado cuando lo desplegamos
     *
     * @param v
     */
    private void checkHeightDifference(View v) {
        // get screen frame rectangle
        Rect r = new Rect();

        v.getWindowVisibleDisplayFrame(r);
        // get screen height
        int screenHeight = BZUtils.getScreenHeight(this);
        // calculate the height difference
        int heightDifference = screenHeight - (r.bottom - r.top);

        // if height difference is different then the last height difference and
        // is bigger then a third of the screen we can assume the keyboard is open
        if (heightDifference > screenHeight / 5 && heightDifference != mLastHeightDifferece) {
            // keyboard visiblevisible
            // get root view layout params
            FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) v.getLayoutParams();
            // set the root view height to screen height minus the height difference
            lp.height = screenHeight - heightDifference;
            // call request layout so the changes will take affect
            v.requestLayout();
            // save the height difference so we will run this code only when a change occurs.
            mLastHeightDifferece = heightDifference;
        } else if (heightDifference != mLastHeightDifferece) {
            // keyboard hidden
            // get root view layout params and reset all the changes we have made when the keyboard opened.
            FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) v.getLayoutParams();
            lp.height = screenHeight;
            // call request layout so the changes will take affect
            v.requestLayout();
            // save the height difference so we will run this code only when a change occurs.
            mLastHeightDifferece = heightDifference;
        }
    }

    /**
     * Method to update the login information after coming back to the app.
     */
    protected synchronized void updateLoginIfNeeded() {
        if (forceRefreshLogin) {
            // Since the method will perform a network call we have to show the dialog
            showProgressDialog(ABAProgressDialog.SimpleDialog);
            forceRefreshLogin = false;

            Crashlytics.log(Log.INFO, "Logout", "Updating login - force refresh is true");

            // Getting current user and performing network call
            ABAUser currentUser = DataStore.getInstance().getUserController().getCurrentUser(getRealm());
            mSessionService.loginWithToken(ABAMasterActivity.this, currentUser.getToken(), tracker, new DataController.ABASimpleCallback() {
                @Override
                public void onSuccess() {
                    // Refresh current page
                    // User could be premium from here: update views if necessary.
                    updateAfterLogin();
                    // Network call finished: dismissing progress dialog
                    dismissProgressDialog();
                }

                @Override
                public void onError(ABAAPIError error) {
                    // If there was an error logging in then the user should be logged out
                    Crashlytics.log(Log.INFO, "Logout", "Unable to force refresh the login with the given token.");
                    logout(true);
                }
            });
        }
    }

    protected void logout(final boolean dismissDialog) {
        Crashlytics.log(Log.INFO, "Logout", "ABAMasterActivity performing logout");
        mSessionService.logout(ABAMasterActivity.this, new DataController.ABALogoutCallback() {
            @Override
            public void onLogoutFinished() {
                // Resetting token so that oauth can be init again
                tokenAccessor.reset();
                ABAMomentBadgeClient.reset(ABAMasterActivity.this);

                if (!isFinishing()) {
                    // Network call finished: dismissing progress dialog
                    if (dismissDialog) {
                        dismissProgressDialog();
                    }
                    // And opening tutorial if activity isn't destroyed
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2
                            && isDestroyed()) {
                        return;
                    }

                    openTutorial();
                }
            }
        });
    }

    /**
     * Method to open the tutorial. Cleaning activities stack after starting the tutorial.
     */
    public void openTutorial() {
        tracker.trackRegistrationShown();
        MixpanelTrackingManager.getSharedInstance(this).trackRegistrationShown();
        Intent intent = new Intent(this, TutorialActivity.class);
        startActivity(intent);
        finish();
    }

    protected void startForceUpdateActivity() {
        startActivity(ForceUpdateActivity.getCallingIntent(this));
    }

    protected void updateAfterLogin() {
        // Implement in subclass if necessary
    }

    protected void showZendeskCrashReportDialog() {
        try {
            RateMyAppDialog rateMyAppDialog = new RateMyAppDialog.Builder(ABAMasterActivity.this)
                    .withSendFeedbackButton(ZendeskFeedbackConfigurationFactory.getCrashFeedbackConfiguration(ABAMasterActivity.this))
                    .build();
            rateMyAppDialog.showAlways(ABAMasterActivity.this);

            // Zendesk dialog Display correctly
            ((ABAApplication) getApplicationContext()).setCrashFlagInSharedPreferences(false);
        } catch (RuntimeException e) {
            Crashlytics.logException(e);
            LogBZ.printStackTrace(e);
        }
    }

    // ================================================================================
    // Protected methods
    // ================================================================================

    public void handlePurchaseResult(SubscriptionResult type) {
        final String message = getMessageForPurchaseResult(type);

        // In caso of success
        if (type == SubscriptionResult.SUBSCRIPTION_RESTORE_OK || type == SubscriptionResult.SUBSCRIPTION_RESULT_OK) {

            Crashlytics.log(Log.INFO, "Logout", "Purchase performed.");

            ABAMasterActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (!ABAMasterActivity.this.isFinishing()) {

                        Builder builder = new Builder(ABAMasterActivity.this, R.style.material_alert_dialog);
                        builder.setMessage(message);
                        builder.setCancelable(false);
                        builder.setPositiveButton(R.string.alertSubscriptionOkButton, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                Crashlytics.log(Log.INFO, "Logout", "Restarting app after purchase.");

                                ABAMasterActivity.forceRefreshLogin = true;
                                ABAMasterActivity.this.startActivity(new Intent(ABAMasterActivity.this, SplashActivity.class));
                                ABAMasterActivity.this.finish();
                            }
                        });
                        builder.show();
                    }
                }
            });
        }
        // Case of error
        else {
            showABAErrorNotification(message);
        }
    }

    protected String getMessageForPurchaseResult(SubscriptionResult type) {
        switch (type) {
            case SUBSCRIPTION_RESULT_OK: {
                // "Congratulations! You are now an ABA Premium member!"
                return this.getString(R.string.alertSubscriptionOkMessage);
            }
            case SUBSCRIPTION_RESTORE_OK: {
                // "Congratulations! You are now an ABA Premium member!"
                return this.getString(R.string.alertSubscriptionOkMessage);
            }
            case SUBSCRIPTION_RESULT_KO: {
                // Oops! An error occurred while changing your subscription type. Please try again.
                return this.getString(R.string.alertSubscriptionKOMessage);
            }
            case SUBSCRIPTION_RESTORE_GENERIC_KO: {
                // An error occurred while retrieving your subscription
                return this.getString(R.string.errorFetchingSubscriptions);
            }
            case SUBSCRIPTION_RESULT_KO_AT_ABA_API: {
                // Ha habido un error al comunicar la compra a nuestros servidores. Puedes pulsar en 'Restaurar compras' mÃ¡s tarde desde la secciÃ³n 'Tu cuenta'. Disculpa las molestias
                return this.getString(R.string.alertSubscriptionKOMessage5);
            }
            case SUBSCRIPTION_RESULT_ALREADY_ASSIGNED: {
                // The purchase made is correctly associated with an ABA English user\'s account.
                return this.getString(R.string.alertSubscriptionKOMessage3);
            }
            case SUBSCRIPTION_RESULT_ERROR_FETCHING_SUBSCRIPTIONS: {
                // An error occurred while retrieving your subscription
                return this.getString(R.string.errorFetchingSubscriptions);
            }
            case SUBSCRIPTION_RESULT_NO_SUBSCRIPTIONS: {
                // No in-app purchases have been found
                return this.getString(R.string.errorRestorePurchaseKey);
            }
            default:
                return this.getString(R.string.errorLogout);
        }
    }

    // ================================================================================
    // Private methods for check Permissions
    // ================================================================================

    public boolean hasAllPermisssion() {
        return hasPermissionMicrophoneGranted() && hasPermissionStorageGranted();
    }

    public boolean hasPermissionMicrophoneGranted() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED;
    }

    private boolean hasPermissionStorageGranted() {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    //Display Alert if app dont have all permissions
    private boolean askMicrophonePermission() {
        if (!hasPermissionMicrophoneGranted()) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.RECORD_AUDIO}, REQUEST_ID_MICROPHONE_PERMISSION);
            return false;
        }
        return true;
    }

    private boolean askStoragePermission() {
        if (!hasPermissionStorageGranted()) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_ID_STORAGE_PERMISSION);
            return false;
        }
        return true;
    }


    private boolean askAllPermissions() {
        if (!hasAllPermisssion()) {
            ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.RECORD_AUDIO,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
            }, REQUEST_ID_ALL_PERMISSIONS);
            return false;
        }
        return true;
    }


    // ================================================================================
    // Public methods for check Permissions
    // ================================================================================

    public boolean checkPermissionMicrophone() {
        if (!hasPermissionMicrophoneGranted()) {
            askMicrophonePermission();
        }
        return hasPermissionMicrophoneGranted();
    }

    public boolean checkPermissionStorage() {
        if (!hasPermissionStorageGranted()) {
            askStoragePermission();
        }
        return hasPermissionStorageGranted();
    }

    public boolean checkAllPermissions() {
        if (!hasAllPermisssion()) {
            askAllPermissions();
        }
        return hasPermissionStorageGranted();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (grantResults.length != 0) {
            switch (requestCode) {
                case REQUEST_ID_MICROPHONE_PERMISSION:
                    if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                        showPermissionDeniedSnackBar(getString(R.string.noMicrophoneAndroidTextKey));
                    }
                    break;
                case REQUEST_ID_STORAGE_PERMISSION:
                    if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                        showPermissionDeniedSnackBar(getString(R.string.noStorageAndroidTextKey));
                    }
                    break;
                case REQUEST_ID_ALL_PERMISSIONS:
                    if (!hasPermissionMicrophoneGranted()) {
                        showPermissionDeniedSnackBar(getString(R.string.noMicrophoneAndroidTextKey));
                    } else if (!hasPermissionStorageGranted()) {
                        showPermissionDeniedSnackBar(getString(R.string.noStorageAndroidTextKey));
                    }
                    break;
            }
        }
    }

    private void showPermissionDeniedSnackBar(final String messageAlert) {
        Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), messageAlert, Snackbar.LENGTH_LONG)
                .setAction(R.string.alertDialogAndroidSettingsButton, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent();
                        intent.setAction(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivity(intent);
                    }
                });
        TextView textView = (TextView) snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);
        textView.setMaxLines(5);
        snackbar.show();
    }
}