package com.abaenglish.videoclass.presentation.section.exercise;

import android.app.FragmentManager;
import android.content.res.Configuration;
import android.graphics.Point;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.text.InputType;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.abaenglish.shepherd.ABAShepherdEditor;
import com.abaenglish.shepherd.plugin.plugins.ShepherdAutomatorPlugin;
import com.abaenglish.videoclass.ABAApplication;
import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.analytics.cooladata.navigation.CooladataNavigationTrackingManager;
import com.abaenglish.videoclass.analytics.cooladata.study.CooladataStudyTrackingManager;
import com.abaenglish.videoclass.data.network.APIManager;
import com.abaenglish.videoclass.data.persistence.ABAExercisePhraseItem;
import com.abaenglish.videoclass.data.persistence.ABAExercises;
import com.abaenglish.videoclass.data.persistence.ABAExercisesGroup;
import com.abaenglish.videoclass.data.persistence.ABAExercisesQuestion;
import com.abaenglish.videoclass.data.persistence.ABAPhrase;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.data.persistence.ABAUser;
import com.abaenglish.videoclass.domain.content.SolutionTextController;
import com.abaenglish.videoclass.domain.content.UserController;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.abaenglish.videoclass.presentation.base.ABAMasterSectionActivity;
import com.abaenglish.videoclass.presentation.base.AudioController;
import com.abaenglish.videoclass.presentation.base.custom.ABAExercisesTextField;
import com.abaenglish.videoclass.presentation.base.custom.ABATextView;
import com.abaenglish.videoclass.presentation.base.custom.FontCache;
import com.abaenglish.videoclass.presentation.section.NextSectionDialog_;
import com.abaenglish.videoclass.presentation.section.SectionActivityInterface;
import com.abaenglish.videoclass.presentation.section.SectionHelper;
import com.abaenglish.videoclass.presentation.section.SectionType;
import com.bzutils.BZScreenHelper;
import com.bzutils.BZUtils;
import com.crashlytics.android.Crashlytics;

import java.util.ArrayList;
import java.util.HashMap;

import static com.abaenglish.videoclass.analytics.cooladata.CooladataTrackingDefinitions.CooladataSectionType.ABAExercises;

/**
 * Created by madalin on 7/05/15.
 */
public class ABAExercisesActivity extends ABAMasterSectionActivity implements Animation.AnimationListener, SectionActivityInterface, View.OnClickListener, AudioController.PlayerControllerInterface {

    private ABAUnit currentUnit;
    private ABAUser currentUser;
    private ABAExercises section;
    private ABAExercisesGroup currentGroup;
    private ABAExercisesQuestion currentQuestion;
    private String correctAnswerPhraseId;
    private ExercisesStatusType status;
    private boolean readOnlyMode;

    private SolutionTextController solutionTextController;

    private ArrayList<LinearLayout> horizontalLayouts;
    private ArrayList<ABAExercisesTextField> blankTextFields;
    private ArrayList<Object> elementsArray;

    private enum ExercisesStatusType {
        kABAExercisesStatusInit(1),
        kABAExercisesStatusMustShowExercise(2),
        kABAExercisesStatusExerciseOK(3),
        kABAExercisesStatusExerciseKO(4),
        kABAExercisesStatusMustSave(5),
        kABAExercisesStatusMustShowNextExercise(6),
        kABAExercisesStatusMustJumpToNextGroup(7),
        kABAExercisesStatusMustShowNextGroup(8),
        kABAExercisesStatusDone(9);

        private final int value;

        ExercisesStatusType(final int newValue) {
            value = newValue;
        }

        public int getValue() {
            return value;
        }
    }

    private ABATextView titleTextView;
    private ABATextView translationTextView;
    private ABATextView solutionTextView;
    private LinearLayout verticalLinearLayout;

    private ABATextView checkAnswersButton;
    private RelativeLayout helpButton;
    private RelativeLayout checkNotificationContent;
    private LinearLayout exercicesLayout;

    private ABATextView checkNotificationText;
    private ImageView checkNotifiacationIcon;

    private Animation slideInFromRight;
    private Animation slideOutFromRight;
    private Animation slideInFromLeft;
    private Animation slideOutFromLeft;
    private Animation rotateDegreesLeft90;
    private Animation rotateDegressRight90;
    private Animation fadeIn;
    private Animation fadeOut;
    private Animation exerciseRotateAnimation;

    private boolean helpPressed = false;
    private boolean isAnimationRun = false;
    private boolean isNotificationShow = false;
    private boolean isHelpShow = false;

    private Handler handler;
    private Handler handlerNotification;
    private Handler handlerKeyboardAnim;
    private Runnable runnable;
    private Runnable runnableNotification;
    private Runnable runnableKeyboardAnim;
    private boolean isKeyboardAnimStart = false;

    // ================================================================================
    // Lifecycle
    // ================================================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercises);

        initApp(getIntent().getExtras().getString(EXTRA_UNIT_ID));
        configActionBar();
        config();

        DataStore.getInstance().getAudioController().addPlayerControllerListener(this);

        solutionTextController = new SolutionTextController();
        solutionTextController.strictContractForms = false;

        updateStatus(ExercisesStatusType.kABAExercisesStatusInit);
        BZUtils.showKeyboard(getApplicationContext(), blankTextFields.get(0));

        trackSection();
    }

    // ================================================================================
    // Private methods - Activity configuration
    // ================================================================================

    private void initApp(String UnitID) {
        currentUnit = DataStore.getInstance().getLevelUnitController().getUnitWithId(getRealm(), UnitID);
        currentUser = UserController.getInstance().getCurrentUser(getRealm());
        section = currentUnit.getSectionExercises();
    }

    private void config() {
        titleTextView = (ABATextView) findViewById(R.id.titleTextView);
        titleTextView.setTypeface(fontCache.getTypeface(FontCache.ABATypeface.sans700));

        translationTextView = (ABATextView) findViewById(R.id.translationTextView);
        translationTextView.setTypeface(fontCache.getTypeface(FontCache.ABATypeface.sans500));

        solutionTextView = (ABATextView) findViewById(R.id.solutionTextView);
        solutionTextView.setTypeface(fontCache.getTypeface(FontCache.ABATypeface.sans500));

        verticalLinearLayout = (LinearLayout) findViewById(R.id.verticalLinearLayout);

        checkAnswersButton = (ABATextView) findViewById(R.id.checkAnswersButton);
        helpButton = (RelativeLayout) findViewById(R.id.help_button);
        checkNotificationContent = (RelativeLayout) findViewById(R.id.check_notification);
        checkNotificationText = (ABATextView) findViewById(R.id.check_notificationText);
        checkNotifiacationIcon = (ImageView) findViewById(R.id.check_notificationIcon);
        exercicesLayout = (LinearLayout) findViewById(R.id.exercisesLayout);

        slideInFromRight = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_in_from_right);
        slideOutFromRight = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_out_to_right);
        slideInFromLeft = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_in_from_left);
        slideOutFromLeft = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_out_to_left);
        rotateDegreesLeft90 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_degrees90_left);
        rotateDegressRight90 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_degress90_right);
        fadeIn = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in_evaluation);
        fadeOut = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_out_evaluation);
        exerciseRotateAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_exercise);

        checkAnswersButton.setText(getResources().getString(R.string.checkResultExercisesKey).toUpperCase());

        slideInFromRight.setAnimationListener(this);
        slideOutFromRight.setAnimationListener(this);
        rotateDegreesLeft90.setAnimationListener(this);
        exerciseRotateAnimation.setAnimationListener(this);

        slideInFromRight.setFillAfter(true);
        slideOutFromRight.setFillAfter(true);
        slideInFromLeft.setFillAfter(true);
        slideOutFromLeft.setFillAfter(true);
        rotateDegreesLeft90.setFillAfter(true);
        rotateDegressRight90.setFillAfter(true);
        fadeOut.setFillAfter(true);
        fadeIn.setFillAfter(true);

        runnable = new Runnable() {
            @Override
            public void run() {
                hideHelpBar();
            }
        };
        runnableNotification = new Runnable() {
            @Override
            public void run() {
                hideAlert();
            }
        };
        runnableKeyboardAnim = new Runnable() {
            @Override
            public void run() {
                BZUtils.showKeyboard(getApplicationContext(), blankTextFields.get(0));
                checkAnswersButton.setVisibility(View.VISIBLE);
                isKeyboardAnimStart = false;
            }
        };

        helpButton.setOnClickListener(this);
        checkAnswersButton.setOnClickListener(this);
    }

    // ================================================================================
    // Private methods
    // ================================================================================

    private void updateStatus(ExercisesStatusType newStatus) {
        status = newStatus;

        switch (status) {
            case kABAExercisesStatusInit: {

                currentQuestion = DataStore.getInstance().getExercisesController().getCurrentABAExercisesQuestion(section);

                if (currentQuestion == null || section.isCompleted()) {
                    readOnlyMode = true;

                    currentGroup = section.getExercisesGroups().get(0);
                    currentQuestion = currentGroup.getQuestions().get(0);
                    checkAnswersButton.setText(R.string.nextExercisesKey);
                } else {
                    currentGroup = currentQuestion.getExercisesGroup();
                }

                updateElements();
                updateStatus(ExercisesStatusType.kABAExercisesStatusMustShowExercise);

                break;
            }
            case kABAExercisesStatusMustShowExercise: {

                setupTitles();
                setupExerciseContent();

                if (readOnlyMode) {
                    fillAnswersInTextFields();
                }

                if (ABAShepherdEditor.isInternal() && ShepherdAutomatorPlugin.isAutomationEnabled(this)) {

                    fillAnswersInTextFields();
                    ShepherdAutomatorPlugin.automateMethod(this, new ShepherdAutomatorPlugin.ABAShepherdAutomatorPluginAction() {
                        @Override
                        public void automate() {
                            // Not starting the automation directly
                            // checkAnswersButton();
                        }
                    });
                }

                break;
            }
            case kABAExercisesStatusExerciseOK: {

                showCorrectAlert();
                playCorrectAnswer();

                break;
            }
            case kABAExercisesStatusExerciseKO: {

                showIncorrectAlert();

                break;
            }
            case kABAExercisesStatusMustSave: {

                if (!readOnlyMode) {
                    DataStore.getInstance().getExercisesController().setBlankPhrasesDoneForExercisesQuestion(getRealm(), currentQuestion, section);
                    updateProgressPercentage();
                }

                // We jump to the next question or next group
                int indexOfCurrentQuestion = currentQuestion.getExercisesGroup().getQuestions().indexOf(currentQuestion);
                if (!isOnlyTitleStructure()) {
                    //Jump to another
                    indexOfCurrentQuestion = indexOfCurrentQuestion + 2;
                } else {
                    indexOfCurrentQuestion++;
                }

                if (indexOfCurrentQuestion <= currentQuestion.getExercisesGroup().getQuestions().size() - 1) {
                    currentQuestion = currentQuestion.getExercisesGroup().getQuestions().get(indexOfCurrentQuestion);
                    currentGroup = currentQuestion.getExercisesGroup();

                    updateElements();

                    updateStatus(ExercisesStatusType.kABAExercisesStatusMustShowNextExercise);
                } else {
                    updateStatus(ExercisesStatusType.kABAExercisesStatusMustJumpToNextGroup);
                }

                break;
            }
            case kABAExercisesStatusMustShowNextExercise: {

                dropCurrentExerciseAnimation();

                if (readOnlyMode) {
                    fillAnswersInTextFields();
                }

                if (ABAShepherdEditor.isInternal() && ShepherdAutomatorPlugin.isAutomationEnabled(this)) {

                    fillAnswersInTextFields();
                    ShepherdAutomatorPlugin.automateMethod(this, new ShepherdAutomatorPlugin.ABAShepherdAutomatorPluginAction() {
                        @Override
                        public void automate() {
                            checkAnswersButton();
                        }
                    });
                }

                checkAnswersButton.setEnabled(true);
                isKeyboardAnimStart = true;
                handlerKeyboardAnim = new Handler();
                handlerKeyboardAnim.postDelayed(runnableKeyboardAnim, 1500);


                break;
            }
            case kABAExercisesStatusMustJumpToNextGroup: {

                int indexCurrentGroup = section.getExercisesGroups().indexOf(currentGroup);
                indexCurrentGroup++;

                if (indexCurrentGroup <= section.getExercisesGroups().size() - 1) {
                    ABAExercisesGroup nextGroup = section.getExercisesGroups().get(indexCurrentGroup);

                    currentQuestion = nextGroup.getQuestions().get(0);
                    currentGroup = currentQuestion.getExercisesGroup();

                    updateElements();

                    updateStatus(ExercisesStatusType.kABAExercisesStatusMustShowNextGroup);
                } else {
                    updateStatus(ExercisesStatusType.kABAExercisesStatusDone);
                }

                break;
            }

            case kABAExercisesStatusMustShowNextGroup: {

                dropCurrentExerciseAnimation();

                if (readOnlyMode) {
                    fillAnswersInTextFields();
                }

                if (ABAShepherdEditor.isInternal() && ShepherdAutomatorPlugin.isAutomationEnabled(this)) {

                    fillAnswersInTextFields();
                    ShepherdAutomatorPlugin.automateMethod(this, new ShepherdAutomatorPlugin.ABAShepherdAutomatorPluginAction() {
                        @Override
                        public void automate() {
                            checkAnswersButton();
                        }
                    });
                }

                checkAnswersButton.setEnabled(true);

                isKeyboardAnimStart = true;
                handlerKeyboardAnim = new Handler();
                handlerKeyboardAnim.postDelayed(runnableKeyboardAnim, 1500);

                break;
            }
            case kABAExercisesStatusDone: {
                if (serverConfig.isImprovedLinearCourseActive() && !readOnlyMode && !NextSectionDialog_.checkNextSectionIsDone(currentUnit.getIdUnit(), 6)) {
                    showNextSectionDialog();
                } else {
                    finishSection(false);
                }
                break;
            }
            default:
                break;
        }
    }

    private void updateElements() {
        int indexOfCurrentQuestion = currentQuestion.getExercisesGroup().getQuestions().indexOf(currentQuestion);

        ArrayList<ABAExercisePhraseItem> itemsArray = DataStore.getInstance().getExercisesController().getExercisesItemsForExerciseQuestion(currentQuestion);
        // Nice  structure for avoid exercise with only title : Ex unit 13
        if (!isOnlyTitleStructure()) {
            ArrayList<ABAExercisePhraseItem> itemsArray2 = DataStore.getInstance().getExercisesController().getExercisesItemsForExerciseQuestion(currentQuestion.getExercisesGroup().getQuestions().get(indexOfCurrentQuestion + 1));
            for (ABAExercisePhraseItem exercisePhraseItem : itemsArray2) {
                itemsArray.add(exercisePhraseItem);
            }
        }

        elementsArray = getElementsWithItemsArray(itemsArray);
        blankTextFields = getBlankTextFieldsForElementsArray(elementsArray);
    }

    private boolean isOnlyTitleStructure() {
        for (ABAPhrase phrase : currentQuestion.getPhrases()) {
            if (!phrase.getBlank().toString().equals("") && !phrase.getAudioFile().toString().equals("")) {
                return true;
            }
        }
        return false;
    }

    private ArrayList<Object> getElementsWithItemsArray(ArrayList<ABAExercisePhraseItem> itemsArray) {
        ArrayList<Object> arrayList = new ArrayList<>();

        for (ABAExercisePhraseItem item : itemsArray) {
            if (item.getType() == ABAExercisePhraseItem.ABAExercisePhraseItemType.kABAExercisePhraseItemTypeNormal) {
                ABATextView tv = new ABATextView(this);
                tv.setText(item.getText());
                tv.setTypeface(fontCache.getTypeface(FontCache.ABATypeface.sans500));
                tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);

                tv.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));

                tv.measure(0, 0);
                int width = tv.getMeasuredWidth();
                tv.setLayoutParams(new LinearLayout.LayoutParams(width, LinearLayout.LayoutParams.WRAP_CONTENT));

                arrayList.add(tv);
            } else {
                ABAExercisesTextField tf = new ABAExercisesTextField(this);
                tf.setSingleLine(true);
                tf.setCorrectAnswer(item);
                tf.setText(item.getText());
                tf.setTypeface(fontCache.getTypeface(FontCache.ABATypeface.sans700));
                tf.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                tf.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);

                GradientDrawable gdDefault = new GradientDrawable();
                gdDefault.setColor(ContextCompat.getColor(this, R.color.abaWhite));
                gdDefault.setCornerRadius(5);
                gdDefault.setStroke(1, ContextCompat.getColor(this, R.color.writeHelpBarColor));

                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    tf.setBackgroundDrawable(gdDefault);
                } else {
                    tf.setBackground(gdDefault);
                }
                tf.setTextColor(ContextCompat.getColor(this, R.color.abaWhite));
                tf.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));

                tf.measure(0, 0);
                int width = tf.getMeasuredWidth();

                tf.setLayoutParams(new LinearLayout.LayoutParams(width, LinearLayout.LayoutParams.WRAP_CONTENT));
                tf.setText("");
                tf.setTextColor(ContextCompat.getColor(this, R.color.abaGrey));
                ((LinearLayout.LayoutParams) tf.getLayoutParams()).setMargins((int) getResources().getDimension(R.dimen.padding5), 0, (int) getResources().getDimension(R.dimen.padding10), 0);

                arrayList.add(tf);
            }
        }

        return arrayList;
    }

    private ArrayList<ABAExercisesTextField> getBlankTextFieldsForElementsArray(ArrayList<Object> elements) {
        ArrayList<ABAExercisesTextField> arrayList = new ArrayList<>();

        for (Object object : elements) {
            if (object instanceof ABAExercisesTextField) {
                arrayList.add((ABAExercisesTextField) object);
            }
        }

        return arrayList;
    }

    private void setupTitles() {
        translationTextView.setText(currentQuestion.getExerciseTranslation());
        titleTextView.setText(currentQuestion.getExercisesGroup().getTitle());
        solutionTextView.setText(getSolutionString());
    }

    private void setupExerciseContent() {

        horizontalLayouts = new ArrayList<>();
        horizontalLayouts.add(horizontalLayout());

        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        int width = BZUtils.getScreenWidht(this);
        double maxWidthD;
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT && !APIManager.isTablet(this)) {
            maxWidthD = width * 0.70;
        } else {
            maxWidthD = width * 0.80;
        }

        int maxWidth = (int) maxWidthD;
        int lineWidth = 0;
        int index = 0;
        int nextBlankWidth = 0;

        for (Object element : elementsArray) {
            ViewGroup.LayoutParams llp = ((View) element).getLayoutParams();
            int sizeN = llp.width;
            lineWidth += sizeN;

            if (element instanceof ABATextView && currentUnit.getIdUnit().equals("13")) {
                //Check if the next element is blank and if it have space to put in same line else new line
                if (index < elementsArray.size() - 1) {
                    Object nextElement = elementsArray.get(index + 1);
                    //The next element is blank
                    if (nextElement instanceof ABAExercisesTextField) {
                        ViewGroup.LayoutParams llpNextElement = ((View) nextElement).getLayoutParams();
                        nextBlankWidth = llpNextElement.width;
                        lineWidth += nextBlankWidth;
                    }
                }
            }

            //check if child has already parent
            ViewGroup childParent = (ViewGroup) ((View) element).getParent();
            if (childParent != null)
                childParent.removeView((View) element);


            if (lineWidth <= maxWidth) {
                horizontalLayouts.get(horizontalLayouts.size() - 1).addView((View) element);
            } else {
                lineWidth = 0;
                nextBlankWidth = 0;
                horizontalLayouts.add(horizontalLayout());
                horizontalLayouts.get(horizontalLayouts.size() - 1).addView((View) element);
            }

            index++;
            lineWidth -= nextBlankWidth;
            nextBlankWidth = 0;
        }

        for (LinearLayout ll : horizontalLayouts) {
            verticalLinearLayout.addView(ll);
        }
    }

    /*
    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }
    */

    private LinearLayout horizontalLayout() {
        LinearLayout horizontal = new LinearLayout(this);
        horizontal.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        horizontal.setOrientation(LinearLayout.HORIZONTAL);
        horizontal.setPadding(0, (int) getResources().getDimension(R.dimen.padding5), 0, (int) getResources().getDimension(R.dimen.padding5));

        return horizontal;
    }

    private String getSolutionString() {
        String solution = "";

        for (ABAExercisesTextField etf : blankTextFields) {
            solution += solutionTextController.formatSolutionText(etf.getCorrectAnswer().getText());

            if (blankTextFields.indexOf(etf) != blankTextFields.size() - 1) {
                solution += ", ";
            }
        }

        return solution;
    }

    private void fillAnswersInTextFields() {
        for (ABAExercisesTextField tf : blankTextFields) {
            tf.setText(tf.getCorrectAnswer().getText());
            tf.setTextColor(ContextCompat.getColor(this, R.color.abaEmailGreen));
            tf.setSelection(tf.getCorrectAnswer().getText().length());
        }
    }

    private void configActionBar() {
        findViewById(R.id.toolbarLeftButton).setOnTouchListener(((ABAApplication) getApplicationContext()).getImagePressedState(null));
        findViewById(R.id.toolbarLeftButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishSection(false);
            }
        });

        updateProgressPercentage();
    }

    private void helpButton() {

        if (!isAnimationRun) {
            if (!helpPressed) {
                showHelpBar();
            } else {
                hideHelpBar();
            }
            isAnimationRun = true;

            if (isNotificationShow) {
                hideAlert();
            }
        }
    }

    private void checkAnswersButton() {

        if (readOnlyMode) {
            checkAnswersButton.setEnabled(false);
            updateStatus(ExercisesStatusType.kABAExercisesStatusExerciseOK);
        } else {
            if (checkAnswers()) {
                checkAnswersButton.setEnabled(false);
                updateStatus(ExercisesStatusType.kABAExercisesStatusExerciseOK);
            } else {
                if (!isAllBlankCorrectOrNull()) {
                    updateStatus(ExercisesStatusType.kABAExercisesStatusExerciseKO);
                }
            }
        }
    }

    private boolean isAllBlankCorrectOrNull() {
        for (ABAExercisesTextField tf : blankTextFields) {
            if (!tf.getText().toString().equals("")) {
                if (!checkTextInputForBlankTextfield(tf)) {
                    return false;
                }
            }
        }

        if (!blankTextFields.get(getPositionFocusedTextField()).getText().toString().equals("")) {
            return true;
        } else {
            return false;
        }
    }

    public int getPositionFocusedTextField() {
        int i = 0;
        for (ABAExercisesTextField tf : blankTextFields) {
            if (tf.hasFocus()) {
                return i;
            }
            i++;
        }
        //If we dont have any textfield focus
        return 0;
    }

    private boolean checkAnswers() {
        boolean correct = true;

        for (ABAExercisesTextField tf : blankTextFields) {

            if (checkTextInputForBlankTextfield(tf)) {
                ABAPhrase phr = new ABAPhrase();
                phr.setAudioFile(tf.getCorrectAnswer().getIdPhrase());
                phr.setPage(tf.getCorrectAnswer().getPage());

                DataStore.getInstance().getExercisesController().setPhraseDone(getRealm(), phr, section, true);

                // Tracking verified text
                CooladataStudyTrackingManager.trackVerifiedText(currentUser.getUserId(), currentUnit.getLevel().getIdLevel(), currentUnit.getIdUnit(), ABAExercises, tf.getCorrectAnswer().getIdPhrase(), true, tf.getText().toString());
                tracker.trackVerifiedText(currentUnit.getLevel().getIdLevel(), currentUnit.getIdUnit(), ABAExercises.toString(), tf.getCorrectAnswer().getIdPhrase(), true, tf.getText().toString());

            } else {
                ABAPhrase phr = new ABAPhrase();
                phr.setAudioFile(tf.getCorrectAnswer().getIdPhrase());
                phr.setPage(tf.getCorrectAnswer().getPage());

                DataStore.getInstance().getExercisesController().setPhraseKO(getRealm(), phr, section, tf.getText().toString());
                correct = false;

                // Tracking verified text
                CooladataStudyTrackingManager.trackVerifiedText(currentUser.getUserId(), currentUnit.getLevel().getIdLevel(), currentUnit.getIdUnit(), ABAExercises, tf.getCorrectAnswer().getIdPhrase(), false, tf.getText().toString());
                tracker.trackVerifiedText(currentUnit.getLevel().getIdLevel(), currentUnit.getIdUnit(), ABAExercises.toString(), tf.getCorrectAnswer().getIdPhrase(), false, tf.getText().toString());
            }
        }

        return correct;
    }

    private boolean checkTextInputForBlankTextfield(ABAExercisesTextField tf) {

        // First we convert the text following some rules and for mantaining the same coherence (contracted forms, lower/uppercase, etc)
        String answerFormatted = solutionTextController.formatSolutionText(tf.getText().toString());

        if (answerFormatted.length() == 0) {
            return false;
        }

        String capitalizedAnswerFormatted = capitalizeFirstLetter(answerFormatted);

        // We get a dictionary in which the key is the index of the word in the phrase. The value will be blank if the word is correct.
        HashMap<Integer, String> errorDict = solutionTextController.checkSolutionText(capitalizedAnswerFormatted, tf.getCorrectAnswer().getText());

        boolean almostOneWordIsIncorrect = solutionTextController.checkPhrase(capitalizedAnswerFormatted, errorDict);
        boolean missingWords = solutionTextController.areThereMissingWords(capitalizedAnswerFormatted, errorDict);

        if (almostOneWordIsIncorrect) {
            Spannable textFormatted = solutionTextController.getSpannableString(answerFormatted, errorDict, this);
            tf.setText(textFormatted);

            if (!missingWords) {
                tf.setSelection(solutionTextController.getFirstIncorrectWordPosition(answerFormatted, errorDict));
            } else {
                tf.setSelection(textFormatted.length());
            }
        } else {
            String text;

            if (solutionTextController.mustChangeOriginalText) {
                text = solutionTextController.formatSolutionText(tf.getCorrectAnswer().getText());
            } else {
                text = tf.getText().toString();
            }

            Spannable formattedString = new SpannableString(text);
            formattedString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.abaEmailGreen)), 0, text.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            tf.setText(formattedString);
            tf.setSelection(formattedString.length());
        }

        return !almostOneWordIsIncorrect;
    }

    private String capitalizeFirstLetter(String textToCap) {

        if (textToCap.length() == 0) {
            return "";
        }

        String firstChar = textToCap.substring(0, 1).toUpperCase();
        String restOfLetters = textToCap.substring(1, textToCap.length());

        return firstChar + restOfLetters;
    }

    private void showCorrectAlert() {

        if (isHelpShow) {
            hideHelpBar();
        }
        if (!isNotificationShow) {
            checkNotificationContent.setBackgroundColor(ContextCompat.getColor(this, R.color.abaEmailGreen));
            checkNotifiacationIcon.setVisibility(View.VISIBLE);
            checkNotificationText.setText(getResources().getString(R.string.sectionExercisesOKKey));
            checkNotificationContent.startAnimation(fadeIn);
            isNotificationShow = true;
        }
    }

    private void showIncorrectAlert() {
        if (isHelpShow) {
            hideHelpBar();
        }
        if (!isNotificationShow) {
            checkNotificationContent.setBackgroundColor(ContextCompat.getColor(this, R.color.borderEditTextError));
            checkNotifiacationIcon.setVisibility(View.GONE);
            checkNotificationText.setText(getResources().getString(R.string.sectionExercisesKOKey));

            checkNotificationContent.startAnimation(fadeIn);
            isNotificationShow = true;
            handlerNotification = new Handler();
            handlerNotification.postDelayed(runnableNotification, 300);
        }
    }

    private void hideAlert() {
        checkNotificationContent.startAnimation(fadeOut);
        isNotificationShow = false;
    }

    private void showHelpBar() {
        helpButton.startAnimation(rotateDegreesLeft90);
        helpPressed = true;
        isHelpShow = true;
        handler = new Handler();
        handler.postDelayed(runnable, 4000);

        // Analytics to ABA Analytics
        sendHelpAction();

        // Analytics to Cooladata
        CooladataStudyTrackingManager.trackTextSuggestion(currentUser.getUserId(), currentUnit.getLevel().getIdLevel(), currentUnit.getIdUnit(), ABAExercises, blankTextFields.get(0).getCorrectAnswer().getIdPhrase());
        tracker.trackGotTextSuggestion(currentUnit.getLevel().getIdLevel(), currentUnit.getIdUnit(), ABAExercises.toString(), blankTextFields.get(0).getCorrectAnswer().getIdPhrase());
    }

    private void hideHelpBar() {
        helpButton.startAnimation(rotateDegressRight90);
        solutionTextView.startAnimation(slideOutFromRight);
        translationTextView.startAnimation(slideInFromLeft);
        helpPressed = false;
        isHelpShow = false;
        handler.removeCallbacks(runnable);
    }

    private void sendHelpAction() {
        ABAPhrase phr = new ABAPhrase();
        phr.setAudioFile(blankTextFields.get(0).getCorrectAnswer().getAudioFile());
        phr.setPage(blankTextFields.get(0).getCorrectAnswer().getPage());

        DataStore.getInstance().getExercisesController().sendHelpAction(getRealm(), phr, section);
    }

    private void removeHorizontalLayouts() {
        verticalLinearLayout.removeAllViews();
    }

    private void dropCurrentExerciseAnimation() {
        final int KeyboardHeight = (BZScreenHelper.getScreenHeight(this) - findViewById(R.id.rootLayout).getHeight() + exercicesLayout.getHeight() + getStatusBarHeight());
        //  final int KeyboardHeight = findViewById(R.id.rootLayout).getHeight();
        BZUtils.hideKeyboard(this);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                checkAnswersButton.setVisibility(View.GONE);
                exercicesLayout.setVerticalGravity(Gravity.NO_GRAVITY);
                exercicesLayout.getLayoutParams().height = KeyboardHeight;
                exercicesLayout.startAnimation(exerciseRotateAnimation);
            }
        }, 150);
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    private void showNextSectionDialog() {
        FragmentManager fragmentManager = getFragmentManager();
        NextSectionDialog_ ndf = NextSectionDialog_.newInstance(SectionHelper.SECTION_EXERCISES, currentUnit.getIdUnit());
        ndf.addListener(this);
        ndf.show(fragmentManager, "Write");
    }

    // ================================================================================
    // NextSectionDialog
    // ================================================================================

    @Override
    public void goToNext(boolean result) {
        if (result) {
            startActivity(SectionHelper.getNextSectionIntent(this, Integer.parseInt(currentUnit.getIdUnit()), SectionHelper.SECTION_EXERCISES));
            finish();
        } else {
            finishSection(true);
        }
    }

    // ================================================================================
    // OnClickListener
    // ================================================================================

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.help_button:
                if (checkAnswersButton.isEnabled())
                    helpButton();
                break;
            case R.id.checkAnswersButton:
                checkAnswersButton();
                break;
        }
    }
    // ================================================================================
    // SessionActivityInterface
    // ================================================================================

    @Override
    public void configTeacher() {
    }

    @Override
    public void finishSection(boolean completed) {

        if (completed) {
            unitDetailABTestingConfigurator.startUnitDetailIntent(this, getIntent().getExtras().getString(EXTRA_UNIT_ID), SectionType.kEjercicios);
        } else {
            unitDetailABTestingConfigurator.startUnitDetailIntent(this, getIntent().getExtras().getString(EXTRA_UNIT_ID));
        }

        if (isKeyboardAnimStart) {
            handlerKeyboardAnim.removeCallbacks(runnableKeyboardAnim);
        }

        DataStore.getInstance().getAudioController().removePlayerControllerListener();
        DataStore.getInstance().getAudioController().stopAudioController();
        finish();
        overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_exit_unitanimation2);
    }

    @Override
    public void updateSectionProgress() {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishSection(false);
    }


    @Override
    public void onAnimationStart(Animation animation) {
        if (animation == slideInFromRight) {
            if (translationTextView.getHeight() != 0) {
                solutionTextView.setHeight(translationTextView.getHeight());
            }
            solutionTextView.setVisibility(View.VISIBLE);
        } else if (animation == fadeIn) {
            checkNotificationContent.setVisibility(View.VISIBLE);
        } else if (animation == exerciseRotateAnimation) {
            solutionTextView.setDrawingCacheEnabled(true);
        }
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        if (animation == rotateDegreesLeft90) {
            solutionTextView.startAnimation(slideInFromRight);
            translationTextView.startAnimation(slideOutFromLeft);
        } else if (animation == slideInFromRight || animation == slideOutFromRight) {
            isAnimationRun = false;
        } else if (animation == exerciseRotateAnimation) {
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            exercicesLayout.setLayoutParams(params);
            exercicesLayout.setVerticalGravity(Gravity.BOTTOM);
            solutionTextView.setDrawingCacheEnabled(false);
            removeHorizontalLayouts();
            setupTitles();
            setupExerciseContent();
        }
    }

    private void updateProgressPercentage() {
        ABATextView unitSectionTitle = (ABATextView) findViewById(R.id.toolbarTitle);
        ABATextView unitSectionProgress = (ABATextView) findViewById(R.id.toolbarSubTitle);

        unitSectionTitle.setText(R.string.unitMenuTitle6Key);
        unitSectionProgress.setText(DataStore.getInstance().getExercisesController().getPercentageForSection(section));
    }

    public void playCorrectAnswer() {

        ABAExercisesTextField tf = blankTextFields.get(0);
        String[] audios = tf.getCorrectAnswer().getAudioFile().split("_");
        correctAnswerPhraseId = tf.getCorrectAnswer().getIdPhrase();

        ABAPhrase audioPhrase = new ABAPhrase();
        audioPhrase.setAudioFile(audios[0]);

        DataStore.getInstance().getAudioController().playPhrase(this, audioPhrase, section.getUnit());
    }

    public void hideCheckButton() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                checkAnswersButton.setVisibility(View.GONE);
            }
        }, 200);
    }

    public void showCheckButton() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                checkAnswersButton.setVisibility(View.VISIBLE);
            }
        }, 250);
    }

    @Override
    public void onAnimationRepeat(Animation animation) {
        // Do nothing
    }

    @Override
    public void startListenPhrase(ABAPhrase phrase) {
        // Do nothing
    }

    @Override
    public void phraseListened() {
        hideAlert();
        updateStatus(ExercisesStatusType.kABAExercisesStatusMustSave);

        // Tracking
        ABAUser currentUser = DataStore.getInstance().getUserController().getCurrentUser(getRealm());
        CooladataStudyTrackingManager.trackListenedToAudio(currentUser.getUserId(), currentUnit.getLevel().getIdLevel(), currentUnit.getIdUnit(), ABAExercises, correctAnswerPhraseId);
        tracker.trackListenedAudio(currentUnit.getLevel().getIdLevel(), currentUnit.getIdUnit(), ABAExercises.toString(), correctAnswerPhraseId);
    }

    @Override
    public void startRecordPhrase(ABAPhrase phrase) {
        //Nothing
    }

    @Override
    public void phraseRecorded() {
        //Nothing
    }

    @Override
    public void startPhraseCompare(ABAPhrase phrase) {
        //Nothing
    }

    @Override
    public void phraseCompared() {
        //Nothing
    }

    @Override
    public void listenError(AudioController.AudioControllerError error) {

        int errorKey = 0;

        switch (error) {
            case kAudioControllerErrorAlreadyPlaying: {
                errorKey = R.string.audioPlayerAlreadyPlayingErrorKey;
                break;
            }
            case kAudioControllerNotEnoughSpaceError: {
                errorKey = R.string.audioPlayerNotEnoughSpaceErrorKey;
                break;
            }
            case kAudioControllerDownloadError: {
                errorKey = R.string.audioPlayerDownloadErrorKey;
                break;
            }
            case kAudioControllerBadAudioFileError: {
                errorKey = R.string.audioPlayerBadAudioFileErrorKey;
                break;
            }
            case kAudioControllerLibraryFailure: {
                errorKey = R.string.audioPlayerBadAudioFileErrorKey;
                break;
            }
        }

        showABAErrorNotification(getResources().getString(errorKey));
    }

    @Override
    public void onPause() {
        super.onPause();
        hideCheckButton();
    }

    // ================================================================================
    // ABAMasterSectionActivity
    // ================================================================================

    protected Object getSection() {
        return section;
    }

    // ================================================================================
    // Private methods - Tracking
    // ================================================================================

    private void trackSection() {
        CooladataNavigationTrackingManager.trackEnteredSection(currentUser.getUserId(), currentUnit.getLevel().getIdLevel(), currentUnit.getIdUnit(), ABAExercises);
        Crashlytics.log(Log.INFO, "Section", "User opens Section Exercises");
        tracker.trackEnteredSection(currentUnit.getLevel().getIdLevel(), currentUnit.getIdUnit(), ABAExercises.toString());
    }
}
