package com.abaenglish.videoclass.presentation.profile;

import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.StringRes;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.abaenglish.videoclass.ABAApplication;
import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.data.network.APIManager;
import com.abaenglish.videoclass.data.persistence.ABAAPIError;
import com.abaenglish.videoclass.data.persistence.ABAUser;
import com.abaenglish.videoclass.domain.content.DataController;
import com.abaenglish.videoclass.domain.content.UserController;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.abaenglish.videoclass.presentation.base.ABAMasterActivity;
import com.abaenglish.videoclass.presentation.base.custom.ABAProgressDialog;
import com.abaenglish.videoclass.session.SessionService;
import com.crashlytics.android.Crashlytics;
import com.jakewharton.rxbinding.widget.RxTextView;

import java.util.concurrent.TimeUnit;

import butterknife.BindColor;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnEditorAction;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.functions.Func3;

/**
 * Created by gnatok on 15/11/16.
 */
public class ProfileChangePasswordActivity extends ABAMasterActivity {
    @BindView(R.id.changePasswordLayout)
    LinearLayout mainLayout;

    @BindView(R.id.oldPasswordEditTextLayout)
    TextInputLayout oldPasswordEditTextLayout;

    @BindView(R.id.oldPasswordEditTextInput)
    TextInputEditText oldPasswordEditTextInput;

    @BindView(R.id.newPasswordEditTextLayout)
    TextInputLayout newPasswordEditTextLayout;

    @BindView(R.id.newPasswordEditTextInput)
    TextInputEditText newPasswordEditTextInput;

    @BindView(R.id.repeatNewPasswordEditTextLayout)
    TextInputLayout repeatNewPasswordEditTextLayout;

    @BindView(R.id.repeatNewPasswordEditTexInput)
    TextInputEditText repeatNewPasswordEditTexInput;

    @BindView(R.id.changePasswordButton)
    Button changePasswordButton;

    @BindView(R.id.toolbarTitle)
    TextView toolbarTitle;

    @BindView(R.id.toolbarSubTitle)
    TextView toolbarSubTitle;

    @BindColor(R.color.changePasswordTextInputColor)
    int changePasswordTextInputColor;

    @BindColor(R.color.recordRedColor)
    int recordRedColor;

    private static String OLD_PASSWORD_EXTRA_KEY = "OLD_PASSWORD_EXTRA_KEY";
    private static String NEW_PASSWORD_EXTRA_KEY = "NEW_PASSWORD_EXTRA_KEY";
    private static String REPEAT_NEW_PASSWORD_EXTRA_KEY = "REPEAT_NEW_PASSWORD_EXTRA_KEY";

    private DataController.ABAAPICallback callback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password_new);
        ButterKnife.bind(this);
        updateRegisterButtonStyle(false);
        configViews();

        oldPasswordEditTextLayout.setErrorEnabled(true);
        oldPasswordEditTextInput.setError(null);
        newPasswordEditTextLayout.setErrorEnabled(true);
        repeatNewPasswordEditTextLayout.setErrorEnabled(true);

        if (savedInstanceState != null) {
            oldPasswordEditTextInput.setText(savedInstanceState.getString(OLD_PASSWORD_EXTRA_KEY));
            newPasswordEditTextInput.setText(savedInstanceState.getString(NEW_PASSWORD_EXTRA_KEY));
            repeatNewPasswordEditTexInput.setText(savedInstanceState.getString(REPEAT_NEW_PASSWORD_EXTRA_KEY));
        }

        configFields();
        Crashlytics.log(Log.INFO, "Change Password", "User opens Change Password View thru Profile");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(OLD_PASSWORD_EXTRA_KEY, oldPasswordEditTextInput.getText().toString());
        outState.putString(NEW_PASSWORD_EXTRA_KEY, newPasswordEditTextInput.getText().toString());
        outState.putString(REPEAT_NEW_PASSWORD_EXTRA_KEY, repeatNewPasswordEditTexInput.getText().toString());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        oldPasswordEditTextInput.setText(savedInstanceState.getString(OLD_PASSWORD_EXTRA_KEY));
        newPasswordEditTextInput.setText(savedInstanceState.getString(NEW_PASSWORD_EXTRA_KEY));
        repeatNewPasswordEditTexInput.setText(savedInstanceState.getString(REPEAT_NEW_PASSWORD_EXTRA_KEY));
        super.onRestoreInstanceState(savedInstanceState);
    }


    private void configViews() {
        oldPasswordEditTextLayout.setTypeface(Typeface.DEFAULT);
        newPasswordEditTextLayout.setTypeface(Typeface.DEFAULT);
        repeatNewPasswordEditTextLayout.setTypeface(Typeface.DEFAULT);
        oldPasswordEditTextInput.setTransformationMethod(new PasswordTransformationMethod());
        newPasswordEditTextInput.setTransformationMethod(new PasswordTransformationMethod());
        repeatNewPasswordEditTexInput.setTransformationMethod(new PasswordTransformationMethod());

        oldPasswordEditTextInput.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        newPasswordEditTextInput.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        repeatNewPasswordEditTexInput.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);

        toolbarTitle.setText(getResources().getString(R.string.menuKey));
        toolbarSubTitle.setText(getResources().getString(R.string.changePasswordTitleKey));
        findViewById(R.id.toolbarLeftButton).setOnTouchListener(((ABAApplication) getApplicationContext()).getImagePressedState(null));
    }

    private void configFields() {
        final Observable<String> oldPasswordSource = streamFromField(oldPasswordEditTextLayout, oldPasswordEditTextInput).skip(1).doOnNext(new Action1<String>() {
            @Override
            public void call(String s) {
                validatePasswordField(oldPasswordEditTextLayout, oldPasswordEditTextInput);

            }
        });

        final Observable<String> newPasswordSource = streamFromField(newPasswordEditTextLayout, newPasswordEditTextInput).skip(1).doOnNext(new Action1<String>() {
            @Override
            public void call(String s) {
                validatePasswordField(newPasswordEditTextLayout, newPasswordEditTextInput);

                if (!repeatNewPasswordEditTexInput.getText().toString().isEmpty()) {
                    validateRepeatPasswordField();
                }

            }
        });

        final Observable<String> repeatNewPasswordSource = streamFromField(repeatNewPasswordEditTextLayout, repeatNewPasswordEditTexInput).skip(1).doOnNext(new Action1<String>() {
            @Override
            public void call(String s) {
                validateRepeatPasswordField();
            }
        });


        Observable.combineLatest(
                oldPasswordSource,
                newPasswordSource,
                repeatNewPasswordSource,
                new Func3<String, String, String, Boolean>() {
                    @Override
                    public Boolean call(String oldPasswordSequence, String newPasswordSequence, String repeatNewPasswordSequence) {
                        Log.d("chasequence", "CharSequence: " + oldPasswordSequence + ", " + newPasswordSequence + ", " + repeatNewPasswordSequence);
                        return isPasswordValid(oldPasswordSequence) && isPasswordValid(newPasswordSequence) && isPasswordValid(repeatNewPasswordSequence) && newPasswordSequence.equals(repeatNewPasswordSequence);
                    }
                })
                .subscribe(new Action1<Boolean>() {
                    @Override
                    public void call(Boolean aBoolean) {
                        updateRegisterButtonStyle(aBoolean);
                    }
                });
    }

    @OnClick(R.id.toolbarLeftButton)
    protected void toolbarLeftButtonClickAction() {
        finishActivity();
    }

    private void finishActivity() {
        finish();
        overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_exit_unitanimation2);
    }

    @OnEditorAction(R.id.repeatNewPasswordEditTexInput)
    protected boolean repeatNewPasswordEditorAction() {
        String oldPasswordString = oldPasswordEditTextInput.getText().toString();
        String newPasswordString = newPasswordEditTextInput.getText().toString();
        String repeatNewPasswordString = repeatNewPasswordEditTexInput.getText().toString();

        if (!isPasswordValid(oldPasswordString)) {
            hightLightErrorField(oldPasswordEditTextLayout, R.string.regErrorPasswordMin);
            return false;
        }

        if (!isPasswordValid(newPasswordString)) {
            hightLightErrorField(newPasswordEditTextLayout, R.string.regErrorPasswordMin);
            return false;
        }

        if (!isPasswordValid(repeatNewPasswordString)) {
            hightLightErrorField(repeatNewPasswordEditTextLayout, R.string.regErrorPasswordMin);
            return false;
        }

        if (!newPasswordString.equals(repeatNewPasswordString)) {
            hightLightErrorField(repeatNewPasswordEditTextLayout, R.string.changePassErrorEqualPassword);
            return false;
        }

        checkOldPassword();
        return true;
    }


    @OnClick(R.id.changePasswordButton)
    protected void changePasswordAction() {
        checkOldPassword();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishActivity();
    }

    private void changePassword() {
        mSessionService.changePassword(newPasswordEditTextInput.getText().toString(), new DataController.ABAAPICallback<Boolean>() {
            @Override
            public void onSuccess(Boolean response) {
                dismissProgressDialog();
                finishActivity();
            }

            @Override
            public void onError(ABAAPIError e) {
                dismissProgressDialog();
                showABAErrorNotification(e.toString());
            }
        });
    }

    private void checkOldPassword() {
        showProgressDialog(ABAProgressDialog.SimpleDialog);
        ABAUser abaUser = DataStore.getInstance().getUserController().getCurrentUser(getRealm());

        mSessionService.checkEmailAndPassword(abaUser.getEmail(), oldPasswordEditTextInput.getText().toString(), new DataController.ABAAPICallback<Boolean>() {
            @Override
            public void onSuccess(Boolean response) {
                if (response) {
                    changePassword();
                } else {
                    dismissProgressDialog();
                    showABAErrorNotification(getString(R.string.changePassErrorOldPassword));
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            oldPasswordEditTextInput.setSelected(true);
                        }
                    }, 150);
                    focusFieldAfterError(oldPasswordEditTextInput);
                }
            }

            @Override
            public void onError(ABAAPIError error) {
                dismissProgressDialog();
                if (error != null) {
                    error.showABANotificationError(ProfileChangePasswordActivity.this);
                }
            }
        });
    }


    // ================================================================================
    // Private methods - RX
    // ================================================================================

    private Observable<String> streamFromField(final TextInputLayout layout, TextInputEditText editText) {
        return RxTextView
                .textChanges(editText)
                .map(new Func1<CharSequence, String>() {
                    @Override
                    public String call(CharSequence charSequence) {
                        return charSequence.toString();
                    }
                })
                .doOnNext(new Action1<String>() {
                    @Override
                    public void call(String charSequence) {
                        Log.d("field validation", "validate streamFromField - setError null");
                        layout.setError(null);
                    }
                })
                .debounce(500, TimeUnit.MILLISECONDS)
                .observeOn(AndroidSchedulers.mainThread());
    }


    private void updateRegisterButtonStyle(boolean isActive) {
        if (isActive) {
            changePasswordButton.setBackgroundResource(R.drawable.tutorial_empieza_selector);
            changePasswordButton.setTextColor(ContextCompat.getColor(this, R.color.abaWhite));
            changePasswordButton.setPressed(true);  // Gypsy king: Montoya
            changePasswordButton.setPressed(false); // Gypsy king: Heredia
            changePasswordButton.setEnabled(true);
        } else {
            changePasswordButton.setBackgroundResource(R.drawable.register_form_disabled_shape);
            changePasswordButton.setTextColor(ContextCompat.getColor(this, R.color.abaDisabledGrey2));
            changePasswordButton.setEnabled(false);
        }
    }

    private boolean isPasswordValid(String string) {
        return !string.isEmpty() && UserController.isPasswordValid(string);
    }

    private void validatePasswordField(TextInputLayout layout, TextInputEditText input) {
        if (!isPasswordValid(input.getText().toString())) {
            hightLightErrorField(layout, R.string.regErrorPasswordMin);
        } else {
            layout.setError(null);
            input.setTextColor(changePasswordTextInputColor);
        }
    }

    private void validateRepeatPasswordField() {
        if (!repeatNewPasswordEditTexInput.getText().toString().equals(newPasswordEditTextInput.getText().toString())) {
            hightLightErrorField(repeatNewPasswordEditTextLayout, R.string.changePassErrorEqualPassword);
        } else {
            repeatNewPasswordEditTextLayout.setError(null);
        }
    }

    private void hightLightErrorField(TextInputLayout layout, @StringRes int message) {
        layout.setError(getString(message));
    }
}
