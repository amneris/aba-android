package com.abaenglish.videoclass.presentation.level;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;

import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.presentation.base.ABAMasterActivity;

/**
 * Created by madalin on 22/04/15.
 */
public class SelectLevelAcivity extends ABAMasterActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        DrawerLayout mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        changeFragment(new LevelSelectionFragment());
    }

    private void changeFragment(LevelSelectionFragment levelSelectionFragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_fragment, levelSelectionFragment, "fragment")
                .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .commit();
    }
}
