package com.abaenglish.videoclass.presentation.section.interpret;

import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;

import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.analytics.cooladata.study.CooladataStudyTrackingManager;
import com.abaenglish.videoclass.data.persistence.ABAInterpret;
import com.abaenglish.videoclass.data.persistence.ABAInterpretRole;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.data.persistence.ABAUser;
import com.abaenglish.videoclass.domain.content.UserController;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.abaenglish.videoclass.presentation.base.ABAMasterActivity;
import com.abaenglish.videoclass.presentation.base.custom.ABATextView;
import com.abaenglish.videoclass.presentation.base.custom.FontCache;
import com.abaenglish.videoclass.presentation.section.SectionActivityInterface;
import com.bzutils.images.RoundedImageView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import static com.abaenglish.videoclass.analytics.cooladata.CooladataTrackingDefinitions.CooladataSectionType.ABAInterpret;


/**
 * Created by madalin on 27/05/15.
 * Small refactor by Jesus on 25/04/2016. Adding Cooladata event.
 */
public class CharacterAdapter extends BaseAdapter implements ListAdapter, View.OnClickListener {
    private ABAMasterActivity mMasterActivity;
    private LayoutInflater mLayoutInflater;
    private CharacterSelectionListener characterSelectionListener;
    private ABAInterpret abaInterpret;
    private ABAUnit currentUnit;

    private FontCache fontCache;

    interface CharacterSelectionListener {
        void onOptionSelected(Intent intent);
    }

    public CharacterAdapter(ABAMasterActivity activity, CharacterSelectionListener characterSelectionListener, ABAUnit unit, ABAInterpret interpretSection, FontCache fontCache) {
        mMasterActivity = activity;
        mLayoutInflater = LayoutInflater.from(activity);
        this.characterSelectionListener = characterSelectionListener;
        this.abaInterpret = interpretSection;
        this.currentUnit = unit;
        this.fontCache = fontCache;
    }

    @Override
    public int getCount() {
        return abaInterpret.getRoles().size();
    }

    @Override
    public Object getItem(int position) {
        return abaInterpret.getRoles().get(position);
    }

    @Override
    public long getItemId(int position) {
        return (long) position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ItemViewHolder mItemViewHolder;
        View row = convertView;
        ABAInterpretRole role = (ABAInterpretRole) getItem(position);

        if (row == null) {
            row = mLayoutInflater.inflate(R.layout.item_interpreation_character, parent, false);
        }

        mItemViewHolder = new ItemViewHolder();
        mItemViewHolder.retryLayoutView = (LinearLayout) row.findViewById(R.id.leftView);
        mItemViewHolder.listenLayoutView = (LinearLayout) row.findViewById(R.id.rightView);
        mItemViewHolder.retryButton = (ImageView) row.findViewById(R.id.interpretRetryButton);
        mItemViewHolder.listenButton = (ImageView) row.findViewById(R.id.interpretListenButton);
        mItemViewHolder.doneIcon = (ImageView) row.findViewById(R.id.doneIcon);
        mItemViewHolder.iconCharacter = (RoundedImageView) row.findViewById(R.id.iconCharacter);
        mItemViewHolder.nameCharacter = (ABATextView) row.findViewById(R.id.nameCharacter);
        mItemViewHolder.characterCell = (RelativeLayout) row.findViewById(R.id.CharacterCell);
        mItemViewHolder.nameCharacter.setText(role.getName());
        mItemViewHolder.nameCharacter.setTypeface(fontCache.getTypeface(FontCache.ABATypeface.sans500));
        ((ABATextView) row.findViewById(R.id.retryLabel)).setTypeface(fontCache.getTypeface(FontCache.ABATypeface.sans500));
        ((ABATextView) row.findViewById(R.id.listenLabel)).setTypeface(fontCache.getTypeface(FontCache.ABATypeface.sans500));

        mItemViewHolder.characterCell.setOnClickListener(this);
        mItemViewHolder.listenButton.setOnClickListener(this);
        mItemViewHolder.retryButton.setOnClickListener(this);

        mItemViewHolder.characterCell.setTag(position);
        mItemViewHolder.retryButton.setTag(position);
        mItemViewHolder.listenButton.setTag(position);

        if (abaInterpret.getRoles().get(position).isCompleted()) {
            mItemViewHolder.listenLayoutView.setVisibility(View.VISIBLE);
            mItemViewHolder.retryLayoutView.setVisibility(View.VISIBLE);
            mItemViewHolder.doneIcon.setVisibility(View.VISIBLE);
        } else {
            mItemViewHolder.listenLayoutView.setVisibility(View.GONE);
            mItemViewHolder.retryLayoutView.setVisibility(View.GONE);
            mItemViewHolder.doneIcon.setVisibility(View.GONE);
        }

        final View rowFinal = row;
        final RoundedImageView roleImage = (RoundedImageView) rowFinal.findViewById(R.id.iconCharacter);

        if (DataStore.getInstance().getLevelUnitController().checkIfFileExist(currentUnit, role.getImageUrl())) {
            DataStore.getInstance().getLevelUnitController().displayImage(currentUnit, role.getImageUrl(), roleImage);
        } else {
            ImageLoader imageLoader = ImageLoader.getInstance(); // Get singleton instance

            imageLoader.loadImage(role.getImageUrl(), new SimpleImageLoadingListener() {
                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    roleImage.setImageBitmap(loadedImage);
                }
            });
        }

        return row;
    }

    @Override
    public void onClick(View v) {
        ABAUser currentUser = UserController.getInstance().getCurrentUser(mMasterActivity.getRealm());
        Intent intent = new Intent(mMasterActivity, InterpretaDialogActivity.class);
        intent.putExtra(SectionActivityInterface.EXTRA_UNIT_ID, abaInterpret.getUnit().getIdUnit());
        intent.putExtra(InterpretaDialogActivity.ROLE_ID, (int) v.getTag());

        switch (v.getId()) {
            case R.id.CharacterCell: {
                if (!abaInterpret.getRoles().get((Integer) v.getTag()).isCompleted()) {
                    // Tracking Cooladata
                    CooladataStudyTrackingManager.trackInterpretationStarted(currentUser.getUserId(), currentUnit.getLevel().getIdLevel(), currentUnit.getIdUnit(), ABAInterpret);

                    // Action
                    characterSelectionListener.onOptionSelected(intent);
                }
                break;
            }
            case R.id.interpretRetryButton: {
                // Tracking Cooladata
                CooladataStudyTrackingManager.trackInterpretationStarted(currentUser.getUserId(), currentUnit.getLevel().getIdLevel(), currentUnit.getIdUnit(), ABAInterpret);
                // Action
                characterSelectionListener.onOptionSelected(intent);
                break;
            }
            case R.id.interpretListenButton: {
                intent.putExtra(InterpretaDialogActivity.LISTEN_MODE, true);
                characterSelectionListener.onOptionSelected(intent);
                break;
            }
        }
    }

    private class ItemViewHolder {
        private LinearLayout retryLayoutView;
        private LinearLayout listenLayoutView;
        private ImageView retryButton;
        private ImageView listenButton;
        private ImageView doneIcon;
        private RoundedImageView iconCharacter;
        private ABATextView nameCharacter;
        private RelativeLayout characterCell;
    }
}
