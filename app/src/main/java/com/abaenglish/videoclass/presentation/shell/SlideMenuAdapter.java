package com.abaenglish.videoclass.presentation.shell;

import android.content.Context;
import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.presentation.abaMoment.customViews.BadgeNotificationTextView;
import com.abaenglish.videoclass.presentation.base.custom.ABATextView;

import java.util.ArrayList;

/**
 * Created by vmadalin on 27/1/16.
 */
class SlideMenuAdapter extends BaseAdapter {

    private Context mContext;
    private ArrayList<SlideMenuItem> menuItemsList;
    private LayoutInflater mLayoutInflater;
    private SlideMenuItem menuItemSelected;

    SlideMenuAdapter(Context context, ArrayList<SlideMenuItem> menuItemsList) {
        this.mContext = context;
        this.menuItemsList = menuItemsList;
        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        if (menuItemsList != null)
            return menuItemsList.size();
        return 0;
    }

    @Override
    public Object getItem(int position) {
        if (menuItemsList != null)
            return menuItemsList.get(position);
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ItemViewHolder mItemViewHolder;

        if (convertView == null) {
            mItemViewHolder = new ItemViewHolder();
            convertView = mLayoutInflater.inflate(R.layout.item_slidemenu, parent, false);
            mItemViewHolder.menuItemImage = (ImageView) convertView.findViewById(R.id.menu_itemIcon);
            mItemViewHolder.menuItemTitle = (ABATextView) convertView.findViewById(R.id.menu_itemTitle);
            mItemViewHolder.menuItemContent = (LinearLayout) convertView.findViewById(R.id.menu_itemContent);
            mItemViewHolder.menuItemSeparator = (ImageView) convertView.findViewById(R.id.menuItemSeparator);
            mItemViewHolder.badgeNotificationTextView = (BadgeNotificationTextView) convertView.findViewById(R.id.menu_itemBadge);
            convertView.setTag(mItemViewHolder);
        } else {
            mItemViewHolder = (ItemViewHolder) convertView.getTag();
        }

        SlideMenuItem slideMenuItem = (SlideMenuItem) getItem(position);

        mItemViewHolder.menuItemImage.setImageResource(slideMenuItem.getMenuItemIconID());
        mItemViewHolder.menuItemTitle.setText(slideMenuItem.getMenuItemTitle());

        //badge notification for AbaMoments
        boolean condition = (slideMenuItem.getMenuType() == MenuType.menuABAMoment && MenuActivity.BADGE_NUMBER > 0);
        int visibility = condition ? View.VISIBLE : View.INVISIBLE;
        mItemViewHolder.badgeNotificationTextView.setVisibility(visibility);

        if (slideMenuItem.equals(menuItemSelected)) {
            mItemViewHolder.menuItemTitle.setTextColor(ContextCompat.getColor(mContext, R.color.abaBackgroundGrey));
            mItemViewHolder.menuItemImage.setColorFilter(ContextCompat.getColor(mContext, R.color.abaBackgroundGrey), PorterDuff.Mode.MULTIPLY);
            mItemViewHolder.menuItemContent.setBackgroundResource(R.color.abaGrey);
            mItemViewHolder.menuItemSeparator.setVisibility(View.INVISIBLE);
        } else {
            mItemViewHolder.menuItemTitle.setTextColor(ContextCompat.getColor(mContext, R.color.abaGrey));
            mItemViewHolder.menuItemImage.setColorFilter(ContextCompat.getColor(mContext, R.color.abaGrey), PorterDuff.Mode.MULTIPLY);
            mItemViewHolder.menuItemContent.setBackgroundResource(android.R.color.transparent);
            if (menuItemSelected == null) {
                mItemViewHolder.menuItemSeparator.setVisibility(View.VISIBLE);
            } else {
                if (menuItemsList.indexOf(menuItemSelected) != 0 && slideMenuItem.equals(menuItemsList.get(menuItemsList.indexOf(menuItemSelected) - 1))) {
                    mItemViewHolder.menuItemSeparator.setVisibility(View.INVISIBLE);
                } else {
                    mItemViewHolder.menuItemSeparator.setVisibility(View.VISIBLE);
                }
            }
        }

        return convertView;
    }

    private class ItemViewHolder {
        private ImageView menuItemImage;
        private ABATextView menuItemTitle;
        private LinearLayout menuItemContent;
        private ImageView menuItemSeparator;
        private BadgeNotificationTextView badgeNotificationTextView;
    }

    void setMenuItemSelected(SlideMenuItem menuItemSelected) {
        this.menuItemSelected = menuItemSelected;
    }

    // ================================================================================
    // Public class - for menu items
    // ================================================================================

    static class SlideMenuItem {
        private int menuItemIconID;
        private String menuItemTitle;
        private MenuType menuType;

        SlideMenuItem(Context context, int iconId, int resId, MenuType menuType) {
            setMenuItemIconID(iconId);
            setMenuItemTitle(context.getString(resId).toUpperCase());
            setMenuType(menuType);
        }

        int getMenuItemIconID() {
            return menuItemIconID;
        }

        void setMenuItemIconID(int menuItemIconID) {
            this.menuItemIconID = menuItemIconID;
        }

        String getMenuItemTitle() {
            return menuItemTitle;
        }

        void setMenuItemTitle(String menuItemTitle) {
            this.menuItemTitle = menuItemTitle;
        }

        MenuType getMenuType() {
            return menuType;
        }

        void setMenuType(MenuType menuType) {
            this.menuType = menuType;
        }
    }
}
