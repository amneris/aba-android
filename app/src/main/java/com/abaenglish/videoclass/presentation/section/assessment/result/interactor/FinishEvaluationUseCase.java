package com.abaenglish.videoclass.presentation.section.assessment.result.interactor;

import com.abaenglish.videoclass.clean.interactor.SingleShotUseCase;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.domain.content.EvaluationController;
import com.abaenglish.videoclass.domain.content.LevelUnitController;

import java.util.concurrent.Callable;

import javax.inject.Inject;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import rx.Single;

/**
 * Created by xabierlosada on 26/07/16.
 */

public class FinishEvaluationUseCase implements SingleShotUseCase<AssessmentResult, Void> {

    private EvaluationController evaluationController;
    private RealmConfiguration realmConfiguration;
    private LevelUnitController levelUnitController;

    @Inject
    public FinishEvaluationUseCase(
            RealmConfiguration realmConfiguration,
            LevelUnitController levelUnitController,
            EvaluationController evaluationController) {
        this.evaluationController = evaluationController;
        this.realmConfiguration = realmConfiguration;
        this.levelUnitController = levelUnitController;
    }

    @Override
    public Single<Void> build(final AssessmentResult assessmentResult) {
        return Single.fromCallable(new Callable<Void>() {
            @Override
            public Void call() throws Exception {
                Realm realm = Realm.getInstance(realmConfiguration);
                ABAUnit unit = levelUnitController.getUnitWithId(realm, assessmentResult.unitName());
                if(assessmentResult.grade().equals(AssessmentResult.Grade.FAIL)){
                    evaluationController.endEvaluationWithKO(realm, unit.getSectionEvaluation());
                } else {
                    evaluationController.endEvaluationWithOK(realm, unit.getSectionEvaluation());
                }
                evaluationController.resetEvaluationAnswers(realm, unit.getSectionEvaluation());
                realm.close();
                return null;
                }
            });
    }
}
