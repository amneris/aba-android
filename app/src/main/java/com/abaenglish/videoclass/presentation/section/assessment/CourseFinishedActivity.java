package com.abaenglish.videoclass.presentation.section.assessment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.presentation.base.custom.ABATextView;
import com.bzutils.images.RoundedImageView;
import com.crashlytics.android.Crashlytics;
import com.nostra13.universalimageloader.core.ImageLoader;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by xabierlosada on 20/07/16.
 */

public class CourseFinishedActivity extends AppCompatActivity {

    public static final String EXTRA_USERNAME = "username";
    public static final String EXTRA_TEACHER_IMAGE = "teacher_image";
    private static final String FOLLOW_TWITTER_URL = "https://twitter.com/intent/follow?user_id=abaenglish&screen_name=abaenglish";
    private static final String FOLLOW_FACEBOOK_URL = "https://www.facebook.com/ABAEnglish.es/?fref=ts&ref=br_tf";
    private static final String FOLLOW_LINKEDIN_URL = "https://www.linkedin.com/company/aba-english";

    @BindView(R.id.textViewCongrats) ABATextView congratsTextView;
    @BindView(R.id.imageViewTeacher) RoundedImageView teacherImageView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course_finished);
        ButterKnife.bind(this);

        String username = getIntent().getExtras().getString(EXTRA_USERNAME);
        String teacherImageUrl = getIntent().getExtras().getString(EXTRA_TEACHER_IMAGE);

        configViews(username, teacherImageUrl);
        Crashlytics.log(Log.INFO, "Section", "User opens Curse Finished View");
    }

    private void configViews(String username, String teacherImageUrl) {
        String congratulationsMessage = getString(R.string.course_completed_well_done).replace("%@", username);
        congratsTextView.setText(congratulationsMessage);
        ImageLoader.getInstance().displayImage(teacherImageUrl, teacherImageView);
    }

    @OnClick(R.id.twitterFab)
    public void followUsOnTwitter() {
        launchFollowIntent(FOLLOW_TWITTER_URL);
    }

    @OnClick(R.id.facebookFab)
    public void followUsOnFacebook() {
        launchFollowIntent(FOLLOW_FACEBOOK_URL);
    }

    @OnClick(R.id.linkedinFab)
    public void followUsOnLinkedin() {
        launchFollowIntent(FOLLOW_LINKEDIN_URL);
    }

    @OnClick(R.id.closeButton)
    public void close() {
        this.finish();
    }

    private void launchFollowIntent(String url) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(intent);
    }
}

