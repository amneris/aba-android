package com.abaenglish.videoclass.presentation.section;

/*
 * Copyright 2016, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.content.Context;
import android.content.Intent;
import android.support.annotation.IntDef;

import com.abaenglish.videoclass.presentation.section.assessment.ABAEvaluationActivity;
import com.abaenglish.videoclass.presentation.section.exercise.ABAExercisesActivity;
import com.abaenglish.videoclass.presentation.section.interpret.ABAInterpretationActivity;
import com.abaenglish.videoclass.presentation.section.speak.ABASpeakActivity;
import com.abaenglish.videoclass.presentation.section.videoclass.ABAFilmActivity;
import com.abaenglish.videoclass.presentation.section.vocabulary.ABAVocabularyActivity;
import com.abaenglish.videoclass.presentation.section.write.ABAWriteActivity;

public class SectionHelper {

    public final static int SECTION_FILM = 1;
    public final static int SECTION_SPEAK = 2;
    public final static int SECTION_WRITE = 3;
    public final static int SECTION_INTERPRET = 4;
    public final static int SECTION_VIDEO_CLASS = 5;
    public final static int SECTION_EXERCISES = 6;
    public final static int SECTION_VOCABULARY = 7;
    public final static int SECTION_ASSESSMENT = 8;

    @IntDef({SECTION_FILM, SECTION_SPEAK, SECTION_WRITE,
            SECTION_INTERPRET, SECTION_VIDEO_CLASS, SECTION_EXERCISES,
            SECTION_VOCABULARY, SECTION_ASSESSMENT})
    public @interface Section{}

    public static Intent getNextSectionIntent(Context context, int currentUnit, @Section int currentSection) {
        Intent intent;
        switch (currentSection) {
            case SECTION_FILM:
                intent = new Intent(context, ABASpeakActivity.class);
                break;
            case SECTION_SPEAK:
                intent = new Intent(context, ABAWriteActivity.class);
                break;
            case SECTION_WRITE:
                intent = new Intent(context, ABAInterpretationActivity.class);
                break;
            case SECTION_INTERPRET:
                intent = new Intent(context, ABAFilmActivity.class);
                intent.putExtra(ABAFilmActivity.SECTION_ID, ABAFilmActivity.VIDEOCLASS_SECTION);
                break;
            case SECTION_VIDEO_CLASS:
                intent = new Intent(context, ABAExercisesActivity.class);
                break;
            case SECTION_EXERCISES:
                intent = new Intent(context, ABAVocabularyActivity.class);
                break;
            case SECTION_VOCABULARY:
                intent = new Intent(context, ABAEvaluationActivity.class);
                break;
            case SECTION_ASSESSMENT:
                intent = new Intent(context, ABAFilmActivity.class);
                intent.putExtra(ABAFilmActivity.SECTION_ID, ABAFilmActivity.FILM_SECTION);
                intent.putExtra(SectionActivityInterface.EXTRA_UNIT_ID, currentUnit + 1);
                return intent;
            default:
                throw new UnsupportedOperationException();
        }
        intent.putExtra(SectionActivityInterface.EXTRA_UNIT_ID, String.valueOf(currentUnit));
        return intent;
    }
}
