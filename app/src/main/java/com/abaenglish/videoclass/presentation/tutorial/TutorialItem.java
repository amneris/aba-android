package com.abaenglish.videoclass.presentation.tutorial;

/**
 * Created by iaguila on 23/3/15.
 * Refactored by xilosada.
 */
public class TutorialItem {

    private int background;
    private String title;
    private String title2;
    private String subtitle;

    private TutorialItem(int background, String title, String title2, String subtitle) {
        this.background = background;
        this.title = title;
        this.title2 = title2;
        this.subtitle = subtitle;
    }

    public int getBackground() {
        return background;
    }

    public String getTitle() {
        return title;
    }

    public String getTitle2() { return title2;}

    public String getSubtitle() {
        return subtitle;
    }

    public static class Builder {
        private int background;
        private String title;
        private String title2;
        private String subTitle;

        public Builder withBackground(int background) {
            this.background = background;
            return this;
        }

        public Builder withTitle(String title) {
            this.title = title;
            return this;
        }

        public Builder withTitle2(String title2) {
            this.title2 = title2;
            return this;
        }

        public Builder withSubtitle(String subtitle) {
            this.subTitle = subtitle;
            return this;
        }

        public TutorialItem build() {
            return new TutorialItem(background, title, title2, subTitle);
        }
    }
}
