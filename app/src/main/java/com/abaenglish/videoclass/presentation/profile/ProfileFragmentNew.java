package com.abaenglish.videoclass.presentation.profile;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.abaenglish.videoclass.ABAApplication;
import com.abaenglish.videoclass.BuildConfig;
import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.analytics.cooladata.profile.CooladataProfileTrackingManager;
import com.abaenglish.videoclass.analytics.helpers.GenericTrackingInterface;
import com.abaenglish.videoclass.data.DownloadController;
import com.abaenglish.videoclass.data.network.oauth.OAuthTokenAccessor;
import com.abaenglish.videoclass.data.network.retrofit.clients.ABAMomentBadgeClient;
import com.abaenglish.videoclass.data.persistence.ABAAPIError;
import com.abaenglish.videoclass.data.persistence.ABAUser;
import com.abaenglish.videoclass.domain.content.DataController;
import com.abaenglish.videoclass.domain.content.PlanController;
import com.abaenglish.videoclass.domain.content.PreferencesProvider;
import com.abaenglish.videoclass.domain.content.PreferencesProviderImpl;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.abaenglish.videoclass.presentation.base.ABAMasterActivity;
import com.abaenglish.videoclass.presentation.base.custom.ABAProgressDialog;
import com.abaenglish.videoclass.presentation.base.custom.ABATextView;
import com.abaenglish.videoclass.presentation.plan.PlansActivity;
import com.abaenglish.videoclass.presentation.shell.MenuActivity;
import com.abaenglish.videoclass.session.SessionService;
import com.android.vending.billing.IInAppBillingService;
import com.crashlytics.android.Crashlytics;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import javax.inject.Inject;

import butterknife.BindColor;
import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.realm.Realm;

/**
 * Created by gnatok on 07/11/2016.
 */

public class ProfileFragmentNew extends Fragment {

    private Unbinder unbinder;
    private ABAUser currentUser;
    private PreferencesProvider pref;
    private ABATextView toolbarTitle;
    private ABATextView toolbarSubtitle;
    private Realm realm;

    @BindView(R.id.profileScrollView)
    ScrollView profileScrollView;
    @BindView(R.id.profileName)
    TextView profileName;
    @BindView(R.id.profileEmail)
    TextView profileEmail;
    @BindView(R.id.profileLang)
    TextView profileLang;
    @BindView(R.id.profileType)
    TextView profileType;
    @BindView(R.id.subscriptionDateField)
    LinearLayout subscriptionDateLayout;
    @BindView(R.id.subscriptionEndDate)
    TextView getSubscriptionDateTextView;
    @BindView(R.id.changePassword)
    LinearLayout changePasswordBtn;
    @BindView(R.id.notificationSwitch)
    SwitchCompat notificationSwitch;
    @BindView(R.id.restorePurchases)
    LinearLayout restorePurchasesBtn;
    @BindView(R.id.mobileDataSwitch)
    SwitchCompat mobileDataSwitch;
    @BindView(R.id.deleteDownloads)
    LinearLayout deleteDownloadsBtn;
    @BindView(R.id.logOutButton)
    Button logOutButton;
    @BindView(R.id.premiumBanner)
    RelativeLayout premiumBanner;
    @BindView(R.id.premiumButton)
    Button premiumButton;
    @BindView(R.id.versionLabel)
    TextView versionLabel;
    @BindString(R.string.profile_version_text)
    String versionText;
    @BindString(R.string.profile_build_text)
    String buildText;
    @BindString(R.string.menuKey)
    String toolbarTitleText;
    @BindString(R.string.yourProfileKey)
    String toolbarSubtitleText;
    @BindString(R.string.profile_delete_downloads_alert_title)
    String deleteDownloadsAlertTitle;
    @BindString(R.string.profile_delete_downloads_alert_ok_button)
    String deleteDownloadsAlertOkButtonText;
    @BindString(R.string.alertSubscriptionKOButton)
    String deleteDownloadsAlertKOButtonText;
    @BindString(R.string.profileRegister2Key)
    String subscriptionPrefix;
    @BindColor(R.color.abaLightBlue)
    int abaLightBlue;
    @BindColor(R.color.abaWhite)
    int abaWhite;

    @Inject
    protected SessionService mSessionService;

    @Inject
    protected OAuthTokenAccessor tokenAccessor;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ABAApplication.get().getApplicationComponent().inject(this);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile_new, container, false);
        unbinder = ButterKnife.bind(this, view);

        realm = Realm.getInstance(ABAApplication.get().getRealmConfiguration());
        currentUser = DataStore.getInstance().getUserController().getCurrentUser(realm);
        pref = new PreferencesProviderImpl(getABAActivity());

        Crashlytics.log(Log.INFO, "Profile", "User opens Profile Page");

        configureView();
        return view;
    }

    @OnClick(R.id.changePassword)
    public void changePassword() {
        Intent intent = new Intent(getContext(), ProfileChangePasswordActivity.class);
        getActivity().startActivity(intent);
        getActivity().overridePendingTransition(R.anim.slide_enter_unitanimation1, R.anim.slide_enter_unitanimation2);
    }

    @OnClick(R.id.restorePurchases)
    public void restorePurchases() {
        Crashlytics.log(Log.INFO, "Restore Purchases", "User clicks Restore Purchases button");

        getABAActivity().showProgressDialog(ABAProgressDialog.SimpleDialog);
        DataStore.getInstance().getPlanController(getABAActivity().tracker).getBillingServices(getContext(), new DataController.ABAAPICallback<IInAppBillingService>() {
            @Override
            public void onSuccess(IInAppBillingService response) {
                DataStore.getInstance().getPlanController(getABAActivity().tracker).prepareToRestorePurchases(currentUser, realm, getABAActivity(), response, new PlanController.ABAPlanCallback() {
                    @Override
                    public void onResult(PlanController.SubscriptionResult type) {
                        getABAActivity().dismissProgressDialog();
                        getABAActivity().handlePurchaseResult(type);
                    }
                });
            }

            @Override
            public void onError(ABAAPIError error) {
                ((ABAMasterActivity) getContext()).showABAErrorNotification(getString(R.string.errorFetchingSubscriptions));
                getABAActivity().dismissProgressDialog();
            }
        });

    }

    @OnClick(R.id.privacyPolicyButton)
    public void privacyPolicyAction() {
        getActivity().overridePendingTransition(R.anim.slide_enter_unitanimation1, R.anim.slide_enter_unitanimation2);
        getActivity().startActivity(LegalInfoActivity.getPolicyInfoIntent(getActivity()));
    }

    @OnClick(R.id.termOfUseButton)
    public void termOfUseAction() {
        getActivity().overridePendingTransition(R.anim.slide_enter_unitanimation1, R.anim.slide_enter_unitanimation2);
        getActivity().startActivity(LegalInfoActivity.getTOUIntent(getActivity()));
    }

    @OnClick(R.id.logOutButton)
    public void logOutAction() {
        Crashlytics.log(Log.INFO, "Logout", "User clicked in logout button - New profile");
        mSessionService.logout(getActivity(), new DataController.ABALogoutCallback() {
            @Override
            public void onLogoutFinished() {
                // Resetting token so that oauth can be init again
                tokenAccessor.reset();

                if (getActivity() != null) {
                    ABAMomentBadgeClient.reset(getActivity());
                    if (!getActivity().isFinishing()) {
                        MenuActivity.resetFirstSessionMenuPerformance(ABAApplication.get());
                        getABAActivity().openTutorial();
                    }

                }
            }
        });
    }

    @OnClick(R.id.deleteDownloads)
    public void deleteDownloadsAction() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.material_alert_dialog);
        builder.setMessage(deleteDownloadsAlertTitle);
        builder.setPositiveButton(deleteDownloadsAlertOkButtonText.toUpperCase(), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                DownloadController.removeDownloadedFiles();
                CooladataProfileTrackingManager.trackDeleteDownloads(currentUser.getUserId());
                Crashlytics.log(Log.INFO, "Profile", "User delete files");
            }
        });

        builder.setCancelable(false);
        builder.setNegativeButton(deleteDownloadsAlertKOButtonText, null);
        builder.show();
    }

    @OnClick(R.id.premiumButton)
    public void premiumButtonAction() {
        getABAActivity().trackPricingScreenWithOrigin(GenericTrackingInterface.PlansViewControllerOriginType.kPlansViewControllerOriginBanner);
        getActivity().startActivity(new Intent(getActivity(), PlansActivity.class));
        getActivity().overridePendingTransition(R.anim.slide_enter_unitanimation1, R.anim.slide_enter_unitanimation2);
        CooladataProfileTrackingManager.trackOpenPricesFromBanner(currentUser.getUserId());
    }

    @OnCheckedChanged(R.id.mobileDataSwitch)
    public void mobileDataSwitchAction(boolean checked) {
        pref.setCarrierDownloadAllowance(checked);
    }

    @OnCheckedChanged(R.id.notificationSwitch)
    public void notificationSwitchAction(boolean checked) {
        pref.setPushNotificationAllowance(checked);
        CooladataProfileTrackingManager.trackNotificationSwitchChange(currentUser.getUserId(), checked);
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        super.onDestroyView();

        if (realm != null) {
            realm.close();
        }
    }

    // Private methods
    private void configureView() {
        configureToolbar();
        configureSwitches();
        configureVersionName();
        setProfileData();
    }

    private void configureVersionName() {
        versionLabel.setText(versionText + " " + BuildConfig.VERSION_NAME + " " + buildText + " " + BuildConfig.VERSION_CODE);
    }

    private void setProfileData() {
        profileName.setText(currentUser.getName() + " " + currentUser.getSurnames());
        profileEmail.setText(currentUser.getEmail());
        setUserLang(currentUser.getUserLang());

        if (DataStore.getInstance().getUserController().isUserPremium()) {
            profileType.setText("Premium");
            premiumBanner.setVisibility(View.GONE);
            profileScrollView.setPadding(0, 0, 0, 0);
            subscriptionDateLayout.setVisibility(View.VISIBLE);
            DateFormat format = DateFormat.getDateInstance(DateFormat.LONG, new Locale(currentUser.getUserLang()));
            String dateString = format.format(new Date(currentUser.getExpirationDate().getTime()));
            getSubscriptionDateTextView.setText(subscriptionPrefix + " " + dateString);
        } else {
            profileType.setText("Free");
            premiumBanner.setVisibility(View.VISIBLE);
        }
    }

    private void configureToolbar() {
        toolbarTitle = (ABATextView) getActivity().findViewById(R.id.toolbarTitle);
        toolbarSubtitle = (ABATextView) getActivity().findViewById(R.id.toolbarSubTitle);
        toolbarTitle.setTextColor(abaLightBlue);
        toolbarSubtitle.setTextColor(abaWhite);
        toolbarTitle.setVisibility(View.VISIBLE);
        toolbarSubtitle.setVisibility(View.VISIBLE);
        toolbarTitle.setText(toolbarTitleText);
        toolbarSubtitle.setText(toolbarSubtitleText);
    }

    private void setUserLang(String language) {
        if (language.equals("es")) {
            profileLang.setText(getResources().getString(R.string.languageSpainKey));
        } else if (language.equals("en")) {
            profileLang.setText(getResources().getString(R.string.languageEnglishKey));
        } else if (language.equals("it")) {
            profileLang.setText(getResources().getString(R.string.languageItalianKey));
        } else if (language.equals("de")) {
            profileLang.setText(getResources().getString(R.string.languageDeutchKey));
        } else if (language.equals("fr")) {
            profileLang.setText(getResources().getString(R.string.languageFrenchKey));
        } else if (language.equals("ru")) {
            profileLang.setText(getResources().getString(R.string.languageRussianKey));
        } else if (language.equals("pt")) {
            profileLang.setText(getResources().getString(R.string.languagePortugueseKey));
        }
    }

    private void configureSwitches() {
        notificationSwitch.setChecked(pref.isPushNotificationAllowed());
        mobileDataSwitch.setChecked(pref.isCarrierDataDownloadAllowed());
    }

    private ABAMasterActivity getABAActivity() {
        return (ABAMasterActivity) getActivity();
    }
}
