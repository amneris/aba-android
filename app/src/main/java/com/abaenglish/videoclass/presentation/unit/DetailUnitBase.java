package com.abaenglish.videoclass.presentation.unit;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.view.View;

import com.abaenglish.videoclass.ABAApplication;
import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.analytics.modern.MixpanelTrackingManager;
import com.abaenglish.videoclass.data.ConnectionService;
import com.abaenglish.videoclass.data.DownloadController;
import com.abaenglish.videoclass.data.persistence.ABAAPIError;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.domain.content.DataController;
import com.abaenglish.videoclass.domain.content.PreferencesProvider;
import com.abaenglish.videoclass.domain.content.PreferencesProviderImpl;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.abaenglish.videoclass.presentation.base.ABAMasterActivity;
import com.abaenglish.videoclass.presentation.base.custom.ABAProgressDialog;
import com.abaenglish.videoclass.presentation.base.custom.ABATextView;
import com.abaenglish.videoclass.presentation.section.SectionsService;
import com.abaenglish.videoclass.presentation.shell.MenuActivity;
import com.bzutils.LogBZ;

import java.util.ArrayList;

import butterknife.BindString;
import io.realm.Realm;

import static com.abaenglish.videoclass.analytics.helpers.GenericTrackingInterface.DownloadDialogResultType.kDownloadDialogCancel;
import static com.abaenglish.videoclass.analytics.helpers.GenericTrackingInterface.DownloadDialogResultType.kDownloadDialogProceed;
import static com.abaenglish.videoclass.analytics.helpers.GenericTrackingInterface.DownloadDialogType.kDownloadDialogStorage;
import static com.abaenglish.videoclass.analytics.helpers.GenericTrackingInterface.DownloadDialogType.kDownloadDialogWifi;

/**
 * Created by Jesus Espejo using mbp13jesus on 08/11/16.
 * Base class with common functionality in Unit detail
 */

public abstract class DetailUnitBase extends ABAMasterActivity {

    // Inner behaviour
    protected boolean isUnitDownloaded;
    public static boolean isDownloadStarted = false;

    // Domain
    public String unitId;
    public String levelId;
    private PreferencesProvider pref;

    // String bindings
    @BindString(R.string.offline_dialog_download)
    protected String dialogDownloadButtonText;
    @BindString(R.string.offline_dialog_delete)
    protected String dialogDeleteButtonText;
    @BindString(R.string.offline_dialog_cancel)
    protected String dialogCancelButtonText;
    @BindString(R.string.offline_dialog_no_wifi_network_title)
    protected String dialogNoWifiTitleText;
    @BindString(R.string.offline_dialog_no_wifi_network_subtitle)
    protected String dialogNoWifiSubtitleText;
    @BindString(R.string.offline_dialog_storage_permission)
    protected String dialogStoragePermissionText;
    @BindString(R.string.dialog_carrier_download_disabled_text)
    protected String dialogCarrierDownloadDisabledText;
    @BindString(R.string.dialog_go_to_profile_button_text)
    protected String dialogGoToProfileText;
    @BindString(R.string.alertSubscriptionOkButton)
    protected String okButtonText;
    @BindString(R.string.offline_dialog_delete_downloaded_unit_title)
    protected String dialogDeleteUnitTitleText;
    @BindString(R.string.offline_dialog_delete_downloaded_unit_subtitle)
    protected String dialogDeleteUnitSubtitleText;

    // UI
    protected ArrayList<String> totalElementsForDownload;

    // ================================================================================
    // Abstract methods
    // ================================================================================

    protected abstract void refreshScreen();

    // ================================================================================
    // Lifecycle
    // ================================================================================


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pref = new PreferencesProviderImpl(this);
    }

    protected void finishUnit() {
        finish();
        overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_exit_unitanimation2);
    }

    // ================================================================================
    // Configuration
    // ================================================================================

    protected void configActionBar() {
        findViewById(R.id.toolbarLeftButton).setOnTouchListener(((ABAApplication) getApplicationContext()).getImagePressedState(null));
        findViewById(R.id.toolbarLeftButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishUnit();
            }
        });

        if (!isUnitDownloaded && DataStore.getInstance().getUserController().isUserPremium()) {
            findViewById(R.id.toolbarRightButton).setBackgroundResource(R.mipmap.download);
            findViewById(R.id.toolbarRightButton).setVisibility(View.VISIBLE);
        } else if (isUnitDownloaded && DataStore.getInstance().getUserController().isUserPremium()) {
            findViewById(R.id.toolbarRightButton).setBackgroundResource(R.mipmap.downloaded);
            findViewById(R.id.toolbarRightButton).setVisibility(View.VISIBLE);
        }

        findViewById(R.id.toolbarRightButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadButtonAction();
            }
        });

        ABATextView unitNumber = (ABATextView) findViewById(R.id.toolbarTitle);
        ABATextView unitTitle = (ABATextView) findViewById(R.id.toolbarSubTitle);

        ABAUnit currentUnit = DataStore.getInstance().getLevelUnitController().getUnitWithId(getRealm(), unitId);
        unitNumber.setText(getString(R.string.unitKey) + " " + currentUnit.getIdUnit());
        unitTitle.setText(currentUnit.getTitle() + " " + (int) currentUnit.getProgress() + "%");
    }


    // ================================================================================
    // Download related methods
    // ================================================================================

    protected void downloadButtonAction() {
        // Checking permissions
        if (checkPermissionStorage()) {

            // If unit can be downloaded
            if (!isUnitDownloaded) {
                downloadFunnel();
            } else {
                removeDownloadFunnel();
            }
        }

    }

    protected void downloadFunnel() {

        ABAUnit currentUnit = DataStore.getInstance().getLevelUnitController().getUnitWithId(getRealm(), unitId);
        // Unit info is completely parsed
        if (currentUnit.isDataDownloaded()) {

            // Connection is available
            if (ConnectionService.isNetworkAvailable(getApplicationContext())) {
                if (!ConnectionService.isWifiAvaliable(getApplicationContext()) && !pref.isCarrierDataDownloadAllowed()) {
                    showCarrierDownloadDisabledAlert();
                } else {
                    askForStorage();
                }
            } else {
                // No connection
                LogBZ.e("Sin conexion");
                showABAErrorNotification(getString(R.string.errorConnection));
            }

        } else {
            LogBZ.e("Parse Unit Error");
            showABAErrorNotification(getString(R.string.getAllSectionsForUnitErrorKey));
        }
    }

    protected void askForStorage() {
        // Creating alert to ask for user confirmation
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.material_alert_dialog);
        builder.setMessage(dialogStoragePermissionText);
        builder.setPositiveButton(dialogDownloadButtonText.toUpperCase(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                tracker.trackDownloadDialog(levelId, unitId, kDownloadDialogStorage, kDownloadDialogProceed);
                MixpanelTrackingManager.getSharedInstance(DetailUnitBase.this).trackDownloadDialog(unitId, kDownloadDialogStorage, kDownloadDialogProceed);
                askForCarrierDownloadIfNeeded();
            }
        });
        builder.setCancelable(false);
        builder.setNegativeButton(dialogCancelButtonText.toUpperCase(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                tracker.trackDownloadDialog(levelId, unitId, kDownloadDialogStorage, kDownloadDialogCancel);
                MixpanelTrackingManager.getSharedInstance(DetailUnitBase.this).trackDownloadDialog(unitId, kDownloadDialogStorage, kDownloadDialogCancel);
            }
        });
        builder.show();
    }

    protected void askForCarrierDownloadIfNeeded() {
        /// Creating alert to ask for user confirmation/ Creating alert to ask for user confirmation
        if (ConnectionService.isWifiAvaliable(getApplicationContext())) {
            startDownloadFilesFromSection();
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.material_alert_dialog);
            builder.setTitle(Html.fromHtml("<b>" + dialogNoWifiTitleText + "</b>"));
            builder.setMessage(dialogNoWifiSubtitleText);
            builder.setPositiveButton(dialogDownloadButtonText.toUpperCase(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    tracker.trackDownloadDialog(levelId, unitId, kDownloadDialogWifi, kDownloadDialogProceed);
                    MixpanelTrackingManager.getSharedInstance(DetailUnitBase.this).trackDownloadDialog(unitId, kDownloadDialogWifi, kDownloadDialogProceed);
                    startDownloadFilesFromSection();
                }
            });
            builder.setCancelable(false);
            builder.setNegativeButton(dialogCancelButtonText.toUpperCase(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    tracker.trackDownloadDialog(levelId, unitId, kDownloadDialogWifi, kDownloadDialogCancel);
                    MixpanelTrackingManager.getSharedInstance(DetailUnitBase.this).trackDownloadDialog(unitId, kDownloadDialogWifi, kDownloadDialogCancel);
                }
            });
            builder.show();
        }
    }

    protected void removeDownloadFunnel() {

        // Creating alert to ask for user confirmation
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.material_alert_dialog);
        builder.setTitle(dialogDeleteUnitTitleText);
        builder.setMessage(dialogDeleteUnitSubtitleText);
        builder.setPositiveButton(dialogDeleteButtonText.toUpperCase(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                DataStore.getInstance().getDownloadController().removeDownloadedFilesForUnit(unitId);

                isUnitDownloaded = false;
                isDownloadStarted = false;
                configActionBar();
                refreshScreen();

                tracker.trackDownloadedFilesRemoved(levelId, unitId);
                MixpanelTrackingManager.getSharedInstance(DetailUnitBase.this).trackDownloadedFilesRemoved(unitId);
            }
        });
        builder.setNegativeButton(dialogCancelButtonText.toUpperCase(), null);
        builder.show();
    }

    protected void showCarrierDownloadDisabledAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.material_alert_dialog);
        builder.setMessage(dialogCarrierDownloadDisabledText);
        builder.setNegativeButton(okButtonText.toUpperCase(), null);
        builder.setPositiveButton(dialogGoToProfileText.toUpperCase(), new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // Go to profile
                setResult(MenuActivity.DETAIL_UNIT_GO_TO_PROFILE_RESULT_CODE);
                finish();
            }
        });
        builder.show();
    }

    // ================================================================================
    // Download process
    // ================================================================================

    protected void startDownloadFilesFromSection() {
        isDownloadStarted = true;

        tracker.trackDownloadStarted(levelId, unitId);
        MixpanelTrackingManager.getSharedInstance(DetailUnitBase.this).trackDownloadStarted(unitId);

        if (ConnectionService.isNetworkAvailable(this)) {
            showProgressDialog(ABAProgressDialog.ComplexDialog);
            ABAUnit currentUnit = DataStore.getInstance().getLevelUnitController().getUnitWithId(getRealm(), unitId);
            SectionsService.getInstance().fetchSections(DetailUnitBase.this, currentUnit, new DataController.ABASimpleCallback() {
                @Override
                public void onSuccess() {
                    dismissProgressDialog();

                    Realm instance = Realm.getInstance(ABAApplication.get().getRealmConfiguration());
                    ABAUnit callbackUnit = DataStore.getInstance().getLevelUnitController().getUnitWithId(instance, unitId);

                    DataStore.getInstance().getDownloadController().getDownloadThread().showDownloadDialog(DetailUnitBase.this, callbackUnit.getLevel().getIdLevel(), callbackUnit.getIdUnit());
                    DataStore.getInstance().getDownloadController().downloadSectionContent(getRealm(), getApplicationContext(), callbackUnit, new DownloadController.DownloadCallback() {
                        @Override
                        public void downloadSuccess() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    isUnitDownloaded = true;
                                    isDownloadStarted = false;
                                    configActionBar();
                                    refreshScreen();
                                    tracker.trackDownloadFinished(levelId, unitId);
                                    MixpanelTrackingManager.getSharedInstance(DetailUnitBase.this).trackDownloadFinished(unitId);
                                }
                            });
                        }

                        @Override
                        public void downloadError() {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    isDownloadStarted = false;
                                    showABAErrorNotification(getString(R.string.offlineDownloadErrorKey));
                                }
                            });
                            LogBZ.e("Error de descarga");
                            tracker.trackDownloadError(levelId, unitId);
                            MixpanelTrackingManager.getSharedInstance(DetailUnitBase.this).trackDownloadError(unitId);
                        }
                    });

                    instance.close();
                }

                @Override
                public void onError(ABAAPIError error) {
                    isDownloadStarted = false;
                    dismissProgressDialog();
                    error.showABANotificationError(DetailUnitBase.this);
                    LogBZ.e("Unit Sections API error: " + error.getError());
                    tracker.trackDownloadError(levelId, unitId);
                    MixpanelTrackingManager.getSharedInstance(DetailUnitBase.this).trackDownloadError(unitId);
                }
            });

        } else {
            isDownloadStarted = false;
            showABAErrorNotification(getResources().getString(R.string.getAllSectionsForUnitErrorKey));
            LogBZ.e("Error de descarga , Sin Conexión");
            tracker.trackDownloadError(levelId, unitId);
            MixpanelTrackingManager.getSharedInstance(DetailUnitBase.this).trackDownloadError(unitId);
        }
    }
}
