package com.abaenglish.videoclass.presentation.unit.variation;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.abaenglish.videoclass.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;

import static com.abaenglish.videoclass.R.id.iv_chevron;

class SectionsAdapter extends ArrayAdapter<SectionViewModel> {

    private final ArrayList<SectionViewModel> section;
    private SectionListener sectionListener;
    private Resources res = getContext().getResources();
    private int disabledColor = res.getColor(R.color.abaDisabledGrey);
    private Drawable greenCircle = res.getDrawable(R.drawable.circle_green_big);
    private Drawable blueCircle = res.getDrawable(R.drawable.circle_blue);
    private int greenColor = res.getColor(R.color.abaEmailGreen);

    interface SectionListener {
        void onSectionClick(SectionViewModel section);
    }

    SectionsAdapter(Context context, ArrayList<SectionViewModel> sections, SectionListener sectionListener) {
        super(context, 0, sections);
        this.section = sections;
        this.sectionListener = sectionListener;
    }

    @SuppressWarnings("deprecation")
    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // Get the data item for this position
        SectionViewModel section = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_section_variation, parent, false);
        }

        ViewHolder holder = new ViewHolder(convertView);

        decorateProgressBar(holder.progressBars, section.getProgress());

        holder.iconView.setImageResource(section.getIcon());

        convertView.setOnClickListener(new OnSectionClick(position));

        if (!section.isUnlocked()) {
            holder.sectionNameTv.setTextColor(disabledColor);
            holder.statusImageView.setImageResource(R.drawable.lock_icon);
            holder.iconView.setAlpha(0.5f);
        }

        if (section.isNextSection()) {
            holder.iconView.setBackgroundDrawable(blueCircle);
        } else if (section.getProgress() == 100) {
            holder.iconView.setBackgroundDrawable(greenCircle);
        }

        // Populate the data into the template view using the data object
        holder.sectionNameTv.setText(res.getString(section.getName()));

        // Return the completed view to render on screen
        return convertView;
    }

    private void decorateProgressBar(List<ProgressBar> progressBars, int completedPercentage) {

        int numberOfBars = progressBars.size();
        for (int i = 0; i < numberOfBars; i++) {
            progressBars.get(i).setMax(25);
            int progress = completedPercentage - 25 * i;
            progress = progress >= 25 ? 25 : progress;
            progressBars.get(i).setProgress(progress);
            if (completedPercentage == 100) {
                progressBars.get(i).getProgressDrawable().setColorFilter(greenColor, PorterDuff.Mode.SRC_IN);
            }
        }
    }

    class ViewHolder {

        @BindView(R.id.textView)
        TextView sectionNameTv;
        @BindView(R.id.imageView)
        ImageView iconView;
        @BindView(iv_chevron)
        ImageView statusImageView;
        @BindViews({R.id.progressBar, R.id.progressBar2, R.id.progressBar3, R.id.progressBar4})
        List<ProgressBar> progressBars;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }

    }

    private class OnSectionClick implements View.OnClickListener {

        private final int position;

        OnSectionClick(int position) {
            this.position = position;
        }

        @Override
        public void onClick(View v) {
            sectionListener.onSectionClick(section.get(position));
        }
    }
}