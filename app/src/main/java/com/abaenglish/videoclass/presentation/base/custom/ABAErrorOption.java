package com.abaenglish.videoclass.presentation.base.custom;

import android.view.View;

/**
 * Created by iaguila on 10/4/15.
 */
public class ABAErrorOption{
    private String option;
    private View.OnClickListener clickListener;

    public ABAErrorOption(String option, View.OnClickListener clickListener) {
        this.option = option;
        this.clickListener = clickListener;
    }

    public String getOption() {
        return option;
    }

    public View.OnClickListener getClickListener() {
        return clickListener;
    }
}
