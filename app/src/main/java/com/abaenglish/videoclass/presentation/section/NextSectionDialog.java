package com.abaenglish.videoclass.presentation.section;

import android.animation.ObjectAnimator;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AlphaAnimation;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.abaenglish.videoclass.ABAApplication;
import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.analytics.helpers.GenericTrackingInterface;
import com.abaenglish.videoclass.analytics.modern.MixpanelTrackingManager;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.data.persistence.ABAUser;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.abaenglish.videoclass.presentation.section.SectionHelper.Section;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;

import io.realm.Realm;

@EFragment
public class NextSectionDialog extends DialogFragment {

    private static final String EXTRA_UNIT = "EXTRA_UNIT";
    public static final String EXTRA_SECTION = "SECTION";

    public static final int DELAY_MILLIS = 5000;

    private static final String ID_ROBIN = "7889918";
    private static final String ID_GRAHAM = "200001";
    private static final String ID_MARTINE = "200015";
    private static final String ID_PRISCILLA = "200085";
    private static final String ID_NIKKI = "200087";
    private static final String ID_ELLA = "1202399";
    private static final String ID_KATE = "200084";

    private Realm realm;
    private ABAUser user;
    private String unitId;
    private int sectionId;

    public interface ResultListener {
        void goToNext(boolean result);
    }

    @ViewById
    ImageView teacherImageView;
    int teacherDrawable;

    @ViewById
    TextView finishedSectionTextView;

    @ViewById
    ProgressBar nextSectionProgressBar;

    @ViewById
    Button buttonNextSection;

    @ViewById
    RelativeLayout allViewsLayout;

    private ResultListener resultListener;
    private boolean mDismissed = false;
    private boolean mixPanelEventSent = false;
    private boolean newPicture = true;

    public static NextSectionDialog_ newInstance(@Section int currentSection, String currentUnitId) {
        NextSectionDialog_ nextSectionDialog = new NextSectionDialog_();
        Bundle bundle = new Bundle();
        bundle.putInt(EXTRA_SECTION, currentSection);
        bundle.putString(EXTRA_UNIT, currentUnitId);
        nextSectionDialog.setArguments(bundle);
        return nextSectionDialog;
    }

    // ================================================================================
    // Lifecycle methods
    // ================================================================================

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);

        realm = Realm.getInstance(ABAApplication.get().getRealmConfiguration());
        user = DataStore.getInstance().getUserController().getCurrentUser(realm);

        sectionId = getArguments().getInt(EXTRA_SECTION);
        unitId = getArguments().getString(EXTRA_UNIT);

        chooseImage(user.getTeacherId());
    }

    @Override
    public void onPause() {
        mDismissed = true;
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();
        nextSectionProgressBar.setProgress(0);
        mDismissed = false;
        startTimeout();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        if (dialog.getWindow() != null) {
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        }
        return dialog;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view;
        if (newPicture) {
            view = inflater.inflate(R.layout.next_section_dialog, container);
            view.findViewById(R.id.quitButton).bringToFront();
            view.findViewById(R.id.teacherImageLayout).bringToFront();
            view.findViewById(R.id.finishedSectionTextLayout).bringToFront();
            view.findViewById(R.id.buttonLayout).bringToFront();

        } else {
            view = inflater.inflate(R.layout.next_section_dialog_circle, container);
        }

        return view;
    }

    @Override
    public void onDestroyView() {
        realm.close();
        super.onDestroyView();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        mDismissed = true;
        if (!mixPanelEventSent) {
            MixpanelTrackingManager.getSharedInstance(getActivity()).trackLinearExpPopupAction(GenericTrackingInterface.PopupActionType.kActionTypeDismiss);
            mixPanelEventSent = true;
        }
    }

    @Override
    public void onCancel(DialogInterface dialog) {
        goToDashboard(false);
        super.onCancel(dialog);
    }

    // ================================================================================
    // Public methods
    // ================================================================================

    public void addListener(ResultListener resultListener) {
        this.resultListener = resultListener;
    }

    public static boolean checkNextSectionIsDone(String unitId, int currentSectionId) {
        Realm instance = Realm.getInstance(ABAApplication.get().getRealmConfiguration());

        ABAUnit unit = DataStore.getInstance().getLevelUnitController().getUnitWithId(instance, unitId);
        boolean nextIsCompleted;
        switch (currentSectionId) {
            case 1:
                nextIsCompleted = unit.getSectionSpeak().isCompleted();
                break;
            case 2:
                nextIsCompleted = unit.getSectionWrite().isCompleted();
                break;
            case 3:
                nextIsCompleted = unit.getSectionInterpret().isCompleted();
                break;
            case 4:
                nextIsCompleted = unit.getSectionVideoClass().isCompleted();
                break;
            case 5:
                nextIsCompleted = unit.getSectionExercises().isCompleted();
                break;
            case 6:
                nextIsCompleted = unit.getSectionVocabulary().isCompleted();
                break;
            case 7:
                nextIsCompleted = unit.getSectionEvaluation().isCompleted();
                break;
            default:
                nextIsCompleted = false;

        }

        instance.close();
        return nextIsCompleted;
    }

    // ================================================================================
    // Private methods
    // ================================================================================

    private void chooseImage(String teacherId) {

        switch (teacherId) {
            case ID_ROBIN:
                teacherDrawable = R.drawable.robin;
                break;
            case ID_GRAHAM:
                teacherDrawable = R.drawable.graham;
                break;
            case ID_PRISCILLA:
                teacherDrawable = R.drawable.priscilla;
                break;
            case ID_MARTINE:
                teacherDrawable = R.drawable.martine;
                newPicture = false;
                break;
            case ID_NIKKI:
                teacherDrawable = R.drawable.nikki;
                break;
            case ID_ELLA:
                teacherDrawable = R.drawable.ella;
                break;
            case ID_KATE:
                teacherDrawable = R.drawable.kate;
                break;
            default:
                teacherDrawable = 0;
        }
    }

    protected void loadTeacherImage() {

        if (newPicture) {
            try {
                getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        ImageLoader.getInstance().displayImage("drawable://" + teacherDrawable, teacherImageView, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
                //Nothing special
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                dismiss();
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                AlphaAnimation animation = new AlphaAnimation(0.0f, 1.0f);
                animation.setDuration(300);
                animation.setInterpolator(new DecelerateInterpolator());
                allViewsLayout.setVisibility(View.VISIBLE);
                allViewsLayout.startAnimation(animation);
                startTimeout();
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
                dismiss();
            }
        });
        finishedSectionTextView.setText(getString(R.string.unitContinueSectionTeacherText) + " " + SectionType.fromInt(sectionId + 1).getSectionName(getActivity()));
        buttonNextSection.setText(getString(R.string.LinealExpContinueButtonKey));
    }

    @Background
    protected void startTimeout() {

        startAnimationProgress();
        try {
            Thread.sleep(DELAY_MILLIS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (!mDismissed) {
            performClick();
        }
    }

    @UiThread
    protected void performClick() {
        if (buttonNextSection != null) {
            buttonNextSection.performClick();
        }
    }

    @UiThread
    protected void startAnimationProgress() {
        if (nextSectionProgressBar != null) {
            //animate the progress bar
            ObjectAnimator progressAnimator;
            progressAnimator = ObjectAnimator.ofInt(nextSectionProgressBar, "progress", nextSectionProgressBar.getProgress(), 100);
            progressAnimator.setDuration(DELAY_MILLIS);
            progressAnimator.start();
        }
    }

    @UiThread
    protected void goToDashboard(boolean result) {
        // going to the next section only if the dialog is not dismissed yet
        if (!mDismissed) {
            if (resultListener != null) {
                resultListener.goToNext(result);
            }
            if (!mixPanelEventSent) {
                MixpanelTrackingManager.getSharedInstance(getActivity()).trackLinearExpPopupAction(GenericTrackingInterface.PopupActionType.kActionTypeNext);
                mixPanelEventSent = true;
            }
            dismiss();
        }
    }

    @AfterViews
    protected void afterViews() {
        loadTeacherImage();
    }

    @Click(R.id.buttonNextSection)
    protected void onNextSectionClick() {
        goToDashboard(true);
    }

    @Click(R.id.quitButton)
    protected void onQuitButtonClick() {
        goToDashboard(false);
    }
}
