package com.abaenglish.videoclass.presentation.base.custom;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.LinearLayout;

import com.abaenglish.videoclass.R;
import com.bzutils.BZScreenHelper;


/**
 * Created by iaguila on 11/11/13.
 */
public class ABAProgressDialog extends ProgressDialog {

    public static int SimpleDialog = 0;
    public static int ComplexDialog = 1;

    Handler handler;
    Runnable runnable;
    int typeProgressDialog;

    public ABAProgressDialog(Activity context, int typeProgressDialog) {
        super(context);
        this.typeProgressDialog = typeProgressDialog;
        setCanceledOnTouchOutside(false);
        setCancelable(false);
        setIndeterminate(true);
    }

    public ABAProgressDialog(Context context, int theme) {
        super(context, theme);
    }

    @Override
    public void show() {
        super.show();
        setContentView(R.layout.view_progress_dialog);
        final ABATextView dialogText = (ABATextView) findViewById(R.id.dialogMessage);
        final LinearLayout progressDialog = (LinearLayout) findViewById(R.id.progressDialog);

        if (typeProgressDialog == ComplexDialog) {
            runnable = new Runnable() {
                @Override
                public void run() {
                    progressDialog.startAnimation(new ResizeAnimation(progressDialog, dialogText, (int) (getPortraitWidth() * 0.6f)));
                }
            };
            handler = new Handler();
            handler.postDelayed(runnable, 1500);
        }
    }

    @Override
    public void dismiss() {
        super.dismiss();
        if (typeProgressDialog == ComplexDialog && handler != null)
            handler.removeCallbacks(runnable);
    }

    private int getPortraitWidth() {
        if (BZScreenHelper.getScreenWidth(getContext()) > BZScreenHelper.getScreenHeight(getContext()))
            return BZScreenHelper.getScreenHeight(getContext());
        return BZScreenHelper.getScreenWidth(getContext());
    }

    // ================================================================================
    // Private methods - ResizeAnimation ProgressDialog
    // ================================================================================

    public class ResizeAnimation extends Animation implements Animation.AnimationListener {
        final int startWidth;
        final int targetWidth;
        ABATextView textView;
        View view;

        public ResizeAnimation(View view, ABATextView textView, int targetWidth) {
            this.view = view;
            this.targetWidth = targetWidth;
            this.startWidth = view.getWidth();
            this.textView = textView;

            setDuration(300);
            setFillAfter(true);
            setAnimationListener(this);
        }

        @Override
        protected void applyTransformation(float interpolatedTime, Transformation t) {
            int newWidth = (int) (startWidth + (targetWidth - startWidth) * interpolatedTime);
            view.getLayoutParams().width = newWidth;
            view.requestLayout();
        }

        @Override
        public void initialize(int width, int height, int parentWidth, int parentHeight) {
            super.initialize(width, height, parentWidth, parentHeight);
        }

        @Override
        public boolean willChangeBounds() {
            return true;
        }

        @Override
        public void onAnimationStart(Animation animation) {
        }

        @Override
        public void onAnimationEnd(Animation animation) {
            textView.setVisibility(View.VISIBLE);
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
        }
    }
}
