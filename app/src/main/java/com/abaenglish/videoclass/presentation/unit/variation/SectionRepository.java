package com.abaenglish.videoclass.presentation.unit.variation;

import java.util.List;

/**
 * Created by xabierlosada on 10/10/16.
 */

public interface SectionRepository {

    List<SectionViewModel> getSections(String unitId);

}
