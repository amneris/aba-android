package com.abaenglish.videoclass.presentation.abaMoment;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.os.CountDownTimer;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.TextView;

import com.abaenglish.videoclass.ABAApplication;
import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.UrlUtils;
import com.abaenglish.videoclass.data.network.retrofit.abaMomentsUtils.ABAMoment;
import com.abaenglish.videoclass.presentation.abaMoment.customViews.CircleABAMomentsListView;
import com.abaenglish.videoclass.presentation.abaMoment.customViews.CircleABAMomentsListView.ABAMomentState;
import com.abaenglish.videoclass.presentation.base.custom.ABATextView;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.IOException;
import java.util.List;

/**
 * Created by Julien on 28/02/2017.
 * Adapter used to display the list of the ABAMoment in the {@link ABAMomentsListFragment}
 */
class ABAMomentRecyclerViewAdapter extends RecyclerView.Adapter<ABAMomentRecyclerViewAdapter.ViewHolder> {

    private final Context mContext;
    private final List<ABAMoment> mValues;
    private final OnListFragmentInteractionListener mListener;

    private final static int colorABAMomentDarkBlue = R.color.colorABAMomentDarkBlue;
    private final static int colorScreenBackground = R.color.abaWhite;
    private final static int colorABAMomentInactive = R.color.colorABAMomentInactive;
    private final static int colorAbaDarkGrey = R.color.abaDarkGrey;

    private ImageLoader imageLoader = ImageLoader.getInstance();
    private int animateItem = -1;

    private enum status {Done, Inactive, Active, New}

    ABAMomentRecyclerViewAdapter(Context context, List<ABAMoment> abaMoments, OnListFragmentInteractionListener listener) {
        mContext = context;
        mValues = abaMoments;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_abamoment, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mABAMoment = mValues.get(position);
        holder.mContentView.setText(mValues.get(position).getLocalName(mContext));
        holder.mContentView.setTextColor(ContextCompat.getColor(mContext, colorAbaDarkGrey));
        holder.mView.setContentDescription(holder.mABAMoment.getId());

        if (animateItem == position) {
            holder.mABAMoment.setStatus("Done");
        }

        if (holder.mABAMoment.getStatus().compareTo(String.valueOf(status.Done)) == 0) {
            //State done
            holder.circleABAMomentsListView.setEnabled(true);
            holder.circleABAMomentsListView.setState(ABAMomentState.DONE);
            imageLoader.displayImage(UrlUtils.getIconUrl(mContext, holder.mABAMoment.getId(), holder.mABAMoment.getIcon()), holder.iconView);
            holder.iconView.setColorFilter(ContextCompat.getColor(mContext, colorScreenBackground));
            holder.statusImageView.setImageResource(R.drawable.check_round_icon);
        } else if (holder.mABAMoment.getStatus().compareTo(String.valueOf(status.Active)) == 0) {
            //State active but not last
            holder.circleABAMomentsListView.setEnabled(true);
            holder.circleABAMomentsListView.setState(ABAMomentState.ACTIVE);
            imageLoader.displayImage(UrlUtils.getIconUrl(mContext, holder.mABAMoment.getId(), holder.mABAMoment.getIcon()), holder.iconView);
            holder.iconView.setColorFilter(ContextCompat.getColor(mContext, colorABAMomentDarkBlue));
            holder.statusImageView.setImageResource(android.R.color.transparent);
        } else if (holder.mABAMoment.getStatus().compareTo(String.valueOf(status.New)) == 0) {
            //State active
            holder.circleABAMomentsListView.setEnabled(true);
            holder.circleABAMomentsListView.setState(ABAMomentState.ACTIVE);
            imageLoader.displayImage(UrlUtils.getIconUrl(mContext, holder.mABAMoment.getId(), holder.mABAMoment.getIcon()), holder.iconView);
            holder.iconView.setColorFilter(ContextCompat.getColor(mContext, colorABAMomentDarkBlue));
            holder.statusImageView.setImageResource(R.drawable.star_circle);
        } else if (holder.mABAMoment.getStatus().compareTo(String.valueOf(status.Inactive)) == 0) {
            //State inactive/disable
            holder.circleABAMomentsListView.setEnabled(false);
            holder.circleABAMomentsListView.setState(ABAMomentState.INACTIVE);
            imageLoader.displayImage(UrlUtils.getIconUrl(mContext, holder.mABAMoment.getId(), holder.mABAMoment.getIcon()), holder.iconView);
            holder.iconView.setColorFilter(ContextCompat.getColor(mContext, colorABAMomentInactive));
            holder.statusImageView.setImageResource(R.drawable.circle_lock_icon);
            holder.mContentView.setTextColor(ContextCompat.getColor(mContext, colorABAMomentInactive));
        }

        //animate the items the user has just finished
        if (animateItem == position) {

            holder.statusImageView.setVisibility(View.INVISIBLE);
            holder.circleABAMomentsListView.setProgress(0);

            //Initiates and starts the countdown timer which gradually increases the "progress" of the AbaMoments
            final int duration = 1000;
            new CountDownTimer(duration, 5) {

                @Override
                public void onTick(long millisUntilFinished) {
                    holder.circleABAMomentsListView.setProgress((int) ((duration - millisUntilFinished) * 100 / duration));
                }

                @Override
                public void onFinish() {

                    holder.circleABAMomentsListView.setProgress(100);

                    //green check
                    holder.statusImageView.setVisibility(View.VISIBLE);
                    Animation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
                    alphaAnimation.setDuration(1000);
                    holder.statusImageView.setAnimation(alphaAnimation);

                    //play the success song
                    try {
                        AssetFileDescriptor afd = ABAApplication.get().getAssets().openFd("success.mp3");
                        MediaPlayer player = new MediaPlayer();
                        player.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
                        player.prepare();
                        player.start();
                        player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                            @Override
                            public void onCompletion(MediaPlayer mp) {
                                mp.release();
                            }
                        });
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }.start();

            animateItem = -1;
        }

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener && holder.circleABAMomentsListView.isEnabled()) {
                    mListener.onListFragmentInteraction(holder.mABAMoment, position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    void animateItemAt(int index) {
        animateItem = index;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final View mView;
        final CircleABAMomentsListView circleABAMomentsListView;
        final TextView mContentView;
        final ImageView statusImageView;
        final ImageView iconView;
        ABAMoment mABAMoment;

        ViewHolder(View view) {
            super(view);
            mView = view;
            circleABAMomentsListView = (CircleABAMomentsListView) view.findViewById(R.id.ABAMomentLogo);
            mContentView = (ABATextView) view.findViewById(R.id.ABAMomentTitle);
            statusImageView = (ImageView) view.findViewById(R.id.statusImageView);
            iconView = (ImageView) view.findViewById(R.id.iconImageView);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }

    interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(ABAMoment abaMoment, int position);
    }
}
