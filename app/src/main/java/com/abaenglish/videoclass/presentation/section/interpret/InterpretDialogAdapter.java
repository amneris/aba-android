package com.abaenglish.videoclass.presentation.section.interpret;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;

import com.abaenglish.videoclass.ABAApplication;
import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.data.persistence.ABAInterpret;
import com.abaenglish.videoclass.data.persistence.ABAInterpretRole;
import com.abaenglish.videoclass.data.persistence.ABAPhrase;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.abaenglish.videoclass.presentation.base.custom.ABATextView;
import com.abaenglish.videoclass.presentation.base.custom.FontCache;
import com.bzutils.images.RoundedImageView;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.ArrayList;

import static com.abaenglish.videoclass.R.id.leftCharIcon;

/**
 * Created by madalin on 28/05/15.
 */
public class InterpretDialogAdapter extends BaseAdapter implements ListAdapter {
    private Context mContext;
    private LayoutInflater mLayoutInflater;

    private ABAInterpret abaInterpret;
    private ABAInterpretRole currentRole;
    private ABAUnit currentUnit;

    private ArrayList<ABAPhrase> items;

    private FontCache fontCache;

    public InterpretDialogAdapter(Context context, ABAUnit unit, ABAInterpretRole role, ArrayList<ABAPhrase> itemsToLoad, FontCache fontCache) {
        mContext = context;
        mLayoutInflater = LayoutInflater.from(context);

        currentUnit = unit;
        items = itemsToLoad;
        currentRole = role;
        abaInterpret = role.getInterpret();

        this.fontCache = fontCache;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return (long) position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        final ItemViewHolder mItemViewHolder;

        if (convertView == null) {
            mItemViewHolder = new ItemViewHolder();
            convertView = mLayoutInflater.inflate(R.layout.item_interpretation_dialog, parent, false);
            mItemViewHolder.leftBubbleView = (RelativeLayout) convertView.findViewById(R.id.leftBubbleView);
            mItemViewHolder.rightBubbleView = (RelativeLayout) convertView.findViewById(R.id.rightBubbleView);

            mItemViewHolder.leftDialog = (ABATextView) convertView.findViewById(R.id.leftBubbleDialogText);
            mItemViewHolder.rightDialog = (ABATextView) convertView.findViewById(R.id.rightBubbleDialogText);
            mItemViewHolder.leftDialog.setTypeface(fontCache.getTypeface(FontCache.ABATypeface.sans500));
            mItemViewHolder.rightDialog.setTypeface(fontCache.getTypeface(FontCache.ABATypeface.sans500));
            mItemViewHolder.leftDialog.setTextColor(ContextCompat.getColor(mContext, R.color.abaGrey));
            mItemViewHolder.rightDialog.setTextColor(ContextCompat.getColor(mContext, R.color.abaGrey));

            mItemViewHolder.leftCharName = (ABATextView) convertView.findViewById(R.id.leftCharName);
            mItemViewHolder.rightCharName = (ABATextView) convertView.findViewById(R.id.rightCharName);
            mItemViewHolder.leftCharName.setTypeface(fontCache.getTypeface(FontCache.ABATypeface.sans300));
            mItemViewHolder.rightCharName.setTypeface(fontCache.getTypeface(FontCache.ABATypeface.sans300));

            mItemViewHolder.leftCharIcon = (RoundedImageView) convertView.findViewById(leftCharIcon);
            mItemViewHolder.rightCharIcon = (RoundedImageView) convertView.findViewById(R.id.rightCharIcon);

            convertView.setTag(mItemViewHolder);
        } else {
            mItemViewHolder = (ItemViewHolder) convertView.getTag();
        }

        final ABAPhrase phrase = (ABAPhrase) getItem(position);

        ABATextView dialog;

        if (!phrase.getInterpretRole().equals(currentRole)) { //Left
            mItemViewHolder.leftBubbleView.setVisibility(View.VISIBLE);
            mItemViewHolder.rightBubbleView.setVisibility(View.GONE);
            mItemViewHolder.leftDialog.setText(phrase.getText());
            mItemViewHolder.leftCharName.setText(phrase.getInterpretRole().getName());
            dialog = mItemViewHolder.leftDialog;
        } else { //Right
            mItemViewHolder.leftBubbleView.setVisibility(View.GONE);
            mItemViewHolder.rightBubbleView.setVisibility(View.VISIBLE);
            mItemViewHolder.rightDialog.setText(phrase.getText());

            if (phrase.getInterpretRole().equals(currentRole)) {
                mItemViewHolder.rightCharName.setText(convertView.getResources().getString(R.string.interpretYouKey));
            } else {
                mItemViewHolder.rightCharName.setText(phrase.getInterpretRole().getName());
            }

            dialog = mItemViewHolder.rightDialog;
        }

        // Check if the phrase is the current phrase in order to format it well
        if (position == items.size() - 1) {
//        if(DataStore.getInstance().getInterpretController().getCurrentPhraseForRole(currentRole, currentRole.getInterpret()).equals(phrase)) {
            dialog.setTypeface(fontCache.getTypeface(FontCache.ABATypeface.sans700));
            dialog.setTextColor(ContextCompat.getColor(mContext, R.color.abaGrey));
        } else {
            dialog.setTypeface(fontCache.getTypeface(FontCache.ABATypeface.sans500));
            dialog.setTextColor(ContextCompat.getColor(mContext, R.color.abaDescriptionLightGrey));
        }

        if (DataStore.getInstance().getLevelUnitController().checkIfFileExist(currentUnit, phrase.getInterpretRole().getImageUrl())) {
            if (!phrase.getInterpretRole().equals(currentRole)) {
                DataStore.getInstance().getLevelUnitController().displayImage(currentUnit, phrase.getInterpretRole().getImageUrl(), mItemViewHolder.leftCharIcon);
            } else {
                DataStore.getInstance().getLevelUnitController().displayImage(currentUnit, phrase.getInterpretRole().getImageUrl(), mItemViewHolder.rightCharIcon);
            }
        } else {
            ImageLoader.getInstance().loadImage(phrase.getInterpretRole().getImageUrl(), new SimpleImageLoadingListener() {
                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    if (!phrase.getInterpretRole().equals(currentRole)) {
                        BitmapDrawable ob = new BitmapDrawable(mItemViewHolder.leftCharIcon.getResources(), loadedImage);
                        mItemViewHolder.leftCharIcon.setImageDrawable(ob);
                    } else {
                        BitmapDrawable ob = new BitmapDrawable(mItemViewHolder.rightCharIcon.getResources(), loadedImage);
                        mItemViewHolder.rightCharIcon.setImageDrawable(ob);
                    }
                }
            });
        }

        return convertView;
    }

    public void refresh() {
        ABAPhrase currentPhrase = DataStore.getInstance().getInterpretController().getCurrentPhraseForRole(currentRole, abaInterpret);

        int indexCurrentPhrase = abaInterpret.getDialog().indexOf(currentPhrase);

        for (int i = 0; i <= indexCurrentPhrase; i++) {
            items.add(abaInterpret.getDialog().get(i));
        }
    }

    private class ItemViewHolder {
        private RelativeLayout leftBubbleView;
        private RelativeLayout rightBubbleView;
        private ABATextView leftDialog;
        private ABATextView rightDialog;
        private ABATextView leftCharName;
        private ABATextView rightCharName;
        private RoundedImageView leftCharIcon;
        private RoundedImageView rightCharIcon;
    }
}
