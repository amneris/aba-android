package com.abaenglish.videoclass.presentation.abaMoment.customViews;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;

import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.presentation.abaMoment.ABAMomentsListFragment;

/**
 * Created by julien on 06/03/2017.
 * Custom view to create the circles enclosing the icons in the ABAMoments list.
 * See {@link ABAMomentsListFragment}
 */

public class CircleABAMomentsListView extends View {

    private Paint paint;
    private Paint backgroundPaint;
    private RectF rectF = new RectF();

    private int progress = 100;
    private ABAMomentState state = ABAMomentState.INACTIVE;

    public enum ABAMomentState {
        ACTIVE,
        INACTIVE,
        DONE
    }

    int lineWidth = 3;

    final static int colorABAMomentDarkBlue = R.color.colorABAMomentDarkBlue;
    final static int colorScreenBackground = R.color.abaWhite;
    final static int colorABAMomentInactive = R.color.colorABAMomentInactive;

    public CircleABAMomentsListView(Context context) {
        super(context);
        init();
    }

    public CircleABAMomentsListView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CircleABAMomentsListView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {

        paint = new Paint();
        backgroundPaint = new Paint();

        paint.setStyle(Paint.Style.STROKE);
        paint.setFlags(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(ContextCompat.getColor(getContext(), colorABAMomentDarkBlue));

        backgroundPaint.setFlags(Paint.ANTI_ALIAS_FLAG);

        lineWidth = (int) (lineWidth * getResources().getDisplayMetrics().density);

    }

    @Override
    protected void onDraw(Canvas canvas) {

        int contentWidth = getWidth();
        int contentHeight = getHeight();

        int radius = (Math.min(contentHeight, contentWidth) / 2);

        int x = contentWidth / 2 - radius;
        int y = contentHeight / 2 - radius;
        rectF.set(x, y, radius * 2, radius * 2);

        paint.setStrokeWidth(lineWidth);
        //radius -= lineWidth;

        switch (state) {
            //white background and colored icon and outline
            case ACTIVE:
                backgroundPaint.setColor(ContextCompat.getColor(getContext(), colorScreenBackground));
                paint.setColor(ContextCompat.getColor(getContext(), colorABAMomentDarkBlue));
                break;
            //white background and gray icon and outline
            case INACTIVE:
                backgroundPaint.setColor(ContextCompat.getColor(getContext(), colorScreenBackground));
                paint.setColor(ContextCompat.getColor(getContext(), colorABAMomentInactive));
                break;
            //colored background and white icon
            case DONE:
                backgroundPaint.setColor(ContextCompat.getColor(getContext(), colorABAMomentDarkBlue));
                paint.setColor(ContextCompat.getColor(getContext(), colorABAMomentDarkBlue));
                break;
        }

        //draw the background behind the icon
        if (progress > 99) {
            canvas.drawCircle(contentWidth / 2, contentHeight / 2, radius, backgroundPaint);
        } else {
            float yHeight = progress / (float) 100 * contentHeight;
            float angle = (float) (Math.acos((radius - yHeight) / radius) * 180 / Math.PI);

            float startAngle = 90 + angle;
            float sweepAngle = 360 - angle * 2;
            backgroundPaint.setColor(ContextCompat.getColor(getContext(), colorScreenBackground));
            canvas.drawArc(rectF, startAngle, sweepAngle, false, backgroundPaint);

            canvas.save();
            canvas.rotate(180, contentWidth / 2, contentHeight / 2);
            backgroundPaint.setColor(ContextCompat.getColor(getContext(), colorABAMomentDarkBlue));
            canvas.drawArc(rectF, 270 - angle, angle * 2, false, backgroundPaint);
            canvas.restore();

            canvas.drawCircle(contentWidth / 2, contentHeight / 2, radius, paint);
        }

        //drawing the circle around the icon
        if (state != ABAMomentState.DONE) {
            canvas.drawCircle(contentWidth / 2, contentHeight / 2, radius, paint);
        }

        super.onDraw(canvas);
    }

    public void setProgress(int progress) {
        this.progress = progress;
        invalidate();
    }

    public void setState(ABAMomentState newState) {
        this.state = newState;
        invalidate();
    }

}
