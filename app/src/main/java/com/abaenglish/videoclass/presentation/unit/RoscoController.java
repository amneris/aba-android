package com.abaenglish.videoclass.presentation.unit;

import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.domain.content.SectionController;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.abaenglish.videoclass.presentation.section.SectionType;
import com.bzutils.LogBZ;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jesus Espejo using mbp13jesus on 15/10/15.
 * Refactored by Jesus Espejo on 12/08/2016: Resolving crash.
 */
public class RoscoController {

    /**
     * Method to create the ordered list of sections.
     *
     * @return Ordered list of sections. The first section will be the one on the top of the rosco
     */
    public List<SectionType> getAllSectionsAscendently() {
        List<SectionType> listOfSections = new ArrayList<>();
        listOfSections.add(SectionType.kABAFilm);
        listOfSections.add(SectionType.kHabla);
        listOfSections.add(SectionType.kEscribe);
        listOfSections.add(SectionType.kInterpreta);
        listOfSections.add(SectionType.kVideoClase);
        listOfSections.add(SectionType.kEjercicios);
        listOfSections.add(SectionType.kVocabulario);
        listOfSections.add(SectionType.kEvaluacion);

        return listOfSections;
    }

    public List<SectionType> getAccessibleSections(ABAUnit unit) {
        return calculateAvailableSections(unit);
    }

    public SectionType getCurrentSectionForUnit(ABAUnit unit) {

        if (!unit.getSectionFilm().isCompleted()) {
            return SectionType.kABAFilm;
        }
        if (!unit.getSectionSpeak().isCompleted()) {
            return SectionType.kHabla;
        }
        if (!unit.getSectionWrite().isCompleted()) {
            return SectionType.kEscribe;
        }
        if (!unit.getSectionInterpret().isCompleted()) {
            return SectionType.kInterpreta;
        }
        if (!unit.getSectionVideoClass().isCompleted()) {
            return SectionType.kVideoClase;
        }
        if (!unit.getSectionExercises().isCompleted()) {
            return SectionType.kEjercicios;
        }
        if (!unit.getSectionVocabulary().isCompleted()) {
            return SectionType.kVocabulario;
        }
        if (!unit.getSectionEvaluation().isCompleted()) {
            return SectionType.kEvaluacion;
        }

        return SectionType.kEvaluacion;
    }

    public SectionController.SectionStatus getStatusForSection(ABAUnit unit, SectionType sectionToCheckStatusFrom) {

        List<SectionType> accessibleSections = getAccessibleSections(unit);

        // Loading the current unit section
        SectionType currentUnitSection = getCurrentSectionForUnit(unit);

        // Getting section
        Object section = getSectionWithSectionType(unit, sectionToCheckStatusFrom);

        try {

            boolean isLocked = isSectionLocked(sectionToCheckStatusFrom, accessibleSections);
            boolean isCompleted = calculateSectionCompletion(section);
            float progress = calculateSectionProgress(section);

            if (isLocked) {
                return SectionController.SectionStatus.kABAStatusLocked;
            } else if (isCompleted || progress == 100) {
                return SectionController.SectionStatus.kABAStatusCompleted;
            } else if (currentUnitSection == sectionToCheckStatusFrom) {
                return SectionController.SectionStatus.kABAStatusActual;
            } else {
                return SectionController.SectionStatus.kABAStatusPending;
            }

        } catch (Exception e) {
            e.printStackTrace();
            LogBZ.d("Error determining section status");
        }

        // If there was an error calculating then the section is locked by default
        return SectionController.SectionStatus.kABAStatusLocked;
    }

    // ================================================================================
    // Private methods
    // ================================================================================

    private Object getSectionWithSectionType(ABAUnit unit, SectionType sectiontype) {
        Object section = null;
        switch (sectiontype) {
            case kABAFilm: {
                section = unit.getSectionFilm();
                break;
            }
            case kEjercicios: {
                section = unit.getSectionExercises();
                break;
            }
            case kEscribe: {
                section = unit.getSectionWrite();
                break;
            }
            case kEvaluacion: {
                section = unit.getSectionEvaluation();
                break;
            }
            case kHabla: {
                section = unit.getSectionSpeak();
                break;
            }
            case kInterpreta: {
                section = unit.getSectionInterpret();
                break;
            }
            case kVideoClase: {
                section = unit.getSectionVideoClass();
                break;
            }
            case kVocabulario: {
                section = unit.getSectionVocabulary();
                break;
            }
        }

        return section;
    }

    private boolean isSectionLocked(SectionType sectiontype, List<SectionType> accessibleSections) {

        // section is locked if it's not included in the accessible sections
        return !accessibleSections.contains(sectiontype);
    }

    private List<SectionType> calculateAvailableSections(ABAUnit unit) {

        // Result
        List<SectionType> listOfAvailableSections = new ArrayList<>();

        // Getting level index and user type
        int unitIndexWithinLevel = DataStore.getInstance().getLevelUnitController().getUnitIndexWithinLevel(unit.getIdUnit(), unit.getLevel().getIdLevel());
        boolean isPremium = DataStore.getInstance().getUserController().isUserPremium();

        if (unitIndexWithinLevel == 1 || isPremium) {
            listOfAvailableSections.add(SectionType.kABAFilm);
            listOfAvailableSections.add(SectionType.kHabla);
            listOfAvailableSections.add(SectionType.kEscribe);
            listOfAvailableSections.add(SectionType.kInterpreta);
            listOfAvailableSections.add(SectionType.kVideoClase);
            listOfAvailableSections.add(SectionType.kEjercicios);
            listOfAvailableSections.add(SectionType.kVocabulario);

            if (unit.getSectionFilm().isCompleted() && unit.getSectionSpeak().isCompleted() && unit.getSectionWrite().isCompleted() && unit.getSectionInterpret().isCompleted() && unit.getSectionVideoClass().isCompleted() && unit.getSectionExercises().isCompleted() && unit.getSectionVocabulary().isCompleted()) {
                listOfAvailableSections.add(SectionType.kEvaluacion);
            }

        } else {

            listOfAvailableSections.add(SectionType.kVideoClase);
        }

        return listOfAvailableSections;
    }

    private boolean calculateSectionCompletion(Object section) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method methodComplete = section.getClass().getMethod("isCompleted");
        boolean isCompleted = (boolean) methodComplete.invoke(section);
        return isCompleted;
    }

    private float calculateSectionProgress(Object section) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Method methodProgress = section.getClass().getMethod("getProgress");
        float progress = (float) methodProgress.invoke(section);
        return progress;
    }
}
