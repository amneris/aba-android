package com.abaenglish.videoclass.presentation.section.assessment;

import android.app.Application;

import com.abaenglish.videoclass.domain.content.EvaluationController;
import com.abaenglish.videoclass.domain.content.LevelUnitController;
import com.abaenglish.videoclass.domain.content.UserController;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.abaenglish.videoclass.presentation.base.custom.FontCache;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by xabierlosada on 21/07/16.
 */

@Module
public class EvaluationModule {

    private final Application application;

    public EvaluationModule(Application application) {
        this.application = application;
    }

    @Provides
    @EvaluationScope
    public LevelUnitController providesLevelUnitController() {
        return DataStore.getInstance().getLevelUnitController();
    }

    @Provides
    @EvaluationScope
    public UserController providesUserController() {
        return DataStore.getInstance().getUserController();
    }

    @Provides
    @EvaluationScope
    public EvaluationController providesEvaluationController() {
        return DataStore.getInstance().getEvaluationController();
    }

}
