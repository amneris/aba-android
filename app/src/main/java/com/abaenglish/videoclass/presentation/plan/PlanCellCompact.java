package com.abaenglish.videoclass.presentation.plan;

import android.content.Context;
import android.widget.TextView;

import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.data.persistence.ABAPlan;
import com.abaenglish.videoclass.domain.content.PlanController;
import com.abaenglish.videoclass.presentation.base.custom.FontCache;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import java.text.NumberFormat;

/**
 * Created by Jesus Espejo using mbp13jesus on 15/03/17.
 * Using Android Annotations.
 */
@EViewGroup(R.layout.item_plan_compact)
public class PlanCellCompact extends AbstractPlanCell {

    @ViewById
    TextView textViewPriceCurrency;

    public PlanCellCompact(Context context) {
        super(context);
    }

    public void updateView(ABAPlan planToRender, FontCache fontCache, int index) {

        super.updateView(planToRender, fontCache, index);

        String price = PlanController.getPriceForOneMonth(planToRender.getDiscountPrice(), planToRender.getDays());

        String priceInteger = getIntegerPrice(price);
        String priceDecimal = getDecimalPrice(price);

        textViewPriceInteger.setText(getNumberFormattedIntegerPrice(priceInteger));
        textViewPriceDecimal.setText(priceDecimal);

        textViewPriceCurrency.setText(planToRender.getCurrency());
    }

    // ================================================================================
    // Private methods
    // ================================================================================

    private String getIntegerPrice(String price) {

        int dotPosition = price.indexOf(",");
        if (dotPosition > 0) {
            return price.substring(0, dotPosition);
        }

        return "";
    }

    private String getDecimalPrice(String price) {
        int dotPosition = price.indexOf(",");
        if (dotPosition > 0) {
            return price.substring(dotPosition, dotPosition + 3);
        }

        return "";
    }

    private String getNumberFormattedIntegerPrice(String priceInteger) {
        return NumberFormat.getNumberInstance().format(Double.valueOf(priceInteger));
    }

}
