package com.abaenglish.videoclass.presentation.plan;

import android.content.Context;

import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.data.persistence.ABAPlan;
import com.abaenglish.videoclass.domain.content.PlanController;
import com.abaenglish.videoclass.presentation.base.custom.FontCache;

import org.androidannotations.annotations.EViewGroup;

/**
 * Created by Jesus Espejo using mbp13jesus on 15/03/17.
 * Using Android Annotations.
 */
@EViewGroup(R.layout.item_plan)
public class PlanCell extends AbstractPlanCell {

    public PlanCell(Context context) {
        super(context);
    }

    // ================================================================================
    // PlanCellUpdateInterface
    // ================================================================================

    public void updateView(ABAPlan planToRender, FontCache fontCache, int index) {

        super.updateView(planToRender, fontCache, index);

        String currencySymbol = planToRender.getCurrencySymbol();
        String price = PlanController.getPriceForOneMonth(planToRender.getDiscountPrice(), planToRender.getDays());

        int dotPosition = price.indexOf(",");
        if (dotPosition > 0) {
            String priceInteger = price.substring(0, dotPosition);
            String priceDecimal = price.substring(dotPosition, dotPosition + 3);

            textViewPriceInteger.setText(priceInteger);
            textViewPriceDecimal.setText(priceDecimal + currencySymbol);
        }
    }


}
