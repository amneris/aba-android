package com.abaenglish.videoclass.presentation.plan;

import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.abaenglish.videoclass.ABAApplication;
import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.domain.content.PlanController;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.abaenglish.videoclass.presentation.base.ABAMasterActivity;
import com.abaenglish.videoclass.presentation.base.ABAMasterFragment;
import com.abaenglish.videoclass.presentation.base.custom.ABAProgressDialog;
import com.abaenglish.videoclass.presentation.base.custom.ABATextView;
import com.crashlytics.android.Crashlytics;

/**
 * Created by madalin on 17/09/15.
 */
public class PlansFragment extends ABAMasterFragment implements View.OnClickListener {

    private LinearLayout mPlansLayout;
    private ListView mPaymentList;
    private ABATextView mPlanTitle;
    private ABATextView mRetryButton;
    private LinearLayout mErrorLinearLayout;
    private PlansAdapter adapter;


    @Override
    protected int onResLayout() {
        return R.layout.fragment_payment;
    }

    @Override
    protected void onConfigView() {
        mPlansLayout = $(R.id.layout_plans);
        mPaymentList = $(R.id.payment_list);
        mPlanTitle = $(R.id.top_text);
        mRetryButton = $(R.id.retry_load_button);
        mErrorLinearLayout = $(R.id.error_layout);

        mRetryButton.setOnClickListener(this);
        loadData();
        Crashlytics.log(Log.INFO, "Plans", "User opens Plans view");
    }

    @Override
    protected void onConfigToolbar(ABATextView toolbarTitle, ABATextView toolbarSubTitle, ImageButton toolbarButton) {
        toolbarTitle.setText(getString(R.string.chooseYourSubscriptionKey));
        toolbarSubTitle.setVisibility(View.GONE);

        if (getActivity() instanceof PlansActivity) {
            toolbarButton.setImageResource(R.mipmap.back);
            toolbarButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // finish activity
                    getActivity().finish();
                    getActivity().overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_exit_unitanimation2);
                }
            });
        }
    }

    private void loadData() {
        if (getActivity() != null) {
            getABAActivity().showProgressDialog(ABAProgressDialog.SimpleDialog);
            DataStore.getInstance().getPlanController(((ABAMasterActivity) getActivity()).tracker).fetchPlanContent(getActivity(), DataStore.getInstance().getUserController().getCurrentUser(getRealm()), new PlanController.ABAPlanCallback() {
                @Override
                public void onResult(PlanController.SubscriptionResult type) {
                    if (type == PlanController.SubscriptionResult.SUBSCRIPTION_RESULT_OK && getActivity() != null) {
                        // Success
                        getABAActivity().dismissProgressDialog();
                        if (!PlanController.getTitleTextPlans(getRealm()).equals("")) {
                            mPlanTitle.setText(PlanController.getTitleTextPlans(getRealm()));
                        }

                        adapter = new PlansAdapter(getABAActivity(), ABAApplication.get().getRealmConfiguration(), getABAActivity().tracker, serverConfig.isSixMonthsTierActive());
                        mPaymentList.setAdapter(adapter);
                        mPlansLayout.setVisibility(View.VISIBLE);
                        Crashlytics.log(Log.INFO, "Plans", "Plans have been loaded");
                    } else {
                        // Error handling
                        if (getActivity() != null) {
                            getABAActivity().dismissProgressDialog();
                            getABAActivity().showABAErrorNotification(getABAActivity().getString(R.string.errorGetProducts));

                            mPlansLayout.setVisibility(View.GONE);
                            mErrorLinearLayout.setVisibility(View.VISIBLE);
                            Crashlytics.log(Log.INFO, "Plans", "Error loading plans");
                        }
                    }
                }
            });
        }
    }

    @Override
    public void onClick(View v) {
        mErrorLinearLayout.setVisibility(View.GONE);
        loadData();
    }
}
