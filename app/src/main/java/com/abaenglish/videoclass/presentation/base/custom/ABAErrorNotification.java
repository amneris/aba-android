package com.abaenglish.videoclass.presentation.base.custom;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.presentation.base.ABAMasterActivity;

import java.util.List;

/**
 * Created by iaguila on 25/3/2015
 * Refactored by xilosada 8/4/2016
 */
public class ABAErrorNotification extends LinearLayout {

    private ABATextView textView;
    private ImageView alertIcon;
    private LinearLayout optionsLayout;
    private String errorString;
    private List<ABAErrorOption> options;

    public enum ABAErrorNotificationType {
        ErrorNotification,
        AlertNotification
    }

    public ABAErrorNotification(Context context, ABAErrorNotificationType abaNotificationType) {
        super(context);
        init(abaNotificationType);
    }

    public ABAErrorNotification(Context context, AttributeSet attrs, ABAErrorNotificationType abaNotificationType) {
        super(context, attrs);
        init(abaNotificationType);
    }

    public ABAErrorNotification(Context context, AttributeSet attrs, int defStyleAttr, ABAErrorNotificationType abaNotificationType) {
        super(context, attrs, defStyleAttr);
        init(abaNotificationType);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ABAErrorNotification(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes, ABAErrorNotificationType abaNotificationType) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(abaNotificationType);
    }

    private void init(ABAErrorNotificationType abaNotificationType) {
        setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        if (abaNotificationType.equals(ABAErrorNotificationType.ErrorNotification)) {
            setOrientation(VERTICAL);
            addTextView(abaNotificationType);
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
                setPadding(
                        0,
                        (int) getContext().getResources().getDimension(R.dimen.padding25),
                        0,
                        0);
            }
        } else {
            this.setGravity(Gravity.CENTER);
            setOrientation(HORIZONTAL);
            addImageView();
            addTextView(abaNotificationType);
            setBackgroundColor(ContextCompat.getColor(getContext(), R.color.notificationAlertColor));
            setPadding(
                    (int) getContext().getResources().getDimension(R.dimen.padding15),
                    (int) getContext().getResources().getDimension(R.dimen.padding15),
                    (int) getContext().getResources().getDimension(R.dimen.padding15),
                    0);
        }
    }

    private void addImageView() {
        alertIcon = new ImageView(getContext());
        alertIcon.setImageResource(R.mipmap.icon_alert);
        addView(alertIcon);
    }

    private void addTextView(ABAErrorNotificationType abaNotificationType) {
        final TypedArray styledAttributes = getContext().getTheme().obtainStyledAttributes(
                new int[]{android.R.attr.actionBarSize});
        int mActionBarSize = (int) styledAttributes.getDimension(0, 0);
        styledAttributes.recycle();

        textView = new ABATextView(getContext());
        textView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        textView.setMinimumHeight(mActionBarSize);
        textView.setGravity(Gravity.CENTER);
        if (abaNotificationType.equals(ABAErrorNotificationType.ErrorNotification))
            textView.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.borderEditTextError));
        textView.setTextColor(ContextCompat.getColor(getContext(), R.color.abaWhite));
        textView.setABATag(getResources().getInteger(R.integer.typefaceSans500));

        addView(textView);
    }

    private void addOptions() {
        optionsLayout = new LinearLayout(getContext());
        optionsLayout.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        optionsLayout.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.abaLightGrey));

        int currentOption = 0;
        for (ABAErrorOption option : options) {
            optionsLayout.addView(addOption(option, currentOption));
            currentOption++;
        }

        addView(optionsLayout);
    }

    private ABATextView addOption(final ABAErrorOption option, int position) {
        final TypedArray styledAttributes = getContext().getTheme().obtainStyledAttributes(
                new int[]{android.R.attr.actionBarSize});
        int mActionBarSize = (int) styledAttributes.getDimension(0, 0);
        styledAttributes.recycle();

        ABATextView optionTextView = new ABATextView(getContext());
        LinearLayout.LayoutParams optionParams = new LayoutParams(0, mActionBarSize);
        optionParams.weight = 1;

        optionTextView.setGravity(Gravity.CENTER);
        optionTextView.setText(option.getOption());
        if (position < options.size() - 1) {
            optionParams.setMargins(0, 0, 2, 0);
        }
        optionTextView.setLayoutParams(optionParams);
        optionTextView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                option.getClickListener().onClick(v);
                dismiss();
            }
        });
        optionTextView.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.abaGrey));
        optionTextView.setTextColor(ContextCompat.getColor(getContext(), R.color.abaWhite));
        optionTextView.setABATag(getResources().getInteger(R.integer.typefaceSans500));
        return optionTextView;
    }

    public void setText(String text) {
        if (textView != null) {
            errorString = text;
            textView.setText(text);
        }
    }

    public void show() {
        setVisibility(GONE);
        FrameLayout rootLayout = (FrameLayout) ((ABAMasterActivity) getContext()).findViewById(android.R.id.content);
        rootLayout.addView(this);
        if (options == null) {
            Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.fade_in);
            setAnimation(animation);
            setVisibility(VISIBLE);
            animation.start();

            postDelayed(new Runnable() {
                @Override
                public void run() {
                    dismiss();
                }
            }, 3000);
        } else {
            setVisibility(VISIBLE);
        }
    }

    public void dismiss() {
        final FrameLayout rootLayout = (FrameLayout) ((ABAMasterActivity) getContext()).findViewById(android.R.id.content);
        if (options == null) {
            Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.fade_out);
            setAnimation(animation);
            setVisibility(GONE);
            animation.start();
            animation.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    rootLayout.removeView(textView);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
        } else {
            setVisibility(GONE);
            rootLayout.removeView(textView);
            rootLayout.removeView(optionsLayout);
        }
    }

    public void setOptions(List<ABAErrorOption> options) {
        this.options = options;
        if (options != null) {
            addOptions();
        }
    }

}