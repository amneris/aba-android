package com.abaenglish.videoclass.presentation.di;

import android.content.Context;

import com.abaenglish.videoclass.ABAApplication;
import com.abaenglish.videoclass.analytics.AnalyticsModule;
import com.abaenglish.videoclass.analytics.TrackerContract;
import com.abaenglish.videoclass.analytics.modern.MixpanelTrackingManager;
import com.abaenglish.videoclass.data.CourseContentService;
import com.abaenglish.videoclass.data.network.oauth.OAuthTokenAccessor;
import com.abaenglish.videoclass.domain.ApplicationConfiguration;
import com.abaenglish.videoclass.domain.abTesting.ServerConfig;
import com.abaenglish.videoclass.domain.abTesting.UnitDetailABTestingConfigurator;
import com.abaenglish.videoclass.presentation.base.ABAMasterActivity;
import com.abaenglish.videoclass.presentation.base.ABAMasterFragment;
import com.abaenglish.videoclass.presentation.base.custom.ABATextView;
import com.abaenglish.videoclass.presentation.base.custom.FontCache;
import com.abaenglish.videoclass.presentation.base.custom.ListenAndRecordControllerView;
import com.abaenglish.videoclass.presentation.profile.ProfileFragmentNew;
import com.abaenglish.videoclass.presentation.section.NextSectionDialog;
import com.abaenglish.videoclass.presentation.tutorial.TutorialActivity;
import com.abaenglish.videoclass.session.SessionService;
import com.nostra13.universalimageloader.core.ImageLoader;

import javax.inject.Singleton;

import dagger.Component;
import io.realm.RealmConfiguration;

/**
 * Created by xilosada on 27/06/16.
 */
@Singleton
@Component(modules = {ApplicationModule.class, AnalyticsModule.class})
public interface ApplicationComponent {
    Context context();

    void inject(TutorialActivity target);

    void inject(ABAApplication target);

    void inject(ABAMasterFragment target);

    void inject(ProfileFragmentNew target);

    void inject(ABAMasterActivity target);

    void inject(MixpanelTrackingManager target);

    void inject(ABATextView target);

    void inject(ListenAndRecordControllerView target);

    void inject(NextSectionDialog target);

    RealmConfiguration realm();

    ImageLoader imageLoader();

    UnitDetailABTestingConfigurator abRouter();

    FontCache fontCache();

    TrackerContract.Tracker tracker();

    ServerConfig abExperiment();

    SessionService sessionService();

    ApplicationConfiguration appConfig();

    OAuthTokenAccessor tokenAccessor();
}