package com.abaenglish.videoclass.presentation.plan;

/**
 * Created by Jesus Espejo using mbp13jesus on 21/03/17.
 */

public interface PlanCellClickListener {
    void clickedItem(int index);
}