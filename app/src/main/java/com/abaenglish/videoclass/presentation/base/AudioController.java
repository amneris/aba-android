package com.abaenglish.videoclass.presentation.base;

import android.content.Context;
import android.content.res.AssetFileDescriptor;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;

import com.abaenglish.videoclass.ABAApplication;
import com.abaenglish.videoclass.data.ConnectionService;
import com.abaenglish.videoclass.data.persistence.ABAPhrase;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.domain.content.GenericController;
import com.bzutils.LogBZ;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by jaume on 27/5/15.
 */
public class AudioController {

    private final int DEFAULT_TIMEOUT = 15000;

    private MediaPlayer mMediaPlayer;
    private MediaRecorder mAudioRecorder;
    private Handler onCompletionHandler;
    private Runnable runnableOnCompletion;

    public PlayerControllerInterface playerControllerListener;

    public enum AudioControllerError {
        kAudioControllerErrorAlreadyPlaying,
        kAudioControllerNotEnoughSpaceError,
        kAudioControllerDownloadError,
        kAudioControllerBadAudioFileError,
        kAudioControllerLibraryFailure
    }

    /**
     * Interface que controla los estados del audio
     */
    public interface PlayerControllerInterface {
        void startListenPhrase(ABAPhrase phrase);

        void phraseListened();

        void startRecordPhrase(ABAPhrase phrase);

        void phraseRecorded();

        void startPhraseCompare(ABAPhrase phrase);

        void phraseCompared();

        void listenError(AudioControllerError error);
    }

    public AudioController() {

    }

    // ================================================================================
    // Public methods - Service layer
    // ================================================================================

   /* public void downloadAllSectionAudios(List<ABAPhrase> phrases, ABAUnit unit,LevelUnitController.DownloadCallback callback) {
        for (ABAPhrase phrase : phrases) {
            if (!checkIfAudioExists(phrase, unit, false))
                DataStore.getInstance().downloadFile(getAudioFileURL(phrase, unit).toString(), getLocalAudioPath(phrase, unit, false));
        }
    } */

    // ================================================================================
    // Public methods - Play
    // ================================================================================

    public void playPhrase(Context aContext, ABAPhrase phrase, ABAUnit unit) {
        notifyStartListenPhrase(phrase);
        MediaPlayer.OnCompletionListener listener = new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                notifyPhraseListened();
            }
        };

        try {

            if (checkIfAudioExists(phrase, unit, false)) {
                playAudioFile(getLocalAudioPath(phrase, unit, false), listener);
            } else {
                Uri uri = Uri.parse(getAudioFileURL(phrase, unit).toString());
                playAudioFile(aContext, uri, phrase, listener);
            }
        } catch (IOException e) {
            notifyListenError(AudioControllerError.kAudioControllerNotEnoughSpaceError);
            e.printStackTrace();
        }
    }

    public void stopAudioController() {
        try {
            if (mAudioRecorder != null) {
                try {
                    mAudioRecorder.stop();
                } catch (RuntimeException stopException) {
                    LogBZ.printStackTrace(stopException);
                    notifyListenError(AudioControllerError.kAudioControllerBadAudioFileError);
                } finally {
                    mAudioRecorder.release();
                    mAudioRecorder = null;
                }
            }

            resetMediaPlayer();

        } catch (IllegalStateException se) {
            LogBZ.printStackTrace(se);
            notifyListenError(AudioControllerError.kAudioControllerBadAudioFileError);
        }
    }

    // ================================================================================
    // Public methods - Record
    // ================================================================================

    public void recordPhrase(final ABAPhrase phrase, final ABAUnit unit) {

        // Check if the file exists and overwrite it
        File phraseFile = new File(getLocalAudioPath(phrase, unit, true));
        if (phraseFile.exists()) {
            phraseFile.delete();
        }
        try {
            AssetFileDescriptor afd = ABAApplication.get().getAssets().openFd("beep.mp3");
            MediaPlayer player = new MediaPlayer();
            player.setDataSource(afd.getFileDescriptor(), afd.getStartOffset(), afd.getLength());
            player.prepare();
            player.start();

            runnableOnCompletion = new Runnable() {
                @Override
                public void run() {
                    try {
                    mAudioRecorder = new MediaRecorder();
                    mAudioRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
                    mAudioRecorder.setOutputFormat(MediaRecorder.OutputFormat.AMR_NB);
                    mAudioRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
                    mAudioRecorder.setOutputFile(getLocalAudioPath(phrase, unit, true));
                    mAudioRecorder.prepare();
                    mAudioRecorder.start();
                    notifyStartRecordingPhrase(phrase);
                    } catch (IOException e) {
                        e.printStackTrace();
                        notifyListenError(AudioControllerError.kAudioControllerNotEnoughSpaceError);
                    } catch (IllegalStateException se) {
                        LogBZ.printStackTrace(se);
                        notifyListenError(AudioControllerError.kAudioControllerBadAudioFileError);
                    } catch (RuntimeException e) {
                        LogBZ.printStackTrace(e);
                        notifyListenError(AudioControllerError.kAudioControllerLibraryFailure);
                    }
                }
            };

            onCompletionHandler = new Handler();
            onCompletionHandler.postDelayed(runnableOnCompletion, 500);
        }
        catch (IOException e) {
            LogBZ.printStackTrace(e);
            notifyListenError(AudioControllerError.kAudioControllerBadAudioFileError);
        }
    }

    public void playRecordedPhrase(final ABAPhrase phrase, final ABAUnit unit, final MediaPlayer.OnCompletionListener listener) throws IOException {
        mMediaPlayer = new MediaPlayer();
        mMediaPlayer.setDataSource(getLocalAudioPath(phrase, unit, true));
        mMediaPlayer.prepare();
        mMediaPlayer.start();
        mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                resetMediaPlayer();
                if (listener != null)
                    listener.onCompletion(mp);
            }
        });
    }

    public void comparePhrases(final Context aContext, final ABAPhrase phrase, final ABAUnit unit) {
        try {
            playRecordedPhrase(phrase, unit, new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    notifyPhraseListened();
                    playPhrase(aContext, phrase, unit);
                    //posible fix for dont play audio after some audio
                    mp.release();
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
            notifyListenError(AudioControllerError.kAudioControllerBadAudioFileError);
        }
    }

    public void stopRecording() {
        try {
            if (mAudioRecorder == null)
                return;
            mAudioRecorder.stop();
            mAudioRecorder.release();
            mAudioRecorder = null;

            notifyPhraseRecorded();
        } catch (RuntimeException e) {
            LogBZ.printStackTrace(e);
            notifyListenError(AudioControllerError.kAudioControllerBadAudioFileError);
        }
    }

    public void stopOnCompletionListener(){
        if(onCompletionHandler != null){
            this.onCompletionHandler.removeCallbacks(runnableOnCompletion);
            onCompletionHandler = null;
        }
    }

    // ================================================================================
    // Public methods - Listeners
    // ================================================================================

    public void addPlayerControllerListener(PlayerControllerInterface listener) {
        playerControllerListener = listener;
    }

    public void removePlayerControllerListener() {
        playerControllerListener = null;
    }

    // ================================================================================
    // Private methods - Play
    // ================================================================================

    private void playAudioFile(String dataSource, final MediaPlayer.OnCompletionListener listener) throws IOException {
        mMediaPlayer = new MediaPlayer();
        mMediaPlayer.setDataSource(dataSource);
        mMediaPlayer.prepare();
        mMediaPlayer.start();
        mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                resetMediaPlayer();
                if (listener != null)
                    listener.onCompletion(mp);
            }
        });
    }

    private void playAudioFile(Context aContext, final Uri datasource, final ABAPhrase phrase, final MediaPlayer.OnCompletionListener listener) throws IOException {
        mMediaPlayer = new MediaPlayer();
        mMediaPlayer.setDataSource(aContext, datasource);
        mMediaPlayer.prepareAsync();

        final Runnable onCompletitionRunneable = new Runnable() {
            @Override
            public void run() {
                resetMediaPlayer();
                if(listener != null)
                    listener.onCompletion(mMediaPlayer);
            }
        };

        final Handler mediaPlayerTimeOut = new Handler();
        mediaPlayerTimeOut.postDelayed(onCompletitionRunneable, DEFAULT_TIMEOUT);

        mMediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
            @Override
            public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
                if(ConnectionService.isNetworkAvailable(ABAApplication.get())) {
                    //Report audio URL Error
                    // MixpanelTrackingManager.getSharedInstance(ABAApplication.get()).trackAudioURLdrop(datasource.toString(), phrase);
                    LogBZ.e("ERROR in this audioURL: " + datasource);
                }
                return false;
            }
        });
        mMediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                if (mMediaPlayer != null) {
                    mediaPlayerTimeOut.removeCallbacks(onCompletitionRunneable);
                    mMediaPlayer.start();
                }
            }
        });
        mMediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                resetMediaPlayer();

                if (listener != null)
                    listener.onCompletion(mp);
            }
        });
    }

    // ================================================================================
    // Private methods - MediaPlayer
    // ================================================================================

    private void resetMediaPlayer() {
        if(mMediaPlayer != null) {
            mMediaPlayer.stop();
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
    }

    // ================================================================================
    // Private methods - Files and URLs
    // ================================================================================

    private static String STATIC_BASE_URL = "http://static.abaenglish.com";

    public URL getAudioFileURL(ABAPhrase phrase, ABAUnit unit) {
        try {
            String idUnit = String.format("%03d", Integer.parseInt(unit.getIdUnit()));
            if (phrase != null) {
                String fileURL = STATIC_BASE_URL + "/audio/course/units/unit" + idUnit + "/audio/" + phrase.getAudioFile() + ".mp3";
                return new URL(fileURL);
            }
            return new URL(STATIC_BASE_URL + "/audio/course/units/unit" + idUnit + "/audio/" + "" + ".mp3");

        } catch (MalformedURLException e) {
            e.printStackTrace();
            notifyListenError(AudioControllerError.kAudioControllerDownloadError);
        }
        return null;
    }

    private boolean checkIfAudioExists(ABAPhrase phrase, ABAUnit unit, boolean forRecording) {
        return new File(getLocalAudioPath(phrase, unit, forRecording)).exists();
    }

    private String getLocalAudioPath(ABAPhrase phrase, ABAUnit unit, boolean forRecording) {
        String extension;
        extension = "mp3";
        if (forRecording)
            extension = "3gp";

        String audioPath = "";
        if (phrase != null)
            audioPath = GenericController.getABAEnglishFolderPath(unit) + "/" + phrase.getAudioFile() + "." + extension;
        return audioPath;
    }

    // ================================================================================
    // Private methods - Notifications
    // ================================================================================

    private void notifyPhraseListened() {
        if(playerControllerListener != null)
            playerControllerListener.phraseListened();
    }

    private void notifyPhraseRecorded() {
        if(playerControllerListener != null)
            playerControllerListener.phraseRecorded();
    }

    private void notifyStartListenPhrase(ABAPhrase phrase) {
        if(playerControllerListener != null)
            playerControllerListener.startListenPhrase(phrase);
    }

    private void notifyStartRecordingPhrase(ABAPhrase phrase) {
        if(playerControllerListener != null)
            playerControllerListener.startRecordPhrase(phrase);
    }

    private void notifyListenError(AudioControllerError error) {
        if(playerControllerListener != null)
            playerControllerListener.listenError(error);
    }

    public void moveAllNotOfflineVersionFolders(){

        String sdPath = Environment.getExternalStorageDirectory().getAbsolutePath();
        for (int i=1; i<=144; i++){
            File sourceDir = new File(sdPath+"/AUDIO"+i);
            if (sourceDir.exists() && sourceDir.isDirectory()){
                try {
                    String[] children = sourceDir.list();
                    for (int j = 0; j < children.length; j++) {
                        File sourceFile = new File(sourceDir, children[j]);
                        File targetFolder = new File(sdPath + "/ABA_English/UNIT" + i);
                        if (!targetFolder.exists()) {
                            targetFolder.mkdirs();
                        }
                        File targetFile = new File(targetFolder, children[j]);
                        sourceFile.renameTo(targetFile);
                    }
                    sourceDir.delete();
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
    }
}
