package com.abaenglish.videoclass.presentation.section.assessment.result.interactor;

import com.abaenglish.videoclass.clean.interactor.SingleShotUseCase;
import com.abaenglish.videoclass.data.persistence.ABALevel;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.domain.content.LevelUnitController;
import com.abaenglish.videoclass.domain.content.UserController;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.Callable;

import javax.inject.Inject;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import rx.Single;

/**
 * Created by xabierlosada on 26/07/16.
 */

public class QualifyAssessmentUseCase implements SingleShotUseCase<QualifyInput, AssessmentResult> {

    private RealmConfiguration realmConfiguration;
    private UserController userController;
    private LevelUnitController levelUnitController;

    @Inject
    public QualifyAssessmentUseCase(RealmConfiguration realm,
                                    UserController userController,
                                    LevelUnitController levelUnitController) {
        this.realmConfiguration = realm;
        this.userController = userController;
        this.levelUnitController = levelUnitController;
    }

    public Single<AssessmentResult> build(final QualifyInput input) {
        return Single.fromCallable(new Callable<AssessmentResult>() {
            @Override
            public AssessmentResult call() throws Exception {
                Realm realm = Realm.getInstance(realmConfiguration);
                ABAUnit unit = levelUnitController.getUnitWithId(realm, String.valueOf(input.unitId()));
                if (unit == null) {
                    throw new UnitNotFoundException();
                }
                ABALevel level = unit.getLevel();
                AssessmentResult.Grade grade = calculateGrade(input.correctAnswers());
                AssessmentResult assessmentResult = AssessmentResult.builder()
                        .isCourseFinished(getCourseIsFinished(realm))
                        .isLastUnitOfLevel(getIsLastUnitOfLevel(unit))
                        .grade(grade)
                        .username(userController.getCurrentUser(realm).getName())
                        .isFirstCompletedUnit(levelUnitController.isFirstCompleteUnit(realm))
                        .levelId(level.getIdLevel())
                        .levelName(level.getName())
                        .numberOfQuestions(unit.getSectionEvaluation().getContent().size())
                        .correctAnswers(input.correctAnswers())
                        .teacherImageUrl(userController.getCurrentUser(realm).getTeacherImage())
                        .nextUnitId(calculateNextUnitId(realm, unit))
                        .unitTitle(unit.getTitle())
                        .unitId(input.unitId())
                        .build();
                realm.close();
                return assessmentResult;
            }
        });
    }

    private AssessmentResult.Grade calculateGrade(int validResponses) {
        if (validResponses == 10) {
            return AssessmentResult.Grade.PERFECT;
        } else if (validResponses > 7) {
            return AssessmentResult.Grade.PASS;
        }
        return AssessmentResult.Grade.FAIL;
    }

    private String calculateNextUnitId(Realm realm, ABAUnit unit) {
        ABALevel level;
        if (getIsLastUnitOfLevel(unit)) {
            level = levelUnitController.getNextLevel(realm, unit.getLevel());
        } else {
            level = unit.getLevel();
        }
        return calculateNextUnitForLevel(unit, level);
    }

    private String calculateNextUnitForLevel(ABAUnit currentUnit, ABALevel level) {
        ArrayList<ABAUnit> units = levelUnitController.getIncompletedUnitsForLevel(level);
        units.remove(currentUnit);
        if (units.size() > 0) {
            Collections.sort(units, new Comparator<ABAUnit>() {
                @Override
                public int compare(ABAUnit lhs, ABAUnit rhs) {
                    return Integer.valueOf(lhs.getIdUnit()) - Integer.valueOf(rhs.getIdUnit());
                }
            });
            return units.get(0).getIdUnit();
        } else {
            return "";
        }
    }

    private boolean getCourseIsFinished(Realm realm) {
        ABALevel finalLevel = levelUnitController.getABALevelWithId(realm, "6");
        ArrayList<ABAUnit> unitList = levelUnitController.getIncompletedUnitsForLevel(finalLevel);

        return finalLevel.isCompleted() || unitList.size() == 0;
    }

    private boolean getIsLastUnitOfLevel(ABAUnit abaUnit) {
        return abaUnit.getLevel().isCompleted()
                || getPendingUnitsInLevel(abaUnit).size() == 0;
    }

    private List<ABAUnit> getPendingUnitsInLevel(ABAUnit currentUnit) {
        ArrayList<ABAUnit> pendingLevels = levelUnitController.getIncompletedUnitsForLevel(currentUnit.getLevel());
        pendingLevels.remove(currentUnit);
        return pendingLevels;

    }

    public static class UnitNotFoundException extends Exception {
    }
}
