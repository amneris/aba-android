package com.abaenglish.videoclass.presentation.tutorial;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;

import com.abaenglish.videoclass.ABAApplication;
import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.data.network.APIManager;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.abaenglish.videoclass.presentation.base.custom.ABATextView;
import com.abaenglish.videoclass.presentation.router.Router;
import com.bzutils.BZUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by vmadalin on 9/3/16.
 * Refactored by xilosada on 13/7/16
 */
public class TutorialActivity extends AppCompatActivity implements TutorialView {

    @BindView(R.id.startABTestViewPager) ViewPager startViewPager;
    @BindView(R.id.loginTextView) ABATextView loginTextView;
    @BindView(R.id.tutorialIndicator1) ImageView indicator1;
    @BindView(R.id.tutorialIndicator2) ImageView indicator2;
    @BindView(R.id.tutorialIndicator3) ImageView indicator3;

    @Inject Router router;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setScreenOrientation();
        setContentView(R.layout.activity_tutorial);
        ABAApplication.get().getApplicationComponent().inject(this);
        ButterKnife.bind(this);
        configViews();
    }

    private void configViews() {
        DataStore.getInstance().setCurrentActivity(this);
        loginTextView.setText(Html.fromHtml(getString(R.string.funnelABtestLoginTextButton) + "<font color='#07BCE6'> " + getString(R.string.funnelABtestLoginJoinTextButton) + "</font>"));

        TutorialPagerAdapter tutorialPagerAdapter = new TutorialPagerAdapter(this);
        startViewPager.setAdapter(tutorialPagerAdapter);
        startViewPager.addOnPageChangeListener(new PageListener());
        startViewPager.setPageTransformer(true, new ViewPager.PageTransformer() {
            @Override
            public void transformPage(View page, float position) {
                View backgroundView = page.findViewById(R.id.tutorialItemBackground);
                if (backgroundView != null) {
                    backgroundView.setTranslationX(-(position * BZUtils.getScreenWidht(TutorialActivity.this) * 0.8f));
                }
            }
        });
    }

    @Override @OnClick(R.id.loginTextView)
    public void onLoginClicked() {
        router.route(Router.ROUTE_TO_LOGIN, this);
    }

    @Override @OnClick(R.id.registerButton)
    public void onRegisterClicked() {
        router.route(Router.ROUTE_TO_REGISTER, this);
    }

    private class PageListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageSelected(int position) {
            if (position == 0) {
                indicator1.setSelected(false);
                indicator2.setSelected(false);
                indicator3.setSelected(false);
            } else {
                indicator1.setSelected(position != 1);
                indicator2.setSelected(position != 2);
                indicator3.setSelected(position != 3);
            }
        }

        @Override public void onPageScrollStateChanged(int state) {}
        @Override public void onPageScrolled(int pos, float posOffset, int posOffsetPixels) {}
    }

    protected void setScreenOrientation() {
        ///Force portrait mode for phones
        if (!APIManager.isTablet(this)) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
    }
}