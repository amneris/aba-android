package com.abaenglish.videoclass.presentation.section.assessment.result;

import com.abaenglish.videoclass.clean.CleanPresenter;
import com.abaenglish.videoclass.clean.Router;

/**
 * Created by xabierlosada on 26/07/16.
 */
public abstract class AssessmentResultViewPresenter
        <T extends AssessmentResultView, Z extends Router> extends CleanPresenter<T,Z> {
    abstract void redoAssessment();
    abstract void shareWithFacebook();
    abstract void shareCertificate();
    abstract void shareWithTwitter();
    abstract void confirmResult();
    abstract void goBack();
}
