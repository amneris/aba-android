package com.abaenglish.videoclass.presentation.section.assessment.result;

import android.content.res.Configuration;
import android.os.Bundle;
import android.text.Html;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Space;

import com.abaenglish.videoclass.ABAApplication;
import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.domain.abTesting.UnitDetailABTestingConfigurator;
import com.abaenglish.videoclass.presentation.base.ABAMasterActivity;
import com.abaenglish.videoclass.presentation.base.custom.ABAButton;
import com.abaenglish.videoclass.presentation.base.custom.ABATextView;
import com.abaenglish.videoclass.presentation.base.custom.TeacherBannerView;
import com.abaenglish.videoclass.social.FacebookPublication;
import com.abaenglish.videoclass.social.LinkedinPublication;
import com.abaenglish.videoclass.social.TwitterPublication;
import com.bzutils.images.RoundedImageView;
import com.nostra13.universalimageloader.core.ImageLoader;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.abaenglish.videoclass.presentation.section.SectionActivityInterface.EXTRA_UNIT_ID;

/**
 * Created by madalin on 14/05/15.
 */
public class AssessmentResultActivity extends ABAMasterActivity implements AssessmentResultView {

    public static final String EXTRA_CORRECT_EXERCISE = "EXTRA_CORRECT_EXERCISE";

    @BindView(R.id.repeat_evaluation)
    ABAButton repeatEvaluation;
    @BindView(R.id.continue_nextUnit)
    ABAButton continueNextUnit;
    @BindView(R.id.smileyIcon)
    ImageView smileyIcon;
    @BindView(R.id.correctExercice)
    ABATextView correctTextExercise;
    @BindView(R.id.evaluation_titleText)
    ABATextView evaluationTitleText;
    @BindView(R.id.evaluation_descriptionText)
    ABATextView evaluationDescriptionText;
    @BindView(R.id.facebook_share)
    ImageView facebookIcon;
    @BindView(R.id.twitter_share)
    ImageView twitterIcon;
    @BindView(R.id.pass_evaluation)
    LinearLayout socialFrame;
    @BindView(R.id.linkedin_certificate)
    LinearLayout linkedinButton;
    @BindView(R.id.spaceButtons)
    Space separadorButtons;
    @BindView(R.id.reviseShare)
    ABATextView reviseShareText;
    @BindView(R.id.teacherImg)
    RoundedImageView teacherBannerImage;
    @BindView(R.id.detailUnitTeacherView)
    TeacherBannerView teacherBanner;


    @Inject
    AssessmentResultPresenter presenter;
    @Inject
    ImageLoader imageLoader;
    @Inject
    UnitDetailABTestingConfigurator unitDetailABTestingConfigurator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle extras = getIntent().getExtras();
        String unitId = extras.getString(EXTRA_UNIT_ID);
        int validAnswers = extras.getInt(EXTRA_CORRECT_EXERCISE);

        setContentView(R.layout.activity_evaluation_revise_execise);
        ButterKnife.bind(this);

        getComponent().inject(this);

        ResultRouter router = new ResultRouter(this, new FacebookPublication(this), new TwitterPublication(this), new LinkedinPublication(this), unitDetailABTestingConfigurator);
        presenter.initialize(this, router);

        presenter.processAnswers(unitId, validAnswers);

        configActionBar();
        configViewForTablet7ForPass();
    }

    private void configActionBar() {
        findViewById(R.id.toolbarLeftButton).setOnTouchListener(((ABAApplication) getApplicationContext()).getImagePressedState(null));
        findViewById(R.id.toolbarLeftButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRedoAssessmentClicked();
            }
        });

        ABATextView unitSectionTitle = (ABATextView) findViewById(R.id.toolbarTitle);
        ABATextView unitSectionProgress = (ABATextView) findViewById(R.id.toolbarSubTitle);
        unitSectionTitle.setText(R.string.unitMenuTitle8Key);
        unitSectionProgress.setVisibility(View.GONE);
    }

    @Override
    public void onDestroy() {
        presenter.destroy();
        super.onDestroy();
    }

    @OnClick(R.id.twitter_share)
    public void onTwitterShareClicked() {
        presenter.shareWithTwitter();
    }

    @OnClick(R.id.linkedin_certificate)
    public void onLinkedinShareClicked() {
        presenter.shareCertificate();
    }

    @OnClick(R.id.facebook_share)
    public void onFacebookShareClicked() {
        presenter.shareWithFacebook();
    }

    @OnClick(R.id.repeat_evaluation)
    public void onRedoAssessmentClicked() {
        presenter.redoAssessment();
    }

    @OnClick(R.id.continue_nextUnit)
    public void onContinueCourseClicked() {
        presenter.confirmResult();
    }

    @Override
    public void renderTeacher(String imageUrl) {
        imageLoader.displayImage(imageUrl, teacherBannerImage);
        teacherBanner.setText(getResources().getString(R.string.evaluationRemindTeacherMessageKey));
        teacherBanner.setVisibility(View.VISIBLE);
    }

    @Override
    public void showNextUnit(String nextUnitId) {
        continueNextUnit.setText(getResources().getString(R.string.evaluationContinuaUnidadButtonKey).toLowerCase() + " " + nextUnitId);
    }

    @Override
    public void showCourseFinished() {
        continueNextUnit.setText("Course finished");
    }

    @Override
    public void renderScore(int correctAnswers, int numberOfQuestions) {
        correctTextExercise.setText(Html.fromHtml(correctAnswers + " " + getResources().getString(R.string.evaluationPreguntaLabelDeKey) + " " +
                "<font color='#FFFFFF'>" + numberOfQuestions + "</font>"));
    }

    @Override
    public void renderFailMessage() {
        smileyIcon.setImageResource(R.mipmap.evaluation_smiley_ko);
        evaluationTitleText.setText(getResources().getString(R.string.evaluationVuelveIntentarloKey));
        evaluationDescriptionText.setText(Html.fromHtml(getResources().getString(R.string.evaluationVuelveIntentarMessage) + " " + "<b>" + "8/10" + "</b>"));
    }

    @Override
    public void renderPassMessage(String unitName) {
        configViewForTablet7ForPass();
        smileyIcon.setImageResource(R.mipmap.evaluation_smiley_ok);
        evaluationTitleText.setText(getResources().getString(R.string.evaluationHasAprobadoKey));
        evaluationDescriptionText.setText(getResources().getString(R.string.evaluationMessageMidOK1Key).toLowerCase() + " "
                + unitName + " ");
        separadorButtons.setVisibility(View.VISIBLE);
    }

    @Override
    public void renderPerfectMessage(String unitName) {
        smileyIcon.setImageResource(R.mipmap.evaluation_smiley_ok);
        evaluationTitleText.setText(getResources().getString(R.string.evaluationFelicidadesKey));
        evaluationDescriptionText.setText(getResources().getString(R.string.evaluationMessageOK1Key) + " "
                + " " + unitName + " " + getResources().getString(R.string.evaluationMessageOK2Key));
    }

    @Override
    public void hideSocial() {
        socialFrame.setVisibility(View.GONE);
    }

    @Override
    public void hideNextUnit() {
        continueNextUnit.setVisibility(View.GONE);
    }

    @Override
    public void showRepeatMistakes() {
        repeatEvaluation.setVisibility(View.VISIBLE);
        repeatEvaluation.setText(getString(R.string.evaluationReviewErrorsKey));
    }

    @Override
    public void showRepeatEvaluation() {
        repeatEvaluation.setVisibility(View.VISIBLE);
        repeatEvaluation.setText(getString(R.string.evaluationRepiteButtonKey));
    }

    @Override
    public void showLinkedinShare() {
        facebookIcon.setVisibility(View.GONE);
        twitterIcon.setVisibility(View.GONE);
        linkedinButton.setVisibility(View.VISIBLE);
        reviseShareText.setText(getString(R.string.evaluationUpdateYourCVLinkedin));
    }

    public void onBackPressed() {
        presenter.goBack();
        super.onBackPressed();
    }

    private void configViewForTablet7ForPass() {

        Configuration config = getResources().getConfiguration();

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE &&
                config.smallestScreenWidthDp >= 600 && config.smallestScreenWidthDp < 650) {
            socialFrame.setOrientation(LinearLayout.HORIZONTAL);
            socialFrame.setGravity(Gravity.CENTER);
            reviseShareText.setText(getResources().getString(R.string.evaluationComparteKey) + "    ");
        }
    }

    protected AssessmentResultComponent getComponent() {
        return ((ABAApplication) getApplication()).getAssessmentResultComponent();
    }
}
