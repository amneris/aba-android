package com.abaenglish.videoclass.presentation.section.write;

import android.app.FragmentManager;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.abaenglish.shepherd.ABAShepherdEditor;
import com.abaenglish.shepherd.plugin.plugins.ShepherdAutomatorPlugin;
import com.abaenglish.videoclass.ABAApplication;
import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.analytics.cooladata.navigation.CooladataNavigationTrackingManager;
import com.abaenglish.videoclass.analytics.cooladata.study.CooladataStudyTrackingManager;
import com.abaenglish.videoclass.data.persistence.ABAPhrase;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.data.persistence.ABAUser;
import com.abaenglish.videoclass.data.persistence.ABAWrite;
import com.abaenglish.videoclass.domain.content.SolutionTextController;
import com.abaenglish.videoclass.domain.content.UserController;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.abaenglish.videoclass.media.TonePlayer;
import com.abaenglish.videoclass.presentation.base.ABAMasterSectionActivity;
import com.abaenglish.videoclass.presentation.base.AudioController;
import com.abaenglish.videoclass.presentation.base.custom.ABAEditText;
import com.abaenglish.videoclass.presentation.base.custom.ABATextView;
import com.abaenglish.videoclass.presentation.base.custom.FontCache;
import com.abaenglish.videoclass.presentation.base.custom.TeacherBannerView;
import com.abaenglish.videoclass.presentation.section.NextSectionDialog_;
import com.abaenglish.videoclass.presentation.section.SectionActivityInterface;
import com.abaenglish.videoclass.presentation.section.SectionHelper;
import com.abaenglish.videoclass.presentation.section.SectionType;
import com.bzutils.BZScreenHelper;
import com.crashlytics.android.Crashlytics;

import java.util.HashMap;

import static com.abaenglish.videoclass.analytics.cooladata.CooladataTrackingDefinitions.CooladataSectionType.ABAWrite;

/**
 * Created by madalin on 7/05/15.
 * Edited by Jesus with regard of Cooladata on 25/04/16
 */
public class ABAWriteActivity extends ABAMasterSectionActivity implements Animation.AnimationListener, SectionActivityInterface, View.OnClickListener, AudioController.PlayerControllerInterface {

    private static final int MAX_CHARS_IN_EDIT_TEXT = 150;

    private ABAUnit currentUnit;
    private ABAUser currentUser;
    private ABAWrite writeSection;

    private ABAEditText writeEditText;
    private FloatingActionButton listenButton;
    private ABATextView checkButton;
    private RelativeLayout helpButton;
    private ABATextView helpBar;
    private Animation slideInFromRight;
    private Animation slideOutFromRight;
    private Animation rotateDegreesLeft90;
    private Animation rotateDegressRight90;
    private Animation listenAnimation;
    private ImageView listenCircleAnimation;
    private boolean wasHelpPressed = false;
    private boolean isAnimationRun = false;
    private boolean isListen = false;
    private Handler handler;
    private Runnable runnable;
    private TextView resultTextView;
    private Animation fadeIn;
    private Animation fadeOutCorrect;
    private Animation fadeOutIncorrect;
    private Runnable runnableNotificationCorrect;
    private Runnable runnableNotificationIncorrect;
    private TextWatcher standardTextWatcher;
    private TeacherBannerView teacherBanner;
    private TonePlayer correctSoundPlayer;
    private AudioController audioController;

    private enum WriteStatusType {
        kABAWriteInitial(1),
        kABAWriteOK(2),
        kABAWriteKO(3),
        kABAWriteCompleted(4),
        kABAWriteLoadNextQuestion(5);

        private final int value;

        WriteStatusType(final int newValue) {
            value = newValue;
        }

        public int getValue() {
            return value;
        }
    }

    private boolean readOnlyMode = false;
    private int currentIndex = 0;
    private WriteStatusType currentStatus;
    private ABAPhrase currentPhrase;
    private SolutionTextController solutionTextController;

    // ================================================================================
    // Lifecycle
    // ================================================================================

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_write);

        initApp(getIntent().getExtras().getString(EXTRA_UNIT_ID));
        config();
        configActionBar();
        configTeacher();

        newStatus(WriteStatusType.kABAWriteInitial);

        solutionTextController = new SolutionTextController();
        solutionTextController.strictContractForms = true;

        audioController = DataStore.getInstance().getAudioController();
        correctSoundPlayer = new TonePlayer(getApplicationContext(), R.raw.correct);

        trackSection();
    }

    @Override
    public void onResume() {
        super.onResume();
        audioController.addPlayerControllerListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        phraseListened();
        audioController.removePlayerControllerListener();
        audioController.stopAudioController();
        correctSoundPlayer.stop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        correctSoundPlayer.destroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishSection(false);
    }

    // ================================================================================
    // Private methods - Activity configuration
    // ================================================================================

    private void initApp(String UnitID) {
        currentUnit = DataStore.getInstance().getLevelUnitController().getUnitWithId(getRealm(), UnitID);
        currentUser = UserController.getInstance().getCurrentUser(getRealm());
        writeSection = DataStore.getInstance().getWriteController().getSectionForUnit(currentUnit);
    }

    private void config() {

        writeEditText = (ABAEditText) findViewById(R.id.writeEditText);
        listenButton = (FloatingActionButton) findViewById(R.id.listen_button);
        checkButton = (ABATextView) findViewById(R.id.checkButton);
        helpButton = (RelativeLayout) findViewById(R.id.help_button);
        helpBar = (ABATextView) findViewById(R.id.help_bar);
        resultTextView = (TextView) findViewById(R.id.resultTextView);
        teacherBanner = (TeacherBannerView) findViewById(R.id.detailUnitTeacherView);

        fadeIn = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in_evaluation);
        fadeIn.setAnimationListener(this);
        fadeIn.setFillAfter(true);

        fadeOutCorrect = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_out_evaluation);
        fadeOutCorrect.setAnimationListener(this);
        fadeOutCorrect.setFillAfter(true);
        fadeOutIncorrect = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_out_evaluation);
        fadeOutIncorrect.setAnimationListener(this);
        fadeOutCorrect.setFillAfter(true);


        listenCircleAnimation = (ImageView) findViewById(R.id.listenCirclesAnimation);

        slideInFromRight = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_in_from_right);
        slideOutFromRight = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_out_to_right);
        rotateDegreesLeft90 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_degrees90_left);
        rotateDegressRight90 = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_degress90_right);
        listenAnimation = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rec_anim);

        slideInFromRight.setFillAfter(true);
        slideOutFromRight.setFillAfter(true);
        rotateDegreesLeft90.setFillAfter(true);
        rotateDegressRight90.setFillAfter(true);

        slideInFromRight.setAnimationListener(this);
        slideOutFromRight.setAnimationListener(this);
        rotateDegreesLeft90.setAnimationListener(this);

        helpBar.setPivotX(BZScreenHelper.getScreenWidth(this));

        runnable = new Runnable() {
            @Override
            public void run() {
                hideHelpBar();
            }
        };
        runnableNotificationCorrect = new Runnable() {
            @Override
            public void run() {
                hideAlertCorrect();
            }
        };
        runnableNotificationIncorrect = new Runnable() {
            @Override
            public void run() {
                hideAlertIncorrect();
            }
        };

        helpButton.setOnClickListener(this);
        listenButton.setOnClickListener(this);
        checkButton.setOnClickListener(this);

        standardTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) {
                    checkButton.setTextColor(ContextCompat.getColor(ABAWriteActivity.this, R.color.abaLightBlue));
                } else {
                    checkButton.setTextColor(ContextCompat.getColor(ABAWriteActivity.this, R.color.writeHelpBarColor));
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        };

        writeEditText.setTypeface(fontCache.getTypeface(FontCache.ABATypeface.slab700));

        // Setting max filter to overwrite default Android setting
        // From http://stackoverflow.com/questions/3285412/limit-text-length-of-edittext-in-android
        // "After setting the InputFilter the maxLength was no longer observed." @goto10
        writeEditText.setFilters(new InputFilter[]{new InputFilter.LengthFilter(MAX_CHARS_IN_EDIT_TEXT)});
    }

    // ================================================================================
    // Private methods
    // ================================================================================

    private void newStatus(WriteStatusType status) {

        currentStatus = status;
        currentPhrase = getCurrentPhrase();

        if (currentPhrase == null) {
            readOnlyMode = true;
            currentPhrase = getCurrentPhrase();
        } else {
            readOnlyMode = getCurrentPhrase().isDone();
        }

        switch (status) {
            case kABAWriteInitial: {

                setupHelpBar();

                if (readOnlyMode) {

                    Spannable formattedString = new SpannableString(currentPhrase.getText());
                    formattedString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.abaEmailGreen)), 0, currentPhrase.getText().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                    writeEditText.setText(formattedString);
                    writeEditText.setSelection(formattedString.length());
                    writeEditText.removeTextChangedListener(standardTextWatcher);

                    checkButton.setText(getResources().getString(R.string.nextButtonKey));
                    checkButton.setTextColor(ContextCompat.getColor(this, R.color.abaLightBlue));

                    helpButton.setVisibility(View.GONE);

                } else {

                    writeEditText.addTextChangedListener(standardTextWatcher);

                    checkButton.setText(getResources().getString(R.string.doneButtonWriteKey));
                    checkButton.setTextColor(ContextCompat.getColor(this, R.color.writeHelpBarColor));

                    helpButton.setVisibility(View.VISIBLE);

                    if (ABAShepherdEditor.isInternal() && ShepherdAutomatorPlugin.isAutomationEnabled(this)) {

                        Spannable formattedString = new SpannableString(currentPhrase.getText());
                        formattedString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.abaEmailGreen)), 0, currentPhrase.getText().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                        writeEditText.setText(formattedString);
                        writeEditText.setSelection(formattedString.length());

                        ShepherdAutomatorPlugin.automateMethod(this, new ShepherdAutomatorPlugin.ABAShepherdAutomatorPluginAction() {
                            @Override
                            public void automate() {
                                checkButtonPressed();
                            }
                        });
                    }
                }
                break;
            }

            case kABAWriteOK: {
                DataStore.getInstance().getWriteController().setPhraseDone(getRealm(), getCurrentPhrase(), writeSection, true);
                showCorrectAlert();
                correctSoundPlayer.play();
                updateProgressPercentage();

                break;
            }

            case kABAWriteKO: {
                DataStore.getInstance().getWriteController().saveProgressForWriteSection(getRealm(), writeSection, writeSection.getUnit(), getCurrentPhrase(), writeEditText.getText().toString(), false, false);
                showIncorrectAlert();

                break;
            }

            case kABAWriteLoadNextQuestion: {

                if (DataStore.getInstance().getWriteController().isSectionCompleted(writeSection)) {
                    DataStore.getInstance().getWriteController().setCompletedSection(getRealm(), writeSection);
                    if (serverConfig.isImprovedLinearCourseActive() && !NextSectionDialog_.checkNextSectionIsDone(currentUnit.getIdUnit(), 3)) {
                        showNextSectionDialog();
                    } else {
                        finishSection(true);
                    }
                } else {
                    writeEditText.setText("");
                    listenButtonPressed();
                    newStatus(WriteStatusType.kABAWriteInitial);
                }

                break;
            }

            default:
                break;
        }
    }

    private void showCorrectAlert() {
        resultTextView.setVisibility(View.VISIBLE);
        resultTextView.setBackgroundColor(ContextCompat.getColor(this, R.color.abaEmailGreen));
        resultTextView.startAnimation(fadeIn);
        resultTextView.setText(getResources().getString(R.string.sectionExercisesOKKey));
        checkButton.setEnabled(false);
        new Handler().postDelayed(runnableNotificationCorrect, 3000);

        checkButton.setTextColor(ContextCompat.getColor(this, R.color.writeHelpBarColor));
    }

    private void showIncorrectAlert() {
        resultTextView.setVisibility(View.VISIBLE);
        resultTextView.setBackgroundColor(ContextCompat.getColor(this, R.color.borderEditTextError));
        resultTextView.startAnimation(fadeIn);
        resultTextView.setText(getResources().getString(R.string.sectionExercisesKOKey));
        checkButton.setEnabled(false);
        new Handler().postDelayed(runnableNotificationIncorrect, 3000);

        checkButton.setTextColor(ContextCompat.getColor(this, R.color.writeHelpBarColor));
    }

    private void hideAlertCorrect() {
        resultTextView.startAnimation(fadeOutCorrect);
    }

    private void hideAlertIncorrect() {
        resultTextView.startAnimation(fadeOutIncorrect);
    }

    private void setupHelpBar() {
        helpBar.setText(currentPhrase.getText());
    }

    private void changeActionButtonPos() {
        // Test
        Resources r = this.getResources();
        int px = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                90,
                r.getDisplayMetrics()
        );

        RelativeLayout ll = (RelativeLayout) findViewById(R.id.actionButtonView);
        RelativeLayout.LayoutParams head_params = (RelativeLayout.LayoutParams) ll.getLayoutParams();
        head_params.setMargins(0, px, 0, 0);
        ll.setLayoutParams(head_params);
    }

    private void configActionBar() {
        findViewById(R.id.toolbarLeftButton).setOnTouchListener(((ABAApplication) getApplicationContext()).getImagePressedState(null));
        findViewById(R.id.toolbarLeftButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishSection(false);
            }
        });

        updateProgressPercentage();
    }

    private void updateProgressPercentage() {
        ABATextView unitSectionTitle = (ABATextView) findViewById(R.id.toolbarTitle);
        ABATextView unitSectionProgress = (ABATextView) findViewById(R.id.toolbarSubTitle);

        unitSectionTitle.setText(R.string.unitMenuTitle3Key);
        unitSectionProgress.setText(DataStore.getInstance().getWriteController().getPercentageForSection(writeSection));
    }

    private void dismissTeacherViewIfShown() {
        teacherBanner.setOnClickListener(null);
        teacherBanner.dismiss(this);
    }

    private void hideHelpBar() {
        helpButton.startAnimation(rotateDegressRight90);
        helpBar.startAnimation(slideOutFromRight);
        wasHelpPressed = false;
        handler.removeCallbacks(runnable);
    }

    private void helpButtonPressed() {
        if (!isAnimationRun) {
            if (wasHelpPressed == false) {
                showHelpBar();
            } else {
                hideHelpBar();
            }
            isAnimationRun = true;
        }
    }

    private void checkButtonPressed() {

        if (readOnlyMode) {
            if (currentIndex < DataStore.getInstance().getWriteController().getPhrasesDatasourceForWriteSection(writeSection).size() - 1) {
                currentIndex++;
                newStatus(WriteStatusType.kABAWriteInitial);
            } else {
                finishSection(false);
            }
        } else if (solutionTextController.formatSolutionText(writeEditText.getText().toString()).length() > 0) {

            if (wasHelpPressed) {
                handler.removeCallbacks(runnable);
                hideHelpBar();
            }

            boolean isTextCorrect = isInputTextCorrect();
            if (isTextCorrect) {
                newStatus(WriteStatusType.kABAWriteOK);
            } else {
                newStatus(WriteStatusType.kABAWriteKO);
            }

            // Tracking
            CooladataStudyTrackingManager.trackVerifiedText(currentUser.getUserId(), currentUnit.getLevel().getIdLevel(), currentUnit.getIdUnit(), ABAWrite, currentPhrase.getIdPhrase(), isTextCorrect, writeEditText.getText().toString());
            tracker.trackListenedAudio(currentUnit.getLevel().getIdLevel(), currentUnit.getIdUnit(), ABAWrite.toString(), currentPhrase.getIdPhrase());
        }
    }

    private boolean isInputTextCorrect() {

        // First we convert the text following some rules and for maintaining the same coherence (contracted forms, lower/uppercase, etc)
        String answerFormatted = solutionTextController.formatSolutionText(writeEditText.getText().toString());

        // We get a dictionary in which the key is the index of the word in the phrase. The value will be blank if the word is correct.
        HashMap<Integer, String> errorDict = solutionTextController.checkSolutionText(writeEditText.getText().toString(), currentPhrase.getText());

        boolean almostOneWordIsIncorrect = solutionTextController.checkPhrase(answerFormatted, errorDict);
        boolean missingWords = solutionTextController.areThereMissingWords(answerFormatted, errorDict);

        EditText et = (EditText) findViewById(R.id.writeEditText);

        if (almostOneWordIsIncorrect) {
            Spannable textFormatted = solutionTextController.getSpannableString(answerFormatted, errorDict, this);
            et.setText(textFormatted);

            if (!missingWords) {
                et.setSelection(solutionTextController.getFirstIncorrectWordPosition(answerFormatted, errorDict));
            } else {
                et.setSelection(textFormatted.length());
            }
        } else {
            String text;

            if (solutionTextController.mustChangeOriginalText) {
                text = solutionTextController.formatSolutionText(getCurrentPhrase().getText());
            } else {
                text = et.getText().toString();
            }

            Spannable formattedString = new SpannableString(text);
            formattedString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.abaEmailGreen)), 0, text.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            et.setText(formattedString);
            et.setSelection(formattedString.length());
        }

        return !almostOneWordIsIncorrect;
    }

    private void listenButtonPressed() {
        if (!isListen) {
            DataStore.getInstance().getAudioController().playPhrase(this, getCurrentPhrase(), writeSection.getUnit());
        }
    }

    private ABAPhrase getCurrentPhrase() {
        if (readOnlyMode) {
            return DataStore.getInstance().getWriteController().getPhrasesDatasourceForWriteSection(writeSection).get(currentIndex);
        } else {
            return DataStore.getInstance().getWriteController().getCurrentPhrase(writeSection);
        }
    }

    private void showHelpBar() {
        helpButton.startAnimation(rotateDegreesLeft90);
        wasHelpPressed = true;
        handler = new Handler();
        handler.postDelayed(runnable, 4000);

        // Send help click to ABA Analytics
        DataStore.getInstance().getWriteController().saveProgressForWriteSection(getRealm(), writeSection, writeSection.getUnit(), getCurrentPhrase(), "", false, true);

        // Tracking
        CooladataStudyTrackingManager.trackTextSuggestion(currentUser.getUserId(), currentUnit.getLevel().getIdLevel(), currentUnit.getIdUnit(), ABAWrite, currentPhrase.getIdPhrase());
        tracker.trackGotTextSuggestion(currentUnit.getLevel().getIdLevel(), currentUnit.getIdUnit(), ABAWrite.toString(), currentPhrase.getIdPhrase());
    }

    private void showNextSectionDialog() {
        FragmentManager fragmentManager = getFragmentManager();
        NextSectionDialog_ ndf = NextSectionDialog_.newInstance(SectionHelper.SECTION_WRITE, currentUnit.getIdUnit());
        ndf.addListener(this);
        ndf.show(fragmentManager, "Write");
    }

    // ================================================================================
    // SessionActivityInterface
    // ================================================================================

    @Override
    public void configTeacher() {
        if (DataStore.getInstance().getWriteController().getTotalPhraseCompletedForWriteSection(writeSection) == 0) {
            if (DataStore.getInstance().getLevelUnitController().checkIfFileExist(currentUnit, currentUser.getTeacherImage())) {
                DataStore.getInstance().getLevelUnitController().displayImage(null, currentUser.getTeacherImage(), teacherBanner.getImageView());
            } else {
                teacherBanner.setImageUrl(currentUser.getTeacherImage());
            }

            teacherBanner.setText(getString(R.string.sectionWriteTeacherKey));
            teacherBanner.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismissTeacherViewIfShown();
                }
            });
        } else {
            teacherBanner.setVisibility(View.GONE);
        }
    }

    @Override
    public void finishSection(boolean completed) {
        if (completed) {
            unitDetailABTestingConfigurator.startUnitDetailIntent(this, getIntent().getExtras().getString(EXTRA_UNIT_ID), SectionType.kEscribe);
        } else {
            unitDetailABTestingConfigurator.startUnitDetailIntent(this, getIntent().getExtras().getString(EXTRA_UNIT_ID));
        }
        DataStore.getInstance().getAudioController().removePlayerControllerListener();
        DataStore.getInstance().getAudioController().stopAudioController();
        finish();
        overridePendingTransition(R.anim.slide_in_from_left, R.anim.slide_exit_unitanimation2);
    }

    @Override
    public void updateSectionProgress() {
    }

    // ================================================================================
    // OnClickListener
    // ================================================================================

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.help_button:
                helpButtonPressed();
                break;

            case R.id.listen_button:
                dismissTeacherViewIfShown();
                listenButtonPressed();
                break;

            case R.id.checkButton:
                checkButtonPressed();
                break;
        }
    }


    // ================================================================================
    // AnimationListener
    // ================================================================================

    @Override
    public void onAnimationStart(Animation animation) {
        if (animation == slideInFromRight) {
            helpBar.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        if (animation == rotateDegreesLeft90) {
            helpBar.startAnimation(slideInFromRight);
        } else if (animation == slideInFromRight || animation == slideOutFromRight) {
            isAnimationRun = false;
        } else if (animation == fadeOutIncorrect) {
            checkButton.setEnabled(true);
            resultTextView.setVisibility(View.GONE);
            checkButton.setTextColor(ContextCompat.getColor(this, R.color.abaLightBlue));
        } else if (animation == fadeOutCorrect) {
            resultTextView.setVisibility(View.GONE);
            checkButton.setEnabled(true);
            newStatus(WriteStatusType.kABAWriteLoadNextQuestion);
            checkButton.setTextColor(ContextCompat.getColor(this, R.color.abaLightBlue));
        }
    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    // ================================================================================
    // PlayerControlsListener
    // ================================================================================

    @Override
    public void startListenPhrase(ABAPhrase phrase) {
        listenCircleAnimation.setVisibility(View.VISIBLE);
        listenCircleAnimation.startAnimation(listenAnimation);
        isListen = true;
    }

    @Override
    public void phraseListened() {
        listenCircleAnimation.setAnimation(null);
        listenCircleAnimation.setVisibility(View.INVISIBLE);
        isListen = false;

        // Tracking
        CooladataStudyTrackingManager.trackListenedToAudio(currentUser.getUserId(), currentUnit.getLevel().getIdLevel(), currentUnit.getIdUnit(), ABAWrite, currentPhrase.getIdPhrase());
        tracker.trackListenedAudio(currentUnit.getLevel().getIdLevel(), currentUnit.getIdUnit(), ABAWrite.toString(), currentPhrase.getIdPhrase());
    }

    @Override
    public void startRecordPhrase(ABAPhrase phrase) {
        // Nothing
    }

    @Override
    public void phraseRecorded() {
        // Nothing
    }

    @Override
    public void startPhraseCompare(ABAPhrase phrase) {
        // Nothing
    }

    @Override
    public void phraseCompared() {
        // Nothing
    }

    @Override
    public void listenError(AudioController.AudioControllerError error) {

        int errorKey = 0;

        switch (error) {
            case kAudioControllerErrorAlreadyPlaying: {
                errorKey = R.string.audioPlayerAlreadyPlayingErrorKey;
                break;
            }
            case kAudioControllerNotEnoughSpaceError: {
                errorKey = R.string.audioPlayerNotEnoughSpaceErrorKey;
                break;
            }
            case kAudioControllerDownloadError: {
                errorKey = R.string.audioPlayerDownloadErrorKey;
                break;
            }
            case kAudioControllerBadAudioFileError: {
                errorKey = R.string.audioPlayerBadAudioFileErrorKey;
                break;
            }
            case kAudioControllerLibraryFailure: {
                errorKey = R.string.audioPlayerBadAudioFileErrorKey;
                break;
            }
        }

        showABAErrorNotification(getResources().getString(errorKey));
    }

    @Override
    public void goToNext(boolean result) {
        if (result) {
            startActivity(SectionHelper.getNextSectionIntent(this, Integer.parseInt(currentUnit.getIdUnit()), SectionHelper.SECTION_WRITE));
            finish();
        } else {
            finishSection(true);
        }
    }

    // ================================================================================
    // ABAMasterSectionActivity
    // ================================================================================

    protected Object getSection() {
        return writeSection;
    }

    // ================================================================================
    // Private methods - Tracking
    // ================================================================================

    private void trackSection() {
        CooladataNavigationTrackingManager.trackEnteredSection(currentUser.getUserId(), currentUnit.getLevel().getIdLevel(), currentUnit.getIdUnit(), ABAWrite);
        tracker.trackEnteredSection(currentUnit.getLevel().getIdLevel(), currentUnit.getIdUnit(), ABAWrite.toString());
        Crashlytics.log(Log.INFO, "Section", "User opens Section Write");
    }
}