package com.abaenglish.videoclass.media;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;

/**
 * Created by Jesus Espejo using mbp13jesus on 20/03/17.
 * Player that will be used in the ABAMoment. One use only
 */

public class ABAMomentPlayer {

    private MediaPlayer mediaPlayer;
    private boolean audioReady = false;
    private boolean audioError = false;
    private boolean answerSent = false;

    private final String audioUrl;

    public interface ABAMomentPlayerListener {
        void audioFinishedPlaying(boolean success);
    }

    public ABAMomentPlayer(Context aContext, String audioUrl) {
        this.audioUrl = audioUrl;
        prepareAudio(aContext);
    }

    public void playAudio(long timeoutInSeconds, final ABAMomentPlayerListener playerListener) {

        // Setting completion listener to be able to notify the listener
        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                mp.stop();
                mp.reset();
                mp.release();
                if (!answerSent) {
                    playerListener.audioFinishedPlaying(true);
                    answerSent = true;
                }
            }
        });

        // if error downloading the file
        if (audioError) {

            // notify the error to the listener
            if (!answerSent) {
                playerListener.audioFinishedPlaying(false);
                answerSent = true;
            }

        } else {

            // If audio is not ready
            if (!audioReady) {

                // Waiting for the audio with max time given as param
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {

                        // After waiting audio is not ready
                        if (audioReady) {

                            // Playing audio
                            mediaPlayer.start();
                        } else {
                            // After waiting audio is not ready
                            // notify the error to the listener
                            if (!answerSent) {
                                playerListener.audioFinishedPlaying(false);
                                answerSent = true;
                            }
                        }
                    }
                }, timeoutInSeconds * 1000);
            } else {
                // If audio is ready: just play
                mediaPlayer.start();
            }
        }
    }

    // ================================================================================
    // Private methods
    // ================================================================================

    private void prepareAudio(Context aContext) {

        mediaPlayer = new MediaPlayer();
        try {
            mediaPlayer.setDataSource(aContext, Uri.parse(audioUrl));
            mediaPlayer.prepareAsync();
            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    audioReady = true;
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
            audioError = true;
        }
    }

}
