package com.abaenglish.videoclass.media;

import android.content.Context;
import android.media.MediaPlayer;

public class TonePlayer implements MediaPlayer.OnErrorListener {

    private MediaPlayer mMediaPlayer;
    private final Context mContext;
    private final int mResId;

    /**
     * {@link MediaPlayer} wrapper to easily play tones from resources.
     * @param  context  an absolute URL giving the base location of the image
     * @param  resId the location of the image, relative to the url argument
     */
    public TonePlayer(Context context, int resId) {
        //TODO Validate the audio resource
        mResId = resId;
        mMediaPlayer = MediaPlayer.create(context, resId);
        mContext = context;
        mMediaPlayer.setOnErrorListener(this);
    }

    /**
    * Play the tone
    */
    public void play() {
        if (mMediaPlayer != null) {
            mMediaPlayer.start();
        }
    }

    /**
    * Stop the tone if it is being played
    */
    public void stop() {
        if (mMediaPlayer.isPlaying()) {
            mMediaPlayer.stop();
        }
    }

    /**
     * Deallocate all for the gc.
     */
    public void destroy() {
        if (mMediaPlayer != null) {
            stop();
            mMediaPlayer.release();
            mMediaPlayer = null;
        }
    }

    private void loadAudioFile() {
        mMediaPlayer.stop();
        mMediaPlayer.release();
        mMediaPlayer = MediaPlayer.create(mContext, mResId);
    }

    @Override
    public boolean onError(MediaPlayer mp, int what, int extra) {
        loadAudioFile();
        return true;
    }
}