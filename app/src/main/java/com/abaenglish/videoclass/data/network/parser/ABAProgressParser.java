package com.abaenglish.videoclass.data.network.parser;

import com.abaenglish.videoclass.analytics.TrackerContract;
import com.abaenglish.videoclass.analytics.cooladata.progress.CooladataProgressTrackingManager;
import com.abaenglish.videoclass.data.persistence.ABALevel;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.data.persistence.dao.ABALevelDAO;
import com.abaenglish.videoclass.domain.datastore.DataStore;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

/**
 * Created by Jesus Espejo using mbp13jesus on 02/03/16.
 * Class to parse all the progress sent from the server
 */
public class ABAProgressParser {

    // ================================================================================
    // Public methods
    // ================================================================================

    public static void parseUnitProgress(Realm realm, String rawProgress, TrackerContract.Tracker tracker) throws JSONException {
        JSONArray jsonArray = new JSONArray(rawProgress);
        parseUnitProgress(realm, jsonArray, tracker);
    }

    public static void parseUnitProgress(Realm realm, JSONArray jsonArray, TrackerContract.Tracker tracker) throws JSONException {
            for (int i = 0; i < jsonArray.length(); i++) {
            // JSON item at index
            JSONObject jsonObject = jsonArray.getJSONObject(i);

            // storing response
            syncUnit(realm, jsonObject);

            // send response to Cooladata
            CooladataProgressTrackingManager.sendUpdatedProgress(jsonObject, DataStore.getInstance().getUserController().getCurrentUser(realm));
            tracker.trackProgressMap(jsonObject);
        }
    }

    public static void syncLevelsProgress(final Realm realm) throws IllegalStateException {
        List<ABALevel> allLevels = new ArrayList<>(ABALevelDAO.getABALevels(realm));

        for (ABALevel level : allLevels) {
            float levelProgress = 0;

            for (ABAUnit unit : level.getUnits()) {
                levelProgress += unit.getUnitSectionProgress();
            }

            float currentLevelProgress = (levelProgress / (24 * 800)) * 100;
            level.setProgress(currentLevelProgress);

            if (currentLevelProgress >= 100) {
                level.setCompleted(true);
            }
        }
    }

    // ================================================================================
    // Private methods
    // ================================================================================

    private static void syncUnit(Realm realm, JSONObject jsonObject) throws JSONException {

        // They send us units with idUnits 0, 150, etc... So, we must control it
        if (Integer.parseInt(jsonObject.getString("IdUnit")) > 0 && Integer.parseInt(jsonObject.getString("IdUnit")) <= 144) {

            ABAUnit unit = DataStore.getInstance().getLevelUnitController().getUnitWithId(realm, jsonObject.getString("IdUnit"));

            if (unit == null) {
                return;
            }

            float filmProgress = Integer.parseInt(jsonObject.getString("AbaFilmPct"));
            float speakProgress = Integer.parseInt(jsonObject.getString("SpeakPct"));
            float writeProgress = Integer.parseInt(jsonObject.getString("WritePct"));
            float interpretProgress = Integer.parseInt(jsonObject.getString("InterpretPct"));
            float videoclassProgress = Integer.parseInt(jsonObject.getString("VideoClassPct"));
            float exercisesProgress = Integer.parseInt(jsonObject.getString("ExercisesPct"));
            float vocabularyProgress = Integer.parseInt(jsonObject.getString("VocabularyPct"));
            float evaluationProgress = Integer.parseInt(jsonObject.getString("AssessmentPct"));

            float unitSectionProgress = filmProgress + speakProgress + writeProgress + interpretProgress + videoclassProgress + exercisesProgress + vocabularyProgress + evaluationProgress;

            unit.setUnitSectionProgress(unitSectionProgress);

            if (filmProgress > unit.getSectionFilm().getProgress()) {
                unit.getSectionFilm().setProgress(filmProgress);

                if (unit.getSectionFilm().getProgress() >= 100) {
                    unit.getSectionFilm().setCompleted(true);
                }
            }

            if (speakProgress > unit.getSectionSpeak().getProgress()) {
                unit.getSectionSpeak().setProgress(speakProgress);

                if (unit.getSectionSpeak().getProgress() >= 100) {
                    unit.getSectionSpeak().setCompleted(true);
                }
            }

            if (writeProgress > unit.getSectionWrite().getProgress()) {
                unit.getSectionWrite().setProgress(writeProgress);

                if (unit.getSectionWrite().getProgress() >= 100) {
                    unit.getSectionWrite().setCompleted(true);
                }
            }

            if (interpretProgress > unit.getSectionInterpret().getProgress()) {
                unit.getSectionInterpret().setProgress(interpretProgress);

                if (unit.getSectionInterpret().getProgress() >= 100) {
                    unit.getSectionInterpret().setCompleted(true);
                }
            }

            if (videoclassProgress > unit.getSectionVideoClass().getProgress()) {
                unit.getSectionVideoClass().setProgress(videoclassProgress);

                if (unit.getSectionVideoClass().getProgress() >= 100) {
                    unit.getSectionVideoClass().setCompleted(true);
                }
            }

            if (exercisesProgress > unit.getSectionExercises().getProgress()) {
                unit.getSectionExercises().setProgress(exercisesProgress);

                if (unit.getSectionExercises().getProgress() >= 100) {
                    unit.getSectionExercises().setCompleted(true);
                }
            }

            if (vocabularyProgress > unit.getSectionVocabulary().getProgress()) {
                unit.getSectionVocabulary().setProgress(vocabularyProgress);

                if (unit.getSectionVocabulary().getProgress() >= 100) {
                    unit.getSectionVocabulary().setCompleted(true);
                }
            }

            if (evaluationProgress > unit.getSectionEvaluation().getProgress()) {
                unit.getSectionEvaluation().setProgress(evaluationProgress);

                if (unit.getSectionEvaluation().getProgress() >= 100) {
                    unit.getSectionEvaluation().setCompleted(true);
                }
            }

            float totalUnitPct = unitSectionProgress / 8;

            if (totalUnitPct > 100) {
                totalUnitPct = 100;
            }

            if (totalUnitPct > unit.getProgress()) {
                unit.setProgress(totalUnitPct);

                if (totalUnitPct >= 100 && !unit.isCompleted()) {
                    unit.setCompleted(true);
                }
            }
        }
    }
}
