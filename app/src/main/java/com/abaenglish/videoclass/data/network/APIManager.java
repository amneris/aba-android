package com.abaenglish.videoclass.data.network;

import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;
import android.provider.Settings;

import com.abaenglish.shepherd.ABAShepherdEditor;
import com.abaenglish.shepherd.configuration.configurators.abacore.ABACoreShepherdEnvironment;
import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.data.ConnectionService;
import com.abaenglish.videoclass.data.persistence.ABAAPIError;
import com.abaenglish.videoclass.domain.ApplicationConfiguration;
import com.abaenglish.videoclass.domain.LanguageController;
import com.abaenglish.videoclass.domain.ProgressActionThread;
import com.abaenglish.videoclass.domain.content.DataController;
import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bzutils.LogBZ;
import com.crashlytics.android.Crashlytics;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.abaenglish.shepherd.configuration.configurators.GenericShepherdConfigurator.ShepherdConfiguratorType;

/**
 * Created by Marc Güell Segarra on 24/3/15.
 * Refactored by Jesus Espejo on 19/11/2015
 */
public class APIManager {

    private ApplicationConfiguration appConfiguration;
    private Context applicationContext;

    private RequestQueue queue;
    private static APIManager ourInstance;

    public static final int REGULAR_SOCKET_TIMEOUT = 10000; // 10 seconds timeout
    public static final int PROGRESS_SOCKET_TIMEOUT = 20000; // 20 seconds timeout
    public static final int RETRY_POLICY = 1; // Capable of handling one redirect

    // ================================================================================
    // Interfaces
    // ================================================================================

    public interface ABAAPIResponseCallback {
        void onResponse(String response);

        void onError(Exception e);
    }

    // ================================================================================
    // Shared instance
    // ================================================================================

    public static synchronized APIManager getInstance() {
        if (ourInstance == null) {
            ourInstance = new APIManager();
        }
        return ourInstance;
    }

    // ================================================================================
    // Public method - Generic call
    // ================================================================================

    //  public void call(final String URL, int method, Map<String, String> params, final List<NameValuePair> paramsToSign, final Map<String, String> headers, final ABAAPIResponseCallback callback) {
    public void call(final String URL, int method, final APIManagerParameterSigner params, final Map<String, String> headers, final ABAAPIResponseCallback callback) {

        LogBZ.d("APICall: " + URL);
        Crashlytics.setString("Last API Call", URL);

        StringRequest sr = new StringRequest(method, URL, createResponseListener(callback), createErrorListener(callback, URL)) {

            // Overriding some new methods
            @Override
            protected Map<String, String> getParams() {
                return params.getMapWithSignature();
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                params.put("Content-Type", "application/x-www-form-urlencoded");

                if (headers != null) {
                    params.putAll(headers);
                }

                return params;
            }
        };

        RetryPolicy policy = new DefaultRetryPolicy(REGULAR_SOCKET_TIMEOUT, RETRY_POLICY, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        sr.setRetryPolicy(policy);

        queue.add(sr);
    }

    // ================================================================================
    // Public methods
    // ================================================================================

    public void loginWithEmail(String email, String password, ABAAPIResponseCallback callback) {
        APIManagerParameterSigner paramSigner = APIManagerParameterSigner.Empty();
        paramSigner.addParameter("deviceId", Settings.Secure.getString(applicationContext.getContentResolver(), Settings.Secure.ANDROID_ID));
        paramSigner.addParameter("sysOper", Build.VERSION.RELEASE);
        paramSigner.addParameter("deviceName", getDeviceName());
        paramSigner.addParameter("email", email);
        paramSigner.addParameter("password", password);

        String URL = appConfiguration.getApiUrl() + "api/en/apiuser/login";

        call(URL, Request.Method.POST, paramSigner, null, callback);
    }

    public void loginWithToken(String token, ABAAPIResponseCallback callback) {
        String URL = appConfiguration.getApiUrl() + "api/en/apiuser/login";
        Map<String, String> headers = new HashMap<>();
        headers.put("ABA_API_AUTH_TOKEN", token);
        headers.put("DEVICE", "Android");

        call(URL, Request.Method.POST, APIManagerParameterSigner.Empty(), headers, callback);
    }

    public void getUnits(String language, ABAAPIResponseCallback callback) {
        Map<String, String> headers = new HashMap<>();
        headers.put("ABA_API_AUTH_TOKEN", appConfiguration.getUserToken());
        String apiURL = "api/" + language + "/unit";

        String URL = appConfiguration.getApiUrl() + apiURL;

        call(URL, Request.Method.GET, APIManagerParameterSigner.Empty(), headers, callback);
    }

    public void recoverPassword(String email, ABAAPIResponseCallback callback) {
        APIManagerParameterSigner paramSigner = APIManagerParameterSigner.Empty();
        paramSigner.addParameter("email", email);
        paramSigner.addParameter("langEnv", LanguageController.getCurrentLanguage());

        String URL = appConfiguration.getApiUrl() + "apiuser/recoverpassword";

        call(URL, Request.Method.POST, paramSigner, null, callback);
    }

    public void registerUser(final String name, final String email, final String password, final ABAAPIResponseCallback callback) {

        ConnectionService.getInstance().getPublicIP(new ConnectionService.ConnectionServiceCallback() {
            @Override
            public void finished(String ipAddress) {
                APIManagerParameterSigner paramSigner = APIManagerParameterSigner.Empty();

                String deviceId = Settings.Secure.getString(applicationContext.getContentResolver(), Settings.Secure.ANDROID_ID);
                paramSigner.addParameter("deviceId", deviceId);

                String sysOper = Build.VERSION.RELEASE;
                paramSigner.addParameter("sysOper", sysOper);

                String deviceName = getDeviceName();
                paramSigner.addParameter("deviceName", deviceName);

                paramSigner.addParameter("email", email);
                paramSigner.addParameter("password", password);

                String langEnv = LanguageController.getCurrentLanguage();
                paramSigner.addParameter("langEnv", langEnv);

                paramSigner.addParameter("name", name);
                paramSigner.addParameter("surnames", "");

                paramSigner.addParameter("ipMobile", ipAddress);

                String idPartner = applicationContext.getString(R.string.PARTNER_ID);
                paramSigner.addParameter("idPartner", idPartner);

                // TODO: add pending parameters for tracking purposes (partner, sourceList, etc)
                paramSigner.addParameter("idSourceList", "0");

                String deviceTypeSource;

                if (isTablet(applicationContext)) {
                    deviceTypeSource = "t";
                } else {
                    deviceTypeSource = "m";
                }

                paramSigner.addParameter("deviceTypeSource", deviceTypeSource);

                // Performing call
                String URL = appConfiguration.getApiUrl() + "register/register";
                call(URL, Request.Method.POST, paramSigner, null, callback);
            }
        });
    }

    public void registerUserWithFacebook(final String facebookId, final String email, final String name, final String surnames, final String gender, final ABAAPIResponseCallback callback) {

        ConnectionService.getInstance().getPublicIP(new ConnectionService.ConnectionServiceCallback() {
            @Override
            public void finished(String ipAddress) {
                APIManagerParameterSigner paramSigner = APIManagerParameterSigner.Empty();
                paramSigner.addParameter("email", email);
                paramSigner.addParameter("name", name);
                paramSigner.addParameter("surnames", surnames);
                paramSigner.addParameter("fbId", facebookId);
                paramSigner.addParameter("gender", gender);
                paramSigner.addParameter("langEnv", LanguageController.getCurrentLanguage());

                paramSigner.addParameter("deviceId", Settings.Secure.getString(applicationContext.getContentResolver(), Settings.Secure.ANDROID_ID));
                paramSigner.addParameter("sysOper", Build.VERSION.RELEASE);
                paramSigner.addParameter("deviceName", getDeviceName());

                paramSigner.addParameter("ipMobile", ipAddress);

                paramSigner.addParameter("idPartner", applicationContext.getString(R.string.PARTNER_ID));
                paramSigner.addParameter("idSourceList", "0");  //Default value will be changed by a cron in server.

                if (isTablet(applicationContext)) {
                    paramSigner.addParameter("deviceTypeSource", "t");
                } else {
                    paramSigner.addParameter("deviceTypeSource", "m");
                }

                String URL = appConfiguration.getApiUrl() + "register/registerfacebook";
                call(URL, Request.Method.POST, paramSigner, null, callback);
            }
        });
    }

    public void getSectionsContent(String unitId, ABAAPIResponseCallback callback) {
        Map<String, String> headers = new HashMap<>();
        headers.put("ABA_API_AUTH_TOKEN", appConfiguration.getUserToken());
        String apiURL = "api/" + LanguageController.getCurrentLanguage() + "/content/" + unitId;

        String URL = appConfiguration.getApiUrl() + apiURL;

        call(URL, Request.Method.GET, APIManagerParameterSigner.Empty(), headers, callback);
    }

    public void getCourseProgress(final String userId, ABAAPIResponseCallback callback) {
        Map<String, String> headers = new HashMap<>();
        headers.put("ABA_API_AUTH_TOKEN", appConfiguration.getUserToken());
        headers.put("DEVICE", "Android");
        String apiURL = "api/" + LanguageController.getCurrentLanguage() + "/progress/coursesectionprogress/" + userId;

        String URL = appConfiguration.getApiUrl() + apiURL;

        call(URL, Request.Method.GET, APIManagerParameterSigner.Empty(), headers, callback);
    }

    public void getCompletedActions(String userId, String unitId, Date lastChangedDate, ABAAPIResponseCallback callback) {
        Map<String, String> headers = new HashMap<>();
        headers.put("ABA_API_AUTH_TOKEN", appConfiguration.getUserToken());
        headers.put("DEVICE", "Android");

        Date seventy = new Date(0); // 0 for 1970
        long lastChanged;

        if (lastChangedDate == null) {
            lastChanged = seventy.getTime() / 1000;
        } else {
            lastChanged = (lastChangedDate.getTime() - seventy.getTime()) / 1000;
        }

        String apiURL = "api/" + LanguageController.getCurrentLanguage() + "/progress/unitlistcompletedelements/" + userId + "/" + unitId + "/" + Long.toString(lastChanged);
        String URL = appConfiguration.getApiUrl() + apiURL;

        call(URL, Request.Method.GET, APIManagerParameterSigner.Empty(), headers, callback);
    }

    public void setCurrentLevel(final String idLevel, final String idUser, ABAAPIResponseCallback callback) {
        Map<String, String> headers = new HashMap<>();
        headers.put("ABA_API_AUTH_TOKEN", appConfiguration.getUserToken());
        String apiURL = "api/apiuser/updateuserlevel/" + idUser + "/" + idLevel;
        String URL = appConfiguration.getApiUrl() + apiURL;

        call(URL, Request.Method.GET, APIManagerParameterSigner.Empty(), headers, callback);
    }

    public void sendProgressActionsToServer(List<Map<String, Object>> actions, final DataController.ABAAPICallback<JSONArray> callback) {
        try {
            String apiURL = "api/" + LanguageController.getCurrentLanguage() + "/progress/register";
            String URL = appConfiguration.getApiUrl() + apiURL;

            // Sending actions as parameters
            JSONArray paramsJSON = new JSONArray();
            for (Map action : actions) {
                paramsJSON.put(new JSONObject(action));
            }

            Response.Listener<JSONArray> responseListener = new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray jsonObject) {
                    LogBZ.d("Successfully sent accions to server");
                    callback.onSuccess(jsonObject);
                }
            };

            Response.ErrorListener errorListener = new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (error instanceof ParseError) {
                        //When we send NEWSESSION we recive empty response and this is for controll it
                        LogBZ.d("Successfully sent accions to server");
                        callback.onSuccess(new JSONArray());
                    } else {
                        callback.onError(new ABAAPIError("No actions for sending to server"));
                    }
                }
            };

            JsonArrayRequest request = new JsonArrayRequest(Request.Method.POST, URL, paramsJSON, responseListener, errorListener) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("Content-Type", "application/x-www-form-urlencoded");
                    params.put("HTTP_PHP_AUTH_TOKEN", appConfiguration.getUserToken());
                    params.put("ABA_API_AUTH_TOKEN", appConfiguration.getUserToken());
                    params.put("DEVICE", "Android");

                    return params;
                }
            };

            RetryPolicy policy = new DefaultRetryPolicy(PROGRESS_SOCKET_TIMEOUT, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
            request.setRetryPolicy(policy);

            queue.add(request);
            ProgressActionThread.resetDivider();

        } catch (Exception e) {
            Crashlytics.logException(e);

            callback.onError(new ABAAPIError("Oops! There has been an error while sending actions to server: " + e.getMessage()));
            e.printStackTrace();

        } catch (OutOfMemoryError e) {
            Crashlytics.logException(e);

            // Try To see ProgressActionsSize
            try {
                Crashlytics.setString("OutOfMemory Action size: ", String.valueOf(actions.size()));
            } catch (OutOfMemoryError e2) {
                Crashlytics.logException(e2);
            }

            ProgressActionThread.increaseDivider();

            callback.onError(new ABAAPIError("Oops! There has been an OutOfMemoryError while sending actions to server: " + e.getMessage()));
            e.printStackTrace();
        }
    }

    public void fetchPlanContent(String userId, ABAAPIResponseCallback callback) {
        Map<String, String> headers = new HashMap<>();
        headers.put("ABA_API_AUTH_TOKEN", appConfiguration.getUserToken());

        String apiURL = "/api/GetProducts/android/" + userId;
        String URL = appConfiguration.getApiUrl() + apiURL;

        call(URL, Request.Method.GET, APIManagerParameterSigner.Empty(), headers, callback);
    }

    private static final String PLATFORM_NAME = "android";

    public void updateUserToPremiumWithPlan(String userID, String userCountry, String currency, String purchasePeriod, String price, String promocode, String completePurchaseReceipt, String purchaseSignature, ABAAPIResponseCallback callback) {
        Map<String, String> headers = new HashMap<>();
        headers.put("ABA_API_AUTH_TOKEN", appConfiguration.getUserToken());
        String apiUrl = "api/apiuser/freetopremium";

        APIManagerParameterSigner paramSigner = APIManagerParameterSigner.Empty();

        Locale.getDefault().getCountry();

        paramSigner.addParameter("countryId", userCountry);
        paramSigner.addParameter("currency", currency);
        paramSigner.addParameter("periodId", purchasePeriod);
        paramSigner.addParameter("userId", userID);
        paramSigner.addParameter("price", price);
        paramSigner.addParameter("purchaseReceipt", completePurchaseReceipt);
        paramSigner.addParameter("platform", PLATFORM_NAME);
        paramSigner.addParameter("purchaseSignature", purchaseSignature);
        paramSigner.addParameter("promocode", promocode);

        String URL = appConfiguration.getApiUrl() + apiUrl;

        call(URL, Request.Method.POST, paramSigner, headers, callback);
    }

    public void changePassword(String newPassword, ABAAPIResponseCallback callback) {
        Map<String, String> headers = new HashMap<>();
        headers.put("ABA_API_AUTH_TOKEN", appConfiguration.getUserToken());
        headers.put("DEVICE", "Android");
        String apiURL = "apiuser/updateuserpassword";

        APIManagerParameterSigner signer = APIManagerParameterSigner.Empty();
        signer.addParameter("password", newPassword);

        String URL = appConfiguration.getApiUrl() + apiURL;
        call(URL, Request.Method.POST, signer, headers, callback);
    }

    public void downloadSubtitles(String subtitleUrl, ABAAPIResponseCallback callback) {
        call(subtitleUrl, Request.Method.GET, APIManagerParameterSigner.Empty(), null, callback);
    }

    public void lastSupportedVersion(final ABAAPIResponseCallback callback) {
        final String URL = appConfiguration.getApiUrl() + "api/abaEnglishApi/requiredVersion";
        RequestQueue queue = Volley.newRequestQueue(applicationContext);
        StringRequest request = new StringRequest(Request.Method.GET, URL, createResponseListener(callback), createErrorListener(callback, URL));
        queue.add(request);
    }

    // ================================================================================
    // Other public methods
    // ================================================================================

    // TODO: This method must be moved to BZUtils or similar
    public static boolean isTablet(Context context) {
        return context.getResources().getBoolean(R.bool.isTablet);
    }

    public static boolean isTabletSize(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK)
                >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }

    public void getPublicIp(ABAAPIResponseCallback callback) {
        String url = appConfiguration.getApiUrl() + "api/abaEnglishApi/iprecognition";
        call(url, Request.Method.GET, APIManagerParameterSigner.Empty(), null, callback);
    }

    // ================================================================================
    // Private methods
    // ================================================================================

    private static String getDeviceName() {
        return android.os.Build.MANUFACTURER + android.os.Build.PRODUCT + "-" + android.os.Build.MODEL;
    }

    protected static Response.Listener<String> createResponseListener(final ABAAPIResponseCallback callback) {
        return new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (callback != null) {
                    callback.onResponse(response);
                }
            }
        };
    }

    protected static Response.ErrorListener createErrorListener(final ABAAPIResponseCallback callback, final String URL) {
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                LogBZ.e("Error en la llamada: " + URL);
                if (callback != null) {
                    callback.onError(error);
                }
                LogBZ.printStackTrace(error);
            }
        };
    }

    // ================================================================================
    // Private methods - Shepherd
    // ================================================================================

    

    // ================================================================================
    // Accessors
    // ================================================================================

    public void setApplicationContext(Context context, ApplicationConfiguration applicationConfiguration) {
        applicationContext = context.getApplicationContext();
        this.appConfiguration = applicationConfiguration;
        queue = Volley.newRequestQueue(applicationContext);
    }
}


