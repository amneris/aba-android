package com.abaenglish.videoclass.data.persistence;

import android.app.Activity;

import com.abaenglish.videoclass.presentation.base.ABAMasterActivity;
import com.bzutils.BZUtils;

/**
 * Created by iaguila on 26/3/15.
 */
public class ABAAPIError {
    private String error;

    public ABAAPIError (){}
    public ABAAPIError (String error){
        this.error = error;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public void showAlertError (Activity activity){
        BZUtils.showSimpleMessage(activity, error);
    }

    public void showABANotificationError (ABAMasterActivity activity){
        activity.showABAErrorNotification(error);
    }
}
