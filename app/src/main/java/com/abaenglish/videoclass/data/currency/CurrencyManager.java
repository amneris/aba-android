package com.abaenglish.videoclass.data.currency;

import android.content.Context;

import com.abaenglish.videoclass.data.network.APIManager;
import com.bzutils.LogBZ;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Created by Jesus Espejo using mbp13jesus on 08/07/15.
 */
public class CurrencyManager {

    private static List<String> listOfCurrencies = null;

    public static void updateCurrencies(final Context context) {
        CurrencyAPIManager.loadCurrencies(context, getListOfCurrencies(), getTargetCurrency(), new APIManager.ABAAPIResponseCallback() {
            @Override
            public void onResponse(String response) {
                // Do nothing
                LogBZ.d("Currencies updated");
            }

            @Override
            public void onError(Exception e) {
                // Do nothing
                LogBZ.d("Error updating currencies");
            }
        });
    }

    public static double convertAmountToDollars(Context context, double amountToConverse, String toUSDollarFromCurrency) throws NoSuchElementException {
        CurrencyPairModel currencyModelToUSD = currencyPairWithCurrencyCode(context, toUSDollarFromCurrency);
        if (currencyModelToUSD != null) {
            return roundedValue(amountToConverse / Double.parseDouble(currencyModelToUSD.getCurrencyRate()));
        }
        throw new NoSuchElementException("The currency " + toUSDollarFromCurrency + " is not present");
    }

    public static double convertAmountToEuros(Context context, double amountToConverse, String toEURfromCurrency) throws NoSuchElementException {
        CurrencyPairModel currencyModel = currencyPairWithCurrencyCode(context, toEURfromCurrency);
        CurrencyPairModel currencyModelEURtoUSD = currencyPairWithCurrencyCode(context, "EUR");
        if (currencyModel != null && currencyModelEURtoUSD != null) {
            return roundedValue(amountToConverse / Double.parseDouble(currencyModel.getCurrencyRate()) * Double.parseDouble(currencyModelEURtoUSD.getCurrencyRate()));
        }
        throw new NoSuchElementException("The currency " + toEURfromCurrency + " is not present");
    }

    // ================================================================================
    // Private method
    // ================================================================================

    private static CurrencyPairModel currencyPairWithCurrencyCode(Context context, String currencyCode) {
        // Reading from cache.
        List<CurrencyPairModel> listOfCurrencyModel = CurrencyCacheManager.loadCurrenciesFromCache(context);
        if (listOfCurrencyModel == null) {
            return null;
        }

        // Searching proper currency
        for (CurrencyPairModel currencyPairModel : listOfCurrencyModel) {
            if (currencyPairModel.getLatterCurrencyCode().equalsIgnoreCase(currencyCode)) {
                return currencyPairModel;
            }
        }
        return null;
    }

    // ================================================================================
    // Private method - Static definitions
    // ================================================================================

    public static List<String> getListOfCurrencies() {
        if (listOfCurrencies == null) {
            listOfCurrencies = new ArrayList<>();
            listOfCurrencies.add("EUR");
            listOfCurrencies.add("SAR");
            listOfCurrencies.add("AUD");
            listOfCurrencies.add("BOB");
            listOfCurrencies.add("BRL");
            listOfCurrencies.add("BGN");
            listOfCurrencies.add("CAD");
            listOfCurrencies.add("QAR");
            listOfCurrencies.add("CLP");
            listOfCurrencies.add("COP");
            listOfCurrencies.add("KRW");
            listOfCurrencies.add("CRC");
            listOfCurrencies.add("DKK");
            listOfCurrencies.add("EGP");
            listOfCurrencies.add("AED");
            listOfCurrencies.add("USD");
            listOfCurrencies.add("PHP");
            listOfCurrencies.add("HUF");
            listOfCurrencies.add("INR");
            listOfCurrencies.add("IDR");
            listOfCurrencies.add("ILS");
            listOfCurrencies.add("JPY");
            listOfCurrencies.add("KZT");
            listOfCurrencies.add("USD");
            listOfCurrencies.add("LBP");
            listOfCurrencies.add("CHF");
            listOfCurrencies.add("MYR");
            listOfCurrencies.add("MAD");
            listOfCurrencies.add("MXN");
            listOfCurrencies.add("NGN");
            listOfCurrencies.add("NOK");
            listOfCurrencies.add("NZD");
            listOfCurrencies.add("PKR");
            listOfCurrencies.add("PEN");
            listOfCurrencies.add("PLN");
            listOfCurrencies.add("HKD");
            listOfCurrencies.add("GBP");
            listOfCurrencies.add("CZK");
            listOfCurrencies.add("RON");
            listOfCurrencies.add("RUB");
            listOfCurrencies.add("SGD");
            listOfCurrencies.add("ZAR");
            listOfCurrencies.add("SEK");
            listOfCurrencies.add("CHF");
            listOfCurrencies.add("THB");
            listOfCurrencies.add("TWD");
            listOfCurrencies.add("TRY");
            listOfCurrencies.add("UAH");
            listOfCurrencies.add("VND");
        }

        return listOfCurrencies;
    }

    public static String getTargetCurrency() {
        return "USD";
    }

    private static double roundedValue(double value) {
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(2, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
