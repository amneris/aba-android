package com.abaenglish.videoclass.data.persistence.dao;

import com.abaenglish.videoclass.data.persistence.ABAUnit;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by Jesus Espejo using mbp13jesus on 01/03/16.
 * Modified by xilosada on 26/04/16
 */
public class ABAUnitDAO {

    /**
     * Get the {@link List} of all {@link ABAUnit} from the {@link Realm}
     *
     * @param realm The {@link Realm} instance
     */
    public static List<ABAUnit> getABAUnits(final Realm realm) {

        RealmQuery<ABAUnit> query = realm.where(ABAUnit.class);
        RealmResults<ABAUnit> results = query.findAll();

        ArrayList<ABAUnit> units = new ArrayList<>();

        for (ABAUnit u : results) {
            units.add(u);
        }

        return units;
    }

    /**
     * Get an {@link ABAUnit} from the {@link Realm} with the id
     *
     * @param idUnit The {@link String} that identifies the {@link ABAUnit}
     * @param realm The {@link Realm} instance
     */
    public static ABAUnit getUnitWithId(Realm realm, String idUnit) {
        return realm.where(ABAUnit.class).equalTo("idUnit", idUnit).findFirst();
    }

    /**
     * Delete all {@link ABAUnit} records from the {@link Realm}
     *
     * @param realm The {@link Realm} instance with a transaction opened
     */
    public static void deleteUnits(Realm realm) {
        realm.delete(ABAUnit.class);
    }
}
