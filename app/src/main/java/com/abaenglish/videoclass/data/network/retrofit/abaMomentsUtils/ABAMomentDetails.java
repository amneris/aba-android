package com.abaenglish.videoclass.data.network.retrofit.abaMomentsUtils;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Collections;
import java.util.List;

public class ABAMomentDetails {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("momentType")
    @Expose
    private String momentType;

    @SerializedName("titles")
    @Expose
    private List<Title> titles = null;

    @SerializedName("icon")
    @Expose
    private String icon;

    @SerializedName("audio")
    @Expose
    private String audio;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("exercises")
    @Expose
    private List<Exercise> exercises = null;

    // ================================================================================
    // Accessors
    // ================================================================================

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMomentType() {
        return momentType;
    }

    public void setMomentType(String momentType) {
        this.momentType = momentType;
    }

    public List<Title> getTitles() {
        return titles;
    }

    public void setTitles(List<Title> titles) {
        this.titles = titles;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Exercise> getExercises() {
        return exercises;
    }

    public void setExercises(List<Exercise> exercises) {
        this.exercises = exercises;
    }

    @Override
    public String toString() {
        return "ABAMomentDetails{" +
                "id='" + id + '\'' +
                ", momentType='" + momentType + '\'' +
                ", titles=" + titles +
                ", icon='" + icon + '\'' +
                ", audio='" + audio + '\'' +
                ", status='" + status + '\'' +
                ", exercises=" + exercises +
                '}';
    }

    public void shake() {
        Collections.shuffle(exercises);
        for (Exercise exercise : exercises) {
            exercise.shake();
        }
    }
    
    // ================================================================================
    // Private classes
    // ================================================================================

    public class Exercise {

        @SerializedName("id")
        @Expose
        private String id;

        @SerializedName("type")
        @Expose
        private String type;

        @SerializedName("items")
        @Expose
        private List<Item> items = null;

        // ================================================================================
        // Accessors
        // ================================================================================

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public List<Item> getItems() {
            return items;
        }

        public void setItems(List<Item> items) {
            this.items = items;
        }

        void shake() {
            getItems().get(1).setCorrect(true);
            Collections.shuffle(getItems());
            for (int i = 0; i < getItems().size(); i++) {
                if (getItems().get(i).getRole().compareTo("question") == 0) {
                    Collections.swap(getItems(), 0, i);
                    return;
                }
            }
        }

        public String getCorrect() {
            for (Item item : getItems()) {
                if (item.isCorrect()) {
                    return item.getId();
                }
            }
            return "";
        }
    }

    public class Item {

        @SerializedName("id")
        @Expose
        private String id;

        @SerializedName("type")
        @Expose
        private String type;

        @SerializedName("audio")
        @Expose
        private String audio;

        @SerializedName("value")
        @Expose
        private String value;

        @SerializedName("role")
        @Expose
        private String role;

        private boolean isCorrect = false;

        // ================================================================================
        // Accessors
        // ================================================================================

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getAudio() {
            return audio;
        }

        public void setAudio(String audio) {
            this.audio = audio;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }

        public boolean isCorrect() {
            return isCorrect;
        }

        public void setCorrect(boolean correct) {
            isCorrect = correct;
        }
    }

    class Title {

        @SerializedName("language")
        @Expose
        private String language;

        @SerializedName("name")
        @Expose
        private String name;

        // ================================================================================
        // Accessors
        // ================================================================================

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    }
}