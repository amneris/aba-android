package com.abaenglish.videoclass.data.currency;

import android.content.Context;
import android.content.res.AssetManager;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.util.List;

/**
 * Created by Jesus Espejo using mbp13jesus on 08/07/15.
 */
public class CurrencyCacheManager {

    private static final String CACHE_FOLDER = "currencyCache";
    private static final String CACHE_FILE_NAME = "currency_cache.data";
    private static final String ASSETS_HARDCODED_NAME = "currency/currency_hardcoded.data";

    public static List<CurrencyPairModel> loadCurrenciesFromCache(Context context) {
        List<CurrencyPairModel> listOfPairModel = null;

        try {
            listOfPairModel = loadCurrenciesFromStream(new FileInputStream(getCacheFile(context)));
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (listOfPairModel == null) {
            try {
                listOfPairModel = loadCurrenciesFromStream(streamFromHardcodedAssetsFile(context));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return listOfPairModel;
    }

    public static void storeCurrenciesInCache(Context context, List<CurrencyPairModel> listOfPairs) {
        if (listOfPairs != null) {
            try {
                ObjectOutput out = new ObjectOutputStream(new FileOutputStream(getCacheFile(context)));
                out.writeObject(listOfPairs);
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    // ================================================================================
    // Private methods - Read from stream
    // ================================================================================

    public static List<CurrencyPairModel> loadCurrenciesFromStream(InputStream stream) {
        List<CurrencyPairModel> listOfPairModel = null;
        try {
            ObjectInputStream in = new ObjectInputStream(stream);
            listOfPairModel = (List<CurrencyPairModel>) in.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return listOfPairModel;
    }

    // ================================================================================
    // Private methods - Directory
    // ================================================================================

    public static InputStream streamFromHardcodedAssetsFile(Context context) throws IOException {
        AssetManager assetManager = context.getAssets();
        InputStream ims = assetManager.open(ASSETS_HARDCODED_NAME);
        return ims;
    }

    // ================================================================================
    // Private methods - Directory
    // ================================================================================

    private static File getCacheDirectory(Context context) {
        File directory = new File(context.getFilesDir().getAbsolutePath() + File.separator + CACHE_FOLDER);
//        String root = Environment.getExternalStorageDirectory().toString();
//        File directory = new File(root + File.separator + CACHE_FOLDER);
        if (!directory.exists()) {
            directory.mkdirs();
        }
        return directory;
    }

    private static File getCacheFile(Context context) {
        File directory = getCacheDirectory(context);
        return new File(directory.getPath() + File.separator + CACHE_FILE_NAME);
    }
}
