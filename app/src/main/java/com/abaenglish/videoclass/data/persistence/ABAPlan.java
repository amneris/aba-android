package com.abaenglish.videoclass.data.persistence;

import io.realm.RealmObject;
import io.realm.annotations.Required;

/**
 * Created by madalin on 18/09/15.
 */
public class ABAPlan extends RealmObject {
    @Required
    private String planTitle;
    @Required
    private String planPromocode;
    private int planIs2x1;
    @Required
    private String originalIdentifier;
    @Required
    private String discountIdentifier;
    @Required
    private String currency;
    @Required
    private String currencySymbol;
    private float originalPrice;
    private float discountPrice;
    private int days;

    public String getPlanTitle() {
        return planTitle;
    }

    public void setPlanTitle(String planTitle) {
        this.planTitle = planTitle;
    }

    public String getPlanPromocode() {
        return planPromocode;
    }

    public void setPlanPromocode(String planPromocode) {
        this.planPromocode = planPromocode;
    }

    public int getPlanIs2x1() {
        return planIs2x1;
    }

    public void setPlanIs2x1(int planIs2x1) {
        this.planIs2x1 = planIs2x1;
    }

    public String getOriginalIdentifier() {
        return originalIdentifier;
    }

    public void setOriginalIdentifier(String originalIdentifier) {
        this.originalIdentifier = originalIdentifier;
    }

    public String getDiscountIdentifier() {
        return discountIdentifier;
    }

    public void setDiscountIdentifier(String discountIdentifier) {
        this.discountIdentifier = discountIdentifier;
    }

    public float getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(float discountPrice) {
        this.discountPrice = discountPrice;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public float getOriginalPrice() {
        return originalPrice;
    }

    public void setOriginalPrice(float originalPrice) {
        this.originalPrice = originalPrice;
    }

    public int getDays() {
        return days;
    }

    public void setDays(int days) {
        this.days = days;
    }


    public String getCurrencySymbol() {
        return currencySymbol;
    }

    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }
}
