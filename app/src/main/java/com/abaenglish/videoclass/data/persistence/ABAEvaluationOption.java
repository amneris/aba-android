package com.abaenglish.videoclass.data.persistence;

import io.realm.RealmObject;

/**
 * Created by Marc Güell Segarra on 15/4/15.
 */
public class ABAEvaluationOption extends RealmObject {
    private boolean isGood;
    private boolean selected;
    private String text;
    private String optionLetter;
    private ABAEvaluationQuestion evaluationQuestion;

    public boolean isGood() {
        return isGood;
    }

    public void setGood(boolean isGood) {
        this.isGood = isGood;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getOptionLetter() {
        return optionLetter;
    }

    public void setOptionLetter(String optionLetter) {
        this.optionLetter = optionLetter;
    }

    public ABAEvaluationQuestion getEvaluationQuestion() {
        return evaluationQuestion;
    }

    public void setEvaluationQuestion(ABAEvaluationQuestion evaluationQuestion) {
        this.evaluationQuestion = evaluationQuestion;
    }
}
