package com.abaenglish.videoclass.data.currency;

import java.io.Serializable;

/**
 * Created by Jesus Espejo using mbp13jesus on 08/07/15.
 */
public class CurrencyPairModel implements Serializable {

    private static final long serialVersionUID = -6944298906677068991L;

    private String currencyPairCode;
    private String currencyRate;

    private String formerCurrencyCode;
    private String latterCurrencyCode;

    public boolean isProperlySet() {
        if (currencyPairCode == null || currencyPairCode.length() == 0) {
            return false;
        }
        if (formerCurrencyCode == null || formerCurrencyCode.length() == 0) {
            return false;
        }
        if (latterCurrencyCode == null || latterCurrencyCode.length() == 0) {
            return false;
        }
        return true;
    }

    // ================================================================================
    // Modified accessors
    // ================================================================================

    public static String UNKNOWN_CURRENCY_PAIR = "N/A";

    public void setCurrencyPairCode(String currencyPairCode) {

        if (currencyPairCode.equalsIgnoreCase(UNKNOWN_CURRENCY_PAIR)) {
            return;
        } else {
            // Storing currency pair
            this.currencyPairCode = currencyPairCode;

            // Trying to split the pair
            int index = currencyPairCode.indexOf("/");
            if (index != -1) { // index -1 is NotFound
                formerCurrencyCode = currencyPairCode.substring(0, index);

                // Ensuring there is a secondary currency code
                if (index + 1 < currencyPairCode.length()) {
                    latterCurrencyCode = currencyPairCode.substring(index + 1);
                }
            }
        }
    }

    // ================================================================================
    // Accessors
    // ================================================================================

    public String getFormerCurrencyCode() {
        return formerCurrencyCode;
    }

    public String getCurrencyPairCode() {
        return currencyPairCode;
    }

    public String getCurrencyRate() {
        return currencyRate;
    }

    public void setCurrencyRate(String currencyRate) {
        this.currencyRate = currencyRate;
    }

    public String getLatterCurrencyCode() {
        return latterCurrencyCode;
    }
}
