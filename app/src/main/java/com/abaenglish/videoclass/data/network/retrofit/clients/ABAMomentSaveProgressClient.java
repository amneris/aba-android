package com.abaenglish.videoclass.data.network.retrofit.clients;

import com.abaenglish.videoclass.data.network.oauth.OAuthClient;
import com.abaenglish.videoclass.data.network.oauth.OAuthTokenAccessor;
import com.abaenglish.videoclass.data.network.oauth.RetrofitRxServiceFactory;
import com.abaenglish.videoclass.data.network.retrofit.abaMomentsUtils.ABAMomentClientDefinitions.ABAMomentErrorType;
import com.abaenglish.videoclass.data.network.retrofit.abaMomentsUtils.ABAMomentProgressRequest;
import com.abaenglish.videoclass.data.network.retrofit.abaMomentsUtils.ABAMomentProgressResponse;
import com.abaenglish.videoclass.data.network.retrofit.services.ABAMomentService;
import com.abaenglish.videoclass.domain.ApplicationConfiguration;
import com.github.scribejava.core.model.OAuth2AccessToken;

import retrofit2.HttpException;
import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Jesus Espejo using mbp13jesus on 12/04/2017.
 */

public class ABAMomentSaveProgressClient extends OAuthClient<Response<ABAMomentProgressResponse>> {

    private ApplicationConfiguration appConfig;

    public ABAMomentSaveProgressClient(ApplicationConfiguration appConfig, OAuthTokenAccessor refresher) {
        super(refresher);
        this.appConfig = appConfig;
    }

    public void saveMomentProgress(final ABAMomentProgressRequest request, final OnResponseListener<ABAMomentProgressResponse> listener) {

        final AuthorizedAction<Response<ABAMomentProgressResponse>> action = new AuthorizedAction<Response<ABAMomentProgressResponse>>() {

            @Override
            public Observable<Response<ABAMomentProgressResponse>> actionCall(OAuth2AccessToken oAuth2AccessToken) {
                String authToken = TYPE_BEARER + oAuth2AccessToken.getAccessToken();

                final ABAMomentService service = RetrofitRxServiceFactory.createRetrofitRxService(ABAMomentService.class, appConfig.getGatewayUrl());
                return service.registerABAMomentProgress(authToken, request);
            }
        };

        performAuthorisedCall(action)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(new Subscriber<Response<ABAMomentProgressResponse>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        System.out.println("==============================================================");
                        System.out.println("ERROR IN ABAMomentSaveProgressClient : Cause : " + e.getCause() + ", Message : " + e.getMessage() + " class: " + e.getClass());
                        if (e instanceof HttpException) {
                            HttpException httpException = (HttpException) e;
                            System.out.println("ERROR IN ABAMomentSaveProgressClient : response : " + httpException.response() + ", Message : " + httpException.message());
                            switch (httpException.code()) {
                                case 401:
                                    listener.onResponseReceived(null, ABAMomentErrorType.TOKEN_INCORRECT.toString());
                                    break;
                                case 403:
                                    listener.onResponseReceived(null, ABAMomentErrorType.USER_NOT_EXIST.toString());
                                    break;
                                case 404:
                                    listener.onResponseReceived(null, ABAMomentErrorType.MOMENT_NOT_EXIST.toString());
                                    break;
                            }
                        }
                        listener.onResponseReceived(null, ABAMomentErrorType.UNKNOWN.toString());
                    }

                    @Override
                    public void onNext(Response<ABAMomentProgressResponse> abaMomentProgressResponseResponse) {

                        // 204 == not in the ABAMoment user set
                        if (abaMomentProgressResponseResponse.code() == 204) {
                            listener.onResponseReceived(null, ABAMomentErrorType.USER_NOT_EXIST.toString());
                        } else {
                            listener.onResponseReceived(abaMomentProgressResponseResponse.body(), ABAMomentErrorType.NONE.toString());
                        }
                    }

                });
    }
}
