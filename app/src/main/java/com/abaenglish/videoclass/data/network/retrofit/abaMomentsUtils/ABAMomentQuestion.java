package com.abaenglish.videoclass.data.network.retrofit.abaMomentsUtils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Collections;
import java.util.List;

public class ABAMomentQuestion {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("type")
    @Expose
    private Integer type;

    @SerializedName("results")
    @Expose
    private List<Result> results = null;
    @SerializedName("items")
    @Expose
    private List<Item> items = null;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public List<Result> getResults() {
        return results;
    }

    public void setResults(List<Result> results) {
        this.results = results;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        return "ABAMomentQuestion{" +
                "id='" + id + '\'' +
                ", type=" + type +
                ", results=" + results +
                ", items=" + items +
                '}';
    }

    public void shake() {
        Collections.shuffle(getItems());
        for (int i = 0; i < getItems().size(); i++) {
            if (getItems().get(i).getRole().compareTo("question") == 0) {
                Collections.swap(getItems(), 0, i);
                return;
            }
        }
    }

    public class Item {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("audio")
        @Expose
        private String audio;
        @SerializedName("value")
        @Expose
        private String value;
        @SerializedName("role")
        @Expose
        private String role;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getAudio() {
            return audio;
        }

        public void setAudio(String audio) {
            this.audio = audio;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getRole() {
            return role;
        }

        public void setRole(String role) {
            this.role = role;
        }

        @Override
        public String toString() {
            return "Item{" +
                    "id='" + id + '\'' +
                    ", type='" + type + '\'' +
                    ", audio='" + audio + '\'' +
                    ", value='" + value + '\'' +
                    ", role='" + role + '\'' +
                    '}';
        }
    }

    public class Result {

        @SerializedName("id")
        @Expose
        private String id;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        @Override
        public String toString() {
            return "Result{" +
                    "id='" + id + '\'' +
                    '}';
        }
    }

}



