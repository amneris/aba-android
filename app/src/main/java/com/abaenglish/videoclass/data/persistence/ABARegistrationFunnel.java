package com.abaenglish.videoclass.data.persistence;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.RealmClass;

/**
 * Created by vmadalin on 8/3/16.
 * Refactored by Jesus on 23/3/16.
 * Deprecated by xilosada: Right now we are not AB Testing the registration funnel, we are going to
 * maintain this file until the next mandatory migration.
 */
@Deprecated
@RealmClass
public class ABARegistrationFunnel extends RealmObject {

    // ================================================================================
    // Attributes
    // ================================================================================
    private int registrationFunnelTypeId;
    private Date registrationFunnelStartDate;
    private boolean finished;
}
