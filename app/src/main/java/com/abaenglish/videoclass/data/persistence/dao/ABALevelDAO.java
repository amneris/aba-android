package com.abaenglish.videoclass.data.persistence.dao;


import com.abaenglish.videoclass.data.persistence.ABALevel;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by Jesus Espejo using mbp13jesus on 01/03/16.
 * Class to handle all the ABALevel queries
 */
public class ABALevelDAO {

    /**
     * It returns a list with all levels
     *
     * @param realm
     * @return
     * @throws IllegalStateException
     */
    public static List<ABALevel> getABALevels(Realm realm) throws IllegalStateException {
        RealmQuery<ABALevel> query = realm.where(ABALevel.class);
        RealmResults<ABALevel> results = query.findAll();
        return new ArrayList<>(results);
    }

    public static List<ABALevel> getABALevelsDescending(Realm realm) {
        RealmQuery<ABALevel> query = realm.where(ABALevel.class);
        RealmResults<ABALevel> results = query.findAllSorted("idLevel", Sort.DESCENDING);
        return new ArrayList<>(results);
    }

    public static ABALevel getABALevelWithId(Realm realm, String idLevel) {
        RealmQuery<ABALevel> query = realm.where(ABALevel.class);
        ABALevel level = query.equalTo("idLevel", idLevel).findFirst();
        return level;
    }
}
