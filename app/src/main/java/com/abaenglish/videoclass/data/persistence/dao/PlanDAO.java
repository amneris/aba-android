package com.abaenglish.videoclass.data.persistence.dao;


import com.abaenglish.videoclass.data.persistence.ABAPlan;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmQuery;

/**
 * Created by Jesus Espejo using mbp13jesus on 01/03/16.
 * Modified by xilosada on 10/05/16
 */
public class PlanDAO {
    public static List<ABAPlan> getPlans(Realm realm) {
        RealmQuery<ABAPlan> query = realm.where(ABAPlan.class);
        return new ArrayList<>(query.findAll());
    }

    public static void deleteAll(Realm realm) {
        realm.delete(ABAPlan.class);
    }
}
