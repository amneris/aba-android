package com.abaenglish.videoclass.data.network.oauth.custom;


import com.github.scribejava.core.builder.api.DefaultApi20;

/**
 * Created by Julien on 10/03/2017.
 * Api class to use OAuth2.0.
 */

public class CustomABAOAuthApi extends DefaultApi20 {

    private static final String AUTHORIZE_URL_PATH = "/oauth/authorize";
    private static final String ACCESS_TOKEN_ENDPOINT_PATH = "/oauth/token";
    private static final String REFRESH_TOKEN_ENDPOINT_PATH = "/oauth/token";

    private String baseUrl;

    public CustomABAOAuthApi(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    @Override
    public String getAccessTokenEndpoint() {
        return baseUrl + ACCESS_TOKEN_ENDPOINT_PATH;
    }

    @Override
    protected String getAuthorizationBaseUrl() {
        return baseUrl + AUTHORIZE_URL_PATH;
    }

    @Override
    public String getRefreshTokenEndpoint() {
        return baseUrl + REFRESH_TOKEN_ENDPOINT_PATH;
    }
}
