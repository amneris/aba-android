package com.abaenglish.videoclass.data.persistence.migration;

import java.util.ArrayList;
import java.util.Date;

import io.realm.DynamicRealm;
import io.realm.FieldAttribute;
import io.realm.RealmMigration;
import io.realm.RealmObjectSchema;
import io.realm.RealmSchema;

/**
 * Created by madalin on 13/10/15.
 */
public class ABARealmMigration implements RealmMigration {

    // Base version
    public static final String REALM_DEFAULT = "default.realm";
    public static final int REALM_DEFAULT_FILE_SCHEMA = 0;

    // Migration 1
    public static final String REALM_MIGRATION1 = "realm.migration1";
    public static final int REALM_MIGRATION1_FILE_SCHEMA = 1;

    // Migration 2
    public static final String REALM_MIGRATION2 = "realm.migration2";
    public static final int REALM_MIGRATION2_FILE_SCHEMA = 2;

    // Migration 3
    public static final String REALM_MIGRATION3 = "realm.migration3";
    public static final int REALM_MIGRATION3_FILE_SCHEMA = 3;

    @Override
    public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {
        RealmSchema schema = realm.getSchema();

        if (oldVersion == 0) {
            RealmObjectSchema abaUserSchema = schema.get("ABAUser");
            abaUserSchema.setNullable("name", true);
            abaUserSchema.setNullable("surnames", true);
            abaUserSchema.setNullable("country", true);
            abaUserSchema.setNullable("email", true);
            abaUserSchema.setNullable("teacherId", true);
            abaUserSchema.setNullable("teacherImage", true);
            abaUserSchema.setNullable("teacherName", true);
            abaUserSchema.setNullable("token", true);
            abaUserSchema.setNullable("externalKey", true);
            abaUserSchema.setNullable("urlImage", true);
            abaUserSchema.setNullable("userLang", true);
            abaUserSchema.setNullable("userType", true);
            abaUserSchema.setNullable("idSession", true);
            abaUserSchema.setNullable("partnerID", true);
            abaUserSchema.setNullable("sourceID", true);
            abaUserSchema.setNullable("gender", true);
            abaUserSchema.setNullable("phone", true);
            abaUserSchema.setNullable("birthdate", true);
            abaUserSchema.setNullable("expirationDate", true);
            abaUserSchema.setNullable("lastLoginDate", true);

            RealmObjectSchema abaExperimentSchema = schema.create("ABAExperiment");
            abaExperimentSchema.addField("userExperimentVariationId", String.class, FieldAttribute.REQUIRED);
            abaExperimentSchema.addField("experimentVariationIdentifier", String.class, FieldAttribute.REQUIRED);
            abaExperimentSchema.addField("experimentIdentifier", String.class, FieldAttribute.REQUIRED);

            abaUserSchema.addRealmListField("experiment", abaExperimentSchema);

            RealmObjectSchema abaEvaluationOptionSchema = schema.get("ABAEvaluationOption");
            abaEvaluationOptionSchema.setNullable("text", true);
            abaEvaluationOptionSchema.setNullable("optionLetter", true);

            RealmObjectSchema abaEvaluationQuestionSchema = schema.get("ABAEvaluationQuestion");
            abaEvaluationQuestionSchema.setNullable("question", true);

            RealmObjectSchema abaProgressActionSchema = schema.get("ABAProgressAction");
            abaProgressActionSchema.setNullable("action", true);
            abaProgressActionSchema.setNullable("audioID", true);
            abaProgressActionSchema.setNullable("text", true);
            abaProgressActionSchema.setNullable("timestamp", true);
            abaProgressActionSchema.setNullable("ip", true);
            abaProgressActionSchema.setNullable("language", true);
            abaProgressActionSchema.setNullable("unitId", true);
            abaProgressActionSchema.setNullable("userId", true);
            abaProgressActionSchema.setNullable("idSession", true);
            abaProgressActionSchema.setNullable("optionLettersEvaluation", true);

            RealmObjectSchema abaWriteDialogSchema = schema.get("ABAWriteDialog");
            abaWriteDialogSchema.setNullable("role", true);

            RealmObjectSchema abaInterpretRoleSchema = schema.get("ABAInterpretRole");
            abaInterpretRoleSchema.setNullable("imageUrl", true);
            abaInterpretRoleSchema.setNullable("imageBigUrl", true);
            abaInterpretRoleSchema.setNullable("name", true);

            RealmObjectSchema abaFilmSchema = schema.get("ABAFilm");
            abaFilmSchema.setNullable("englishSubtitles", true);
            abaFilmSchema.setNullable("translatedSubtitles", true);

            RealmObjectSchema abaSpeakDialogSchema = schema.get("ABASpeakDialog");
            abaSpeakDialogSchema.setNullable("role", true);

            RealmObjectSchema abaExercisesQuestionSchema = schema.get("ABAExercisesQuestion");
            abaExercisesQuestionSchema.setNullable("exerciseTranslation", true);

            RealmObjectSchema abaExercisesGroupSchema = schema.get("ABAExercisesGroup");
            abaExercisesGroupSchema.setNullable("title", true);

            RealmObjectSchema abaHelpSchema = schema.get("ABAHelp");
            abaHelpSchema.setNullable("idHelp", true);
            abaHelpSchema.setNullable("title", true);
            abaHelpSchema.setNullable("desc", true);

            RealmObjectSchema abaVideoSchema = schema.get("ABAVideoClass");
            abaVideoSchema.setNullable("englishSubtitles", true);
            abaVideoSchema.setNullable("translatedSubtitles", true);
            abaVideoSchema.setNullable("previewImageURL", true);

            RealmObjectSchema abaRoleSchema = schema.get("ABARole");
            abaRoleSchema.setNullable("imageUrl", true);
            abaRoleSchema.setNullable("imageBigUrl", true);
            abaRoleSchema.setNullable("name", true);

            RealmObjectSchema abaUnitSchema = schema.get("ABAUnit");
            abaUnitSchema.setNullable("title", true);
            abaUnitSchema.setNullable("desc", true);
            abaUnitSchema.setNullable("teacherTip", true);
            abaUnitSchema.setNullable("filmImageInactiveUrl", true);
            abaUnitSchema.setNullable("filmImageUrl", true);
            abaUnitSchema.setNullable("unitImage", true);
            abaUnitSchema.setNullable("unitImageInactive", true);
            abaUnitSchema.setNullable("videoClassImageUrl", true);
            abaUnitSchema.setNullable("lastChanged", true);

            RealmObjectSchema abaPhraseSchema = schema.get("ABAPhrase");
            abaPhraseSchema.setNullable("idPhrase", true);
            abaPhraseSchema.setNullable("page", true);
            abaPhraseSchema.setNullable("text", true);
            abaPhraseSchema.setNullable("translation", true);
            abaPhraseSchema.setNullable("serverDate", true);
            abaPhraseSchema.setNullable("speakRole", true);
            abaPhraseSchema.setNullable("wordType", true);

            RealmObjectSchema abaContactSchema = schema.get("ABAContact");
            abaContactSchema.setNullable("email", true);
            oldVersion++;
        }

        if (oldVersion == 1) {
            RealmObjectSchema abaPlanSchema = schema.create("ABAPlan");
            abaPlanSchema.addField("planTitle", String.class, FieldAttribute.REQUIRED);
            abaPlanSchema.addField("planPromocode", String.class, FieldAttribute.REQUIRED);
            abaPlanSchema.addField("planIs2x1", int.class, FieldAttribute.REQUIRED);
            abaPlanSchema.addField("originalIdentifier", String.class, FieldAttribute.REQUIRED);
            abaPlanSchema.addField("discountIdentifier", String.class, FieldAttribute.REQUIRED);
            abaPlanSchema.addField("currency", String.class, FieldAttribute.REQUIRED);
            abaPlanSchema.addField("currencySymbol", String.class, FieldAttribute.REQUIRED);
            abaPlanSchema.addField("originalPrice", float.class, FieldAttribute.REQUIRED);
            abaPlanSchema.addField("discountPrice", float.class, FieldAttribute.REQUIRED);
            abaPlanSchema.addField("days", int.class, FieldAttribute.REQUIRED);

            RealmObjectSchema abaUserSchema = schema.get("ABAUser");
            abaUserSchema.addRealmListField("plans", abaPlanSchema);
            oldVersion++;
        }

        if (oldVersion == 2) {
            RealmObjectSchema abaFunnelSchema = schema.create("ABARegistrationFunnel");
            abaFunnelSchema.addField("registrationFunnelTypeId", int.class);
            abaFunnelSchema.addField("registrationFunnelStartDate", Date.class);
            abaFunnelSchema.addField("finished", boolean.class);
            oldVersion++;
        }
    }

    public static class ABARealm {
        private String name;
        private int version;

        private ABARealm(String name, int version) {
            this.name = name;
            this.version = version;
        }

        public int getVersion() {
            return version;
        }

        public String getName() {
            return name;
        }
    }

    public static ArrayList<ABARealm> getOldRealms() {
        ArrayList<ABARealm> oldListRealms = new ArrayList<>();
        oldListRealms.add(new ABARealm(REALM_DEFAULT, REALM_DEFAULT_FILE_SCHEMA));
        oldListRealms.add(new ABARealm(REALM_MIGRATION1, REALM_MIGRATION1_FILE_SCHEMA));
        oldListRealms.add(new ABARealm(REALM_MIGRATION2, REALM_MIGRATION2_FILE_SCHEMA));
        return oldListRealms;
    }
}
