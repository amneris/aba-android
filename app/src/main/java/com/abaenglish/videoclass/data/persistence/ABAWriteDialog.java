package com.abaenglish.videoclass.data.persistence;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by Marc Güell Segarra on 14/4/15.
 */

public class ABAWriteDialog extends RealmObject {
    private String role;
    private RealmList<ABAPhrase> dialog;
    private ABAWrite writeSection;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public RealmList<ABAPhrase> getDialog() {
        return dialog;
    }

    public void setDialog(RealmList<ABAPhrase> dialog) {
        this.dialog = dialog;
    }

    public ABAWrite getWriteSection() {
        return writeSection;
    }

    public void setWriteSection(ABAWrite writeSection) {
        this.writeSection = writeSection;
    }
}
