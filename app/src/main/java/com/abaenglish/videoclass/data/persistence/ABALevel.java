package com.abaenglish.videoclass.data.persistence;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by Marc Güell Segarra on 25/3/15.
 */
public class ABALevel extends RealmObject {

    @PrimaryKey @Required
    private String          idLevel;
    @Required
    private String          desc;
    @Required
    private String          name;
    private boolean         completed;
    private float           progress;

    private RealmList<ABAUnit> units;

    // TODO: add certificate

    public String getIdLevel() {
        return idLevel;
    }

    public void setIdLevel(String idLevel) {
        this.idLevel = idLevel;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public float getProgress() {
        return progress;
    }

    public void setProgress(float progress) {
        this.progress = progress;
    }

    public RealmList<ABAUnit> getUnits() {
        return units;
    }

    public void setUnits(RealmList<ABAUnit> units) {
        this.units = units;
    }
}
