package com.abaenglish.videoclass.data.network.parser;

import com.crashlytics.android.Crashlytics;

import org.json.JSONObject;

/**
 * Created by Jesus Espejo using mbp13jesus on 11/03/16.
 * Class to parse the IP address from the given json
 */
public class ABAIPParser {

    public static String parseIP(String response) {

        String ip = null;

        try {
            // Parsing response
            JSONObject jObject = new JSONObject(response);
            ip = jObject.get("publicIp").toString();

        } catch (Exception e) {
            Crashlytics.logException(e);
            e.printStackTrace();
        }

        return ip;
    }
}
