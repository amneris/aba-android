package com.abaenglish.videoclass.data.file;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.Looper;

import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.analytics.FirebaseTracker;
import com.abaenglish.videoclass.analytics.TrackerContract;
import com.abaenglish.videoclass.analytics.modern.MixpanelTrackingManager;
import com.abaenglish.videoclass.data.DownloadController;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.abaenglish.videoclass.presentation.base.ABAMasterActivity;
import com.abaenglish.videoclass.presentation.unit.DetailUnitActivity;
import com.bzutils.LogBZ;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import javax.inject.Inject;

import dagger.Component;

public final class DownloadThread extends Thread {

    private Handler handler;
    private int totalCompleted = 0;
    private int totalFilesForDownload;
    private boolean isStartDownloaded = false;
    private String currentUnitId;
    private String currentLevelId;
    private ProgressDialog progressDialog;
    private List<DownloadThreadListener> downloadThreadListeners = new ArrayList<>();

    public DownloadThread() {

    }

    @Override
    public void run() {
        try {
            // preparing a looper on current thread
            // the current thread is being detected implicitly
            Looper.prepare();
            LogBZ.d("DownloadThread entering the loop");
            handler = new Handler();
            Looper.loop();
            LogBZ.d("DownloadThread exiting gracefully");

        } catch (Exception t) {
            LogBZ.printStackTrace(t);
            LogBZ.d("DownloadThread halted due to an error");
            throw t;
        }
    }

    public synchronized void enqueueDownload(final DownloadTask task, final DownloadController.DownloadCallback callback, final CountDownLatch signal) {
        // Wrap DownloadTask into another Runnable to track the statistics
        handler.post(new Runnable() {
            @Override
            public void run() {
                try {
                    if (isStartDownloaded) {
                        LogBZ.d("Start downloading file: " + task.getFileURL());
                        task.run();
                    }
                } catch (Exception e) {
                    hideDownloadDialog();
                    resetTotalCompleted();
                    LogBZ.printStackTrace(e);
                    callback.downloadError();
                } finally {
                    //register task completion
                    synchronized (DownloadThread.this) {
                        if (isStartDownloaded) {
                            totalCompleted++;
                            setProgressDialog(100 * totalCompleted / totalFilesForDownload);
                            downloadCompleted(task.getFileURL());
                            LogBZ.d("Finished downloading: " + task.getFileURL());

                            if (totalFilesForDownload <= totalCompleted) {
                                hideDownloadDialog();
                                resetTotalCompleted();
                                callback.downloadSuccess();
                            }

                            // Only use that on tests
                            if (signal != null) {
                                signal.countDown();
                            }
                        }
                    }
                }
            }
        });
    }

    private void downloadCompleted(String url) {
        for (DownloadThreadListener listener : downloadThreadListeners) {
            listener.downloadCompleted(url);
        }
    }

    public void setDownloadTotalFileForDownload(int files) {
        totalFilesForDownload = files;
    }

    public int getTotalFilesForDownload() {
        return totalFilesForDownload;
    }

    public void addDownloadListener(DownloadThreadListener listener) {
        downloadThreadListeners.add(listener);
    }

    public int getTotalCompletedDownloadFiles() {
        return totalCompleted;
    }

    public void stopThread() {
        isStartDownloaded = false;
    }

    public boolean isDownloadStarted() {
        return isStartDownloaded;
    }

    public void startThread() {
        isStartDownloaded = true;
    }

    public void resetTotalCompleted() {
        totalCompleted = 0;
        isStartDownloaded = false;
    }

    public void showDownloadDialog(final Activity activity, String levelId, String unitID) {
        isStartDownloaded = true;
        currentUnitId = unitID;
        currentLevelId = levelId;

        progressDialog = new ProgressDialog(activity, R.style.material_one_button_alert_dialog);
        progressDialog.setMessage(activity.getResources().getString(R.string.offline_dialog_downloading));
        progressDialog.setOwnerActivity(activity);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setCancelable(true);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, activity.getString(R.string.offline_dialog_cancel).toUpperCase(), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // cancel Download
                stopDownloadProcess(activity);
                dialog.dismiss();
            }
        });
        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                // cancel Download back button
                stopDownloadProcess(activity);
            }
        });
        progressDialog.setProgressNumberFormat(null);
        progressDialog.setProgress(DataStore.getInstance().getDownloadController().getDownloadThread().getTotalCompletedDownloadFiles());
        progressDialog.show();
    }

    public void stopDownloadProcess(Context context) {
        if (DetailUnitActivity.isDownloadStarted) {
            MixpanelTrackingManager.getSharedInstance(context).trackDownloadCancelled(currentUnitId);
            //TODO Kill the Singleton
            FirebaseTracker.getInstance().trackDownloadCancelled(currentLevelId, currentUnitId);
        }

        DetailUnitActivity.isDownloadStarted = false;
        DataStore.getInstance().getDownloadController().getDownloadThread().stopThread();
        DataStore.getInstance().getDownloadController().getDownloadThread().resetTotalCompleted();
        ((ABAMasterActivity) context).showABAErrorNotification(context.getResources().getString(R.string.offlineDownloadStoppedKey));
    }

    public void hideDownloadDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    public void setProgressDialog(int progress) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.setProgress(progress);
        }
    }
}
