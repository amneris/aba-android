package com.abaenglish.videoclass.data.currency;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jesus Espejo using mbp13jesus on 08/07/15.
 */
public class CurrencyPairModelParser {

    private static String RATE_KEY = "rate";
    private static String RATE_NAME_KEY = "Name";
    private static String RATE_RATE_KEY = "Rate";

    public static List<CurrencyPairModel> parseString(String stringToParse) {
        try {
            InputStream stream = new ByteArrayInputStream(stringToParse.getBytes("UTF8"));

            XmlPullParserFactory xmlFactoryObject = XmlPullParserFactory.newInstance();
            XmlPullParser xmlparser = xmlFactoryObject.newPullParser();
            xmlparser.setInput(stream, "UTF8");
            return walkThroughParser(xmlparser);
        } catch (XmlPullParserException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return null;
    }

    private static List<CurrencyPairModel> walkThroughParser(XmlPullParser aParser) {
        List<CurrencyPairModel> listOfCurrenciesModel = new ArrayList<>();
        CurrencyPairModel currentParsingPair = null;
        String parsedText = null;
        try {
            int event = aParser.getEventType();
            // Parsing until document done
            while (event != XmlPullParser.END_DOCUMENT) {

                // Declaring aux variables
                String parsedTag = aParser.getName();

                switch (event) {

                    // Start event
                    case XmlPullParser.START_TAG:
                        if (parsedTag.equals(RATE_KEY)) {
                            currentParsingPair = new CurrencyPairModel();
                        }
                        break;

                    // Text parsed
                    case XmlPullParser.TEXT:
                        parsedText = aParser.getText();
                        break;

                    // End event
                    case XmlPullParser.END_TAG:
                        if (parsedTag.equals(RATE_KEY)) {
                            if (currentParsingPair.isProperlySet()) {
                                listOfCurrenciesModel.add(currentParsingPair);
                            }
                            currentParsingPair = null;
                        } else if (parsedTag.equals(RATE_NAME_KEY)) {
                            currentParsingPair.setCurrencyPairCode(parsedText);
                        } else if (parsedTag.equals(RATE_RATE_KEY)) {
                            currentParsingPair.setCurrencyRate(parsedText);
                        }
                        break;
                }
                event = aParser.next();
            }
        } catch (XmlPullParserException | IOException e) {
            e.printStackTrace();
        }

        return listOfCurrenciesModel;
    }
}
