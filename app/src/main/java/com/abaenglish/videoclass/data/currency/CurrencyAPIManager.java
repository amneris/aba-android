package com.abaenglish.videoclass.data.currency;

import android.content.Context;

import com.abaenglish.videoclass.data.network.APIManager;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.List;

/**
 * Created by Jesus Espejo using mbp13jesus on 08/07/15.
 */
public class CurrencyAPIManager {

    private static String URL_BASE = "http://query.yahooapis.com/v1/public/yql?q=select Name, Rate from yahoo.finance.xchange where pair in (%@)&env=store://datatables.org/alltableswithkeys";

    public static void loadCurrencies(final Context context, List<String> listOfCurrencies, String targetCurrency, final APIManager.ABAAPIResponseCallback callback) {
        String currencySequence = createCurrenciesSequence(listOfCurrencies, targetCurrency);
        String urlWithCurrencies = URL_BASE.replace("%@", currencySequence);
        String escapedUrl = urlWithCurrencies.replace(" ", "%20");

        Response.Listener<String> responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                // Performing parsing in background
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        List<CurrencyPairModel> parsedListOfCurrencies = CurrencyPairModelParser.parseString(response);
                        CurrencyCacheManager.storeCurrenciesInCache(context, parsedListOfCurrencies);
                        callback.onResponse(response);
                    }
                }).start();
            }
        };

        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (callback != null) {
                    callback.onError(error);
                }
            }
        };

        // Creating request
        StringRequest sr = new StringRequest(Request.Method.POST, escapedUrl, responseListener, errorListener);

        // Setting timeout
        RetryPolicy policy = new DefaultRetryPolicy(APIManager.REGULAR_SOCKET_TIMEOUT, APIManager.RETRY_POLICY, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        sr.setRetryPolicy(policy);

        // Performing the call
        Volley.newRequestQueue(context).add(sr);
    }

    // ================================================================================
    // Private methods
    // ================================================================================

    private static String createCurrenciesSequence(List<String> listOfCurrencies, String targetCurrency) {
        String startQuotes = "'";
        String endQuotes = "'";
        String currencyListSequence = createCurreciesPairSequence(listOfCurrencies, targetCurrency);

        return startQuotes + currencyListSequence + endQuotes;
    }

    private static String createCurreciesPairSequence(List<String> listOfCurrencies, String targetCurrency) {
        StringBuilder sequence = new StringBuilder();
        for (String currency : listOfCurrencies) {
            sequence.append(targetCurrency).append(currency);
            if (listOfCurrencies.indexOf(currency) != listOfCurrencies.size() - 1) {
                sequence.append(",");
            }
        }
        return sequence.toString();
    }

}
