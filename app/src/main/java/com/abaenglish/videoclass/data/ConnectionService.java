package com.abaenglish.videoclass.data;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.abaenglish.videoclass.data.network.APIManager;
import com.abaenglish.videoclass.data.network.parser.ABAIPParser;
import com.abaenglish.videoclass.domain.content.ProgressActionController;

/**
 * Created by Jesus Espejo using mbp13jesus on 03/03/16.
 * Class to handle connectivity routines
 */
public class ConnectionService {

    private static volatile ConnectionService instance;
    private static final String NO_IP = "0.0.0.0";

    // ================================================================================
    // Interfaces
    // ================================================================================

    public interface ConnectionServiceCallback {
        void finished(String ipAddress);
    }

    // ================================================================================
    // Static methods
    // ================================================================================

    public static ConnectionService getInstance() {
        if (instance == null) {
            synchronized (ConnectionService.class) {
                if (instance == null) {
                    instance = new ConnectionService();
                }
            }
        }

        return instance;
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static boolean isWifiAvaliable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected() && activeNetworkInfo.getType() == ConnectivityManager.TYPE_WIFI;
    }

    // ================================================================================
    // Instance methods
    // ================================================================================

    /**
     * Method to determine what is the current device IP address
     *
     * @param callback
     */
    public void getPublicIP(final ConnectionServiceCallback callback) {
        APIManager.getInstance().getPublicIp(new APIManager.ABAAPIResponseCallback() {
            @Override
            public void onResponse(String response) {

                // Parsing response
                String ipAddress = ABAIPParser.parseIP(response);
                if (ipAddress != null) {

                    // Setting ip into progress action
                    ProgressActionController.setUserIP(ipAddress);
                } else {
                    ipAddress = NO_IP;
                }

                callback.finished(ipAddress);
            }

            @Override
            public void onError(Exception e) {
                callback.finished(NO_IP);
            }
        });
    }
}
