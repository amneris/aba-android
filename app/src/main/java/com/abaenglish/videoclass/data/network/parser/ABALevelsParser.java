package com.abaenglish.videoclass.data.network.parser;


import com.abaenglish.videoclass.data.persistence.ABALevel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import io.realm.Realm;

/**
 * Created by Jesus Espejo using mbp13jesus on 01/03/16.
 */
public class ABALevelsParser {

    public static void createABALevels(Realm realm, String response) throws JSONException {
        JSONObject json = new JSONObject(response);
        JSONArray levelsJson = json.getJSONArray("levels");

        for (int i = 0; i < levelsJson.length(); i++) {
            JSONObject levelObject = levelsJson.getJSONObject(i);

            final ABALevel level = realm.createObject(ABALevel.class);
            level.setIdLevel(levelObject.getString("idLevel"));
            level.setName(levelObject.getString("name"));
            level.setDesc(levelObject.getString("descKey"));
        }
    }
}
