package com.abaenglish.videoclass.data.persistence;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by Marc Güell Segarra on 25/3/15.
 */
public class ABAUser extends RealmObject {

    @PrimaryKey @Required
    private String          userId;
    private String          name;
    private String          surnames;
    private String          country;
    private String          email;
    private String          teacherId;
    private String          teacherImage;
    private String          teacherName;
    private String          token;
    private String          externalKey;
    private String          urlImage;
    private String          userLang;
    private String          userType;
    private String          idSession;
    private String          partnerID;
    private String          sourceID;
    private String          gender;
    private String          phone;
    private String          birthdate;

    private Date            expirationDate;
    private Date            lastLoginDate;
    private int             subscriptionPeriod;
    private ABALevel        currentLevel;
    private RealmList<ABAUnit> certs;
    private RealmList<ABAExperiment> experiment;
    private RealmList<ABAPlan> plans;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurnames() {
        return surnames;
    }

    public void setSurnames(String surnames) {
        this.surnames = surnames;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(String teacherId) {
        this.teacherId = teacherId;
    }

    public String getTeacherImage() {
        return teacherImage;
    }

    public void setTeacherImage(String teacherImage) {
        this.teacherImage = teacherImage;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public String getUserLang() {
        return userLang;
    }

    public void setUserLang(String userLang) {
        this.userLang = userLang;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public int getSubscriptionPeriod() {
        return subscriptionPeriod;
    }

    public void setSubscriptionPeriod(int subscriptionPeriod) {
        this.subscriptionPeriod = subscriptionPeriod;
    }

    public ABALevel getCurrentLevel() {
        return currentLevel;
    }

    public void setCurrentLevel(ABALevel currentLevel) {
        this.currentLevel = currentLevel;
    }

    public RealmList<ABAUnit> getCerts() {
        return certs;
    }

    public void setCerts(RealmList<ABAUnit> certs) {
        this.certs = certs;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getIdSession() {
        return idSession;
    }

    public void setIdSession(String idSession) {
        this.idSession = idSession;
    }

    public String getPartnerID() {
        return partnerID;
    }

    public void setPartnerID(String partnerID) {
        this.partnerID = partnerID;
    }

    public String getSourceID() {
        return sourceID;
    }

    public void setSourceID(String sourceID) {
        this.sourceID = sourceID;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getLastLoginDate() {
        return lastLoginDate;
    }

    public void setLastLoginDate(Date lastLoginDate) {
        this.lastLoginDate = lastLoginDate;
    }

    public String getExternalKey() {
        return externalKey;
    }

    public void setExternalKey(String externalKey) {
        this.externalKey = externalKey;
    }

    public RealmList<ABAExperiment> getExperiment() {
        return experiment;
    }

    public void setExperiment(RealmList<ABAExperiment> experiment) {
        this.experiment = experiment;
    }

    public RealmList<ABAPlan> getPlans() {
        return plans;
    }

    public void setPlans(RealmList<ABAPlan> plans) {
        this.plans = plans;
    }
}