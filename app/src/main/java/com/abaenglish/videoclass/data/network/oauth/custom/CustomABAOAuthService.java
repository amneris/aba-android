package com.abaenglish.videoclass.data.network.oauth.custom;

import android.util.Base64;

import com.github.scribejava.core.builder.api.DefaultApi20;
import com.github.scribejava.core.model.AbstractRequest;
import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.model.OAuthConfig;
import com.github.scribejava.core.model.OAuthConstants;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.oauth.OAuth20Service;
import com.github.scribejava.core.services.Base64Encoder;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.concurrent.ExecutionException;

/**
 * Created by julien on 16/03/2017.
 * Class to override the creation of the refreshToken query to add an Authorization header.
 */

public class CustomABAOAuthService extends OAuth20Service {

    private static final String GRANT_TYPE_TOKEN = "token";
    private DefaultApi20 api;

    public CustomABAOAuthService(OAuth20Service build) {
        super(build.getApi(), build.getConfig());
        this.api = build.getApi();
    }

    protected OAuthRequest createAccessTokenPasswordGrantRequest(String username, String password) {

        /*
        // Jesus: We should be doing this but parameter OAuthConstants.GRANT_TYPE does not get overwritten

        OAuthRequest request2 = super.createAccessTokenPasswordGrantRequest(username, password);
        request2.addParameter(OAuthConstants.GRANT_TYPE, GRANT_TYPE_TOKEN);
        return request2
        */

        final OAuthRequest request = new OAuthRequest(this.api.getAccessTokenVerb(), this.api.getAccessTokenEndpoint());
        final OAuthConfig config = getConfig();
        request.addParameter(OAuthConstants.USERNAME, username);
        request.addParameter(OAuthConstants.PASSWORD, password);

        final String scope = config.getScope();
        if (scope != null) {
            request.addParameter(OAuthConstants.SCOPE, scope);
        }

        // This is the only line changed if you compare this method to the parent method `super.createAccessTokenPasswordGrantRequest(...)`
        request.addParameter(OAuthConstants.GRANT_TYPE, GRANT_TYPE_TOKEN);

        final String apiKey = config.getApiKey();
        final String apiSecret = config.getApiSecret();
        if (apiKey != null && apiSecret != null) {
            request.addHeader(OAuthConstants.HEADER,
                    OAuthConstants.BASIC + ' '
                            + Base64Encoder.getInstance()
                            .encode(String.format("%s:%s", apiKey, apiSecret).getBytes(Charset.forName("UTF-8"))));
        }

        return request;
    }

    @Override
    protected OAuthRequest createRefreshTokenRequest(String refreshToken) {
        OAuthRequest request =  super.createRefreshTokenRequest(refreshToken);

        final OAuthConfig config = getConfig();
        String userCredentials = config.getApiKey() + ":" + config.getApiSecret();
        String basicAuth = "Basic " + Base64.encodeToString(userCredentials.getBytes(), Base64.DEFAULT);
        request.addHeader("Authorization", basicAuth);

        return request;
    }
}
