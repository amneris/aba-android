package com.abaenglish.videoclass.data.network.oauth;


import com.github.scribejava.core.model.OAuth2AccessToken;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EBean;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by Julien on 10/03/2017.
 * OAuth client.
 */

public class OAuthClient<T> {

    protected static final String TYPE_BEARER = "bearer ";
    private OAuthTokenAccessor refresher;

    public OAuthClient(OAuthTokenAccessor refresher) {
        super();
        this.refresher = refresher;
    }

    // ================================================================================
    // INTERFACE
    // ================================================================================

    public interface OnResponseListener<T> {
        void onResponseReceived(T response, String error);
    }

    public interface AuthorizedAction<T> {
        Observable<T> actionCall(OAuth2AccessToken oAuth2AccessToken);
    }

    // ================================================================================
    // PUBLIC METHODS
    // ================================================================================

    protected Observable<T> performAuthorisedCall(final AuthorizedAction<T> action) {

        // Error action
        final OAuthUnauthorizedErrorRoutine<T> errorRoutine = new OAuthUnauthorizedErrorRoutine<>(new OAuthUnauthorizedErrorRoutine.AuthorizedErrorRoutineAction() {
            @Override
            public Observable errorRoutineCall() {
                return refreshTokenAndProceed(action)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());
            }
        });

        return refresher.getOauth2Token().flatMap(new Func1<OAuth2AccessToken, Observable<T>>() {
            @Override
            public Observable<T> call(OAuth2AccessToken oAuth2AccessToken) {
                return action.actionCall(oAuth2AccessToken)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .onErrorResumeNext(errorRoutine.authorizationErrorRoutine());
            }
        });
    }

    // creating the observable
    private Observable<T> refreshTokenAndProceed(final AuthorizedAction<T> callback) {

        // The authorisation error action is: getting a new token
        OAuthUnauthorizedErrorRoutine<OAuth2AccessToken> errorRoutine = new OAuthUnauthorizedErrorRoutine<>(new OAuthUnauthorizedErrorRoutine.AuthorizedErrorRoutineAction() {
            @Override
            public Observable errorRoutineCall() {
                refresher.reset();
                return refresher.getOauth2Token()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread());
            }
        });

        return refresher.refreshOauth2Token()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(errorRoutine.authorizationErrorRoutine())
                .flatMap(new Func1<OAuth2AccessToken, Observable<T>>() {
                    @Override
                    public Observable<T> call(OAuth2AccessToken oAuth2AccessToken) {
                        return callback.actionCall(oAuth2AccessToken)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread());
                    }
                });
    }
}
