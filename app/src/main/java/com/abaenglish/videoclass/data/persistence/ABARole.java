package com.abaenglish.videoclass.data.persistence;

import io.realm.RealmObject;

/**
 * Created by Marc Güell Segarra on 9/4/15.
 */
public class ABARole extends RealmObject {
    private String          imageUrl;
    private String          imageBigUrl;
    private String          name;
    private ABAUnit         unit;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImageBigUrl() {
        return imageBigUrl;
    }

    public void setImageBigUrl(String imageBigUrl) {
        this.imageBigUrl = imageBigUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ABAUnit getUnit() {
        return unit;
    }

    public void setUnit(ABAUnit unit) {
        this.unit = unit;
    }
}
