package com.abaenglish.videoclass.data;

import android.content.Context;
import android.os.StrictMode;

import com.abaenglish.videoclass.data.file.DownloadTask;
import com.abaenglish.videoclass.data.file.DownloadThread;
import com.abaenglish.videoclass.data.file.DownloadThreadListener;
import com.abaenglish.videoclass.data.persistence.ABALevel;
import com.abaenglish.videoclass.data.persistence.ABAPhrase;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.domain.content.GenericController;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.bzutils.LogBZ;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import io.realm.Realm;

/**
 * Created by Jesus Espejo using mbp13jesus on 15/10/15.
 */
public class DownloadController {

    private DownloadThread downloadThread;

    public interface DownloadCallback {
        void downloadSuccess();

        void downloadError();
    }

    public DownloadController() {
        super();

        downloadThread = new DownloadThread();
        downloadThread.start();
    }

    public boolean isAllFileDownloaded(ABAUnit currentUnit, ArrayList<String> listWithAllUrlFiles) {
        for (String url : listWithAllUrlFiles) {
            if (!GenericController.checkIfFileExist(currentUnit, url)) {
                return false;
            }
        }
        return true;
    }

    public ArrayList<String> getArrayWithDownloadFilesError(ABAUnit currentUnit, ArrayList<String> listWithAllUrlFiles) {
        ArrayList<String> urlFilesForDownload = new ArrayList<>();
        for (String url : listWithAllUrlFiles) {
            if (!GenericController.checkIfFileExist(currentUnit, url)) {
                urlFilesForDownload.add(url);
            }
        }
        return urlFilesForDownload;
    }

    public void downloadSectionContent(Realm realm, Context context, ABAUnit currentUnit, DownloadCallback callback) {
        // download all sectionFiles
        ArrayList<String> totalElementsForDownload = getTotalElementsForDownloadAtUnit(realm, currentUnit);
        setTotalFileForDownload(getAllFilesForDownload(currentUnit, totalElementsForDownload));
        for (String url : totalElementsForDownload) {
            if (url.equals(DataStore.getInstance().getUserController().getCurrentUser(realm).getTeacherImage())) {
                //Download Teacher
                downloadFile(url, GenericController.getLocalFilePath(null, url), callback, null);
            } else {
                if (!GenericController.checkIfFileExist(currentUnit, url)) {
                    //Download other Files
                    downloadFile(url, GenericController.getLocalFilePath(currentUnit, url), callback, null);
                }
            }
        }
    }

    public ArrayList<String> getTotalElementsForDownloadAtUnit(Realm realm, ABAUnit currentUnit) {
        if (currentUnit != null) {

            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);

            ArrayList<String> totalElemetsForDownload = new ArrayList<>();

            //ADD unit audios
            for (ABAPhrase phrase : DataStore.getInstance().getSpeakController().getElementsFromSection(currentUnit.getSectionSpeak())) {
                totalElemetsForDownload.add(DataStore.getInstance().getAudioController().getAudioFileURL(phrase, currentUnit).toString());
            }
            for (ABAPhrase phrase : DataStore.getInstance().getVocabularyController().getElementsFromSection(currentUnit.getSectionVocabulary())) {
                totalElemetsForDownload.add(DataStore.getInstance().getAudioController().getAudioFileURL(phrase, currentUnit).toString());
            }
            for (ABAPhrase phrase : DataStore.getInstance().getWriteController().getPhrasesDatasourceForWriteSection(currentUnit.getSectionWrite())) {
                totalElemetsForDownload.add(DataStore.getInstance().getAudioController().getAudioFileURL(phrase, currentUnit).toString());
            }
            for (ABAPhrase phrase : DataStore.getInstance().getInterpretController().getElementsFromSection(currentUnit.getSectionInterpret())) {
                totalElemetsForDownload.add(DataStore.getInstance().getAudioController().getAudioFileURL(phrase, currentUnit).toString());
            }
            for (ABAPhrase phrase : DataStore.getInstance().getExercisesController().getPhrasesFromExercises(currentUnit.getSectionExercises())) {
                totalElemetsForDownload.add(DataStore.getInstance().getAudioController().getAudioFileURL(phrase, currentUnit).toString());
            }

            //ADD unit videos
            totalElemetsForDownload.add(currentUnit.getSectionFilm().getHdVideoURL().toString());
            totalElemetsForDownload.add(currentUnit.getSectionVideoClass().getHdVideoURL().toString());

            //ADD unit subtitles
            totalElemetsForDownload.add(currentUnit.getSectionFilm().getEnglishSubtitles());
            totalElemetsForDownload.add(currentUnit.getSectionFilm().getTranslatedSubtitles());
            totalElemetsForDownload.add(currentUnit.getSectionVideoClass().getEnglishSubtitles());
            totalElemetsForDownload.add(currentUnit.getSectionVideoClass().getTranslatedSubtitles());

            //ADD user teacher image
            String teacherImage = DataStore.getInstance().getUserController().getCurrentUser(realm).getTeacherImage();
            if (!GenericController.checkIfFileExist(null, teacherImage)) {
                totalElemetsForDownload.add(teacherImage);
            }

            //ADD unit sections backgrounds
            totalElemetsForDownload.add(currentUnit.getFilmImageUrl());
            totalElemetsForDownload.add(currentUnit.getFilmImageInactiveUrl());
            totalElemetsForDownload.add(currentUnit.getVideoClassImageUrl());

            //ADD unit roles image
            for (int i = 0; i < currentUnit.getSectionInterpret().getRoles().size(); i++) {
                totalElemetsForDownload.add(currentUnit.getSectionInterpret().getRoles().get(i).getImageUrl());
                totalElemetsForDownload.add(currentUnit.getSectionInterpret().getRoles().get(i).getImageBigUrl());
            }

            LogBZ.e("Files for download " + totalElemetsForDownload.size());
            return removeRepetitions(currentUnit, totalElemetsForDownload);
        }
        return null;
    }

    public int getAllFilesForDownload(ABAUnit currentUnit, ArrayList<String> listWithAllUrlFiles) {
        int i = 0;
        for (String url : listWithAllUrlFiles) {
            if (!GenericController.checkIfFileExist(currentUnit, url)) {
                i++;
            }
        }
        return i;
    }

    private ArrayList<String> removeRepetitions(ABAUnit currentUnit, ArrayList<String> totalElemetsForDownload) {
        ArrayList<String> urls = new ArrayList<>();
        for (String url : totalElemetsForDownload) {
            if (url != null && !urls.contains(url) && GenericController.getFileNameFromUrl(currentUnit, url).length() != 0) {
                urls.add(url);
            }
        }
        LogBZ.e("Files with RemoveRepetitions " + urls.size());
        return urls;
    }

    private void downloadImagesForSections(Realm realm, ABAUnit currentUnit, DownloadCallback callback) {
        // Download Dashboard UnitsBackground
        List<ABALevel> allLevels = DataStore.getInstance().getLevelUnitController().allLevels(realm);
        for (ABALevel level : allLevels) {
            for (ABAUnit unit : level.getUnits()) {
                downloadFile(unit.getUnitImage(), GenericController.getLocalFilePath(null, unit.getUnitImage()), callback, null);
                downloadFile(unit.getUnitImageInactive(), GenericController.getLocalFilePath(null, unit.getUnitImageInactive()), callback, null);
            }
        }
    }

    // ================================================================================
    // Accessors
    // ================================================================================

    public DownloadThread getDownloadThread() {
        return downloadThread;
    }

    public void setDownloadThread(DownloadThread downloadThread) {
        this.downloadThread = downloadThread;
    }

    public void downloadFile(String fileURL, String destinationPath, DownloadController.DownloadCallback downloadCallback, CountDownLatch signal) {
        downloadThread.enqueueDownload(new DownloadTask(fileURL, destinationPath, downloadCallback), downloadCallback, signal);
    }

    public void setTotalFileForDownload(int files) {
        downloadThread.setDownloadTotalFileForDownload(files);
    }

    public void addDownloadListener(DownloadThreadListener listener) {
        downloadThread.addDownloadListener(listener);
    }

    // ================================================================================
    // Public methods - Remove downloaded files
    // ================================================================================

    public static void removeDownloadedFiles() {
        File abaEnglishFolder = new File(GenericController.abaEnglishFolderPath);
        if (!abaEnglishFolder.exists()) {
            return;
        }

        for (int unitIndex = 1; unitIndex < 144; unitIndex++) {
            String abaEnglishUnit = GenericController.abaEnglishFolderPath + "/UNIT" + unitIndex;
            File unitFolder = new File(abaEnglishUnit);
            if (unitFolder.exists()) {
                GenericController.deleteDirectory(unitFolder);
                LogBZ.d("Removing downloaded files for unit " + unitIndex);
            }
        }

        // TODO: CALLBACK HERE
    }

    public void removeDownloadedFilesForUnit(String idUnit) {
        String abaEnglishUnitFolder = GenericController.abaEnglishFolderPath + "/UNIT" + idUnit;
        File unitFolder = new File(abaEnglishUnitFolder);
        if (unitFolder.exists()) {
            GenericController.deleteDirectory(unitFolder);
            LogBZ.d("Removing downloaded files for unit " + idUnit);
        }
    }
}
