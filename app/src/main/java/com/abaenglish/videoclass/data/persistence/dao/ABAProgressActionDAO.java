package com.abaenglish.videoclass.data.persistence.dao;

import com.abaenglish.videoclass.data.persistence.ABAProgressAction;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by Jesus Espejo using mbp13jesus on 01/03/16.
 * Class to handle all the database queries for the ABAProgressAction model
 */
public class ABAProgressActionDAO {

    public static List<ABAProgressAction> getPendingProgressActions(Realm realm) {

        RealmQuery<ABAProgressAction> query = realm.where(ABAProgressAction.class);
        query.equalTo("sentToServer", false);

        return query.findAllSorted("timestamp");
    }

    public static List<ABAProgressAction> getPendingProgressActions(Realm realm, Integer limit) {

        RealmQuery<ABAProgressAction> query = realm.where(ABAProgressAction.class);
        query.equalTo("sentToServer", false);

        RealmResults<ABAProgressAction> results = query.findAllSorted("timestamp");

        List<ABAProgressAction> limitedResults;
        if (limit < results.size()) {
            limitedResults = results.subList(0, limit);
        } else {
            limitedResults = results;
        }

        return limitedResults;
    }

    public static Long getPendingActionsCount(Realm realm) {
        RealmQuery<ABAProgressAction> query = realm.where(ABAProgressAction.class);
        query.equalTo("sentToServer", false);
        return query.count();
    }
}
