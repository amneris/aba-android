package com.abaenglish.videoclass.data.persistence;

import android.support.annotation.NonNull;

import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.presentation.unit.variation.Section;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by Marc Güell Segarra on 9/4/15.
 */
public class ABAFilm extends RealmObject implements Section {

    @PrimaryKey @Required
    private String abaFilmID;

    @Required private String hdVideoURL;
    @Required private String sdVideoURL;

    private String englishSubtitles;
    private String translatedSubtitles;
    private ABAUnit unit;
    private boolean completed;
    private boolean unlock;
    private float progress;

    @NonNull
    @Override public int getName() {
        return R.string.filmSectionKey;
    }

    @NonNull
    @Override public int getIcon() {
        return R.mipmap.play_button;
    }

    @NonNull
    @Override public int getCompletedPercentage() {
        return (int)getProgress();
    }

    @NonNull
    @Override public boolean isUnlocked() {
        return unlock;
    }

    @Override
    public SectionType getType() {
        return SectionType.FILM;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public boolean isUnlock() {
        return unlock;
    }

    public void setUnlock(boolean unlock) {
        this.unlock = unlock;
    }

    public float getProgress() {
        return progress;
    }

    public void setProgress(float progress) {
        this.progress = progress;
    }

    public String getEnglishSubtitles() {
        return englishSubtitles;
    }

    public void setEnglishSubtitles(String englishSubtitles) {
        this.englishSubtitles = englishSubtitles;
    }

    public String getHdVideoURL() {
        return hdVideoURL;
    }

    public void setHdVideoURL(String hdVideoURL) {
        this.hdVideoURL = hdVideoURL;
    }

    public String getSdVideoURL() {
        return sdVideoURL;
    }

    public void setSdVideoURL(String sdVideoURL) {
        this.sdVideoURL = sdVideoURL;
    }

    public String getTranslatedSubtitles() {
        return translatedSubtitles;
    }

    public void setTranslatedSubtitles(String translatedSubtitles) {
        this.translatedSubtitles = translatedSubtitles;
    }

    public ABAUnit getUnit() {
        return unit;
    }
    public void setUnit(ABAUnit unit) {
        this.unit = unit;
    }

    public String getAbaFilmID() {
        return abaFilmID;
    }

    public void setAbaFilmID(String abaFilmID) {
        this.abaFilmID = abaFilmID;
    }
}
