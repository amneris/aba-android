package com.abaenglish.videoclass.data.network.retrofit.clients;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.abaenglish.videoclass.data.network.oauth.OAuthClient;
import com.abaenglish.videoclass.data.network.oauth.OAuthTokenAccessor;
import com.abaenglish.videoclass.data.network.oauth.RetrofitRxServiceFactory;
import com.abaenglish.videoclass.data.network.retrofit.abaMomentsUtils.ABAMomentBadge;
import com.abaenglish.videoclass.data.network.retrofit.abaMomentsUtils.ABAMomentClientDefinitions.ABAMomentErrorType;
import com.abaenglish.videoclass.data.network.retrofit.services.ABAMomentService;
import com.abaenglish.videoclass.domain.ApplicationConfiguration;
import com.github.scribejava.core.model.OAuth2AccessToken;

import java.util.List;

import retrofit2.HttpException;
import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Jesus Espejo using mbp13jesus on 19/04/2017.
 */

public class ABAMomentBadgeClient extends OAuthClient<Response<List<ABAMomentBadge>>> {

    private ApplicationConfiguration appConfig;
    private Context applicationContext;

    public ABAMomentBadgeClient(ApplicationConfiguration appConfig, OAuthTokenAccessor refresher, Context aContext) {
        super(refresher);
        this.appConfig = appConfig;
        this.applicationContext = aContext.getApplicationContext();
    }

    public void fetchBadge(final String uuid, final OnResponseListener<Integer> listener) {

        final AuthorizedAction<Response<List<ABAMomentBadge>>> action = new AuthorizedAction<Response<List<ABAMomentBadge>>>() {

            @Override
            public Observable<Response<List<ABAMomentBadge>>> actionCall(OAuth2AccessToken oAuth2AccessToken) {
                String authToken = TYPE_BEARER + oAuth2AccessToken.getAccessToken();

                final ABAMomentService service = RetrofitRxServiceFactory.createRetrofitRxService(ABAMomentService.class, appConfig.getGatewayUrl());
                Log.e("authToken", authToken + " " + uuid);
                return service.getABAMomentBadge(authToken);
            }
        };

        // read from cache
        if (getLastBadgeResponse(applicationContext) >= 0) {
            listener.onResponseReceived(getLastBadgeResponse(applicationContext), ABAMomentErrorType.NONE.toString());
        } else {
            listener.onResponseReceived(null, ABAMomentErrorType.USER_NOT_EXIST.toString());
        }

        performAuthorisedCall(action)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<List<ABAMomentBadge>>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        System.out.println("==============================================================");
                        System.out.println("ERROR IN ABAMomentBadgeClient : Cause : " + e.getCause() + ", Message : " + e.getMessage() + " class: " + e.getClass());
                        if (e instanceof HttpException) {
                            HttpException httpException = (HttpException) e;
                            switch (httpException.code()) {
                                case 401:
                                    listener.onResponseReceived(null, ABAMomentErrorType.TOKEN_INCORRECT.toString());
                                    break;
                                case 403:
                                    listener.onResponseReceived(null, ABAMomentErrorType.USER_NOT_EXIST.toString());
                                    break;
                            }
                        } else {
                            listener.onResponseReceived(null, ABAMomentErrorType.UNKNOWN.toString());
                        }

                        // save cache
                        saveBadgeResponse(null, applicationContext);
                    }

                    @Override
                    public void onNext(Response<List<ABAMomentBadge>> abaMomentProgressResponse) {

                        // 204 == not in the ABAMoment user set
                        if (abaMomentProgressResponse.code() == 204) {
                            listener.onResponseReceived(null, ABAMomentErrorType.USER_NOT_EXIST.toString());
                            saveBadgeResponse(null, applicationContext);
                        } else {

                            listener.onResponseReceived(abaMomentProgressResponse.body().size(), ABAMomentErrorType.NONE.toString());

                            // save cache
                            saveBadgeResponse(abaMomentProgressResponse.body().size(), applicationContext);
                        }
                    }
                });
    }

    // ================================================================================
    // Public methods
    // ================================================================================

    public static void reset(Context applicationContext) {
        saveBadgeResponse(null, applicationContext);
    }

    // ================================================================================
    // Private methods
    // ================================================================================

    private static final String BADGE_SHARED_PREFERENCE = "BADGE_SHARED_PREFERENCE";
    private static final String BADGE_COUNT_SHARED_PREFERENCE_KEY = "BADGE_COUNT_SHARED_PREFERENCE_KEY";

    private static void saveBadgeResponse(Integer badgeCount, Context applicationContext) {
        SharedPreferences.Editor editor = applicationContext.getSharedPreferences(BADGE_SHARED_PREFERENCE, Context.MODE_PRIVATE).edit();
        if (badgeCount == null) {
            editor.remove(BADGE_COUNT_SHARED_PREFERENCE_KEY);
        } else {
            editor.putInt(BADGE_COUNT_SHARED_PREFERENCE_KEY, badgeCount);
        }
        editor.apply();
    }

    private static int getLastBadgeResponse(Context applicationContext) {
        SharedPreferences preferences = applicationContext.getSharedPreferences(BADGE_SHARED_PREFERENCE, Context.MODE_PRIVATE);
        return preferences.getInt(BADGE_COUNT_SHARED_PREFERENCE_KEY, -1);
    }
}
