package com.abaenglish.videoclass.data.persistence;

import io.realm.RealmObject;

/**
 * Created by marc on 15/6/15.
 */
public class ABAHelp extends RealmObject {
    private String idHelp;
    private String title;
    private String desc;

    public String getIdHelp() {
        return idHelp;
    }

    public void setIdHelp(String idHelp) {
        this.idHelp = idHelp;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
