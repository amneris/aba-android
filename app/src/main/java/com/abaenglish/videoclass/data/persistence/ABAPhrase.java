package com.abaenglish.videoclass.data.persistence;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.Required;

/**
 * Created by Marc Güell Segarra on 10/4/15.
 */
public class ABAPhrase extends RealmObject {
    @Required
    private String audioFile;
    private boolean done;
    private String idPhrase;
    private String page;
    private String text;
    private String translation;
    private boolean listened;
    private Date serverDate;
    private int sectionType;

    // For Interpret phrases
    private ABAInterpretRole interpretRole;
    private ABAInterpret interpret;

    // For Exercises phrases
    @Required
    private String blank;
    private ABAExercisesQuestion exercisesQuestion;

    // For Speak phrases
    private boolean isSpeakDialogPhrase;      // This will mean ABASpeakDialogPhrase (true)
    private ABASpeakDialog speakDialog;
    private String speakRole;

    private ABAPhrase fatherPhrase;
    private RealmList<ABAPhrase> subPhrases;
    private ABASpeakDialog speakDialogSample;

    // For Vocabulary phrases
    private String wordType;
    private ABAVocabulary abaVocabulary;

    // For Write phrases
    private ABAWriteDialog writeDialog;

    public ABAWriteDialog getWriteDialog() {
        return writeDialog;
    }

    public void setWriteDialog(ABAWriteDialog writeDialog) {
        this.writeDialog = writeDialog;
    }

    public String getAudioFile() {
        return audioFile;
    }

    public void setAudioFile(String audioFile) {
        this.audioFile = audioFile;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public String getIdPhrase() {
        return idPhrase;
    }

    public void setIdPhrase(String idPhrase) {
        this.idPhrase = idPhrase;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTranslation() {
        return translation;
    }

    public void setTranslation(String translation) {
        this.translation = translation;
    }

    public boolean isListened() {
        return listened;
    }

    public void setListened(boolean listened) {
        this.listened = listened;
    }

    public Date getServerDate() {
        return serverDate;
    }

    public void setServerDate(Date serverDate) {
        this.serverDate = serverDate;
    }

    public int getSectionType() {
        return sectionType;
    }

    public void setSectionType(int sectionType) {
        this.sectionType = sectionType;
    }

    public ABAInterpret getInterpret() {
        return interpret;
    }

    public void setInterpret(ABAInterpret interpret) {
        this.interpret = interpret;
    }

    public String getBlank() {
        return blank;
    }

    public void setBlank(String blank) {
        this.blank = blank;
    }

    public ABAExercisesQuestion getExercisesQuestion() {
        return exercisesQuestion;
    }

    public void setExercisesQuestion(ABAExercisesQuestion exercisesQuestion) {
        this.exercisesQuestion = exercisesQuestion;
    }

    public boolean isSpeakDialogPhrase() {
        return isSpeakDialogPhrase;
    }

    public void setIsSpeakDialogPhrase(boolean speakDialogPhrase) {
        this.isSpeakDialogPhrase = speakDialogPhrase;
    }

    public ABASpeakDialog getSpeakDialog() {
        return speakDialog;
    }

    public void setSpeakDialog(ABASpeakDialog speakDialog) {
        this.speakDialog = speakDialog;
    }

    public ABAPhrase getFatherPhrase() {
        return fatherPhrase;
    }

    public void setFatherPhrase(ABAPhrase fatherPhrase) {
        this.fatherPhrase = fatherPhrase;
    }

    public RealmList<ABAPhrase> getSubPhrases() {
        return subPhrases;
    }

    public void setSubPhrases(RealmList<ABAPhrase> subPhrases) {
        this.subPhrases = subPhrases;
    }

    public ABAInterpretRole getInterpretRole() {
        return interpretRole;
    }

    public void setInterpretRole(ABAInterpretRole interpretRole) {
        this.interpretRole = interpretRole;
    }

    public String getSpeakRole() {
        return speakRole;
    }

    public void setSpeakRole(String speakRole) {
        this.speakRole = speakRole;
    }

    public ABASpeakDialog getSpeakDialogSample() {
        return speakDialogSample;
    }

    public void setSpeakDialogSample(ABASpeakDialog speakDialogSample) {
        this.speakDialogSample = speakDialogSample;
    }

    public String getWordType() {
        return wordType;
    }

    public void setWordType(String wordType) {
        this.wordType = wordType;
    }

    public ABAVocabulary getAbaVocabulary() {
        return abaVocabulary;
    }

    public void setAbaVocabulary(ABAVocabulary abaVocabulary) {
        this.abaVocabulary = abaVocabulary;
    }
}
