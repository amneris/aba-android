package com.abaenglish.videoclass.data.network.parser;

import android.content.Context;
import android.os.Bundle;
import android.os.RemoteException;

import com.abaenglish.videoclass.data.persistence.ABAPlan;
import com.android.vending.billing.IInAppBillingService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;

/**
 * Created by Jesus Espejo using mbp13jesus on 01/03/16.
 * Class to parse ABAPlans from JSON
 */
public class ABAPlanParser {

    private static final int RESULT_OK = 0;

    public static List<ABAPlan> parsePlans(Realm realm, Context context, IInAppBillingService mService, String response) throws JSONException, RemoteException {

        List<ABAPlan> listOfPlans = new ArrayList<>();

        JSONObject jsonObject = new JSONObject(response);
        String title = jsonObject.getString("text");
        JSONArray jsonArray = jsonObject.getJSONArray("tiers");

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject planObject = jsonArray.getJSONObject(i);
            ABAPlan plan = realm.createObject(ABAPlan.class);

            plan.setPlanTitle(title);
            if (planObject.isNull("promocode")) {
                plan.setPlanPromocode("");
            } else {
                plan.setPlanPromocode(planObject.getString("promocode"));
            }

            plan.setPlanIs2x1(Integer.parseInt(planObject.getString("is2x1")));
            plan.setOriginalIdentifier(planObject.getString("originalidentifier"));
            plan.setDiscountIdentifier(planObject.getString("discountidentifier"));
            plan.setDays(Integer.parseInt(planObject.getString("days")));
            hydrateSkuFields(context, mService, plan);

            listOfPlans.add(plan);
        }

        return listOfPlans;
    }

    public static void hydrateSkuFields(Context context, IInAppBillingService service, ABAPlan target) throws JSONException, RemoteException {

        // Preparing search
        ArrayList<String> skuList = new ArrayList<>();
        if (target.getOriginalIdentifier() != null) {
            skuList.add(target.getOriginalIdentifier());
        }
        skuList.add(target.getDiscountIdentifier());
        Bundle querySkus = new Bundle();
        querySkus.putStringArrayList("ITEM_ID_LIST", skuList);

        // Performing search
        Bundle skuDetails = service.getSkuDetails(3, context.getPackageName(), "subs", querySkus);
        int response = skuDetails.getInt("RESPONSE_CODE");
        if (response == RESULT_OK) {

            // Iterating products
            ArrayList<String> responseList = skuDetails.getStringArrayList("DETAILS_LIST");
            for (String productJsonString : responseList) {
                JSONObject productJson = new JSONObject(productJsonString);

                // Reading productId and matching
                String sku = productJson.getString("productId");

                if (sku.equalsIgnoreCase(target.getDiscountIdentifier())) {
                    // Discount price and currency
                    target.setCurrencySymbol(getCurrencyFromPrice(productJson.getString("price")));
                    target.setCurrency(productJson.getString("price_currency_code"));
                    target.setDiscountPrice(getPriceFromMicroPrice(productJson.getString("price_amount_micros")));
                }

                // Original identifier can be the same than the discount identifier
                if (sku.equalsIgnoreCase(target.getOriginalIdentifier())) {
                    // Original price and currency
                    target.setOriginalPrice(getPriceFromMicroPrice(productJson.getString("price_amount_micros")));
                }
            }

        } else {
            throw new RuntimeException("It could not update SKU fields because the Google API call failed");
        }
    }

    private static String getCurrencyFromPrice(String priceWithCurrency) {
        if (priceWithCurrency != null)
            return priceWithCurrency.replaceAll("[0-9.,\\s]", "");
        return "";
    }

    private static float getPriceFromMicroPrice(String microPrice) {
        return Float.parseFloat(microPrice) / 1000000;
    }

}
