package com.abaenglish.videoclass.data.network.retrofit.abaMomentsUtils;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ABAMomentBadge {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("order")
    @Expose
    private Integer order;

    @SerializedName("addedDate")
    @Expose
    private String addedDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }

    public String getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(String addedDate) {
        this.addedDate = addedDate;
    }

    @Override
    public String toString() {
        return "ABAMomentBadge{" +
                "id='" + id + '\'' +
                ", order=" + order +
                ", addedDate='" + addedDate + '\'' +
                '}';
    }
}

