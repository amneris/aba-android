package com.abaenglish.videoclass.data.persistence;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by Marc Güell Segarra on 15/4/15.
 */
public class ABASpeakDialog extends RealmObject {
    private String role;
    private ABASpeak abaSpeak;
    private RealmList<ABAPhrase> dialog;
    private RealmList<ABAPhrase> sample;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public ABASpeak getAbaSpeak() {
        return abaSpeak;
    }

    public void setAbaSpeak(ABASpeak abaSpeak) {
        this.abaSpeak = abaSpeak;
    }


    public RealmList<ABAPhrase> getDialog() {
        return dialog;
    }

    public void setDialog(RealmList<ABAPhrase> dialog) {
        this.dialog = dialog;
    }

    public RealmList<ABAPhrase> getSample() {
        return sample;
    }

    public void setSample(RealmList<ABAPhrase> sample) {
        this.sample = sample;
    }
}
