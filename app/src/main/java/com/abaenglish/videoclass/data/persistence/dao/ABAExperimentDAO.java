package com.abaenglish.videoclass.data.persistence.dao;


import com.abaenglish.videoclass.data.persistence.ABAExperiment;

import java.util.List;

import io.realm.Realm;

/**
 * Created by Jesus Espejo using mbp13jesus on 23/03/16.
 * Modified by xilosada on 10/05/16
 */
public class ABAExperimentDAO {
    public static List<ABAExperiment> getABAExperiment(Realm realm) {
        return realm.where(ABAExperiment.class).findAll();
    }

    public static void deleteAll(Realm realm) {
        realm.delete(ABAExperiment.class);
    }
}
