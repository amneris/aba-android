package com.abaenglish.videoclass.data.file;

import android.Manifest;
import android.app.ActivityManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.ImageView;

import java.lang.ref.WeakReference;

/**
 * Created by xabierlosada on 29/09/16.
 */

public class ImageDecoder {

    private static ImageDecoder INSTANCE;
    private WeakReference<Context> context;
    private ImageCache imageCache;

    public static synchronized void init(Context context) {
        INSTANCE = new ImageDecoder(context);
    }

    private int calculateMaxCacheSize() {
        final int memClass = ((ActivityManager) context.get().getSystemService(
                Context.ACTIVITY_SERVICE)).getMemoryClass();
        return 1024 * 1024 * memClass / 8;
    }

    private ImageDecoder(Context context) {
        this.context = new WeakReference<>(context);
        int maxSize = calculateMaxCacheSize();
        this.imageCache = new ImageCache(maxSize);
    }

    public static ImageDecoder getInstance() throws Exception {
        if (INSTANCE == null) {
            throw new Exception("Image decoder must be initialized");
        } else {
            return INSTANCE;
        }
    }

    private Bitmap loadBitmap(String path) {
        Bitmap bitmap = imageCache.get(path);
        if (imageCache.get(path) != null) {
            Log.w("Image loaded", "Image loaded from cache");
            return bitmap;
        }
        Log.w("Image loaded", "Image loaded from disk");
        bitmap = loadSampledImage(path);
        imageCache.put(path, bitmap);
        return bitmap;
    }

    public synchronized void displayImage(String path, ImageView imageView) {
        WeakReference<ImageView> weakReference = new WeakReference<>(imageView);
        new ImageLoadTask(weakReference).execute(path);
    }

    private class ImageLoadTask extends AsyncTask<String, Void, Bitmap> {

        private final Context context;
        private WeakReference<ImageView> imageViewWeakReference;

        ImageLoadTask(WeakReference<ImageView> imageViewWeakReference) {
            this.imageViewWeakReference = imageViewWeakReference;
            this.context = imageViewWeakReference.get().getContext();
        }

        @Override
        protected Bitmap doInBackground(String... path) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                return loadBitmap(path[0]);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if (bitmap != null && imageViewWeakReference.get() != null) {
                imageViewWeakReference.get().setImageBitmap(bitmap);
            }
        }
    }

    public Bitmap loadSampledImage(String pathName) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = false;
        options.inPreferredConfig = Bitmap.Config.RGB_565;
        options.inDither = true;
        return BitmapFactory.decodeFile(pathName, options);
    }
}
