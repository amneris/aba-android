package com.abaenglish.videoclass.data.persistence;

import io.realm.RealmObject;

/**
 * Created by marc on 15/6/15.
 */
public class ABAContact extends RealmObject {
    private String email;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
