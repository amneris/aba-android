package com.abaenglish.videoclass.data.persistence;

import android.support.annotation.NonNull;

import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.presentation.unit.variation.Section;

import io.realm.RealmObject;
import io.realm.annotations.Required;

/**
 * Created by Marc Güell Segarra on 9/4/15.
 */
public class ABAVideoClass extends RealmObject implements Section {

    @Required private String sdVideoURL;
    @Required private String hdVideoURL;

    private String previewImageURL;
    private String englishSubtitles;
    private String translatedSubtitles;
    private ABAUnit unit;
    private boolean completed;
    private boolean unlock;
    private float progress;

    @NonNull
    @Override public int getName() {
        return R.string.videoClassSectionKey;
    }

    @NonNull
    @Override public int getIcon() {
        return R.mipmap.icon_unit_videoclass;
    }

    @NonNull
    @Override public int getCompletedPercentage() {
        return (int)getProgress();
    }

    @NonNull
    @Override public boolean isUnlocked() {
        return unlock;
    }

    @Override
    public SectionType getType() {
        return SectionType.VIDEOCLASS;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public boolean isUnlock() {
        return unlock;
    }

    public void setUnlock(boolean unlock) {
        this.unlock = unlock;
    }

    public float getProgress() {
        return progress;
    }

    public void setProgress(float progress) {
        this.progress = progress;
    }

    public String getEnglishSubtitles() {
        return englishSubtitles;
    }

    public void setEnglishSubtitles(String englishSubtitles) {
        this.englishSubtitles = englishSubtitles;
    }

    public String getHdVideoURL() {
        return hdVideoURL;
    }

    public void setHdVideoURL(String hdVideoURL) {
        this.hdVideoURL = hdVideoURL;
    }

    public String getPreviewImageURL() {
        return previewImageURL;
    }

    public void setPreviewImageURL(String previewImageURL) {
        this.previewImageURL = previewImageURL;
    }

    public String getSdVideoURL() {
        return sdVideoURL;
    }

    public void setSdVideoURL(String sdVideoURL) {
        this.sdVideoURL = sdVideoURL;
    }

    public String getTranslatedSubtitles() {
        return translatedSubtitles;
    }

    public void setTranslatedSubtitles(String translatedSubtitles) {
        this.translatedSubtitles = translatedSubtitles;
    }

    public ABAUnit getUnit() {
        return unit;
    }

    public void setUnit(ABAUnit unit) {
        this.unit = unit;
    }
}
