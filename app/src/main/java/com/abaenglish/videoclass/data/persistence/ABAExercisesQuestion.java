package com.abaenglish.videoclass.data.persistence;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by marc on 13/4/15.
 */
public class ABAExercisesQuestion extends RealmObject {
    private boolean completed;
    private String exerciseTranslation;
    private RealmList<ABAPhrase> phrases;
    private ABAExercisesGroup exercisesGroup;

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public String getExerciseTranslation() {
        return exerciseTranslation;
    }

    public void setExerciseTranslation(String exerciseTranslation) {
        this.exerciseTranslation = exerciseTranslation;
    }

    public RealmList<ABAPhrase> getPhrases() {
        return phrases;
    }

    public void setPhrases(RealmList<ABAPhrase> phrases) {
        this.phrases = phrases;
    }

    public ABAExercisesGroup getExercisesGroup() {
        return exercisesGroup;
    }

    public void setExercisesGroup(ABAExercisesGroup exercisesGroup) {
        this.exercisesGroup = exercisesGroup;
    }
}
