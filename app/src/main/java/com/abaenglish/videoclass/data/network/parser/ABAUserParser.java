package com.abaenglish.videoclass.data.network.parser;

import com.abaenglish.videoclass.analytics.cooladata.CooladataTrackingCommons;
import com.abaenglish.videoclass.data.persistence.ABAExperiment;
import com.abaenglish.videoclass.data.persistence.ABALevel;
import com.abaenglish.videoclass.data.persistence.ABAUser;
import com.abaenglish.videoclass.data.persistence.dao.UserDAO;
import com.abaenglish.videoclass.domain.datastore.DataStore;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Random;

import io.realm.Realm;

/**
 * Created by Jesus Espejo using mbp13jesus on 01/03/16.
 * Class to parse ABAUser
 */
public class ABAUserParser {

    private static final String EXPIRATION_DATE_FORMAT = "dd-MM-yyyy";

    public static ABAUser parseABAUser(Realm realm, String rawJsonUser, String name, String email, String facebookId) throws JSONException, ParseException {

        JSONObject jsonObject = new JSONObject(rawJsonUser);

        CooladataTrackingCommons.recordUserProperties(jsonObject);

        // Preparing values
        SimpleDateFormat sdf = new SimpleDateFormat(EXPIRATION_DATE_FORMAT);
        ABALevel level = DataStore.getInstance().getLevelUnitController().getABALevelWithId(realm, "1");
        if (jsonObject.has("userLevel") && jsonObject.getString("userLevel") != null && jsonObject.getString("userLevel").length() > 0) {
            level = DataStore.getInstance().getLevelUnitController().getABALevelWithId(realm, jsonObject.getString("userLevel"));
        }

        // Reading current user
        ABAUser currentUser = UserDAO.getCurrentUser(realm);

        // Ensuring user instance
        if (currentUser == null) {
            currentUser = realm.createObject(ABAUser.class);
        }

        currentUser.setName(jsonObject.has("name") ? jsonObject.getString("name") : name);
        currentUser.setEmail(jsonObject.has("email") ? jsonObject.getString("email") : email);

        // Optional attributes
        if (jsonObject.has("surnames")) {
            currentUser.setSurnames(jsonObject.getString("surnames"));
        }

        if (jsonObject.has("externalkey")) {
            currentUser.setExternalKey(jsonObject.getString("externalkey"));
        }
        if (jsonObject.has("idSource")) {
            currentUser.setSourceID(jsonObject.getString("idSource"));
        }
        if (jsonObject.has("idPartner")) {
            currentUser.setPartnerID(jsonObject.getString("idPartner"));
        }
        if (facebookId != null) {
            currentUser.setUrlImage("http://graph.facebook.com/" + facebookId + "/picture?type=large");
        }
        if (jsonObject.has("gender")) {
            currentUser.setGender(jsonObject.getString("gender"));
        }
        if (jsonObject.has("birthdate")) {
            currentUser.setBirthdate(jsonObject.getString("birthdate"));
        }
        if (jsonObject.has("phone")) {
            currentUser.setPhone(jsonObject.getString("phone"));
        }
        if (jsonObject.getString("teacherImage") != null && jsonObject.getString("teacherImage").length() > 0) {
            currentUser.setTeacherImage(jsonObject.getString("teacherImage"));
        }

        // Mandatory attributes
        currentUser.setCountry(jsonObject.getString("countryNameIso"));
        currentUser.setToken(jsonObject.getString("token"));
        currentUser.setUserId(jsonObject.getString("userId"));
        currentUser.setUserLang(jsonObject.getString("userLang"));
        currentUser.setTeacherId(jsonObject.getString("teacherId"));
        currentUser.setTeacherName(jsonObject.getString("teacherName"));
        currentUser.setLastLoginDate(new Date()); // Last login date is right now

        // Setting attributes using prepared local values
        currentUser.setExpirationDate(sdf.parse(jsonObject.getString("expirationDate")));
        currentUser.setCurrentLevel(level);
        currentUser.setIdSession(GenerateRandomStringIdSession());

        // Parsing more complicated attributes
        parseUserType(jsonObject, currentUser);

        // Experiments
        for (ABAExperiment experiment : parseListOfExperiments(realm, jsonObject)) {
            currentUser.getExperiment().add(experiment);
        }

        return currentUser;
    }

    public static String parseToken(String jsonWithToken) throws JSONException {
        JSONObject jsonObject = new JSONObject(jsonWithToken);
        if (jsonObject.has("token")) {
            return jsonObject.getString("token");
        }

        throw new NoSuchElementException();
    }

    // ================================================================================
    // Private methods
    // ================================================================================

    private static List<ABAExperiment> parseListOfExperiments(Realm realm, JSONObject jsonObject) throws JSONException {

        List<ABAExperiment> listOfExperiments = new ArrayList<>();

        if (jsonObject.has("profiles")) {
            JSONArray jsonArray = new JSONArray(jsonObject.getString("profiles"));

            for (int i = 0; i < jsonArray.length(); i++) {
                ABAExperiment abaExperiment = realm.createObject(ABAExperiment.class);
                JSONObject jObject = jsonArray.getJSONObject(i);

                if (jObject.keys().hasNext()) {
                    abaExperiment.setExperimentIdentifier(jObject.keys().next());
                    JSONObject jsonExperiment = jObject.getJSONObject(jObject.keys().next());

                    if (jsonExperiment.has("variation")) {
                        abaExperiment.setExperimentVariationIdentifier(jsonExperiment.getString("variation"));
                    }

                    if (jsonExperiment.has("type")) {
                        abaExperiment.setUserExperimentVariationId(jsonExperiment.getString("type"));
                    }

                    listOfExperiments.add(abaExperiment);
                    // currentUser.getExperiment().add(abaExperiment);
                }
            }
        }

        return listOfExperiments;
    }

    private static void parseUserType(JSONObject jsonObject, ABAUser currentUser) throws JSONException {
        String userType = jsonObject.getString("userType");
        if (userType == null || userType.length() == 0) {
            currentUser.setUserType("1");
        } else {
            if (userType.matches("1") ||
                    userType.matches("2") ||
                    userType.matches("3") ||
                    userType.matches("4") ||
                    userType.matches("5") ||
                    userType.matches("6")) {

                currentUser.setUserType(userType);
            } else {
                currentUser.setUserType("1");
            }
        }
    }

    private static String GenerateRandomStringIdSession() {
        String alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXZY0123456789";
        StringBuilder sb = new StringBuilder(32);
        for (int i = 0; i < 32; i++)
            sb.append(alphabet.charAt(new Random().nextInt(alphabet.length())));
        return sb.toString();
    }

}
