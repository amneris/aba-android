package com.abaenglish.videoclass.data.persistence;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by Marc Güell Segarra on 10/4/15.
 */
public class ABAInterpretRole extends RealmObject {
    private boolean completed;
    private ABAInterpret interpret;
    private RealmList<ABAPhrase> interpretPhrase;
    private String          imageUrl;
    private String          imageBigUrl;
    private String          name;
    private ABAUnit         unit;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getImageBigUrl() {
        return imageBigUrl;
    }

    public void setImageBigUrl(String imageBigUrl) {
        this.imageBigUrl = imageBigUrl;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ABAUnit getUnit() {
        return unit;
    }

    public void setUnit(ABAUnit unit) {
        this.unit = unit;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public ABAInterpret getInterpret() {
        return interpret;
    }

    public void setInterpret(ABAInterpret interpret) {
        this.interpret = interpret;
    }

    public RealmList<ABAPhrase> getInterpretPhrase() {
        return interpretPhrase;
    }

    public void setInterpretPhrase(RealmList<ABAPhrase> interpretPhrase) {
        this.interpretPhrase = interpretPhrase;
    }
}
