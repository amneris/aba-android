package com.abaenglish.videoclass.data.persistence.dao;

import android.util.Log;

import com.abaenglish.videoclass.ABAApplication;
import com.abaenglish.videoclass.data.persistence.ABAEvaluation;
import com.abaenglish.videoclass.data.persistence.ABAEvaluationOption;
import com.abaenglish.videoclass.data.persistence.ABAEvaluationQuestion;
import com.abaenglish.videoclass.data.persistence.ABAExercises;
import com.abaenglish.videoclass.data.persistence.ABAExercisesGroup;
import com.abaenglish.videoclass.data.persistence.ABAExercisesQuestion;
import com.abaenglish.videoclass.data.persistence.ABAExperiment;
import com.abaenglish.videoclass.data.persistence.ABAFilm;
import com.abaenglish.videoclass.data.persistence.ABAHelp;
import com.abaenglish.videoclass.data.persistence.ABAInterpret;
import com.abaenglish.videoclass.data.persistence.ABAInterpretRole;
import com.abaenglish.videoclass.data.persistence.ABALevel;
import com.abaenglish.videoclass.data.persistence.ABAPlan;
import com.abaenglish.videoclass.data.persistence.ABARegistrationFunnel;
import com.abaenglish.videoclass.data.persistence.ABARole;
import com.abaenglish.videoclass.data.persistence.ABASpeak;
import com.abaenglish.videoclass.data.persistence.ABASpeakDialog;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.data.persistence.ABAUser;
import com.abaenglish.videoclass.data.persistence.ABAVideoClass;
import com.abaenglish.videoclass.data.persistence.ABAVocabulary;
import com.abaenglish.videoclass.data.persistence.ABAWrite;
import com.abaenglish.videoclass.data.persistence.ABAWriteDialog;
import com.crashlytics.android.Crashlytics;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by Jesus Espejo using mbp13jesus on 02/03/16.
 * Method to handle the ABAUser DB access
 */
public class ABAUserDAO {

    public static void deleteCurrentUser() {
        Crashlytics.log(Log.INFO, "DB cleaning", "deleteCurrentUser method call");

        Realm realm = Realm.getInstance(ABAApplication.get().getRealmConfiguration());
        try {
            realm.beginTransaction();

            // Removing user
            deleteAllInstances(realm, ABAUser.class);

            // Removing Roles
            deleteAllInstances(realm, ABARole.class);

            // Removing Evaluation
            deleteAllInstances(realm, ABAEvaluation.class);
            deleteAllInstances(realm, ABAEvaluationOption.class);
            deleteAllInstances(realm, ABAEvaluationQuestion.class);

            // Removing Exercises
            deleteAllInstances(realm, ABAExercises.class);
            deleteAllInstances(realm, ABAExercisesGroup.class);
            deleteAllInstances(realm, ABAExercisesQuestion.class);

            // Removing ABAFilm
            deleteAllInstances(realm, ABAFilm.class);

            // Removing ABAInterpret
            deleteAllInstances(realm, ABAInterpret.class);
            deleteAllInstances(realm, ABAInterpretRole.class);

            // Removing ABAPhrase
            // this.deleteAllInstances(realm, ABAPhrase.class);

            // Removing ABASpeak
            deleteAllInstances(realm, ABASpeak.class);
            deleteAllInstances(realm, ABASpeakDialog.class);

            // Removing ABAVideoClass
            deleteAllInstances(realm, ABAVideoClass.class);

            // Removing ABAVocabulary
            deleteAllInstances(realm, ABAVocabulary.class);

            // Removing ABAWrite
            deleteAllInstances(realm, ABAWrite.class);
            deleteAllInstances(realm, ABAWriteDialog.class);

            // Removing ABARegisterFunnel
            deleteAllInstances(realm, ABARegistrationFunnel.class);

            // Removing ABAExperiment
            deleteAllInstances(realm, ABAExperiment.class);

            // Removing units
            deleteAllInstances(realm, ABAUnit.class);

            // Removing helps
            deleteAllInstances(realm, ABAHelp.class);

            // Removing plans
            deleteAllInstances(realm, ABAPlan.class);

            // Cleaning levels
            List listToDelete = allInstances(realm, ABALevel.class);
            for (Object aListToDelete : listToDelete) {
                ABALevel levelToClean = (ABALevel) aListToDelete;
                levelToClean.setProgress(0);
                levelToClean.setCompleted(false);
            }

            realm.commitTransaction();

        } catch (Exception ex) {
            ex.printStackTrace();
            Crashlytics.logException(ex);
        }
        realm.close();
    }

    private static void deleteAllInstances(final Realm realmOpenTransaction, final Class aClass) {
        RealmQuery genericQuery = realmOpenTransaction.where(aClass);
        RealmResults results = genericQuery.findAll();
        results.deleteAllFromRealm();
    }

    private static List allInstances(final Realm realmOpenTransaction, final Class aClass) {
        RealmQuery genericQuery = realmOpenTransaction.where(aClass);
        RealmResults evaluationResults = genericQuery.findAll();

        // Moving results to independent Array to avoid exception
        Iterator it = evaluationResults.iterator();
        ArrayList arrayList = new ArrayList();
        while (it.hasNext()) {
            arrayList.add(it.next());
        }

        // Returning array
        return arrayList;
    }
}
