package com.abaenglish.videoclass.data.network;

import com.bzutils.BZUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Jesus Espejo using mbp13jesus on 19/11/15.
 */
public class APIManagerParameterSigner {

    private String SIGNATURE_KEY = "signature";
    private List<String> listOfParametersKey;
    private Map<String, String> mapsOfParameters;

    private APIManagerParameterSigner() {
        listOfParametersKey = new ArrayList<>();
        mapsOfParameters = new HashMap<>();
    }

    public static APIManagerParameterSigner Empty(){
        return new APIManagerParameterSigner();
    }

    // ================================================================================
    // Public methods
    // ================================================================================

    public void addParameter(String key, String value) {
        listOfParametersKey.add(key);
        mapsOfParameters.put(key, value);
    }

    public Map<String, String> getMapWithSignature() {

        Map<String, String> mapToWithSignature = new HashMap<>(mapsOfParameters);
        if (!listOfParametersKey.isEmpty()) {
            mapToWithSignature.put(SIGNATURE_KEY, getSignatureFromParamsConcatenated(listOfParametersKey, mapsOfParameters));
        }
        return mapToWithSignature;
    }

    // ================================================================================
    // Private methods
    // ================================================================================

    private String getSignatureFromParamsConcatenated(List<String> listOfKeys, Map<String, String> mapOfParams) {
        StringBuilder fixedData = new StringBuilder("Mobile Outsource Aba AgnKey");
        for (String key : listOfKeys) {
            fixedData.append(mapOfParams.get(key));
        }

        return BZUtils.md5(fixedData.toString());
    }
}
