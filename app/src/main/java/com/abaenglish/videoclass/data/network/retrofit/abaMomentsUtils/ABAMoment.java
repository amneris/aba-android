package com.abaenglish.videoclass.data.network.retrofit.abaMomentsUtils;

import android.content.Context;
import android.os.Build;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Julien on 02/03/2017.
 * POJO for parsing the ABA Moment list from server.
 */

public class ABAMoment implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("titles")
    @Expose
    private List<Title> titles;

    @SerializedName("icon")
    @Expose
    private String icon;

    @SerializedName("audio")
    @Expose
    private String audio;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("momentType")
    @Expose
    private String momentType;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMomentType() {
        return momentType;
    }

    public void setMomentType(String momentType) {
        this.momentType = momentType;
    }

    public List<Title> getTitles() {
        return titles;
    }

    public void setTitles(List<Title> titles) {
        this.titles = titles;
    }

    public String getDefaultName() {

        for (Title title : titles) {
            if (title.isDefault()) {
                return title.getName();
            }
        }

        return "";
    }

    @SuppressWarnings("deprecation")
    public String getLocalName(Context context) {

        //get the default locale language of the app
        String locale;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            locale = context.getResources().getConfiguration().getLocales().get(0).toString();
        } else {
            locale = context.getResources().getConfiguration().locale.toString();
        }

        for (Title title : titles) {
            if (locale.contains(title.getLanguage())) {
                return title.getName();
            }
        }

        return getDefaultName();
    }

    private class Title implements Serializable {

        @SerializedName("language")
        @Expose
        private String language;
        @SerializedName("name")
        @Expose
        private String name;

        public String getLanguage() {
            return language;
        }

        public void setLanguage(String language) {
            this.language = language;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public boolean isDefault() {
            return language.compareTo("en") == 0;
        }
    }

}
