package com.abaenglish.videoclass.data.network.parser;

import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.domain.content.ExercisesController;
import com.abaenglish.videoclass.domain.content.InterpretController;
import com.abaenglish.videoclass.domain.content.SpeakController;
import com.abaenglish.videoclass.domain.content.VocabularyController;
import com.abaenglish.videoclass.domain.content.WriteController;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.abaenglish.videoclass.presentation.section.SectionType;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import io.realm.Realm;

/**
 * Created by Jesus Espejo using mbp13jesus on 02/03/16.
 * Class to parse the completed actions
 */
public class ABACompletedActionsParser {

    public static void parseCompletedActions(Realm realm, String rawProgressAction, String unitID) throws JSONException {
        ABAUnit unit = DataStore.getInstance().getLevelUnitController().getUnitWithId(realm, unitID);

        JSONArray jsonArray = new JSONArray(rawProgressAction);

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject actionObject = jsonArray.getJSONObject(i);
            Integer sectionType = (Integer) actionObject.get("SectionTypeId");

            switch (SectionType.fromInt(sectionType)) {
                case kHabla: {
                    SpeakController speakController = DataStore.getInstance().getSpeakController();
                    if (!speakController.isSectionCompleted(unit.getSectionSpeak())) {
                        speakController.syncCompletedActions(realm, unit.getSectionSpeak(), actionObject.getJSONArray("Elements"));
                    }
                    break;
                }
                case kVocabulario: {
                    VocabularyController vocabularyController = DataStore.getInstance().getVocabularyController();
                    if (!vocabularyController.isSectionCompleted(unit.getSectionVocabulary())) {
                        vocabularyController.syncCompletedActions(realm, unit.getSectionVocabulary(), actionObject.getJSONArray("Elements"));
                    }
                    break;
                }
                case kEscribe: {
                    WriteController writeController = DataStore.getInstance().getWriteController();
                    if (!writeController.isSectionCompleted(unit.getSectionWrite())) {
                        writeController.syncCompletedActions(realm, unit.getSectionWrite(), actionObject.getJSONArray("Elements"));
                    }
                    break;
                }
                case kInterpreta: {
                    InterpretController interpretController = DataStore.getInstance().getInterpretController();
                    if (!interpretController.isSectionCompleted(unit.getSectionInterpret())) {
                        interpretController.syncCompletedActions(realm, unit.getSectionInterpret(), actionObject.getJSONArray("Elements"));
                    }
                    break;
                }
                case kEjercicios: {
                    ExercisesController exercisesController = DataStore.getInstance().getExercisesController();
                    if (!exercisesController.isSectionCompleted(unit.getSectionExercises())) {
                        exercisesController.syncCompletedActions(realm, unit.getSectionExercises(), actionObject.getJSONArray("Elements"));
                    }
                    break;
                }
                default:
                    break;
            }
        }
    }

    // ================================================================================
    // Private methods
    // ================================================================================


}
