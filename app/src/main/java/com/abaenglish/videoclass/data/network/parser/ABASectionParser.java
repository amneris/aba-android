package com.abaenglish.videoclass.data.network.parser;

import com.abaenglish.videoclass.data.persistence.ABAEvaluation;
import com.abaenglish.videoclass.data.persistence.ABAEvaluationOption;
import com.abaenglish.videoclass.data.persistence.ABAEvaluationQuestion;
import com.abaenglish.videoclass.data.persistence.ABAExercises;
import com.abaenglish.videoclass.data.persistence.ABAExercisesGroup;
import com.abaenglish.videoclass.data.persistence.ABAExercisesQuestion;
import com.abaenglish.videoclass.data.persistence.ABAFilm;
import com.abaenglish.videoclass.data.persistence.ABAInterpret;
import com.abaenglish.videoclass.data.persistence.ABAInterpretRole;
import com.abaenglish.videoclass.data.persistence.ABAPhrase;
import com.abaenglish.videoclass.data.persistence.ABARole;
import com.abaenglish.videoclass.data.persistence.ABASpeak;
import com.abaenglish.videoclass.data.persistence.ABASpeakDialog;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.data.persistence.ABAVideoClass;
import com.abaenglish.videoclass.data.persistence.ABAVocabulary;
import com.abaenglish.videoclass.data.persistence.ABAWrite;
import com.abaenglish.videoclass.data.persistence.ABAWriteDialog;
import com.abaenglish.videoclass.data.persistence.dao.ABAUnitDAO;
import com.abaenglish.videoclass.presentation.section.SectionType;
import com.google.gson.JsonParseException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import io.realm.Realm;

/**
 * Created by Jesus Espejo using mbp13jesus on 03/03/16.
 * Class with some methods to parser all the sections
 */
public class ABASectionParser {

    public static void parseSections(Realm realm, String rawSections, String targetUnitId) throws JSONException {

        ABAUnit unit = ABAUnitDAO.getUnitWithId(realm, targetUnitId);
        Integer sectionsCounter = 0;

        JSONArray jsonArray = new JSONArray(rawSections);
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject contentJson = jsonArray.getJSONObject(i);

            if (contentJson.has("AbaFilm")) {
                JSONArray jsonFilmArray = contentJson.getJSONArray("AbaFilm");
                ABASectionParser.parseFilmSection(realm, jsonFilmArray, unit);
                sectionsCounter++;
            } else if (contentJson.has("Speak")) {
                JSONArray speakArray = contentJson.getJSONArray("Speak");
                ABASectionParser.parseSpeakSection(realm, speakArray, unit);
                sectionsCounter++;
            } else if (contentJson.has("Write")) {
                JSONArray writeArray = contentJson.getJSONArray("Write");
                ABASectionParser.parseWriteSection(realm, writeArray, unit);
                sectionsCounter++;
            } else if (contentJson.has("Video Classes")) {
                JSONArray jsonVideoClassArray = contentJson.getJSONArray("Video Classes");
                ABASectionParser.parseVideoClassSection(realm, jsonVideoClassArray, unit);
                sectionsCounter++;
            } else if (contentJson.has("Interpret")) {
                JSONArray interpretArray = contentJson.getJSONArray("Interpret");
                ABASectionParser.parseIntepretSection(realm, interpretArray, unit);
                sectionsCounter++;
            } else if (contentJson.has("Exercises")) {
                JSONArray exercisesArray = contentJson.getJSONArray("Exercises");
                ABASectionParser.parseExercisesSection(realm, exercisesArray, unit);
                sectionsCounter++;
            } else if (contentJson.has("Vocabulary")) {
                JSONArray vocabularyArray = contentJson.getJSONArray("Vocabulary");
                ABASectionParser.parseVocabularySection(realm, vocabularyArray, unit);
                sectionsCounter++;
            } else if (contentJson.has("Assesment")) {
                JSONArray evaluationArray = contentJson.getJSONArray("Assesment");
                ABASectionParser.parseEvaluationSection(realm, evaluationArray, unit);
                sectionsCounter++;
            } else {
                throw new JsonParseException("Unexpected section type");
            }

            if (sectionsCounter == SectionType.kEvaluacion.getValue()) {
                unit.setDataDownloaded(true);
            }
        }
    }

    private static void parseFilmSection(Realm realm, JSONArray jsonArray, ABAUnit unit) throws JSONException {

        ABAFilm section = unit.getSectionFilm();

        JSONObject jsonObject = jsonArray.getJSONObject(0);

        section.setEnglishSubtitles(jsonObject.getString("English Subtitles"));
        section.setHdVideoURL(jsonObject.getString("SD Video URL"));
        section.setSdVideoURL(jsonObject.getString("Mobile SD Video URL"));
        section.setTranslatedSubtitles(jsonObject.getString("Translated Subtitles"));

        section.setUnit(unit);
        unit.setSectionFilm(section);
    }

    private static void parseSpeakSection(Realm realm, JSONArray jsonArray, ABAUnit unit) throws JSONException {

        ABASpeak section = unit.getSectionSpeak();

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);

            if (!jsonObject.has("dialog")) {
                continue;
            }

            ABASpeakDialog speakDialog = realm.createObject(ABASpeakDialog.class);
            speakDialog.setAbaSpeak(section);
            speakDialog.setRole(jsonObject.getString("Role"));

            section.getContent().add(speakDialog);

            if (jsonObject.has("dialog")) {
                JSONArray dialogs = jsonObject.getJSONArray("dialog");

                boolean firstItem = true;
                int dialogCount = 0;

                for (int j = 0; j < dialogs.length(); j++) {
                    dialogCount++;

                    JSONObject dialogJson = dialogs.getJSONObject(j);

                    if (dialogJson.getString("audio").isEmpty()) {
                        continue;
                    }

                    ABAPhrase phrase = realm.createObject(ABAPhrase.class);
                    phrase.setSectionType(SectionType.kHabla.getValue());
                    phrase.setIsSpeakDialogPhrase(true);

                    phrase.setIdPhrase(dialogJson.getString("audio"));
                    phrase.setAudioFile(dialogJson.getString("audio"));
                    phrase.setText(dialogJson.getString("text"));
                    phrase.setPage(dialogJson.getString("page"));
                    phrase.setSpeakDialog(speakDialog);

                    if (jsonObject.has("translation")) {
                        Object translationEntry = jsonObject.get("translation");

                        if (translationEntry instanceof JSONArray) {
                            if (dialogCount == dialogs.length()) {

                                JSONArray unusefulArray = (JSONArray) translationEntry;

                                phrase.setTranslation((String) unusefulArray.get(0));
                            }
                        } else {
                            phrase.setTranslation((String) translationEntry);
                        }
                    }

                    if (firstItem) {
                        firstItem = false;
                        phrase.setSpeakRole(jsonObject.getString("Role"));
                    } else {
                        phrase.setSpeakRole("");
                    }

                    speakDialog.getDialog().add(phrase);
                }

                if (jsonObject.has("sample")) {
                    Object guessObject = jsonObject.get("sample");

                    if (guessObject instanceof JSONArray) {
                        JSONArray sampleArray = (JSONArray) guessObject;

                        for (int j = 0; j < sampleArray.length(); j++) {
                            Object guessAnotherObject = sampleArray.get(j);

                            if (guessAnotherObject instanceof JSONObject) {
                                JSONObject sampleJson = (JSONObject) guessAnotherObject;

                                if (sampleJson.getString("text").length() > 0) {     // Controlar textos vacíos
                                    ABAPhrase phrase = realm.createObject(ABAPhrase.class);
                                    phrase.setSectionType(SectionType.kHabla.getValue());
                                    phrase.setIsSpeakDialogPhrase(false);

                                    if (sampleJson.getString("audio").isEmpty()) {
                                        continue;
                                    }

                                    phrase.setIdPhrase(sampleJson.getString("audio"));
                                    phrase.setAudioFile(sampleJson.getString("audio"));
                                    phrase.setText(sampleJson.getString("text"));
                                    phrase.setPage(sampleJson.getString("page"));

                                    if (sampleJson.has("translation")) {
                                        phrase.setTranslation(sampleJson.getString("translation"));
                                    }

                                    phrase.setSpeakDialogSample(speakDialog);
                                    speakDialog.getSample().add(phrase);
                                }
                            } else {      // array de phrases de un sample
                                JSONArray sampleArrayJson = (JSONArray) guessAnotherObject;

                                ABAPhrase fatherPhrase = realm.createObject(ABAPhrase.class);
                                fatherPhrase.setSectionType(SectionType.kHabla.getValue());
                                fatherPhrase.setIsSpeakDialogPhrase(false);

                                for (int k = 0; k < sampleArrayJson.length(); k++) {
                                    JSONObject sampleJson = sampleArrayJson.getJSONObject(k);

                                    ABAPhrase phrase = realm.createObject(ABAPhrase.class);
                                    phrase.setSectionType(SectionType.kHabla.getValue());
                                    phrase.setIsSpeakDialogPhrase(false);

                                    if (sampleJson.getString("audio").isEmpty()) {
                                        continue;
                                    }

                                    phrase.setIdPhrase(sampleJson.getString("audio"));
                                    phrase.setAudioFile(sampleJson.getString("audio"));
                                    phrase.setText(sampleJson.getString("text"));
                                    phrase.setPage(sampleJson.getString("page"));

                                    if (sampleJson.has("translation")) {
                                        phrase.setTranslation(sampleJson.getString("translation"));
                                    }

                                    fatherPhrase.getSubPhrases().add(phrase);
                                    phrase.setFatherPhrase(fatherPhrase);
                                }

                                fatherPhrase.setSpeakDialogSample(speakDialog);
                                speakDialog.getSample().add(fatherPhrase);
                            }
                        }
                    }
                }
            }
        }
    }

    private static void parseIntepretSection(Realm realm, JSONArray jsonArray, ABAUnit unit) throws JSONException {

        ABAInterpret abaInterpret = unit.getSectionInterpret();

        HashMap<String, String> rolesUrlImages = new HashMap<>();
        HashMap<String, String> rolesUrlBigImages = new HashMap<>();

        for (ABARole role : unit.getRoles()) {
            rolesUrlImages.put(role.getName(), role.getImageUrl());
            rolesUrlBigImages.put(role.getName(), role.getImageBigUrl());
        }

        HashMap<String, ABAInterpretRole> rolesMap = new HashMap<>();

        for (int i = 0; i < jsonArray.length(); i++) {

            JSONObject jsonObject = jsonArray.getJSONObject(i);

            if (rolesMap.get(jsonObject.getString("Role")) == null) {
                ABAInterpretRole role = realm.createObject(ABAInterpretRole.class);
                role.setName(jsonObject.getString("Role"));
                role.setCompleted(false);
                role.setInterpret(abaInterpret);
                abaInterpret.getRoles().add(role);

                if (rolesUrlImages.get(jsonObject.getString("Role")) != null) {
                    role.setImageUrl(rolesUrlImages.get(jsonObject.getString("Role")));
                }

                if (rolesUrlBigImages.get(jsonObject.getString("Role")) != null) {
                    role.setImageBigUrl(rolesUrlBigImages.get(jsonObject.getString("Role")));
                }

                rolesMap.put(jsonObject.getString("Role"), role);
            }

            if (jsonObject.has("dialog")) {
                JSONArray dialogArray = jsonObject.getJSONArray("dialog");

                for (int j = 0; j < dialogArray.length(); j++) {
                    JSONObject dialogData = dialogArray.getJSONObject(j);
                    ABAPhrase interpretPhrase = realm.createObject(ABAPhrase.class);
                    interpretPhrase.setSectionType(SectionType.kInterpreta.getValue());

                    interpretPhrase.setIdPhrase(dialogData.getString("audio"));
                    interpretPhrase.setText(dialogData.getString("text"));
                    interpretPhrase.setAudioFile(dialogData.getString("audio"));
                    interpretPhrase.setPage(dialogData.getString("page"));
                    interpretPhrase.setInterpret(abaInterpret);

                    ABAInterpretRole role = rolesMap.get(jsonObject.getString("Role"));
                    interpretPhrase.setInterpretRole(role);
                    role.getInterpretPhrase().add(interpretPhrase);
                    abaInterpret.getDialog().add(interpretPhrase);
                }
            }
        }
    }

    private static void parseWriteSection(Realm realm, JSONArray jsonArray, ABAUnit unit) throws JSONException {

        ABAWrite abaWrite = unit.getSectionWrite();

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);

            ABAWriteDialog writeDialog = realm.createObject(ABAWriteDialog.class);
            writeDialog.setRole(jsonObject.getString("Role"));
            writeDialog.setWriteSection(abaWrite);

            if (jsonObject.has("dialog")) {
                JSONArray dialogArray = jsonObject.getJSONArray("dialog");

                for (int j = 0; j < dialogArray.length(); j++) {
                    JSONObject phraseJson = dialogArray.getJSONObject(j);

                    ABAPhrase writePhrase = realm.createObject(ABAPhrase.class);
                    writePhrase.setSectionType(SectionType.kEscribe.getValue());

                    writePhrase.setAudioFile(phraseJson.getString("audio"));
                    writePhrase.setIdPhrase(phraseJson.getString("audio"));
                    writePhrase.setText(phraseJson.getString("text"));
                    writePhrase.setPage(phraseJson.getString("page"));

                    writePhrase.setWriteDialog(writeDialog);
                    writeDialog.getDialog().add(writePhrase);
                }
            }

            abaWrite.getContent().add(writeDialog);
        }
    }

    private static void parseVideoClassSection(Realm realm, JSONArray jsonArray, ABAUnit unit) throws JSONException {
        ABAVideoClass section = unit.getSectionVideoClass();

        JSONObject jsonObject = jsonArray.getJSONObject(0);

        section.setEnglishSubtitles(jsonObject.getString("English Subtitles"));
        section.setHdVideoURL(jsonObject.getString("SD Video URL"));

        if (jsonObject.has("Preview Image URL")) {
            section.setPreviewImageURL(jsonObject.getString("Preview Image URL"));
        }

        section.setSdVideoURL(jsonObject.getString("Mobile SD Video URL"));
        section.setTranslatedSubtitles(jsonObject.getString("Translated Subtitles"));
        section.setUnit(unit);
    }

    private static void parseExercisesSection(Realm realm, JSONArray jsonArray, ABAUnit unit) throws JSONException {

        ABAExercises abaExercises = unit.getSectionExercises();

        for (int i = 0; i < jsonArray.length(); i++) {
            ABAExercisesGroup exerciseGroup = realm.createObject(ABAExercisesGroup.class);
            exerciseGroup.setExercises(abaExercises);
            exerciseGroup.setCompleted(false);
            abaExercises.getExercisesGroups().add(exerciseGroup);

            JSONObject exercisesGroupJson = jsonArray.getJSONObject(i);

            // Capturamos titulo del ejercicio (nos mandan arrays dobles, y otras estructuras surrealistas, so...)
            JSONArray titleArray = exercisesGroupJson.getJSONArray("title");

            if (titleArray.length() > 0) {
                JSONObject titleJson = titleArray.getJSONObject(0);

                if (titleJson.has("translation")) {
                    exerciseGroup.setTitle(titleJson.getString("translation"));
                }
            }

            if (exercisesGroupJson.has("exercise")) {
                JSONArray exerciseQuestions = exercisesGroupJson.getJSONArray("exercise");
                for (int j = 0; j < exerciseQuestions.length(); j++) {

                    ABAExercisesQuestion exerciseQuestion = realm.createObject(ABAExercisesQuestion.class);
                    exerciseQuestion.setExercisesGroup(exerciseGroup);
                    exerciseQuestion.setCompleted(false);
                    exerciseGroup.getQuestions().add(exerciseQuestion);

                    JSONArray abaExerciseQuestionDataArray = exerciseQuestions.getJSONArray(j);

                    for (int k = 0; k < abaExerciseQuestionDataArray.length(); k++) {
                        Object guessObject = abaExerciseQuestionDataArray.get(k);

                        if (guessObject instanceof JSONArray) {          // Exercises entry
                            JSONArray abaExerciseQuestionArray = (JSONArray) guessObject;

                            for (int l = 0; l < abaExerciseQuestionArray.length(); l++) {
                                JSONObject jsonObject = abaExerciseQuestionArray.getJSONObject(l);

                                if (jsonObject.has("translation")) {
                                    exerciseQuestion.setExerciseTranslation(jsonObject.getString("translation"));
                                } else if (jsonObject.has("text") && !jsonObject.has("audio")) {
                                    ABAPhrase exercisesPhrase = realm.createObject(ABAPhrase.class);
                                    exercisesPhrase.setSectionType(SectionType.kEjercicios.getValue());

                                    exercisesPhrase.setExercisesQuestion(exerciseQuestion);

                                    exercisesPhrase.setText(jsonObject.getString("text"));
                                    exercisesPhrase.setBlank("");
                                    exerciseQuestion.getPhrases().add(exercisesPhrase);
                                } else {
                                    ABAPhrase exercisesPhrase = realm.createObject(ABAPhrase.class);
                                    exercisesPhrase.setSectionType(SectionType.kEjercicios.getValue());

                                    exercisesPhrase.setExercisesQuestion(exerciseQuestion);

                                    if (jsonObject.has("blank")) {
                                        if (jsonObject.getString("blank").length() > 0) {
                                            String idPhrase = jsonObject.getString("audio") + "_" + jsonObject.getString("posAudio");
                                            exercisesPhrase.setIdPhrase(idPhrase);
                                            exercisesPhrase.setBlank(jsonObject.getString("blank"));
                                        } else {
                                            exercisesPhrase.setIdPhrase(jsonObject.getString("audio"));
                                        }
                                    } else {
                                        exercisesPhrase.setIdPhrase(jsonObject.getString("audio"));
                                    }

                                    exercisesPhrase.setText(jsonObject.getString("text"));
                                    exercisesPhrase.setAudioFile(jsonObject.getString("audio"));
                                    exercisesPhrase.setDone(false);
                                    exercisesPhrase.setPage(jsonObject.getString("page"));
                                    exerciseQuestion.getPhrases().add(exercisesPhrase);
                                }
                            }
                        } else {                                          // Translation entry
                            JSONObject jsonObject = (JSONObject) guessObject;

                            if (jsonObject.has("translation")) {
                                exerciseQuestion.setExerciseTranslation(jsonObject.getString("translation"));
                            } else {
                                exerciseQuestion.setExerciseTranslation("");
                            }
                        }
                    }
                }
            }
        }
    }

    private static void parseVocabularySection(Realm realm, JSONArray jsonArray, ABAUnit unit) throws JSONException {

        ABAVocabulary abaVocabulary = unit.getSectionVocabulary();

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);

            ABAPhrase phrase = realm.createObject(ABAPhrase.class);
            phrase.setSectionType(SectionType.kVocabulario.getValue());

            phrase.setAudioFile(jsonObject.getString("audio"));
            phrase.setText(jsonObject.getString("text"));

            if (jsonObject.has("translation")) {
                phrase.setTranslation(jsonObject.getString("translation"));
            }

            phrase.setWordType(jsonObject.getString("wordType"));
            phrase.setPage(jsonObject.getString("page"));
            phrase.setAbaVocabulary(abaVocabulary);
            abaVocabulary.getContent().add(phrase);
        }
    }

    private static void parseEvaluationSection(Realm realm, JSONArray jsonArray, ABAUnit unit) throws JSONException {

        ABAEvaluation abaEvaluation = unit.getSectionEvaluation();

        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);

            ABAEvaluationQuestion abaEvaluationQuestion = realm.createObject(ABAEvaluationQuestion.class);
            abaEvaluationQuestion.setOrder(Integer.parseInt(jsonObject.getString("Order")));
            abaEvaluationQuestion.setQuestion(jsonObject.getString("Question"));

            abaEvaluationQuestion.setAbaEvaluation(abaEvaluation);
            abaEvaluation.getContent().add(abaEvaluationQuestion);

            ABAEvaluationOption abaEvaluationOptionA = realm.createObject(ABAEvaluationOption.class);
            abaEvaluationOptionA.setEvaluationQuestion(abaEvaluationQuestion);
            abaEvaluationQuestion.getOptions().add(abaEvaluationOptionA);

            abaEvaluationOptionA.setGood(false);
            abaEvaluationOptionA.setText(jsonObject.getString("OptionA"));
            abaEvaluationOptionA.setOptionLetter("a");

            ABAEvaluationOption abaEvaluationOptionB = realm.createObject(ABAEvaluationOption.class);
            abaEvaluationOptionB.setEvaluationQuestion(abaEvaluationQuestion);
            abaEvaluationQuestion.getOptions().add(abaEvaluationOptionB);

            abaEvaluationOptionB.setGood(false);
            abaEvaluationOptionB.setText(jsonObject.getString("OptionB"));
            abaEvaluationOptionB.setOptionLetter("b");

            ABAEvaluationOption abaEvaluationOptionC = realm.createObject(ABAEvaluationOption.class);
            abaEvaluationOptionC.setEvaluationQuestion(abaEvaluationQuestion);
            abaEvaluationQuestion.getOptions().add(abaEvaluationOptionC);

            abaEvaluationOptionC.setGood(false);
            abaEvaluationOptionC.setText(jsonObject.getString("OptionC"));
            abaEvaluationOptionC.setOptionLetter("c");

            if (jsonObject.getString("Answer").equals("a")) {
                abaEvaluationOptionA.setGood(true);
            } else if (jsonObject.getString("Answer").equals("b")) {
                abaEvaluationOptionB.setGood(true);
            } else if (jsonObject.getString("Answer").equals("c")) {
                abaEvaluationOptionC.setGood(true);
            }
        }
    }

}
