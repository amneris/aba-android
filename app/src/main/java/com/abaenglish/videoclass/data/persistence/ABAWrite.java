package com.abaenglish.videoclass.data.persistence;

import android.support.annotation.NonNull;

import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.presentation.unit.variation.Section;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by Marc Güell Segarra on 14/4/15.
 */
public class ABAWrite extends RealmObject implements Section {

    private ABAUnit unit;
    private RealmList<ABAWriteDialog> content;
    private boolean completed;
    private boolean unlock;
    private float progress;

    @NonNull
    @Override public int getName() {
        return R.string.writeSectionKey;
    }

    @NonNull
    @Override public int getIcon() {
        return R.mipmap.icon_unit_write;
    }

    @NonNull
    @Override public int getCompletedPercentage() {
        return (int)getProgress();
    }

    @NonNull
    @Override public boolean isUnlocked() {
        return unlock;
    }

    @Override
    public SectionType getType() {
        return SectionType.WRITE;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public boolean isUnlock() {
        return unlock;
    }

    public void setUnlock(boolean unlock) {
        this.unlock = unlock;
    }

    public float getProgress() {
        return progress;
    }

    public void setProgress(float progress) {
        this.progress = progress;
    }

    public RealmList<ABAWriteDialog> getContent() {
        return content;
    }

    public void setContent(RealmList<ABAWriteDialog> content) {
        this.content = content;
    }

    public ABAUnit getUnit() {
        return unit;
    }

    public void setUnit(ABAUnit unit) {
        this.unit = unit;
    }
}
