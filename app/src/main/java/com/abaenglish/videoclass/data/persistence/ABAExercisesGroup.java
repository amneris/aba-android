package com.abaenglish.videoclass.data.persistence;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by marc on 13/4/15.
 */
public class ABAExercisesGroup extends RealmObject {
    private boolean completed;
    private String title;
    private ABAExercises exercises;
    private RealmList<ABAExercisesQuestion> questions;

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ABAExercises getExercises() {
        return exercises;
    }

    public void setExercises(ABAExercises exercises) {
        this.exercises = exercises;
    }

    public RealmList<ABAExercisesQuestion> getQuestions() {
        return questions;
    }

    public void setQuestions(RealmList<ABAExercisesQuestion> questions) {
        this.questions = questions;
    }
}
