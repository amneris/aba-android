package com.abaenglish.videoclass.data.persistence;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by Marc Güell Segarra on 15/4/15.
 */
public class ABAEvaluationQuestion extends RealmObject {
    private int order;
    private String question;
    private boolean answered;
    private ABAEvaluation abaEvaluation;
    private RealmList<ABAEvaluationOption> options;

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public boolean isAnswered() {
        return answered;
    }

    public void setAnswered(boolean answered) {
        this.answered = answered;
    }

    public ABAEvaluation getAbaEvaluation() {
        return abaEvaluation;
    }

    public void setAbaEvaluation(ABAEvaluation abaEvaluation) {
        this.abaEvaluation = abaEvaluation;
    }

    public RealmList<ABAEvaluationOption> getOptions() {
        return options;
    }

    public void setOptions(RealmList<ABAEvaluationOption> options) {
        this.options = options;
    }
}
