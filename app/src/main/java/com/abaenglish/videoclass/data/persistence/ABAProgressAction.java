package com.abaenglish.videoclass.data.persistence;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by jaume on 18/5/15.
 */
public class ABAProgressAction extends RealmObject {
    @PrimaryKey @Required
    private String actionID;
    private String action;
    private String audioID;
    private boolean correct;
    private boolean isHelp;
    private int page;
    private int punctuation;
    private int sectionType;
    private boolean sentToServer = false;
    private String text;
    private boolean startSession;
    private String timestamp;
    private String ip;
    private String language;
    private String unitId;
    private String userId;
    private String idSession;
    private String optionLettersEvaluation;

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getAudioID() {
        return audioID;
    }

    public void setAudioID(String audioID) {
        this.audioID = audioID;
    }

    public boolean getCorrect() {
        return correct;
    }

    public void setCorrect(boolean correct) {
        this.correct = correct;
    }

    public boolean getIsHelp() {
        return isHelp;
    }

    public void setIsHelp(boolean isHelp) {
        this.isHelp = isHelp;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPunctuation() {
        return punctuation;
    }

    public void setPunctuation(int punctuation) {
        this.punctuation = punctuation;
    }

    public int getSectionType() {
        return sectionType;
    }

    public void setSectionType(int sectionType) {
        this.sectionType = sectionType;
    }

    public boolean getSentToServer() {
        return sentToServer;
    }

    public void setSentToServer(boolean sentToServer) {
        this.sentToServer = sentToServer;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean getStartSession() {
        return startSession;
    }

    public void setStartSession(boolean startSession) {
        this.startSession = startSession;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getOptionLettersEvaluation() {
        return optionLettersEvaluation;
    }

    public void setOptionLettersEvaluation(String optionLettersEvaluation) {
        this.optionLettersEvaluation = optionLettersEvaluation;
    }

    public String getIdSession() {
        return idSession;
    }

    public void setIdSession(String idSession) {
        this.idSession = idSession;
    }

    public String getActionID() {
        return actionID;
    }

    public void setActionID(String actionID) {
        this.actionID = actionID;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
