package com.abaenglish.videoclass.data.persistence;

import io.realm.RealmObject;

/**
 * Created by Marc Güell Segarra on 26/3/15.
 */
public class ABACert extends RealmObject {
    public ABALevel getLevel() {
        return level;
    }

    public void setLevel(ABALevel level) {
        this.level = level;
    }

    private ABALevel level;
}
