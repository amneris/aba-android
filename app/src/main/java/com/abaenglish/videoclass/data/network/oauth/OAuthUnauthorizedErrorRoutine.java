package com.abaenglish.videoclass.data.network.oauth;

import com.abaenglish.videoclass.data.network.oauth.custom.OAuthError;
import com.google.gson.Gson;

import retrofit2.HttpException;
import rx.Observable;
import rx.functions.Func1;

/**
 * Created by Jesus Espejo using mbp13jesus on 17/03/17.
 * Class to handle the unauthorized exception that might be risen by OAuth 2.0
 */

public class OAuthUnauthorizedErrorRoutine<T> {

    public interface AuthorizedErrorRoutineAction<T> {
        Observable<T> errorRoutineCall();
    }

    private AuthorizedErrorRoutineAction<T> actionForError;

    public OAuthUnauthorizedErrorRoutine(AuthorizedErrorRoutineAction action) {
        this.actionForError = action;
    }

    protected Func1<Throwable, Observable<? extends T>> authorizationErrorRoutine() {
        return new Func1<Throwable, Observable<? extends T>>() {
            @Override
            public Observable<? extends T> call(Throwable throwable) {

                if (throwable instanceof HttpException) {

                    HttpException httpException = (HttpException) throwable;

                    // Unauthorised - token expired
                    if (httpException.code() == 401) {

                        //test if the error is invalid_token
                        try {
                            OAuthError error = new Gson().fromJson(httpException.response().errorBody().string(), OAuthError.class);
                            if (error.isInvalidToken()) {
                                System.out.println("==============================================================");
                                System.out.println("OAuthClient : Access token has expired");

                                return actionForError.errorRoutineCall();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                } else if (throwable instanceof IllegalArgumentException) {
                    if (throwable.getMessage().contains("invalid_token")) {

                        System.out.println("==============================================================");
                        System.out.println("OAuthClient : Refresh token has expired");
                        return actionForError.errorRoutineCall();
                    }
                }

                return Observable.error(throwable);
            }
        };
    }
}


