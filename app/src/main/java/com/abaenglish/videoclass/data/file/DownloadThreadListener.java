package com.abaenglish.videoclass.data.file;

public interface DownloadThreadListener {
	void downloadCompleted(String downloadURL);
}
