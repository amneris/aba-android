package com.abaenglish.videoclass.data;

import android.content.Context;
import android.content.res.AssetManager;

import com.abaenglish.videoclass.ABAApplication;
import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.analytics.TrackerContract;
import com.abaenglish.videoclass.data.network.APIManager;
import com.abaenglish.videoclass.data.network.parser.ABALevelsParser;
import com.abaenglish.videoclass.data.network.parser.ABAUnitParser;
import com.abaenglish.videoclass.data.persistence.ABAAPIError;
import com.abaenglish.videoclass.data.persistence.ABALevel;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.data.persistence.ABAUser;
import com.abaenglish.videoclass.data.persistence.dao.ABALevelDAO;
import com.abaenglish.videoclass.data.persistence.dao.ABAUnitDAO;
import com.abaenglish.videoclass.domain.ApplicationConfiguration;
import com.abaenglish.videoclass.domain.LanguageController;
import com.abaenglish.videoclass.domain.ProgressService;
import com.abaenglish.videoclass.domain.content.DataController;
import com.abaenglish.videoclass.domain.content.DataController.ABASimpleCallback;
import com.abaenglish.videoclass.domain.content.UserController;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.crashlytics.android.Crashlytics;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import io.realm.Realm;

/**
 * Created by Jesus Espejo using mbp13jesus on 01/03/16.
 * Class to handle all the course content
 */
public class CourseContentService {

    private static final String LEVELS_JSON_FILE = "levels.json";

    private static volatile CourseContentService instance;
    private ApplicationConfiguration applicationConfiguration;

    public CourseContentService(ApplicationConfiguration applicationConfiguration) {
        this.applicationConfiguration = applicationConfiguration;
    }

    public void initializeContent(final Context context, final String rawJsonResponse, final ABASimpleCallback callback, TrackerContract.Tracker tracker) {
        initializeContent(context, rawJsonResponse, null, null, null, callback, tracker);
    }

    /**
     * Method that checks levels and units completion and downloads both of then in case they are not
     * cached.
     *
     * @param context
     * @param rawJsonResponse
     * @param email
     * @param facebookId
     * @param callback
     */
    public void initializeContent(final Context context, final String rawJsonResponse, final String name, final String email, final String facebookId, final ABASimpleCallback callback, final TrackerContract.Tracker tracker) {
        try {
            Crashlytics.setString("user email", email);

            // Extracting info from jsonObject
            JSONObject jsonObject = new JSONObject(rawJsonResponse);
            String userLanguage = jsonObject.getString("userLang");
            String token = jsonObject.getString("token");

            applicationConfiguration.setUserToken(token);

            // Making sure the app content is filled
            initializeLevelsWithLanguage(context, userLanguage, new ABASimpleCallback() {

                @Override
                public void onSuccess() {
                    UserController.getInstance().saveUserInfo(rawJsonResponse, name, email, facebookId, new DataController.ABAAPICallback<ABAUser>() {
                        @Override
                        public void onSuccess(ABAUser parsedUser) {

                            // Storing user language and token
                            LanguageController.setCurrentLanguage(context, parsedUser.getUserLang());
                            applicationConfiguration.setUserToken(parsedUser.getToken());

                            // Downloading courser progress and proceeding when finished
                            ProgressService.getInstance().getCourseProgress(parsedUser, callback, tracker);
                        }

                        @Override
                        public void onError(ABAAPIError error) {
                            callback.onError(error);
                        }
                    });
                }

                @Override
                public void onError(ABAAPIError error) {
                    callback.onError(error);
                }
            });

        } catch (JSONException e) {
            callback.onError(new ABAAPIError("Bad JSON from webserver response."));
        } catch (IllegalStateException e) {
            callback.onError(new ABAAPIError(e.getLocalizedMessage()));
        }
    }

    public void setCurrentLevel(final Context aContext, ABALevel level, final ABASimpleCallback callback) {

        final String idLevel = level.getIdLevel();

        Realm realm = Realm.getInstance(ABAApplication.get().getRealmConfiguration());
        ABAUser currentUser = DataStore.getInstance().getUserController().getCurrentUser(realm);

        APIManager.getInstance().setCurrentLevel(idLevel, currentUser.getUserId(), new APIManager.ABAAPIResponseCallback() {

            @Override
            public void onResponse(String response) {
                // Once updated in the WS, we must change it also in Realm database

                Realm realmToUpdateCurrentLevel = Realm.getInstance(ABAApplication.get().getRealmConfiguration());
                ABAUser currentUser = DataStore.getInstance().getUserController().getCurrentUser(realmToUpdateCurrentLevel);
                ABALevel levelFromId = DataStore.getInstance().getLevelUnitController().getABALevelWithId(realmToUpdateCurrentLevel, idLevel);

                realmToUpdateCurrentLevel.beginTransaction();

                currentUser.setCurrentLevel(levelFromId);

                realmToUpdateCurrentLevel.commitTransaction();
                realmToUpdateCurrentLevel.close();

                callback.onSuccess();
            }

            @Override
            public void onError(Exception e) {
                callback.onError(new ABAAPIError(aContext.getString(R.string.errorSettingCurrentUnit)));
            }
        });

        realm.close();
    }


    // ================================================================================
    // Private methods
    // ================================================================================

    private void initializeLevelsWithLanguage(final Context context, final String language, final ABASimpleCallback callback) {
        Realm realmToGetLevels = Realm.getInstance(ABAApplication.get().getRealmConfiguration());

        List<ABALevel> levels = ABALevelDAO.getABALevels(realmToGetLevels);
        final List<ABAUnit> units = ABAUnitDAO.getABAUnits(realmToGetLevels);

        if (levels.size() == 0) {
            initializeLevels(context, new ABASimpleCallback() {
                @Override
                public void onSuccess() {
                    if (units.size() == 0) {
                        initializeUnits(language, callback);
                    } else {
                        callback.onSuccess();
                    }
                }

                @Override
                public void onError(ABAAPIError error) {
                    callback.onError(error);
                }
            });
        } else {

            if (units.size() == 0) {
                initializeUnits(language, callback);
            } else {
                callback.onSuccess();
            }
        }

        realmToGetLevels.close();
    }

    // ================================================================================
    // Private methods - ABALevel
    // ================================================================================

    private void initializeLevels(Context context, final ABASimpleCallback callback) {

        Realm realm = Realm.getInstance(ABAApplication.get().getRealmConfiguration());
        try {
            String rawLevels = fetchLevelsFromJSON(context);

            realm.beginTransaction();
            ABALevelsParser.createABALevels(realm, rawLevels);
            realm.commitTransaction();

        } catch (IOException e) {
            if (realm.isInTransaction()) {
                realm.cancelTransaction();
            }
            callback.onError(new ABAAPIError("levelsInitializer - IOException"));
        } catch (JSONException e) {
            if (realm.isInTransaction()) {
                realm.cancelTransaction();
            }
            callback.onError(new ABAAPIError("levelsInitializer - JSONException"));
        } catch (Exception e) {
            if (realm.isInTransaction()) {
                realm.cancelTransaction();
            }
            callback.onError(new ABAAPIError(e.getLocalizedMessage()));
        }

        realm.close();
        callback.onSuccess();
    }

    private static String fetchLevelsFromJSON(final Context context) throws IOException {

        AssetManager mngr = context.getAssets();
        InputStream is = mngr.open(LEVELS_JSON_FILE);
        int size = is.available();
        byte[] buffer = new byte[size];
        is.read(buffer);
        is.close();

        String jsonString = new String(buffer, "UTF-8");
        return jsonString;
    }

    // ================================================================================
    // Private methods - ABAUnit
    // ================================================================================

    private void initializeUnits(String language, final ABASimpleCallback callback) {

        APIManager.getInstance().getUnits(language, new APIManager.ABAAPIResponseCallback() {

            @Override
            public void onResponse(String response) {

                Realm realm = Realm.getInstance(ABAApplication.get().getRealmConfiguration());
                try {
                    realm.beginTransaction();
                    ABAUnitDAO.deleteUnits(realm);
                    ABAUnitParser.createABAUnits(realm, response);
                    realm.commitTransaction();

                    callback.onSuccess();
                } catch (JSONException ex) {
                    if (realm.isInTransaction()) {
                        realm.cancelTransaction();
                    }
                    callback.onError(new ABAAPIError("Bad JSON from webserver response."));
                } catch (IllegalStateException ex) {
                    if (realm.isInTransaction()) {
                        realm.cancelTransaction();
                    }
                    callback.onError(new ABAAPIError(ex.getLocalizedMessage()));
                }
                realm.close();
            }

            @Override
            public void onError(Exception e) {
                callback.onError(new ABAAPIError("fetchUnits - Exception" + e.getLocalizedMessage()));
            }
        });
    }

}
