package com.abaenglish.videoclass.data.network.retrofit.clients;

import com.abaenglish.videoclass.data.network.oauth.OAuthClient;
import com.abaenglish.videoclass.data.network.oauth.OAuthTokenAccessor;
import com.abaenglish.videoclass.data.network.oauth.RetrofitRxServiceFactory;
import com.abaenglish.videoclass.data.network.retrofit.abaMomentsUtils.ABAMoment;
import com.abaenglish.videoclass.data.network.retrofit.abaMomentsUtils.ABAMomentClientDefinitions;
import com.abaenglish.videoclass.data.network.retrofit.services.ABAMomentService;
import com.abaenglish.videoclass.domain.ApplicationConfiguration;
import com.github.scribejava.core.model.OAuth2AccessToken;

import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Jesus Espejo using mbp13jesus on 19/04/2017.
 */

public class ABAMomentListClient extends OAuthClient<List<ABAMoment>> {

    private ApplicationConfiguration appConfig;

    // ================================================================================
    // Constructor
    // ================================================================================

    public ABAMomentListClient(ApplicationConfiguration appConfig, OAuthTokenAccessor refresher) {
        super(refresher);
        this.appConfig = appConfig;
    }

    // ================================================================================
    // Public methods
    // ================================================================================

    public void fetchABAMomentsList(final OnResponseListener<List<ABAMoment>> listener) {

        final AuthorizedAction<List<ABAMoment>> action = new AuthorizedAction<List<ABAMoment>>() {

            @Override
            public Observable<List<ABAMoment>> actionCall(OAuth2AccessToken oAuth2AccessToken) {
                String authToken = TYPE_BEARER + oAuth2AccessToken.getAccessToken();
                ABAMomentService service = RetrofitRxServiceFactory.createRetrofitRxService(ABAMomentService.class, appConfig.getGatewayUrl());
                return service.getABAMomentsList(authToken);
            }
        };

        performAuthorisedCall(action)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<List<ABAMoment>>() {
                    @Override
                    public void onCompleted() {
                        // Doing nothing for now
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        listener.onResponseReceived(null, ABAMomentClientDefinitions.ABAMomentErrorType.UNKNOWN.toString());
                    }

                    @Override
                    public void onNext(List<ABAMoment> abaMoments) {
                        listener.onResponseReceived(abaMoments, ABAMomentClientDefinitions.ABAMomentErrorType.NONE.toString());
                    }
                });
    }
}
