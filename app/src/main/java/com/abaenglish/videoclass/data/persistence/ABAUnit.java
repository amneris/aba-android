package com.abaenglish.videoclass.data.persistence;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by Marc Güell Segarra on 25/3/15.
 */
public class ABAUnit extends RealmObject {

    @PrimaryKey @Required
    private String          idUnit;
    private String          title;
    private String          desc;
    private String          teacherTip;
    private String          filmImageInactiveUrl;
    private String          filmImageUrl;
    private String          unitImage;
    private String          unitImageInactive;
    private String          videoClassImageUrl;

    private boolean         completed;
    private float           progress;
    private float           unitSectionProgress;
    private Date            lastChanged;
    private ABALevel        level;
    private RealmList<ABARole> roles;

    // TODO: add pending sections
    private ABAFilm sectionFilm;
    private ABAVideoClass sectionVideoClass;
    private ABAInterpret sectionInterpret;
    private ABAExercises sectionExercises;
    private ABAVocabulary sectionVocabulary;
    private ABAWrite sectionWrite;
    private ABASpeak sectionSpeak;
    private ABAEvaluation sectionEvaluation;


    public ABAUnit() {

    }

    /** This field marks if the sections are downloaded */
    private boolean dataDownloaded;

    ABAUnit(String idUnit, String title, String desc, String teacherTip, String filmImageInactiveUrl, String filmImageUrl, String unitImage, String unitImageInactive, String videoClassImageUrl, boolean completed, float progress, float unitSectionProgress, Date lastChanged, ABALevel level, RealmList<ABARole> roles, ABAFilm sectionFilm, ABAVideoClass sectionVideoClass, ABAInterpret sectionInterpret, ABAExercises sectionExercises, ABAVocabulary sectionVocabulary, ABAWrite sectionWrite, ABASpeak sectionSpeak, ABAEvaluation sectionEvaluation, boolean dataDownloaded) {
        this.idUnit = idUnit;
        this.title = title;
        this.desc = desc;
        this.teacherTip = teacherTip;
        this.filmImageInactiveUrl = filmImageInactiveUrl;
        this.filmImageUrl = filmImageUrl;
        this.unitImage = unitImage;
        this.unitImageInactive = unitImageInactive;
        this.videoClassImageUrl = videoClassImageUrl;
        this.completed = completed;
        this.progress = progress;
        this.unitSectionProgress = unitSectionProgress;
        this.lastChanged = lastChanged;
        this.level = level;
        this.roles = roles;
        this.sectionFilm = sectionFilm;
        this.sectionVideoClass = sectionVideoClass;
        this.sectionInterpret = sectionInterpret;
        this.sectionExercises = sectionExercises;
        this.sectionVocabulary = sectionVocabulary;
        this.sectionWrite = sectionWrite;
        this.sectionSpeak = sectionSpeak;
        this.sectionEvaluation = sectionEvaluation;
        this.dataDownloaded = dataDownloaded;
    }

    public String getIdUnit() {
        return idUnit;
    }

    public void setIdUnit(String idUnit) {
        this.idUnit = idUnit;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getTeacherTip() {
        return teacherTip;
    }

    public void setTeacherTip(String teacherTip) {
        this.teacherTip = teacherTip;
    }

    public String getFilmImageInactiveUrl() {
        return filmImageInactiveUrl;
    }

    public void setFilmImageInactiveUrl(String filmImageInactiveUrl) {
        this.filmImageInactiveUrl = filmImageInactiveUrl;
    }

    public String getFilmImageUrl() {
        return filmImageUrl;
    }

    public void setFilmImageUrl(String filmImageUrl) {
        this.filmImageUrl = filmImageUrl;
    }

    public String getUnitImage() {
        return unitImage;
    }

    public void setUnitImage(String unitImage) {
        this.unitImage = unitImage;
    }

    public String getUnitImageInactive() {
        return unitImageInactive;
    }

    public void setUnitImageInactive(String unitImageInactive) {
        this.unitImageInactive = unitImageInactive;
    }

    public String getVideoClassImageUrl() {
        return videoClassImageUrl;
    }

    public void setVideoClassImageUrl(String videoClassImageUrl) {
        this.videoClassImageUrl = videoClassImageUrl;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public float getProgress() {
        return progress;
    }

    public void setProgress(float progress) {
        this.progress = progress;
    }

    public float getUnitSectionProgress() {
        return unitSectionProgress;
    }

    public void setUnitSectionProgress(float unitSectionProgress) {
        this.unitSectionProgress = unitSectionProgress;
    }

    public Date getLastChanged() {
        return lastChanged;
    }

    public void setLastChanged(Date lastChanged) {
        this.lastChanged = lastChanged;
    }

    public ABALevel getLevel() {
        return level;
    }

    public void setLevel(ABALevel level) {
        this.level = level;
    }

    public ABAFilm getSectionFilm() {
        return sectionFilm;
    }

    public void setSectionFilm(ABAFilm sectionFilm) {
        this.sectionFilm = sectionFilm;
    }

    public ABAVideoClass getSectionVideoClass() {
        return sectionVideoClass;
    }

    public void setSectionVideoClass(ABAVideoClass sectionVideoClass) {
        this.sectionVideoClass = sectionVideoClass;
    }

    public RealmList<ABARole> getRoles() {
        return roles;
    }

    public void setRoles(RealmList<ABARole> roles) {
        this.roles = roles;
    }

    public ABAInterpret getSectionInterpret() {
        return sectionInterpret;
    }

    public void setSectionInterpret(ABAInterpret sectionInterpret) {
        this.sectionInterpret = sectionInterpret;
    }

    public ABAExercises getSectionExercises() {
        return sectionExercises;
    }

    public void setSectionExercises(ABAExercises sectionExercises) {
        this.sectionExercises = sectionExercises;
    }

    public ABAVocabulary getSectionVocabulary() {
        return sectionVocabulary;
    }

    public void setSectionVocabulary(ABAVocabulary sectionVocabulary) {
        this.sectionVocabulary = sectionVocabulary;
    }

    public ABAWrite getSectionWrite() {
        return sectionWrite;
    }

    public void setSectionWrite(ABAWrite sectionWrite) {
        this.sectionWrite = sectionWrite;
    }

    public ABASpeak getSectionSpeak() {
        return sectionSpeak;
    }

    public void setSectionSpeak(ABASpeak sectionSpeak) {
        this.sectionSpeak = sectionSpeak;
    }

    public ABAEvaluation getSectionEvaluation() {
        return sectionEvaluation;
    }

    public void setSectionEvaluation(ABAEvaluation sectionEvaluation) {
        this.sectionEvaluation = sectionEvaluation;
    }

    public boolean isDataDownloaded() {
        return dataDownloaded;
    }

    public void setDataDownloaded(boolean dataDownloaded) {
        this.dataDownloaded = dataDownloaded;
    }
}
