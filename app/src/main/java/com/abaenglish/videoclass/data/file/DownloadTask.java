package com.abaenglish.videoclass.data.file;

import com.abaenglish.videoclass.data.DownloadController;
import com.abaenglish.videoclass.domain.datastore.DataStore;
import com.bzutils.LogBZ;

import org.apache.commons.io.IOUtils;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * This is not a real download task.
 * It just sleeps for some random time when it's launched.
 * The idea is not to require a connection and not to eat it.
 */
public class DownloadTask implements Runnable {

    private static final String TAG = DownloadTask.class.getSimpleName();

    private String fileURL;
    private String destinationPath;
    private DownloadController.DownloadCallback downloadCallback;

    public DownloadTask(String fileURL, String destinationPath, DownloadController.DownloadCallback downloadCallback) {
        this.fileURL = fileURL;
        this.destinationPath = destinationPath;
        this.downloadCallback = downloadCallback;
    }

    @Override
    public void run() {
        InputStream input = null;
        OutputStream output = null;
        HttpURLConnection connection = null;
        try {
            URL url = new URL(getRedirectUrl(fileURL));

            connection = (HttpURLConnection) url.openConnection();
            connection.connect();

            // expect HTTP 200 OK, so we don't mistakenly save error report
            // instead of the file
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                //return "Server returned HTTP " + connection.getResponseCode()+ " " + connection.getResponseMessage();
                return;
            }

            // download the file
            input = connection.getInputStream();
            output = new FileOutputStream(destinationPath);

            byte data[] = new byte[4096];
            int count;
            while ((count = input.read(data)) != -1) {
                output.write(data, 0, count);
            }
        } catch (Exception e) {
            LogBZ.printStackTrace(e);
            downloadCallback.downloadError();
            DataStore.getInstance().getDownloadController().getDownloadThread().stopThread();
            DataStore.getInstance().getDownloadController().getDownloadThread().resetTotalCompleted();
        } finally {
            try {
                if (output != null)
                    output.close();
                if (input != null)
                    input.close();
            } catch (IOException ignored) {
                LogBZ.printStackTrace(ignored);
            }

            if (connection != null)
                connection.disconnect();
        }
    }

    public String getFileURL() {
        return fileURL;
    }

    protected String getRedirectUrl(String link) {
        String resultUrl = link;
        HttpURLConnection connection = null;
        try {
            connection = (HttpURLConnection) new URL(link).openConnection();
            connection.setInstanceFollowRedirects(false);
            connection.connect();
            int responseCode = connection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_MOVED_PERM || responseCode == HttpURLConnection.HTTP_MOVED_TEMP) {
                String locationUrl = connection.getHeaderField("Location");

                if (locationUrl != null && locationUrl.trim().length() > 0) {
                    IOUtils.close(connection);
                    resultUrl = getRedirectUrl(locationUrl);
                }
            }
        } catch (Exception e) {
            LogBZ.e("error getRedirectUrl" + e);
        } finally {
            IOUtils.close(connection);
        }
        return resultUrl;
    }
}
