package com.abaenglish.videoclass.data.persistence;

import java.util.Date;

import io.realm.RealmList;

public class ABAUnitBuilder {
    private String idUnit;
    private String title;
    private String desc;
    private String teacherTip;
    private String filmImageInactiveUrl;
    private String filmImageUrl;
    private String unitImage;
    private String unitImageInactive;
    private String videoClassImageUrl;
    private boolean completed;
    private float progress;
    private float unitSectionProgress;
    private Date lastChanged;
    private ABALevel level;
    private RealmList<ABARole> roles;
    private ABAFilm sectionFilm;
    private ABAVideoClass sectionVideoClass;
    private ABAInterpret sectionInterpret;
    private ABAExercises sectionExercises;
    private ABAVocabulary sectionVocabulary;
    private ABAWrite sectionWrite;
    private ABASpeak sectionSpeak;
    private ABAEvaluation sectionEvaluation;
    private boolean dataDownloaded;

    public ABAUnitBuilder setIdUnit(String idUnit) {
        this.idUnit = idUnit;
        return this;
    }

    public ABAUnitBuilder setTitle(String title) {
        this.title = title;
        return this;
    }

    public ABAUnitBuilder setDesc(String desc) {
        this.desc = desc;
        return this;
    }

    public ABAUnitBuilder setTeacherTip(String teacherTip) {
        this.teacherTip = teacherTip;
        return this;
    }

    public ABAUnitBuilder setFilmImageInactiveUrl(String filmImageInactiveUrl) {
        this.filmImageInactiveUrl = filmImageInactiveUrl;
        return this;
    }

    public ABAUnitBuilder setFilmImageUrl(String filmImageUrl) {
        this.filmImageUrl = filmImageUrl;
        return this;
    }

    public ABAUnitBuilder setUnitImage(String unitImage) {
        this.unitImage = unitImage;
        return this;
    }

    public ABAUnitBuilder setUnitImageInactive(String unitImageInactive) {
        this.unitImageInactive = unitImageInactive;
        return this;
    }

    public ABAUnitBuilder setVideoClassImageUrl(String videoClassImageUrl) {
        this.videoClassImageUrl = videoClassImageUrl;
        return this;
    }

    public ABAUnitBuilder setCompleted(boolean completed) {
        this.completed = completed;
        return this;
    }

    public ABAUnitBuilder setProgress(float progress) {
        this.progress = progress;
        return this;
    }

    public ABAUnitBuilder setUnitSectionProgress(float unitSectionProgress) {
        this.unitSectionProgress = unitSectionProgress;
        return this;
    }

    public ABAUnitBuilder setLastChanged(Date lastChanged) {
        this.lastChanged = lastChanged;
        return this;
    }

    public ABAUnitBuilder setLevel(ABALevel level) {
        this.level = level;
        return this;
    }

    public ABAUnitBuilder setRoles(RealmList<ABARole> roles) {
        this.roles = roles;
        return this;
    }

    public ABAUnitBuilder setSectionFilm(ABAFilm sectionFilm) {
        this.sectionFilm = sectionFilm;
        return this;
    }

    public ABAUnitBuilder setSectionVideoClass(ABAVideoClass sectionVideoClass) {
        this.sectionVideoClass = sectionVideoClass;
        return this;
    }

    public ABAUnitBuilder setSectionInterpret(ABAInterpret sectionInterpret) {
        this.sectionInterpret = sectionInterpret;
        return this;
    }

    public ABAUnitBuilder setSectionExercises(ABAExercises sectionExercises) {
        this.sectionExercises = sectionExercises;
        return this;
    }

    public ABAUnitBuilder setSectionVocabulary(ABAVocabulary sectionVocabulary) {
        this.sectionVocabulary = sectionVocabulary;
        return this;
    }

    public ABAUnitBuilder setSectionWrite(ABAWrite sectionWrite) {
        this.sectionWrite = sectionWrite;
        return this;
    }

    public ABAUnitBuilder setSectionSpeak(ABASpeak sectionSpeak) {
        this.sectionSpeak = sectionSpeak;
        return this;
    }

    public ABAUnitBuilder setSectionEvaluation(ABAEvaluation sectionEvaluation) {
        this.sectionEvaluation = sectionEvaluation;
        return this;
    }

    public ABAUnitBuilder setDataDownloaded(boolean dataDownloaded) {
        this.dataDownloaded = dataDownloaded;
        return this;
    }

    public ABAUnit createABAUnit() {
        return new ABAUnit(idUnit, title, desc, teacherTip, filmImageInactiveUrl, filmImageUrl, unitImage, unitImageInactive, videoClassImageUrl, completed, progress, unitSectionProgress, lastChanged, level, roles, sectionFilm, sectionVideoClass, sectionInterpret, sectionExercises, sectionVocabulary, sectionWrite, sectionSpeak, sectionEvaluation, dataDownloaded);
    }
}