package com.abaenglish.videoclass.data.persistence;

import io.realm.RealmObject;
import io.realm.annotations.Required;

/**
 * Created by madalin on 5/10/15.
 */
public class ABAExperiment extends RealmObject {
    @Required
    private String      userExperimentVariationId;
    @Required
    private String      experimentVariationIdentifier;
    @Required
    private String      experimentIdentifier;

    public String getUserExperimentVariationId() {
        return userExperimentVariationId;
    }

    public void setUserExperimentVariationId(String userExperimentVariationId) {
        this.userExperimentVariationId = userExperimentVariationId;
    }

    public String getExperimentVariationIdentifier() {
        return experimentVariationIdentifier;
    }

    public void setExperimentVariationIdentifier(String experimentVariationIdentifier) {
        this.experimentVariationIdentifier = experimentVariationIdentifier;
    }

    public String getExperimentIdentifier() {
        return experimentIdentifier;
    }

    public void setExperimentIdentifier(String experimentIdentifier) {
        this.experimentIdentifier = experimentIdentifier;
    }

}
