package com.abaenglish.videoclass.data.network.parser;

import com.abaenglish.videoclass.data.persistence.ABAEvaluation;
import com.abaenglish.videoclass.data.persistence.ABAExercises;
import com.abaenglish.videoclass.data.persistence.ABAFilm;
import com.abaenglish.videoclass.data.persistence.ABAInterpret;
import com.abaenglish.videoclass.data.persistence.ABALevel;
import com.abaenglish.videoclass.data.persistence.ABARole;
import com.abaenglish.videoclass.data.persistence.ABASpeak;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.data.persistence.ABAVideoClass;
import com.abaenglish.videoclass.data.persistence.ABAVocabulary;
import com.abaenglish.videoclass.data.persistence.ABAWrite;
import com.abaenglish.videoclass.domain.datastore.DataStore;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.UUID;

import io.realm.Realm;

/**
 * Created by Jesus Espejo using mbp13jesus on 01/03/16.
 */
public class ABAUnitParser {

    public static Boolean createABAUnits(Realm realm, String response) throws JSONException {

        JSONArray jsonArray = new JSONArray(response);

        for (int i = 0; i < jsonArray.length(); i++) {       // We iterate the 144 units
            JSONArray unitJsonArray = jsonArray.getJSONArray(i);

            // TOTAL: 7 objects for unit
            if (unitJsonArray.length() != 7) {

                throw new JSONException("Bad JSON from webserver response.");

            } else {

                String idLevel = DataStore.getInstance().getLevelUnitController().getIdLevelForUnitId(unitJsonArray.getJSONObject(0).getString("idUnit"));
                ABALevel level = DataStore.getInstance().getLevelUnitController().getABALevelWithId(realm, idLevel);

                final ABAUnit unit = realm.createObject(ABAUnit.class);

                // We create sections empty objects
                ABAEvaluation abaEvaluation = realm.createObject(ABAEvaluation.class);
                abaEvaluation.setUnit(unit);
                unit.setSectionEvaluation(abaEvaluation);

                ABAExercises abaExercises = realm.createObject(ABAExercises.class);
                abaExercises.setUnit(unit);
                unit.setSectionExercises(abaExercises);

                ABAFilm abaFilm = realm.createObject(ABAFilm.class);
                abaFilm.setAbaFilmID(UUID.randomUUID().toString());
                abaFilm.setUnit(unit);
                unit.setSectionFilm(abaFilm);

                ABAInterpret abaInterpret = realm.createObject(ABAInterpret.class);
                abaInterpret.setUnit(unit);
                unit.setSectionInterpret(abaInterpret);

                ABASpeak abaSpeak = realm.createObject(ABASpeak.class);
                abaSpeak.setUnit(unit);
                unit.setSectionSpeak(abaSpeak);

                ABAVideoClass abaVideoClass = realm.createObject(ABAVideoClass.class);
                abaVideoClass.setUnit(unit);
                unit.setSectionVideoClass(abaVideoClass);

                ABAVocabulary abaVocabulary = realm.createObject(ABAVocabulary.class);
                abaVocabulary.setUnit(unit);
                unit.setSectionVocabulary(abaVocabulary);

                ABAWrite abaWrite = realm.createObject(ABAWrite.class);
                abaWrite.setUnit(unit);
                unit.setSectionWrite(abaWrite);

                for (int j = 0; j < unitJsonArray.length(); j++) {
                    JSONObject unitJsonObject = unitJsonArray.getJSONObject(j);

                    switch (j) {
                        case 0: {
                            // 1 - NSDictionary - Description, Title, idUnit
                            unit.setIdUnit(unitJsonObject.getString("idUnit"));
                            unit.setTitle(unitJsonObject.getString("Title"));
                            unit.setDesc(unitJsonObject.getString("Description"));
                            break;
                        }
                        case 1: {
                            // 2 - NSDictionary - Roles (and inside is a array of different NSDictionaries)
                            JSONArray rolesArray = unitJsonObject.getJSONArray("Roles");
                            HashMap<String, ABARole> abaRolesMap = new HashMap<>();

                            for (int k = 0; k < rolesArray.length(); k++) {
                                JSONObject roleObject = rolesArray.getJSONObject(k);

                                if (abaRolesMap.get(roleObject.getString("RoleName")) == null) {
                                    ABARole role = realm.createObject(ABARole.class);

                                    if (roleObject.has("RoleSmallImageUrl")) {
                                        role.setImageUrl(roleObject.getString("RoleSmallImageUrl"));
                                    }

                                    if (roleObject.has("RoleBigImageUrl")) {
                                        role.setImageBigUrl(roleObject.getString("RoleBigImageUrl"));
                                    }

                                    role.setName(roleObject.getString("RoleName"));
                                    role.setUnit(unit);
                                    unit.getRoles().add(role);

                                    abaRolesMap.put(roleObject.getString("RoleName"), role);
                                } else {
                                    ABARole role = abaRolesMap.get(roleObject.getString("RoleName"));

                                    if (roleObject.has("RoleSmallImageUrl")) {
                                        role.setImageUrl(roleObject.getString("RoleSmallImageUrl"));
                                    }

                                    if (roleObject.has("RoleBigImageUrl")) {
                                        role.setImageBigUrl(roleObject.getString("RoleBigImageUrl"));
                                    }

                                    abaRolesMap.put(roleObject.getString("RoleName"), role);
                                }
                            }

                            break;
                        }
                        case 2: {
                            // 3 - NSDictionary - AbaFilmImage
                            unit.setFilmImageUrl(unitJsonObject.getString("AbaFilmImage"));
                            break;
                        }
                        case 3: {
                            // 4 - NSDictionary - AbaFilmImageInactive
                            unit.setFilmImageInactiveUrl(unitJsonObject.getString("AbaFilmImageInactive"));
                            break;
                        }
                        case 4: {
                            // 5 - NSDictionary - VideoClassImage
                            unit.setVideoClassImageUrl(unitJsonObject.getString("VideoClassImage"));
                            break;
                        }
                        case 5: {
                            // 6 - NSDictionary - UnitImage
                            unit.setUnitImage(unitJsonObject.getString("UnitImage"));
                            break;
                        }
                        case 6: {
                            // 7 - NSDictionary - UnitImageInactive
                            unit.setUnitImageInactive(unitJsonObject.getString("UnitImageInactive"));
                            break;
                        }
                    }
                }

                unit.setLevel(level);
                level.getUnits().add(unit);

            }
        }

        return true;
    }
}
