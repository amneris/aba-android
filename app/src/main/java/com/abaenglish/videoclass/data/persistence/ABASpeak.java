package com.abaenglish.videoclass.data.persistence;

import android.support.annotation.NonNull;

import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.presentation.unit.variation.Section;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by Marc Güell Segarra on 15/4/15.
 */
public class ABASpeak extends RealmObject implements Section {

    private RealmList<ABASpeakDialog> content;
    private ABAPhrase currentPhrase;
    private ABAUnit unit;
    private boolean completed;
    private boolean unlock;
    private float progress;

    @NonNull
    @Override public int getName() {
        return R.string.speakSectionKey;
    }

    @NonNull
    @Override public int getIcon() {
        return R.mipmap.icon_unit_speak;
    }

    @NonNull
    @Override public int getCompletedPercentage() {
        return (int)getProgress();
    }

    @NonNull
    @Override public boolean isUnlocked() {
        return unlock;
    }

    @Override
    public SectionType getType() {
        return SectionType.SPEAK;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public boolean isUnlock() {
        return unlock;
    }

    public void setUnlock(boolean unlock) {
        this.unlock = unlock;
    }

    public float getProgress() {
        return progress;
    }

    public void setProgress(float progress) {
        this.progress = progress;
    }

    public ABAUnit getUnit() {
        return unit;
    }

    public void setUnit(ABAUnit unit) {
        this.unit = unit;
    }

    public RealmList<ABASpeakDialog> getContent() {
        return content;
    }

    public void setContent(RealmList<ABASpeakDialog> content) {
        this.content = content;
    }

    public ABAPhrase getCurrentPhrase() {
        return currentPhrase;
    }


    public void setCurrentPhrase(ABAPhrase currentPhrase) {
        this.currentPhrase = currentPhrase;
    }
}
