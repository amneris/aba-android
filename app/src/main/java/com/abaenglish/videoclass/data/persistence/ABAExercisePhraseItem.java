package com.abaenglish.videoclass.data.persistence;

/**
 * Created by Marc Güell Segarra on 10/6/15.
 */
public class ABAExercisePhraseItem {

    public ABAExercisePhraseItem() {

    }
    private String text;
    private String audioFile;
    private String idPhrase;
    private String page;
    private ABAExercisePhraseItemType type;

    public enum ABAExercisePhraseItemType {
        kABAExercisePhraseItemTypeNormal(1),
        kABAExercisePhraseItemTypeBlank(2);

        private final int value;

        ABAExercisePhraseItemType(final int newValue) {
            value = newValue;
        }

        public int getValue() { return value; }
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public ABAExercisePhraseItemType getType() {
        return type;
    }

    public void setType(ABAExercisePhraseItemType type) {
        this.type = type;
    }

    public String getAudioFile() {
        return audioFile;
    }

    public void setAudioFile(String audioFile) {
        this.audioFile = audioFile;
    }

    public String getIdPhrase() {
        return idPhrase;
    }

    public void setIdPhrase(String idPhrase) {
        this.idPhrase = idPhrase;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }
}
