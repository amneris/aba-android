package com.abaenglish.videoclass.data.network.retrofit.clients;

import android.util.Log;

import com.abaenglish.videoclass.data.network.oauth.OAuthClient;
import com.abaenglish.videoclass.data.network.oauth.OAuthTokenAccessor;
import com.abaenglish.videoclass.data.network.oauth.RetrofitRxServiceFactory;
import com.abaenglish.videoclass.data.network.retrofit.abaMomentsUtils.ABAMomentClientDefinitions;
import com.abaenglish.videoclass.data.network.retrofit.abaMomentsUtils.ABAMomentDetails;
import com.abaenglish.videoclass.data.network.retrofit.services.ABAMomentService;
import com.abaenglish.videoclass.domain.ApplicationConfiguration;
import com.github.scribejava.core.model.OAuth2AccessToken;

import retrofit2.HttpException;
import retrofit2.Response;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by julien on 20/04/2017.
 */
public class ABAMomentClient extends OAuthClient<Response<ABAMomentDetails>> {

    private ApplicationConfiguration appConfig;

    // ================================================================================
    // Constructor
    // ================================================================================

    public ABAMomentClient(ApplicationConfiguration appConfig, OAuthTokenAccessor refresher) {
        super(refresher);
        this.appConfig = appConfig;
    }

    // ================================================================================
    // Public methods
    // ================================================================================

    public void fetchABAMoment(final String abaMomentId, final OnResponseListener<ABAMomentDetails> listener) {


        final AuthorizedAction<Response<ABAMomentDetails>> action = new AuthorizedAction<Response<ABAMomentDetails>>() {

            @Override
            public Observable<Response<ABAMomentDetails>> actionCall(OAuth2AccessToken oAuth2AccessToken) {
                String authToken = TYPE_BEARER + oAuth2AccessToken.getAccessToken();

                final ABAMomentService service = RetrofitRxServiceFactory.createRetrofitRxService(ABAMomentService.class, appConfig.getGatewayUrl());
                Log.e("authToken", authToken);
                return service.getABAMoment(authToken, abaMomentId);
            }
        };

        performAuthorisedCall(action)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<ABAMomentDetails>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        System.out.println("==============================================================");
                        System.out.println("ERROR IN ABAMomentClient : Cause : " + e.getCause() + ", Message : " + e.getMessage() + " class: " + e.getClass());
                        if (e instanceof HttpException) {
                            HttpException httpException = (HttpException) e;
                            switch (httpException.code()) {
                                case 401:
                                    listener.onResponseReceived(null, ABAMomentClientDefinitions.ABAMomentErrorType.TOKEN_INCORRECT.toString());
                                    break;
                                case 403:
                                    listener.onResponseReceived(null, ABAMomentClientDefinitions.ABAMomentErrorType.USER_NOT_EXIST.toString());
                                    break;
                            }
                        }
                        listener.onResponseReceived(null, ABAMomentClientDefinitions.ABAMomentErrorType.UNKNOWN.toString());
                    }

                    @Override
                    public void onNext(Response<ABAMomentDetails> abaMomentDetailsResponse) {
                        // 204 == not in the ABAMoment user set
                        if (abaMomentDetailsResponse.code() == 204) {
                            listener.onResponseReceived(null, ABAMomentClientDefinitions.ABAMomentErrorType.USER_NOT_EXIST.toString());
                        } else {
                            listener.onResponseReceived(abaMomentDetailsResponse.body(), ABAMomentClientDefinitions.ABAMomentErrorType.NONE.toString());
                        }
                    }
                });
    }

}
