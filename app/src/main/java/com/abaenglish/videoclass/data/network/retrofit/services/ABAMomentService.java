package com.abaenglish.videoclass.data.network.retrofit.services;

import com.abaenglish.videoclass.data.network.retrofit.abaMomentsUtils.ABAMoment;
import com.abaenglish.videoclass.data.network.retrofit.abaMomentsUtils.ABAMomentBadge;
import com.abaenglish.videoclass.data.network.retrofit.abaMomentsUtils.ABAMomentDetails;
import com.abaenglish.videoclass.data.network.retrofit.abaMomentsUtils.ABAMomentProgressRequest;
import com.abaenglish.videoclass.data.network.retrofit.abaMomentsUtils.ABAMomentProgressResponse;

import java.util.List;

import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import rx.Observable;

/**
 * API Call for ABAMoments
 * Created by Julien on 01/03/2017.
 */

public interface ABAMomentService {

    @GET("/api/v1/moments/{id}?expand=exercises")
    Observable<Response<ABAMomentDetails>> getABAMoment(@Header("Authorization") String authorization, @Path("id") String id);

    @GET("/api/v1/moments/")
    Observable<List<ABAMoment>> getABAMomentsList(@Header("Authorization") String authorization);

    @POST("/api/v1/moments/progress")
    Observable<Response<ABAMomentProgressResponse>> registerABAMomentProgress(@Header("Authorization") String authorization, @Body ABAMomentProgressRequest request);

    @GET("/api/v1/moments/progress")
    Observable<Response<List<ABAMomentBadge>>> getABAMomentBadge(@Header("Authorization") String authorization);
}
