package com.abaenglish.videoclass.data.persistence.dao;

import com.abaenglish.videoclass.data.persistence.ABAUser;

import io.realm.Realm;
import io.realm.RealmQuery;

/**
 * Created by Jesus Espejo using mbp13jesus on 01/03/16.
 * Class to perform all the DB I/O operations
 */
public class UserDAO {

    public static ABAUser getCurrentUser(Realm realm) {
        RealmQuery<ABAUser> query = realm.where(ABAUser.class);
        return query.findFirst();
    }
}
