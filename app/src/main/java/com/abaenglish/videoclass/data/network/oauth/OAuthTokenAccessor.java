package com.abaenglish.videoclass.data.network.oauth;

import com.abaenglish.videoclass.data.network.oauth.custom.CustomABAOAuthApi;
import com.abaenglish.videoclass.data.network.oauth.custom.CustomABAOAuthService;
import com.abaenglish.videoclass.domain.ApplicationConfiguration;
import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.model.OAuth2AccessToken;

import rx.Observable;
import rx.Subscriber;

/**
 * Created by Jesus Espejo using mbp13jesus on 17/03/17.
 * Class to handle the token creation and update.
 */

public class OAuthTokenAccessor {

    public OAuth2AccessToken oauthToken;
    private ApplicationConfiguration appConfig;

    public OAuthTokenAccessor(ApplicationConfiguration appConfig) {
        this.appConfig = appConfig;
    }

    // ================================================================================
    // PRIVATE METHODS
    // ================================================================================

    public Observable<OAuth2AccessToken> getOauth2Token() {

        return Observable.create(new Observable.OnSubscribe<OAuth2AccessToken>() {

            @Override
            public void call(Subscriber<? super OAuth2AccessToken> subscriber) {

                if (oauthToken != null) {

                    subscriber.onNext(oauthToken);
                    subscriber.onCompleted();

                } else {

                    final CustomABAOAuthService service = new CustomABAOAuthService(new ServiceBuilder()
                            .apiKey(appConfig.getClientId())
                            .apiSecret(appConfig.getClientSecret())
                            .debug()
                            .build(new CustomABAOAuthApi(appConfig.getGatewayUrl())));
                    try {
                        OAuth2AccessToken oAuth2AccessToken = service.getAccessTokenPasswordGrant(appConfig.getUserEmail(), appConfig.getUserToken());
                        oauthToken = oAuth2AccessToken;
                        subscriber.onNext(oAuth2AccessToken);
                        subscriber.onCompleted();
                    } catch (Exception e) {
                        e.printStackTrace();
                        subscriber.onError(e);
                    }
                }
            }
        });
    }

    public Observable<OAuth2AccessToken> refreshOauth2Token() {

        return Observable.create(new Observable.OnSubscribe<OAuth2AccessToken>() {

            @Override
            public void call(Subscriber<? super OAuth2AccessToken> subscriber) {

                final CustomABAOAuthService service = new CustomABAOAuthService(new ServiceBuilder()
                        .apiKey(appConfig.getClientId())
                        .apiSecret(appConfig.getClientSecret())
                        .debug()
                        .build(new CustomABAOAuthApi(appConfig.getGatewayUrl())));
                try {
                    OAuth2AccessToken oAuth2AccessToken = service.refreshAccessToken(oauthToken.getRefreshToken());
                    oauthToken = oAuth2AccessToken;
                    subscriber.onNext(oAuth2AccessToken);
                    System.out.println("==============================================================");
                    System.out.println("OAuthClient NewToken : " + oAuth2AccessToken);
                    subscriber.onCompleted();
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("==============================================================");
                    System.out.println("ERROR IN OAuthClient RefreshToken : Cause : " + e.getCause() + ", Message : " + e.getMessage());
                    subscriber.onError(e);
                }
            }
        });
    }

    public void reset() {
        oauthToken = null;
    }

    // ================================================================================
    // Accessors
    // ================================================================================
}