package com.abaenglish.videoclass.data.persistence;

import android.support.annotation.NonNull;

import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.presentation.unit.variation.Section;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by marc on 13/4/15.
 */
public class ABAExercises extends RealmObject implements Section {

    private ABAUnit unit;
    private RealmList<ABAExercisesGroup> exercisesGroups;
    private boolean completed;
    private boolean unlock;
    private float progress;

    @NonNull
    @Override public int getName() {
        return R.string.exercisesSectionKey;
    }

    @NonNull
    @Override public int getIcon() {
        return R.mipmap.icon_unit_exercise;
    }

    @NonNull
    @Override public int getCompletedPercentage() {
        return (int)getProgress();
    }

    @NonNull
    @Override public boolean isUnlocked() {
        return unlock;
    }

    @Override
    public SectionType getType() {
        return SectionType.EXERCISE;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public boolean isUnlock() {
        return unlock;
    }

    public void setUnlock(boolean unlock) {
        this.unlock = unlock;
    }

    public float getProgress() {
        return progress;
    }

    public void setProgress(float progress) {
        this.progress = progress;
    }

    public ABAUnit getUnit() {
        return unit;
    }

    public void setUnit(ABAUnit unit) {
        this.unit = unit;
    }

    public RealmList<ABAExercisesGroup> getExercisesGroups() {
        return exercisesGroups;
    }

    public void setExercisesGroups(RealmList<ABAExercisesGroup> exercisesGroups) {
        this.exercisesGroups = exercisesGroups;
    }
}
