package com.abaenglish.videoclass.data.persistence;

import android.support.annotation.NonNull;

import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.presentation.unit.variation.Section;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by Marc Güell Segarra on 15/4/15.
 */
public class ABAEvaluation extends RealmObject implements Section {

    private ABAUnit unit;
    private RealmList<ABAEvaluationQuestion> content;
    private boolean completed;
    private boolean unlock;
    private float progress;

    @NonNull
    @Override public int getName() {
        return R.string.evaluaKey;
    }

    @NonNull
    @Override public int getIcon() {
        return R.mipmap.icon_unit_evaluation;
    }

    @NonNull
    @Override public int getCompletedPercentage() {
        return (int)getProgress();
    }

    @NonNull
    @Override public boolean isUnlocked() {
        return unlock;
    }

    @Override
    public SectionType getType() {
        return SectionType.ASSESSMENT;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public boolean isUnlock() {
        return unlock;
    }

    public void setUnlock(boolean unlock) {
        this.unlock = unlock;
    }

    public float getProgress() {
        return progress;
    }

    public void setProgress(float progress) {
        this.progress = progress;
    }

    public ABAUnit getUnit() {
        return unit;
    }

    public void setUnit(ABAUnit unit) {
        this.unit = unit;
    }

    public RealmList<ABAEvaluationQuestion> getContent() {
        return content;
    }

    public void setContent(RealmList<ABAEvaluationQuestion> content) {
        this.content = content;
    }
}
