package com.abaenglish.videoclass.data.persistence;

import android.support.annotation.NonNull;

import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.presentation.unit.variation.Section;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by Marc Güell Segarra on 10/4/15.
 */
public class ABAInterpret extends RealmObject implements Section {

    private RealmList<ABAInterpretRole> roles;
    private RealmList<ABAPhrase> dialog;
    private ABAUnit unit;
    private boolean completed;
    private boolean unlock;
    private float progress;

    @NonNull
    @Override public int getName() {
        return R.string.interpretSectionKey;
    }

    @NonNull
    @Override public int getIcon() {
        return R.mipmap.icon_unit_interpret;
    }

    @NonNull
    @Override public int getCompletedPercentage() {
        return (int)getProgress();
    }

    @NonNull
    @Override public boolean isUnlocked() {
        return unlock;
    }

    @Override
    public SectionType getType() {
        return SectionType.INTERPRET;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public boolean isUnlock() {
        return unlock;
    }

    public void setUnlock(boolean unlock) {
        this.unlock = unlock;
    }

    public float getProgress() {
        return progress;
    }

    public void setProgress(float progress) {
        this.progress = progress;
    }

    public RealmList<ABAInterpretRole> getRoles() {
        return roles;
    }

    public void setRoles(RealmList<ABAInterpretRole> roles) {
        this.roles = roles;
    }

    public RealmList<ABAPhrase> getDialog() {
        return dialog;
    }

    public void setDialog(RealmList<ABAPhrase> dialog) {
        this.dialog = dialog;
    }

    public ABAUnit getUnit() {
        return unit;
    }

    public void setUnit(ABAUnit unit) {
        this.unit = unit;
    }
}
