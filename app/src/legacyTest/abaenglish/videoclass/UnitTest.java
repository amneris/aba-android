package com.abaenglish.videoclass;

import android.content.Context;

import com.abaenglish.utils.TestUtils;
import com.abaenglish.videoclass.data.network.APIManager;
import com.abaenglish.videoclass.data.persistence.ABAAPIError;
import com.abaenglish.videoclass.data.persistence.ABALevel;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.domain.content.DataController;
import com.abaenglish.videoclass.domain.content.LevelUnitController;
import com.abaenglish.videoclass.session.SessionService;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import io.realm.Realm;

/**
 * Created by madalin on 23/11/15.
 */
public class UnitTest extends ABABaseTestCase {

    private SessionService mSessionService;
    private LevelUnitController mLevelUnitController;
    private Realm realm;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mSessionService = new SessionService();
        mLevelUnitController = new LevelUnitController();

        final CountDownLatch signal = new CountDownLatch(1);
        Context aContext = getInstrumentation().getTargetContext();

        APIManager apiManager = new APIManager();
        apiManager.setApplicationContext(aContext);
        mSessionService.loginWithEmail(aContext, PREMIUM_USER_EMAIL, PREMIUM_USER_PASSWORD, new DataController.ABASimpleCallback() {
            @Override
            public void onSuccess() {
                signal.countDown();
            }

            @Override
            public void onError(ABAAPIError error) {
                signal.countDown();
            }
        });
        signal.await();
        realm = Realm.getInstance(getRealmConfiguration());
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        if (realm != null)
            realm.close();
    }

    public void testHasLevels() throws InterruptedException {
        assertEquals(mLevelUnitController.getAllLevelsDescending(realm).size(), TestUtils.NUMBER_OF_LEVEL);
    }

    public void testLevelsHaveAllUnits() throws InterruptedException {
        List<ABALevel> levelsArrayList = mLevelUnitController.getAllLevelsDescending(realm);
        assertNotNull(levelsArrayList);

        for (ABALevel level : levelsArrayList) {
            assertEquals(level.getUnits().size(), TestUtils.NUMBER_OF_UNIT_PER_LEVEL);
        }
    }

    public void testHasAllUnits() throws InterruptedException {
        List<ABALevel> levelsArrayList = mLevelUnitController.getAllLevelsDescending(realm);
        assertNotNull(levelsArrayList);

        int numberOfUnits = 0;
        for (ABALevel level : levelsArrayList) {
            numberOfUnits = numberOfUnits + level.getUnits().size();
        }
        assertEquals(numberOfUnits, TestUtils.NUMBER_OF_UNITS);
    }

    public void testCompleteUnit() throws InterruptedException {
        ABAUnit unit = TestUtils.getRandomUnit(mLevelUnitController.getAllLevelsDescending(realm));
        realm.beginTransaction();
        mLevelUnitController.setCompletedUnit(realm, unit);
        realm.commitTransaction();
        assertNotSame(mLevelUnitController.getCompleteUnits(realm), 0);

        boolean unitFound = false;
        for (ABAUnit completedUnit : getCompletedUnits(realm)) {
            if (completedUnit.getIdUnit().equals(unit.getIdUnit())) {
                unitFound = true;
            }
        }

        assertTrue(unitFound);
        assertEquals((int) unit.getProgress(), 100);
        assertEquals(unit.isCompleted(), true);
    }

    public void testIntegrationEnvironmentIsSet() {
        checkIntegrationEnvironmentIsSet();
    }


        // ================================================================================
    // Private methods not to test
    // ================================================================================

    private ArrayList<ABAUnit> getCompletedUnits(Realm realm) {
        ArrayList<ABAUnit> completedUnits = new ArrayList<>();

        for (ABALevel level : mLevelUnitController.getAllLevelsDescending(realm)) {
            for (ABAUnit unit : level.getUnits()) {
                if (unit.isCompleted() || unit.getProgress() == 100)
                    completedUnits.add(unit);
            }
        }
        return completedUnits;
    }
}
