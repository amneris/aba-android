package com.abaenglish.videoclass.domain.content;

import android.content.Context;

import com.abaenglish.utils.TestUtils;
import com.abaenglish.videoclass.ABASectionTestCase;
import com.abaenglish.videoclass.data.persistence.ABAAPIError;
import com.abaenglish.videoclass.data.persistence.ABAFilm;
import com.abaenglish.videoclass.data.persistence.ABAUnit;

import junit.framework.AssertionFailedError;

import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;

import io.realm.Realm;

/**
 * Created by madalin on 27/11/15.
 */
public class FilmControllerTest extends ABASectionTestCase {

    private LevelUnitController levelUnitController = new LevelUnitController();
    private FilmController filmController = new FilmController();

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testCompleteAbaFilmSection() throws InterruptedException {
        Context aContext = getInstrumentation().getTargetContext();
        final CountDownLatch signal = new CountDownLatch(1);
        final boolean[] succeeded = {false};

        Realm realmToCompleteABAFilm = Realm.getInstance(getRealmConfiguration());

        ABAUnit randomUnit = TestUtils.getRandomUnit(levelUnitController.getAllLevelsDescending(realmToCompleteABAFilm));
        final String randomUnitID = randomUnit.getIdUnit();
        setRandomCurrentLenguage(aContext);

        levelUnitController.getSectionsforUnit(aContext, randomUnit, new DataController.ABASimpleCallback() {
            @Override
            public void onSuccess() {
                Realm realm = Realm.getInstance(getRealmConfiguration());
                try {
                    ABAUnit currentUnit = levelUnitController.getUnitWithId(realm, randomUnitID);
                    filmController.setCompletedSection(realm, currentUnit.getSectionFilm());

                    assertTrue(currentUnit.getSectionFilm().isCompleted());
                    assertTrue(filmController.isSectionCompleted(currentUnit.getSectionFilm()));

                    assertEquals((int) currentUnit.getSectionFilm().getProgress(), 100);
                    assertEquals((int) filmController.getProgressForSection(currentUnit.getSectionFilm()), 100);
                    assertEquals(filmController.getPercentageForSection(currentUnit.getSectionFilm()), "100%");

                    succeeded[0] = true;
                    signal.countDown();
                } catch (AssertionFailedError e) {
                    signal.countDown();
                }
                realm.close();
            }

            @Override
            public void onError(ABAAPIError error) {
                signal.countDown();
            }
        });
        signal.await();
        realmToCompleteABAFilm.close();
        assertTrue(succeeded[0]);
    }

    public void testDownloadSubtitlesForABAFilm() throws InterruptedException {
        String randomUnitID = getAllSectionsForRandomUnit();
        Realm realmToDownloadSubs = Realm.getInstance(getRealmConfiguration());

        ArrayList<String> subtitlesUrlArray = new ArrayList<>();
        ABAUnit currentUnit = levelUnitController.getUnitWithId(realmToDownloadSubs, randomUnitID);
        ABAFilm abaFilmSection = filmController.getSectionForUnit(currentUnit);

        subtitlesUrlArray.add(abaFilmSection.getEnglishSubtitles());
        subtitlesUrlArray.add(abaFilmSection.getTranslatedSubtitles());
        boolean subtitlesDownloaded = downloadAllSubtitleFromVideosSection(subtitlesUrlArray);

        realmToDownloadSubs.close();
        assertTrue(subtitlesDownloaded);
    }
}
