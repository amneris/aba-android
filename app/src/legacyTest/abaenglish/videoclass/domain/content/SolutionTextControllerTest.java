package com.abaenglish.videoclass.domain.content;


import com.abaenglish.videoclass.ABABaseTestCase;

import java.util.HashMap;

/**
 * Created by madalin on 27/11/15.
 */
public class SolutionTextControllerTest extends ABABaseTestCase {

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testSolutionTextValidator() throws InterruptedException {
        String userPhrase, correctPhrase;

        userPhrase = "I'm very happy tonight, tom";
        correctPhrase = "I am very happy tonight, Tom";
        assertFalse(checkPhraseWithCorrectPhrase(userPhrase, correctPhrase));

        userPhrase = "I´m very happy tonight, Tom";
        correctPhrase = "I'm very happy tonight, Tom";
        assertTrue(checkPhraseWithCorrectPhrase(userPhrase, correctPhrase));

        userPhrase = "It is monday";
        correctPhrase = "It´s Monday";
        assertFalse(checkPhraseWithCorrectPhrase(userPhrase, correctPhrase));

        userPhrase = "It´s Monday";
        correctPhrase = "It is Monday";
        assertTrue(checkPhraseWithCorrectPhrase(userPhrase, correctPhrase));

        userPhrase = "Today´s February and I like tomatoes in the street";
        correctPhrase = "Today is February and I like tomatoes in the street";
        assertTrue(checkPhraseWithCorrectPhrase(userPhrase, correctPhrase));

        userPhrase = "Today´s february and I like tomatoes in the street";
        correctPhrase = "Today is February and I like tomatoes in the street";
        assertFalse(checkPhraseWithCorrectPhrase(userPhrase, correctPhrase));

        userPhrase = "Today is February and i like tomatoes in the street";
        correctPhrase = "Today´s February and I like tomatoes in the street";
        assertFalse(checkPhraseWithCorrectPhrase(userPhrase, correctPhrase));

        userPhrase = "Hello, is this a test phrase with question mark?";
        correctPhrase = "Hello, is this a test phrase with question mark?";
        assertTrue(checkPhraseWithCorrectPhrase(userPhrase, correctPhrase));


        userPhrase = "Hello is this a test phrase with question mark";
        correctPhrase = "Hello, is this a test phrase with question mark?";
        assertTrue(checkPhraseWithCorrectPhrase(userPhrase, correctPhrase));

        userPhrase = "They´re happy!";
        correctPhrase = "They are happy!";
        assertTrue(checkPhraseWithCorrectPhrase(userPhrase, correctPhrase));

        userPhrase = "Cote cafe cafes facades";
        correctPhrase = "Côte café cafés façades";
        assertTrue(checkPhraseWithCorrectPhrase(userPhrase, correctPhrase));

        userPhrase = "Côte café cafés façades";
        correctPhrase = "Côte café cafés façades";
        assertTrue(checkPhraseWithCorrectPhrase(userPhrase, correctPhrase));

    }

    // ================================================================================
    // Private methods not to test
    // ================================================================================

    private boolean checkPhraseWithCorrectPhrase(String userPhrase, String correctPhrase) {
        SolutionTextController solutionTextController = new SolutionTextController();
        String answerFormatted = solutionTextController.formatSolutionText(userPhrase);
        HashMap<Integer, String> errorDict = solutionTextController.checkSolutionText(answerFormatted, correctPhrase);
        return !solutionTextController.checkPhrase(answerFormatted, errorDict);
    }
}
