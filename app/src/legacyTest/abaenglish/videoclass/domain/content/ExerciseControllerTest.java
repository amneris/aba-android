package com.abaenglish.videoclass.domain.content;

import android.content.Context;

import com.abaenglish.utils.TestUtils;
import com.abaenglish.videoclass.ABASectionTestCase;
import com.abaenglish.videoclass.data.persistence.ABAAPIError;
import com.abaenglish.videoclass.data.persistence.ABAExercises;
import com.abaenglish.videoclass.data.persistence.ABAExercisesQuestion;
import com.abaenglish.videoclass.data.persistence.ABAUnit;

import junit.framework.AssertionFailedError;

import java.util.concurrent.CountDownLatch;

import io.realm.Realm;

/**
 * Created by madalin on 27/11/15.
 */
public class ExerciseControllerTest extends ABASectionTestCase {

    private LevelUnitController levelUnitController = new LevelUnitController();
    private ExercisesController exercisesController = new ExercisesController();

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testCompleteExerciseSection() throws InterruptedException {
        Context aContext = getInstrumentation().getTargetContext();
        final CountDownLatch signal = new CountDownLatch(1);
        final boolean[] succeeded = {false};

        Realm realmToCompleteExercises = Realm.getInstance(getRealmConfiguration());

        ABAUnit randomUnit = TestUtils.getRandomUnit(levelUnitController.getAllLevelsDescending(realmToCompleteExercises));
        final String randomUnitID = randomUnit.getIdUnit();
        setRandomCurrentLenguage(aContext);

        levelUnitController.getSectionsforUnit(aContext, randomUnit, new DataController.ABASimpleCallback() {
            @Override
            public void onSuccess() {
                Realm realm = Realm.getInstance(getRealmConfiguration());
                try {
                    ABAUnit currentUnit = levelUnitController.getUnitWithId(realm, randomUnitID);
                    completeSection(realm, currentUnit, exercisesController);

                    assertTrue(currentUnit.getSectionExercises().isCompleted());
                    assertEquals((int) currentUnit.getSectionExercises().getProgress(), 100);

                    succeeded[0] = true;
                    signal.countDown();
                } catch (AssertionFailedError e) {
                    signal.countDown();
                }

                realm.close();
            }

            @Override
            public void onError(ABAAPIError error) {
                signal.countDown();
            }
        });

        signal.await();
        assertTrue(succeeded[0]);
        realmToCompleteExercises.close();
    }

    public void testGetPhrasesFromExercisesSection() throws InterruptedException {
        Context aContext = getInstrumentation().getTargetContext();
        final CountDownLatch signal = new CountDownLatch(1);
        final boolean[] succeeded = {false};

        Realm realmToGetPhrases = Realm.getInstance(getRealmConfiguration());

        ABAUnit randomUnit = TestUtils.getRandomUnit(levelUnitController.getAllLevelsDescending(realmToGetPhrases));
        final String randomUnitID = randomUnit.getIdUnit();

        levelUnitController.getSectionsforUnit(aContext, randomUnit, new DataController.ABASimpleCallback() {
            @Override
            public void onSuccess() {
                Realm realm = Realm.getInstance(getRealmConfiguration());
                try {
                    ABAUnit currentUnit = levelUnitController.getUnitWithId(realm, randomUnitID);
                    assertNotSame(currentUnit.getSectionExercises().getExercisesGroups().size(), 0);

                    succeeded[0] = true;
                    signal.countDown();
                } catch (AssertionFailedError e) {
                    signal.countDown();
                }
                realm.close();
            }

            @Override
            public void onError(ABAAPIError error) {
                signal.countDown();
            }
        });

        signal.await();
        assertTrue(succeeded[0]);
        realmToGetPhrases.close();
    }

    public void testCompleteAllPhrasesFromExercisesSection() throws InterruptedException {
        String randomUnitID = getAllSectionsForRandomUnit();
        Realm realmToGetPhrases = Realm.getInstance(getRealmConfiguration());

        ABAExercises exercisesSection = exercisesController.getSectionForUnit(levelUnitController.getUnitWithId(realmToGetPhrases, randomUnitID));
        while(exercisesController.getCurrentABAExercisesQuestion(exercisesSection) != null) {
            ABAExercisesQuestion exercisesQuestion = exercisesController.getCurrentABAExercisesQuestion(exercisesSection);
            exercisesController.setBlankPhrasesDoneForExercisesQuestion(realmToGetPhrases, exercisesQuestion, exercisesSection);
        }

        assertTrue(exercisesController.isSectionCompleted(exercisesSection));
        assertTrue(exercisesController.allGroupsAreCompleted(exercisesSection));
        assertEquals(exercisesController.getTotalElementsForSection(exercisesSection), exercisesController.getElementsCompletedForSection(exercisesSection));
        assertEquals((int) exercisesController.getProgressForSection(exercisesSection), 100);
        assertEquals(exercisesController.getPercentageForSection(exercisesSection), "100%");

        realmToGetPhrases.close();
    }
}
