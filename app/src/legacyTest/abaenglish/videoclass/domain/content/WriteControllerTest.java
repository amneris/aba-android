package com.abaenglish.videoclass.domain.content;

import android.content.Context;

import com.abaenglish.utils.TestUtils;
import com.abaenglish.videoclass.ABASectionTestCase;
import com.abaenglish.videoclass.data.persistence.ABAAPIError;
import com.abaenglish.videoclass.data.persistence.ABAPhrase;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.data.persistence.ABAWrite;

import junit.framework.AssertionFailedError;

import java.util.List;
import java.util.concurrent.CountDownLatch;

import io.realm.Realm;

/**
 * Created by madalin on 27/11/15.
 */
public class WriteControllerTest extends ABASectionTestCase {

    private LevelUnitController levelUnitController = new LevelUnitController();
    private WriteController writeController = new WriteController();

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testCompleteWriteSection() throws InterruptedException {
        Context aContext = getInstrumentation().getTargetContext();
        final CountDownLatch signal = new CountDownLatch(1);
        final boolean[] succeeded = {false};

        Realm realmToCompleteWrite = Realm.getInstance(getRealmConfiguration());

        ABAUnit randomUnit = TestUtils.getRandomUnit(levelUnitController.getAllLevelsDescending(realmToCompleteWrite));
        final String randomUnitID = randomUnit.getIdUnit();
        setRandomCurrentLenguage(aContext);

        levelUnitController.getSectionsforUnit(aContext, randomUnit, new DataController.ABASimpleCallback() {
            @Override
            public void onSuccess() {
                Realm realm = Realm.getInstance(getRealmConfiguration());
                try {
                    ABAUnit currentUnit = levelUnitController.getUnitWithId(realm, randomUnitID);

                    new WriteController().setCompletedSection(realm, currentUnit.getSectionWrite());
                    assertTrue(currentUnit.getSectionWrite().isCompleted());
                    assertEquals((int) currentUnit.getSectionWrite().getProgress(), 100);

                    succeeded[0] = true;
                    signal.countDown();
                } catch (AssertionFailedError e) {
                    signal.countDown();
                }
                realm.close();
            }

            @Override
            public void onError(ABAAPIError error) {
                signal.countDown();
            }
        });
        signal.await();
        assertTrue(succeeded[0]);
        realmToCompleteWrite.close();
    }

    public void testGetPhrasesFromWriteSection() throws InterruptedException {
        Context aContext = getInstrumentation().getTargetContext();
        final CountDownLatch signal = new CountDownLatch(1);
        final boolean[] succeeded = {false};

        Realm realmToGetPhrases = Realm.getInstance(getRealmConfiguration());

        ABAUnit randomUnit = TestUtils.getRandomUnit(levelUnitController.getAllLevelsDescending(realmToGetPhrases));
        final String randomUnitID = randomUnit.getIdUnit();
        setRandomCurrentLenguage(aContext);

        levelUnitController.getSectionsforUnit(aContext, randomUnit,
                new DataController.ABASimpleCallback() {
            @Override
            public void onSuccess() {
                Realm realm = Realm.getInstance(getRealmConfiguration());
                try {
                    ABAUnit currentUnit = levelUnitController.getUnitWithId(realm, randomUnitID);
                    assertNotSame(currentUnit.getSectionWrite().getContent().size(), 0);

                    succeeded[0] = true;
                    signal.countDown();
                } catch (AssertionFailedError e) {
                    signal.countDown();
                }
                realm.close();
            }

            @Override
            public void onError(ABAAPIError error) {
                signal.countDown();
            }
        });
        signal.await();
        assertTrue(succeeded[0]);
        realmToGetPhrases.close();
    }

    public void testToGetAllAudiosIdForWritePhrases() throws InterruptedException {
        Realm realmToGetAudios = Realm.getInstance(getRealmConfiguration());
        String randomUnitID = getAllSectionsForRandomUnit();

        ABAWrite writeSection = writeController.getSectionForUnit(levelUnitController.getUnitWithId(realmToGetAudios, randomUnitID));
        assertEquals(writeController.getAllAudioIDsForSection(writeSection).size(), writeController.getTotalElementsForSection(writeSection) / 2);

        List<String> audiosList = writeController.getAllAudioIDsForSection(writeSection);
        assertFalse(audiosList.contains(null));
        assertFalse(audiosList.contains(""));

        realmToGetAudios.close();
    }

    public void testCompleteAllPhrasesFromWriteSection() throws InterruptedException {
        String randomUnitID = getAllSectionsForRandomUnit();
        Realm realmToCompletePhrases = Realm.getInstance(getRealmConfiguration());

        ABAWrite writeSection = writeController.getSectionForUnit(levelUnitController.getUnitWithId(realmToCompletePhrases, randomUnitID));
        while (writeController.getCurrentPhrase(writeSection) != null) {
            ABAPhrase currentPhrase = writeController.getCurrentPhrase(writeSection);
            assertEquals(writeController.phraseWithID(currentPhrase.getIdPhrase(), currentPhrase.getPage(), writeSection), currentPhrase);

            writeController.setPhraseDone(realmToCompletePhrases, currentPhrase, writeSection, false);
            assertTrue(currentPhrase.isDone() && currentPhrase.isListened());
        }

        boolean isSectionCompleted = writeController.isSectionCompleted(writeSection);
        boolean elementsCompletedAreEqual = writeController.getTotalElementsForSection(writeSection).equals(writeController.getElementsCompletedForSection(writeSection));
        int progressForSection = (int) writeController.getProgressForSection(writeSection);
        String percentageForSection = writeController.getPercentageForSection(writeSection);

        realmToCompletePhrases.close();

        assertTrue(isSectionCompleted);
        assertTrue(elementsCompletedAreEqual);
        assertEquals(100, progressForSection);
        assertEquals("100%", percentageForSection);
    }
}
