package com.abaenglish.videoclass.domain.content;


import com.abaenglish.utils.TestUtils;
import com.abaenglish.videoclass.ABASectionTestCase;
import com.abaenglish.videoclass.data.network.APIManager;
import com.abaenglish.videoclass.data.persistence.ABAAPIError;
import com.abaenglish.videoclass.data.persistence.ABAProgressAction;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.data.persistence.ABAUser;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CountDownLatch;

import io.realm.Realm;

/**
 * Created by madalin on 30/11/15.
 */
public class ProgressActionControllerTest extends ABASectionTestCase {

    private LevelUnitController mLevelUnitController;
    private ProgressActionController mProgressActionController;
    private UserController mUserController = new UserController();

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        mLevelUnitController = new LevelUnitController();
        mUserController = new UserController();
        mProgressActionController = new ProgressActionController();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testGenerateProgressAction() throws InterruptedException {
        Realm realmToCreateActions = Realm.getInstance(getRealmConfiguration());
        ABAUser currentUser = mUserController.getCurrentUser(realmToCreateActions);
        ABAUnit randomUnit = TestUtils.getRandomUnit(mLevelUnitController.getAllLevelsDescending(realmToCreateActions));

        ABAProgressAction progressAction = mProgressActionController.generateProgressAction(null, randomUnit, currentUser);
        assertNotNull(progressAction);
        assertNotNull(ProgressActionController.toMap(progressAction));
        realmToCreateActions.close();
    }

    public void testCreateProgressAction() throws InterruptedException {
        Realm realmToCreateActions = Realm.getInstance(getRealmConfiguration());
        ABAUser currentUser = mUserController.getCurrentUser(realmToCreateActions);

        ABAProgressAction progressAction = mProgressActionController.createProgressAction(null, null, currentUser, null, false, false, null);
        List<Map<String, Object>> actionMaps = new ArrayList<>();
        actionMaps.add(ProgressActionController.toMap(progressAction));

        assertNotNull(progressAction);
        assertNotNull(actionMaps);
        ProgressActionController.saveProgressAction(progressAction);
        assertNotNull(ProgressActionController.getPendingProgressActions(realmToCreateActions));

        final boolean[] succeeded = {false};
        final CountDownLatch signalForFirstChange = new CountDownLatch(1);
        APIManager.getInstance().sendProgressActionsToServer(actionMaps, new DataController.ABAAPICallback<JSONArray>() {

            @Override
            public void onSuccess(JSONArray response) {
                succeeded[0] = true;
                signalForFirstChange.countDown();
            }

            @Override
            public void onError(ABAAPIError error) {
                signalForFirstChange.countDown();
            }
        });
        signalForFirstChange.await();
        realmToCreateActions.close();
        assertTrue(succeeded[0]);
    }
}
