package com.abaenglish.videoclass.domain.content;

import android.content.Context;

import com.abaenglish.videoclass.ABASectionTestCase;
import com.abaenglish.videoclass.data.DownloadController;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.domain.datastore.DataStore;

import java.io.File;
import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;

import io.realm.Realm;

/**
 * Created by madalin on 1/12/15.
 */
public class DownloadControllerTest extends ABASectionTestCase {

    private LevelUnitController levelUnitController = new LevelUnitController();
    private DownloadController downloadController = new DownloadController();

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testDownloadUnit() throws InterruptedException {
        final Context aContext = getInstrumentation().getTargetContext();
        final CountDownLatch signal = new CountDownLatch(1);
        final boolean[] succeeded = {false};

        File abaEnglishNoMediaFolder = new File(LevelUnitController.abaFolderNoMediaPath);

        File abaEnglishFolderPath = new File(GenericController.abaEnglishFolderPath);
        GenericController.deleteDirectory(abaEnglishNoMediaFolder);
        GenericController.deleteDirectory(abaEnglishFolderPath);

        final String randomUnitID = getAllSectionsForRandomUnit();

        Realm realmToDownloadUnit = Realm.getInstance(getRealmConfiguration());

        final ABAUnit randomUnit = levelUnitController.getUnitWithId(realmToDownloadUnit, randomUnitID);
        ArrayList<String> totalElementsForDownload = downloadController.getTotalElementsForDownloadAtUnit(realmToDownloadUnit, randomUnit);
        assertFalse(downloadController.isAllFileDownloaded(randomUnit, totalElementsForDownload));

        downloadController.getDownloadThread().startThread();
        downloadSectionContent(realmToDownloadUnit, randomUnit, new DownloadController.DownloadCallback() {
            @Override
            public void downloadSuccess() {
                /* Realm realm = Realm.getInstance(getRealmConfiguration());
                ABAUnit currentUnit = levelUnitController.getUnitWithId(realm, randomUnitID);
                ArrayList<String> totalElementsForDownload = downloadController.getTotalElementsForDownloadAtUnit(realm, currentUnit);
                ArrayList<String> arrayWithDownloadFilesError = downloadController.getArrayWithDownloadFilesError(currentUnit, totalElementsForDownload);

                if (!arrayWithDownloadFilesError.isEmpty()) {
                    LogBZ.e("Running Test ->> testDownloadUnit with Error Urls : " + arrayWithDownloadFilesError
                            + " and ContentLanguage: " + LanguageController.getCurrentLanguage(aContext).toUpperCase());
                }

                assertTrue(downloadController.isAllFileDownloaded(currentUnit, totalElementsForDownload));
                realm.close(); */

                succeeded[0] = true;
                signal.countDown();
            }

            @Override
            public void downloadError() {
                signal.countDown();
            }
        });
        signal.await();
        assertTrue(succeeded[0]);
        realmToDownloadUnit.close();
    }

    public void downloadSectionContent(Realm realm, ABAUnit currentUnit, DownloadController.DownloadCallback callback) throws InterruptedException {

        //download all sectionFiles
        ArrayList<String> totalElementsForDownload = downloadController.getTotalElementsForDownloadAtUnit(realm, currentUnit);
        downloadController.setTotalFileForDownload(downloadController.getAllFilesForDownload(currentUnit, totalElementsForDownload));
        String teacherImageUrl = DataStore.getInstance().getUserController().getCurrentUser(realm).getTeacherImage();

        for (String url : totalElementsForDownload) {
            CountDownLatch signal = new CountDownLatch(1);
            if (url.equals(teacherImageUrl)) {
                //Download Teacher
                downloadController.downloadFile(url, GenericController.getLocalFilePath(null, url), callback, signal);
            } else {
                if (!GenericController.checkIfFileExist(currentUnit, url)) {
                    //Download other Files
                    downloadController.downloadFile(url, GenericController.getLocalFilePath(currentUnit, url), callback, signal);
                }
            }
            signal.await();
        }
    }
}
