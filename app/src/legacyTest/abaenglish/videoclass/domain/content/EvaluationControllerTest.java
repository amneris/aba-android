package com.abaenglish.videoclass.domain.content;

import android.content.Context;

import com.abaenglish.utils.TestUtils;
import com.abaenglish.videoclass.ABASectionTestCase;
import com.abaenglish.videoclass.data.persistence.ABAAPIError;
import com.abaenglish.videoclass.data.persistence.ABAEvaluation;
import com.abaenglish.videoclass.data.persistence.ABAEvaluationOption;
import com.abaenglish.videoclass.data.persistence.ABAEvaluationQuestion;
import com.abaenglish.videoclass.data.persistence.ABAUnit;

import junit.framework.AssertionFailedError;

import java.util.concurrent.CountDownLatch;

import io.realm.Realm;

/**
 * Created by madalin on 27/11/15.
 */
public class EvaluationControllerTest extends ABASectionTestCase {

    private LevelUnitController levelUnitController = new LevelUnitController();
    private EvaluationController evaluationController = new EvaluationController();

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testCompleteEvaluationSection() throws InterruptedException {
        Context aContext = getInstrumentation().getTargetContext();
        final CountDownLatch signal = new CountDownLatch(1);
        final boolean[] succeeded = {false};

        Realm realmToCompleteEvaluations = Realm.getInstance(getRealmConfiguration());

        ABAUnit randomUnit = TestUtils.getRandomUnit(levelUnitController.getAllLevelsDescending(realmToCompleteEvaluations));
        final String randomUnitID = randomUnit.getIdUnit();
        setRandomCurrentLenguage(aContext);

        levelUnitController.getSectionsforUnit(aContext, randomUnit, new DataController.ABASimpleCallback() {
            @Override
            public void onSuccess() {
                Realm realmToSetSectionsCompleted = Realm.getInstance(getRealmConfiguration());
                try {
                    ABAUnit currentUnit = levelUnitController.getUnitWithId(realmToSetSectionsCompleted, randomUnitID);
                    completeSection(realmToSetSectionsCompleted, currentUnit, evaluationController);

                    assertTrue(currentUnit.getSectionEvaluation().isCompleted());
                    assertEquals((int) currentUnit.getSectionEvaluation().getProgress(), 100);

                    succeeded[0] = true;
                    signal.countDown();
                } catch (AssertionFailedError e) {
                    signal.countDown();
                }
                realmToSetSectionsCompleted.close();
            }

            @Override
            public void onError(ABAAPIError error) {
                signal.countDown();
            }
        });
        signal.await();
        assertTrue(succeeded[0]);
        realmToCompleteEvaluations.close();
    }

    public void testCompleteAllExercisesForEvaluationSection() throws InterruptedException {
        Realm realmToCompleteAllExercises = Realm.getInstance(getRealmConfiguration());
        String randomUnitID = getAllSectionsForRandomUnit();

        ABAEvaluation evaluationSection = levelUnitController.getUnitWithId(realmToCompleteAllExercises, randomUnitID).getSectionEvaluation();

        int evaluationQuestionAnswers = EvaluationController.getEvaluationAnswers(evaluationSection).size();
        for (ABAEvaluationQuestion evaluationQuestion : evaluationSection.getContent()) {
            ABAEvaluationOption evaluationOption = TestUtils.getRandomOption(evaluationQuestion.getOptions());
            evaluationController.evaluationQuestionAnsweredWithOption(realmToCompleteAllExercises, evaluationOption);
            evaluationQuestionAnswers++;
        }
        assertEquals(evaluationQuestionAnswers, EvaluationController.getEvaluationAnswers(evaluationSection).size());
        assertEquals(EvaluationController.getEvaluationAnswers(evaluationSection).size(), evaluationSection.getContent().size());

        //Reset Evaluation Progress
        evaluationController.resetEvaluationAnswers(realmToCompleteAllExercises, evaluationSection);
        assertEquals(EvaluationController.getEvaluationAnswers(evaluationSection).size(), 0);

        realmToCompleteAllExercises.close();
    }
}
