package com.abaenglish.videoclass.domain.content;

import android.content.Context;

import com.abaenglish.videoclass.ABABaseTestCase;
import com.abaenglish.videoclass.data.network.APIManager;
import com.abaenglish.videoclass.data.persistence.ABAAPIError;
import com.abaenglish.videoclass.data.persistence.ABAUser;
import com.abaenglish.videoclass.session.SessionService;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import io.realm.Realm;

/**
 * Created by Jesus Espejo using mbp13jesus on 13/11/15.
 */
public class PlanControllerTest extends ABABaseTestCase {

    private UserController mUserController;
    private PlanController mPlanController;
    private SessionService mSessionService;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mUserController = new UserController();
        mSessionService = new SessionService();
        mPlanController = new PlanController();

        Context aContext = getInstrumentation().getTargetContext();
        final CountDownLatch signal = new CountDownLatch(1);

        APIManager apiManager = new APIManager();
        apiManager.setApplicationContext(aContext);
        mSessionService.loginWithEmail(aContext, PREMIUM_USER_EMAIL, PREMIUM_USER_PASSWORD, new DataController.ABASimpleCallback() {
            @Override
            public void onSuccess() {
                signal.countDown();
            }

            @Override
            public void onError(ABAAPIError error) {
                signal.countDown();
            }
        });
        signal.await();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testGetProductsCheckJson() throws InterruptedException {
        Context aContext = getInstrumentation().getTargetContext();
        if (GooglePlayServicesUtil.isGooglePlayServicesAvailable(aContext) == ConnectionResult.SUCCESS) {
            final CountDownLatch signal = new CountDownLatch(1);
            final boolean[] didFail = {false};

            Realm realm = Realm.getInstance(getRealmConfiguration());
            ABAUser currentUser = mUserController.getCurrentUser(realm);
            assertNotNull(currentUser);
            mPlanController.fetchPlanContent(aContext, currentUser, new PlanController.ABAPlanCallback() {
                @Override
                public void onResult(PlanController.SubscriptionResult type) {
                    if (type != PlanController.SubscriptionResult.SUBSCRIPTION_RESULT_OK) {
                        didFail[0] = true;
                    }
                    signal.countDown();
                }
            });
            realm.close();

            signal.await(20, TimeUnit.SECONDS);
            assertFalse(didFail[0]);
        }
    }
}
