package com.abaenglish.videoclass.domain.content;

import android.content.Context;

import com.abaenglish.videoclass.ABABaseTestCase;
import com.abaenglish.videoclass.data.network.APIManager;
import com.abaenglish.videoclass.domain.SupportedVersionManager;

import org.json.JSONException;

import java.util.Date;
import java.util.concurrent.CountDownLatch;

/**
 * Created by Jesus Espejo using mbp13jesus on 26/01/16.
 */
public class SupportedVersionManagerTest extends ABABaseTestCase {

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        final Context aContext = getInstrumentation().getTargetContext();
        SupportedVersionManager.resetPreferences(aContext);
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testWebCall() throws InterruptedException {

        final Context aContext = getInstrumentation().getTargetContext();
        final CountDownLatch signal = new CountDownLatch(1);
        final boolean[] succeeded = {false};

        APIManager apiManager = new APIManager();
        apiManager.setApplicationContext(aContext);
        apiManager.lastSupportedVersion(new APIManager.ABAAPIResponseCallback() {
            @Override
            public void onResponse(String response) {
                succeeded[0] = true;
                signal.countDown();
            }

            @Override
            public void onError(Exception e) {
                signal.countDown();
            }
        });

        signal.await();
        assertTrue(succeeded[0]);
    }

    public void testWebCall2() throws InterruptedException {

        final Context aContext = getInstrumentation().getTargetContext();
        final CountDownLatch signal = new CountDownLatch(1);

        // This will force to point to integration
        APIManager.getInstance().setApplicationContext(aContext);

        SupportedVersionManager versionManager = new SupportedVersionManager(aContext);
        versionManager.updateLastSupportedVersion(new SupportedVersionManager.SupportedVersionCallback() {
            @Override
            public void finishedWithVersion(String version, SupportedVersionManager.VersionSourceType type) {
                switch (type) {
                    case WEBSERVICE: {
                        assertNotNull(version);
                        break;
                    }
                    case CACHE:
                    default: {
                        fail("Version origin should be WEBSERVICE");
                    }
                }

                signal.countDown();
            }
        });
        signal.await();
    }

    private static final String JSON_RESPONSE_1 = "{\"android\":\"2.2.0\",\"ios\":\"2.2.1\"}";
    private static final String JSON_RESPONSE_2 = "{\"android\":\"2.2.0\"}";
    private static final String JSON_RESPONSE_3 = "{}";

    public void testJsonParser() {

        final Context aContext = getInstrumentation().getTargetContext();
        SupportedVersionManager versionManager = new SupportedVersionManager(aContext);

        try {
            String version = versionManager.parseSupportedVersion(JSON_RESPONSE_1);
            assertEquals(version, "2.2.0");

            String version2 = versionManager.parseSupportedVersion(JSON_RESPONSE_2);
            assertEquals(version2, "2.2.0");

            String version3 = versionManager.parseSupportedVersion(JSON_RESPONSE_3);
            assertNull(version3);

        } catch (JSONException e) {
            e.printStackTrace();
            fail();
        }
    }

    private static final String BAD_JSON_RESPONSE = "{\"android\":\"2.2.0\" :}";

    public void testBadJsonParser() {
        final Context aContext = getInstrumentation().getTargetContext();
        SupportedVersionManager versionManager = new SupportedVersionManager(aContext);
        try {
            // JSONException expected
            String version = versionManager.parseSupportedVersion(BAD_JSON_RESPONSE);
            fail();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void testCheckVersionIntegrity() {
        final Context aContext = getInstrumentation().getTargetContext();
        SupportedVersionManager versionManager = new SupportedVersionManager(aContext);

        assertTrue(versionManager.checkVersionIntegrity("2.2.0"));
        assertTrue(versionManager.checkVersionIntegrity("2.2"));
        assertTrue(versionManager.checkVersionIntegrity("2"));
        assertTrue(versionManager.checkVersionIntegrity("2.2.2.2"));
        assertTrue(versionManager.checkVersionIntegrity("22.22.23.24"));

        assertFalse(versionManager.checkVersionIntegrity("2."));
        assertFalse(versionManager.checkVersionIntegrity("2.2."));
        assertFalse(versionManager.checkVersionIntegrity(""));
        assertFalse(versionManager.checkVersionIntegrity("."));
        assertFalse(versionManager.checkVersionIntegrity("a.2"));
        assertFalse(versionManager.checkVersionIntegrity("a..2"));
        assertFalse(versionManager.checkVersionIntegrity("2a2"));
        assertFalse(versionManager.checkVersionIntegrity(null));

        assertFalse(versionManager.checkVersionIntegrity("2.2.2.2 "));
        assertFalse(versionManager.checkVersionIntegrity(" 2.2.2.2"));
        assertFalse(versionManager.checkVersionIntegrity(" 2.2.2.2 "));
        assertFalse(versionManager.checkVersionIntegrity(" 2.2.2. 2 "));
        assertFalse(versionManager.checkVersionIntegrity("2.2.2. 2"));
    }

    public void testDefaultVersionIsNotNull() {
        final Context aContext = getInstrumentation().getTargetContext();
        SupportedVersionManager versionManager = new SupportedVersionManager(aContext);

        String defaultVersion = versionManager.readSupportedVersionFromCache();
        assertNotNull(defaultVersion);
        assertEquals(defaultVersion, "");
    }

    public void testSaveVersionSuccessful() {
        final Context aContext = getInstrumentation().getTargetContext();
        SupportedVersionManager versionManager = new SupportedVersionManager(aContext);

        // Checking initially the version is empty
        testDefaultVersionIsNotNull();

        // Saving a new version
        String versionToSave = "5.0.0";
        versionManager.saveSupportedVersionToCache(versionToSave);

        // Checking saved version
        String savedVersion = versionManager.readSupportedVersionFromCache();
        assertEquals(versionToSave, savedVersion);

        // Elapsed time since last save is less than 5 seconds
        Long saveDate = versionManager.readUpdateDateFromCache();
        Long elapsedMillis = saveDate - new Date().getTime();
        assertTrue(elapsedMillis < 0);
        assertTrue(Math.abs(elapsedMillis) < (5 * 1000));
    }

    public void testResetVersion() {

        final Context aContext = getInstrumentation().getTargetContext();
        SupportedVersionManager versionManager = new SupportedVersionManager(aContext);

        // Saving a new version
        String versionToSave = "5.0.0";
        versionManager.saveSupportedVersionToCache(versionToSave);

        // Checking saved version
        String savedVersion = versionManager.readSupportedVersionFromCache();
        assertEquals(versionToSave, savedVersion);

        // Resetting version
        SupportedVersionManager.resetPreferences(aContext);

        // Checking saved version is reset
        String savedVersionAfterReset = versionManager.readSupportedVersionFromCache();
        assertEquals(savedVersionAfterReset, "");

        // Checking
        Long lastUpdateTimeAfterReset = versionManager.readUpdateDateFromCache();
        assertEquals(lastUpdateTimeAfterReset.longValue(), (long) 0);
    }

    public void testShouldPerformUpdate() {

        final Context aContext = getInstrumentation().getTargetContext();
        SupportedVersionManager versionManager = new SupportedVersionManager(aContext);

        // After the reset
        assertTrue(versionManager.shouldPerformUpdate());

        // Saving a new version
        String versionToSave = "5.0.0";
        versionManager.saveSupportedVersionToCache(versionToSave);
        assertFalse(versionManager.shouldPerformUpdate());

        Long time61minutesLater = new Date().getTime() + SupportedVersionManager.UPDATE_WINDOW_MILLIS + 60 * 1000;
        assertTrue(versionManager.shouldPerformUpdateWithMillis(time61minutesLater));

        Long time59minutesLater = new Date().getTime() + SupportedVersionManager.UPDATE_WINDOW_MILLIS - 60 * 1000;
        assertFalse(versionManager.shouldPerformUpdateWithMillis(time59minutesLater));

        // After the reset
        SupportedVersionManager.resetPreferences(aContext);
        assertTrue(versionManager.shouldPerformUpdate());
    }

    public void testPerformUpdateIfNeeded() throws InterruptedException {

        final Context aContext = getInstrumentation().getTargetContext();
        SupportedVersionManager versionManager = new SupportedVersionManager(aContext);

        // This will force to point to integration
        APIManager.getInstance().setApplicationContext(aContext);

        // First call should be a webcall
        final CountDownLatch signal = new CountDownLatch(1);
        versionManager.performUpdateIfNeeded(new SupportedVersionManager.SupportedVersionCallback() {
            @Override
            public void finishedWithVersion(String version, SupportedVersionManager.VersionSourceType type) {
                if (type != SupportedVersionManager.VersionSourceType.WEBSERVICE) {
                    fail("First call should return type = WEBSERVICE");
                }
                assertNotNull(version);
                signal.countDown();
            }
        });
        signal.await();

        // Second call should read from cache
        final CountDownLatch signal2 = new CountDownLatch(1);
        versionManager.performUpdateIfNeeded(new SupportedVersionManager.SupportedVersionCallback() {
            @Override
            public void finishedWithVersion(String version, SupportedVersionManager.VersionSourceType type) {
                if (type != SupportedVersionManager.VersionSourceType.CACHE) {
                    fail("First call should return type = CACHE");
                }
                assertNotNull(version);
                signal2.countDown();
            }
        });
        signal2.await();

        SupportedVersionManager.resetPreferences(aContext);

        // After reset, the call should be a webcall
        final CountDownLatch signal3 = new CountDownLatch(1);
        versionManager.performUpdateIfNeeded(new SupportedVersionManager.SupportedVersionCallback() {
            @Override
            public void finishedWithVersion(String version, SupportedVersionManager.VersionSourceType type) {
                if (type != SupportedVersionManager.VersionSourceType.WEBSERVICE) {
                    fail("First call should return type = WEBSERVICE");
                }
                assertNotNull(version);
                signal3.countDown();
            }
        });
        signal3.await();
    }

    public void testCheckCurrentVersionSupport() {

        final Context aContext = getInstrumentation().getTargetContext();
        SupportedVersionManager versionManager = new SupportedVersionManager(aContext);

        assertTrue(versionManager.checkCurrentVersionSupport("1.0", "0.9"));
        assertTrue(versionManager.checkCurrentVersionSupport("1.0", "1.0"));
        assertFalse(versionManager.checkCurrentVersionSupport("1.0", "1.1"));

        assertTrue(versionManager.checkCurrentVersionSupport("1.0.0", "0.9.9"));
        assertTrue(versionManager.checkCurrentVersionSupport("1.0.0", "1.0.0"));
        assertFalse(versionManager.checkCurrentVersionSupport("1.0.0", "1.0.1"));

        assertTrue(versionManager.checkCurrentVersionSupport("1.0.0.0", "0.9.9.9"));
        assertTrue(versionManager.checkCurrentVersionSupport("1.0.0.0", "1.0.0.0"));
        assertFalse(versionManager.checkCurrentVersionSupport("1.0.0.0", "1.0.0.1"));

        assertFalse(versionManager.checkCurrentVersionSupport("1.0.0", "1.0.0.0"));
        assertFalse(versionManager.checkCurrentVersionSupport("1.0.0", "1.0.0.1"));
        assertTrue(versionManager.checkCurrentVersionSupport("1.0.0", "1.0"));

        assertTrue(versionManager.checkCurrentVersionSupport("1.0.0", ""));
    }

    public void testIsCurrentVersionSupported() {
        final Context aContext = getInstrumentation().getTargetContext();
        SupportedVersionManager versionManager = new SupportedVersionManager(aContext);
        versionManager.isCurrentVersionSupported(new SupportedVersionManager.SupportedVersionResponse() {
            @Override
            public void isSupportedVersion(boolean supported, String lastSupportedVersion) {
                assertTrue(supported);
            }
        });
    }
}
