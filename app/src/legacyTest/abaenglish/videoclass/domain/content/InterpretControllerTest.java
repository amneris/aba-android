package com.abaenglish.videoclass.domain.content;

import android.content.Context;

import com.abaenglish.utils.TestUtils;
import com.abaenglish.videoclass.ABASectionTestCase;
import com.abaenglish.videoclass.data.persistence.ABAAPIError;
import com.abaenglish.videoclass.data.persistence.ABAInterpret;
import com.abaenglish.videoclass.data.persistence.ABAInterpretRole;
import com.abaenglish.videoclass.data.persistence.ABAPhrase;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.domain.LanguageController;
import com.bzutils.LogBZ;

import junit.framework.AssertionFailedError;

import java.util.List;
import java.util.concurrent.CountDownLatch;

import io.realm.Realm;

/**
 * Created by madalin on 27/11/15.
 */
public class InterpretControllerTest extends ABASectionTestCase {

    private LevelUnitController levelUnitController = new LevelUnitController();
    private InterpretController interpretController = new InterpretController();

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testCompleteInterpretSection() throws InterruptedException {
        Context aContext = getInstrumentation().getTargetContext();
        final CountDownLatch signal = new CountDownLatch(1);
        final boolean[] succeeded = {false};

        Realm realmToCompleteSection = Realm.getInstance(getRealmConfiguration());
        ABAUnit randomUnit = TestUtils.getRandomUnit(levelUnitController.getAllLevelsDescending(realmToCompleteSection));

        final String randomUnitID = randomUnit.getIdUnit();
        setRandomCurrentLenguage(aContext);

        levelUnitController.getSectionsforUnit(aContext, randomUnit, new DataController.ABASimpleCallback() {
            @Override
            public void onSuccess() {
                Realm realm = Realm.getInstance(getRealmConfiguration());
                try {
                    ABAUnit currentUnit = levelUnitController.getUnitWithId(realm, randomUnitID);
                    completeSection(realm, currentUnit, interpretController);

                    assertTrue(currentUnit.getSectionInterpret().isCompleted());
                    assertEquals((int) currentUnit.getSectionInterpret().getProgress(), 100);

                    succeeded[0] = true;
                    signal.countDown();

                } catch (AssertionFailedError e) {
                    signal.countDown();
                }
                realm.close();
            }

            @Override
            public void onError(ABAAPIError error) {
                signal.countDown();
            }
        });
        signal.await();
        assertTrue(succeeded[0]);
        realmToCompleteSection.close();
    }

    public void testGetPhrasesFromInterpretSection() throws InterruptedException {
        Context aContext = getInstrumentation().getTargetContext();
        final CountDownLatch signal = new CountDownLatch(1);
        final boolean[] succeeded = {false};

        Realm realmToGetPhrases = Realm.getInstance(getRealmConfiguration());

        ABAUnit randomUnit = TestUtils.getRandomUnit(levelUnitController.getAllLevelsDescending(realmToGetPhrases));
        final String randomUnitID = randomUnit.getIdUnit();
        setRandomCurrentLenguage(aContext);
        LogBZ.e("Running Test ->> testGetPhrasesFromInterpretSection with Unit: " + randomUnitID
                + " and UnitLanguage: " + LanguageController.getCurrentLanguage(aContext).toUpperCase());
        levelUnitController.getSectionsforUnit(aContext, randomUnit, new DataController.ABASimpleCallback() {
            @Override
            public void onSuccess() {
                Realm realm = Realm.getInstance(getRealmConfiguration());
                try {
                    ABAUnit currentUnit = levelUnitController.getUnitWithId(realm, randomUnitID);
                    assertNotSame(currentUnit.getSectionInterpret().getDialog().size(), 0);

                    succeeded[0] = true;
                    signal.countDown();

                } catch (AssertionFailedError e) {
                    signal.countDown();
                }
                realm.close();
            }

            @Override
            public void onError(ABAAPIError error) {
                signal.countDown();
            }
        });
        signal.await();
        assertTrue(succeeded[0]);
        realmToGetPhrases.close();
    }

    public void testToGetAllAudiosIdForVocabularyPhrases() throws InterruptedException {

        String randomUnitID = getAllSectionsForRandomUnit();
        Realm realmToGetAudios = Realm.getInstance(getRealmConfiguration());

        ABAInterpret interpretSection = interpretController.getSectionForUnit(levelUnitController.getUnitWithId(realmToGetAudios, randomUnitID));
        assertEquals(interpretController.getAllAudioIDsForSection(interpretSection).size(), interpretController.getElementsFromSection(interpretSection).size());

        List<String> audiosList = interpretController.getAllAudioIDsForSection(interpretSection);
        assertFalse(audiosList.contains(null));
        assertFalse(audiosList.contains(""));

        realmToGetAudios.close();
    }

    public void testCompleteAllDialogsForInterpretSection() throws InterruptedException {

        String randomUnitID = getAllSectionsForRandomUnit();
        Realm realmToCompleteDialogs = Realm.getInstance(getRealmConfiguration());

        ABAInterpret interpretSection = interpretController.getSectionForUnit(levelUnitController.getUnitWithId(realmToCompleteDialogs, randomUnitID));
        for (ABAInterpretRole interpretRole : interpretSection.getRoles()) {
            int donePhrasesForRole = interpretController.getDonePhrasesForRole(interpretRole, interpretSection).size();
            for (ABAPhrase phrase : interpretController.getPhrasesForRole(interpretRole, interpretSection)) {
                interpretController.setPhraseDoneForRole(realmToCompleteDialogs, phrase, interpretRole, false);
                donePhrasesForRole++;
            }
            assertEquals(donePhrasesForRole, interpretController.getDonePhrasesForRole(interpretRole, interpretSection).size());
        }

        assertTrue(interpretController.isSectionCompleted(interpretSection));
        assertTrue(interpretController.allRolesAreCompleted(interpretSection));
        assertEquals(interpretController.getTotalElementsForSection(interpretSection), interpretController.getElementsCompletedForSection(interpretSection));
        assertEquals((int) interpretController.getProgressForSection(interpretSection), 100);
        assertEquals(interpretController.getPercentageForSection(interpretSection), "100%");

        //Erase Progress for Section
        for (ABAInterpretRole interpretRole : interpretSection.getRoles()) {
            for (ABAPhrase phrase : interpretRole.getInterpretPhrase()) {
                assertEquals(interpretController.phraseWithID(phrase.getAudioFile(), phrase.getPage(), interpretSection), phrase);
            }
            interpretController.eraseProgressForRole(realmToCompleteDialogs, interpretRole, interpretSection);
        }

        assertFalse(interpretController.isSectionCompleted(interpretSection));
        assertFalse(interpretController.allRolesAreCompleted(interpretSection));
        assertNotSame(interpretController.getTotalElementsForSection(interpretSection), interpretController.getElementsCompletedForSection(interpretSection));
        assertNotSame((int) interpretController.getProgressForSection(interpretSection), 100);
        assertNotSame(interpretController.getPercentageForSection(interpretSection), "100%");

        realmToCompleteDialogs.close();
    }
}
