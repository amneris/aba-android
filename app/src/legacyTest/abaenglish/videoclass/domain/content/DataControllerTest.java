package com.abaenglish.videoclass.domain.content;

import android.content.Context;

import com.abaenglish.videoclass.ABABaseTestCase;
import com.abaenglish.videoclass.data.network.APIManager;
import com.abaenglish.videoclass.data.persistence.ABAAPIError;
import com.abaenglish.videoclass.session.SessionService;

import junit.framework.AssertionFailedError;

import java.util.concurrent.CountDownLatch;

import io.realm.Realm;

/**
 * Created by Jesus Espejo using mbp13jesus on 16/11/15.
 */
public class DataControllerTest extends ABABaseTestCase {

    private static String DEFAULT_USER_TOKEN;

    private UserController mUserController;
    private SessionService mSessionService;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mSessionService = new SessionService();
        mUserController = new UserController();

        final CountDownLatch signal = new CountDownLatch(1);
        Context aContext = getInstrumentation().getTargetContext();

        APIManager apiManager = new APIManager();
        apiManager.setApplicationContext(aContext);
        mSessionService.loginWithEmail(aContext, PREMIUM_USER_EMAIL, PREMIUM_USER_PASSWORD, new DataController.ABASimpleCallback() {
            @Override
            public void onSuccess() {
                //Save Token for try after login with Token
                Realm realm = Realm.getInstance(getRealmConfiguration());
                DEFAULT_USER_TOKEN = mUserController.getCurrentUser(realm).getToken();
                realm.close();

                signal.countDown();
            }

            @Override
            public void onError(ABAAPIError error) {
                signal.countDown();
            }
        });
        signal.await();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testLoginTokenProcess() throws InterruptedException {
        final Context aContext = getInstrumentation().getTargetContext();
        final CountDownLatch signal = new CountDownLatch(1);
        final boolean[] succeeded = {false};

        APIManager apiManager = new APIManager();
        apiManager.setApplicationContext(aContext);
        mSessionService.loginWithToken(aContext, DEFAULT_USER_TOKEN, new DataController.ABASimpleCallback() {
            @Override
            public void onSuccess() {
                succeeded[0] = true;
                checkIfUserCreatedCorrectlyInDDBB(aContext, signal);
                signal.countDown();
            }

            @Override
            public void onError(ABAAPIError error) {
                signal.countDown();
            }
        });

        signal.await();
        assertTrue(succeeded[0]);
    }


    // ================================================================================
    // Private methods not to test
    // ================================================================================

    private void checkIfUserCreatedCorrectlyInDDBB(Context aContext, final CountDownLatch signal) {
        try {

            Realm realm = Realm.getInstance(getRealmConfiguration());
            assertNotNull(mUserController.getCurrentUser(realm));
            realm.close();

            mSessionService.logout(aContext, new DataController.ABALogoutCallback() {
                @Override
                public void onLogoutFinished() {
                    Realm realm = Realm.getInstance(getRealmConfiguration());
                    assertNull(mUserController.getCurrentUser(realm));
                    realm.close();
                }
            });
        } catch (AssertionFailedError e) {
            signal.countDown();
        }
    }
}
