package com.abaenglish.videoclass.domain.content;

import android.content.Context;

import com.abaenglish.utils.TestUtils;
import com.abaenglish.videoclass.ABASectionTestCase;
import com.abaenglish.videoclass.data.persistence.ABAAPIError;
import com.abaenglish.videoclass.data.persistence.ABAPhrase;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.data.persistence.ABAVocabulary;

import junit.framework.AssertionFailedError;

import java.util.List;
import java.util.concurrent.CountDownLatch;

import io.realm.Realm;

/**
 * Created by madalin on 27/11/15.
 */
public class VocabularyControllerTest extends ABASectionTestCase {

    private LevelUnitController levelUnitController = new LevelUnitController();
    private VocabularyController vocabularyController = new VocabularyController();

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testCompleteVocabularySection() throws InterruptedException {
        Context aContext = getInstrumentation().getTargetContext();
        final CountDownLatch signal = new CountDownLatch(1);
        final boolean[] succeeded = {false};

        Realm realmToCompleteVocabulary = Realm.getInstance(getRealmConfiguration());

        ABAUnit randomUnit = TestUtils.getRandomUnit(levelUnitController.getAllLevelsDescending(realmToCompleteVocabulary));
        final String randomUnitID = randomUnit.getIdUnit();
        setRandomCurrentLenguage(aContext);

        levelUnitController.getSectionsforUnit(aContext, randomUnit, new DataController.ABASimpleCallback() {
            @Override
            public void onSuccess() {
                Realm realm = Realm.getInstance(getRealmConfiguration());
                try {
                    ABAUnit currentUnit = levelUnitController.getUnitWithId(realm, randomUnitID);
                    ABAVocabulary vocabularySection = vocabularyController.getSectionForUnit(currentUnit);
                    vocabularyController.setCompletedSection(realm, vocabularySection);

                    assertTrue(vocabularySection.isCompleted());
                    assertEquals((int) vocabularySection.getProgress(), 100);

                    succeeded[0] = true;
                    signal.countDown();

                } catch (AssertionFailedError e) {
                    signal.countDown();
                }
                realm.close();
            }

            @Override
            public void onError(ABAAPIError error) {
                signal.countDown();
            }
        });
        signal.await();
        assertTrue(succeeded[0]);
        realmToCompleteVocabulary.close();
    }

    public void testGetPhrasesFromVocabularySection() throws InterruptedException {
        Context aContext = getInstrumentation().getTargetContext();
        final CountDownLatch signal = new CountDownLatch(1);
        final boolean[] succeeded = {false};

        Realm realmToGetPhrases = Realm.getInstance(getRealmConfiguration());

        ABAUnit randomUnit = TestUtils.getRandomUnit(levelUnitController.getAllLevelsDescending(realmToGetPhrases));
        final String randomUnitID = randomUnit.getIdUnit();
        setRandomCurrentLenguage(aContext);

        levelUnitController.getSectionsforUnit(aContext, randomUnit, new DataController.ABASimpleCallback() {
            @Override
            public void onSuccess() {
                Realm realm = Realm.getInstance(getRealmConfiguration());
                try {
                    ABAUnit currentUnit = levelUnitController.getUnitWithId(realm, randomUnitID);
                    assertNotSame(currentUnit.getSectionVocabulary().getContent().size(), 0);

                    succeeded[0] = true;
                    signal.countDown();

                } catch (AssertionFailedError e) {
                    signal.countDown();
                }
                realm.close();
            }

            @Override
            public void onError(ABAAPIError error) {
                signal.countDown();
            }
        });
        signal.await();
        assertTrue(succeeded[0]);
        realmToGetPhrases.close();
    }

    public void testToGetAllAudiosIdForVocabularyPhrases() throws InterruptedException {
        String randomUnitID = getAllSectionsForRandomUnit();
        Realm realmToGetAudios = Realm.getInstance(getRealmConfiguration());

        ABAUnit currentUnit = levelUnitController.getUnitWithId(realmToGetAudios, randomUnitID);
        assertEquals(vocabularyController.getAllAudioIDsForSection(currentUnit.getSectionVocabulary()).size(), vocabularyController.getElementsFromSection(currentUnit.getSectionVocabulary()).size());

        List<String> audiosList = vocabularyController.getAllAudioIDsForSection(currentUnit.getSectionVocabulary());
        assertFalse(audiosList.contains(null));
        assertFalse(audiosList.contains(""));

        realmToGetAudios.close();
    }

    public void testCompleteAllPhrasesFromVocabularySection() throws InterruptedException {
        String randomUnitID = getAllSectionsForRandomUnit();
        Realm realmToGetAllPhrases = Realm.getInstance(getRealmConfiguration());

        ABAVocabulary vocabularySection = vocabularyController.getSectionForUnit(levelUnitController.getUnitWithId(realmToGetAllPhrases, randomUnitID));
        while (vocabularyController.getCurrentPhraseForSection(vocabularySection, VocabularyController.ContinueWithFirstUndoneWord) != null) {
            ABAPhrase currentPhrase = vocabularyController.getCurrentPhraseForSection(vocabularySection, VocabularyController.ContinueWithFirstUndoneWord);
            assertEquals(vocabularyController.phraseWithID(currentPhrase.getAudioFile(), currentPhrase.getPage(), vocabularySection), currentPhrase);

            vocabularyController.setPhraseDone(realmToGetAllPhrases, currentPhrase, vocabularySection, false);
            assertTrue(currentPhrase.isDone() && currentPhrase.isListened());
        }

        assertTrue(vocabularyController.isSectionCompleted(vocabularySection));
        assertEquals(vocabularyController.getTotalPhrasesCompletedForVocabulary(vocabularySection), (int) vocabularyController.getTotalElementsForSection(vocabularySection));
        assertEquals((int) vocabularyController.getProgressForSection(vocabularySection), 100);
        assertEquals(vocabularyController.getPercentageForSection(vocabularySection), "100%");

        realmToGetAllPhrases.close();
    }
}
