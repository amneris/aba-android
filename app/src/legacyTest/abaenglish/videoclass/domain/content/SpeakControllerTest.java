package com.abaenglish.videoclass.domain.content;

import android.content.Context;

import com.abaenglish.utils.TestUtils;
import com.abaenglish.videoclass.ABASectionTestCase;
import com.abaenglish.videoclass.data.persistence.ABAAPIError;
import com.abaenglish.videoclass.data.persistence.ABAPhrase;
import com.abaenglish.videoclass.data.persistence.ABASpeak;
import com.abaenglish.videoclass.data.persistence.ABAUnit;

import junit.framework.AssertionFailedError;

import java.util.concurrent.CountDownLatch;

import io.realm.Realm;

/**
 * Created by madalin on 27/11/15.
 */
public class SpeakControllerTest extends ABASectionTestCase {

    private LevelUnitController levelUnitController = new LevelUnitController();
    private SpeakController speakController = new SpeakController();

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testCompleteSpeakSection() throws InterruptedException {
        Context aContext = getInstrumentation().getTargetContext();
        final CountDownLatch signal = new CountDownLatch(1);
        final boolean[] succeeded = {false};

        Realm realmToCompleteSpeak = Realm.getInstance(getRealmConfiguration());

        final ABAUnit randomUnit = TestUtils.getRandomUnit(levelUnitController.getAllLevelsDescending(realmToCompleteSpeak));
        final String randomUnitID = randomUnit.getIdUnit();
        setRandomCurrentLenguage(aContext);

        levelUnitController.getSectionsforUnit(aContext, randomUnit, new DataController.ABASimpleCallback() {
            @Override
            public void onSuccess() {
                Realm realm = Realm.getInstance(getRealmConfiguration());
                try {
                    ABAUnit currentUnit = levelUnitController.getUnitWithId(realm, randomUnitID);
                    ABASpeak speakSection = speakController.getSectionForUnit(currentUnit);
                    speakController.setCompletedSection(realm, speakSection);

                    assertTrue(speakSection.isCompleted());
                    assertEquals((int) speakSection.getProgress(), 100);

                    succeeded[0] = true;
                    signal.countDown();

                } catch (AssertionFailedError e) {
                    signal.countDown();
                }
                realm.close();
            }

            @Override
            public void onError(ABAAPIError error) {
                signal.countDown();
            }
        });
        signal.await();
        assertTrue(succeeded[0]);
        realmToCompleteSpeak.close();
    }

    public void testGetPhrasesFromSpeakSection() throws InterruptedException {
        Context aContext = getInstrumentation().getTargetContext();
        final CountDownLatch signal = new CountDownLatch(1);
        final boolean[] succeeded = {false};

        Realm realmToGetSpeakPhrases = Realm.getInstance(getRealmConfiguration());

        ABAUnit randomUnit = TestUtils.getRandomUnit(levelUnitController.getAllLevelsDescending(realmToGetSpeakPhrases));
        final String randomUnitID = randomUnit.getIdUnit();
        setRandomCurrentLenguage(aContext);

        levelUnitController.getSectionsforUnit(aContext, randomUnit, new DataController.ABASimpleCallback() {
            @Override
            public void onSuccess() {
                Realm realm = Realm.getInstance(getRealmConfiguration());
                try {
                    ABAUnit currentUnit = levelUnitController.getUnitWithId(realm, randomUnitID);
                    assertNotSame(currentUnit.getSectionSpeak().getContent().size(), 0);

                    succeeded[0] = true;
                    signal.countDown();

                } catch (AssertionFailedError e) {
                    signal.countDown();
                }
                realm.close();
            }

            @Override
            public void onError(ABAAPIError error) {
                signal.countDown();
            }
        });
        signal.await();
        assertTrue(succeeded[0]);
        realmToGetSpeakPhrases.close();
    }

    public void testCompleteAllPhrasesFromSpeakSection() throws InterruptedException {

        String randomUnitID = getAllSectionsForRandomUnit();
        Realm realmToCompletePhrasesAtSpeak = Realm.getInstance(getRealmConfiguration());

        ABASpeak speakSection = speakController.getSectionForUnit(levelUnitController.getUnitWithId(realmToCompletePhrasesAtSpeak, randomUnitID));
        while (speakController.getCurrentPhraseForSection(speakSection, VocabularyController.ContinueWithFirstUndoneWord) != null) {
            ABAPhrase currentPhrase = speakController.getCurrentPhraseForSection(speakSection, VocabularyController.ContinueWithFirstUndoneWord);
            assertEquals(speakController.phraseWithID(currentPhrase.getAudioFile(), currentPhrase.getPage(), speakSection), currentPhrase);

            speakController.setPhraseDone(realmToCompletePhrasesAtSpeak, currentPhrase, speakSection, false);
            assertTrue(currentPhrase.isDone() && currentPhrase.isListened());
        }

        assertTrue(speakController.isSectionCompleted(speakSection));
        assertEquals(speakController.getElementsCompletedForSection(speakSection), speakController.getTotalElementsForSection(speakSection));
        assertEquals((int) speakController.getProgressForSection(speakSection), 100);
        assertEquals(speakController.getPercentageForSection(speakSection), "100%");

        realmToCompletePhrasesAtSpeak.close();
    }
}
