package com.abaenglish.videoclass.domain.content;

import android.content.Context;

import com.abaenglish.utils.TestUtils;
import com.abaenglish.videoclass.ABASectionTestCase;
import com.abaenglish.videoclass.data.network.APIManager;
import com.abaenglish.videoclass.data.network.parser.ABAProgressParser;
import com.abaenglish.videoclass.data.persistence.ABAAPIError;
import com.abaenglish.videoclass.data.persistence.ABALevel;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.data.persistence.ABAUser;
import com.abaenglish.videoclass.domain.ProgressService;
import com.abaenglish.videoclass.domain.datastore.DataStore;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.CountDownLatch;

import io.realm.Realm;

/**
 * Created by madalin on 30/11/15.
 */
public class ProgressControllerTest extends ABASectionTestCase {

    private ProgressService mProgressService;
    private LevelUnitController levelUnitController = new LevelUnitController();

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mProgressService = new ProgressService();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testGetCourseProgress() throws InterruptedException {
        final CountDownLatch signal = new CountDownLatch(1);
        final boolean[] succeeded = {false};

        Realm realmToRetrieveProgress = Realm.getInstance(getRealmConfiguration());

        APIManager.getInstance().getCourseProgress(DataStore.getInstance().getUserController().getCurrentUser(realmToRetrieveProgress).getUserId(), new APIManager.ABAAPIResponseCallback() {
            @Override
            public void onResponse(String response) {
                final Realm realmSyncProgress = Realm.getInstance(getRealmConfiguration());
                try {
                    realmSyncProgress.beginTransaction();
                    ABAProgressParser.parseUnitProgress(realmSyncProgress, response);
                    ABAProgressParser.syncLevelsProgress(realmSyncProgress);
                    realmSyncProgress.commitTransaction();
                    succeeded[0] = true;
                } catch (Exception e) {
                    realmSyncProgress.cancelTransaction();
                }
                signal.countDown();
                realmSyncProgress.close();

            }

            @Override
            public void onError(Exception e) {
                signal.countDown();
            }
        });

        signal.await();
        realmToRetrieveProgress.close();
        assertTrue(succeeded[0]);
    }

    public void testGetCompletedAction() throws InterruptedException {
        final CountDownLatch signal = new CountDownLatch(1);
        final boolean[] succeeded = {false};

        String randomUnitID = getAllSectionsForRandomUnit();
        Realm realmToGetCompletedActions = Realm.getInstance(getRealmConfiguration());
        ABAUser currentUser = DataStore.getInstance().getUserController().getCurrentUser(realmToGetCompletedActions);
        ABAUnit randomUnit = DataStore.getInstance().getLevelUnitController().getUnitWithId(realmToGetCompletedActions, randomUnitID);
        mProgressService.updateCompletedActions(currentUser, randomUnit, new DataController.ABASimpleCallback() {
            @Override
            public void onSuccess() {
                succeeded[0] = true;
                signal.countDown();
            }

            @Override
            public void onError(ABAAPIError error) {
                signal.countDown();
            }
        });
        signal.await();
        realmToGetCompletedActions.close();
        assertTrue(succeeded[0]);
    }

    public void testToSyncUnitsWithCompletedProgress() throws InterruptedException, JSONException {
        final boolean[] succeeded = {false};

        Realm realmToSyncUnits = Realm.getInstance(getRealmConfiguration());

        JSONArray jsonArray = new JSONArray();
        for (ABALevel level : levelUnitController.getAllLevelsDescending(realmToSyncUnits)) {
            for (ABAUnit unit : level.getUnits()) {
                JSONObject jsonObject = new JSONObject();
                String[] progressKeys = new String[]{"IdUnit", "AbaFilmPct", "ExercisesPct", "InterpretPct", "SpeakPct", "VocabularyPct", "VideoClassPct", "WritePct", "AssessmentPct"};
                String[] values = new String[]{unit.getIdUnit(), "100", "100", "100", "100", "100", "100", "100", "100"};
                for (int i = 0; i < progressKeys.length; i++) {
                    jsonObject.put(progressKeys[i], values[i]);
                }
                jsonArray.put(jsonObject);
            }
        }

        try {
            realmToSyncUnits.beginTransaction();
            ABAProgressParser.parseUnitProgress(realmToSyncUnits, jsonArray);
            realmToSyncUnits.commitTransaction();
            succeeded[0] = true;
        } catch (Exception e) {
            e.printStackTrace();
            succeeded[0] = false;
        }

        realmToSyncUnits.close();
        assertTrue(succeeded[0]);
    }

    public void testUpdateProgressUnit() throws InterruptedException {
        Context aContext = getInstrumentation().getTargetContext();
        final CountDownLatch signal = new CountDownLatch(1);
        final boolean[] succeeded = {false};

        Realm realmToUpdateProgress = Realm.getInstance(getRealmConfiguration());
        ABAUnit randomUnit = realmToUpdateProgress.where(ABAUnit.class).findFirst();
        assertNotNull(randomUnit);
        final String randomUnitID = randomUnit.getIdUnit();

        levelUnitController.getSectionsforUnit(aContext, randomUnit, new DataController.ABASimpleCallback() {
            @Override
            public void onSuccess() {
                Realm realmToCompareProgress = Realm.getInstance(getRealmConfiguration());

                ABAUnit randomUnit = realmToCompareProgress.where(ABAUnit.class).equalTo("idUnit",randomUnitID).findFirst();
                completeAllSectionsForUnit(realmToCompareProgress, randomUnit);

                // We complete all Sections but progressUnit is not update
                assertNotSame((int) randomUnit.getProgress(), 100);

                // Update progress for Unit
                realmToCompareProgress.beginTransaction();
                mProgressService.updateProgressUnit(randomUnit);
                realmToCompareProgress.commitTransaction();
                assertEquals((int) randomUnit.getProgress(), 100);

                succeeded[0] = true;
                realmToCompareProgress.close();
                signal.countDown();
            }

            @Override
            public void onError(ABAAPIError error) {
                signal.countDown();
            }
        });

        signal.await();
        realmToUpdateProgress.close();
        assertTrue(succeeded[0]);
    }

    // ================================================================================
    // Private methods not to test
    // ================================================================================

    private void checkIfAllUnitsAreCompleted(Realm realm) {
        assertEquals(levelUnitController.getCompleteUnits(realm), TestUtils.NUMBER_OF_UNITS);
        for (ABALevel level : levelUnitController.getAllLevelsDescending(realm)) {
            assertEquals(levelUnitController.getCompletedUnitsForLevel(level).size(), TestUtils.NUMBER_OF_UNIT_PER_LEVEL);
            assertEquals(levelUnitController.getIncompletedUnitsForLevel(level).size(), 0);
        }
    }
}
