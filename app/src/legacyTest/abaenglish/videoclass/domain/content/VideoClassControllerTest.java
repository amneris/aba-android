package com.abaenglish.videoclass.domain.content;

import android.content.Context;

import com.abaenglish.utils.TestUtils;
import com.abaenglish.videoclass.ABASectionTestCase;
import com.abaenglish.videoclass.data.persistence.ABAAPIError;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.data.persistence.ABAVideoClass;

import junit.framework.AssertionFailedError;

import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;

import io.realm.Realm;

/**
 * Created by madalin on 27/11/15.
 */
public class VideoClassControllerTest extends ABASectionTestCase {

    private LevelUnitController levelUnitController = new LevelUnitController();
    private VideoClassController videoClassController = new VideoClassController();

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testCompleteVideoClassSection() throws InterruptedException {
        Context aContext = getInstrumentation().getTargetContext();
        final CountDownLatch signal = new CountDownLatch(1);
        final boolean[] succeeded = {false};

        Realm realmToCompleteVideo = Realm.getInstance(getRealmConfiguration());

        ABAUnit randomUnit = TestUtils.getRandomUnit(levelUnitController.getAllLevelsDescending(realmToCompleteVideo));
        final String randomUnitID = randomUnit.getIdUnit();
        setRandomCurrentLenguage(aContext);

        levelUnitController.getSectionsforUnit(aContext, randomUnit, new DataController.ABASimpleCallback() {
            @Override
            public void onSuccess() {
                Realm realm = Realm.getInstance(getRealmConfiguration());
                try {
                    ABAUnit currentUnit = levelUnitController.getUnitWithId(realm, randomUnitID);
                    ABAVideoClass videoClassSection = videoClassController.getSectionForUnit(currentUnit);
                    videoClassController.setCompletedSection(realm, videoClassSection);

                    assertTrue(videoClassSection.isCompleted());
                    assertTrue(videoClassController.isSectionCompleted(videoClassSection));

                    assertEquals((int) videoClassSection.getProgress(), 100);
                    assertEquals((int) videoClassController.getProgressForSection(videoClassSection), 100);
                    assertEquals(videoClassController.getPercentageForSection(videoClassSection), "100%");

                    succeeded[0] = true;
                    signal.countDown();
                } catch (AssertionFailedError e) {
                    signal.countDown();
                }

                realm.close();
            }

            @Override
            public void onError(ABAAPIError error) {
                signal.countDown();
            }
        });
        signal.await();
        assertTrue(succeeded[0]);
        realmToCompleteVideo.close();
    }

    public void testDownloadSubtitlesForVideoClass() throws InterruptedException {
        String randomUnitID = getAllSectionsForRandomUnit();
        Realm realmToDownloadSubtitles = Realm.getInstance(getRealmConfiguration());

        ArrayList<String> subtitlesUrlArray = new ArrayList<>();
        ABAUnit currentUnit = levelUnitController.getUnitWithId(realmToDownloadSubtitles, randomUnitID);
        ABAVideoClass videoClassSection = videoClassController.getSectionForUnit(currentUnit);

        subtitlesUrlArray.add(videoClassSection.getEnglishSubtitles());
        subtitlesUrlArray.add(videoClassSection.getTranslatedSubtitles());
        boolean subtitlesDownloaded = downloadAllSubtitleFromVideosSection(subtitlesUrlArray);

        realmToDownloadSubtitles.close();

        assertTrue(subtitlesDownloaded);
    }
}
