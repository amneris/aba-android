package com.abaenglish.videoclass.domain.content;

import android.content.Context;

import com.abaenglish.utils.TestUtils;
import com.abaenglish.videoclass.ABABaseTestCase;
import com.abaenglish.videoclass.data.CourseContentService;
import com.abaenglish.videoclass.data.network.APIManager;
import com.abaenglish.videoclass.data.network.parser.ABAProgressParser;
import com.abaenglish.videoclass.data.persistence.ABAAPIError;
import com.abaenglish.videoclass.data.persistence.ABALevel;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.data.persistence.ABAUser;
import com.abaenglish.videoclass.session.SessionService;

import junit.framework.AssertionFailedError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;
import java.util.concurrent.CountDownLatch;

import io.realm.Realm;

/**
 * Created by madalin on 23/11/15.
 */
public class LevelUnitControllerTest extends ABABaseTestCase {

    private UserController mUserController;
    private LevelUnitController mLevelUnitController;

    private SessionService mSessionService;
    private CourseContentService mContentService;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mUserController = new UserController();
        mSessionService = new SessionService();
        mContentService = new CourseContentService();
        mLevelUnitController = new LevelUnitController();

        Context aContext = getInstrumentation().getTargetContext();
        final CountDownLatch signal = new CountDownLatch(1);

        APIManager apiManager = new APIManager();
        apiManager.setApplicationContext(aContext);

        mSessionService.loginWithEmail(aContext, PREMIUM_USER_EMAIL, PREMIUM_USER_PASSWORD, new DataController.ABASimpleCallback() {
            @Override
            public void onSuccess() {
                signal.countDown();
            }

            @Override
            public void onError(ABAAPIError error) {
                fail("Login could not be performed.");
                signal.countDown();
            }
        });
        signal.await();

        Realm realmToCheckUnits = Realm.getInstance(getRealmConfiguration());
        List pepe = mLevelUnitController.getAllLevelsDescending(realmToCheckUnits);
        assertFalse(pepe.isEmpty());
        realmToCheckUnits.close();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testSaveUserData() throws InterruptedException {

        Realm realmInstance = Realm.getInstance(getRealmConfiguration());

        assertNotNull(mUserController.getCurrentUser(realmInstance));
        ABAUser user = mUserController.getCurrentUser(realmInstance);
        String newName = "Test Name";

        realmInstance.beginTransaction();
        user.setName(newName);
        realmInstance.commitTransaction();

        assertEquals(mUserController.getCurrentUser(realmInstance).getName(), newName);

        realmInstance.close();
    }

    public void testUserHaveCurrentLevel() throws InterruptedException {
        Realm realm = Realm.getInstance(getRealmConfiguration());
        ABAUser currentUser = mUserController.getCurrentUser(realm);
        assertNotNull(currentUser.getCurrentLevel());
        realm.close();
    }

    public void testChangeUserLevel() throws InterruptedException {
        Context aContext = getInstrumentation().getTargetContext();
        final CountDownLatch signal1 = new CountDownLatch(1);
        final boolean[] succeeded = {false};

        Realm realmInstanceToSetNewLevel = Realm.getInstance(getRealmConfiguration());

        List<ABALevel> allLevelsDescending = mLevelUnitController.getAllLevelsDescending(realmInstanceToSetNewLevel);
        ABALevel currentLevel = mUserController.getCurrentUser(realmInstanceToSetNewLevel).getCurrentLevel();
        final ABALevel randomLevel = TestUtils.getRandomLevelWithoutCurrent(allLevelsDescending, currentLevel);
        final String randomLevelID = randomLevel.getIdLevel();

        mContentService.setCurrentLevel(aContext, randomLevel, new DataController.ABASimpleCallback() {
            @Override
            public void onSuccess() {
                try {
                    succeeded[0] = true;
                    signal1.countDown();
                } catch (AssertionFailedError e) {
                    signal1.countDown();
                }
            }

            @Override
            public void onError(ABAAPIError error) {
                fail("error performing level change");
                signal1.countDown();
            }
        });
        signal1.await();
        realmInstanceToSetNewLevel.close();

        Realm realmToReadResults = Realm.getInstance(getRealmConfiguration());
        assertEquals(mUserController.getCurrentUser(realmToReadResults).getCurrentLevel().getIdLevel(), randomLevelID);
        assertTrue(succeeded[0]);
        realmToReadResults.close();
    }

    public void testUnitCompleted() throws InterruptedException {

        Realm realmToCheckUnitCompletion = Realm.getInstance(getRealmConfiguration());
        ABAUnit randomUnit = TestUtils.getRandomUnit(mLevelUnitController.getAllLevelsDescending(realmToCheckUnitCompletion));
        realmToCheckUnitCompletion.beginTransaction();
        mLevelUnitController.setCompletedUnit(realmToCheckUnitCompletion, randomUnit);
        realmToCheckUnitCompletion.commitTransaction();

        assertTrue(randomUnit.isCompleted());
        assertEquals((int) randomUnit.getProgress(), 100);
        realmToCheckUnitCompletion.close();
    }

    public void testUserSetUnitProgress() throws InterruptedException, JSONException {
        final boolean[] succeeded = {false};

        Realm realmToSetUnitProgress = Realm.getInstance(getRealmConfiguration());

        final ABAUnit randomUnit = TestUtils.getRandomUnit(mLevelUnitController.getAllLevelsDescending(realmToSetUnitProgress));
        String[] progressKeys = new String[]{"IdUnit", "AbaFilmPct", "ExercisesPct", "InterpretPct", "SpeakPct", "VocabularyPct", "VideoClassPct", "WritePct", "AssessmentPct"};
        String[] values = new String[]{randomUnit.getIdUnit(), "100", "100", "100", "100", "100", "100", "100", "100"};

        JSONObject jsonObject = new JSONObject();
        for (int i = 0; i < progressKeys.length; i++) {
            jsonObject.put(progressKeys[i], values[i]);
        }
        JSONArray jsonArray = new JSONArray();
        jsonArray.put(jsonObject);

        try {

            realmToSetUnitProgress.beginTransaction();
            ABAProgressParser.parseUnitProgress(realmToSetUnitProgress, jsonArray);
            realmToSetUnitProgress.commitTransaction();

            ABAUnit updatedABAUnit = mLevelUnitController.getUnitWithId(realmToSetUnitProgress, randomUnit.getIdUnit());
            assertTrue(updatedABAUnit.isCompleted());
            assertEquals((int) updatedABAUnit.getProgress(), 100);

            succeeded[0] = true;
        } catch (Exception e) {
            succeeded[0] = false;
            e.printStackTrace();
        }

        realmToSetUnitProgress.close();
        assertTrue(succeeded[0]);
    }
}
