package com.abaenglish.videoclass.domain.content;

import android.content.Context;

import com.abaenglish.utils.TestUtils;
import com.abaenglish.videoclass.ABABaseTestCase;
import com.abaenglish.videoclass.data.network.APIManager;
import com.abaenglish.videoclass.data.persistence.ABAAPIError;
import com.abaenglish.videoclass.data.persistence.ABAUser;
import com.abaenglish.videoclass.session.SessionService;

import junit.framework.AssertionFailedError;

import java.util.concurrent.CountDownLatch;

import io.realm.Realm;

/**
 * Created by madalin on 27/11/15.
 */
public class UserControllerTest extends ABABaseTestCase {

    private final static String DEFAULT_USER_NAME = "Student";
    private final static String DEFAULT_USER_EMAIL = "student+esf@abaenglish.com";
    private final static String DEFAULT_USER_PASSWORD = "abaenglish";

    private UserController mUserController;
    private SessionService mSessionService;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mUserController = new UserController();
        mSessionService = new SessionService();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    // ================================================================================
    // Test Check Credentials
    // ================================================================================

    public void testCheckCredentials() throws InterruptedException {
        //Check with correct credentials
        assertTrue(mUserController.checkName(DEFAULT_USER_NAME));
        assertTrue(mUserController.checkEmail(DEFAULT_USER_EMAIL));
        assertTrue(mUserController.checkPassword(DEFAULT_USER_PASSWORD));
        assertTrue(mUserController.checkPasswordNOEmpty(DEFAULT_USER_PASSWORD));

        //Check with incorrect credentials
        assertFalse(mUserController.checkName(""));

        assertFalse(mUserController.checkEmail(""));
        assertFalse(mUserController.checkEmail("test.test.com"));
        assertFalse(mUserController.checkEmail("test@test."));
        assertFalse(mUserController.checkEmail("test@.com"));
        assertFalse(mUserController.checkEmail("test.com"));

        assertFalse(mUserController.checkPassword(""));
        assertFalse(mUserController.checkPassword("12345"));

        assertFalse(mUserController.checkPasswordNOEmpty(""));
    }

    public void testUserCheckCredentials() throws InterruptedException {
        final CountDownLatch signal = new CountDownLatch(1);
        final boolean[] succeeded = {false};

        APIManager apiManager = new APIManager();
        apiManager.setApplicationContext(getInstrumentation().getTargetContext());
        mUserController.checkEmailAndPassword(DEFAULT_USER_EMAIL, DEFAULT_USER_PASSWORD, new DataController.ABAAPICallback<Boolean>() {
            @Override
            public void onSuccess(Boolean response) {
                succeeded[0] = true;
                signal.countDown();
            }

            @Override
            public void onError(ABAAPIError error) {
                signal.countDown();
            }
        });

        signal.await();
        assertTrue(succeeded[0]);
    }

    // ================================================================================
    // Test Action Process
    // ================================================================================

    public void testLoginProcess() throws InterruptedException {
        final Context aContext = getInstrumentation().getTargetContext();
        final CountDownLatch signal = new CountDownLatch(1);
        final boolean[] succeeded = {false};

        APIManager apiManager = new APIManager();
        apiManager.setApplicationContext(aContext);
        mSessionService.loginWithEmail(aContext, DEFAULT_USER_EMAIL, DEFAULT_USER_PASSWORD, new DataController.ABASimpleCallback() {
            @Override
            public void onSuccess() {
                Realm realmToCheckUser = Realm.getInstance(getRealmConfiguration());
                try {
                    assertNotNull(mUserController.getCurrentUser(realmToCheckUser));
                    assertTrue(mUserController.isUserLogged());

                    checkUserProperlyCreatedInDDBB(realmToCheckUser, aContext);
                    succeeded[0] = true;
                    signal.countDown();

                } catch (AssertionFailedError e) {
                    signal.countDown();
                }

                realmToCheckUser.close();
            }

            @Override
            public void onError(ABAAPIError error) {
                signal.countDown();
            }
        });

        // Waiting until the plan controller is finished
        signal.await();
        assertTrue(succeeded[0]);
    }

    public void testLoginWithFacebookProcess() throws InterruptedException {
        final Context aContext = getInstrumentation().getTargetContext();
        final CountDownLatch signal = new CountDownLatch(1);
        final boolean[] succeeded = {false};

        APIManager apiManager = new APIManager();
        apiManager.setApplicationContext(aContext);
        mSessionService.registerUserWithFacebook(aContext, "jespejo@abaenglish.com", "1423011378021227", "Jesus Spiegel", "", "male", new DataController.ABAAPIFacebookCallback() {
            @Override
            public void onSuccess(Boolean isNewUser) {
                Realm realmToCheckUser = Realm.getInstance(getRealmConfiguration());
                try {
                    assertNotNull(mUserController.getCurrentUser(realmToCheckUser));
                    assertFalse(isNewUser);
                    assertTrue(mUserController.isUserLogged());

                    checkUserProperlyCreatedInDDBB(realmToCheckUser, aContext);

                    succeeded[0] = true;
                    signal.countDown();

                } catch (AssertionFailedError e) {
                    signal.countDown();
                }
                realmToCheckUser.close();
            }

            @Override
            public void onError(ABAAPIError error) {
                signal.countDown();
            }
        });

        signal.await();
        assertTrue(succeeded[0]);
    }

    public void testRegisterProcess() throws InterruptedException {
        final Context aContext = getInstrumentation().getTargetContext();
        final CountDownLatch signal = new CountDownLatch(1);
        final boolean[] succeeded = {false};

        APIManager apiManager = new APIManager();
        apiManager.setApplicationContext(aContext);
        mSessionService.register(aContext, "EmailGenerator", TestUtils.generateEmail(), "123456", new DataController.ABASimpleCallback() {
            @Override
            public void onSuccess() {
                Realm realmToCheckUser = Realm.getInstance(getRealmConfiguration());
                try {
                    assertNotNull(mUserController.getCurrentUser(realmToCheckUser));
                    assertTrue(mUserController.isUserLogged());

                    checkUserProperlyCreatedInDDBB(realmToCheckUser, aContext);

                    succeeded[0] = true;
                    signal.countDown();
                } catch (AssertionFailedError e) {
                    signal.countDown();
                }
                realmToCheckUser.close();
            }

            @Override
            public void onError(ABAAPIError error) {
                signal.countDown();
            }
        });

        signal.await();
        assertTrue(succeeded[0]);
    }

    public void testRegisterWithFacebookProcess() throws InterruptedException {
        final Context aContext = getInstrumentation().getTargetContext();
        final CountDownLatch signal = new CountDownLatch(1);
        final boolean[] succeeded = {false};

        APIManager apiManager = new APIManager();
        apiManager.setApplicationContext(aContext);
        mSessionService.registerUserWithFacebook(aContext, TestUtils.generateEmail(), TestUtils.generateFacebookID(), "Test", "Facebook", "male", new DataController.ABAAPIFacebookCallback() {
            @Override
            public void onSuccess(Boolean isNewUser) {
                Realm realmToCheckUser = Realm.getInstance(getRealmConfiguration());
                try {
                    assertNotNull(mUserController.getCurrentUser(realmToCheckUser));
                    assertTrue(isNewUser);
                    assertTrue(mUserController.isUserLogged());

                    checkUserProperlyCreatedInDDBB(realmToCheckUser, aContext);

                    succeeded[0] = true;
                    signal.countDown();
                } catch (AssertionFailedError e) {
                    signal.countDown();
                }
                realmToCheckUser.close();
            }

            @Override
            public void onError(ABAAPIError error) {
                signal.countDown();
            }
        });

        signal.await();
        assertTrue(succeeded[0]);
    }

    public void testUserPremium() throws InterruptedException {
        final Context aContext = getInstrumentation().getTargetContext();
        final CountDownLatch signal = new CountDownLatch(1);
        final boolean[] succeeded = {false};

        APIManager apiManager = new APIManager();
        apiManager.setApplicationContext(aContext);
        mSessionService.loginWithEmail(aContext, PREMIUM_USER_EMAIL, PREMIUM_USER_PASSWORD, new DataController.ABASimpleCallback() {
            @Override
            public void onSuccess() {
                Realm realmToCheckUser = Realm.getInstance(getRealmConfiguration());
                try {
                    ABAUser currentUser = mUserController.getCurrentUser(realmToCheckUser);
                    assertNotNull(currentUser);
                    assertTrue(mUserController.isPremium(currentUser));

                    checkUserProperlyCreatedInDDBB(realmToCheckUser, aContext);

                    succeeded[0] = true;
                    signal.countDown();

                } catch (AssertionFailedError e) {
                    signal.countDown();
                }

                realmToCheckUser.close();
            }

            @Override
            public void onError(ABAAPIError error) {
                signal.countDown();
            }
        });
        signal.await();
        assertTrue(succeeded[0]);
    }

    public void testRecoverPassword() throws InterruptedException {
        Context aContext = getInstrumentation().getTargetContext();
        final CountDownLatch signal = new CountDownLatch(1);
        final boolean[] succeeded = {false};

        mSessionService.recoverPassword(aContext, DEFAULT_USER_EMAIL, new DataController.ABAAPICallback<Boolean>() {
            @Override
            public void onSuccess(Boolean response) {
                succeeded[0] = true;
                assertTrue(response);
                signal.countDown();
            }

            @Override
            public void onError(ABAAPIError error) {
                signal.countDown();
            }
        });
        signal.await();
        assertTrue(succeeded[0]);
    }

    // ================================================================================
    // Private methods not to test
    // ================================================================================

    private void checkUserProperlyCreatedInDDBB(final Realm realm, Context aContext) {

        assertNotNull(mUserController.getCurrentUser(realm));
        mSessionService.logout(aContext, new DataController.ABALogoutCallback() {
            @Override
            public void onLogoutFinished() {
                assertNull(mUserController.getCurrentUser(realm));
            }
        });
    }
}
