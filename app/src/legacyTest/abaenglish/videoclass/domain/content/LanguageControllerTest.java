package com.abaenglish.videoclass.domain.content;

import android.content.Context;

import com.abaenglish.utils.TestUtils;
import com.abaenglish.videoclass.ABABaseTestCase;
import com.abaenglish.videoclass.domain.LanguageController;

import java.util.Arrays;
import java.util.Locale;


/**
 * Created by madalin on 27/11/15.
 */
public class LanguageControllerTest extends ABABaseTestCase {
    
    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testDefaultLanguageFromDefault() throws  InterruptedException {
        Context aContext = getInstrumentation().getTargetContext();
        assertTrue(Arrays.asList(LanguageController.SUPPORTED_LANGUAGE_ARRAY).contains(LanguageController.getCurrentLanguage(aContext)));
    }

    public void testSetLanguage() throws InterruptedException {
        Context aContext = getInstrumentation().getTargetContext();
        String randomLanguage = TestUtils.getRandomOfStringArray(LanguageController.SUPPORTED_LANGUAGE_ARRAY);
        LanguageController.setCurrentLanguage(aContext, randomLanguage);
        assertEquals(LanguageController.getCurrentLanguage(aContext), randomLanguage);
    }

    public void testForceAppLanguage() throws InterruptedException {
        Context aContext = getInstrumentation().getTargetContext();
        String randomLanguage = TestUtils.getRandomOfStringArray(LanguageController.SUPPORTED_LANGUAGE_ARRAY);
        Locale.setDefault(new Locale(randomLanguage));

        LanguageController.setCurrentLanguage(aContext, TestUtils.DEFAULT_USER_LANG);
        LanguageController.currentLanguageOverride(aContext);
        assertEquals(LanguageController.getCurrentLanguage(aContext), TestUtils.DEFAULT_USER_LANG);
    }
}
