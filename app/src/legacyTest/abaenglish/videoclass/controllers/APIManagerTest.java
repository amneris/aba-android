package com.abaenglish.videoclass.controllers;

import android.content.Context;

import com.abaenglish.shepherd.ABAShepherdEditor;
import com.abaenglish.shepherd.configuration.engine.model.ShepherdGenericEnvironment;
import com.abaenglish.utils.TestUtils;
import com.abaenglish.videoclass.ABABaseTestCase;
import com.abaenglish.videoclass.data.network.APIManager;
import com.abaenglish.videoclass.domain.LanguageController;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import static com.abaenglish.shepherd.configuration.configurators.GenericShepherdConfigurator.ShepherdConfiguratorType;

/**
 * Created by Jesus Espejo using mbp13jesus on 18/11/15.
 */
public class APIManagerTest extends ABABaseTestCase {

    @Override
    protected void setUp() throws Exception {
        super.setUp();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testLoginWithEmailAPICall() throws InterruptedException {
        Context aContext = getInstrumentation().getTargetContext();

        final CountDownLatch signal = new CountDownLatch(1);
        final boolean[] succeeded = {false};

        APIManager apiManager = new APIManager();
        apiManager.setApplicationContext(aContext);
        apiManager.loginWithEmail(TestUtils.DEFAULT_USER_EMAIL, TestUtils.DEFAULT_USER_PASSWORD, new APIManager.ABAAPIResponseCallback() {
            @Override
            public void onResponse(String response) {
                succeeded[0] = true;
                signal.countDown();
            }

            @Override
            public void onError(Exception e) {
                signal.countDown();
            }
        });

        // Waiting until the call is finished
        signal.await();
        assertTrue(succeeded[0]);
    }

    public void testLoginWithToken() throws InterruptedException {
        Context aContext = getInstrumentation().getTargetContext();

        final CountDownLatch signal = new CountDownLatch(1);
        final boolean[] succeeded = {false};

        APIManager apiManager = new APIManager();
        apiManager.setApplicationContext(aContext);
        apiManager.loginWithToken(TestUtils.DEFAULT_LOGIN_TOKEN, new APIManager.ABAAPIResponseCallback() {
            @Override
            public void onResponse(String response) {
                succeeded[0] = true;
                signal.countDown();
            }

            @Override
            public void onError(Exception e) {
                signal.countDown();
            }
        });

        // Waiting until the call is finished
        signal.await();
        assertTrue(succeeded[0]);
    }

    public void testGetUnits() throws InterruptedException {
        Context aContext = getInstrumentation().getTargetContext();
        final boolean[] succeeded = {true};

        final List<String> listOfSupportedLanguages = new ArrayList<>();
        Collections.addAll(listOfSupportedLanguages, LanguageController.SUPPORTED_LANGUAGE_ARRAY);
        final CountDownLatch allUnitsDownloadedSignal = new CountDownLatch(listOfSupportedLanguages.size());

        APIManager apiManager = new APIManager();
        apiManager.setApplicationContext(aContext);
        apiManager.setToken(TestUtils.DEFAULT_SESSION_TOKEN);
        for (String language : listOfSupportedLanguages) {
            apiManager.getUnits(language, new APIManager.ABAAPIResponseCallback() {
                @Override
                public void onResponse(String response) {
                    // Cool!
                    allUnitsDownloadedSignal.countDown();
                }

                @Override
                public void onError(Exception e) {
                    succeeded[0] = false;
                    allUnitsDownloadedSignal.countDown();
                }
            });
        }

        allUnitsDownloadedSignal.await();
        assertTrue(succeeded[0]);
    }

    public void testGetPublicIp() throws InterruptedException {
        Context aContext = getInstrumentation().getTargetContext();
        final boolean[] succeeded = {false};
        final CountDownLatch signal = new CountDownLatch(1);

        APIManager apiManager = new APIManager();
        apiManager.setApplicationContext(aContext);
        apiManager.getPublicIp(new APIManager.ABAAPIResponseCallback() {
            @Override
            public void onResponse(String response) {
                succeeded[0] = true;
                signal.countDown();
            }

            @Override
            public void onError(Exception e) {
                signal.countDown();
            }
        });

        // Waiting until the call is finished
        signal.await();
        assertTrue(succeeded[0]);
    }

    public void testRegisterUser() throws InterruptedException {
        Context aContext = getInstrumentation().getTargetContext();

        // Before testing Registry - checking current environment is not default
        ShepherdGenericEnvironment currentEnvironment = ABAShepherdEditor.shared(aContext).environmentForShepherdConfigurationType(aContext, ShepherdConfiguratorType.kShepherdConfiguratorTypeABACore);
        assertNotNull(currentEnvironment);

        // Proceeding to register
        final boolean[] succeeded = {false};
        final CountDownLatch signal = new CountDownLatch(1);

        APIManager apiManager = new APIManager();
        apiManager.setApplicationContext(aContext);
        apiManager.registerUser(TestUtils.DEFAULT_USER_NAME_FOR_TESTS, TestUtils.generateEmail(), TestUtils.DEFAULT_USER_PASSWORD, new APIManager.ABAAPIResponseCallback() {
            @Override
            public void onResponse(String response) {
                succeeded[0] = true;
                signal.countDown();
            }

            @Override
            public void onError(Exception e) {
                signal.countDown();
            }
        });

        // Waiting until the call is finished
        signal.await();
        assertTrue(succeeded[0]);
    }

    public void testGetSectionsContent() throws InterruptedException {
        Context aContext = getInstrumentation().getTargetContext();
        final boolean[] succeeded = {false};
        final CountDownLatch signal = new CountDownLatch(1);

        APIManager apiManager = new APIManager();
        apiManager.setApplicationContext(aContext);
        apiManager.setToken(TestUtils.DEFAULT_SESSION_TOKEN);
        apiManager.getSectionsContent("1", new APIManager.ABAAPIResponseCallback() {
            @Override
            public void onResponse(String response) {
                succeeded[0] = true;
                signal.countDown();
            }

            @Override
            public void onError(Exception e) {
                signal.countDown();
            }
        });

        // Waiting until the call is finished
        signal.await();
        assertTrue(succeeded[0]);
    }

    public void testGetAllSectionsContent() throws InterruptedException {

        Context aContext = getInstrumentation().getTargetContext();
        final boolean[] succeeded = {true};
        final CountDownLatch signal = new CountDownLatch(TestUtils.NUMBER_OF_UNITS);

        // Doing 144 calls
        APIManager apiManager = new APIManager();
        apiManager.setApplicationContext(aContext);
        apiManager.setToken(TestUtils.DEFAULT_SESSION_TOKEN);
        for (int unitIndex = 1; unitIndex <= TestUtils.NUMBER_OF_UNITS; unitIndex++) {
            apiManager.getSectionsContent(String.valueOf(unitIndex), new APIManager.ABAAPIResponseCallback() {
                @Override
                public void onResponse(String response) {
                    signal.countDown();
                }

                @Override
                public void onError(Exception e) {
                    succeeded[0] = false;
                    signal.countDown();
                }
            });
        }

        // Waiting until the call is finished
        signal.await();
        assertTrue(succeeded[0]);
    }

    public void testGetAllSectionsAndAllLanguages() throws InterruptedException {
        Context aContext = getInstrumentation().getTargetContext();

        final List<String> listOfSupportedLanguages = new ArrayList<>();
        Collections.addAll(listOfSupportedLanguages, LanguageController.SUPPORTED_LANGUAGE_ARRAY);

        String currentLanguage = LanguageController.getCurrentLanguage(aContext);

        for (String language : listOfSupportedLanguages) {
            LanguageController.setCurrentLanguage(aContext, language);
            testGetAllSectionsContent();
        }

        // To finish setting the original language back
        LanguageController.setCurrentLanguage(aContext, currentLanguage);
    }

    public void testGetCourseProgress() throws InterruptedException {
        Context aContext = getInstrumentation().getTargetContext();
        final boolean[] succeeded = {false};
        final CountDownLatch signal = new CountDownLatch(1);

        APIManager apiManager = new APIManager();
        apiManager.setToken(TestUtils.DEFAULT_SESSION_TOKEN);
        apiManager.setApplicationContext(aContext);
        apiManager.getCourseProgress(TestUtils.DEFAULT_USER_ID, new APIManager.ABAAPIResponseCallback() {
            @Override
            public void onResponse(String response) {
                succeeded[0] = true;
                signal.countDown();
            }

            @Override
            public void onError(Exception e) {
                succeeded[0] = false;
                signal.countDown();
            }
        });

        // Waiting until the call is finished
        signal.await();
        assertTrue(succeeded[0]);
    }

    public void testGetCompletedActions() throws InterruptedException {
        Context aContext = getInstrumentation().getTargetContext();
        final boolean[] succeeded = {false};
        final CountDownLatch signal = new CountDownLatch(1);

        APIManager apiManager = new APIManager();
        apiManager.setApplicationContext(aContext);
        apiManager.setToken(TestUtils.DEFAULT_SESSION_TOKEN);
        apiManager.getCompletedActions(TestUtils.DEFAULT_USER_ID, "1", null, new APIManager.ABAAPIResponseCallback() {
            @Override
            public void onResponse(String response) {
                succeeded[0] = true;
                signal.countDown();
            }

            @Override
            public void onError(Exception e) {
                succeeded[0] = false;
                signal.countDown();
            }
        });

        // Waiting until the call is finished
        signal.await();
        assertTrue(succeeded[0]);
    }

    public void testGetAllCompletedActions() throws InterruptedException {
        Context aContext = getInstrumentation().getTargetContext();
        final boolean[] succeeded = {true};
        final CountDownLatch signal = new CountDownLatch(TestUtils.NUMBER_OF_UNITS);

        // Doing 144 calls
        APIManager apiManager = new APIManager();
        apiManager.setApplicationContext(aContext);
        apiManager.setToken(TestUtils.DEFAULT_SESSION_TOKEN);
        for (int unitIndex = 1; unitIndex <= TestUtils.NUMBER_OF_UNITS; unitIndex++) {
            apiManager.getCompletedActions(TestUtils.DEFAULT_USER_ID, String.valueOf(unitIndex), null, new APIManager.ABAAPIResponseCallback() {
                @Override
                public void onResponse(String response) {
                    succeeded[0] = true;
                    signal.countDown();
                }

                @Override
                public void onError(Exception e) {
                    succeeded[0] = false;
                    signal.countDown();
                }
            });
        }

        // Waiting until the call is finished
        signal.await();
        assertTrue(succeeded[0]);
    }

    public void testSetCurrentLevel() throws InterruptedException {
        Context aContext = getInstrumentation().getTargetContext();
        final boolean[] succeeded = {true};
        final CountDownLatch signalForFirstChange = new CountDownLatch(1);

        APIManager apiManager = new APIManager();
        apiManager.setApplicationContext(aContext);

        // Changing current level to 6
        apiManager.setCurrentLevel("6", TestUtils.DEFAULT_USER_ID, new APIManager.ABAAPIResponseCallback() {
            @Override
            public void onResponse(String response) {
                succeeded[0] = true;
                signalForFirstChange.countDown();
            }

            @Override
            public void onError(Exception e) {
                succeeded[0] = false;
                signalForFirstChange.countDown();
            }
        });

        // Waiting until the call is finished
        signalForFirstChange.await();
        assertTrue(succeeded[0]);

        final CountDownLatch signalForSecondChange = new CountDownLatch(1);

        // Returning current level to 1
        apiManager.setCurrentLevel("1", TestUtils.DEFAULT_USER_ID, new APIManager.ABAAPIResponseCallback() {
            @Override
            public void onResponse(String response) {
                succeeded[0] = true;
                signalForSecondChange.countDown();
            }

            @Override
            public void onError(Exception e) {
                succeeded[0] = false;
                signalForSecondChange.countDown();
            }
        });

        signalForSecondChange.await();
        assertTrue(succeeded[0]);
    }

    public void testFetchPlanContent() throws InterruptedException {
        Context aContext = getInstrumentation().getTargetContext();
        final boolean[] succeeded = {false};
        final CountDownLatch signal = new CountDownLatch(1);

        APIManager apiManager = new APIManager();
        apiManager.setToken(TestUtils.DEFAULT_SESSION_TOKEN);
        apiManager.setApplicationContext(aContext);
        apiManager.fetchPlanContent(TestUtils.DEFAULT_USER_ID, new APIManager.ABAAPIResponseCallback() {
            @Override
            public void onResponse(String response) {
                succeeded[0] = true;
                signal.countDown();
            }

            @Override
            public void onError(Exception e) {
                succeeded[0] = false;
                signal.countDown();
            }
        });

        // Waiting until the call is finished
        signal.await();
        assertTrue(succeeded[0]);
    }


    public void testChangePassword() throws InterruptedException {
        Context aContext = getInstrumentation().getTargetContext();
        final boolean[] succeeded = {false};

        // Before testing Registry - checking current environment is not default
        ShepherdGenericEnvironment currentEnvironment = ABAShepherdEditor.shared(aContext).environmentForShepherdConfigurationType(aContext, ShepherdConfiguratorType.kShepherdConfiguratorTypeABACore);
        assertNotNull(currentEnvironment);

        //////////////////
        // Registering new user
        //////////////////

        // Proceeding to register
        final CountDownLatch signalFinishedRegister = new CountDownLatch(1);

        final APIManager apiManager = new APIManager();
        apiManager.setApplicationContext(aContext);
        apiManager.registerUser(TestUtils.DEFAULT_USER_NAME_FOR_TESTS, TestUtils.generateEmail(), TestUtils.DEFAULT_USER_PASSWORD, new APIManager.ABAAPIResponseCallback() {
            @Override
            public void onResponse(String response) {

                try {

                    // Parsing result to get new token
                    JSONObject jsonObj = new JSONObject(response);
                    String token = jsonObj.getString("token");
                    apiManager.setToken(token);

                    // Once here the call is completed
                    succeeded[0] = true;

                } catch (JSONException e) {

                    e.printStackTrace();
                    succeeded[0] = false;
                }

                signalFinishedRegister.countDown();
            }

            @Override
            public void onError(Exception e) {
                succeeded[0] = false;
                signalFinishedRegister.countDown();
            }
        });

        // Waiting until the call is finished
        signalFinishedRegister.await();
        assertTrue(succeeded[0]);

        //////////////////
        // Changing password
        //////////////////

        final CountDownLatch signalChangedPassword = new CountDownLatch(1);

        String newPassword = TestUtils.DEFAULT_USER_PASSWORD + "0";
        apiManager.changePassword(newPassword, new APIManager.ABAAPIResponseCallback() {
            @Override
            public void onResponse(String response) {
                succeeded[0] = true;
                signalChangedPassword.countDown();
            }

            @Override
            public void onError(Exception e) {
                succeeded[0] = false;
                signalChangedPassword.countDown();
            }
        });

        // Waiting until the call is finished
        signalChangedPassword.await();
        assertTrue(succeeded[0]);
    }

    public void testDownloadSubtitles() throws InterruptedException {
        Context aContext = getInstrumentation().getTargetContext();
        final boolean[] succeeded = {false};
        final CountDownLatch signal = new CountDownLatch(1);

        String subtitleUrl = "https://campus.abaenglish.com/themes/ABAenglish/media/course/rsc_video/sv/s_sv_049en.srt";
        APIManager apiManager = new APIManager();
        apiManager.setApplicationContext(aContext);
        apiManager.downloadSubtitles(subtitleUrl, new APIManager.ABAAPIResponseCallback() {
            @Override
            public void onResponse(String response) {
                succeeded[0] = true;
                signal.countDown();
            }

            @Override
            public void onError(Exception e) {
                succeeded[0] = false;
                signal.countDown();
            }
        });

        // Waiting until the call is finished
        signal.await();
        assertTrue(succeeded[0]);
    }

    public void testUpdateUserToPremiumWithPlan() throws InterruptedException {
        Context aContext = getInstrumentation().getTargetContext();
        final boolean[] succeeded = {false};
        final CountDownLatch signal = new CountDownLatch(1);

        APIManager apiManager = new APIManager();
        apiManager.setApplicationContext(aContext);
        apiManager.updateUserToPremiumWithPlan(TestUtils.DEFAULT_USER_ID, "IT","USD", "6", "54.99","zztop", "ljsdlfkj", "purchase signature", new APIManager.ABAAPIResponseCallback() {
            @Override
            public void onResponse(String response) {
                succeeded[0] = false;
                signal.countDown();
            }

            @Override
            public void onError(Exception e) {
                succeeded[0] = true;
                signal.countDown();
            }
        });

        // Waiting until the call is finished
        signal.await();
        assertTrue(succeeded[0]);
    }

    // ================================================================================
    // Private methods
    // ================================================================================

}
