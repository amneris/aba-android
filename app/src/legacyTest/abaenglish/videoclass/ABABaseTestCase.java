package com.abaenglish.videoclass;

import android.content.Context;
import android.test.InstrumentationTestCase;

import com.abaenglish.shepherd.ABAShepherdEditor;
import com.abaenglish.shepherd.configuration.configurators.abacore.ABACoreShepherdEnvironment;
import com.abaenglish.shepherd.configuration.engine.model.ShepherdConfiguration;
import com.abaenglish.shepherd.configuration.engine.model.ShepherdGenericEnvironment;
import com.abaenglish.utils.TestUtils;
import com.abaenglish.videoclass.data.network.APIManager;
import com.abaenglish.videoclass.domain.LanguageController;

import java.util.List;

import io.realm.RealmConfiguration;

import static com.abaenglish.shepherd.configuration.configurators.GenericShepherdConfigurator.ShepherdConfiguratorType;

/**
 * Created by Jesus Espejo using mbp13jesus on 16/11/15.
 */
public class ABABaseTestCase extends InstrumentationTestCase {

    public final static String PREMIUM_USER_EMAIL = "student+es@abaenglish.com";
    public final static String PREMIUM_USER_PASSWORD = "abaenglish";
    // Realm
    private RealmConfiguration realmConfiguration;

    @Override
    protected void setUp() throws Exception {
        super.setUp();

        setupIntegrationEnvironment();

        Context aContext = getInstrumentation().getTargetContext();
        APIManager.getInstance().setApplicationContext(aContext);
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        ABAShepherdEditor.shared(getInstrumentation().getTargetContext()).resetCurrentEnvironments(getInstrumentation().getTargetContext());
    }

    public void checkIntegrationEnvironmentIsSet() {
        Context aContext = getInstrumentation().getTargetContext();

        ABACoreShepherdEnvironment environment = (ABACoreShepherdEnvironment) ABAShepherdEditor.shared(aContext).environmentForShepherdConfigurationType(aContext, ShepherdConfiguratorType.kShepherdConfiguratorTypeABACore);
        assertNotNull(environment);
        assertEquals("Integration", environment.getEnvironmentName());
    }

    // ================================================================================
    // Public methods
    // ================================================================================

    public void setRandomCurrentLenguage(Context context) {
        LanguageController.setCurrentLanguage(context, TestUtils.getRandomOfStringArray(LanguageController.SUPPORTED_LANGUAGE_ARRAY));
    }

    protected RealmConfiguration getRealmConfiguration() {
        if (realmConfiguration == null) {
            realmConfiguration = ABAApplication.get().getRealmConfiguration();
        }
        return realmConfiguration;
    }

    // ================================================================================
    // Protected methods - Shepherd configuration
    // ================================================================================

    private void setupIntegrationEnvironment() {

        Context aContext = getInstrumentation().getTargetContext();
        ShepherdConfiguration configuration = searchABAConfiguration(aContext);
        ABACoreShepherdEnvironment environment = getIntegrationEnvironment(configuration.getListOfEnvironments());

        ABAShepherdEditor.shared(aContext).setCurrentEnvironment(aContext, environment, configuration);
    }

    private ShepherdConfiguration searchABAConfiguration(Context aContext) {
        for (ShepherdConfiguration configuration : ABAShepherdEditor.shared(aContext).getListOfConfigurations()) {
            if (configuration.getType().equals(ShepherdConfiguratorType.kShepherdConfiguratorTypeABACore)) {
                return configuration;
            }
        }
        return null;
    }

    private ABACoreShepherdEnvironment getIntegrationEnvironment(List<ShepherdGenericEnvironment> listOfEnvs) {
        for (ShepherdGenericEnvironment env : listOfEnvs) {
            if (env.getEnvironmentName().equalsIgnoreCase("integration")) {
                return (ABACoreShepherdEnvironment) env;
            }
        }
        return null;
    }
}
