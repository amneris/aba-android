package com.abaenglish.videoclass;

import android.content.Context;

import com.abaenglish.utils.TestUtils;
import com.abaenglish.videoclass.data.network.APIManager;
import com.abaenglish.videoclass.data.persistence.ABAAPIError;
import com.abaenglish.videoclass.data.persistence.ABAPhrase;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.domain.content.DataController;
import com.abaenglish.videoclass.domain.content.LevelUnitController;
import com.abaenglish.videoclass.domain.content.VocabularyController;
import com.abaenglish.videoclass.session.SessionService;

import junit.framework.AssertionFailedError;

import java.util.concurrent.CountDownLatch;

import io.realm.Realm;

/**
 * Created by madalin on 24/11/15.
 */
public class PhrasesTest extends ABABaseTestCase {

    private SessionService mSessionService = new SessionService();
    private LevelUnitController mLevelUnitController = new LevelUnitController();
    private Realm realm;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        final CountDownLatch signal = new CountDownLatch(1);
        Context aContext = getInstrumentation().getTargetContext();

        APIManager apiManager = new APIManager();
        apiManager.setApplicationContext(aContext);
        mSessionService.loginWithEmail(aContext, PREMIUM_USER_EMAIL, PREMIUM_USER_PASSWORD, new DataController.ABASimpleCallback() {
            @Override
            public void onSuccess() {
                signal.countDown();
            }

            @Override
            public void onError(ABAAPIError error) {
                signal.countDown();
            }
        });
        signal.await();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        if(realm != null)
            realm.close();
    }

    public void testMarkPhrase() throws InterruptedException {
        realm = Realm.getInstance(getRealmConfiguration());
        ABAUnit randomUnit = TestUtils.getRandomUnit(mLevelUnitController.getAllLevelsDescending(realm));
        final CountDownLatch signal = new CountDownLatch(1);
        final boolean[] succeeded = {false};

        final String randomUnitID = randomUnit.getIdUnit();
        mLevelUnitController.getSectionsforUnit(getInstrumentation().getTargetContext(), randomUnit, new DataController.ABASimpleCallback() {
            @Override
            public void onSuccess() {
                Realm realm = Realm.getInstance(getRealmConfiguration());
                try {
                    ABAUnit currentUnit = mLevelUnitController.getUnitWithId(realm, randomUnitID);
                    ABAPhrase randomPhrase = TestUtils.getRandomPhrase(currentUnit.getSectionVocabulary().getContent());
                    new VocabularyController().setPhraseDone(realm, randomPhrase, currentUnit.getSectionVocabulary(), false);

                    assertTrue(randomPhrase.isListened());
                    assertTrue(randomPhrase.isDone());
                    realm.close();
                    succeeded[0] = true;
                    signal.countDown();
                } catch (AssertionFailedError e) {
                    realm.close();
                    signal.countDown();
                }
            }

            @Override
            public void onError(ABAAPIError error) {
                signal.countDown();
            }
        });
        signal.await();
        assertTrue(succeeded[0]);
    }
}
