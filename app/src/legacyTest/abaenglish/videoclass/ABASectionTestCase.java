package com.abaenglish.videoclass;


import android.content.Context;

import com.abaenglish.utils.TestUtils;
import com.abaenglish.videoclass.data.persistence.ABAAPIError;
import com.abaenglish.videoclass.data.persistence.ABAEvaluation;
import com.abaenglish.videoclass.data.persistence.ABAExercises;
import com.abaenglish.videoclass.data.persistence.ABAExercisesQuestion;
import com.abaenglish.videoclass.data.persistence.ABAFilm;
import com.abaenglish.videoclass.data.persistence.ABAInterpret;
import com.abaenglish.videoclass.data.persistence.ABAInterpretRole;
import com.abaenglish.videoclass.data.persistence.ABAPhrase;
import com.abaenglish.videoclass.data.persistence.ABASpeak;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.data.persistence.ABAVideoClass;
import com.abaenglish.videoclass.data.persistence.ABAVocabulary;
import com.abaenglish.videoclass.data.persistence.ABAWrite;
import com.abaenglish.videoclass.domain.content.DataController;
import com.abaenglish.videoclass.domain.content.EvaluationController;
import com.abaenglish.videoclass.domain.content.ExercisesController;
import com.abaenglish.videoclass.domain.content.FilmController;
import com.abaenglish.videoclass.domain.content.InterpretController;
import com.abaenglish.videoclass.domain.content.LevelUnitController;
import com.abaenglish.videoclass.domain.content.SectionController;
import com.abaenglish.videoclass.domain.content.SpeakController;
import com.abaenglish.videoclass.domain.content.VideoClassController;
import com.abaenglish.videoclass.domain.content.VocabularyController;
import com.abaenglish.videoclass.domain.content.WriteController;
import com.abaenglish.videoclass.presentation.section.videoclass.videoplayer.TimedTextObject;
import com.abaenglish.videoclass.session.SessionService;

import java.util.ArrayList;
import java.util.concurrent.CountDownLatch;

import io.realm.Realm;

/**
 * Created by madalin on 29/11/2015.
 */
public class ABASectionTestCase extends ABABaseTestCase {

    private LevelUnitController levelUnitController = new LevelUnitController();
    private FilmController filmController = new FilmController();
    private SpeakController speakController = new SpeakController();
    private SessionService sessionService = new SessionService();
    private WriteController writeController = new WriteController();
    private InterpretController interpretController = new InterpretController();
    private VideoClassController videoClassController = new VideoClassController();
    private VocabularyController vocabularyController = new VocabularyController();
    private ExercisesController exercisesController = new ExercisesController();
    private EvaluationController evaluationController = new EvaluationController();

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        final CountDownLatch signal = new CountDownLatch(1);
        sessionService.loginWithEmail(getInstrumentation().getTargetContext(), PREMIUM_USER_EMAIL, PREMIUM_USER_PASSWORD, new DataController.ABASimpleCallback() {
            @Override
            public void onSuccess() {
                signal.countDown();
            }

            @Override
            public void onError(ABAAPIError error) {
                signal.countDown();
            }
        });
        signal.await();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        final Context aContext = getInstrumentation().getTargetContext();
        final CountDownLatch signal = new CountDownLatch(1);
        Realm realmToLogOut = Realm.getInstance(getRealmConfiguration());
        sessionService.logout(aContext, new DataController.ABALogoutCallback() {
            @Override
            public void onLogoutFinished() {
                signal.countDown();
            }
        });
        signal.await();
        realmToLogOut.close();
    }

    protected String getAllSectionsForRandomUnit() throws InterruptedException {
        Context aContext = getInstrumentation().getTargetContext();
        final CountDownLatch signal = new CountDownLatch(1);
        final boolean[] succeeded = {false};
        setRandomCurrentLenguage(aContext);

        Realm realmToGetAllSections = Realm.getInstance(getRealmConfiguration());

        ABAUnit randomUnit = TestUtils.getRandomUnit(levelUnitController.getAllLevelsDescending(realmToGetAllSections));
        String randomUnitID = randomUnit.getIdUnit();

        levelUnitController.getSectionsforUnit(aContext, randomUnit, new DataController.ABASimpleCallback() {
            @Override
            public void onSuccess() {
                succeeded[0] = true;
                signal.countDown();
            }

            @Override
            public void onError(ABAAPIError error) {
                signal.countDown();
            }
        });
        signal.await();
        assertTrue(succeeded[0]);
        realmToGetAllSections.close();

        return randomUnitID;
    }

    protected <K extends SectionController> void completeSection(Realm realm, ABAUnit unit, K section) {
        realm.beginTransaction();
        section.setCompletedSection(realm, section.getSectionForUnit(unit));
        realm.commitTransaction();
    }

    protected boolean downloadAllSubtitleFromVideosSection(ArrayList<String> subtitlesUrlArray) throws InterruptedException {

        final boolean[] succeeded = {false};

        for (String subtitleUrl : subtitlesUrlArray) {

            if (subtitleUrl != null && !subtitleUrl.equalsIgnoreCase("")) {

                final CountDownLatch signal = new CountDownLatch(1);
                new FilmController().downloadSubtitles(subtitleUrl, new DataController.ABAAPICallback<TimedTextObject>() {
                    @Override
                    public void onSuccess(TimedTextObject response) {
                        if (response != null) {
                            succeeded[0] = true;
                        }
                        signal.countDown();
                    }

                    @Override
                    public void onError(ABAAPIError error) {
                        succeeded[0] = false;
                        signal.countDown();
                    }
                });
                signal.await();
                if (!succeeded[0]) {
                    return succeeded[0];
                }
            } else {
                return false;
            }
        }

        return succeeded[0];
    }

    protected void completeAllSectionsForUnit(Realm realm, ABAUnit unit) {
        //Complete ABAFilm
        realm.beginTransaction();
        ABAFilm abaFilmSection = unit.getSectionFilm();
        abaFilmSection.setCompleted(true);
        abaFilmSection.setProgress(100);
        realm.commitTransaction();
        assertTrue(filmController.isSectionCompleted(abaFilmSection));

        //Complete Speak
        ABASpeak speakSection = unit.getSectionSpeak();
        while (speakController.getCurrentPhraseForSection(speakSection, SpeakController.ContinueWithFirstUndoneWord) != null) {
            ABAPhrase currentPhrase = speakController.getCurrentPhraseForSection(speakSection, SpeakController.ContinueWithFirstUndoneWord);
            speakController.setPhraseDone(realm, currentPhrase, speakSection, false);
        }
        assertTrue(speakController.isSectionCompleted(speakSection));

        //Complete Write
        ABAWrite writeSection = unit.getSectionWrite();
        while (writeController.getCurrentPhrase(writeSection) != null) {
            ABAPhrase currentPhrase = writeController.getCurrentPhrase(writeSection);
            writeController.setPhraseDone(realm, currentPhrase, writeSection, false);
        }
        assertTrue(writeController.isSectionCompleted(writeSection));

        //Complete Interpreta
        ABAInterpret interpretSection = unit.getSectionInterpret();
        while (!interpretController.allRolesAreCompleted(interpretSection)) {
            for (ABAInterpretRole role : interpretSection.getRoles()) {
                for (ABAPhrase phrase : role.getInterpretPhrase()) {
                    interpretController.setPhraseDoneForRole(realm, phrase, role, true);
                }
            }
        }
        assertTrue(interpretController.isSectionCompleted(interpretSection));

        //Complete VideoClass
        realm.beginTransaction();
        ABAVideoClass videoClassSection = unit.getSectionVideoClass();
        videoClassSection.setCompleted(true);
        videoClassSection.setProgress(100);
        realm.commitTransaction();
        assertTrue(videoClassController.isSectionCompleted(videoClassSection));

        //Complete Exercises
        ABAExercises exercisesSection = unit.getSectionExercises();
        while (exercisesController.getCurrentABAExercisesQuestion(exercisesSection) != null) {
            ABAExercisesQuestion exercisesQuestion = exercisesController.getCurrentABAExercisesQuestion(exercisesSection);
            exercisesController.setBlankPhrasesDoneForExercisesQuestion(realm, exercisesQuestion, exercisesSection);
        }
        assertTrue(exercisesController.isSectionCompleted(exercisesSection));

        //Complete Vocabulary
        ABAVocabulary vocabularySection = unit.getSectionVocabulary();
        while (vocabularyController.getCurrentPhraseForSection(vocabularySection, VocabularyController.ContinueWithFirstUndoneWord) != null) {
            ABAPhrase currentPhrase = vocabularyController.getCurrentPhraseForSection(vocabularySection, VocabularyController.ContinueWithFirstUndoneWord);
            vocabularyController.setPhraseDone(realm, currentPhrase, vocabularySection, false);
        }
        assertTrue(vocabularyController.isSectionCompleted(vocabularySection));

        //Complete Evaluation
        realm.beginTransaction();
        ABAEvaluation evaluationSection = unit.getSectionEvaluation();
        evaluationSection.setCompleted(true);
        evaluationSection.setProgress(100);
        realm.commitTransaction();
        assertTrue(evaluationController.isSectionCompleted(evaluationSection));
    }
}
