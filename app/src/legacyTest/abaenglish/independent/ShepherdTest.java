package com.abaenglish.independent;

import android.content.Context;
import android.test.InstrumentationTestCase;

import com.abaenglish.shepherd.ABAShepherdEditor;
import com.abaenglish.shepherd.configuration.engine.model.ShepherdConfiguration;
import com.abaenglish.shepherd.configuration.engine.model.ShepherdGenericEnvironment;

import java.util.List;

import static com.abaenglish.shepherd.configuration.configurators.GenericShepherdConfigurator.ShepherdConfiguratorType;

/**
 * Created by Jesus Espejo using mbp13jesus on 16/11/15.
 */
public class ShepherdTest extends InstrumentationTestCase {

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        ABAShepherdEditor.shared(getInstrumentation().getTargetContext()).resetCurrentEnvironments(getInstrumentation().getTargetContext());
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    public void testShepherdStartup() {
        ABAShepherdEditor.shared(getInstrumentation().getTargetContext());
    }

    public void testShepherdDefaultEnvironment() {
        Context aContext = getInstrumentation().getTargetContext();

        ShepherdGenericEnvironment env = ABAShepherdEditor.shared(aContext).environmentForShepherdConfigurationType(aContext, ShepherdConfiguratorType.kShepherdConfiguratorTypeABACore);
        assertNotNull(env);
        assertTrue(env.isDefaultEnvironment());
        assertFalse(env.getEnvironmentName().isEmpty());
    }

    public void testShepherdOnProduction() {
        Context aContext = getInstrumentation().getTargetContext();

        ShepherdGenericEnvironment env = ABAShepherdEditor.shared(aContext).environmentForShepherdConfigurationType(aContext, ShepherdConfiguratorType.kShepherdConfiguratorTypeABACore);
        assertNotNull(env);
        assertTrue(env.isDefaultEnvironment());
        assertFalse(env.getEnvironmentName().isEmpty());
        assertEquals(env.getEnvironmentName(), "Production");
    }

    public void testShepherdNonDefaultEnvironment() {
        Context aContext = getInstrumentation().getTargetContext();

        // Checking default environment
        ShepherdGenericEnvironment defaultEnvironment = ABAShepherdEditor.shared(aContext).environmentForShepherdConfigurationType(aContext, ShepherdConfiguratorType.kShepherdConfiguratorTypeABACore);
        assertNotNull(defaultEnvironment);
        assertTrue(defaultEnvironment.isDefaultEnvironment());

        // Changing configuration
        ShepherdConfiguration abaConfiguration = searchConfiguration(aContext, "ABA Endpoints");
        ShepherdGenericEnvironment nonDefaultEnv = getFirstNonDefaultEnvironment(abaConfiguration.getListOfEnvironments());
        ABAShepherdEditor.shared(aContext).setCurrentEnvironment(aContext, nonDefaultEnv, abaConfiguration);

        // Checking current environment is not default
        ShepherdGenericEnvironment currentEnvironment = ABAShepherdEditor.shared(aContext).environmentForShepherdConfigurationType(aContext, ShepherdConfiguratorType.kShepherdConfiguratorTypeABACore);
        assertNotNull(currentEnvironment);
        assertFalse(currentEnvironment.isDefaultEnvironment());

        // Environments are not the same
        assertFalse(currentEnvironment.getEnvironmentName().equalsIgnoreCase(defaultEnvironment.getEnvironmentName()));
    }

    public void testShepherdNonDefaultEnvironmentAndReset() {
        Context aContext = getInstrumentation().getTargetContext();

        // Checking default environment
        ShepherdGenericEnvironment defaultEnvironment = ABAShepherdEditor.shared(aContext).environmentForShepherdConfigurationType(aContext, ShepherdConfiguratorType.kShepherdConfiguratorTypeABACore);
        assertNotNull(defaultEnvironment);
        assertTrue(defaultEnvironment.isDefaultEnvironment());

        // Changing configuration
        ShepherdConfiguration abaConfiguration = searchConfiguration(aContext, "ABA Endpoints");
        ShepherdGenericEnvironment nonDefaultEnv = getFirstNonDefaultEnvironment(abaConfiguration.getListOfEnvironments());
        ABAShepherdEditor.shared(aContext).setCurrentEnvironment(aContext, nonDefaultEnv, abaConfiguration);

        // Checking current environment is not default
        ShepherdGenericEnvironment currentEnvironment = ABAShepherdEditor.shared(aContext).environmentForShepherdConfigurationType(aContext, ShepherdConfiguratorType.kShepherdConfiguratorTypeABACore);
        assertNotNull(currentEnvironment);
        assertFalse(currentEnvironment.isDefaultEnvironment());

        // Environments are not the same
        assertFalse(currentEnvironment.getEnvironmentName().equalsIgnoreCase(defaultEnvironment.getEnvironmentName()));

        // Resetting shepherd
        ABAShepherdEditor.shared(getInstrumentation().getTargetContext()).resetCurrentEnvironments(getInstrumentation().getTargetContext());

        // Reading current environment again
        ShepherdGenericEnvironment newCurrentEnvironment = ABAShepherdEditor.shared(aContext).environmentForShepherdConfigurationType(aContext, ShepherdConfiguratorType.kShepherdConfiguratorTypeABACore);
        assertNotNull(newCurrentEnvironment);
        assertTrue(newCurrentEnvironment.isDefaultEnvironment());

        // Environments are not the same
        assertFalse(newCurrentEnvironment.getEnvironmentName().equalsIgnoreCase(currentEnvironment.getEnvironmentName()));
        // but equal to the first default one
        assertTrue(newCurrentEnvironment.getEnvironmentName().equalsIgnoreCase(defaultEnvironment.getEnvironmentName()));
    }

    // ================================================================================
    // Private methods not to test
    // ================================================================================

    private ShepherdConfiguration searchConfiguration(Context aContext, String configurationName) {
        for (ShepherdConfiguration configuration : ABAShepherdEditor.shared(aContext).getListOfConfigurations()) {
            if (configuration.getConfigurationName().equalsIgnoreCase(configurationName)) {
                return configuration;
            }
        }
        return null;
    }

    private ShepherdGenericEnvironment getFirstNonDefaultEnvironment(List<ShepherdGenericEnvironment> listOfEnvs) {
        for (ShepherdGenericEnvironment env : listOfEnvs) {
            if (!env.isDefaultEnvironment()) {
                return env;
            }
        }
        return null;
    }
}
