package com.abaenglish.independent;

import android.test.InstrumentationTestCase;

import com.abaenglish.videoclass.ABAApplication;
import com.abaenglish.videoclass.data.persistence.ABAUser;

import io.realm.Realm;
import io.realm.RealmQuery;
import io.realm.RealmResults;

/**
 * Created by Jesus Espejo using mbp13jesus on 17/11/15.
 */
public class RealmTest extends InstrumentationTestCase {

    // Realm
    private Realm realm;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        getRealm();

        realm.beginTransaction();
        RealmQuery genericQuery = realm.where(ABAUser.class);
        RealmResults results = genericQuery.findAll();
        results.deleteAllFromRealm();
        realm.commitTransaction();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();

        realm.beginTransaction();
        realm.delete(ABAUser.class);
        realm.commitTransaction();
        realm.close();
    }

    public void testSimpleUserCreation() {

        realm.beginTransaction();

        RealmQuery<ABAUser> query = realm.where(ABAUser.class);
        ABAUser existingUser = query.findFirst();
        assertNull(existingUser);

        existingUser = realm.createObject(ABAUser.class);
        existingUser.setEmail("test@test.com");
        existingUser.setName("test");

        realm.commitTransaction();

        query = realm.where(ABAUser.class);
        existingUser = query.findFirst();
        assertNotNull(existingUser);
    }

    // ================================================================================
    // Private methods
    // ================================================================================

    private Realm getRealm() {
        if (realm == null)
            realm = Realm.getInstance(ABAApplication.get().getRealmConfiguration());
        return realm;
    }
}
