package com.abaenglish.videoclass.unit.currency;

import android.content.Context;
import android.support.test.filters.MediumTest;
import android.support.test.runner.AndroidJUnit4;

import com.abaenglish.videoclass.data.currency.CurrencyAPIManager;
import com.abaenglish.videoclass.data.currency.CurrencyCacheManager;
import com.abaenglish.videoclass.data.currency.CurrencyManager;
import com.abaenglish.videoclass.data.currency.CurrencyPairModel;
import com.abaenglish.videoclass.data.network.APIManager;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertNotSame;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.number.IsCloseTo.closeTo;
import static org.junit.Assert.assertThat;

/**
 * Created by madalin on 27/11/15.
 * Refactored by Jesus
 */
@MediumTest
@RunWith(AndroidJUnit4.class)
public class CurrencyManagerTest {

    private final double DEFAULT_PRICE = 100;
    private Context aContext;
    private boolean isCurrencyLoaded;

    @Before
    public void setUp() throws InterruptedException {
        aContext = getInstrumentation().getTargetContext().getApplicationContext();
        if (!isCurrencyLoaded) {
            isCurrencyLoaded = loadCurrencies();
        }
        CurrencyManager.updateCurrencies(aContext);
    }

    @Test
    public void testCurrencyCacheManager() {
        List<CurrencyPairModel> listOfPairModel = CurrencyCacheManager.loadCurrenciesFromCache(aContext);
        assertNotNull(listOfPairModel);
        assertFalse(listOfPairModel.isEmpty());
    }

    @Test
    public void testHardcodedCacheFile() throws IOException {
        List<CurrencyPairModel> listOfPairModel = CurrencyCacheManager.loadCurrenciesFromStream(CurrencyCacheManager.streamFromHardcodedAssetsFile(aContext));
        assertNotNull(listOfPairModel);
        assertFalse(listOfPairModel.isEmpty());
        assertEquals(listOfPairModel.size(), CurrencyManager.getListOfCurrencies().size());
    }

    @Test
    public void testCurrencyConvertAmounts() throws InterruptedException {
        for (String currencyCode : CurrencyManager.getListOfCurrencies()) {
            if (!currencyCode.equals(CurrencyManager.getTargetCurrency())) {
                assertNotSame(CurrencyManager.convertAmountToDollars(aContext, DEFAULT_PRICE, currencyCode), DEFAULT_PRICE);
            } else {
                assertEquals(CurrencyManager.convertAmountToDollars(aContext, DEFAULT_PRICE, currencyCode), DEFAULT_PRICE);
            }
        }
    }

    @Test
    public void testConvertEurToEur() {
        double amountInEuros = CurrencyManager.convertAmountToEuros(aContext, DEFAULT_PRICE, "EUR");
        assertEquals(amountInEuros, DEFAULT_PRICE);
    }

    @Test
    public void testConvertToUSDtoEUR() {
        double amountInEuros = CurrencyManager.convertAmountToEuros(aContext, DEFAULT_PRICE, "USD");
        double amountInDollars = CurrencyManager.convertAmountToDollars(aContext, amountInEuros, "EUR");

        assertEquals(DEFAULT_PRICE, amountInDollars);
    }

    @Test
    public void testConvertDirectVSIndirectConversion() {
        double amountInEur = CurrencyManager.convertAmountToEuros(aContext, DEFAULT_PRICE, "CAD");

        double amountInUSD = CurrencyManager.convertAmountToDollars(aContext, DEFAULT_PRICE, "CAD");
        double amountInEurFromUSD = CurrencyManager.convertAmountToEuros(aContext, amountInUSD, "USD");
        assertThat(amountInEur,is(closeTo(amountInEurFromUSD, 0.05)));
    }

    @Test
    public void convertFromUAH() {
        double amountInEur = CurrencyManager.convertAmountToEuros(aContext, DEFAULT_PRICE, "UAH");

        double amountInUSD = CurrencyManager.convertAmountToDollars(aContext, DEFAULT_PRICE, "UAH");
        double amountInEurFromUSD = CurrencyManager.convertAmountToEuros(aContext, amountInUSD, "USD");
        
        assertThat(amountInEur, is(closeTo(amountInEurFromUSD, 0.02)));
    }

    private boolean loadCurrencies() throws InterruptedException {
        final CountDownLatch signal = new CountDownLatch(1);
        final boolean[] succeeded = {false};
        CurrencyAPIManager.loadCurrencies(aContext, CurrencyManager.getListOfCurrencies(), CurrencyManager.getTargetCurrency(), new APIManager.ABAAPIResponseCallback() {
            @Override
            public void onResponse(String response) {
                succeeded[0] = true;
                assertNotNull(response);
                signal.countDown();
            }

            @Override
            public void onError(Exception e) {
                signal.countDown();
            }
        });
        signal.await();
        return succeeded[0];
    }
}
