package com.abaenglish.videoclass.critical;

import android.content.Context;

import com.abaenglish.shepherd.ABAShepherdEditor;
import com.abaenglish.shepherd.configuration.configurators.GenericShepherdConfigurator;
import com.abaenglish.shepherd.configuration.engine.model.ShepherdGenericEnvironment;

import org.junit.Before;
import org.junit.Test;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;

/**
 * Created by Jesus Espejo using mbp13jesus on 23/03/17.
 */

public class CriticalTest {

    @Before
    public void setUp() throws Exception {

        ABAShepherdEditor.shared(getInstrumentation().getTargetContext()).resetCurrentEnvironments(getInstrumentation().getTargetContext());
    }

    @Test
    public void testShepherdIsInProduction() {

        Context aContext = getInstrumentation().getTargetContext();

        ShepherdGenericEnvironment env = ABAShepherdEditor.shared(aContext).environmentForShepherdConfigurationType(aContext, GenericShepherdConfigurator.ShepherdConfiguratorType.kShepherdConfiguratorTypeABACore);
        assertNotNull(env);
        assertTrue(env.isDefaultEnvironment());
        assertFalse(env.getEnvironmentName().isEmpty());
        assertEquals(env.getEnvironmentName(), "Production");
    }
}
