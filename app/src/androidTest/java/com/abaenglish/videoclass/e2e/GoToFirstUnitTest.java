package com.abaenglish.videoclass.e2e;

import android.content.Context;
import android.content.Intent;
import android.support.test.rule.ActivityTestRule;

import com.abaenglish.videoclass.presentation.section.SectionActivityInterface;
import com.abaenglish.videoclass.presentation.section.assessment.ABAEvaluationActivity;
import com.abaenglish.videoclass.presentation.section.assessment.EvaluationInitialPage;
import com.abaenglish.videoclass.presentation.section.assessment.result.AssessmentResultActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static android.support.test.InstrumentationRegistry.getInstrumentation;

/**
 * Created by xabierlosada on 21/07/16.
 */

public class GoToFirstUnitTest {

    @Rule
    public ActivityTestRule<ABAEvaluationActivity> activityRule = new ActivityTestRule<>(
            ABAEvaluationActivity.class, true, false);

    private EvaluationInitialPage initialPage;

    @Before
    public void setUp() {
        this.initialPage = new EvaluationInitialPage();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void shouldFitOnEveryPageOfTheHappyPathInEnglish() throws InterruptedException {
        goToFirstUnit();
    }

    public void goToFirstUnit() throws InterruptedException {
        Context targetContext = getInstrumentation()
                .getTargetContext();
        Intent result = new Intent(targetContext, AssessmentResultActivity.class);
        result.putExtra(SectionActivityInterface.EXTRA_UNIT_ID,"1");
        activityRule.launchActivity(result);
        initialPage
                .startAssessment()
                .selectFirstOption()
                .selectFirstOption()
                .selectFirstOption()
                .selectFirstOption()
                .selectFirstOption()
                .selectFirstOption()
                .selectFirstOption()
                .selectFirstOption()
                .selectFirstOption()
                .selectFirstOption()
                .finishAssessment();
    }

}
