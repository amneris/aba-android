package com.abaenglish.videoclass.e2e;

import android.support.test.rule.ActivityTestRule;

import com.abaenglish.videoclass.presentation.tutorial.TutorialActivity;
import com.abaenglish.videoclass.presentation.tutorial.TutorialPage;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static android.content.Context.MODE_PRIVATE;
import static com.abaenglish.videoclass.presentation.base.ABAMasterActivity.RUN_SHARED_PREFERENCES;

/**
 * Created by xabierlosada on 21/07/16.
 */

public class FullNavigationWithScreenshotsTest {

    @Rule
    public ActivityTestRule<TutorialActivity> activityRule = new ActivityTestRule<>(
            TutorialActivity.class);

    private static final String GERMAN = "de";
    private static final String SPANISH = "es";
    private static final String FRENCH = "fr";
    private static final String ENGLISH = "en";
    private static final String ITALIAN = "it";
    private static final String PORTUGUESE = "pt";
    private static final String RUSSIAN = "ru";

    private TutorialPage initialPage;

    @Before
    public void setUp() {
        this.initialPage = new TutorialPage();
    }

    @After
    public void tearDown() {
        activityRule.getActivity()
                .getApplicationContext()
                .getSharedPreferences(RUN_SHARED_PREFERENCES, MODE_PRIVATE)
                .edit()
                .clear()
                .apply();
    }

    @Test
    public void shouldFitOnEveryPageOfTheHappyPathInEnglish() throws InterruptedException {
        takeAScreenShotOfEveryPageFromTheTutorialToLogin(ENGLISH);
        takeAScreenShotOfEveryPageOfTheShell(ENGLISH);
    }

    @Test
    public void shouldFitOnEveryPageOfTheHappyPathFrench() throws InterruptedException {
        takeAScreenShotOfEveryPageFromTheTutorialToLogin(FRENCH);
        takeAScreenShotOfEveryPageOfTheShell(FRENCH);
    }

    @Test
    public void shouldFitOnEveryPageOfTheHappyPathGerman() throws InterruptedException {
        takeAScreenShotOfEveryPageFromTheTutorialToLogin(GERMAN);
        takeAScreenShotOfEveryPageOfTheShell(GERMAN);
    }

    @Test
    public void shouldFitOnEveryPageOfTheHappyPathItalian() throws InterruptedException {
        takeAScreenShotOfEveryPageFromTheTutorialToLogin(ITALIAN);
        takeAScreenShotOfEveryPageOfTheShell(ITALIAN);
    }

    @Test
    public void shouldFitOnEveryPageOfTheHappyPathPortuguese() throws InterruptedException {
        takeAScreenShotOfEveryPageFromTheTutorialToLogin(PORTUGUESE);
        takeAScreenShotOfEveryPageOfTheShell(PORTUGUESE);
    }

    @Test
    public void shouldFitOnEveryPageOfTheHappyPathRussian() throws InterruptedException {
        takeAScreenShotOfEveryPageFromTheTutorialToLogin(RUSSIAN);
        takeAScreenShotOfEveryPageOfTheShell(RUSSIAN);
    }

    @Test
    public void shouldFitOnEveryPageOfTheHappyPathSpanish() throws InterruptedException {
        takeAScreenShotOfEveryPageFromTheTutorialToLogin(SPANISH);
        takeAScreenShotOfEveryPageOfTheShell(SPANISH);
    }

    public void takeAScreenShotOfEveryPageFromTheTutorialToLogin(String tag) throws InterruptedException {
        this.initialPage.setLanguage(activityRule, tag);
        this.initialPage
                .takeAScreenShot(activityRule, getTag("tutorial0",tag))
                .swipeLeftGesture()
                .takeAScreenShot(activityRule, getTag("tutorial1",tag))
                .swipeLeftGesture()
                .takeAScreenShot(activityRule, getTag("tutorial2",tag))
                .swipeLeftGesture()
                .takeAScreenShot(activityRule, getTag("tutorial3",tag))
                .goToRegister()
                .takeAScreenShot(activityRule, getTag("register",tag))
                .goToLogin()
                .takeAScreenShot(activityRule, getTag("loggedIn",tag));
//                .doLogin("un.pavo.friki@gmail.com","un.pavo.friki")
//                .goToProfile()
//                .takeAScreenShot(activityRule, getTag("profile",tag))
//                .goToCertificates()
//                .takeAScreenShot(activityRule, getTag("certificates",tag))
//                .goToHelpDesk()
//                .takeAScreenShot(activityRule, getTag("helpdesk",tag))
//                .goToLevels()
//                .takeAScreenShot(activityRule, getTag("levels",tag))
//                .goToProfile()
//                .doLogout();
    }



    public TutorialPage takeAScreenShotOfEveryPageOfTheShell(String tag) throws InterruptedException {
//        return new LoginPage()
//                .doLogin("un.pavo.friki@gmail.com","un.pavo.friki")
//                .goToProfile()
//                .takeAScreenShot(activityRule, getTag("profile",tag))
//                .goToCertificates()
//                .takeAScreenShot(activityRule, getTag("certificates",tag))
//                .goToHelpDesk()
//                .takeAScreenShot(activityRule, getTag("helpdesk",tag))
//                .goToLevels()
//                .takeAScreenShot(activityRule, getTag("levels",tag))
//                .goToProfile()
//                .doLogout();
        return null;
    }

    private String getTag(String tutorial0, String language) {
        return tutorial0+"_"+language;
    }

}
