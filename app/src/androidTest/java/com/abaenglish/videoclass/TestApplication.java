package com.abaenglish.videoclass;

import com.abaenglish.videoclass.presentation.MockApplicationModule;
import com.abaenglish.videoclass.presentation.di.ApplicationComponent;
import com.abaenglish.videoclass.presentation.di.DaggerApplicationComponent;
import com.abaenglish.videoclass.presentation.section.assessment.DaggerEvaluationComponent;
import com.abaenglish.videoclass.presentation.section.assessment.EvaluationComponent;
import com.abaenglish.videoclass.presentation.section.assessment.EvaluationModule;
import com.abaenglish.videoclass.presentation.section.assessment.result.AssessmentResultComponent;
import com.abaenglish.videoclass.presentation.section.assessment.result.DaggerMockAssessmentResultComponent;
import com.abaenglish.videoclass.presentation.section.assessment.result.MockAssessmentResultModule;

/**
 * Created by xabierlosada on 25/07/16.
 */
public class TestApplication extends ABAApplication {

    public enum ExecutionMode {
        MOCK,
        INTEGRATION,
        PRODUCTION
    }

    public EvaluationComponent evaluationComponent;
    private ExecutionMode executionMode = ExecutionMode.INTEGRATION;
    private AssessmentResultComponent assessmentResultComponent;
    private ApplicationComponent applicationComponent;

    public void setExecutionMode(ExecutionMode executionMode) {
        this.executionMode = executionMode;
    }

    @Override
    public ApplicationComponent getApplicationComponent() {
        if(executionMode == ExecutionMode.MOCK) {
            if (applicationComponent == null) {
                return applicationComponent = DaggerApplicationComponent.builder()
                        .applicationModule(new MockApplicationModule(this))
                        .build();
            }
            return applicationComponent;
        }
        return super.getApplicationComponent();
    }

    @Override
    public EvaluationComponent getEvaluationComponent() {
        if(executionMode == ExecutionMode.MOCK) {

            if (evaluationComponent == null) {
                return evaluationComponent = DaggerEvaluationComponent
                        .builder()
                        .evaluationModule(new EvaluationModule(this))
                        .build();
            }
            return evaluationComponent;
        }
        return super.getEvaluationComponent();
    }

    @Override
    public AssessmentResultComponent getAssessmentResultComponent() {
        if (executionMode == ExecutionMode.MOCK) {
            if(assessmentResultComponent == null) {
                return assessmentResultComponent = DaggerMockAssessmentResultComponent.builder()
                        .applicationComponent(getApplicationComponent())
                        .mockAssessmentResultModule(new MockAssessmentResultModule())
                        .build();
            }
            return assessmentResultComponent;
        }
        return super.getAssessmentResultComponent();
    }
}
