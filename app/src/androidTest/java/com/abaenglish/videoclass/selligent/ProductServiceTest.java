package com.abaenglish.videoclass.selligent;

/*
 * Copyright 2016, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import com.google.gson.Gson;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class ProductServiceTest {

    OkHttpClient okClient;
    Gson gson;

    @Before
    public void setUp() {
        okClient = new OkHttpClient();
        gson = new Gson();
    }

    @Test
    public void testSelligentEndpoint() throws IOException {
        Request request = new Request.Builder()
                .url("http://mobile-selligent-dev-apitools.proxy.aba.land:10002/optiext/optiextension.dll?ID=vHKtblTll2JrfgTN_T2CGgGKw0ygfTyVoGnX20w6LyMVU1vIszMrnimbOXS%2BGz46oyw07SBZHv")
                .build();

        Response response = okClient.newCall(request).execute();
        if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

        String value = response.body().string();

        TierCollection tierCollection = gson.fromJson(value, TierCollection.class);
        assertThat(3, is(tierCollection.tiers.length));
    }

}
