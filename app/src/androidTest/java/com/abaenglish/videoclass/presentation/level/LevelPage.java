package com.abaenglish.videoclass.presentation.level;

import com.abaenglish.videoclass.presentation.shell.ShellPage;

/**
 * Created by xabierlosada on 21/07/16.
 */
public class LevelPage extends ShellPage<LevelPage> {

    @Override
    protected LevelPage getPage() {
        return this;
    }
}
