package com.abaenglish.videoclass.presentation.section.assessment;

import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.presentation.Page;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by xabierlosada on 21/07/16.
 */

public class EvaluationInitialPage extends Page<EvaluationInitialPage> {

    public CongratulationPage goToCourseFinished() {
        onView(withId(R.id.continue_nextUnit)).perform(click());
        return new CongratulationPage();
    }

    @Override
    protected EvaluationInitialPage getPage() {
        return this;
    }

    public ExercisePage startAssessment() {
        onView(withId(R.id.start_evaluation)).perform(click());
        return new ExercisePage();
    }
}
