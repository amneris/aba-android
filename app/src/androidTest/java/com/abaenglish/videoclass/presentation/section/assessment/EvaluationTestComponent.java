package com.abaenglish.videoclass.presentation.section.assessment;

import android.content.Context;

import com.abaenglish.videoclass.analytics.TrackerContract;
import com.abaenglish.videoclass.data.CourseContentService;
import com.abaenglish.videoclass.data.network.oauth.OAuthTokenAccessor;
import com.abaenglish.videoclass.domain.ApplicationConfiguration;
import com.abaenglish.videoclass.domain.abTesting.ServerConfig;
import com.abaenglish.videoclass.domain.abTesting.UnitDetailABTestingConfigurator;
import com.abaenglish.videoclass.session.SessionService;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = EvaluationTestModule.class)
public interface EvaluationTestComponent extends EvaluationComponent {

    UnitDetailABTestingConfigurator abRouter();

    Context context();

    TrackerContract.Tracker tracker();

    ServerConfig serverConfig();

    SessionService sessionService();

//    CourseContentService contentService();

    ApplicationConfiguration appConfig();

    OAuthTokenAccessor tokenAccessor();

    void inject(ABAEvaluationActivityTest target);

    void inject(EvaluationExerciseActivityTest target);
}
