package com.abaenglish.videoclass.presentation.profile;

import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.presentation.shell.ShellPage;
import com.abaenglish.videoclass.presentation.tutorial.TutorialPage;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by xabierlosada on 18/07/16.
 */

public class ProfilePage extends ShellPage<ProfilePage> {

    public TutorialPage doLogout() {
        onView(withId(R.id.logOutButton))
                .perform(scrollTo(),click());
        return new TutorialPage();
    }

    @Override
    protected ProfilePage getPage() {
        return this;
    }
}
