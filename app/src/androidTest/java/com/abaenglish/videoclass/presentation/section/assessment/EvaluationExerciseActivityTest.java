package com.abaenglish.videoclass.presentation.section.assessment;

import android.app.Instrumentation;
import android.content.Context;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.util.Log;

import com.abaenglish.videoclass.TestApplication;
import com.abaenglish.videoclass.data.persistence.ABAEvaluation;
import com.abaenglish.videoclass.data.persistence.ABALevel;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.domain.content.EvaluationController;
import com.abaenglish.videoclass.domain.content.LevelUnitController;
import com.abaenglish.videoclass.domain.content.UserController;
import com.abaenglish.videoclass.presentation.base.ABAMasterActivity;
import com.abaenglish.videoclass.presentation.section.SectionActivityInterface;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;

import javax.inject.Inject;

import io.realm.Realm;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

/**
 * Created by xabierlosada on 21/07/16.
 */
@LargeTest
@RunWith(AndroidJUnit4.class)
public class EvaluationExerciseActivityTest {

    @Inject
    LevelUnitController levelUnitController;
    @Inject
    UserController userController;
    @Inject
    EvaluationController evaluationController;

    @Rule
    public ActivityTestRule<EvaluationExerciseActivity> mActivityRule =
            new ActivityTestRule<>(EvaluationExerciseActivity.class, true, false);

    @Before
    public void setUp() {
        Instrumentation instrumentation = InstrumentationRegistry.getInstrumentation();
        TestApplication app
                = (TestApplication) instrumentation.getTargetContext().getApplicationContext();
        app.setExecutionMode(TestApplication.ExecutionMode.MOCK);
        ((EvaluationTestComponent) app.getEvaluationComponent()).inject(this);
    }


    private Intent getFinalUnitIntent() {
        Context targetContext = getInstrumentation()
                .getTargetContext();
        Intent result = new Intent(targetContext, EvaluationExerciseActivity.class);
        return result.putExtra(SectionActivityInterface.EXTRA_UNIT_ID, "144");
    }

    @Test public void shouldShowAFakeExercise() {
        when(levelUnitController.getUnitWithId(any(Realm.class), any(String.class))).thenReturn(FakeFactory.getFakeUnit());
        when(levelUnitController.isFirstCompleteUnit(any(Realm.class))).thenReturn(false);
        when(levelUnitController.checkIfFileExist(any(ABAUnit.class),any(String.class))).thenReturn(false);
        ArrayList<ABAUnit> abaUnits = new ArrayList<>();
        abaUnits.add(FakeFactory.getFakeUnit());
        when(levelUnitController.getIncompletedUnitsForLevel(any(ABALevel.class))).thenReturn(abaUnits);

        when(userController.getCurrentUser(any(Realm.class))).thenReturn(FakeFactory.getFakeUser());

        doNothing().when(evaluationController).endEvaluationWithOK(any(Realm.class), any(ABAEvaluation.class));
//        doNothing().when(evaluationController).goNextLevelifCurrentIsCompleted(any(Realm.class),any(ABAMasterActivity.class),any(Long.class));

        mActivityRule.launchActivity(getFinalUnitIntent());

        Log.d("Test","test");

    }
}

