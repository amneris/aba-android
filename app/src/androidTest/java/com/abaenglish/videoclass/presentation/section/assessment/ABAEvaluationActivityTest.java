package com.abaenglish.videoclass.presentation.section.assessment;

import android.app.Instrumentation;
import android.content.Context;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;

import com.abaenglish.videoclass.TestApplication;
import com.abaenglish.videoclass.data.persistence.ABAEvaluation;
import com.abaenglish.videoclass.data.persistence.ABAEvaluationQuestion;
import com.abaenglish.videoclass.data.persistence.ABALevel;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.data.persistence.ABAUnitBuilder;
import com.abaenglish.videoclass.data.persistence.ABAUser;
import com.abaenglish.videoclass.domain.content.EvaluationController;
import com.abaenglish.videoclass.domain.content.LevelUnitController;
import com.abaenglish.videoclass.domain.content.UserController;
import com.abaenglish.videoclass.presentation.base.ABAMasterActivity;
import com.abaenglish.videoclass.presentation.section.SectionActivityInterface;
import com.abaenglish.videoclass.presentation.section.assessment.result.AssessmentResultActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.ArrayList;

import javax.inject.Inject;

import io.realm.Realm;
import io.realm.RealmList;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

/**
 * Created by xabierlosada on 26/07/16.
 */
public class ABAEvaluationActivityTest {

    @Inject LevelUnitController levelUnitController;
    @Inject UserController userController;
    @Inject EvaluationController evaluationController;
    private EvaluationInitialPage initialPage;


    @Before
    public void setUp() {
        Instrumentation instrumentation = InstrumentationRegistry.getInstrumentation();
        TestApplication app
                = (TestApplication) instrumentation.getTargetContext().getApplicationContext();
        app.setExecutionMode(TestApplication.ExecutionMode.MOCK);
        EvaluationTestComponent component = (EvaluationTestComponent) app.getEvaluationComponent();
        component.inject(this);
    }


    @Rule
    public ActivityTestRule<ABAEvaluationActivity> mActivityRule =
            new ActivityTestRule<>(ABAEvaluationActivity.class, true, false);

    private static ABAUnit getFakeUnit() {
        ABALevel level = new ABALevel();
        level.setCompleted(true);
        ABAEvaluation evaluation = new ABAEvaluation();
        evaluation.setContent(new RealmList<ABAEvaluationQuestion>());
        level.setIdLevel("5");
        return new ABAUnitBuilder()
                .setCompleted(true)
                .setDataDownloaded(true)
                .setDesc("desc")
                .setTitle("title")
                .setLevel(level)
                .setSectionEvaluation(evaluation)
                .createABAUnit();
    }

    private ABAUser getFakeUser() {
        ABAUser user = new ABAUser();
        user.setTeacherImage("https://pbs.twimg.com/profile_images/737623081246593026/4PiWH8U4.jpg");
        user.setUserId("1");
        return user;
    }

    private Intent getFinalUnitIntent() {
        Context targetContext = getInstrumentation()
                .getTargetContext();
        Intent result = new Intent(targetContext, AssessmentResultActivity.class);
        return result.putExtra(SectionActivityInterface.EXTRA_UNIT_ID, 144);
    }

    @Test
    public void shouldShowTheAssessmentStartConfirmationScreen() {
        when(levelUnitController.getUnitWithId(any(Realm.class), any(String.class))).thenReturn(getFakeUnit());
        when(levelUnitController.isFirstCompleteUnit(any(Realm.class))).thenReturn(false);
        when(levelUnitController.checkIfFileExist(any(ABAUnit.class),any(String.class))).thenReturn(false);
        ArrayList<ABAUnit> abaUnits = new ArrayList<>();
        abaUnits.add(getFakeUnit());
        when(levelUnitController.getIncompletedUnitsForLevel(any(ABALevel.class))).thenReturn(abaUnits);

        when(userController.getCurrentUser(any(Realm.class))).thenReturn(getFakeUser());

        doNothing().when(evaluationController).endEvaluationWithOK(any(Realm.class), any(ABAEvaluation.class));
//        doNothing().when(evaluationController).goNextLevelifCurrentIsCompleted(any(Realm.class),any(ABAMasterActivity.class),any(Long.class));

        mActivityRule.launchActivity(getFinalUnitIntent());


        initialPage = new EvaluationInitialPage();
        initialPage.startAssessment()
                .selectFirstOption()
                .finishAssessment();
    }

}