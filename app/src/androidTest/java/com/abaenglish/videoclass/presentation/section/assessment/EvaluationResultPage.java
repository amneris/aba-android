package com.abaenglish.videoclass.presentation.section.assessment;

import com.abaenglish.videoclass.presentation.Page;

/**
 * Created by xabierlosada on 26/07/16.
 */
public class EvaluationResultPage extends Page<EvaluationResultPage> {

    @Override
    protected EvaluationResultPage getPage() {
        return this;
    }

}
