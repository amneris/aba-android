package com.abaenglish.videoclass.presentation.register;

import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.presentation.Page;
import com.abaenglish.videoclass.presentation.login.LoginPage;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by xabierlosada on 18/07/16.
 */
public class RegisterPage extends Page<RegisterPage> {

    @Override
    protected RegisterPage getPage() {
        return this;
    }

    public LoginPage goToLogin() {
        onView(withId(R.id.loginTextView)).perform(click());
        return new LoginPage();
    }
}
