package com.abaenglish.videoclass.presentation.section.assessment;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.abaenglish.videoclass.analytics.TrackerContract;
import com.abaenglish.videoclass.analytics.helpers.GenericTrackingInterface;
import com.abaenglish.videoclass.data.CourseContentService;
import com.abaenglish.videoclass.data.file.ImageDecoder;
import com.abaenglish.videoclass.data.network.oauth.OAuthTokenAccessor;
import com.abaenglish.videoclass.domain.ApplicationConfiguration;
import com.abaenglish.videoclass.domain.abTesting.ServerConfig;
import com.abaenglish.videoclass.domain.abTesting.UnitDetailABTestingConfigurator;
import com.abaenglish.videoclass.domain.content.EvaluationController;
import com.abaenglish.videoclass.domain.content.LevelUnitController;
import com.abaenglish.videoclass.domain.content.PlanController;
import com.abaenglish.videoclass.domain.content.UserController;
import com.abaenglish.videoclass.presentation.base.custom.FontCache;
import com.abaenglish.videoclass.session.SessionService;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;

import org.json.JSONObject;
import org.mockito.Mockito;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.realm.RealmConfiguration;

/**
 * Created by xabierlosada on 25/07/16.
 */

@Module
public class EvaluationTestModule {

    private final Context context;

    public EvaluationTestModule(Context context) {
        this.context = context;
    }

    @Provides
    @Singleton
    public LevelUnitController providesLevelUnitController() {
        return Mockito.mock(LevelUnitController.class);
    }

    @Provides
    @Singleton
    public FontCache providesFontCache() {
        return Mockito.mock(FontCache.class);
    }

    @Provides
    @Singleton
    public UserController providesUserController() {
        return Mockito.mock(UserController.class);
    }

    @Provides
    @Singleton
    public EvaluationController providesEvaluationController() {
        return Mockito.mock(EvaluationController.class);
    }

    @Provides
    @Singleton
    public ServerConfig providesServerConfig() {
        return Mockito.mock(ServerConfig.class);
    }

    @Provides
    @Singleton
    public UnitDetailABTestingConfigurator providesUnitDetailRouter(ServerConfig serverConfig) {
        return new UnitDetailABTestingConfigurator(serverConfig);
    }

    @Provides
    @Singleton
    public TrackerContract.Tracker providesTracker() {
        return new TrackerContract.Tracker() {


            @Override
            public void trackConfigurationChanged() {

            }

            @Override
            public void trackDownloadStarted(String levelId, String unitId) {

            }

            @Override
            public void trackDownloadCancelled(String levelId, String unitId) {

            }

            @Override
            public void trackDownloadDialog(String levelId, String unitId, GenericTrackingInterface.DownloadDialogType dialogType, GenericTrackingInterface.DownloadDialogResultType resultType) {

            }

            @Override
            public void trackDownloadError(String levelId, String unitId) {

            }

            @Override
            public void trackDownloadFinished(String levelId, String unitId) {

            }

            @Override
            public void trackDownloadedFilesRemoved(String levelId, String unitId) {

            }

            @Override
            public void trackPurchaseActivated() {

            }

            @Override
            public void trackSubscriptionErrorWithSubscriptionStatus(PlanController.SubscriptionResult result) {

            }

            @Override
            public void trackRegistrationShown() {

            }

            @Override
            public void trackSubscriptionShown(GenericTrackingInterface.PlansViewControllerOriginType originType) {

            }

            @Override
            public void trackCourseFinished() {

            }

            @Override
            public void trackFirstAppOpen() {

            }

            @Override
            public void trackGooglePlayServicesOutdated() {

            }

            @Override
            public void trackLogout() {

            }

            @Override
            public void addProperty(String name, String value) {

            }

            @Override
            public void trackWatchedFilm(String levelId, String unitId, String sectionId) {

            }

            @Override
            public void trackListenedAudio(String levelId, String unitId, String sectionId, String exerciseId) {

            }

            @Override
            public void trackRecordedAudio(String levelId, String unitId, String sectionId, String exerciseId) {

            }

            @Override
            public void trackComparedAudio(String levelId, String unitId, String sectionId, String exerciseId) {

            }

            @Override
            public void trackVerifiedText(String levelId, String unitId, String sectionId, String exerciseId, boolean result, String textWritten) {

            }

            @Override
            public void trackGotTextSuggestion(String levelId, String unitId, String sectionId, String exerciseId) {

            }

            @Override
            public void trackStartedInterpretation(String levelId, String unitId, String sectionId) {

            }

            @Override
            public void trackProgressUpdateTotal(String levelId, String unitId, String sectionId, int progress) {

            }

            @Override
            public void trackReviewedTestResults(String levelId, String unitId, String testResults, int result) {

            }

            @Override
            public void trackUserRegistered(String userId, String signInMethod) {

            }

            @Override
            public void trackChangeLevel(String userLevel) {

            }

            @Override
            public void trackUserLogin(String userId, String signInMethod) {

            }

            @Override
            public void trackEnteredUnit(String levelId, String unitId) {

            }

            @Override
            public void trackEnteredSection(String levelId, String unitId, String sectionId) {

            }

            @Override
            public void trackEnteredCourseIndex(String levelId) {

            }

            @Override
            public void trackOpenedHelp() {

            }

            @Override
            public void trackOpenedPrices() {

            }

            @Override
            public void trackSelectedPlan(double amount, String currency, String discountIdentifier, String productDuration) {

            }

            @Override
            public void trackPaid(double amount, String currency, String discountIdentifier, String productDuration) {

            }

            @Override
            public void trackProgressMap(JSONObject jsonObject) {

            }
        };
    }

    @Provides
    @Singleton
    public RealmConfiguration providesRealmConfiguration() {
        return new RealmConfiguration.Builder(this.context)
                .inMemory()
                .build();
    }


    @Provides
    @Singleton
    public ImageLoader providesImageLoader() {

        boolean cacheInMemory = true;
        boolean cacheOnDisc = true;
        boolean resetViewOnLoad = false;

        DisplayImageOptions.Builder optionsBuilder = new DisplayImageOptions.Builder();

        BitmapFactory.Options resizeOptions = new BitmapFactory.Options();
        resizeOptions.inScaled = true;

        ImageDecoder.init(context);

        optionsBuilder.resetViewBeforeLoading(resetViewOnLoad)
                .cacheInMemory(cacheInMemory)
                .cacheOnDisk(cacheOnDisc)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .decodingOptions(resizeOptions)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT);

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this.context)
                .defaultDisplayImageOptions(optionsBuilder.build())
                .memoryCache(new WeakMemoryCache())
                .build();

        ImageLoader.getInstance().init(config);
        return ImageLoader.getInstance();
    }

    @Provides
    @Singleton
    public Context providesContext() {
        return this.context;
    }

    @Singleton
    @Provides
    SessionService providesSessionService() {
        return new SessionService(provideContentService(), providesAppConfiguration());
    }

    @Singleton
    @Provides
    CourseContentService provideContentService() {
        return new CourseContentService(providesAppConfiguration());
    }

    @Singleton
    @Provides
    ApplicationConfiguration providesAppConfiguration() {
        return new ApplicationConfiguration();
    }


    @Singleton
    @Provides
    OAuthTokenAccessor providesTokenAccessor(ApplicationConfiguration appConfig) {
        return new OAuthTokenAccessor(appConfig);
    }
}
