package com.abaenglish.videoclass.presentation.helpdesk;

import com.abaenglish.videoclass.presentation.shell.ShellPage;

/**
 * Created by xabierlosada on 21/07/16.
 */
public class HelpDeskPage extends ShellPage<HelpDeskPage> {

    @Override
    protected HelpDeskPage getPage() {
        return this;
    }
}
