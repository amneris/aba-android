package com.abaenglish.videoclass.presentation.section.assessment.result;

import android.app.Instrumentation;
import android.content.Context;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.abaenglish.videoclass.TestApplication;
import com.abaenglish.videoclass.presentation.section.SectionActivityInterface;
import com.abaenglish.videoclass.presentation.section.assessment.EvaluationResultPage;
import com.abaenglish.videoclass.presentation.section.assessment.result.interactor.AssessmentResult;
import com.abaenglish.videoclass.presentation.section.assessment.result.interactor.QualifyAssessmentUseCase;
import com.abaenglish.videoclass.presentation.section.assessment.result.interactor.QualifyInput;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;

import rx.Single;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static com.abaenglish.videoclass.presentation.section.assessment.result.interactor.AssessmentResult.Grade.FAIL;
import static com.abaenglish.videoclass.presentation.section.assessment.result.interactor.AssessmentResult.Grade.PASS;
import static com.abaenglish.videoclass.presentation.section.assessment.result.interactor.AssessmentResult.Grade.PERFECT;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

/**
 * Created by xabierlosada on 23/07/16.
 */
@LargeTest
@RunWith(AndroidJUnit4.class)
public class AssessmentResultActivityTest {

    @Inject
    QualifyAssessmentUseCase qualifyAssessmentUseCase;

    @Rule
    public ActivityTestRule<AssessmentResultActivity> activityRule =
            new ActivityTestRule<>(AssessmentResultActivity.class, true, false);
    private EvaluationResultPage evaluationPage;

    @Before
    public void setUp() {
        evaluationPage = new EvaluationResultPage();
        Instrumentation instrumentation = InstrumentationRegistry.getInstrumentation();
        TestApplication app
                = (TestApplication) instrumentation.getTargetContext().getApplicationContext();
        app.setExecutionMode(TestApplication.ExecutionMode.MOCK);
        MockAssessmentResultComponent component = (MockAssessmentResultComponent) app.getAssessmentResultComponent();
        component.inject(this);
    }

    private Intent getResultIntent(String unitId, int correctExercises) {
        Context targetContext = getInstrumentation()
                .getTargetContext();
        Intent result = new Intent(targetContext, AssessmentResultActivity.class);
        result.putExtra(SectionActivityInterface.EXTRA_UNIT_ID, unitId);
        result.putExtra(AssessmentResultActivity.EXTRA_CORRECT_EXERCISE, correctExercises);
        return result;
    }

    @Test
    public void shouldShowPerfectScoreForLastUnitOfLevel() {
        AssessmentResult assessmentResult = generateResultFake("33", PERFECT)
                .isLastUnitOfLevel(true)
                .build();
        when(qualifyAssessmentUseCase.build(any(QualifyInput.class))).thenReturn(Single.just(assessmentResult));

        activityRule.launchActivity(getResultIntent("33", 10));
        evaluationPage.takeAScreenShot(activityRule,"shouldShowPerfectScoreForNormalUnit");
    }


    @Test
    public void shouldShowPerfectScoreForLastUnit() {
        AssessmentResult assessmentResult = generateResultFake("144", PERFECT)
                .isCourseFinished(true)
                .build();
        when(qualifyAssessmentUseCase.build(any(QualifyInput.class))).thenReturn(Single.just(assessmentResult));

        activityRule.launchActivity(getResultIntent("144", 10));
        evaluationPage.takeAScreenShot(activityRule,"shouldShowPerfectScoreForNormalUnit");
    }

    @Test
    public void shouldShowPerfectScoreForNormalUnit() {
        AssessmentResult assessmentResult = generateResultFake("143", PERFECT).isCourseFinished(true).build();
        when(qualifyAssessmentUseCase.build(any(QualifyInput.class))).thenReturn(Single.just(assessmentResult));
        activityRule.launchActivity(getResultIntent("143", 10));
        evaluationPage.takeAScreenShot(activityRule,"shouldShowPerfectScoreForLastUnit");
    }

    @Test
    public void shouldShowPassScoreForNormalUnit() {
        AssessmentResult assessmentResult = generateResultFake("144", PASS).isCourseFinished(true).build();
        when(qualifyAssessmentUseCase.build(any(QualifyInput.class))).thenReturn(Single.just(assessmentResult));
        activityRule.launchActivity(getResultIntent("144", 1));
        evaluationPage.takeAScreenShot(activityRule,"shouldShowPassScoreForNormalUnit");
    }


    @Test
    public void shouldShowLinkedinForPassedLastUnitOfTheLevelUnit() {
        AssessmentResult assessmentResult = generateResultFake("144", PERFECT).isLastUnitOfLevel(true).build();
        when(qualifyAssessmentUseCase.build(any(QualifyInput.class))).thenReturn(Single.just(assessmentResult));
        activityRule.launchActivity(getResultIntent("144", 1));
        evaluationPage.takeAScreenShot(activityRule,"shouldShowPassScoreForNormalUnit");
    }

    @Test
    public void shouldShowFailScoreForNormalUnit() {
        AssessmentResult assessmentResult = generateResultFake("144", FAIL).isCourseFinished(true).build();
        when(qualifyAssessmentUseCase.build(any(QualifyInput.class))).thenReturn(Single.just(assessmentResult));
        activityRule.launchActivity(getResultIntent("143", 7));
        new EvaluationResultPage().takeAScreenShot(activityRule,"shouldShowFailScoreForNormalUnit");
    }

    @Test
    public void shouldShowPassedFirstUnit() {
        AssessmentResult assessmentResult = generateResultFake("144", PASS).isFirstCompletedUnit(true).build();
        when(qualifyAssessmentUseCase.build(any(QualifyInput.class))).thenReturn(Single.just(assessmentResult));
        activityRule.launchActivity(getResultIntent("1", 9));
        new EvaluationResultPage().takeAScreenShot(activityRule,"shouldShowPassedFirstUnit");
    }

    private static AssessmentResult.Builder generateResultFake(String unitId, AssessmentResult.Grade grade) {
        int correctAnswers;
        switch (grade) {
            case PASS:
                correctAnswers = 8;
                break;
            case PERFECT:
                correctAnswers = 10;
                break;
            default:
                correctAnswers = 4;
        }
        return AssessmentResult.builder()
                .numberOfQuestions(10)
                .correctAnswers(correctAnswers)
                .grade(grade)
                .teacherImageUrl("https://pbs.twimg.com/profile_images/737623081246593026/4PiWH8U4.jpg")
                .username("Pepe")
                .levelName("The level")
                .levelId("1")
                .unitId(unitId)
                .unitTitle("The title "+unitId)
                .nextUnitId(unitId + 1);
    }
}