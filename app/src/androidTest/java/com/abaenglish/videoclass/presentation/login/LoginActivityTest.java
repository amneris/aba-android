package com.abaenglish.videoclass.presentation.login;

import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Created by xabierlosada on 08/06/16.
 */

@LargeTest
@RunWith(AndroidJUnit4.class)
public class LoginActivityTest {

    @Rule
    public IntentsTestRule<LoginActivity> mActivityRule = new IntentsTestRule<>(
            LoginActivity.class);

    private LoginPage loginPage;

    @Before
    public void setUp() {
        loginPage = new LoginPage();
    }

    @Test
    public void shouldLoginWithDefaultCredentials() throws InterruptedException {
        loginPage.doLogin("rainbow@unicorn.com","rainbow")
                .denyPermissions()
                .denyPermissions()
                .goToProfile()
                .doLogout();
    }

    @Test
    public void shouldLoginWithFacebook() throws Exception {
        loginPage.doLoginWithFacebook()
                .denyPermissions()
                .denyPermissions()
                .goToProfile()
                .doLogout();
    }
}