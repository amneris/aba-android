package com.abaenglish.videoclass.presentation.section.assessment.result;

import com.abaenglish.videoclass.presentation.di.ApplicationComponent;
import com.abaenglish.videoclass.presentation.section.assessment.ActivityScope;

import dagger.Component;

/**
 * Created by xabierlosada on 27/07/16.
 */
@ActivityScope
@Component(modules = MockAssessmentResultModule.class, dependencies = ApplicationComponent.class)
public interface MockAssessmentResultComponent extends AssessmentResultComponent {
    void inject(AssessmentResultActivityTest target);
}
