package com.abaenglish.videoclass.presentation.certificate;

import com.abaenglish.videoclass.presentation.shell.ShellPage;

/**
 * Created by xabierlosada on 21/07/16.
 */
public class CertificatePage extends ShellPage<CertificatePage> {
    @Override
    protected CertificatePage getPage() {
        return this;
    }
}