package com.abaenglish.videoclass.presentation.course;

import com.abaenglish.videoclass.presentation.shell.ShellPage;

/**
 * Created by xabierlosada on 18/07/16.
 */
public class CoursePage extends ShellPage<CoursePage> {

    @Override
    protected CoursePage getPage() {
        return this;
    }
}
