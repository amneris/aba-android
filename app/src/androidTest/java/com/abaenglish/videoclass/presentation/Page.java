package com.abaenglish.videoclass.presentation;

import android.app.Activity;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.uiautomator.UiDevice;

import com.abaenglish.videoclass.domain.LanguageController;
import com.jraska.falcon.FalconSpoon;

/**
 * Created by xabierlosada on 18/07/16.
 */

public abstract class Page<T extends Page> {

    private UiDevice mDevice;

    protected abstract T getPage();

    public Page() {
        this.mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
    }

    public UiDevice getDevice() {
        return mDevice;
    }

    public T setLanguage(ActivityTestRule activityTestRule, String language) {
        final Activity activity = activityTestRule.getActivity();
        LanguageController.setCurrentLanguage(activityTestRule.getActivity(), language);
        try {
            activityTestRule.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    activity.recreate();
                }
            });
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return getPage();
    }

    public T takeAScreenShot(ActivityTestRule activityTestRule, String tag) {
        FalconSpoon.screenshot(activityTestRule.getActivity(), tag);
        return getPage();
    }

}
