package com.abaenglish.videoclass.presentation.shell;

import android.os.Build;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiSelector;
import android.util.Log;

import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.presentation.Page;
import com.abaenglish.videoclass.presentation.certificate.CertificatePage;
import com.abaenglish.videoclass.presentation.course.CoursePage;
import com.abaenglish.videoclass.presentation.helpdesk.HelpDeskPage;
import com.abaenglish.videoclass.presentation.level.LevelPage;
import com.abaenglish.videoclass.presentation.profile.ProfilePage;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Created by xabierlosada on 18/07/16.
 */
public abstract class ShellPage<T extends ShellPage<T>> extends Page<T> {

    public T denyPermissions() throws InterruptedException {
        Thread.sleep(300);
        if (Build.VERSION.SDK_INT >= 23) {
            UiObject allowPermissions = super.getDevice().findObject(new UiSelector().text("Deny"));
            if (allowPermissions.exists()) {
                try {
                    allowPermissions.click();
                } catch (UiObjectNotFoundException e) {
                    Log.e(e.getLocalizedMessage(), "There is no permissions dialog to interact with ");
                }
            }
        }
        return getPage();
    }

    public ProfilePage goToProfile() {
        onView(withId(R.id.toolbarLeftButton)).perform(click());
        onView(withText("YOUR ACCOUNT")).perform(click());
        return new ProfilePage();
    }

    public LevelPage goToLevels() {
        onView(withId(R.id.toolbarLeftButton)).perform(click());
        onView(withText(R.string.levelsMenuItemKey)).perform(click());
        return new LevelPage();
    }

    public HelpDeskPage goToHelpDesk() {
        onView(withId(R.id.toolbarLeftButton)).perform(click());
        onView(withText(R.string.helpMenuItemKey)).perform(click());
        return new HelpDeskPage();
    }

    public CoursePage goToCourses() {
        onView(withId(R.id.toolbarLeftButton)).perform(click());
        onView(withText(R.string.unitsMenuItemKey)).perform(click());
        return new CoursePage();
    }

    public CertificatePage goToCertificates() {
        onView(withId(R.id.toolbarLeftButton)).perform(click());
        onView(withText(R.string.yourCertsMenuItemKey)).perform(click());
        return new CertificatePage();
    }


}



