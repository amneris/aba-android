package com.abaenglish.videoclass.presentation.section.assessment;

import com.abaenglish.videoclass.data.persistence.ABAEvaluation;
import com.abaenglish.videoclass.data.persistence.ABAEvaluationOption;
import com.abaenglish.videoclass.data.persistence.ABAEvaluationQuestion;
import com.abaenglish.videoclass.data.persistence.ABALevel;
import com.abaenglish.videoclass.data.persistence.ABAUnit;
import com.abaenglish.videoclass.data.persistence.ABAUnitBuilder;
import com.abaenglish.videoclass.data.persistence.ABAUser;

import io.realm.RealmList;

/**
 * Created by xabierlosada on 26/07/16.
 */

public class FakeFactory {


    public static ABAUnit getFakeUnit() {
        ABALevel level = new ABALevel();
        level.setCompleted(true);
        level.setName("BUSINESS");
        ABAEvaluation evaluation = new ABAEvaluation();
        ABAEvaluationQuestion evaluationQuestion = new ABAEvaluationQuestion();
        evaluationQuestion.setQuestion("WTF?");
        ABAEvaluationOption evaluationOptionA = new ABAEvaluationOption();
        evaluationOptionA.setGood(true);
        evaluationOptionA.setText("MotherFucker");
        evaluationOptionA.setOptionLetter("a");
        ABAEvaluationOption evaluationOptionB = new ABAEvaluationOption();
        evaluationOptionB.setGood(false);
        evaluationOptionB.setText("MotherFucker");
        evaluationOptionB.setOptionLetter("b");
        ABAEvaluationOption evaluationOptionC = new ABAEvaluationOption();
        evaluationOptionC.setGood(false);
        evaluationOptionC.setText("MotherFucker");
        evaluationOptionC.setOptionLetter("c");
        RealmList<ABAEvaluationOption> options = new RealmList<>();
        options.add(evaluationOptionA);
        options.add(evaluationOptionB);
        options.add(evaluationOptionC);

        evaluationQuestion.setOptions(options);

        RealmList<ABAEvaluationQuestion> questions =  new RealmList<>();
        questions.add(evaluationQuestion);

        evaluation.setContent(questions);

        level.setIdLevel("6");
        return new ABAUnitBuilder()
                .setCompleted(true)
                .setDataDownloaded(true)
                .setDesc("desc")
                .setTitle("title")
                .setLevel(level)
                .setSectionEvaluation(evaluation)
                .createABAUnit();
    }

    public static ABAUser getFakeUser() {
        ABAUser user = new ABAUser();
        user.setTeacherImage("https://pbs.twimg.com/profile_images/737623081246593026/4PiWH8U4.jpg");
        return user;
    }

}
