package com.abaenglish.videoclass.presentation.login;

import android.support.test.runner.lifecycle.Stage;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiSelector;
import android.widget.Button;
import android.widget.EditText;

import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.presentation.BusyWhileLoadingActivityIdlingResource;
import com.abaenglish.videoclass.presentation.Page;
import com.abaenglish.videoclass.presentation.course.CoursePage;
import com.abaenglish.videoclass.presentation.shell.MenuActivity;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by xabierlosada on 18/07/16.
 */

public class LoginPage extends Page<LoginPage> {

    public CoursePage doLogin(String username, String password) throws InterruptedException {
        onView(withId(R.id.emailEditTextInput)).perform(replaceText(username));
        onView(withId(R.id.passwordEditTextInput)).perform(replaceText(password));
        onView(withId(R.id.loginActionButton)).perform(click());
        BusyWhileLoadingActivityIdlingResource idlingResource = new BusyWhileLoadingActivityIdlingResource(MenuActivity.class, Stage.RESUMED);
        while (!idlingResource.isIdleNow()) {
            Thread.sleep(300);
        }
        return new CoursePage();
    }

    public CoursePage doLoginWithFacebook() throws UiObjectNotFoundException, InterruptedException {
        getDevice().pressBack(); //Close keyboard
        onView(withId(R.id.loginFacebookButton)).perform(click());

        UiObject editTextMail = getDevice().findObject(new UiSelector()
                .instance(0) //Email
                .className(EditText.class));
        editTextMail.setText("un.pavo.friki@gmail.com");

        UiObject editTextPassword = getDevice().findObject(new UiSelector()
                .instance(1) //Password
                .className(EditText.class));
        editTextPassword.setText("un.pavo.friki");

        getDevice().findObject(new UiSelector()
                .className(Button.class)).click();

        getDevice().findObject(new UiSelector()
                .instance(1)
                .className(Button.class)).click();

        BusyWhileLoadingActivityIdlingResource idlingResource = new BusyWhileLoadingActivityIdlingResource(MenuActivity.class, Stage.RESUMED);
        while (!idlingResource.isIdleNow()) {
            Thread.sleep(300);
        }
        return new CoursePage();
    }

    @Override
    protected LoginPage getPage() {
        return this;
    }
}
