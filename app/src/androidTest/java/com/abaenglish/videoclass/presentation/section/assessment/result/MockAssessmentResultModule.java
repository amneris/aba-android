package com.abaenglish.videoclass.presentation.section.assessment.result;

import android.content.Context;

import com.abaenglish.videoclass.analytics.TrackerContract;
import com.abaenglish.videoclass.presentation.section.assessment.ActivityScope;
import com.abaenglish.videoclass.presentation.section.assessment.result.interactor.FinishEvaluationUseCase;
import com.abaenglish.videoclass.presentation.section.assessment.result.interactor.QualifyAssessmentUseCase;

import org.mockito.Mockito;

import dagger.Module;
import dagger.Provides;

/**
 * Created by xabierlosada on 27/07/16.
 */

@Module
public class MockAssessmentResultModule {

    QualifyAssessmentUseCase mockQualifyAssessmetUseCase = Mockito.mock(QualifyAssessmentUseCase.class);

    @Provides
    @ActivityScope
    public AssessmentResultPresenter providesResultPresenter(
            Context context,
            QualifyAssessmentUseCase qualifyAssessmentUseCase,
            FinishEvaluationUseCase finishEvaluationUseCase,
            TrackerContract.Tracker tracker) {
        return new AssessmentResultPresenter(qualifyAssessmentUseCase, finishEvaluationUseCase, tracker);
    }

    @Provides
    @ActivityScope
    public QualifyAssessmentUseCase providesQualifyAssessmentUseCase() {
        return mockQualifyAssessmetUseCase;
    }

    @Provides
    @ActivityScope
    public FinishEvaluationUseCase providesFinishEvaluationUseCase() {
        return Mockito.mock(FinishEvaluationUseCase.class);
    }
}