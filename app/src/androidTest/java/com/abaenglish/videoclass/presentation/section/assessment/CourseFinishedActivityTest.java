package com.abaenglish.videoclass.presentation.section.assessment;

import android.content.Context;
import android.content.Intent;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import com.squareup.spoon.Spoon;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.InstrumentationRegistry.getInstrumentation;

/**
 * Created by xabierlosada on 21/07/16.
 */
@LargeTest
@RunWith(AndroidJUnit4.class)
public class CourseFinishedActivityTest {

    @Rule
    public ActivityTestRule<CourseFinishedActivity> mActivityRule = new ActivityTestRule<>(
            CourseFinishedActivity.class, true, false);
    private CongratulationPage initialPage;

    @Before
    public void setUp() {
        Context targetContext = getInstrumentation()
                .getTargetContext();
        Intent intent = new Intent(targetContext,CourseFinishedActivity.class);
        intent.putExtra(CourseFinishedActivity.EXTRA_USERNAME, "Maria");
        intent.putExtra(CourseFinishedActivity.EXTRA_TEACHER_IMAGE, "https://pbs.twimg.com/profile_images/737623081246593026/4PiWH8U4.jpg");

        mActivityRule.launchActivity(intent);
        initialPage = new CongratulationPage();
    }

    @Test
    public void shouldShowCongratulationsScreen() throws Exception {
        Spoon.screenshot(mActivityRule.getActivity(),"course_finished");
    }

    @Test
    public void shouldFollowABAWithLinkedin() throws Exception {
        initialPage.shareWithLinkedin();
        Spoon.screenshot(mActivityRule.getActivity(),"linkedin");
    }

    @Test
    public void shouldFollowABAWithTwitter() throws Exception {
        initialPage.shareWithTwitter();
        Spoon.screenshot(mActivityRule.getActivity(),"twitter");
    }

    @Test
    public void shouldFollowABAWithFacebook() throws Exception {
        initialPage.shareWithFacebook();
        Spoon.screenshot(mActivityRule.getActivity(),"facebook");
    }

}
