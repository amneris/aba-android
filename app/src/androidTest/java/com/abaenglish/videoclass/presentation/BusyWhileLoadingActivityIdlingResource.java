package com.abaenglish.videoclass.presentation;

import android.app.Activity;
import android.support.test.espresso.IdlingResource;
import android.support.test.runner.lifecycle.ActivityLifecycleCallback;
import android.support.test.runner.lifecycle.ActivityLifecycleMonitorRegistry;
import android.support.test.runner.lifecycle.Stage;

public class BusyWhileLoadingActivityIdlingResource implements IdlingResource {
    private ResourceCallback resourceCallback;
    private final String name;
    // Need this strong reference because ActivityLifecycleMonitorRegistry won't hold one
    private final ActivityLifecycleCallback activityLifecycleCallback;
    private boolean isIdle = false;

    public BusyWhileLoadingActivityIdlingResource(
            final Class<? extends Activity> clazz, Stage stage){
        name = BusyWhileLoadingActivityIdlingResource.class.getSimpleName()+" "+clazz.getSimpleName();
        activityLifecycleCallback = new WaitActivityStartLifeCycleCallback(clazz, stage);

        ActivityLifecycleMonitorRegistry.getInstance()
                .addLifecycleCallback(activityLifecycleCallback);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public boolean isIdleNow() {
        return isIdle;
    }

    @Override
    public void registerIdleTransitionCallback(ResourceCallback resourceCallback) {
        this.resourceCallback = resourceCallback;
    }

    private class WaitActivityStartLifeCycleCallback implements ActivityLifecycleCallback {

        private Class<? extends Activity> myClass;
        private Stage stage;

        public WaitActivityStartLifeCycleCallback(Class<? extends Activity> myClass, Stage stage) {
            this.myClass = myClass;
            this.stage = stage;
        }

        @Override
        public void onActivityLifecycleChanged(Activity activity, Stage stage) {
            if (!myClass.isAssignableFrom(activity.getClass())) {
                return;
            }
            if (this.stage.equals(stage)) {
                isIdle = true;
                ActivityLifecycleMonitorRegistry.getInstance()
                        .removeLifecycleCallback(activityLifecycleCallback);
                resourceCallback.onTransitionToIdle();
            }
        }
    }
}