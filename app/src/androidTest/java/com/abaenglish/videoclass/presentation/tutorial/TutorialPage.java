package com.abaenglish.videoclass.presentation.tutorial;

import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.presentation.Page;
import com.abaenglish.videoclass.presentation.login.LoginPage;
import com.abaenglish.videoclass.presentation.register.RegisterPage;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.swipeLeft;
import static android.support.test.espresso.action.ViewActions.swipeRight;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by xabierlosada on 18/07/16.
 */
public class TutorialPage extends Page<TutorialPage> {

    public LoginPage goToLogin() {
        onView(withId(R.id.loginTextView)).perform(click());
        return new LoginPage();
    }

    public RegisterPage goToRegister() {
        onView(withId(R.id.registerButton)).perform(click());
        return new RegisterPage();
    }

    public TutorialPage swipeLeftGesture() {
        onView(withId(R.id.startABTestViewPager))
                .perform(swipeLeft());
        return this;
    }

    public TutorialPage swipeRightGesture() {
        onView(withId(R.id.startABTestViewPager))
                .perform(swipeRight());
        return this;
    }

    @Override
    protected TutorialPage getPage() {
        return this;
    }
}
