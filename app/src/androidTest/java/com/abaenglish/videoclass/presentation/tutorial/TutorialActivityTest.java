package com.abaenglish.videoclass.presentation.tutorial;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class TutorialActivityTest {

    private TutorialPage initialPage;

    @Rule
    public ActivityTestRule<TutorialActivity> mActivityRule = new ActivityTestRule<>(
            TutorialActivity.class);

    @Before
    public void setUp() {
        initialPage = new TutorialPage();
    }

    @Test
    public void shouldLoginFromSecondPage() throws Exception {
        initialPage.goToLogin()
                .doLoginWithFacebook()
                .denyPermissions()
                .denyPermissions()
                .goToProfile()
                .doLogout();
    }

    @Test
    public void shouldSwipeBetweenScreensAndLogin() throws Exception {
        initialPage
                .goToLogin()
                .doLoginWithFacebook()
                .denyPermissions()
                .denyPermissions()
                .goToProfile()
                .doLogout();
    }

}