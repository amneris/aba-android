package com.abaenglish.videoclass.presentation.section.assessment;

import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.presentation.Page;
import com.abaenglish.videoclass.presentation.course.CoursePage;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by xabierlosada on 21/07/16.
 */
public class CongratulationPage extends Page<CongratulationPage> {

    public CoursePage closePage() {
        onView(withId(R.id.closeButton)).perform(click());
        return new CoursePage();
    }

    public CongratulationPage shareWithFacebook() {
        onView(withId(R.id.facebookFab))
                .perform(click());
        return this;
    }


    public CongratulationPage shareWithTwitter() {
        onView(withId(R.id.twitterFab))
                .perform(click());
        return this;
    }


    public CongratulationPage shareWithLinkedin() {
        onView(withId(R.id.linkedinFab))
                .perform(click());
        return this;
    }

    @Override
    protected CongratulationPage getPage() {
        return this;
    }
}
