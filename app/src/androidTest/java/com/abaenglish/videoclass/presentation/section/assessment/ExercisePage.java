package com.abaenglish.videoclass.presentation.section.assessment;

import com.abaenglish.videoclass.R;
import com.abaenglish.videoclass.presentation.Page;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by xabierlosada on 21/07/16.
 */

public class ExercisePage extends Page<ExercisePage> {

    public CongratulationPage goToCourseFinished() {
        onView(withId(R.id.continue_nextUnit)).perform(click());
        return new CongratulationPage();
    }

    public ExercisePage selectFirstOption() {
        onView(withId(R.id.evaluationOption1)).perform(click());
        return getPage();
    }

    public ExercisePage selectSecondOption() {
        onView(withId(R.id.evaluationOption2)).perform(click());
        return getPage();
    }

    public ExercisePage selectThirdOption() {
        onView(withId(R.id.evaluationOption3)).perform(click());
        return getPage();
    }

    public EvaluationResultPage finishAssessment() {
        return new EvaluationResultPage();
    }

    @Override
    protected ExercisePage getPage() {
        return this;
    }
}
