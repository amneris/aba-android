package com.abaenglish.videoclass.presentation;

import android.app.Application;

import com.abaenglish.videoclass.presentation.di.ApplicationModule;

import io.realm.RealmConfiguration;

/**
 * Created by xabierlosada on 27/07/16.
 */
public class MockApplicationModule extends ApplicationModule {

    private final Application application;

    public MockApplicationModule(Application application) {
        super(application);
        this.application = application;
    }

    @Override
    public RealmConfiguration providesRealm() {
        return new RealmConfiguration.Builder(application)
                .name("test.realm")
                .inMemory()
                .build();
    }
}
