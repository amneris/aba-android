package com.abaenglish.videoclass.utils;

import android.app.Instrumentation;
import android.support.test.InstrumentationRegistry;

import com.abaenglish.videoclass.TestApplication;
import com.abaenglish.videoclass.data.persistence.ABAEvaluationOption;
import com.abaenglish.videoclass.data.persistence.ABALevel;
import com.abaenglish.videoclass.data.persistence.ABAPhrase;
import com.abaenglish.videoclass.data.persistence.ABAUnit;

import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;

/**
 * Created by Jesus Espejo using mbp13jesus on 18/11/15.
 */
public class TestUtils {

    // Login
    public static String DEFAULT_USER_EMAIL = "student+esf@abaenglish.com";
    public static String DEFAULT_USER_PASSWORD = "abaenglish";
    public static String DEFAULT_LOGIN_TOKEN = "a45e3c3fce701c057e5321b36b4bc871";
    public static String DEFAULT_USER_ID = "83";
    public static String DEDAULT_USER_COUNTRY = "ES";
    public static String DEFAULT_USER_LANG = "es";

    // Session
    public static String DEFAULT_SESSION_TOKEN = "aeea91e1db7bd49423502b567c1f7bb6";

    // Register
    public static String DEFAULT_USER_NAME_FOR_TESTS = "Automated Test";

    // Units
    public static final int NUMBER_OF_LEVEL = 6;
    public static final int NUMBER_OF_UNIT_PER_LEVEL = 24;
    public static final int NUMBER_OF_UNITS = 144;


    // ================================================================================
    // Methods
    // ================================================================================

    public static void setMockMode() {
        Instrumentation instrumentation = InstrumentationRegistry.getInstrumentation();
        TestApplication app
                = (TestApplication) instrumentation.getTargetContext().getApplicationContext();
        app.setExecutionMode(TestApplication.ExecutionMode.MOCK);
    }

    public static String generateEmail() {
        String emailAccount = new RandomStringGenerator(20).nextString();
        return emailAccount + "@abaenglish.com";
    }

    public static String generateFacebookID() {
        String facebookID = new RandomStringGenerator(16).nextNumberString();
        return facebookID;
    }

    public static ABAUnit getRandomUnit(List<ABALevel> levelArrayList) {
        List<ABAUnit> unitsArrayList = new ArrayList<>();
        for (ABALevel level : levelArrayList) {
            for (ABAUnit unit : level.getUnits()) {
                unitsArrayList.add(unit);
            }
        }
        return unitsArrayList.get(getRandomForInteger(unitsArrayList.size()));
    }

    public static ABALevel getRandomLevel(List<ABALevel> levelArrayList) {
        return levelArrayList.get(getRandomForInteger(levelArrayList.size()));
    }

    public static ABALevel getRandomLevelWithoutCurrent(List<ABALevel> levelArrayList, ABALevel currentUserLevel) {
        List<ABALevel> levelsArray = new ArrayList<>(levelArrayList);
        levelsArray.remove(currentUserLevel);
        return levelsArray.get(getRandomForInteger(levelsArray.size()));
    }

    public static ABAPhrase getRandomPhrase(RealmList<ABAPhrase> phraseArrayList) {
        return phraseArrayList.get(getRandomForInteger(phraseArrayList.size()));
    }

    public static String getRandomOfStringArray(String[] stringArrayList) {
        return stringArrayList[getRandomForInteger(stringArrayList.length)];
    }

    public static String getRandomOfList(List<String> arrayList) {
        return arrayList.get(getRandomForInteger(arrayList.size()));
    }

    public static ABAEvaluationOption getRandomOption(List<ABAEvaluationOption> optionsArrayList) {
        return optionsArrayList.get(getRandomForInteger(optionsArrayList.size()));
    }

    // ================================================================================
    // private methods
    // ================================================================================

    private static int getRandomForInteger(int integer) {
        //Random rand = new Random();
        //return rand.nextInt(integer);
        return 1;
    }
}
