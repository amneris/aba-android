package com.abaenglish.videoclass.utils;

import java.util.Random;

/**
 * Created by Jesus Espejo using mbp13jesus on 18/11/15.
 */
public class RandomStringGenerator {

    private static final char[] symbols;
    private static final char[] numberSymbols;

    static {
        StringBuilder tmp = new StringBuilder();
        for (char ch = '0'; ch <= '9'; ++ch)
            tmp.append(ch);
        numberSymbols = tmp.toString().toCharArray();
    }

    static {
        StringBuilder tmp = new StringBuilder();
        for (char ch = '0'; ch <= '9'; ++ch)
            tmp.append(ch);
        for (char ch = 'a'; ch <= 'z'; ++ch)
            tmp.append(ch);
        symbols = tmp.toString().toCharArray();
    }

    private final Random random = new Random();

    private final char[] buf;

    public RandomStringGenerator(int length) {
        if (length < 1)
            throw new IllegalArgumentException("length < 1: " + length);
        buf = new char[length];
    }

    public String nextString() {
        for (int idx = 0; idx < buf.length; ++idx)
            buf[idx] = symbols[random.nextInt(symbols.length)];
        return new String(buf);
    }

    public String nextNumberString() {
        for (int idx = 0; idx < buf.length; ++idx)
            buf[idx] = numberSymbols[random.nextInt(numberSymbols.length)];
        return new String(buf);
    }
}
