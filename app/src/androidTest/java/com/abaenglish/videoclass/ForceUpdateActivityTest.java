package com.abaenglish.videoclass;

import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.UiDevice;
import android.test.suitebuilder.annotation.LargeTest;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasAction;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasData;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.allOf;

/**
 * ABA3.0 File created by xilosada on 29/06/16.
 */
@LargeTest
@RunWith(AndroidJUnit4.class)
public class ForceUpdateActivityTest {

    @Rule
    public IntentsTestRule<ForceUpdateActivity> mIntentRule = new IntentsTestRule<>(
            ForceUpdateActivity.class);
    private UiDevice uiDevice;

    @Before
    public void setup() {
        uiDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
    }

    @After
    public void tearDown() {
        uiDevice.pressHome();
    }

    @Test
    public void shouldOpenPlayStoreWhenButtonIsClicked() {
        onView(withId(R.id.goToStoreButton)).perform(click());
        intended(allOf(hasAction(Intent.ACTION_VIEW), hasData("http://play.google.com/store/apps/details?id=com.abaenglish.videoclass")));
    }
}