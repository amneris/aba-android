package com.abaenglish.videoclass.oauth;

import com.abaenglish.videoclass.data.network.oauth.OAuthTokenAccessor;
import com.abaenglish.videoclass.domain.ApplicationConfiguration;
import com.github.scribejava.core.model.OAuth2AccessToken;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertNull;
import static junit.framework.Assert.fail;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by Jesus Espejo using mbp13jesus on 17/03/17.
 * for OAuth testing.
 */
public class OAuthTokenAccessorTest {

    private OAuthTokenAccessor tokenRefresher;

    @Before
    public void init() throws Exception {
        ApplicationConfiguration appConfig = new ApplicationConfiguration();
        appConfig.setUserEmail("space@space.com");
        appConfig.setUserToken("e8a1ff06229b453cd04c2225bc9bf586");
        appConfig.setGatewayUrl("https://gateway.dev.aba.land/");
        appConfig.setClientId("mobile-test-abawebapps");
        appConfig.setClientSecret("vrmQUdx3Mxw8YrP4");

        tokenRefresher = new OAuthTokenAccessor(appConfig);
    }

    @Test
    public void testGetToken() {

        OAuth2AccessToken retrievedToken = tokenRefresher.getOauth2Token().toBlocking().single();
        assertNotNull(retrievedToken);
        assertNotNull(retrievedToken.getAccessToken());
        assertNotNull(retrievedToken.getRefreshToken());
        assertTrue(retrievedToken.getExpiresIn() > 2);
    }

    @Test
    public void testGetTokenTwice() {

        OAuth2AccessToken retrievedToken = tokenRefresher.getOauth2Token().toBlocking().single();
        assertNotNull(retrievedToken);
        assertNotNull(retrievedToken.getAccessToken());
        assertNotNull(retrievedToken.getRefreshToken());
        assertTrue(retrievedToken.getExpiresIn() > 2);

        OAuth2AccessToken retrievedToken2 = tokenRefresher.getOauth2Token().toBlocking().single();
        assertEquals(retrievedToken, retrievedToken2);
        assertEquals(retrievedToken.getAccessToken(), retrievedToken2.getAccessToken());
    }

    @Test
    public void testGetTokenAfterReset() {

        OAuth2AccessToken retrievedToken = tokenRefresher.getOauth2Token().toBlocking().single();
        assertNotNull(retrievedToken);
        assertNotNull(retrievedToken.getAccessToken());
        assertNotNull(retrievedToken.getRefreshToken());
        assertTrue(retrievedToken.getExpiresIn() > 2);

        tokenRefresher.reset();

        OAuth2AccessToken retrievedToken2 = tokenRefresher.getOauth2Token().toBlocking().single();
        assertNotEquals(retrievedToken, retrievedToken2);
        assertNotEquals(retrievedToken.getAccessToken(), retrievedToken2.getAccessToken());
    }

    @Test
    public void testRefreshToken() {

        OAuth2AccessToken retrievedToken = tokenRefresher.getOauth2Token().toBlocking().single();
        assertNotNull(retrievedToken);
        assertNotNull(retrievedToken.getAccessToken());
        assertNotNull(retrievedToken.getRefreshToken());
        assertTrue(retrievedToken.getExpiresIn() > 2);

        try {
            Thread.sleep((retrievedToken.getExpiresIn() + 1) * 1000);

            OAuth2AccessToken updatedToken = tokenRefresher.refreshOauth2Token().toBlocking().single();
            assertNotNull(updatedToken);
            assertNotNull(updatedToken.getAccessToken());
            assertNotNull(updatedToken.getRefreshToken());
            assertTrue(updatedToken.getExpiresIn() > 2);

        } catch (InterruptedException e) {
            e.printStackTrace();
            fail();
        }
    }

    @Test
    public void testRefreshTokenIsExpired() {
        OAuth2AccessToken retrievedToken = tokenRefresher.getOauth2Token().toBlocking().single();
        assertNotNull(retrievedToken);
        assertNotNull(retrievedToken.getAccessToken());
        assertNotNull(retrievedToken.getRefreshToken());
        assertTrue(retrievedToken.getExpiresIn() > 2);

        try {

            long refreshTokenExpirationSeconds = 10;
            Thread.sleep((refreshTokenExpirationSeconds + 1) * 1000);

            OAuth2AccessToken refreshedToken = tokenRefresher.refreshOauth2Token().toBlocking().single();
            assertNull(refreshedToken);

        } catch (InterruptedException e) {
            e.printStackTrace();
            fail();
        } catch (Exception e) {
            if (!e.getLocalizedMessage().contains("invalid_token")) {
                e.printStackTrace();
                fail();
            }
        }

    }
}
