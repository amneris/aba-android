package com.abaenglish.videoclass.oauth;

import com.abaenglish.videoclass.data.network.oauth.OAuthClient;
import com.abaenglish.videoclass.data.network.oauth.OAuthTokenAccessor;
import com.abaenglish.videoclass.domain.ApplicationConfiguration;
import com.abaenglish.videoclass.oauth.services.Me;
import com.abaenglish.videoclass.oauth.services.MeServiceClient;

import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.CountDownLatch;

import static com.abaenglish.videoclass.data.network.retrofit.abaMomentsUtils.ABAMomentClientDefinitions.ABAMomentErrorType;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by Julien on 10/03/2017
 * for OAuth testing.
 */
public class OAuthMomentsClientTest {

    private MeServiceClient meClient;
    private OAuthTokenAccessor tokenAccessor;

    @Before
    public void init() throws Exception {
        ApplicationConfiguration appConfig = new ApplicationConfiguration();
        appConfig.setUserEmail("space@space.com");
        appConfig.setUserToken("e8a1ff06229b453cd04c2225bc9bf586");
        appConfig.setGatewayUrl("https://gateway.dev.aba.land/");
        appConfig.setClientId("mobile-test-abawebapps");
        appConfig.setClientSecret("vrmQUdx3Mxw8YrP4");

        tokenAccessor = new OAuthTokenAccessor(appConfig);

        meClient = new MeServiceClient(appConfig, tokenAccessor);
    }

    @Test
    public void testMeRequest() throws InterruptedException {

        final CountDownLatch signal = new CountDownLatch(1);
        final boolean[] succeeded = {false};

        meClient.getMeInfo(new OAuthClient.OnResponseListener<Me>() {
            @Override
            public void onResponseReceived(Me response, String error) {
                assertNotNull(response);
                succeeded[0] = ABAMomentErrorType.NONE.toString().equalsIgnoreCase(error);
                signal.countDown();
            }
        });

        signal.await();
        assertTrue(succeeded[0]);
    }


    @Test
    public void testRequestAfterAccessTokenExpiration() throws InterruptedException {

        final CountDownLatch signal = new CountDownLatch(1);
        final boolean[] succeeded = {false};

        // First call (getting a new token)
        meClient.getMeInfo(new OAuthClient.OnResponseListener<Me>() {
            @Override
            public void onResponseReceived(Me response, String error) {
                assertNotNull(response);
                succeeded[0] = ABAMomentErrorType.NONE.toString().equalsIgnoreCase(error);
                signal.countDown();
            }
        });

        signal.await();
        assertTrue(succeeded[0]);

        // waiting for access token to expire
        int expirationInSeconds = tokenAccessor.oauthToken.getExpiresIn();
        Thread.sleep(++expirationInSeconds * 1000);

        final CountDownLatch signal2 = new CountDownLatch(1);
        succeeded[0] = false;

        // Second call (refreshing token)
        meClient.getMeInfo(new OAuthClient.OnResponseListener<Me>() {
            @Override
            public void onResponseReceived(Me response, String error) {
                assertNotNull(response);
                succeeded[0] = ABAMomentErrorType.NONE.toString().equalsIgnoreCase(error);
                signal2.countDown();
            }
        });

        signal2.await();
        assertTrue(succeeded[0]);
    }

    @Test
    public void testRequestAfterRefreshTokenExpiration() throws InterruptedException {

        final CountDownLatch signal = new CountDownLatch(1);
        final boolean[] succeeded = {false};

        // First call (getting a new token)
        meClient.getMeInfo(new OAuthClient.OnResponseListener<Me>() {
            @Override
            public void onResponseReceived(Me response, String error) {
                assertNotNull(response);
                succeeded[0] = ABAMomentErrorType.NONE.toString().equalsIgnoreCase(error);
                signal.countDown();
            }
        });

        signal.await();
        assertTrue(succeeded[0]);

        // waiting for the refresh token to expire  (10 seconds hardcoded)
        int expirationInSeconds = 10;
        Thread.sleep(++expirationInSeconds * 1000);

        final CountDownLatch signal2 = new CountDownLatch(1);
        succeeded[0] = false;

        // Second call (refreshing token)
        meClient.getMeInfo(new OAuthClient.OnResponseListener<Me>() {
            @Override
            public void onResponseReceived(Me response, String error) {
                assertNotNull(response);
                succeeded[0] = ABAMomentErrorType.NONE.toString().equalsIgnoreCase(error);
                signal2.countDown();
            }
        });

        signal2.await();
        assertTrue(succeeded[0]);
    }
}