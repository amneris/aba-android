package com.abaenglish.videoclass.oauth.services;

import retrofit2.http.GET;
import retrofit2.http.Header;
import rx.Observable;

/**
 * Created by Jesus Espejo using mbp13jesus on 26/04/2017.
 */

public interface MeService {

    @GET("api/v1/users/me")
    Observable<Me> getMeInfo(@Header("Authorization") String authorization);
}
