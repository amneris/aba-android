package com.abaenglish.videoclass.oauth.services;

import com.abaenglish.videoclass.data.network.oauth.OAuthClient;
import com.abaenglish.videoclass.data.network.oauth.OAuthTokenAccessor;
import com.abaenglish.videoclass.data.network.oauth.RetrofitRxServiceFactory;
import com.abaenglish.videoclass.data.network.retrofit.abaMomentsUtils.ABAMomentClientDefinitions;
import com.abaenglish.videoclass.domain.ApplicationConfiguration;
import com.github.scribejava.core.model.OAuth2AccessToken;

import rx.Observable;
import rx.Subscriber;

/**
 * Created by Jesus Espejo using mbp13jesus on 26/04/2017.
 */

public class MeServiceClient extends OAuthClient<Me> {

    private ApplicationConfiguration appConfig;

    public MeServiceClient(ApplicationConfiguration appConfig, OAuthTokenAccessor refresher) {
        super(refresher);
        this.appConfig = appConfig;
    }

    public void getMeInfo(final OnResponseListener<Me> listener) {

        final AuthorizedAction<Me> action = new AuthorizedAction<Me>() {

            @Override
            public Observable<Me> actionCall(OAuth2AccessToken oAuth2AccessToken) {
                final MeService service = RetrofitRxServiceFactory.createRetrofitRxService(MeService.class, appConfig.getGatewayUrl());
                return service.getMeInfo(TYPE_BEARER + oAuth2AccessToken.getAccessToken());
            }
        };

        performAuthorisedCall(action).subscribe(new Subscriber<Me>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                e.printStackTrace();
                System.out.println("==============================================================");
                System.out.println("ERROR IN OAuthMomentServiceClient : Cause : " + e.getCause() + ", Message : " + e.getMessage() + " class: " + e.getClass());
                listener.onResponseReceived(null, ABAMomentClientDefinitions.ABAMomentErrorType.UNKNOWN.toString());
            }

            @Override
            public void onNext(Me me) {
                listener.onResponseReceived(me,ABAMomentClientDefinitions.ABAMomentErrorType.NONE.toString());
            }
        });
    }
}
