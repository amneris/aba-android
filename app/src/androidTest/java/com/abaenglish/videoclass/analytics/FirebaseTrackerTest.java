//package com.abaenglish.videoclass.analytics;
//
//import android.content.Context;
//import android.support.test.runner.AndroidJUnit4;
//
//import org.junit.Before;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.Mock;
//
//import static org.mockito.Mockito.times;
//import static org.mockito.Mockito.verify;
//import static org.mockito.MockitoAnnotations.initMocks;
//
//@RunWith(AndroidJUnit4.class)
//public class FirebaseTrackerTest {
//
//    @Mock private Context context;
//    @Mock private GpsIdProvider gpsIdProvider;
//    @Mock private UserInfoProvider userInfoProvider;
//
//    private FirebaseTracker firebaseTrackerManager;
//
//    @Before public void setUp() {
//        initMocks(this);
//        this.firebaseTrackerManager = new FirebaseTracker(context, gpsIdProvider, new FakeUserInfoProvider());
//    }
//
//    @Test public void trackUserLogin() throws Exception {
//        firebaseTrackerManager.trackUserLogin();
//        verify(gpsIdProvider, times(1)).getAdvertasingId();
//    }
//
//    @Test public void trackEnteredSection() throws Exception {
//        firebaseTrackerManager.trackEnteredSection(1,1,1);
//    }
//
//    @Test public void trackEnteredUnit() throws Exception {
//        firebaseTrackerManager.trackEnteredUnit(1,1);
//    }
//
//    @Test public void trackOpenedHelp() throws Exception {
//        firebaseTrackerManager.trackOpenedHelp();
//    }
//
//    @Test public void trackOpenedPrices() throws Exception {
//        firebaseTrackerManager.trackOpenedPrices();
//    }
//
//    @Test public void trackSelectedPlan() throws Exception {
//
//    }
//
//    @Test public void trackPaid() throws Exception {
//
//    }
//
//    @Test public void trackUserRegistered() throws Exception {
//
//    }
//
//    @Test public void trackChangeLevel() throws Exception {
//
//    }
//
//    @Test public void trackWatchedFilm() throws Exception {
//
//    }
//
//    @Test public void trackListenedAudio() throws Exception {
//
//    }
//
//    @Test public void trackRecordedAudio() throws Exception {
//
//    }
//
//    @Test public void trackComparedAudio() throws Exception {
//
//    }
//
//    @Test public void trackVerifiedText() throws Exception {
//
//    }
//
//    @Test public void trackGotTextSuggestion() throws Exception {
//
//    }
//
//    @Test public void trackStartedInterpretation() throws Exception {
//
//    }
//
//    @Test public void trackProgressUpdateTotal() throws Exception {
//
//    }
//
//    @Test public void trackReviewedTestResults() throws Exception {
//
//    }
//
//    @Test public void addProperty() throws Exception {
//
//    }
//
//    private class FakeUserInfoProvider implements UserInfoProvider {
//        @Override public void refresh() {}
//
//        @Override public String getUserId() { return "fakeUserId"; }
//
//        @Override public String getUserLevel() { return "3"; }
//
//        @Override public String getUserLangId() { return "it"; }
//
//        @Override public String getUserBirthday() { return "3/10/1998"; }
//
//        @Override public String getUserCountryId() { return "CountryId"; }
//
//        @Override public String getUserType() { return "3"; }
//
//        @Override public String getUserEntryDate() { return "13"; }
//
//        @Override public String getUserExpirationDate() { return "3/10/2017"; }
//
//        @Override public String getUserPartnerFirstPayId() { return "21121"; }
//    }
//}