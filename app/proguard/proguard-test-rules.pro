# Proguard rules that are applied to your test apk/code.
-ignorewarnings

# Reuse the main project's ProGuard mapping
#-applymapping ../build/outputs/mapping/base/debug/mapping.txt

# ProGuard is automatically fed the obfuscated code of the main
# project. It also needs the plain version:
#-injars ../mainproject/bin/classes
#-injars ../mainproject/libs/android-support-v4.jar


-keepattributes *Annotation*

-dontnote junit.framework.**
-dontnote junit.runner.**

-dontwarn android.test.**
-dontwarn android.support.test.**
-dontwarn org.junit.**
-dontwarn org.hamcrest.**
-dontwarn com.squareup.javawriter.JavaWriter
# Uncomment this if you use Mockito
#-dontwarn org.mockito.**