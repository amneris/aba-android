# Generates bin/proguard-mapping.txt
-printmapping mapping.txt

-keep class com.abaenglish.shepherd.** {
  public protected private *;
}

-keep class com.abaenglish.videoclass.domain.content.LevelUnitController{
  public protected private *;
}

-keep class com.abaenglish.videoclass.data.file.DownloadThread{
  public protected private *;
}

-keep class com.abaenglish.videoclass.domain.content.ProgressActionController{
  public protected private *;
}

-keep class com.abaenglish.videoclass.domain.content.SectionController{
  public protected private *;
}

-keep class com.abaenglish.videoclass.domain.SupportedVersionManager{
  public protected private *;
}

-keep class com.abaenglish.videoclass.data.persistence.** { *; }