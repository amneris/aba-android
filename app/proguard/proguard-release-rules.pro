-dontoptimize
-dontusemixedcaseclassnames
-dontskipnonpubliclibraryclasses
-dontpreverify
-verbose

-dontwarn org.apache.commons.codec.CharEncoding
-dontwarn org.apache.commons.codec.digest.DigestUtils